<?php

### todo sakrij kontroler iz url-a

require_once '../control/root_config.php'; // root putanja
// dozvola pristupa ovom fajlu
require_once ROOT.'model/access_controler.php';

// uzimamo samo u kontroleru
require_once ROOT.'model/shared_func.php';

$action = readGet('action');

if (! empty($action)) {    
    switch ($action) {
        case "test":   
            include '../view/test.php';
            break;            
            
        default:
            posaljiRezAjax("error1", "invalid action", $action);
            break;
    }  
   
}
else{  
    posaljiRezAjax("error2", "ERR no action set");
}


?>