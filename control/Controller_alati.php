<?php
/**
 * Controler za pristup alatima PDF i Elevation
 * @uses DAOjedStanja.php
 * @uses PdfCreator.class.php
 * 
 * @access getPdf
 * @access getElevation
 *
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      27.09.2018.
 * @version    fileVer 1.0 
 * 
 * @todo removeSimilarEntries()
 */
session_start();
require_once '../control/root_config.php'; // root putanja
require_once ROOT.'model/access_controler.php'; // dozvola pristupa ovom fajlu
require_once ROOT.'model/shared_func.php'; // uzimamo samo u kontroleru

#www.jawg.io api key
const APIkey_jawg = "Dzq09zL87Pr32Hu2K0SCkG6FKTTwjJOo4eNe6t8GysOBJFyEXgrjqPgekQTQPZn0";
#www.mapquest.com api key
const APIkey_mq = "aN9kyuVtV0AS7FNWdjvTi8bePswJbm6i";
########selektor koji api zelim da koristim###############
const API_SELECTOR = APIkey_mq;

$action = readPost('action');

if (! empty($action)) { 
    
    $msg="";
    $user = $_SESSION['user']; // ovo je provereno u access_controler
    $userId = (int)$user['id'];
    
    $projName = readPost('projName');
    $projId = (int)readPost('projId');
    $jsID = readPost('jsID');
    $tackeTla = readPost('tackeTla',true);
    $objektiTrase = readPost('objektiTrase',true);
    $stubnaMesta = readPost('stubovi',true);
    $tackeLanc = readPost('tackeLanc',true);
    $oznakaProv = readPost('provodnik');
    $odo = readPost('odo');
    $temperatura = readPost('temperatura');
    $naprezanje_max = readPost('naprezanje');     
    
########debug#######
// baza cita karakter po karakter, cim jedan nije ok - baza prekida dalje upisivanje u to polje
#$userId = "/2323";
#$oznakaProv = "1.2";
#$odo = "0. 1";
#$temperatura = "1.2";
#$naprezanje_max = " 2 2.2";
##################
    
    switch ($action) {
        case "getPdf":   
            
            $msg = f_proveriUnoseProjekat();            
            if ( ! $msg )  break;
                                    
            $msg .= "\n".$projName;
            $msg .= "\n".$oznakaProv;
            $msg .= "\n".$odo;
            $msg .= "\n".$temperatura;
            $msg .= "\n".$naprezanje_max;            
            
            try {
                f_fileTestAndRequire(ROOT.'model/DAOjedStanja.php');
                $daoJS = new DAOjedStanja();
                $objJS = $daoJS->selectJSById($jsID, $userId)["data"]; 
                $objJS = json_decode($objJS,TRUE);
                
                if ($daoJS->insert_PDF($userId, $jsID) == 1){
                    f_fileTestAndRequire(ROOT.'model/PdfCreator.class.php');
                    // kreira pdf fajl i automatski ga salje klijentu
                    PdfCreator::create($user,$projName,$tackeTla, $stubnaMesta, $tackeLanc, $objJS);
                }
                else{
                    posaljiRezAjax("error-ap1","pdf insert fault");                    
                }       
                
            }
            catch (Exception $e)  {
                posaljiRezAjax("error-ajs2","JS read fault");
            }
            
            break;
////////////////////////////////////////////////////////////////////////////////////
        case "getElevation":
            $lat1 = readPost('latFirst');
            $long1 = readPost('longFirst');
            $lat2 = readPost('latLast');
            $long2 = readPost('longLast'); 
            $distance = (float)readPost('distance');
            
            /* ima neki problem kada mnozim sa vel brojevima
            mnozenje sa 2 nije problem */
            if ($distance <= 100) {
                $brojTacaka = (int)$distance * 10; 
            } 
            elseif ($distance <= 500) {
                $brojTacaka = (int)$distance * 5; 
            } 
            elseif ($distance <= 1500) {
                $brojTacaka = (int)$distance * 3; 
            }
            elseif ($distance <= 3000) {
                $brojTacaka = (int)$distance * 2; 
            }
            else{
                $brojTacaka = (int)$distance;
            }
            $brojTacaka = 2*(int)$distance;
            
             
            if (empty($lat1) || !is_numeric($lat1) ||
                empty($long1) || !is_numeric($long1) ||
                empty($lat2) || !is_numeric($lat2) ||
                empty($long2) || !is_numeric($long2) ||
                empty($distance) || !is_numeric($distance)
                )
            { // ako postoji a nije broj
                posaljiRezAjax('error-ae2',"nepravilan unos koordinata");
                break;
            }
            
            $link = createLinkToJson($lat1,$lat2,$long1,$long2,$brojTacaka);
            $json = retriveJsonFile($link);
            
            $outNiz = API_SELECTOR == APIkey_jawg ?
                kreirajNizTacakaTlo_jawg($distance,$json) :
                kreirajNizTacakaTlo_mq($distance,$json);
//echo $json;

            if ($outNiz === false || sizeof($outNiz)<2) {
                // poruka useru je vec definisana u kreirajNizTacakaTlo()
                posaljiRezAjax('error-ae2', "kreirajNizTacakaTlo FALSE");
                exit();
                break;
            }
            
            $finalNiz = removeSimilarEntries($outNiz, "y", 0.1);
            
            posaljiRezAjax(STATUS_SUCCESS, "podaci su spremni", $finalNiz);
            break;                
              
////////////////////////////////////////////////////////////////////////////////////    
        default:
            posaljiRezAjax('error02',"unknown action");
            break;
    }//end switch 

}// end if !empty($action)
else{
    posaljiRezAjax('error03',"undefined action");
}

/**
 * Kreiranje linka za MapQuest[POST] / jawg[GET] metodu do json fajla sa podacima o visinama
 * @param float $lat1
 * @param float $lat2
 * @param float $long1
 * @param float $long2
 * @param int $brojTacaka
 * @return array with keys: ['url'],['dataName'],['dataValue']
 */
function createLinkToJson($lat1,$lat2,$long1,$long2,$brojTacaka){
    $out = array();
    switch (API_SELECTOR){
        case APIkey_jawg:
            if ($brojTacaka>1024) $brojTacaka = 1024; //jawg ogranicenje
            $link = "https://api.jawg.io/elevations?path=$lat1,$long1%7C$lat2,$long2&samples=$brojTacaka&access-token=".APIkey_jawg;
            $out["url"] = $link;
            break;
            
        case APIkey_mq:
            $tacke = kreirajStringTacakaZaMapQuestZahtev($lat1,$lat2,$long1,$long2,$brojTacaka);
            $link = "http://open.mapquestapi.com/elevation/v1/profile?key=".APIkey_mq."&useFilter=true&outShapeFormat=none&latLngCollection=$tacke";
            $out['url'] = "http://open.mapquestapi.com/elevation/v1/profile?key=".APIkey_mq."&useFilter=true&outShapeFormat=none";
            $out['dataName'] = 'latLngCollection';
            $out['dataValue'] = $tacke;
            break;
    }
    return $out;
};

/**
 * MapQuest - na osnovu dve latlong tacke prave se tacke izmedju
 * tako da ih ukupno bude $brojTacaka, i formira se string koji
 * odgovara mapquest zahtevu
 * @param float $lat1
 * @param float $lat2
 * @param float $long1
 * @param float $long2
 * @param int $brojTacaka
 * @return string //31.54,21.54,31.55,21.55,31.60..."
 */
function kreirajStringTacakaZaMapQuestZahtev($lat1,$lat2,$long1,$long2,$brojTacaka){
    if($brojTacaka > 10000) $brojTacaka = 10000; //zastita
    $latDiff = floatval($lat2) - floatval($lat1);
    $longDiff = floatval($long2) - floatval($long1);
    
    $increment_lat = $latDiff/$brojTacaka;
    $increment_long = $longDiff/$brojTacaka;    
    
    $out = $lat1.",".$long1;
    for ($i = 1; $i < $brojTacaka-1; $i++) { // prva i poslednju tacku definisemo van petlje
        $latNew =   $lat1 + $i*$increment_lat;
        $longNew = $long1 + $i*$increment_long;
        $out .= ",".$latNew.",".$longNew;
    }    
    $out .= ",".$lat2.",".$long2;

    return $out;    
}

/**
 * Koristici GET/POST skidamo sa API url json fajl sa podacima o visinama
 * @param array with keys: ['url'],['dataName'],['dataValue'] 
 * @return object JSON 
 */
function retriveJsonFile($linkNiz){
    ini_set("allow_url_fopen", 1); // mora da se setuje zbog file_get_contents()
    
    // iskljuci poruke da ih ne bi user video    
    if (!defined("IS_LOCAL")) error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);     
    
    switch (API_SELECTOR){
        case APIkey_jawg: // GET metoda
            $link = $linkNiz["url"];
            try {
                error_reporting(E_ALL & ~E_WARNING);//ne treba mi warning ako nesto nije u redu - to cu sam da obradim
                $json = file_get_contents($link);
                error_reporting(E_ALL & E_WARNING);//vracam
                if ($json === false){ // u pitanju je graska pri pristupu fajlu, mora da se koristi ===
                    if (defined("IS_LOCAL")){
                        // ucitavamo localni fajl - za dev
                        $json = file_get_contents("../devData/elevations.json");
                        break;
                    }
                    else{
                        // u pitanju je live verzija, saljemo podatke o greski korinniku
                        posaljiRezAjax("errorE-1", "podaci nisu dostupni",$link);
                        exit();
                        break; // prekidamo dalje izvrsavanje
                    }
                }
            } catch (Exception $e) {
                posaljiRezAjax("errorE-2", "podaci nisu dostupni",$link);
                exit();
            }
            break;
            
        case APIkey_mq: // POST metoda
            
            $link = $linkNiz["url"];
            $data = array($linkNiz["dataName"] => $linkNiz["dataValue"]);

            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    ### mora ovako - ne moze application/json
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                ),
            ); 
            $context  = stream_context_create($options);
            
            try {
                error_reporting(E_ALL & ~E_WARNING);//ne treba mi warning ako nesto nije u redu - to cu sam da obradim
                $json = file_get_contents($link, false, $context);
                error_reporting(E_ALL & E_WARNING);//vracam
                if ($json === false){ // u pitanju je graska pri pristupu fajlu, mora da se koristi ===
                    if (defined("IS_LOCAL")){
                        // ucitavamo localni fajl - za dev
                        $json = file_get_contents("../devData/elevations_mq.json");
                        break;
                    }
                    else{
                        // u pitanju je live verzija, saljemo podatke o greski korinniku
                        posaljiRezAjax("errorE-1", "podaci nisu dostupni",$link);
                        exit();
                        break; // prekidamo dalje izvrsavanje
                    }
                }
            } catch (Exception $e) {
                posaljiRezAjax("errorE-2", "podaci nisu dostupni",$link);
                exit();
            }
          
            break;
            
    }
    ini_set("allow_url_fopen", 0);
    
    // vrati nazad warning poruke
    if (!defined("IS_LOCAL")) error_reporting(E_ALL & E_WARNING & E_NOTICE);   
    return $json;
};

/**
 * MapQuest - na osnovu json objekta kreira niz tacaka [stacionaza, visina] koji ce biti posat klijentu
 * @param float $distance
 * @param object $json
 * @return array["x","y"] ili FALSE ako je greska
 */
function  kreirajNizTacakaTlo_mq($distance,$json) {
    $distance = floatval($distance);
    $jsonDecod = json_decode($json,TRUE);
    
    $jsonStatus = $jsonDecod["info"]["statuscode"];
    if ($jsonStatus !== 0) { // prema MQ dokumentaciji
        // ako status nije 0 onda je neka greska -> message
        error_reporting(E_ALL & ~E_NOTICE); // iskljuci E_NOTICE poruke da ih ne bi user video
        $jsonMsg = $jsonDecod["info"]["messages"][0];
        error_reporting(E_ALL & E_NOTICE); // iskljuci E_NOTICE poruke da ih ne bi user video
        return false;
    }
    
    if ( ! $jsonDecod["elevationProfile"] ) return false;
    $data = $jsonDecod["elevationProfile"];
    
    $outNiz = array();
    foreach ($data as $point) {
        
        ### ovo nemoze ! $point['distance']
        ### jer distanca moze da bude i 0
        if ( !isset($point['height']) || !isset($point['distance']) ) {
            continue; // nastavljamo dalje
        }
        $x = $point['distance']*1000;        
        $y = $point['height'];        
        $outNiz[] = array("x" => round($x,2) , "y" => round($y,2));
        
    }
    
    return $outNiz;
}

/**
 * Brisanje clanova niza cija $key vrednost nije veca za $epsilon veca od $key vrednosti susednih
 * clanova niza
 * @param array $inNiz 
 * @param string $key npr.'x', 'visina'...
 * @param float $epsilon npr. 0.1 , 0.005...
 * @return array outNiz
 */
function removeSimilarEntries($inNiz, $key, $epsilon) {
    if (!isset($inNiz) || !is_array($inNiz) || !array_key_exists($key, $inNiz[0])) {
        //posaljiRezAjax("error-ajs2","JS read fault");
        return $inNiz; // nista nije izmenjeno
    }
return $inNiz;
### todo - fix ovo ispod
    
    $outNiz = array();
    $i = 0;
    while ($i < sizeof($inNiz) ) {
        $outNiz[] = $inNiz[$i];
        $curent = $inNiz[$i][$key];
        $i++;
        $j=$i;
        if ($i<sizeof($inNiz)) {
            $next = $inNiz[$j][$key];
            $diff = $next-$curent;
            $znak = $znakNew = $diff<0 ? "-" : ($diff>0 ? "+" : "=");
        }
        while ($j < sizeof($inNiz)-1) {
            if ($znak == "=") { 
                $next = $inNiz[$j][$key]; 
                $nextNew = $inNiz[$j+1][$key];
                $diffNovo = $nextNew-$next;
                $znakNew = $diffNovo<0 ? "-" : ($diffNovo>0 ? "+" : "=");
                $j++;
                if ($znakNew != "=") {
                    $i=$j-1;
                    break;
                }
                
                
            
            } 
            else{
                if ( abs($diff) >= $epsilon ||
                $znak != $znakNew) {
                    $i=$j;
                    break;
                }
                else{
                    $j++;                
                    $next = $inNiz[$j][$key];
                    $diff = $next - $curent;
                    
                    
                    $nextNew = $inNiz[$j+1][$key];
                    $znakNew = ($nextNew-$next)<0 ? "-" : (($nextNew-$next)>0 ? "+" : "=");
                }
            }
        }
    }
    
    return $outNiz;
}



/**
 * JAWG - na osnovu json objekta kreira niz tacaka [stacionaza, visina] koji ce biti posat klijentu
 * @param float $distance
 * @param object $json
 * @return array["x","y"] ili FALSE ako je greska
 */
function kreirajNizTacakaTlo_jawg($distance,$json) {
    $distance = floatval($distance);
    $jsonDecod = json_decode($json,TRUE);
    $latlongA = $jsonDecod[0]['location'];
    $latlongB = end($jsonDecod)['location'];
    $latDiff = floatval($latlongA["lat"]) - floatval($latlongB["lat"]);
    $longDiff = floatval($latlongA["lng"]) - floatval($latlongB["lng"]);
    
    if($latDiff>=$longDiff){
        $skalarName = "lat";
        $skalarDiff = $latDiff;
    }
    else{
        $skalarName = "lng";
        $skalarDiff = $longDiff;
    }
    
    $outNiz = array();
    foreach ($jsonDecod as $point) {
        if (! $point['location'][$skalarName]) {
            return false;
        }
        $razlika = abs( $point['location'][$skalarName] - $latlongA[$skalarName] );
        $procenat = $razlika/$skalarDiff; //ustvari u rel.jedinicama
        $x= $procenat * $distance;
        
        $y = $point["elevation"];
        
        $outNiz[] = array("x" => round($x,2) , "y" => round($y,2));
        
    }
    
    return $outNiz;
}


?>