<?php
/**
 * Controler za pristup bazi - kreiranje lancanice i tabele ugiba
 * 
 * @uses DAOjedStanja.php
 * @uses ProracunTacakaLancanice.php
 * @uses JednacinaStanja.class.php
 * 
 * @access getLancanica
 * @access GetTabelaUgiba
 *
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      27.09.2018.
 * @version    fileVer 1.0
 *
 */
session_start();
require_once '../control/root_config.php'; // root putanja
require_once ROOT . 'model/access_controler.php'; // dozvola pristupa ovom fajlu
require_once ROOT . 'model/shared_func.php'; // uzimamo samo u kontroleru

$action = readPost('action');

if (! empty($action)) {
    
    $msg = "";
    $user = $_SESSION['user']; // ovo je provereno u access_controler
    $userId = (int) $user['id'];
    
    $tackeTla = readPost('tackeTla', true);
    $stubnaMesta = readPost('stubovi', true);
    $oznakaProv = readPost('provodnik');
    $odo = readPost('odo');
    $temperatura = readPost('temperatura');
    $naprezanje_max = readPost('naprezanje');
    
    $msgErr = "";
    
    // ##debug##########
    // $oznakaProv ="Al/Ce-70/12" ;
    // $odo = 2.5;
    // $temperatura = 55;
    // $naprezanje_max= 8;
    // ##############
    
    if (empty($tackeTla))
        $msgErr .= "\nnisu uneti podaci o uzduznom profilu trase";
    elseif (! is_array($tackeTla) && sizeof($tackeTla) < 2)
        $msgErr .= "\nmoraju biti unete bar dve tacke uzduznog profila";
    
    if (empty($stubnaMesta))
        $msgErr .= "\nnisu uneti podaci o stubnim mestima";
    elseif (! is_array($stubnaMesta) || sizeof($stubnaMesta) < 2)
        $msgErr .= "\nmora biti uneta bar dva stubna mesta";
    
    if (empty($oznakaProv) || empty($odo) || (empty($temperatura) && $temperatura != 0) || empty($naprezanje_max))
        $msgErr .= "\nsva input polja moraju biti popunjena";
    
    if (! is_numeric($odo) || ! is_numeric($temperatura) || empty($naprezanje_max))
        $msgErr .= "\nodo, temperatura i naprezanje mora biti brojna vrednost";
    
    $tipProv = check_oznakaProv($userId, $oznakaProv);
    if (! $tipProv)
        $msgErr .= "\nnepoznata oznaka provodnika";
    
        
    if (!empty($msgErr)){
        posaljiRezAjax('errorGL-1', $msgErr);
        exit();        
    }
    
    f_fileTestAndRequire(ROOT.'model/ProracunTacakaLancanice.php');
    $nizRaspona = f_kreiraj_nizRaspona($stubnaMesta);
    if ($nizRaspona === FALSE) {
        posaljiRezAjax('errorGL-2', $msgErr);
        exit();
    }
    // ovo ispod ne proveravam jer su ulazni parametri vec provereni
    $nizCosFi = f_kreiraj_nizCosFi($nizRaspona, $stubnaMesta);
    $raspon_id = f_idealniRaspon($nizRaspona, $nizCosFi);
    $cosFi_id = f_idealniCosFi($nizRaspona, $nizCosFi);  
        
    switch ($action) {
        case "getLancanica":            
            f_fileTestAndRequire(ROOT.'model/JednacinaStanja.class.php');
            $jedStanja = JednacinaStanja::create($tipProv, $naprezanje_max, $raspon_id, $cosFi_id, $odo, $temperatura);
            
            if (! empty($jedStanja)) {
                f_fileTestAndRequire(ROOT.'model/DAOjedStanja.php');
                $daoJS = new DAOjedStanja();
                $userId = (int) $user['id'];
                $daoJS->insert_JS($userId, json_encode($jedStanja));
                $jsID = (int) $daoJS->getIDofLastJS($userId)["id"];
            }
            
            $parametarLancanice = $jedStanja->getParametarLancanice();
            $nizTacaka_lanc = f_tackeLancanice($parametarLancanice, $stubnaMesta);
            
            posaljiRezAjax(STATUS_SUCCESS, $jsID, $nizTacaka_lanc);
                        
            break;
        
        case "GetTabelaUgiba":
            
            $nizJS = array();
            $matricaUgiba = array(); // [nizRaspona][nizTemperatura]
            f_fileTestAndRequire(ROOT.'model/JednacinaStanja.class.php');
            
            foreach (NIZ_TEMPERATURA as $temperatura) {
                $nizJS[] = JednacinaStanja::create($tipProv, $naprezanje_max, $raspon_id, $cosFi_id, $odo, $temperatura);
            } 
            
            $specTezinaProv = $tipProv["tezina"];
            $tezinaProvSaLedom = $nizJS[0]->getTezinaProv_minus5led();
            
            for ($i = 0; $i < sizeof($nizRaspona); $i++) {
                $matricaUgiba[$i] = array();
                for ($t = 0; $t < sizeof(NIZ_TEMPERATURA)-1; $t++) {                    
                    $tezina = $specTezinaProv;
                    $matricaUgiba[$i][]= round(JednacinaStanja::UgibUcm($nizRaspona[$i], $nizCosFi[$i], $nizJS[$t]->getSigmaNOVO(), $tezina), 0);
                } 
                $tezina = $tezinaProvSaLedom;
                $matricaUgiba[$i][]= round(JednacinaStanja::UgibUcm($nizRaspona[$i], $nizCosFi[$i], $nizJS[$t]->getSigmaNOVO(), $tezina), 0);
            }
            
            $outData = array();
            $outData["ugib"] = $matricaUgiba;
            $outData["heder"] = array(
                $oznakaProv,
                round($specTezinaProv,8),
                round($tezinaProvSaLedom,8),
                round($nizJS[0]->getAkr(),2),
                round($raspon_id,2),
                round($cosFi_id,4)
            );
            
            posaljiRezAjax(STATUS_SUCCESS, "uspesno kreirani podaci tabele ugiba", $outData);
            
            break;
        
        default:
            posaljiRezAjax('error02', "unknown action");
            break;
    }
} else {
    posaljiRezAjax('error03', "undefined action");
}

?>