<?php
/**
 * Controler za pristup bazi
 *
 * @uses DAOuserProjekti.php
 *
 * @access save
 * @access update
 * @access getUserProjectsList
 * @access getSingleProj
 * @access delete
 *
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      27.09.2018.
 * @version    fileVer 1.0
 */
session_start();
require_once '../control/root_config.php'; // root putanja
require_once ROOT.'model/access_controler.php'; // dozvola pristupa ovom fajlu
require_once ROOT.'model/shared_func.php'; // uzimamo samo u kontroleru

$action = readPost('action');

if (! empty($action)) {
    f_fileTestAndRequire(ROOT.'model/DAOuserProjekti.php');
    $dao = new DAOuserProjekti();
    
    $msg="";    
    $user = $_SESSION['user']; // ovo je provereno u access_controler
    $userId = (int)$user['id'];
    
    $projName = readPost('projName');
    $projId = (int)readPost('projId');
    $jsID = readPost('jsID');
    $tackeTla = readPost('tackeTla',true);
    $objektiTrase = readPost('objektiTrase',true);
    $stubnaMesta = readPost('stubovi',true);
    $tackeLanc = readPost('tackeLanc',true);
    $oznakaProv = readPost('provodnik');
    $odo = readPost('odo');
    $temperatura = readPost('temperatura');
    $naprezanje_max = readPost('naprezanje');  
    
    ########debug#######
    // baza cita karakter po karakter, cim jedan nije ok - baza prekida dalje upisivanje u to polje
    #$userId = "/2323";
    #$oznakaProv = "1.2";
    #$odo = 2;
    #$temperatura = "1.2";
    #$naprezanje_max = " 2 2.2";
    ##################
    
    switch ($action) {
        case "save":  
                        
            $msg = f_proveriUnoseProjekat();            
            if ( ! $msg )  break;
                
            $msg .= "\n".$projName;
            $msg .= "\n".$oznakaProv;
            $msg .= "\n".$odo;
            $msg .= "\n".$temperatura;
            $msg .= "\n".$naprezanje_max;            
            
            try {
                $rowCount = $dao->insert_userProj($userId,$projName, $oznakaProv, $odo, $temperatura,
                    $naprezanje_max, json_encode($tackeTla), json_encode($objektiTrase), json_encode($stubnaMesta), json_encode($tackeLanc), $jsID);
                if ($rowCount == 1) {
                    // samo jedan red/unos u bazi je sacuvan
                    posaljiRezAjax(STATUS_SUCCESS,$msg);
                }
                elseif ($rowCount == 0){
                    // nista nije promenjeno
                    posaljiRezAjax("error-pjs1","insert project fault");
                }
                else{
                    // promenjeno je vise redova ili exception - ALERT
                    f_userUnsetAjaxCall();
                }
            } catch (Exception $e) {
                posaljiRezAjax("error-pjs2", "prov save fault");
            }
            
            break;
////////////////////////////////////////////////////////////////////////////////////    
        case "update":
            
            $msg = f_proveriUnoseProjekat();            
            if ( ! $msg ) break;
            elseif(empty($projId)&&$projId!=0){
                posaljiRezAjax('error-pju1',"nisu uneta sva polja potrebna za kreiranje lancanice");
                break;
            }               
            
            $msg .= "\n".$projName;
            $msg .= "\n".$oznakaProv;
            $msg .= "\n".$odo;
            $msg .= "\n".$temperatura;
            $msg .= "\n".$naprezanje_max;
            
            try {
                $rowCount = $dao->update_userProj($projId,$userId,$projName, $oznakaProv, $odo, $temperatura,
                    $naprezanje_max, json_encode($tackeTla), json_encode($objektiTrase), json_encode($stubnaMesta), json_encode($tackeLanc),$jsID);
                if ($rowCount == 1) {
                    // samo jedan red/unos u bazi je sacuvan
                    posaljiRezAjax(STATUS_SUCCESS,$msg);
                }
                elseif ($rowCount == 0){
                    // nista nije promenjeno
                    posaljiRezAjax("error-pju2","promene nisu sacuvane");
                }
                else{
                    // promenjeno je vise redova ili exception - ALERT
                    f_userUnsetAjaxCall(); 
                }
            } catch (Exception $e) {
                posaljiRezAjax("error-pju3", "prov update fault");
            }
            
            break;
/////////////////////////////////////////////////////////////////////
        case "getUserProjectsList":
                                          
            $data = $dao->getList_proj_user($userId);
            if (is_array($data) && sizeof($data) >=1)
                posaljiRezAjax(STATUS_SUCCESS,"list created",$data);
            else
                posaljiRezAjax("error-pjg3","no project available");  
                
            break;
///////////////////////////////////////////////////////////////////////////////
        case "getSingleProj":
            if (empty($projId) || !is_int($projId) ){                    
                posaljiRezAjax('error-pjgs1',$projId);
                break;
            }
            
            $data = $dao->selectProjByID($userId, $projId);
            if (is_array($data) && sizeof($data) >=1)
                posaljiRezAjax(STATUS_SUCCESS,"project selected",$data);
            else
                posaljiRezAjax("error-pjgs2","project NOT available"); 
            
            break;
/////////////////////////////////////////////////////////////////////////                
        case "delete":
            if (empty($projId) || empty($projName) ||!is_int($projId) ){
                posaljiRezAjax('error-pjd1',$projId);
                break;
            }
            
            
            try {
                $rowCount = $dao->deleteProjByID($userId, $projId);
                if ($rowCount == 1) {
                    // samo jedan red/unos u bazi je obrisan
                    posaljiRezAjax(STATUS_SUCCESS,"deleted",$projId); // treba mi $projId kod usera
                }
                elseif ($rowCount == 0){
                    // nista nije promenjeno
                    posaljiRezAjax("error-pjd2"," nije moguce brisanje $projName"); 
                }
                else{
                    // promenjeno je vise redova ili exception - ALERT
                    f_userUnsetAjaxCall();
                }
            } catch (Exception $e) {
                posaljiRezAjax("error-pju3", "prov delete fault");
            }
            
            break;
////////////////////////////////////////////////////////////////////                   
        default:
            posaljiRezAjax('error02',"unknown action");
            break;
    }//end switch 

}// end if !empty($action)
else{
    posaljiRezAjax('error03',"undefined action");
}
    
  


?>