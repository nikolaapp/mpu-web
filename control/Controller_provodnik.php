<?php
/**
 * Controler za pristup bazi
 * 
 * @uses DAOprovodnici.php
 * 
 * @access getUserProvList
 * @access getProv
 * @access saveProv
 * @access editProv
 * @access deleteProv
 *
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      27.09.2018.
 * @version    fileVer 1.0
 */

session_start();
require_once '../control/root_config.php'; // root putanja
require_once ROOT . 'model/access_controler.php'; // dozvola pristupa ovom fajlu
require_once ROOT . 'model/shared_func.php'; // uzimamo samo u kontroleru

$action = readPost('action');

if (! empty($action)) {
    
    $msg = "";
    $user = $_SESSION['user']; // ovo je provereno u access_controler
    $userId = (int) $user['id'];
    
    f_fileTestAndRequire(ROOT.'model/DAOprovodnici.php');
    $daoProv = new DAOprovodnici();
    
    // #######debug#######
    // baza cita karakter po karakter, cim jedan nije ok - baza prekida dalje upisivanje u to polje
    // $userId = "/2323";
    // $oznakaProv = "1.2";
    // $odo = "0. 1";
    // $temperatura = "1.2";
    // $naprezanje_max = " 2 2.2";
    // #################
    
    switch ($action) {
        case "getUserProvList":
            try {
                $listaProv = $daoProv->get_provList_user($userId);
                posaljiRezAjax(STATUS_SUCCESS, "podaci su spremni", $listaProv);
                break;
            } catch (Exception $e) {
                posaljiRezAjax("error-p1", "prov lista read fault");
            }
            break;
// //////////////////////////////////////////////////////////////////////////////////
        case "getProv":
            $provId = (int)readPost('provId');
            if (empty($provId) || !is_int($provId) ){
                posaljiRezAjax('error-gp3',"ne odgovarajuca sifra provodnika");
                break;
            }
            
            try {
                $singleProv = $daoProv->selectProvByID($provId, $userId);
                posaljiRezAjax(STATUS_SUCCESS, "podaci su spremni", $singleProv);
                break;
            } catch (Exception $e) {
                posaljiRezAjax("error-p2", "single prov read fault");
            }
            break;
// //////////////////////////////////////////////////////////////////////////////////
        case "saveProv":
            
            $provName = readPost('provName');            
            if (empty($provName)){
                $ms = "Nepravilna oznaka provodnika";
                posaljiRezAjax('errorP2', $ms);
                break;                
            }            
            if (check_oznakaProv($userId, $provName)) {
                $ms = "Vec postoji provodnik sa oznakom $provName";
                posaljiRezAjax('errorP2-1', $ms);
                break;
            }            
            
            f_readData();
            if (f_validateData($userId) === false) break;
            
            try {
                $rowCount = $daoProv->saveNewProv($userId, $provName, $provPresek, $provPrecnik, $provTezina, $provE, $provAlfa, $provI);
                if ($rowCount == 1) {
                    // samo jedan red/unos u bazi je sacuvan                    
                    posaljiRezAjax(STATUS_SUCCESS, "$provName");
                }
                elseif ($rowCount == 0){
                    // nista nije promenjeno
                    posaljiRezAjax("error-ps1", "nije moguce sacuvati ovaj unos");
                }
                else{
                    f_userUnsetAjaxCall();
                }
                
                break;
            } catch (Exception $e) {
                posaljiRezAjax("error-sp3", "prov save fault");
            }
            break;
//////////////////////////////////////////////////////////////////////////////////
        case "editProv":
            
            $provId = (int)readPost('provId');
            if (empty($provId) || !is_int($provId) ){
                posaljiRezAjax('error-pe-1',"ne odgovarajuca sifra provodnika", "$provId");
                break;
            }
            
            f_readData();            
            if (f_validateData($userId) === false) break; // poruka je definisana u funkciji
                
            try {
                $rowCount = $daoProv->editProvByID($userId, $provId, $provPresek, $provPrecnik, $provTezina, $provE, $provAlfa, $provI);
                if ($rowCount == 1) {
                    // samo jedan red/unos u bazi je promenjen
                    posaljiRezAjax(STATUS_SUCCESS, "sacuvane su izmene");
                }
                elseif ($rowCount == 0){
                    // nista nije promenjeno
                    posaljiRezAjax("error-pe5", "nije moguce izmeniti ovaj unos");
                }
                else{
                    f_userUnsetAjaxCall();
                }
                
                break;
            } catch (Exception $e) {
                posaljiRezAjax("error-pe3", "prov edit fault");
            }
            break;
// //////////////////////////////////////////////////////////////////////////////////
        case "deleteProv":            
            $provId = (int)readPost('provId');
            if (empty($provId) || !is_int($provId) ){
                posaljiRezAjax('error-pd-1',"ne odgovarajuca sifra provodnika", "$provId");
                break;
            }
            
            $provTmp = $daoProv->selectProvByID($provId, $userId);
            if (is_array($provTmp)) $provName = $provTmp['oznaka'];
            
            try {
                $rowCount = $daoProv->deleteProvByID($userId, $provId);
                if ($rowCount == 1) {
                    // samo jedan red/unos u bazi je sacuvan
                    posaljiRezAjax(STATUS_SUCCESS, "$provName");
                }
                elseif ($rowCount == 0){
                    // nista nije promenjeno
                    posaljiRezAjax("error-pd-2", "nije moguce brisanje");
                }
                else{
                    f_userUnsetAjaxCall();
                }
                
                break;
            } catch (Exception $e) {
                posaljiRezAjax("error-pd-3", "prov delete fault");
            }
            
            break;
        // //////////////////////////////////////////////////////////////////////////////////
        default:
            posaljiRezAjax('error02', "unknown action");
            break;
    } // end switch
} // end if !empty($action)
else {
    posaljiRezAjax('error03', "undefined action");
}

/**
 * Ucitavanje pristiglih podataka
 * koristi save i edit/update
 */
function f_readData(){
    global $provPresek,$provPrecnik;
    global $provTezina,$provE,$provAlfa,$provI;

    $provPresek = (float) readPost('provPresek');
    $provPrecnik = (float) readPost('provPrecnik');
    $provTezina = (float) readPost('provTezina');
    $provE = (float) readPost('provE');
    $provAlfa = (float) readPost('provAlfa');
    $provI = (float) readPost('provI');
}

/**
 * Validacija pristiglih podataka
 * @return FALSE ako je greska /  TRUE ako je ok
 */
function f_validateData($userId){ 
    global $provPresek,$provPrecnik;
    global $provTezina,$provE,$provAlfa,$provI;
    
    $msg = "";
    
    if (empty($provPresek) || empty($provPrecnik)
        || empty($provTezina) || empty($provE) || empty($provAlfa)
        || (empty($provI) && $provI != 0) ) {
        posaljiRezAjax('errorP1', "nisu uneta sva polja potrebna za kreiranje provodnika");
        return false;
    }     
    
    if (!is_numeric($provPresek) || !is_numeric($provPrecnik) || !is_numeric($provI) || 
        !is_numeric($provTezina) || !is_numeric($provE) || !is_numeric($provAlfa)){ 
        posaljiRezAjax('errorP3',"svi parametri moraju biti brojne vrednosti");
        return false;
    }    
    
    return true;
}

?>