<?php
/**
 * Controler za pristup bazi
 * 
 * @uses DAOuser.php
 * 
 * @access Login
 * @access Register
 * @access Logout
 *
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      27.09.2018.
 * @version    fileVer 1.0
 * 
 * @todo sakrij adresu kontrolera iz url-a
 */

session_start();
require_once '../control/root_config.php'; // root putanja
require_once ROOT.'model/access_controler.php'; // dozvola pristupa ovom fajlu
require_once ROOT.'model/shared_func.php'; // uzimamo samo u kontroleru

const LOCATION_REG = "location: ../view/login.php?tab=register";
const LOCATION_LOG = "location: ../view/login.php?tab=login";

$action = readPost('action');

if (! empty($action)) {
    f_fileTestAndRequire(ROOT.'model/DAOuser.php');
    
    $ime = readPost('ime');
    $email = readPost('email');
    $username = readPost('username');
    $password = readPost('password');
    $passwordConf = readPost('passwordConf');
    $prikaz = readPost('prikaz');
    $id = readPost('id');    
    
    switch ($action) {
        case "Login":  
            if (! empty($username) && ! empty($password)) {
                $dao = new DAOuser(LOCATION_LOG);  
 /*  debug       
                if (false) {
                    $start = microtime(true);
                    for ($i = 0; $i < 5000; $i++)
                        $user = $dao->selectUserByUsernameAndPassword($username, $password);
                    
                    $time_elapsed_secs = microtime(true) - $start;
                    $_SESSION['msg'] = $time_elapsed_secs;
                    header(LOCATION_LOG);
                    exit();
                }
 */
                    
                // ako je doslo do greske hvata ga catch i f_debug()
                // ako nema ni jednog pogotka onda je false
                // ako je nasao nesto onda je array
                $user = $dao->selectUserByUsernameAndPassword($username, $password);
                 
                if ($user) { // pronadjen je user u bazi
                    
                    if ($prikaz == "YES") { // preko cookie pamtimo user/pass
                        $cookie = array(
                            "username" => $username,
                            "password" => $password
                        );
                        // mora da se stavi '/' - da bi cookie bio dostupan u celom domenu (u svim folderima)
                        setcookie("cookie_user", json_encode($cookie), time() + 1 * 24 * 60 * 60, '/');
                    } else {
                        // ne moze preko unset
                        // unset($_COOKIE['cookie_user']);
                        // mora ovako - i obavezno '/'
                        setcookie("cookie_user", "", time() - 3600, '/');
                    }
                    
                    $_SESSION['user'] = $user; // setujemo aktivnog user sa njegovim podacima
                    unset($_SESSION['msg']); // brisemo err poruku
                    
                    // heder vrsi redirekciju, a posto vraca void onda ne utice na exit
                    // exit prekida izvrsavanje skripa
                    exit(header("location: ../view/webapp.php"));
                    
                } else {
                    f_debug("Pogresan user/pass","",LOCATION_LOG);
                }
            } else {
                f_debug("Morate popuniti sva polja","",LOCATION_LOG);
            }
            
            break;
            
            
        case "Register":
            $dao = new DAOuser(LOCATION_REG); 
            
            if ( empty($username) || empty($password) ||empty($email) || empty($ime) || empty($passwordConf)) {
                f_debug("morate popuniti sva polja","",LOCATION_REG);
            }
            
            $emailS = filter_var($email, FILTER_SANITIZE_EMAIL);
            if ( filter_var($emailS, FILTER_VALIDATE_EMAIL) === false ||
                $emailS != $email) {
                    f_debug("Nepravilan format email adrese","",LOCATION_REG);
            }
            
            if ($password !== $passwordConf) {
                f_debug("Password nije pravilno potvrdjen","",LOCATION_REG);
            }
            
            $pom=$dao->doesUsernameExist($username);
            if ($pom["broj"] >= 1) { 
                // proveravam da li postoji tacno jedan u bazi, ako ih ima vise onda je vec postoji neka greska u bazi
                f_debug("username $username je vec zauzet",$pom,LOCATION_REG);
            }
            
            $pom=$dao->doesEmailExist($email);                        
            if ($pom["broj"] >= 1) { 
                // proveravam da li postoji tacno jedan u bazi, ako ih ima vise onda je vec postoji neka greska u bazi
                f_debug("email $email je vec zauzet",$pom,LOCATION_REG);
            }

// prosli smo sve provere 

            //prvo upisujemo user u bazu, ali sa isActive === false;
            $hash = md5( rand(0,1000)); // string duzine 32 karak
                        
            try {
                $rowCount = $dao->insert_user($ime, $username, $password, $email, $hash);
                if ($rowCount == 1) {
                    // sve je OK - nastavi dalje
                }
                elseif ($rowCount == 0){
                    // nista nije promenjeno
                    posaljiRezAjax("error-ur1", "registracija trenutno nije moguca",LOCATION_REG);
                    break;
                }
                else{
                    //f_userUnsetAjaxCall(); nije u pitanju ajax
                    posaljiRezAjax("error-ur2", "registracija trenutno nije moguca",LOCATION_REG);
                    break;
                }
            } catch (Exception $e) {
                posaljiRezAjax("error-ur3", "registracija trenutno nije moguca",LOCATION_REG);
            }
            
// upisan je novi user u bazu            
            
            $lastInsertId = $dao->lastInsertId();
            $newUser = $dao->selectUserById($lastInsertId);
            
            if (!$newUser) {
                f_debug("error-ur4 - registracija trenutno nije moguca","selectUserById() $lastInsertId",LOCATION_REG);
            }

//ocitan je novi user iz baze            
            
            f_fileTestAndRequire(ROOT.'model/Mail.php');
            $flag = f_createMail($newUser["ime"], $newUser["username"], $newUser["password"],  $newUser["email"], $hash);
            
            if (!$flag) { // sve ok
                $_SESSION['msg']  = "<span style='color:green'>pre logovanja proverite email '$email' kako bi verifikovali unete podatke</span>";
                exit(header(LOCATION_LOG));
            }
            else{ //greska
                if (defined("IS_LOCAL")) {
                    //f_debug("bilo je problema - proveri da li je poslat mejl na adresu '$email'",$flag,LOCATION_REG);
                }
                f_debug("trenutno nije moguce poslati mejl na adresu '$email'",$flag,LOCATION_REG);           
            }
            
            break;
            

        case "Logout":
            session_unset();
            session_destroy();
            exit(header(LOCATION_LOG));
            break;            
       
        default:
            f_debug(1919,"invalid action: $action",LOCATION_LOG);
            break;
    }
} else {
    f_debug(1917,"no action",LOCATION_LOG);
}

?>