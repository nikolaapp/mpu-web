<?php
/**
 * Controler za pristup bazi
 *
 * @uses DAOuserHistory.php
 *
 * @access GetInfo
 * @access GetHelpFile_trasa
 * @access GetHelpFile_objekti 
 *
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      27.09.2018.
 * @version    fileVer 1.0
 * 
 */
session_start();
require_once '../control/root_config.php'; // root putanja
require_once ROOT.'model/access_controler.php'; // dozvola pristupa ovom fajlu
require_once ROOT.'model/shared_func.php'; // uzimamo samo u kontroleru

$action = readPost('action');

if (! empty($action)) {
    $msg="";
    $user = $_SESSION['user']; // ovo je provereno u access_controler
    $userId = (int)$user['id'];      
    
    switch ($action) {
        case "GetInfo":
            $msgErr=""; 
                    
            if(empty($msgErr)){
                
                f_fileTestAndRequire(ROOT.'model/DAOuserHistory.php');
                $dao = new DAOuserHistory();
                
                $output_01 = array(); // istorija za 1 dan
                $output_30 = array(); // istorija za 30 dana
                $output_total = array(); // istorija - ukuno
                $output_quata = array(); // max kvota za free/lite/pro
                
                $output_01["brojProj"]=$dao->getTotalUserProj($userId, 1) ["broj"];
                $output_01["brojJS"]=$dao->getTotalUserJS($userId,1) ["broj"];
                $output_01["brojPDF"]=$dao->getTotalUserPDF($userId,1) ["broj"];
                
                $output_30["brojProj"]=$dao->getTotalUserProj($userId, 30) ["broj"];
                $output_30["brojJS"]=$dao->getTotalUserJS($userId,30) ["broj"];
                $output_30["brojPDF"]=$dao->getTotalUserPDF($userId,30) ["broj"];
                
                $output_total["brojProj"]=$dao->getTotalUserProj($userId, 9999) ["broj"];
                $output_total["brojJS"]=$dao->getTotalUserJS($userId,9999) ["broj"];
                $output_total["brojPDF"]=$dao->getTotalUserPDF($userId,9999) ["broj"];
                
                $levelName = $user["level"];
                $output_quota=$dao->getUserLevelQuota($levelName);
                     
                $output = array();                
                $output["d01"] = $output_01;
                $output["d30"] = $output_30;
                $output["total"] = $output_total;
                $output["quota"] = $output_quota;
                
                posaljiRezAjax(STATUS_SUCCESS, "podaci su dostupni", $output);
                
            } // end if(empty($msgErr))
            else{
                $msgErr = "trenutno nije dostupno";
                posaljiRezAjax(2918,$msgErr);
            }
            
            break;
        
        case "GetHelpFile_trasa":
            $filepath=ROOT."model/primer_uzduzni_profil.txt";            
            
            error_reporting(E_ALL & ~E_NOTICE); // iskljuci E_NOTICE poruke da ih ne bi user video
            if(is_readable($filepath)){
                // ocitava fajl i automatski salje nazad klijentu
                readfile($filepath);
            }
            else{
                posaljiRezAjax("2920 trenutno nedostupno");
            }
            error_reporting(E_ALL & E_NOTICE); // iskljuci E_NOTICE poruke da ih ne bi user video
                        
            break;
            
        case "GetHelpFile_objekti":
            $filepath=ROOT."model/primer_objekti_na_trasi.txt";
            
            error_reporting(E_ALL & ~E_NOTICE); // iskljuci E_NOTICE poruke da ih ne bi user video
            if(is_readable($filepath)){
                // ocitava fajl i automatski salje nazad klijentu
                readfile($filepath);
            }
            else{
                posaljiRezAjax("2920 trenutno nedostupno");
            }
            error_reporting(E_ALL & E_NOTICE); // iskljuci E_NOTICE poruke da ih ne bi user video
            
            
            break;
            
        default:
            f_debug(2919,"invalid action: ".$action);
            break;
    }
} else {
    f_debug(2917,"no action");
}

?>