 <?php
echo "bbbbbbbbbbbbbb";
require_once('tcpdf/tcpdf_import.php');

// create new PDF document
//@param PDF_PAGE_ORIENTATION (string) page orientation. Possible values are (case insensitive):<ul><li>P or Portrait (default)</li><li>L or Landscape</li><li>'' (empty string) for automatic orientation</li></ul>
//@param PDF_PAGE_UNIT (string) User measure unit. Possible values are:<ul><li>pt: point</li><li>mm: millimeter (default)</li><li>cm: centimeter</li><li>in: inch</li></ul><br />A point equals 1/72 of inch, that is to say about 0.35 mm (an inch being 2.54 cm). This is a very common unit in typography; font sizes are expressed in that unit.
//@param PDF_PAGE_SIZE (mixed) The format used for pages. It can be either: one of the string values specified at getPageSizeFromFormat() {} or an array of parameters specified at setPageFormat().
//@param $unicode (boolean) TRUE means that the input text is unicode (default = true)
//@param $encoding (string) Charset encoding (used only when converting back html entities); default is UTF-8.
//@param $diskcache (boolean) DEPRECATED FEATURE
//@param $pdfa (boolean) If TRUE set the document to PDF/A mode.
const PDF_ORIENTATION = 'L'; 	// p/l orjentacija stranice
// mada moram oped u addPage da zadam orjentaciju sa "L"
const PDF_PAGE_UNIT = "mm"; // sirina i visina
const PDF_PAGE_SIZE = array(550, 250); // sirina i visina --- ali orjentacija zavisi od prvog parametra
$pdf = new TCPDF(PDF_ORIENTATION, PDF_PAGE_UNIT, PDF_PAGE_SIZE , true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 012');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//-------------------------------------------------------------------
/*
@param mixed :zoom
The zoom to use. It can be one of the following string values or a number indicating the zooming factor to use.
fullpage: displays the entire page on screen
fullwidth: uses maximum width of window
real: uses real size (equivalent to 100% zoom)
default: uses viewer default mode

The page layout. Possible values are:
SinglePage Display one page at a time
OneColumn Display the pages in one column
TwoColumnLeft Display the pages in two columns, with odd-numbered pages on the left
TwoColumnRight Display the pages in two columns, with odd-numbered pages on the right
TwoPageLeft (PDF 1.5) Display the pages two at a time, with odd-numbered pages on the left
TwoPageRight (PDF 1.5) Display the pages two at a time, with odd-numbered pages on the right

A name object specifying how the document should be displayed when opened:
UseNone Neither document outline nor thumbnail images visible
UseOutlines Document outline visible
UseThumbs Thumbnail images visible
FullScreen Full-screen mode, with no menu bar, window controls, or any other window visible
UseOC (PDF 1.5) Optional content group panel visible
UseAttachments (PDF 1.6) Attachments panel visible
*/
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// disable header and footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 009', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set font
$pdf->SetFont('helvetica', '', 10);
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins --- ovo mi ne traba
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks --- false -> treba mi samo jedna strana
$pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);

// set image scale factor -> skaliranje slika
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
// ---------------------------------------------------------
// ---------------------------------------------------------
// set array for viewer preferences
$preferences = array(
    'HideToolbar' => false,
    'HideMenubar' => false,
    'HideWindowUI' => false,
    'FitWindow' => true,
    'CenterWindow' => true,
    'DisplayDocTitle' => true,
    'NonFullScreenPageMode' => 'UseNone', // UseNone, UseOutlines, UseThumbs, UseOC
    'ViewArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
    'ViewClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
    'PrintArea' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
    'PrintClip' => 'CropBox', // CropBox, BleedBox, TrimBox, ArtBox
    'PrintScaling' => 'AppDefault', // None, AppDefault
    'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
    'PickTrayByPDFSize' => true,
    'PrintPageRange' => array(1,1,2,3),
    'NumCopies' => 2
);

// set pdf viewer preferences
$pdf->setViewerPreferences($preferences);

// ---------------------------------------------------------
// ---------------------------------------------------------
// ---------------------------------------------------------

// add a page
$pdf->AddPage();


$style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 100, 'color' => array(255, 0, 0));
$style2 = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 0, 0));
$style3 = array('width' => 1, 'cap' => 'round', 'join' => 'round', 'dash' => '2,10', 'color' => array(255, 0, 0));
$style4 = array('L' => 0,
                'T' => array('width' => 0.25, 'cap' => 'butt', 'join' => 'miter', 'dash' => '20,10', 'phase' => 10, 'color' => array(100, 100, 255)),
                'R' => array('width' => 0.50, 'cap' => 'round', 'join' => 'miter', 'dash' => 0, 'color' => array(50, 50, 127)),
                'B' => array('width' => 0.75, 'cap' => 'square', 'join' => 'miter', 'dash' => '30,10,5,10'));
$style5 = array('width' => 0.25, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 64, 128));
$style6 = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,10', 'color' => array(0, 128, 0));
$style7 = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 128, 0));


//okvir
$style_okvir = array('width' => 0.5, 'cap' => 'round', 'join' => 'round', 'dash' => 0, 'color' => array(255, 0, 0));
$marginaL  = 20;
$marginaR=$marginaT=$marginaB = 5;
$sirina = $pdf->getPageWidth() - $marginaL - $marginaR;
$visina = $pdf->getPageHeight(0) - $marginaT - $marginaB;
$pdf->Rect($marginaL, $marginaR, $sirina, $visina , 'C', $style_okvir);

$stilLinije = array('width' => 2.5, 'cap' => 'round', 'join' => 'round', 'dash' => '0', 'phase' => 0, 'color' => array(255, 0, 0));

// Line
$pdf->Text(5, 4, 'Line examples');
$pdf->Line(5, 10, 80, 30, $stilLinije);
$pdf->Line(5, 10, 5, 30, $stilLinije);
$pdf->Line(5, 10, 80, 10, $stilLinije);

// Rect
$pdf->Text(100, 4, 'Rectangle examples');
$pdf->Rect(100, 10, 40, 20, 'DF', $style4, array(220, 220, 200));
$pdf->Rect(145, 10, 40, 20, 'D', array('all' => $style3));


// Polygonal Line
$pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 0, 0)));
$pdf->Text(5, 149, 'PolyLinePolyLinePolyLine');
$pdf->PolyLine(array(80,165,90,160,100,165,110,160,120,165,130,160,140,165), 'D', array(), array());

// -----------------------------------------------------------------

// Clean any content of the output buffer
ob_end_clean();

//Close and output PDF document
$pdf->Output('example_012.pdf', 'I');
?>