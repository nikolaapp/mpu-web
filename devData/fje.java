private static double nadjiTemeLancanice(int i, float parametarLancanice,
                    float[] nizStacionaza_STUB_X, float[] nizKota_STUBiUZE_Y){
        /** !@# 01.03.2018-3 */
        // i - index temena u zateznom polju koji nam treba
        double[] nizRaspona = konvertovanje_stacionaza_u_raspone(nizStacionaza_STUB_X);

        /******
         ovo je nedovoljno precizno - ne poklapa se kraj lancanice sa visinom drugog stuba:
         nizTemenaLanc_rel[i] = nizStubova_X[i] + nizRaspona[i]/2 - parametarLancanice*h[i+1]/nizRaspona[i];


         ovo je ok - u skladu sa Alen Hatibovi� !@# vidi pdf u assets_no_build:
         "ODRE�IVANJE JEDNA�INA VODA I UGIBA NA OSNOVU ZADANOG PARAMETRA LAN�ANICE"
         racuna odstojanje temena lancanice od pocetno stuba
         */
            return nizStacionaza_STUB_X[i] + nizRaspona[i] / 2
                    - parametarLancanice * arcSinH((nizKota_STUBiUZE_Y[i + 1] - nizKota_STUBiUZE_Y[i])
                            / (2 * parametarLancanice * Math.sinh(nizRaspona[i] / (2 * parametarLancanice))
                    )
            );

}

private float[] tackeLancanice(float parametarLancanice){
        // racuna sve Y tacke lancanice (jedan niz za sve raspone)

        // ovo ispod je zbog kvaliteta crteza - povecavamo broj tacaka za koje racunamo Y vrednosti
        final int brojMedjuTacaka = 10; // ne menjaj!!!
        int brojTacaka = (Math.round( nizStacionaza_STUB_X[nizStacionaza_STUB_X.length-1] -
                 nizStacionaza_STUB_X[0])  ); // preciznije sa Math.round

        nizStacionaza_lancanica_X = new float[brojTacaka*brojMedjuTacaka];
        float [] nizKota_lancanica_Y = new float[brojTacaka*brojMedjuTacaka];
        // if (BuildConfig.DEBUG)Log.e("TAG17","brojTacaka= "+ brojTacaka);

        int index_x_koor;
        int indexStuba;
        int indexStuba_prethodnaIteracija=-1; // da bi se indeksi razlikovali u prvoj iteraciji
        float teme_lanc_X=0, kotaVesanjaUze_Y, y_dodatak=0;
        //double[] nizTemenaLancanica = nizTemenaLancanica(parametarLancanice,nizStacionaza_STUB_X,nizKota_STUBiUZE_Y);
        for (int i = 0; i <  brojTacaka; i++) {
            for (int j = 0; j < brojMedjuTacaka; j++) {
                // if (BuildConfig.DEBUG)Log.e("TAG17","i= "+ i +" j= "+ j  );

                index_x_koor = i*brojMedjuTacaka + j; //  povecavano broj stacionaza lancanice na ukupno brojTacaka*brojMedjuTacaka
                float dodatak = (i+j/(float)brojMedjuTacaka); //    j/10 vraca int -> ne bi imao decimalnu vrednost

                // kako ide petlja tako se krecemo od pocetnog stuba, tacku po tacku
                nizStacionaza_lancanica_X[index_x_koor] = nizStacionaza_STUB_X[0] + dodatak;

                // ovo je mo�no
                indexStuba = ProveraSigVisina_Fragment.indexSusednog_ManjegClanaNiza(nizStacionaza_lancanica_X[index_x_koor], nizStacionaza_STUB_X);

                // neke velicine se ne menjaju za tacke koje su u istom rasponu
                if (indexStuba_prethodnaIteracija != indexStuba) {

                    teme_lanc_X = (float) nadjiTemeLancanice(indexStuba, parametarLancanice,
                                            nizStacionaza_STUB_X,nizKota_STUBiUZE_Y );

                    kotaVesanjaUze_Y = nizKota_STUBiUZE_Y[indexStuba];

                    y_dodatak = kotaVesanjaUze_Y -
                            lancanica_vredY_u_tackiX(nizStacionaza_STUB_X[indexStuba] - teme_lanc_X, parametarLancanice);
                }

                nizKota_lancanica_Y[index_x_koor] = y_dodatak +
                        lancanica_vredY_u_tackiX( nizStacionaza_lancanica_X[index_x_koor] - teme_lanc_X, parametarLancanice );

                indexStuba_prethodnaIteracija = indexStuba;
              }

        }
        // if (BuildConfig.DEBUG)Log.e("TAG17","nizStacionaza_lancanica_X.length= "+ nizStacionaza_lancanica_X.length);
        // if (BuildConfig.DEBUG)Log.e("TAG17","nizStacionaza_lancanica_X "+ Arrays.toString(nizStacionaza_lancanica_X));

        return nizKota_lancanica_Y;
    }

	

public static float lancanica_vredY_u_tackiX(float x, float p) {
        // x je rastojanje od temena lancanice
        // p = parametar;
        //teme lancanice je u [0,p]             --- p * Math.cosh(x/p)
        // teme lancanice je u koord. pocetku   --- p * Math.cosh(x/p) - p
        return p * (float) Math.cosh(x / p) - p;
    }

	
	
public static float arcSinH(double x) {
        return (float) Math.log(x + Math.sqrt(x * x + 1.0));
    }
	
	
	private void pripremiParametreZaLancanicu(){

        nizRaspona = konvertovanje_stacionaza_u_raspone(nizStacionaza_STUB_X);
        nizCosFi = ProveraSigVisina_Fragment.izracunajCosFi(nizRaspona, nizKota_STUBiUZE_Y);

        double idealniRaspon = IdealniRaspon(nizRaspona,nizCosFi);
        double idealniCosFi = IdealniCosFi(nizRaspona,nizCosFi);

        // za za Wycisk
        JednacinaStanja js_5 = new JednacinaStanja(mTipUzeta, sigma0, idealniRaspon,
                idealniCosFi, koefDodOpt, -5);

        // za prikaz koef i naprezanja, i za pozivanje SacuvajGrafik
        js = new JednacinaStanja(mTipUzeta, sigma0, idealniRaspon,
                idealniCosFi, koefDodOpt, temperatura);

        double specTezina, dodTezina, rezTezina;
        specTezina = mTipUzeta.getSpecTezina();
        dodTezina = DodatnoOptSnegLed(koefDodOpt, mTipUzeta.getPrecnik(), mTipUzeta.getPresek());
        rezTezina =specTezina+dodTezina;

        // za Wycisk
        parametarLancanice_5 = (float) (js_5.getSigmaNOVO() / rezTezina);

        if(temperatura==-5)
            parametarLancanice = parametarLancanice_5;
        else
            parametarLancanice = (float) (js.getSigmaNOVO() / specTezina);

        float[] niz = ProveraSigVisina_Fragment. izracunajVisinskeRazlike(nizKota_STUBiUZE_Y);

        if (StubnoMesto.getNizStubnimMesta() == null ||
                getBool_potrebanUpdate())
            kreirajNizStubnimMesta(parametarLancanice, parametarLancanice_5);
            setBool_potrebanUpdate(false);
    }
