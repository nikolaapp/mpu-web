package nsp.mpu._1_jedn_stanja_act;

import android.support.v4.app.Fragment;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.pomocne_klase.ActivityZaJedanFragment;
import nsp.mpu.pomocne_klase.AnimacijeKlasa;

@Obfuscate
public class JedStanjaActivity extends ActivityZaJedanFragment {

    @Override
    protected Fragment KreirajFragment(){
        return new JedStanjaFragment();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AnimacijeKlasa.ParAnimacijaInOut parAnim = new AnimacijeKlasa.ParAnimacijaInOut();
        overridePendingTransition(parAnim.in, parAnim.out);
    }
}
