package nsp.mpu._1_jedn_stanja_act;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu._0MainActivity;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.JednacinaStanja;

import static nsp.mpu.database.PODACI_ListaTipovaUzadi.getListaUzadi;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animirajTekst_running;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;
import static nsp.mpu.pomocne_klase.StaticPodaci.temperaturaProv_default;
import static nsp.mpu.pomocne_klase.StaticPodaci.temperaturaProv_max;
import static nsp.mpu.pomocne_klase.StaticPodaci.temperaturaProv_min;
import static nsp.mpu.pomocne_klase.StaticPodaci.tipskaDodOpt;

@Obfuscate
public class JedStanjaFragment extends Fragment{

    protected PODACI_TipUzeta mTU;
    private NumberPicker mTipUzeta;
    private EditText mMaxNaprezanje;
    private EditText mRaspon;
    private EditText mCosFi;
    private NumberPicker mDodOpt;
    private NumberPicker mT1;

    private Button mRezNovoNaprez;
    private Button mUgib;
    private Button mRezKoefA;
    private Button mRezKoefB;


    private double sigma0, koefDodOpt,  raspon, cosFi;
    private int temp1;


    @Override
    public void onCreate(Bundle b){
        super.onCreate(b);

        /////pocetne vrednosti
        sigma0 = 9;
        raspon =100;
        cosFi = 1;
        temp1= temperaturaProv_default;
     }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        if (_0MainActivity.mDaLiJeTablet)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        View v = li.inflate(R.layout.fragment_jed_stanja, vg, false);
        setHasOptionsMenu(false);

        final String [] nizOznakaUzadi = getListaUzadi(getActivity()).getNizNazivUzadi();

        mTipUzeta = (NumberPicker) v.findViewById(R.id.num_picker_uze);
        mTipUzeta.setDisplayedValues(nizOznakaUzadi);
        mTipUzeta.setMinValue(0);
        mTipUzeta.setMaxValue(nizOznakaUzadi.length-1);
        mTipUzeta.setWrapSelectorWheel(true);
        mTipUzeta.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mTipUzeta.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                 if (BuildConfig.DEBUG)Log.e("TAG","newVal= " + newVal);
                mTU= getListaUzadi(getActivity()).getTipUzetaIzListe(nizOznakaUzadi[newVal]);
                 if (BuildConfig.DEBUG)Log.e("TAG","tipUzeta= " + mTU.getOznakaUzeta());
            }
        });
        mTipUzeta.setValue(getListaUzadi(getActivity()).pozicijaUzetaUListi("Al/Ce-70/12"));
        mTU=getListaUzadi(getActivity()).getTipUzetaIzListe(nizOznakaUzadi[mTipUzeta.getValue()]);
        
        mMaxNaprezanje = (EditText) v.findViewById(R.id.dugme_f1_odaberi_naprezanje);
        mMaxNaprezanje.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                double pomocnaVar =0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<0.001||pomocnaVar>999){
                    sigma0=0;
                    prikaziToast(getContext(),"Unesi naprezanje od 0.001 do 99");
                } else
                    sigma0=pomocnaVar;
                 if (BuildConfig.DEBUG)Log.e("TAG","sigma0= " + sigma0);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });

        mRaspon = (EditText) v.findViewById(R.id.dugme_f1_unesi_raspon);
        mRaspon.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                 if (BuildConfig.DEBUG)Log.e("TAG","PRE PARSE raspon= " + raspon);
                double pomocnaVar =0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<1||pomocnaVar>999){
                    raspon=0;
                    prikaziToast(getContext(),"Unesi raspon od 1 do 1000 metara");
                } else
                    raspon=pomocnaVar;
                 if (BuildConfig.DEBUG)Log.e("TAG","raspon= " + raspon);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });

        mCosFi = (EditText) v.findViewById(R.id.dugme_f1_unesi_cosFi);
        mCosFi.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                 if (BuildConfig.DEBUG)Log.e("TAG","PRE PARSE cosFi= " + cs);

                double pomocnaVar = 1;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar>1){
                    cosFi=1;
                    prikaziToast(getContext(),"Unesi cosFi vece od 0 do max 1");
                } else if (pomocnaVar==0)
                    cosFi=1;
                else
                    cosFi=pomocnaVar;

                 if (BuildConfig.DEBUG)Log.e("TAG","cosFi= " + cosFi);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });

        mT1 = (NumberPicker) v.findViewById(R.id.num_picker_temp);
        mT1.setMinValue(0);
        mT1.setMaxValue(temperaturaProv_max - temperaturaProv_min);
        mT1.setWrapSelectorWheel(true);
        mT1.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mT1.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int i) {
                if (i == Math.abs(temperaturaProv_min))
                    return "0";
                else
                    return String.valueOf(i + temperaturaProv_min);
            }
        });
        mT1.setValue(temp1+Math.abs(temperaturaProv_min));
        mT1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                temp1=newVal+ temperaturaProv_min;
                 if (BuildConfig.DEBUG)Log.e("TAGtemp","temp1= " + temp1);
            }
        });



        mDodOpt = (NumberPicker) v.findViewById(R.id.num_picker_odo);
        mDodOpt.setDisplayedValues( tipskaDodOpt );
        mDodOpt.setMinValue(0);
        mDodOpt.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mDodOpt.setMaxValue(tipskaDodOpt.length-1);
        mDodOpt.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                 if (BuildConfig.DEBUG)Log.e("TAG","newVal= " + newVal);
                koefDodOpt =Double.valueOf(tipskaDodOpt[newVal]);
                 if (BuildConfig.DEBUG)Log.e("TAG","koefDodOpt= " + koefDodOpt);
            }
        });
        koefDodOpt=1;

        TextView mRezultati = (TextView) v.findViewById(R.id.dugme_f1_rezultati);
        mRezultati.setText("Rešenje (σ\u00B3 + A\u22C5σ\u00B2 = Β):");


        mUgib = (Button) v.findViewById(R.id.dugme_f2_ugib);
        mUgib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prikaziRezultate();
                prikaziToast(getContext(),"Ugib u cm");
            }
        });

        mRezKoefA = (Button) v.findViewById(R.id.dugme_f1_koefA);
        mRezKoefA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prikaziRezultate();
                prikaziToast(getContext(),"Koeficijent A");
            }
        });

        mRezKoefB = (Button) v.findViewById(R.id.dugme_f1_koefB);
        mRezKoefB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prikaziRezultate();
                prikaziToast(getContext(),"Koeficijent B");
            }
        });

        mRezNovoNaprez = (Button) v.findViewById(R.id.dugme_f1_novo_naprezanje);
        mRezNovoNaprez.setText("σ [daN/mm²]");
        mRezNovoNaprez.setAllCaps(false);
        mRezNovoNaprez.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prikaziRezultate();
                prikaziToast(getContext(),"NOVO naprezanje");
            }
        });

        animirajTekst_running(getContext(),(TextView) v.findViewById(R.id.text_stepeni),"Al/Ce");

        if (BuildConfig.IS_DEMO) {
            final String s = "Nije moguće menjati u ovoj verziji aplikacije";
            (v.findViewById(R.id.text_xodo)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast toast = Toast.makeText(getContext(),s,Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP,0,200);
                    toast.show();
                }
            });
            mDodOpt.setValue(1);
            koefDodOpt = 1.6;
            mDodOpt.setEnabled(false);
        }




        return v;
    }

    private void prikaziRezultate(){
/*
        if (sigma0==0 || sigma0>100)
            prikaziToast(getContext(),"Unesi naprezanje izmedju 0 i 100");
        else if (cosFi==0 || cosFi>1)
            prikaziToast(getContext(),"Unesi cosFi izmedju 0 do ukljucivo 1");
        else if (raspon<1 || raspon>1000)
            prikaziToast(getContext(),"Unesi raspon od 1 do 1000");
        else {
*/
        mCosFi.setText(String.valueOf( cosFi));
        mRaspon.setText(String.valueOf( raspon));
        mMaxNaprezanje.setText(String.valueOf( sigma0));

             if (BuildConfig.DEBUG)Log.e("TAG", "JednacinaStanja pozvan konstruktor" + mTU.getOznakaUzeta() + " " + sigma0 + " " + raspon + " " + cosFi + " " + koefDodOpt + " " + temp1);
            JednacinaStanja js = new JednacinaStanja(mTU, sigma0, raspon, cosFi,
                    koefDodOpt, temp1);
            mRezNovoNaprez.setText(String.format("%.5f", js.getSigmaNOVO()));
            mUgib.setText(String.format("%.0f", js.getUgib())+" cm");
            mRezKoefA.setText("A="+String.format("%.2f", js.getKoefA()));
            mRezKoefB.setText(String.format("B="+"%.2f", js.getKoefB()));
            // if (BuildConfig.DEBUG)Log.e("TAG1", "getUgib" + String.format("%.2f", js.getUgib()));


        /*
        double rastojanje = 50;
        double ugibB = JednacinaStanja.UgibUcm(raspon,cosFi,
                js.getSigmaNOVO(),mTU.getSpecTezina(),rastojanje);
        double d = JednacinaStanja.naprezanjePrekoUgiba( raspon,  cosFi,
                ugibB, mTU.getSpecTezina(),  rastojanje,  sigma0);
        mRazno.setText(String.format("%.5f",d));
         if (BuildConfig.DEBUG)Log.e("TAG", "PODACI_razno -raspon" + raspon + " js.getSigmaNOVO()" + js.getSigmaNOVO() + " rastojanje" + rastojanje + " ugibB" + ugibB + " sigma0" + sigma0);
        */
    }

}
