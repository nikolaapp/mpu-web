package nsp.mpu._2_tabela_ugiba;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;

@Obfuscate
public class CrtanjeLancanice_Fragment extends Fragment {
    private float parametar;
    private float[] nizRaspona_float;
    private float[] nizCosFi_float;
    private FrameLayout mFL;

    public static final String ARGTag1 = "ARGTag1";
    public static final String ARGTag2 = "ARGTag2";
    public static final String ARGTag3 = "ARGTag3";

    public static CrtanjeLancanice_Fragment noviFrag(double parametar, double[] nizRaspona, double[] nizCosFi){
        Bundle args = new Bundle();
        args.putDouble(ARGTag1, parametar);
        args.putDoubleArray(ARGTag2, nizRaspona);
        args.putDoubleArray(ARGTag3, nizCosFi);

        CrtanjeLancanice_Fragment f = new CrtanjeLancanice_Fragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        parametar = (float) getArguments().getDouble("ARGTag1");
        double[] nizRaspona = getArguments().getDoubleArray("ARGTag2");
        double[] nizCosFi = getArguments().getDoubleArray("ARGTag3");

        nizRaspona_float = new float[nizRaspona.length];
        nizCosFi_float = new float[nizCosFi.length];
        for (int i = 0; i < nizRaspona_float.length ; i++) {
            nizRaspona_float[i] = (float) nizRaspona[i];
            nizCosFi_float[i] = (float) nizCosFi[i];
        }
    }

    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.activity_prikaz_fragmenta, vg, false);

        CrtanjeLancanice_canvas mLancanica =
                new CrtanjeLancanice_canvas(getActivity(),parametar, nizRaspona_float, nizCosFi_float );

        mFL = (FrameLayout) v.findViewById(R.id.activity_prikaz_fragmenta);
        mFL.addView(mLancanica);

        return v;
    }

}
