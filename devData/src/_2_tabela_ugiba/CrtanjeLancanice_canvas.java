package nsp.mpu._2_tabela_ugiba;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;

@Obfuscate
public class CrtanjeLancanice_canvas extends View {

    private Paint paint_funkcija = new Paint();
    private Paint paint_stub = new Paint();
    Canvas mCanvas;
    float parametar;
    private float[] nizRaspona;
    private float[] nizCosFi;
    float[] nizStubova_X;
    float[] nizStubova_Y;
    float[] nizTemenaLanc;
    float[] h;  // razlika u visinama vesanja uzeta u rasponu
    private final static int margina = 10; // margina za x osu da bi se lepo prikazali krajnji stubovi i lancanica

    private static float visinaStuba;
    private static float razmeraX;
    private static float razmeraY;


    public CrtanjeLancanice_canvas(Context context, float parametar, float[] nizRaspona, float[] nizCosFi) {
        super(context);
        // if (BuildConfig.DEBUG)Log.e("TAG4", "CrtanjeFunkcija------------------");
        paint_funkcija.setColor(Color.RED);
        paint_funkcija.setStrokeWidth(3);
        paint_stub.setColor(Color.BLACK);
        paint_stub.setStrokeWidth(5);

        // 6.0.2.2///////////////povecavamo parametarLancanice da se lancanica ne bi vukla po zemlji
        if (nizRaspona.length==1&&parametar<100)
            this.parametar=777;
        else
            this.parametar = parametar;


         if (BuildConfig.DEBUG)Log.e("TAG4","this.parametarLancanice= "+this.parametar);

        this.nizRaspona = nizRaspona;
        this.nizCosFi = nizCosFi;

        nizTemenaLanc = new float[nizRaspona.length];
        nizStubova_X = new float[nizRaspona.length + 1];
        nizStubova_Y = new float[nizRaspona.length + 1];
                    h = new float[nizRaspona.length + 1];
    }

    @Override
    public void onDraw(Canvas canvas) {

        // if (BuildConfig.DEBUG)Log.e("TAG4","onDraw");
        mCanvas = canvas;
        visinaStuba = canvas.getHeight() / 2;

        float zateznoPolje = 0;
        for (double d : nizRaspona)
            zateznoPolje += d;
        razmeraX = (canvas.getWidth() - 2 * margina) / zateznoPolje;

        nizRaspona = prilagodiRazmeri(nizRaspona, razmeraX);

        nizStubova_X[0] = margina / 2;
        for (int i = 1; i < nizStubova_X.length; i++) {
            nizStubova_X[i] = nizStubova_X[i - 1] + nizRaspona[i - 1];
        }

        h[0] = 0;
        nizStubova_Y[0] = 0;
        for (int i = 1; i < nizStubova_Y.length; i++) {
            h[i] = (float) Math.sqrt(naKradrat(nizRaspona[i - 1]) * (1 / naKradrat(nizCosFi[i-1]) - 1));

            //6.0.3/////////
            if (nizCosFi[i-1]<0) h[i] *= -1;

            nizStubova_Y[i] = nizStubova_Y[i - 1] + h[i];
        }

        //6.0.3////////////////////////
        float ukupnaVisina=0;
        float koordY_max = 0;
        float koordY_min = 0;
        float sumaVisina = 0;
        for (int i=0; i<h.length; i++) {
            sumaVisina += h[i];
            if (koordY_max < sumaVisina)   koordY_max = sumaVisina;
            if (koordY_min > sumaVisina)   koordY_min = sumaVisina;
        }
        ukupnaVisina = visinaStuba + koordY_max + Math.abs(koordY_min);
        for (int i=0; i<nizStubova_Y.length; i++)
            nizStubova_Y[i] += Math.abs(koordY_min);
        //////////////////////////////

        razmeraY = (canvas.getHeight() - margina) / ukupnaVisina; // ne mora 2*margina jer je donji deo uvek ok

        nizStubova_Y = prilagodiRazmeri(nizStubova_Y, razmeraY);
        h = prilagodiRazmeri(h, razmeraY);
        visinaStuba = prilagodiRazmeri(visinaStuba, razmeraY);

//         if (BuildConfig.DEBUG)Log.e("TAG4","nizStubova_Y= "+Arrays.toString(nizStubova_Y));
//         if (BuildConfig.DEBUG)Log.e("TAG4","visina poslednjeg stuba= "+(canvas.getHeight()-nizStubova_Y[1]-visinaStuba));
        // if (BuildConfig.DEBUG)Log.e("TAG4","visinaStuba= "+visinaStuba);
        // if (BuildConfig.DEBUG)Log.e("TAG4","h= "+Arrays.toString(h));

        for (int i = 0; i < nizTemenaLanc.length; i++)
            // ovo je nedovoljno precizno - ne poklapa se kraj lancanice sa visinom drugog stuba
            /*
            nizTemenaLanc_rel[i] = nizStubova_X[i] + nizRaspona[i]/2 - parametarLancanice*h[i+1]/nizRaspona[i];
            */

            // ovo je ok - u skladu sa Alen Hatibović:
            // "ODREĐIVANJE JEDNAČINA VODA I UGIBA NA OSNOVU ZADANOG PARAMETRA LANČANICE"
            nizTemenaLanc[i] = nizStubova_X[i] + nizRaspona[i] / 2 - parametar * arcSinH(h[i + 1] / (2 * parametar * Math.sinh(nizRaspona[i] / (2 * parametar))));


        if (true) {
            for (int i = 0; i < nizStubova_X.length; i++) {
                stub_crtanje_tacaka((nizStubova_X[i]), nizStubova_Y[i]);
                if (i == 0)
                    continue;
                lancanica_crtanje_tacaka((nizStubova_X[i - 1]), (nizStubova_X[i]), (nizStubova_Y[i - 1] + visinaStuba), (nizTemenaLanc[i - 1]), parametar);
            }
        }
    }

    private static float prilagodiRazmeri(float var, float razmera) {
        return var * razmera;
    }

    private static float[] prilagodiRazmeri(float[] niz, float razmera) {
        for (int i = 0; i < niz.length; i++)
            niz[i] *= razmera;
        return niz;
    }

    private static double naKradrat(double x) {return x * x;}

    private static float lancanica_vredY_u_tackiX(float x, float parametar) {
        float p = parametar;
        //teme lancanice je u [0,p]             --- p * Math.cosh(x/p)
        // teme lancanice je u koord. pocetku   --- p * Math.cosh(x/p) - p
        return p * (float) Math.cosh(x / p) - p;
    }

    private void lancanica_crtanje_tacaka(float x_start, float x_stop, float y_kotaStuba,
                                          float teme_lanc, float parametar) {

        float x_screen;   // x koordinate za prikaz na ekranu [0, canvas.getWidth()]
        float y_screen;   // y koordinate za prikaz na ekranu [0, canvas.getHeight()]
        float x;          // x koordinate za proracun lancanice koja ima teme u tacki: (int teme_lanc)
        float y;          // y = f(x)

        // podizananje lancanice do najvise tacke levog stuba u rasponu (po y-osi)
        float y_dodatak = y_kotaStuba - lancanica_vredY_u_tackiX(x_start - teme_lanc, parametar);

        for (x_screen = x_start; x_screen <= x_stop; x_screen++) {
            x = x_screen - teme_lanc;

            y = lancanica_vredY_u_tackiX(x, parametar) + y_dodatak;

            if (y < 0)
                y_screen = mCanvas.getHeight();
            else
                y_screen = (mCanvas.getHeight() - y); // jer je tacka (0,0) u gornjem levom uglu ekrana
            if (x_screen==x_stop/2) {
                // if (BuildConfig.DEBUG)Log.e("TAG4","parametarLancanice=" + parametarLancanice +" x_screen=" + x_screen + " y_screen="+y_screen +" y="+y+" y_dodatak="+y_dodatak);
            }
            mCanvas.drawPoint(x_screen, y_screen, paint_funkcija);
        }
    }

    private void stub_crtanje_tacaka(float x_pozicija_stuba, float y_pozicija_stuba) {
        float x_start = x_pozicija_stuba;
        float y_start = mCanvas.getHeight() - y_pozicija_stuba;
        float x_stop = x_start;
        float y_stop = y_start - visinaStuba; // minus jer se pixeli broje od gore na dole

        mCanvas.drawLine(x_start, y_start, x_stop, y_stop, paint_stub);
    }

    static float arcSinH(double x) {
        return (float) Math.log(x + Math.sqrt(x * x + 1.0));
    }

}



/*
    static float arcCosH(float x) {
        return Math.log(x + Math.sqrt(x * x - 1.0));
    }

    private static float pocetakLancanice(float yStuba, float parametarLancanice){
        //uskladi_pocetak_lancanice_sa_visinom_stuba
        float p=parametarLancanice;
        return p * arcCosH( (yStuba+p) / p);
    }

    private static float parabola_vredY_u_tackiX(float x, float parametarLancanice){
        float p=parametarLancanice;
        //teme lancanice je u [0,p]
        //return (x*x)/(2*p) + (x*x*x*x)/(24*p*p*p) + p;


        // teme lancanice je u koord. pocetku
        return (x*x)/(2*p) + (x*x*x*x)/(24*p*p*p);
    }
*/
