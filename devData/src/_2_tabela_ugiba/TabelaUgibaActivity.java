package nsp.mpu._2_tabela_ugiba;

import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;
import nsp.mpu._0MainActivity;
import nsp.mpu.pomocne_klase.ActivityZaJedanFragment;
import nsp.mpu.pomocne_klase.AnimacijeKlasa;

@Obfuscate
public class TabelaUgibaActivity extends ActivityZaJedanFragment {

    @Override
    protected Fragment KreirajFragment(){
        if ( _0MainActivity.mDaLiJeTablet == true) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            this.setTitle("nazad");
        }
        return new TabelaUgibaUnos_Fragment();
    }

////////////////////Prikaz dva fragmenta 4.1.0//////////////////////
    @Override
    protected int postaviNovLayout(){
        return R.layout.activity_reference_jedan_frag_ili_tri_ugib;
    }
//////////////////////////////////////////////////////////////////////
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AnimacijeKlasa.ParAnimacijaInOut parAnim = new AnimacijeKlasa.ParAnimacijaInOut();
        overridePendingTransition(parAnim.in, parAnim.out);
    }

}
