package nsp.mpu._2_tabela_ugiba;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.NumberPicker;

import java.util.Arrays;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.JednacinaStanja;

import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu.database.PODACI_ListaTipovaUzadi.getListaUzadi;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animacijaZaRecycleView;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_IN;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_OUT;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;
import static nsp.mpu.pomocne_klase.StaticPodaci.temperaturaProv_max;
import static nsp.mpu.pomocne_klase.StaticPodaci.temperaturaProv_min;
import static nsp.mpu.pomocne_klase.StaticPodaci.tipskaDodOpt;

@Obfuscate
public class TabelaUgibaUnos_Fragment extends Fragment{

    private PODACI_TipUzeta mTU;
    private NumberPicker mTipUzeta;
    private EditText mMaxNaprezanje;
    private NumberPicker mDodOpt;
    private EditText mNovaTemperatura;
    private CheckBox mCheckBox;
    private EditText mBrojRaspona;
    private RecyclerView mRecyclerView;
    private AdapterRaspona  mAdapterRaspona;

    private Button mPrikaziUgibe;

    public final static int[] pocetniNizTemp = {-5,0,40,80};
    public final static int[] pocetniNizTemp_Tablet = {-20,-5,0,10,40,60,80};

    private double sigma0=9, koefDodOpt=1;
    private int[] nizTemp;
    private int brojRaspona;
    private int brojRasponaAdapter;
    private double[] nizRaspona;
    private double[] nizCosFi;
    private final double sigma0_default=9;
    private final double raspon_default=100;
    private final double cosFi_default=1;


    @Override
    public void onSaveInstanceState(Bundle outState) {
        // mora ovo jer se pri promeni orjentacije ekrana
        // ponovo kreira fragment
        outState.putDoubleArray("nizRaspona", nizRaspona);
        outState.putDoubleArray("nizCosFi", nizCosFi);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle b){
        super.onCreate(b);
        // kopira elemente, a ne samo reference kao nizTemp = pocetniNizTemp
        // na taj nacin se pri promeni nizTemp ne menja pocetniNizTemp
        if (mDaLiJeTablet)
            nizTemp = pocetniNizTemp_Tablet.clone();
        else
            nizTemp = pocetniNizTemp.clone();

        if (b!=null){
            nizRaspona = b.getDoubleArray("nizRaspona");
            brojRaspona=nizRaspona.length;
            brojRasponaAdapter = brojRaspona;
            nizCosFi = b.getDoubleArray("nizCosFi");
        }
        else {
            nizRaspona = new double[]{raspon_default};
            nizCosFi = new double[]{cosFi_default};
            brojRaspona=1;
            brojRasponaAdapter=1;
        }
    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.fragment_tabele_ugiba, vg, false);
        if ( ! mDaLiJeTablet)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        final String [] nizOznakaUzadi = getListaUzadi(getActivity()).getNizNazivUzadi();

        mTipUzeta = (NumberPicker) v.findViewById(R.id.num_picker_uze);
        mTipUzeta.setDisplayedValues(nizOznakaUzadi);
        mTipUzeta.setMinValue(0);
        mTipUzeta.setMaxValue(nizOznakaUzadi.length-1);
        mTipUzeta.setWrapSelectorWheel(true);
        mTipUzeta.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mTipUzeta.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                 if (BuildConfig.DEBUG)Log.e("TAG","newVal= " + newVal);
                mTU= getListaUzadi(getActivity()).getTipUzetaIzListe(nizOznakaUzadi[newVal]);
                 if (BuildConfig.DEBUG)Log.e("TAG","tipUzeta= " + mTU.getOznakaUzeta());
            }
        });
        mTipUzeta.setValue(getListaUzadi(getActivity()).pozicijaUzetaUListi("Al/Ce-70/12"));
        mTU=getListaUzadi(getActivity()).getTipUzetaIzListe(nizOznakaUzadi[mTipUzeta.getValue()]);

        mCheckBox = (CheckBox) v.findViewById(R.id.dugme_f2_checkBox);
        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton cb, boolean b){
                if (mNovaTemperatura.isShown()) {
                    mNovaTemperatura.setVisibility(View.GONE);
                    mNovaTemperatura.getText().clear();
                    nizTemp = mDaLiJeTablet?
                            pocetniNizTemp_Tablet.clone():
                            pocetniNizTemp.clone();
                }
                else{
                    mNovaTemperatura.setVisibility(View.VISIBLE);
                    mNovaTemperatura.requestFocus();
                }
            }
        });

        mNovaTemperatura = (EditText) v.findViewById(R.id.dugme_f2_odaberi_temp);
        mNovaTemperatura.setVisibility(View.GONE);
        mNovaTemperatura.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                 if (BuildConfig.DEBUG)Log.e("TAG","PRE PARSE temp= " + cs);
                int pomocnaVar =-100;
                try {
                    pomocnaVar = Integer.parseInt(cs.toString());
                } catch (NumberFormatException e) {}


                if (pomocnaVar<temperaturaProv_min||pomocnaVar>temperaturaProv_max){
                    nizTemp = mDaLiJeTablet?
                            pocetniNizTemp_Tablet.clone():
                            pocetniNizTemp.clone();
                    if (mNovaTemperatura.getVisibility() == View.VISIBLE)
                        prikaziToast(getContext(),"Unesi temperatutu od "+temperaturaProv_min+" do "+temperaturaProv_max);
                } else {
                    if (pomocnaVar==-5) pomocnaVar = JednacinaStanja.TEMP_MINUS5_BEZ_LEDA;
                    nizTemp[nizTemp.length - 1] = pomocnaVar;
                }
                 if (BuildConfig.DEBUG)Log.e("TAG","temp= " + nizTemp[nizTemp.length-1]);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });

        mMaxNaprezanje = (EditText) v.findViewById(R.id.dugme_f2_odaberi_naprezanje);
        mMaxNaprezanje.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                double pomocnaVar =0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<0.01||pomocnaVar>99){
                    sigma0=sigma0_default;
                    prikaziToast(getContext(),"Unesi naprazenje od 0.01 do 99");
                } else
                    sigma0=pomocnaVar;
                 if (BuildConfig.DEBUG)Log.e("TAG","sigma0= " + sigma0);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });

        mBrojRaspona = (EditText) v.findViewById(R.id.dugme_f2_broj_raspona);
        mBrojRaspona.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                 if (BuildConfig.DEBUG)Log.e("TAG","PRE PARSE raspon= " + brojRaspona);
                int pomocnaVar =1;
                try {
                    pomocnaVar = Integer.parseInt(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar>30){
                    brojRaspona = 1;
                    prikaziToast(getContext(),"Unesi broj raspona u polju: od 1 do 30");
                } else
                    brojRaspona = pomocnaVar;
                    startAdaptera();
                 if (BuildConfig.DEBUG)Log.e("TAG","raspon= " + brojRaspona);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });

        mDodOpt = (NumberPicker) v.findViewById(R.id.num_picker_odo);
        mDodOpt.setDisplayedValues( tipskaDodOpt );
        mDodOpt.setMinValue(0);
        mDodOpt.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mDodOpt.setMaxValue(tipskaDodOpt.length-1);
        mDodOpt.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                 if (BuildConfig.DEBUG)Log.e("TAG","newVal= " + newVal);
                koefDodOpt =Double.valueOf(tipskaDodOpt[newVal]);
                 if (BuildConfig.DEBUG)Log.e("TAG","koefDodOpt= " + koefDodOpt);
            }
        });

        mPrikaziUgibe = (Button) v.findViewById(R.id.dugme_f2_prikazi_ugibe);
        mPrikaziUgibe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment noviFrag_tabela = TabeleUgibaPrikaz_Fragment.noviFragmentTabUgbPrikaz
                        (mTU.getOznakaUzeta(), sigma0, nizRaspona, nizCosFi,koefDodOpt, nizTemp);

                FragmentManager fm = getActivity().getSupportFragmentManager();
                if (mDaLiJeTablet)
                    fm.beginTransaction()
                            .setCustomAnimations(
                                    odaberiAnimaciju_fragment_IN(),
                                    odaberiAnimaciju_fragment_OUT(),
                                    odaberiAnimaciju_fragment_IN(),
                                    odaberiAnimaciju_fragment_OUT()
                            )
                        .replace(R.id.activity_prikaz_drugog_fragmenta, noviFrag_tabela)
                        .commit();
                else
                    fm.beginTransaction()
                            .setCustomAnimations(
                                    odaberiAnimaciju_fragment_IN(),
                                    odaberiAnimaciju_fragment_OUT(),
                                    odaberiAnimaciju_fragment_IN(),
                                    odaberiAnimaciju_fragment_OUT()
                            )
                        .replace(R.id.activity_prikaz_fragmenta, noviFrag_tabela)
                        .addToBackStack(null) // back dugme vraca na zamenjeni frag
                        .commit();
            }
        });

        mRecyclerView = (RecyclerView) v.findViewById(R.id.f2_recycle_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        startAdaptera();

        if (BuildConfig.IS_DEMO) {
            mDodOpt.setValue(1);
            koefDodOpt = 1.6;
            mDodOpt.setEnabled(false);
        }

        return v;
    }

    private class HolderRaspona extends RecyclerView.ViewHolder     {
        //HOLDER od ADAPTERA dobija layout izgled i podatke iz modela, koje vezuje

        private EditText mEditTextRaspon;
        private EditText mEditTextCosFi;

        private HolderRaspona(View v) {
            super(v);

            mEditTextRaspon = (EditText) v.findViewById(R.id.f2_rasponi_raspon);
            mEditTextRaspon.addTextChangedListener(new TextWatcher() { // mora ovako
                @Override
                public void onTextChanged(CharSequence cs, int s, int b, int c) {
                     if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE holder Raspon= " + cs);

                    double pomocnaVar =0;
                    try {
                        pomocnaVar = Double.parseDouble(cs.toString());
                    } catch (NumberFormatException e) {}

                    if (pomocnaVar<1||pomocnaVar>999){
                        nizRaspona[getAdapterPosition()]=raspon_default;
                        if(mEditTextRaspon.didTouchFocusSelect())
                            prikaziToast(getContext(),"Unesi raspon od 1 do 1000 metara");
                    } else
                        nizRaspona[getAdapterPosition()]=pomocnaVar;

                     if (BuildConfig.DEBUG)Log.e("TAG", "holder Raspon= " + nizRaspona[getAdapterPosition()]+" index "+getAdapterPosition());
                }
                @Override
                public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
                @Override
                public void afterTextChanged(Editable e) {}
            });

            mEditTextCosFi = (EditText) v.findViewById(R.id.f2_rasponi_cosFi);
            mEditTextCosFi.addTextChangedListener(new TextWatcher() { // mora ovako
                @Override
                public void onTextChanged(CharSequence cs, int s, int b, int c) {
                     if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE holder CosFi= " + cs);

                    double pomocnaVar = 1;
                    try {
                        pomocnaVar = Double.parseDouble(cs.toString());
                    } catch (NumberFormatException e) {}

                    if (pomocnaVar<=-1 || pomocnaVar>1){
                        nizCosFi[getAdapterPosition()]=cosFi_default;
                        if(mEditTextCosFi.didTouchFocusSelect())
                            prikaziToast(getContext(),"Unesi cosFi u intervalu (-1,1]");
                    } else if (pomocnaVar==0)
                        nizCosFi[getAdapterPosition()]=sigma0_default;
                    else
                        nizCosFi[getAdapterPosition()]=pomocnaVar;

                     if (BuildConfig.DEBUG)Log.e("TAG", "holder CosFi= " + nizCosFi[getAdapterPosition()]+" index "+getAdapterPosition());
                }
                @Override
                public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
                @Override
                public void afterTextChanged(Editable e) {}
            });
        }
    }

    private class AdapterRaspona extends RecyclerView.Adapter<HolderRaspona>{

        public AdapterRaspona(int brojRasponaNovo){
            nizRaspona = Arrays.copyOf(nizRaspona, brojRasponaNovo);
            nizCosFi = Arrays.copyOf(nizCosFi, brojRasponaNovo);

            if (brojRasponaNovo > brojRasponaAdapter) {
                Arrays.fill(nizRaspona, brojRasponaAdapter, brojRasponaNovo, raspon_default);
                Arrays.fill(nizCosFi, brojRasponaAdapter, brojRasponaNovo, cosFi_default);
            }

//            ovo se na kraju izvrsava!
            brojRasponaAdapter = brojRasponaNovo;
        }

        @Override
        //kreira i salje izgled fragmenta ka holderu
        public HolderRaspona onCreateViewHolder(ViewGroup vg, int i){
            LayoutInflater li = LayoutInflater.from(getActivity());
            View v = li.inflate(R.layout.fragment_recycler_view_po_dva_elementa,vg,false);
            return new HolderRaspona(v);
        }

        @Override
        //salje podatke iz modela ka holderu
        public void onBindViewHolder(HolderRaspona holder, int pozicija){
            if (nizRaspona[pozicija]==raspon_default &&  nizCosFi[pozicija]==cosFi_default){
                holder.mEditTextRaspon.setHint("raspon["+(pozicija+1)+"]");
                holder.mEditTextCosFi.setHint("cosFi["+(pozicija+1)+"]");
            } else {
                holder.mEditTextRaspon.setText(String.valueOf(nizRaspona[pozicija]));
                holder.mEditTextCosFi.setText(String.valueOf(nizCosFi[pozicija]));
            }

            if (pozicija+1 == brojRasponaAdapter){
                holder.mEditTextCosFi.setImeOptions(EditorInfo.IME_ACTION_DONE);
            }
        }
        @Override
        //Velicina liste sa kojom treba adapter da radi
        public int getItemCount(){
            return brojRasponaAdapter;
        }
    }

    private void startAdaptera(){
        mAdapterRaspona = new AdapterRaspona(brojRaspona);
        mRecyclerView.setAdapter(mAdapterRaspona);
        animacijaZaRecycleView(mRecyclerView, true);
    }


}
