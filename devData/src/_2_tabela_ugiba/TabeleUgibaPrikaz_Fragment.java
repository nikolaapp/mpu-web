package nsp.mpu._2_tabela_ugiba;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_ListaTipovaUzadi;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.JednacinaStanja;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animacijaZaRecycleView;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_IN;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_OUT;
import static nsp.mpu.pomocne_klase.JednacinaStanja.DodatnoOptSnegLed;
import static nsp.mpu.pomocne_klase.JednacinaStanja.TEMP_MINUS5_BEZ_LEDA;

//import nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Activity;

@Obfuscate
public class TabeleUgibaPrikaz_Fragment extends Fragment
        implements View.OnClickListener{
    private PODACI_TipUzeta mTU;
    private double sigma0, koefDodOpt;
    private int brojRaspona;
    private double[] nizRaspona;
    private double[] nizCosFi;
    private double[] nizCosFi_znakovi;
    private int[] nizTemp;
    private double nizNaprezanja[], nizKoefA[], nizKoefB[];
    private double mKriticniRaspon, mIdealniCosFi, mIdealniRaspon;

    private TextView mPrikazAkr;

    /////////////// 6.0.0
    private TextView[] nizTextViewTemp, nizTextViewNaprezanja, nizTextViewKoefA, nizTextViewKoefB;

    public static final String ARGTag1 = "ARGTag1";
    public static final String ARGTag2 = "ARGTag2";
    public static final String ARGTag3 = "ARGTag3";
    public static final String ARGTag4 = "ARGTag4";
    public static final String ARGTag5 = "ARGTag5";
    public static final String ARGTag6 = "ARGTag6";

    private RecyclerView mRecyclerView;
    private AdapterPrikazaRaspona mAdapterPrikazaRaspona;

    public String kopiranjeTabeleUgiba;
    public String kopiranjeNaslova;
    public String kopiranjeTemperature;
    public String kopiranjeUgiba;
    public String kopiranjeNaprezanja;
    public String kopiranjeKoef_A;
    public String kopiranjeKoef_B;

    public static boolean nemaTreciFrag = false;

    public static TabeleUgibaPrikaz_Fragment noviFragmentTabUgbPrikaz
            (String tipUzeta, double sigma0, double[] nizRaspona, double[] nizCosFi,
             double koefDodOpt, int[] nizTemp){
        Bundle args = new Bundle();
        args.putString(ARGTag1, tipUzeta);
        args.putDouble(ARGTag2, sigma0);
        args.putDoubleArray(ARGTag3, nizRaspona);
        args.putDoubleArray(ARGTag4, nizCosFi);
        args.putDouble(ARGTag5, koefDodOpt);
        args.putIntArray(ARGTag6, nizTemp);

        TabeleUgibaPrikaz_Fragment newFrag = new TabeleUgibaPrikaz_Fragment();
        newFrag.setArguments(args);
        return newFrag;
    }

    @Override
    public void onCreate(Bundle b){
        super.onCreate(b);
        setHasOptionsMenu(false);

//        if (getActivity() instanceof ProveraSigVisina_Activity)
//            ((ProveraSigVisina_Activity) getActivity()).setActionBarTitle("Tabela ugiba");

        String s = getArguments().getString("ARGTag1");
        mTU= PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).getTipUzetaIzListe(s);
        sigma0 = getArguments().getDouble("ARGTag2");
        nizRaspona = getArguments().getDoubleArray("ARGTag3");
        nizCosFi_znakovi = getArguments().getDoubleArray("ARGTag4"); // koristimo za crtanje
        koefDodOpt = getArguments().getDouble("ARGTag5");
        nizTemp = getArguments().getIntArray("ARGTag6");
        brojRaspona= nizRaspona.length;

        //6.0.3/////////////////////
        // uzimamo pozitivnu vrednost zbog mogucih gresaka u kosnijim proracunima
        nizCosFi = nizCosFi_znakovi.clone();
        for (int i = 0; i < nizCosFi_znakovi.length; i++) {
            if (nizCosFi_znakovi[i]<0)
                nizCosFi[i] *= -1;
        }
        // if (BuildConfig.DEBUG)Log.e("TAG4","nizCosFi_znakovi " + Arrays.toString(nizCosFi_znakovi));


        mIdealniRaspon = JednacinaStanja.IdealniRaspon(nizRaspona,nizCosFi);
        mIdealniCosFi = JednacinaStanja.IdealniCosFi(nizRaspona,nizCosFi);

        JednacinaStanja js = new JednacinaStanja(mTU, sigma0, mIdealniRaspon,
                mIdealniCosFi, koefDodOpt, -5);
        mKriticniRaspon = js.getAkr();

        nizNaprezanja = new double[nizTemp.length];
        nizKoefA = new double[nizTemp.length];
        nizKoefB = new double[nizTemp.length];

        for (int i = 0; i < nizTemp.length; i++) {
            js = new JednacinaStanja(mTU, sigma0, mIdealniRaspon,
                    mIdealniCosFi, koefDodOpt, nizTemp[i]);
            nizNaprezanja[i] = js.getSigmaNOVO();
            nizKoefA[i] = js.getKoefA();
            nizKoefB[i] = js.getKoefB();
        }

    }

    public static String centralnoPoravananje(String text, int len){
        String out = String.format("%"+len+"s%s%"+len+"s", "",text,"");
        float mid = (out.length()/2);
        float start = mid - (len/2);
        float end = start + len;
        return out.substring((int)start, (int)end);
    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b){
        View v;
        if (mDaLiJeTablet)
            v = li.inflate(R.layout.fragment_prikaz_tabele_ugiba_tablet, vg, false);
        else {
            v = li.inflate(R.layout.fragment_prikaz_tabele_ugiba, vg, false);
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        if (getActivity().findViewById(R.id.activity_prikaz_treceg_fragmenta) == null) {
            // znaci da ga je pozvala activity koja nema mesto za treci frag (SigVisine)
            //prikaziToast(getContext(),"nema treci frag");
            nemaTreciFrag = true;
        }//else prikaziToast(getContext(),"IMA treci frag");

        mPrikazAkr = (TextView) v.findViewById(R.id.f21_prikaza_akr);
        String s1 = String.format("%.2f", mKriticniRaspon);
        String s2 = String.format("%.2f", mIdealniRaspon);
        String s3 = String.format("%.5f", mIdealniCosFi);
        String s4 = String.format("%.3f", 1000*mTU.getSpecTezina());

        double dodOpter1 = DodatnoOptSnegLed(koefDodOpt, mTU.getPrecnik(), mTU.getPresek());
        String s5 = String.format("%.3f", 1000*(dodOpter1+mTU.getSpecTezina()));

        mPrikazAkr.setText("Akr="+s1+"   Aid="+s2+"   cosFiId="+s3+
                "\ntez.uzeta ="+s4+"   rez.tezina="+s5);

        kopiranjeNaslova = String.valueOf(mPrikazAkr.getText());

        ispisiTemperature(v);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.f21_prikazUgiba_recycle_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ispisiNaprezanja(v);
        ispisiKoef_A(v);
        ispisiKoef_B(v);

        kopiranjeTemperature =  String.format("\n%-17s","temp/rasponi");
        kopiranjeNaprezanja =   String.format("\n%-18s","naprezanja");
        kopiranjeKoef_A =       String.format("\n%-20s","koef.A");
        kopiranjeKoef_B =       String.format("\n%-20s","koef.B");

        String s;
        for (int i =0; i<nizTemp.length; i++){
            if (nizTemp[i]==-5)
                s=-5+"_led";
            else if (nizTemp[i]==TEMP_MINUS5_BEZ_LEDA)
                s=-5+"";
            else
                s=nizTemp[i]+"";

            // %s za string
            kopiranjeTemperature += String.format("%7s    ",s);

            kopiranjeNaprezanja +=  String.format("%11.2f",nizNaprezanja[i]);
            kopiranjeKoef_A +=      String.format("%11.2f",nizKoefA[i]);
            kopiranjeKoef_B +=      String.format("%10.2f",nizKoefB[i]);
        }


        startAdaptera();

        Button KopiranjeButton = (Button) v.findViewById(R.id.f21_kopiranje);
        KopiranjeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kopiranjeTabeleUgiba = kopiranjeNaslova;
                kopiranjeTabeleUgiba += kopiranjeTemperature;
                kopiranjeTabeleUgiba += kopiranjeUgiba;
                kopiranjeTabeleUgiba += kopiranjeNaprezanja;
                kopiranjeTabeleUgiba += kopiranjeKoef_A;
                kopiranjeTabeleUgiba += kopiranjeKoef_B;

                //6.0.2.1 onemogucava da se pri ponovnom kreiranju fagmenta dupliraju redovi za raspon/ugib
                kopiranjeUgiba=null;

                Fragment noviFrag = TabeleUgibaPrikaz_Fragment_kopiranje.
                        noviFragmentTabUgbKopiranje(kopiranjeTabeleUgiba);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                if (mDaLiJeTablet)
                    fm.beginTransaction()
                            .setCustomAnimations(
                                     odaberiAnimaciju_fragment_IN(),                             odaberiAnimaciju_fragment_OUT(),                             odaberiAnimaciju_fragment_IN(),                             odaberiAnimaciju_fragment_OUT()
                            )
                            .replace(R.id.activity_prikaz_drugog_fragmenta, noviFrag)
                            .addToBackStack(null) // back dugme vraca na zamenjeni frag
                            .commit();
                else
                    fm.beginTransaction()
                            .setCustomAnimations(
                                     odaberiAnimaciju_fragment_IN(),
                                    odaberiAnimaciju_fragment_OUT(),
                                    odaberiAnimaciju_fragment_IN(),
                                    odaberiAnimaciju_fragment_OUT()
                            )
                            .replace(R.id.activity_prikaz_fragmenta, noviFrag)
                            .addToBackStack(null) // back dugme vraca na zamenjeni frag
                            .commit();
            }
        });

        //pocetno kreiranje frag za crtanje lancanice (za -5stepeni)
        //samo za tablet && ako ima treci frag
        if (mDaLiJeTablet && !nemaTreciFrag){
            for (int i = 0; i <nizTemp.length ; i++) {
                if (nizTemp[i]==-5) {
                    nizTextViewTemp[i].performClick();
                    break;
                }
                if (i==nizTemp.length-1) // ako nema -5 u nizu temperatura
                    nizTextViewTemp[0].performClick();
            }
        }

        return v;
    }

    @Override
    public void onClick(View view) {
        int index=0;
        Drawable pozadina = mPrikazAkr.getBackground();

        for (int i = 0; i < nizTextViewTemp.length; i++) {
            nizTextViewTemp[i].setBackground(pozadina);//resetuj boju
            if (view.getId() == nizTextViewTemp[i].getId())
                index = i;
        }

        double rezTezina = mTU.getSpecTezina();
        if(nizTemp[index]==-5)
            rezTezina += DodatnoOptSnegLed(koefDodOpt, mTU.getPrecnik(), mTU.getPresek() );

        double parametarLancanice = nizNaprezanja[index]/rezTezina;

        Fragment noviFrag_crtanje = CrtanjeLancanice_Fragment.noviFrag
                (parametarLancanice, nizRaspona, nizCosFi_znakovi); //////6.0.3 nizCosFi_znakovi
        FragmentManager fm = getActivity().getSupportFragmentManager();

        if (mDaLiJeTablet) {
            int pom = R.id.activity_prikaz_treceg_fragmenta;

            // znaci da ga se pozvala activity koja nema mesto za treci frag (SigVisine)
            if (nemaTreciFrag)
                pom = R.id.activity_prikaz_drugog_fragmenta;

            fm.beginTransaction()
                    .setCustomAnimations(
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT(),
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(pom, noviFrag_crtanje)
                    .commit();
        }else
            fm.beginTransaction()
                    .setCustomAnimations(
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT(),
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_fragmenta, noviFrag_crtanje)
                    .addToBackStack(null) // back dugme vraca na zamenjeni frag
                    .commit();

        view.setBackgroundColor(Color.RED);

        String pom;
        if (nizTemp[index]==-5)
            pom=nizTemp[index]+"_led";
        else if (nizTemp[index]==TEMP_MINUS5_BEZ_LEDA)
            pom="-5";
        else
            pom= String.valueOf(nizTemp[index]);

        StaticPodaci.prikaziToast(getActivity(),
                "Prikazana je lancanica za temperaturu od "+pom+" stepeni");

    }
    @Obfuscate//import
    private class HolderPrikazaRaspona extends RecyclerView.ViewHolder {
        //HOLDER od ADAPTERA dobija layout izgled i podatke iz modela, koje vezuje

        private TextView mTextRaspon;
        private TextView[] nizTextViewUgiba;

        private HolderPrikazaRaspona(View v) {
            super(v);

            mTextRaspon = (TextView) v.findViewById(R.id.f22_raspon);

            nizTextViewUgiba = new TextView[nizTemp.length];
            nizTextViewUgiba[0] = (TextView) v.findViewById(R.id.f22_0_ugib);
            nizTextViewUgiba[1] = (TextView) v.findViewById(R.id.f22_1_ugib);
            nizTextViewUgiba[2] = (TextView) v.findViewById(R.id.f22_2_ugib);
            nizTextViewUgiba[3] = (TextView) v.findViewById(R.id.f22_3_ugib);
            if (mDaLiJeTablet) {
                nizTextViewUgiba[4] = (TextView) v.findViewById(R.id.f22_4_ugib);
                nizTextViewUgiba[5] = (TextView) v.findViewById(R.id.f22_5_ugib);
                nizTextViewUgiba[6] = (TextView) v.findViewById(R.id.f22_6_ugib);
            }

        }
    }
    @Obfuscate//import
    private class AdapterPrikazaRaspona extends RecyclerView.Adapter<HolderPrikazaRaspona>{

        int brojRaspona_adapter;

        public AdapterPrikazaRaspona(int brojRaspona){
            brojRaspona_adapter = brojRaspona;
        }

        @Override
        //kreira i salje izgled fragmenta ka holderu
        public HolderPrikazaRaspona onCreateViewHolder(ViewGroup vg, int i){
            LayoutInflater li = LayoutInflater.from(getActivity());
            View v;
            if (mDaLiJeTablet)
                v = li.inflate(R.layout.fragment_prikaz_tabele_ugiba_pojedinacno_tablet,vg,false);
            else
                v = li.inflate(R.layout.fragment_prikaz_tabele_ugiba_pojedinacno,vg,false);
            return new HolderPrikazaRaspona(v);
        }

        @Override
        //salje podatke iz modela ka holderu
        public void onBindViewHolder(HolderPrikazaRaspona holder, int p){

            double raspon = nizRaspona[p];
            double cosFi = nizCosFi[p];
            double tezinaUzeta = mTU.getSpecTezina();
            double dodOpter = DodatnoOptSnegLed(koefDodOpt, mTU.getPrecnik(), mTU.getPresek());


            holder.mTextRaspon.setText(String.format("%3.2f ",raspon));

            if (kopiranjeUgiba==null) // da ne bi zadrzao null string pri prvoj += naredbi
                kopiranjeUgiba =  String.format("\n%13s       ", raspon);
            else
                kopiranjeUgiba += String.format("\n%13s       ", raspon);

            double nizUgiba[] = new double[nizTemp.length];
            for (int i = 0; i < nizUgiba.length; i++) {
                if (nizTemp[i]==-5)
                    nizUgiba[i] = JednacinaStanja.UgibUcm(raspon, cosFi, nizNaprezanja[i], tezinaUzeta+dodOpter);
                else
                    nizUgiba[i] = JednacinaStanja.UgibUcm(raspon, cosFi, nizNaprezanja[i], tezinaUzeta);

                kopiranjeUgiba += String.format("%11.0f ", nizUgiba[i]);
                 if (BuildConfig.DEBUG)Log.e("TAG1","nizUgiba[i]= " +nizUgiba[i]);
                 if (BuildConfig.DEBUG)Log.e("TAG1","kopiranjeUgiba " +String.valueOf(kopiranjeUgiba));
            }

            for (int i = 0; i < holder.nizTextViewUgiba.length; i++) {
                holder.nizTextViewUgiba[i].setText(String.format(Locale.getDefault(),"%.0f", nizUgiba[i]));
            }

        }
        @Override
        //Velicina liste sa kojom treba adapter da radi
        public int getItemCount(){
            return brojRaspona_adapter;
        }
    }

    private void startAdaptera(){
        mAdapterPrikazaRaspona = new AdapterPrikazaRaspona(brojRaspona);
        mRecyclerView.setAdapter(mAdapterPrikazaRaspona);
        animacijaZaRecycleView(mRecyclerView, true);
    }

    private void ispisiTemperature(View v) {

        nizTextViewTemp = new TextView[nizTemp.length];
        nizTextViewTemp[0] = (TextView) v.findViewById(R.id.f21_0_temp);
        nizTextViewTemp[1] = (TextView) v.findViewById(R.id.f21_1_temp);
        nizTextViewTemp[2] = (TextView) v.findViewById(R.id.f21_2_temp);
        nizTextViewTemp[3] = (TextView) v.findViewById(R.id.f21_3_temp);
        if (mDaLiJeTablet) {
            nizTextViewTemp[4] = (TextView) v.findViewById(R.id.f21_4_temp);
            nizTextViewTemp[5] = (TextView) v.findViewById(R.id.f21_5_temp);
            nizTextViewTemp[6] = (TextView) v.findViewById(R.id.f21_6_temp);
        }

        for (int i = 0; i < nizTextViewTemp.length; i++) {
            String pom;
            if (nizTemp[i]==-5)
                pom = nizTemp[i]+"_led";
            else if (nizTemp[i]== TEMP_MINUS5_BEZ_LEDA)
                pom = "-5";
            else
                pom = String.format("%d", nizTemp[i]);

            nizTextViewTemp[i].setText(pom);
            nizTextViewTemp[i].setTypeface(null, Typeface.ITALIC);
            if (!mDaLiJeTablet || !nemaTreciFrag) // ako nema treci frag(sigVisine) - nema potreba za crtanjem ugiba
                nizTextViewTemp[i].setOnClickListener(this);
        }

    }

    private void ispisiNaprezanja(View v){
        nizTextViewNaprezanja = new TextView[nizTemp.length];
        nizTextViewNaprezanja[0] = (TextView) v.findViewById(R.id.f21_0_naprezanje);
        nizTextViewNaprezanja[1] = (TextView) v.findViewById(R.id.f21_1_naprezanje);
        nizTextViewNaprezanja[2] = (TextView) v.findViewById(R.id.f21_2_naprezanje);
        nizTextViewNaprezanja[3] = (TextView) v.findViewById(R.id.f21_3_naprezanje);
        if (mDaLiJeTablet) {
            nizTextViewNaprezanja[4] = (TextView) v.findViewById(R.id.f21_4_naprezanje);
            nizTextViewNaprezanja[5] = (TextView) v.findViewById(R.id.f21_5_naprezanje);
            nizTextViewNaprezanja[6] = (TextView) v.findViewById(R.id.f21_6_naprezanje);
        }

        for (int i = 0; i < nizTextViewNaprezanja.length; i++) {
            nizTextViewNaprezanja[i].setText(String.format("%.2f", nizNaprezanja[i]));
            nizTextViewNaprezanja[i].setTypeface(null, Typeface.ITALIC);
        }
    }

    private void ispisiKoef_A(View v) {
        nizTextViewKoefA = new TextView[nizTemp.length];
        nizTextViewKoefA[0] = (TextView) v.findViewById(R.id.f21_0_koefA);
        nizTextViewKoefA[1] = (TextView) v.findViewById(R.id.f21_1_koefA);
        nizTextViewKoefA[2] = (TextView) v.findViewById(R.id.f21_2_koefA);
        nizTextViewKoefA[3] = (TextView) v.findViewById(R.id.f21_3_koefA);
        if (mDaLiJeTablet) {
            nizTextViewKoefA[4] = (TextView) v.findViewById(R.id.f21_4_koefA);
            nizTextViewKoefA[5] = (TextView) v.findViewById(R.id.f21_5_koefA);
            nizTextViewKoefA[6] = (TextView) v.findViewById(R.id.f21_6_koefA);
        }

        for (int i = 0; i < nizTextViewKoefA.length; i++) {
            nizTextViewKoefA[i].setText(String.format("%.2f", nizKoefA[i]));
            nizTextViewKoefA[i].setTypeface(null, Typeface.ITALIC);
        }
    }

    private void ispisiKoef_B(View v) {
        nizTextViewKoefB = new TextView[nizTemp.length];
        nizTextViewKoefB[0] = (TextView) v.findViewById(R.id.f21_0_koefB);
        nizTextViewKoefB[1] = (TextView) v.findViewById(R.id.f21_1_koefB);
        nizTextViewKoefB[2] = (TextView) v.findViewById(R.id.f21_2_koefB);
        nizTextViewKoefB[3] = (TextView) v.findViewById(R.id.f21_3_koefB);
        if (mDaLiJeTablet) {
            nizTextViewKoefB[4] = (TextView) v.findViewById(R.id.f21_4_koefB);
            nizTextViewKoefB[5] = (TextView) v.findViewById(R.id.f21_5_koefB);
            nizTextViewKoefB[6] = (TextView) v.findViewById(R.id.f21_6_koefB);
        }

        for (int i = 0; i < nizTextViewKoefB.length; i++) {
            nizTextViewKoefB[i].setText(String.format("%.2f", nizKoefB[i]));
            nizTextViewKoefB[i].setTypeface(null, Typeface.ITALIC);
        }
    }

}
