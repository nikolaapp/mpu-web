package nsp.mpu._2_tabela_ugiba;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;

//import nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Activity;

@Obfuscate
public class TabeleUgibaPrikaz_Fragment_kopiranje extends Fragment {
    public static final String ARGTag1 = "ARGTag1";
    private String mKopiranjeTabeleUgiba;

    public static TabeleUgibaPrikaz_Fragment_kopiranje
    noviFragmentTabUgbKopiranje(String s) {
        Bundle args = new Bundle();
        args.putString(ARGTag1, s);

        TabeleUgibaPrikaz_Fragment_kopiranje newFrag = new TabeleUgibaPrikaz_Fragment_kopiranje();
        newFrag.setArguments(args);
        return newFrag;
    }


    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setHasOptionsMenu(false);

//        if (getActivity() instanceof ProveraSigVisina_Activity)
//            ((ProveraSigVisina_Activity) getActivity()).setActionBarTitle("Tabela ugiba");

        mKopiranjeTabeleUgiba = getArguments().getString("ARGTag1");
    }

    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v;
        /*
        if (mDaLiJeTablet)
            v = li.inflate(R.layout.fragment_prikaz_tabele_ugiba_tablet, vg, false);
        else {
            v = li.inflate(R.layout.fragment_prikaz_tabele_ugiba, vg, false);
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        */
        v = li.inflate(R.layout.fragment_prikaz_tabele_ugiba_kopiranje, vg, false);


        TextView mTextView = (TextView) v.findViewById(R.id.f23_tabela_ugiba_kopiranje);
        mTextView.setTextIsSelectable(true);
        // if (BuildConfig.DEBUG)Log.e("TAG1","mKopiranjeTabeleUgiba");

        mTextView.setText(mKopiranjeTabeleUgiba);

        return v;
    }

}