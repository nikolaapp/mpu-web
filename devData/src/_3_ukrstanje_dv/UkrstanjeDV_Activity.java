package nsp.mpu._3_ukrstanje_dv;

import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;

import java.util.List;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;
import nsp.mpu._0MainActivity;
import nsp.mpu.pomocne_klase.ActivityZaJedanFragment;
import nsp.mpu.pomocne_klase.AnimacijeKlasa;
import nsp.mpu.pomocne_klase.JednacinaStanja;

import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_IN;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_OUT;
import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;

@Obfuscate
public class UkrstanjeDV_Activity extends ActivityZaJedanFragment {

    public enum TipFragmenta {VN, NN}

    private static final int[] nizTemp_default = {-5,40,80};
    public static int[] nizTemp = nizTemp_default.clone();
    public static void izmeniNizTemp(int novaTemp){
        if (novaTemp == nepostoji){
            nizTemp = nizTemp_default.clone();
            return;
        }

        for (int i = 0; i < nizTemp.length; i++)
            if (nizTemp[i] == novaTemp)
                return;

        nizTemp[nizTemp.length-1] = novaTemp;
    }


    JednacinaStanja[] nizJedStanja_VN;
    public double kotaTla;
    double[] nizKotaUzadi_VN;
    double[] nizVisina_VN;
    double udaljenost_VN;
    public void ucitajParam_VN( JednacinaStanja[] nizJedStanja,
                                double[] nizKotaUzadi, double[] nizVisina, double udaljenost, double kotaTla){
        this.nizJedStanja_VN = nizJedStanja;
        this.nizKotaUzadi_VN = nizKotaUzadi;
        this.nizVisina_VN = nizVisina;
        this.udaljenost_VN = udaljenost;
        this.kotaTla = kotaTla;

        noviNNFrag();
    }
    Fragment fragNN;
    private void noviNNFrag(){
        //pri povratku sa rez fragmenta fragNN je lepo rekreiran na unete vrednosti
        //da pri sledecoj transiziji sa fragVN na fragNN ne bi ponovo kreirao fragNN sa default vred
        //onda uvodimo da pri tranziciji sa fragNN na fragVN (preko backstact) sacuva SavedState od fragNN
        //i onda taj SavedState iskoristiomo pri ponovnom kreiranju fragNN
        //fragVN se kreira samo pri pocetnom pozivu aktivnosti, jer se uvek vraca na stanje sa backstack


        if ( ! _0MainActivity.mDaLiJeTablet ) {
            if ( fragNN == null ){
                fragNN =  UkrstanjeDV_Fragment. noviFragmentUkrastanja(TipFragmenta.NN);
                // if (BuildConfig.DEBUG)Log.e("TAG40","noviNNFrag create " );
            }else {
                fragNN.setInitialSavedState(savedState);
                // if (BuildConfig.DEBUG)Log.e("TAG40", "noviNNFrag old ");
            }

            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT(),
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_fragmenta, fragNN)
                    .addToBackStack("VN")
                    .commit();

        }

        else{ // za tablet

            if(getSupportFragmentManager()
                    .findFragmentById(R.id.activity_prikaz_drugog_fragmenta)
                                                == null){
                fragNN =  UkrstanjeDV_Fragment. noviFragmentUkrastanja(TipFragmenta.NN);
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(
                                odaberiAnimaciju_fragment_IN(),
                                odaberiAnimaciju_fragment_OUT(),
                                odaberiAnimaciju_fragment_IN(),
                                odaberiAnimaciju_fragment_OUT()
                        )
                        .replace(R.id.activity_prikaz_drugog_fragmenta, fragNN)
                        .commit();
            }
            else { //fragNN je vec prikazan, prikazujemo rezultate

                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(
                                odaberiAnimaciju_fragment_IN(),
                                odaberiAnimaciju_fragment_OUT(),
                                odaberiAnimaciju_fragment_IN(),
                                odaberiAnimaciju_fragment_OUT()
                        )
                        .replace(R.id.activity_prikaz_treceg_fragmenta,
                                new UkrstanjeDV_Fragment_Rez())
                        .commit();
            }

        }

    }

    Fragment.SavedState savedState;
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            savedState = getSupportFragmentManager().saveFragmentInstanceState(getVisibleFragment());
            // if (BuildConfig.DEBUG)Log.e("TAG40","onBackPressed fragNN" );
        }
        // if (BuildConfig.DEBUG)Log.e("TAG40","onBackPressed ---" );
        AnimacijeKlasa.ParAnimacijaInOut parAnim = new AnimacijeKlasa.ParAnimacijaInOut();
        overridePendingTransition(parAnim.in, parAnim.out);
        super.onBackPressed();
    }
    private Fragment getVisibleFragment(){
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if(fragments != null){
            for(Fragment fragment : fragments){
                if(fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }

    JednacinaStanja[] nizJedStanja_NN;
    double[] nizKotaUzadi_NN;
    double[] nizVisina_NN;
    double udaljenost_NN;
    public void ucitajParam_NN( JednacinaStanja[] nizJedStanja,
                                double[] nizKotaUzadi, double[] nizVisina, double udaljenost){
        this.nizJedStanja_NN = nizJedStanja;
        this.nizKotaUzadi_NN = nizKotaUzadi;
        this.nizVisina_NN = nizVisina;
        this.udaljenost_NN = udaljenost;

        noviREzFrag();
    }
    private void noviREzFrag(){
        Fragment fragREZ =  new UkrstanjeDV_Fragment_Rez();

        if ( _0MainActivity.mDaLiJeTablet == false )
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT(),
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT()
                    )
                .replace(R.id.activity_prikaz_fragmenta, fragREZ)
                .addToBackStack("NN")
                .commit();
        else
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.activity_prikaz_treceg_fragmenta, fragREZ)
                    .commit();

    }



    @Override
    protected Fragment KreirajFragment(){
        if ( _0MainActivity.mDaLiJeTablet == true)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        return  UkrstanjeDV_Fragment.noviFragmentUkrastanja(TipFragmenta.VN);
    }
    @Override
    protected int postaviNovLayout(){
        return R.layout.activity_reference_jedan_frag_ili_tri;
    }

}