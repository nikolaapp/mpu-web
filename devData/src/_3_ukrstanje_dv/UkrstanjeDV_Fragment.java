package nsp.mpu._3_ukrstanje_dv;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.JednacinaStanja;

import static nsp.mpu.database.PODACI_ListaTipovaUzadi.getListaUzadi;
import static nsp.mpu.pomocne_klase.JednacinaStanja.TEMP_MINUS5_BEZ_LEDA;
import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;
import static nsp.mpu.pomocne_klase.StaticPodaci.tipskaDodOpt;
import static nsp.mpu.pomocne_klase.StaticPodaci.tipskaDodOpt_pozUlisti;

@Obfuscate
public class UkrstanjeDV_Fragment extends Fragment {

    private PODACI_TipUzeta mTU;
    private NumberPicker mTipUzeta;
    private EditText mMaxNaprezanje;
    private NumberPicker mDodOpt;
    private EditText mNovaTemperatura;
    private CheckBox mCheckBox_temp;

    private Button mUnesiOstaleDV;

    private double sigma0, koefDodOpt;
    private double raspon;
    private double cosFi;
    private double udaljenost;
    private double kotaT;
    private double kotaU1;
    private double kotaU2;

    private final double sigma0_def = 9;
    private final double koefDodOpt_def = 1;
    private final double raspon_def = 100;
    private final double cosFi_def = 1;
    private final double udaljenost_def = 50;
    private final double kotaT_def = 0;
    private final double kotaU1_def = 10;
    private final double kotaU2_def = 10;

    UkrstanjeDV_Activity.TipFragmenta tipFragmenta;

    String [] nizOznakaUzadi;
    @Override
    public void onCreate(Bundle b){
        super.onCreate(b);

        tipFragmenta = (UkrstanjeDV_Activity.TipFragmenta)
                            getArguments().getSerializable("TipFragmenta");

        nizOznakaUzadi = getListaUzadi(getActivity()).getNizNazivUzadi();
        mTU=getListaUzadi(getActivity()).getTipUzetaIzListe("Al/Ce-70/12");
        sigma0=sigma0_def;
        koefDodOpt=koefDodOpt_def;
        raspon=raspon_def;
        cosFi=cosFi_def ;
        udaljenost=udaljenost_def;
        kotaT=kotaT_def ;
        kotaU1=kotaU1_def;
        kotaU2=kotaU2_def;


    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.fragment_ukrstanje_dv, vg, false);

        if (b!=null){
            mTU = getListaUzadi(getActivity()).getTipUzetaIzListe(b.getString("getOznakaUzeta"));
            koefDodOpt = b.getDouble("koefDodOpt");
            // if (BuildConfig.DEBUG)Log.e("TAG40","onCreateView mTU= " + mTU.getOznakaUzeta());
            // if (BuildConfig.DEBUG)Log.e("TAG40","onCreateView koefDodOpt= " + koefDodOpt);
        }

        TextView tw = (TextView) v.findViewById(R.id.dugme_f3_tip_fragmenta);
        if (tipFragmenta == UkrstanjeDV_Activity.TipFragmenta.VN) {
            tw.setText("Parametri DV viseg napona");
            tw.setTextColor(Color.parseColor("#98c1e0")); //LtBlue
        }else if (tipFragmenta == UkrstanjeDV_Activity.TipFragmenta.NN) {
            tw.setText("Parametri DV nizeg napona");
            tw.setTextColor(Color.parseColor("#2fb675"));//LtGreen
        }else
            tw.setText("greska");

        mTipUzeta = (NumberPicker) v.findViewById(R.id.num_picker_uze);
        mTipUzeta.setDisplayedValues(nizOznakaUzadi);
        mTipUzeta.setMinValue(0);
        mTipUzeta.setMaxValue(nizOznakaUzadi.length-1);
        mTipUzeta.setWrapSelectorWheel(true);
        mTipUzeta.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mTipUzeta.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                 if (BuildConfig.DEBUG)Log.e("TAG","newVal= " + newVal);
                mTU= getListaUzadi(getActivity()).getTipUzetaIzListe(nizOznakaUzadi[newVal]);
                 if (BuildConfig.DEBUG)Log.e("TAG","tipUzeta= " + mTU.getOznakaUzeta());
            }
        });
        mTipUzeta.setValue(getListaUzadi(getActivity()).pozicijaUzetaUListi(mTU.getOznakaUzeta()));

//////////////////////////////////////////////////////////////////////
        mMaxNaprezanje = (EditText) v.findViewById(R.id.dugme_f3_odaberi_naprezanje);
        mMaxNaprezanje.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                double pomocnaVar =0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<1||pomocnaVar>99){
                    sigma0=sigma0_def;
                    prikaziToast(getContext(),"Unesi naprazenje od 1 do 99");
                } else
                    sigma0=pomocnaVar;
                 if (BuildConfig.DEBUG)Log.e("TAG","sigma0= " + sigma0);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });
//////////////////////////////////////////////////////////////////////
        mDodOpt = (NumberPicker) v.findViewById(R.id.num_picker_odo);
        mDodOpt.setDisplayedValues( tipskaDodOpt );
        mDodOpt.setMinValue(0);
        mDodOpt.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mDodOpt.setMaxValue(tipskaDodOpt.length-1);
        mDodOpt.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                 if (BuildConfig.DEBUG)Log.e("TAG","newVal= " + newVal);
                koefDodOpt =Double.valueOf(tipskaDodOpt[newVal]);
                 if (BuildConfig.DEBUG)Log.e("TAG","koefDodOpt= " + koefDodOpt);
            }
        });
        mDodOpt.setValue(tipskaDodOpt_pozUlisti(koefDodOpt));

//////////////////////////////////////////////////////////////////////        //////////////////////////////////////////////////////////////////////
        mCheckBox_temp = (CheckBox) v.findViewById(R.id.dugme_f3_checkBox_temp);
        if (tipFragmenta == UkrstanjeDV_Activity.TipFragmenta.NN  ){
            mCheckBox_temp.setVisibility(View.GONE);
        }
        mCheckBox_temp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton cb, boolean b){
                if (mNovaTemperatura.isShown()) {
                    mNovaTemperatura.setVisibility(View.GONE);
                    UkrstanjeDV_Activity.izmeniNizTemp(nepostoji);
                }
                else {
                    mNovaTemperatura.setVisibility(View.VISIBLE);
                    mNovaTemperatura.requestFocus();
                }
            }
        });
//////////////////////////////////////////////////////////////////////
        mNovaTemperatura = (EditText) v.findViewById(R.id.dugme_f3_odaberi_temp);
        mNovaTemperatura.setVisibility(View.GONE);
        mNovaTemperatura.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                 if (BuildConfig.DEBUG)Log.e("TAG","PRE PARSE temp= " + cs);
                int temperatura =nepostoji;
                try {
                    temperatura = Integer.parseInt(cs.toString());
                } catch (NumberFormatException e) {}

                if (temperatura<-40||temperatura>200){
                    if (mNovaTemperatura.getVisibility()== View.VISIBLE)
                        prikaziToast(getContext(),"Unesi temperatutu od -40 do 200");
                } else
                    UkrstanjeDV_Activity.izmeniNizTemp(temperatura);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });
//////////////////////////////////////////////////////////////////////

        EditText mEditTextRaspon;
        EditText mUdaljenost;
        EditText mKotaT;
        EditText mKotaU1;
        EditText mKotaU2;

        mEditTextRaspon = (EditText) v.findViewById(R.id.f3_rasponi_raspon);
        mEditTextRaspon.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE holder Raspon= " + cs);

                double pomocnaVar =0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<1||pomocnaVar>999){
                    raspon=raspon_def;
                    prikaziToast(getContext(),"Unesi raspon od 1 do 1000 metara");
                } else
                    raspon=pomocnaVar;

                izracunajCosFi();
                 if (BuildConfig.DEBUG)Log.e("TAG", "holder Raspon= " + raspon);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });
//////////////////////////////////////////////////////////////////////
        mEditTextCosFi = (EditText) v.findViewById(R.id.f3_rasponi_cosFi);
        mEditTextCosFi.setEnabled(false);
        /*
        // bug !@#1.1.18 - funkcija ce da upisuje cosFi ovde, ali ce textwatcher
        // da stalno ispvalja vred na 1 (dok ce edittext da lepo prikaze vred od npr 0.9954)
        mEditTextCosFi.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE holder CosFi= " + cs);

                double pomocnaVar = 1;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) { if (BuildConfig.DEBUG)Log.e("TAG", "NumberFormatException = " + pomocnaVar);}

                 if (BuildConfig.DEBUG)Log.e("TAG", "pomocnaVar= " + pomocnaVar);

                if (pomocnaVar>1){
                    cosFi=1;
                    prikaziToast(getContext(),"Unesi cosFi vece od 0 do max 1");
                } else if (pomocnaVar==0)
                    cosFi=1;
                else
                    cosFi=pomocnaVar;

                 if (BuildConfig.DEBUG)Log.e("TAG", "holder CosFi= " + cosFi);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });*/
//////////////////////////////////////////////////////////////////////
        mUdaljenost = (EditText) v.findViewById(R.id.f3_udaljenost_od_stuba);
        mUdaljenost.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                double pomocnaVar = nepostoji;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<0||pomocnaVar> raspon){
                    udaljenost=udaljenost_def;
                    prikaziToast(getContext(),"Unesi udaljenost u m: od 1 do duzine raspona");
                } else
                    udaljenost=pomocnaVar;
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });
//////////////////////////////////////////////////////////////////////
        mKotaT = (EditText) v.findViewById(R.id.f3_kota_terena);
        if (tipFragmenta == UkrstanjeDV_Activity.TipFragmenta.NN  ){
            mKotaT.setVisibility(View.GONE);
        }

        mKotaT.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                double pomocnaVar =nepostoji;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<0||pomocnaVar>2017){
                    kotaT=kotaT_def;
                    prikaziToast(getContext(),"Unesi kotu terena u m - od 0 do 2017");
                } else
                    kotaT=pomocnaVar;
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });
//////////////////////////////////////////////////////////////////////
        mKotaU1 = (EditText) v.findViewById(R.id.f3_kota_uzeta1);
        mKotaU1.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                double pomocnaVar =nepostoji;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<kotaT||pomocnaVar>2117){
                    kotaU1=kotaU1_def;
                    prikaziToast(getContext(),"Unesi kotu terena u m - izmedju kote terena i 2117");
                } else
                    kotaU1=pomocnaVar;

                izracunajCosFi();
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });
//////////////////////////////////////////////////////////////////////
        mKotaU2 = (EditText) v.findViewById(R.id.f3_kota_uzeta2);
        mKotaU2.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                double pomocnaVar =nepostoji;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<kotaT||pomocnaVar>2117){
                    kotaU2=kotaU2_def;
                        /*Toast t = new Toast(getActivity());
                        t.makeText(getActivity(),"Unesi kotu terena u m - izmedju kote terena i 2117");
                        t.show();
                                //.makeText(getActivity(),"Unesi kotu terena u m - izmedju kote terena i 2117");
                    */
                } else
                    kotaU2=pomocnaVar;

                izracunajCosFi();
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });


//////////////////////////////////////////////////////////////////////
        mUnesiOstaleDV = (Button) v.findViewById(R.id.dugme_f3_unesi_ostale_podatke);
        if (tipFragmenta == UkrstanjeDV_Activity.TipFragmenta.VN)
            mUnesiOstaleDV.setText("Sacuvaj parametre");
        else if (tipFragmenta == UkrstanjeDV_Activity.TipFragmenta.NN)
            mUnesiOstaleDV.setText("Sacuvaj parametre");
        else
            mUnesiOstaleDV.setText("greska");
        mUnesiOstaleDV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proracuni();
            }
        });

        if (BuildConfig.IS_DEMO) {
            mDodOpt.setValue(1);
            koefDodOpt = 1.6;
            mDodOpt.setEnabled(false);
        }

        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("getOznakaUzeta",mTU.getOznakaUzeta());
        outState.putDouble("koefDodOpt",koefDodOpt);

         if (BuildConfig.DEBUG)Log.e("TAG40","outState mTU= " + mTU.getOznakaUzeta());
         if (BuildConfig.DEBUG)Log.e("TAG40","outState koefDodOpt= " + koefDodOpt);
    }

    EditText mEditTextCosFi;

    private void izracunajCosFi(){
        if (raspon!=0)
            cosFi=raspon /
                    Math.sqrt( Math.pow(raspon,2)
                           + Math.pow(kotaU2-kotaU1,2));
        else cosFi=1;
        mEditTextCosFi.setText(String.format("%.5f",cosFi));
    }

    public static UkrstanjeDV_Fragment noviFragmentUkrastanja(UkrstanjeDV_Activity.TipFragmenta TipFragmenta){
        Bundle args = new Bundle();
        args.putSerializable("TipFragmenta", TipFragmenta);

        UkrstanjeDV_Fragment newFrag = (new UkrstanjeDV_Fragment());
        newFrag.setArguments(args);

        return newFrag;
    }

    private void proracuni(){

        final int[] nizTemp = ((UkrstanjeDV_Activity) getActivity()).nizTemp;

        JednacinaStanja[] nizJedStanja = new JednacinaStanja[nizTemp.length];

       double[] nizKotaUzadi = new double[nizTemp.length];
       double[] nizVisina = new double[nizTemp.length];

        for (int i=0; i<nizTemp.length;i++) {
                if (nizTemp[i] == -5 && tipFragmenta == UkrstanjeDV_Activity.TipFragmenta.NN)
                    nizJedStanja[i] = new JednacinaStanja(mTU, sigma0, raspon,
                            cosFi, koefDodOpt, TEMP_MINUS5_BEZ_LEDA);
                else
                    nizJedStanja[i] = new JednacinaStanja(mTU, sigma0, raspon,
                            cosFi, koefDodOpt, nizTemp[i]);


            double K1, K2;
            if (kotaU1>kotaU2) {
                K1=kotaU1;
                K2=kotaU2;
            } else {
                K1=kotaU2;
                K2=kotaU1;
            }
            nizKotaUzadi[i] = K1 - (K1-K2)*udaljenost/raspon - nizJedStanja[i].getUgibX(udaljenost)/100;
            if (tipFragmenta == UkrstanjeDV_Activity.TipFragmenta.NN)
                kotaT = ((UkrstanjeDV_Activity) getActivity()).kotaTla;
            nizVisina[i] = nizKotaUzadi[i] - kotaT;
        }

        if (tipFragmenta == UkrstanjeDV_Activity.TipFragmenta.VN)
            ((UkrstanjeDV_Activity) getActivity())
                    .ucitajParam_VN(nizJedStanja, nizKotaUzadi, nizVisina, udaljenost, kotaT );
        else if (tipFragmenta == UkrstanjeDV_Activity.TipFragmenta.NN)
            ((UkrstanjeDV_Activity) getActivity())
                    .ucitajParam_NN(nizJedStanja, nizKotaUzadi, nizVisina, udaljenost );

    }

}