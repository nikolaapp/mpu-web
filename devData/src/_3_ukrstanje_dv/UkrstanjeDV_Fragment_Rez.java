package nsp.mpu._3_ukrstanje_dv;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;
import nsp.mpu.pomocne_klase.JednacinaStanja;
import nsp.mpu.pomocne_klase.ViewPager_ispravanWrapHeight;

@Obfuscate
public class UkrstanjeDV_Fragment_Rez extends Fragment {

    private TextView mTRezNaslov;
    private TextView mTRez0;
    private TextView mTRez1;
    private TextView mTRez2;

    public static int[] nizTemp;
    public static JednacinaStanja[] nizJedStanja_VN;
    public static double[] nizKotaUzadi_VN;
    public static double[] nizVisina_VN;
    public static JednacinaStanja[] nizJedStanja_nn;
    public static double[] nizKotaUzadi_nn;
    public static double[] nizVisina_nn;
    public static double udaljenost_VN, udaljenost_NN;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        UkrstanjeDV_Activity act = ((UkrstanjeDV_Activity) getActivity());

        nizTemp = ((UkrstanjeDV_Activity) getActivity()).nizTemp;
        nizJedStanja_VN = act.nizJedStanja_VN;
        nizJedStanja_nn = act.nizJedStanja_NN;
        nizKotaUzadi_VN = act.nizKotaUzadi_VN;
        nizKotaUzadi_nn = act.nizKotaUzadi_NN;
        nizVisina_VN = act.nizVisina_VN;
        nizVisina_nn = act.nizVisina_NN;
        udaljenost_VN = act.udaljenost_VN;
        udaljenost_NN = act.udaljenost_NN;

    }


    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.fragment_ukrstanje_dv_rez, vg, false);

        SampleFragmentPagerAdapter pagerAdapter = new
                SampleFragmentPagerAdapter(getChildFragmentManager(),getContext());
        final ViewPager_ispravanWrapHeight viewPager = (ViewPager_ispravanWrapHeight) v.findViewById(R.id.tab_layout_viewpager);
        viewPager.setAdapter(pagerAdapter);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tab_layout_tabs);
        tabLayout.setupWithViewPager(viewPager);


        mTRezNaslov = (TextView) v.findViewById(R.id.tab_layout_naslov);
        mTRezNaslov.setText("Rezultati");


        //posto prikazuje prazan frag sve dok se ne odradi prvi swipe
        //onda ovako sumuliramo swipe da bi odmah dobio popunjen layout
        viewPager.setCurrentItem(1);
        viewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                viewPager.setCurrentItem(0);
            }
        },100);

        return v;
    }


    private class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 3;
        private Context context;

        public SampleFragmentPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            return UkrstanjeDV_Fragment_Rez_single_tab.newInstance(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return nizTemp[position] + "°C" ;
        }
    }


}