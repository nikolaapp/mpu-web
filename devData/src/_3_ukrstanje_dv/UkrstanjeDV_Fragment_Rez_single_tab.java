package nsp.mpu._3_ukrstanje_dv;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;

/**
 * Created by Nikola on 21.12.17.
 */

@Obfuscate
public class UkrstanjeDV_Fragment_Rez_single_tab extends Fragment
implements View.OnLongClickListener{


    public static UkrstanjeDV_Fragment_Rez_single_tab newInstance(int oznaka) {
        Bundle args = new Bundle();
        args.putInt("oznaka", oznaka);
        UkrstanjeDV_Fragment_Rez_single_tab fragment = new UkrstanjeDV_Fragment_Rez_single_tab();
        fragment.setArguments(args);
        return fragment;
    }

    private int oznaka;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        oznaka = getArguments().getInt("oznaka");
    }

    TextView mTRez0;
    TextView mTRez1;
    TextView mTRez2;
    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.tab_layout_single_tab, vg, false);

        mTRez0 = (TextView) v.findViewById(R.id.f33_prikaz_rezultata_VN);
        mTRez1 = (TextView) v.findViewById(R.id.f33_prikaz_rezultata_nn);
        mTRez2 = (TextView) v.findViewById(R.id.f33_prikaz_rezultata_raz);

        prikaziRezultate();

        v.setOnLongClickListener(this);

        return v;

    }

    private void prikaziRezultate() {
        String flagLedVN = (UkrstanjeDV_Fragment_Rez.nizTemp[oznaka]==-5) ? " (sa ledom):" : ":";
        String text_VN = "DV viseg napona" +flagLedVN+
                    "\n  naprezanje=" + String.format("%.2f[daN/mm²]", UkrstanjeDV_Fragment_Rez.nizJedStanja_VN[oznaka].getSigmaNOVO()) +
                    "\n  A=" + String.format("%.2f", UkrstanjeDV_Fragment_Rez.nizJedStanja_VN[oznaka].getKoefA()) +
                    "  B=" + String.format("%.2f", UkrstanjeDV_Fragment_Rez.nizJedStanja_VN[oznaka].getKoefB()) +
                    "\n  ugib=" + String.format("%.0f[cm]", UkrstanjeDV_Fragment_Rez.nizJedStanja_VN[oznaka].getUgib()) +
                    "  ugib_b=" + String.format("%.0f[cm]", UkrstanjeDV_Fragment_Rez.nizJedStanja_VN[oznaka].getUgibX(UkrstanjeDV_Fragment_Rez.udaljenost_VN)) +
                    "\n  kotaUzeta=" + String.format("%.2f[m]", UkrstanjeDV_Fragment_Rez.nizKotaUzadi_VN[oznaka]) +
                    "\n  VISINAuzeta=" + String.format("%.2f[m]", UkrstanjeDV_Fragment_Rez.nizVisina_VN[oznaka]);


        String flagLedNN = (UkrstanjeDV_Fragment_Rez.nizTemp[oznaka]==-5) ? " (bez leda):" : ":";
        String text_NN = "DV nizeg napona" +flagLedNN+
                    "\n  naprezanje=" + String.format("%.2f[daN/mm²]", UkrstanjeDV_Fragment_Rez.nizJedStanja_nn[oznaka].getSigmaNOVO()) +
                    "\n  A=" + String.format("%.2f", UkrstanjeDV_Fragment_Rez.nizJedStanja_nn[oznaka].getKoefA()) +
                    "  B=" + String.format("%.2f", UkrstanjeDV_Fragment_Rez.nizJedStanja_nn[oznaka].getKoefB()) +
                    "\n  ugib=" + String.format("%.0f[cm]", UkrstanjeDV_Fragment_Rez.nizJedStanja_nn[oznaka].getUgib()) +
                    "  ugib_b=" + String.format("%.0f[cm]", UkrstanjeDV_Fragment_Rez.nizJedStanja_nn[oznaka].getUgibX(UkrstanjeDV_Fragment_Rez.udaljenost_NN)) +
                    "\n  kotaUzeta=" + String.format("%.2f[m]", UkrstanjeDV_Fragment_Rez.nizKotaUzadi_nn[oznaka]) +
                    "\n  VISINAuzeta=" + String.format("%.2f[m]", UkrstanjeDV_Fragment_Rez.nizVisina_nn[oznaka]);


        double razlikaVisina = Math.abs(UkrstanjeDV_Fragment_Rez.nizVisina_VN[oznaka] - UkrstanjeDV_Fragment_Rez.nizVisina_nn[oznaka]);
        String text_raz ="RASTOJANJE = " + String.format("%.2f[m]", razlikaVisina);



        mTRez0.setText(text_VN);
        mTRez1.setText(text_NN);
        mTRez2.setText(text_raz);

        if (razlikaVisina < 2.5)
            mTRez2.setTextColor(Color.RED);

    }


    private void posaljiIzvestaj(CharSequence cs) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT, cs);
        //i.putExtra(Intent.EXTRA_SUBJECT, mTRezNaslov.getText());
        i = Intent.createChooser(i, "Odaberi nacin slanja:");
        startActivity(i);
    }

    @Override
    public boolean onLongClick(View v) {
        String t = UkrstanjeDV_Fragment_Rez.nizTemp[oznaka]+"[°C]";
        String out = "Pri " + t +"\n\n"+
                mTRez0.getText() + "\n\n"+
                mTRez1.getText() + "\n\n"+
                mTRez2.getText();
        posaljiIzvestaj(out);
        return false;
    }
}
