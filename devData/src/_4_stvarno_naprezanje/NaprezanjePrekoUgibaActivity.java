package nsp.mpu._4_stvarno_naprezanje;

import android.support.v4.app.Fragment;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;
import nsp.mpu.pomocne_klase.ActivityZaJedanFragment;
import nsp.mpu.pomocne_klase.AnimacijeKlasa;

@Obfuscate
public class NaprezanjePrekoUgibaActivity extends ActivityZaJedanFragment {

    @Override
    protected Fragment KreirajFragment(){
        return new NaprezanjePrekoUgiba_Fragment();
    }

///////////////////Prikaz dva fragmenta 5.0.1//////////////////////
    @Override
    protected int postaviNovLayout(){
        return R.layout.activity_reference_jedan_frag_ili_oba;
    }
//////////////////////////////////////////////////////////////////////
    @Override
    public void onBackPressed() {
         super.onBackPressed();
        AnimacijeKlasa.ParAnimacijaInOut parAnim = new AnimacijeKlasa.ParAnimacijaInOut();
        overridePendingTransition(parAnim.in, parAnim.out);
    }

}
