package nsp.mpu._4_stvarno_naprezanje;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.Arrays;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;
import nsp.mpu._0MainActivity;

import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animacijaZaRecycleView;
import static nsp.mpu.pomocne_klase.StaticPodaci.mojHtmlString;
import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;

@Obfuscate
public class NaprezanjePrekoUgibaPrikaz_Fragment extends Fragment{

    private TextView mNaslov, mPodnaslov;
    private TextView mUgib, mUdaljenost, mNaprezanje0, mNaprezanje1;
    private TextView mRezultat;
    private RecyclerView mRecyclerView;
    private AdapterPrikazaNaprezanja mAdapterPrikazaNaprezanja;

    private double[] nizUgiba, nizUdaljenosti, nizNaprezanja0, nizNaprezanja1;
    private int t0, t1;
    private double sigma0, sigma1;
    private float textSize;
    private boolean[] nizSelektovanja;

    public static final String ARGTag1 = "ARGTag1";
    public static final String ARGTag2 = "ARGTag2";
    public static final String ARGTag3 = "ARGTag3";
    public static final String ARGTag4 = "ARGTag4";
    public static final String ARGTag5 = "ARGTag5";
    public static final String ARGTag6 = "ARGTag6";
    public static final String ARGTag7 = "ARGTag7";
    public static final String ARGTag8 = "ARGTag8";

    public static NaprezanjePrekoUgibaPrikaz_Fragment noviFragmentNaprezPrekoUgibaPrikaz
            (int t0, int t1, double[] nizUgiba, double[] nizUdaljenosti, double[] nizNaprezanja0,
             double[] nizNaprezanja1, double sigma0, double sigma1){
        Bundle args = new Bundle();
        args.putInt(ARGTag1, t0);
        args.putInt(ARGTag2, t1);
        args.putDoubleArray(ARGTag3, nizUgiba);
        args.putDoubleArray(ARGTag4, nizUdaljenosti);
        args.putDoubleArray(ARGTag5, nizNaprezanja0);
        args.putDoubleArray(ARGTag6, nizNaprezanja1);
        args.putDouble(ARGTag7, sigma0);
        args.putDouble(ARGTag8, sigma1);

        NaprezanjePrekoUgibaPrikaz_Fragment newFrag = new NaprezanjePrekoUgibaPrikaz_Fragment();
        newFrag.setArguments(args);
        return newFrag;
    }

    @Override
    public void onCreate(Bundle b){
        super.onCreate(b);

        t0 = getArguments().getInt(ARGTag1);
        t1 = getArguments().getInt(ARGTag2);
        nizUgiba = getArguments().getDoubleArray(ARGTag3);
        nizUdaljenosti = getArguments().getDoubleArray(ARGTag4);
        nizNaprezanja0 = getArguments().getDoubleArray(ARGTag5);
        nizNaprezanja1 = getArguments().getDoubleArray(ARGTag6);
        sigma0 = getArguments().getDouble(ARGTag7);
        sigma1 = getArguments().getDouble(ARGTag8);
        
////////////////////////////////////// 5.0.2 prikaz naprezanja po projektu u holderu/////////
        // if (BuildConfig.DEBUG)Log.e("TAG3","nizNaprezanja1 "+nizNaprezanja1.length +" nizNaprezanja1[0] "+nizNaprezanja1[0]);

        nizUgiba = Arrays.copyOf(nizUgiba, nizUgiba.length + 1);
        nizUdaljenosti = Arrays.copyOf(nizUdaljenosti, nizUdaljenosti.length+1);
        nizNaprezanja0 = Arrays.copyOf(nizNaprezanja0, nizNaprezanja0.length+1);
        nizNaprezanja1 = Arrays.copyOf(nizNaprezanja1, nizNaprezanja1.length+1);

        // nepostoji kao flag za holder
        nizUgiba[nizUgiba.length-1] = nepostoji;
        nizUdaljenosti[nizUdaljenosti.length-1]= nepostoji;

        nizNaprezanja0[nizNaprezanja0.length-1]=sigma0;
        nizNaprezanja1[nizNaprezanja1.length-1]=sigma1;

        // if (BuildConfig.DEBUG)Log.e("TAG3","nizNaprezanja1 "+nizNaprezanja1.length+" nizNaprezanja1[0] "+nizNaprezanja1[0]+" nizNaprezanja1[1] "+nizNaprezanja1[1]);

        nizSelektovanja = new boolean[nizUgiba.length];
        Arrays.fill(nizSelektovanja, true);
/////////////////////////////////////////////////////////////////////////////////////////////////////////

        textSize = 26;
    }

    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b){
        View v;
        if (mDaLiJeTablet)
            v = li.inflate(R.layout.fragment_naprezanje_preko_ugiba_prikaz, vg, false);
        else {
            v = li.inflate(R.layout.fragment_naprezanje_preko_ugiba_prikaz, vg, false);
        }

        mNaslov = (TextView)v.findViewById(R.id.f51_naslov);
        mPodnaslov = (TextView)v.findViewById(R.id.f51_podnaslov);
        mUgib = (TextView)v.findViewById(R.id.f51_ugib);
        mUdaljenost = (TextView)v.findViewById(R.id.f51_odstojanje);
        mNaprezanje0 = (TextView)v.findViewById(R.id.f51_naprezanje0);
        mNaprezanje1 = (TextView)v.findViewById(R.id.f51_naprezanje1);
        mRezultat = (TextView)v.findViewById(R.id.f51_rezultat);

        mUgib.setText("f[cm]");
        mUdaljenost.setText("b[m]");
        mNaprezanje0.setText("σ_" + t0);
        mNaprezanje1.setText("σ_" + t1);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.f51_prikazUgiba_recycle_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        startAdaptera();

        prikaziRezultate();
        mRezultat.setTextIsSelectable(true);
        mRezultat.setBackgroundResource(R.drawable.borderline_sivo);


        if (_0MainActivity.mDaLiJeTablet) {
            mNaslov.setTextSize(textSize+4);
            mPodnaslov.setTextSize(textSize);
            mUgib.setTextSize(textSize);
            mUdaljenost.setTextSize(textSize);
            mNaprezanje0.setTextSize(textSize);
            mNaprezanje1.setTextSize(textSize);
            mRezultat.setTextSize(textSize);
        }

        return v;
    }

    @Obfuscate//import
    private class HolderPrikazaNaprezanja extends RecyclerView.ViewHolder {
        //HOLDER od ADAPTERA dobija layout izgled i podatke iz modela, koje vezuje

        private TextView mUgib;
        private TextView mUdaljenost;
        private TextView mNaprezanje0;
        private TextView mNaprezanje1;
        private CheckBox mCheckBox;

        private HolderPrikazaNaprezanja(View v) {
            super(v);

            mUgib = (TextView) v.findViewById(R.id.f52_0_ugib);
            mUdaljenost = (TextView) v.findViewById(R.id.f52_0_odstojanje);
            mNaprezanje0 = (TextView) v.findViewById(R.id.f52_1_maxnaprezanje);
            mNaprezanje1 = (TextView) v.findViewById(R.id.f52_2_napez_pri_temp);
            mCheckBox = (CheckBox) v.findViewById(R.id.f52_5_CheckBox);
            mCheckBox.setChecked(true);


            if (_0MainActivity.mDaLiJeTablet) {
                mUgib.setTextSize(textSize);
                mUdaljenost.setTextSize(textSize);
                mNaprezanje0.setTextSize(textSize);
                mNaprezanje1.setTextSize(textSize);
            }

        }
    }
    @Obfuscate//import
    private class AdapterPrikazaNaprezanja extends RecyclerView.Adapter<HolderPrikazaNaprezanja>{

        public AdapterPrikazaNaprezanja(){}

        @Override
        //kreira i salje izgled fragmenta ka holderu
        public HolderPrikazaNaprezanja onCreateViewHolder(ViewGroup vg, int i){
            LayoutInflater li = LayoutInflater.from(getActivity());
            View v;
            if (mDaLiJeTablet)
                v = li.inflate(R.layout.fragment_naprezanje_preko_ugiba_prikaz_recycleview,vg,false);
            else
                v = li.inflate(R.layout.fragment_naprezanje_preko_ugiba_prikaz_recycleview,vg,false);
            return new HolderPrikazaNaprezanja(v);
        }

        @Override
        //salje podatke iz modela ka holderu
        public void onBindViewHolder(final HolderPrikazaNaprezanja  holder, final int p){

            if (nizUgiba[p]==nepostoji || nizUdaljenosti[p]==nepostoji){
                holder.mUgib.setText("prema");
                holder.mUdaljenost.setText("projektu");
                holder.mUgib.setTypeface(null, Typeface.BOLD);
                holder.mUdaljenost.setTypeface(null, Typeface.BOLD);
                holder.mNaprezanje0.setTypeface(null, Typeface.BOLD);
                holder.mNaprezanje1.setTypeface(null, Typeface.BOLD);

            }else{
                holder.mUgib.setText(String.format("%.1f", nizUgiba[p]));
                holder.mUdaljenost.setText(String.format("%.2f", nizUdaljenosti[p]));
            }

            holder.mNaprezanje0.setText(String.format("%.3f", nizNaprezanja0[p]));
            holder.mNaprezanje1.setText(String.format("%.3f", nizNaprezanja1[p]));

            holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton cb, boolean b){
                    nizSelektovanja[p] = holder.mCheckBox.isChecked();
                    prikaziRezultate();
                }
            });
        }
        @Override
        //Velicina liste sa kojom treba adapter da radi
        public int getItemCount(){
            return nizUgiba.length;
        }
    }

    private void startAdaptera(){
        mAdapterPrikazaNaprezanja = new AdapterPrikazaNaprezanja();
        mRecyclerView.setAdapter(mAdapterPrikazaNaprezanja);
        animacijaZaRecycleView(mRecyclerView, true);
    }

    private static double srednjaVrednost(double[] niz, boolean[] nizSelektovanja){

        double[] pomocniNiz = new double[1]; // niz od jednog clana
        boolean flag=true;
        for (int i = 0, j=1; i < nizSelektovanja.length; i++)
            if (nizSelektovanja[i]) { // ako je selektovan onda se upisuje u pomocniNiz
                if (flag) {
                    // prvi clan - ostali koriste Arrays.copyOf(pomocniNiz, j+1) pre upisa
                    pomocniNiz[0] = niz[i];
                    flag=false;
                } else {
                    // kopira trenutne clanove i povecava duzinu niza za 1
                    pomocniNiz = Arrays.copyOf(pomocniNiz, j+1);
                    // upisuje u novostrvorenu (poslednju) poziciju
                    pomocniNiz[j] = niz[i];
                    j++;
                }
            }
        niz=pomocniNiz;

//         if (BuildConfig.DEBUG)Log.e("TAG3","######niz.length " + niz.length);
//        for (int i = 0; i <niz.length ; i++)
//             if (BuildConfig.DEBUG)Log.e("TAG3","niz"+i+ " = " + niz[i]);

        double suma = 0;
        for (int i = 0; i <niz.length ; i++)
            suma += niz[i];
        return suma/(niz.length);
    }

    private void prikaziRezultate(){

        String temp0 = t0+"", temp1 = t1+"";
        if (t0==-5) temp0 = t0 + "_led";
        if (t1==-5) temp1 = t1 + "_led";
        mRezultat.setText(mojHtmlString("Srednja vrednost selektovanih naprezanja [daN/mm²]:<font color='green'>"+
                String.format("<br><b>%.3f",srednjaVrednost(nizNaprezanja0, nizSelektovanja)) + "   </b>pri  "+temp0+" °C" +
                String.format("<br><b>%.3f",srednjaVrednost(nizNaprezanja1, nizSelektovanja)) + "   </b>pri  "+temp1+" °C</font>"
        ));
    }
}
