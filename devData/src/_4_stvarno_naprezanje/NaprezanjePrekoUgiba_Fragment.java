package nsp.mpu._4_stvarno_naprezanje;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.JednacinaStanja;

import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu.database.PODACI_ListaTipovaUzadi.getListaUzadi;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animacijaZaRecycleView;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animirajTekst_running;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_IN;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_OUT;
import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;
import static nsp.mpu.pomocne_klase.StaticPodaci.pozicijaTipskogOpterUNizu;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;
import static nsp.mpu.pomocne_klase.StaticPodaci.temperaturaProv_default;
import static nsp.mpu.pomocne_klase.StaticPodaci.temperaturaProv_max;
import static nsp.mpu.pomocne_klase.StaticPodaci.temperaturaProv_min;
import static nsp.mpu.pomocne_klase.StaticPodaci.tipskaDodOpt;

@Obfuscate
public class NaprezanjePrekoUgiba_Fragment extends Fragment{

    private NumberPicker mTipUzeta;
    private EditText mMaxNaprezanje;
    private EditText mRaspon;
    private EditText mCosFi, mKotaU1, mKotaU2;
    private NumberPicker mDodOpt;
    private NumberPicker mT1;
    private EditText mBrojUgiba;
    private CheckBox mCheckBox_visine;
    private LinearLayout mLinearLayout;

    private RecyclerView mRecyclerView;
    private AdapterUgiba mAdapterUgiba;

    private Button mProracun;


    protected PODACI_TipUzeta mTU;
    private double sigma0, sigma_trenutno, koefDodOpt,  raspon, cosFi;
    private int brojUgiba, temp_trenutna;
    private double[] nizUgiba, nizUdaljenosti, nizNaprezanja_trenTemp, nizNaprezanja_t0;
    private double visinaU1, visinaU2;
    private int t0; // temperatura pocetnog stanja u jed.stanja
    private String [] nizOznakaUzadi;
    private static boolean prvoPokretanje = true;

    private int brojUgiba_adapter;
    private final double ugib_default=50.1;
    private final double udaljenost_default=10.01;
    private final double sigma0_default=9;
    private final double raspon_default=100;
    private final double cosFi_default=1;

    @Override
    public void onCreate(Bundle b){
        super.onCreate(b);

        /////pocetne vrednosti
        sigma0 = sigma0_default;
        raspon =raspon_default;
        cosFi = cosFi_default;
        brojUgiba=1;

        koefDodOpt=1;
        nizOznakaUzadi = getListaUzadi(getActivity()).getNizNazivUzadi();
        mTU=getListaUzadi(getActivity()).getTipUzetaIzListe("Al/Ce-70/12");
        temp_trenutna = temperaturaProv_default;

        nizUgiba = new double[]{ugib_default};
        nizUdaljenosti = new double[]{udaljenost_default};
    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        if (mDaLiJeTablet)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        View v = li.inflate(R.layout.fragment_naprezanje_preko_ugiba, vg, false);
        setHasOptionsMenu(true);

        mTipUzeta = (NumberPicker) v.findViewById(R.id.num_picker_uze);
        mTipUzeta.setDisplayedValues(nizOznakaUzadi);
        mTipUzeta.setMinValue(0);
        mTipUzeta.setMaxValue(nizOznakaUzadi.length-1);
        // default je true, tako da je ovo nepotrebno
        // mTipUzeta.setWrapSelectorWheel(true);
        mTipUzeta.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mTipUzeta.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                 if (BuildConfig.DEBUG)Log.e("TAG","newVal= " + newVal);
                mTU= getListaUzadi(getActivity()).getTipUzetaIzListe(nizOznakaUzadi[newVal]);
                 if (BuildConfig.DEBUG)Log.e("TAG","tipUzeta= " + mTU.getOznakaUzeta());
            }
        });
        mTipUzeta.setValue(getListaUzadi(getActivity()).pozicijaUzetaUListi(mTU.getOznakaUzeta()));
////////////////////////////////////////////////////////////////////////
        mDodOpt = (NumberPicker) v.findViewById(R.id.num_picker_odo);
        mDodOpt.setDisplayedValues( tipskaDodOpt );
        mDodOpt.setMinValue(0);
        mDodOpt.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mDodOpt.setMaxValue(tipskaDodOpt.length-1);
        mDodOpt.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                 if (BuildConfig.DEBUG)Log.e("TAG","newVal= " + newVal);
                koefDodOpt =Double.valueOf(tipskaDodOpt[newVal]);
                 if (BuildConfig.DEBUG)Log.e("TAG","koefDodOpt= " + koefDodOpt);
            }
        });
        mDodOpt.setValue(pozicijaTipskogOpterUNizu(koefDodOpt));
//////////////////////////////////////////////////////////////////////////
        mT1 = (NumberPicker) v.findViewById(R.id.num_picker_temp);
        mT1.setMinValue(0);
        mT1.setMaxValue(temperaturaProv_max - temperaturaProv_min);
        mT1.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mT1.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int i) {
                if (i == Math.abs(temperaturaProv_min))
                    return "0";
                else
                    return String.valueOf(i + temperaturaProv_min);
            }
        });
        mT1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                temp_trenutna =newVal+ temperaturaProv_min;
                 if (BuildConfig.DEBUG)Log.e("TAGtemp","temp_trenutna= " + temp_trenutna);
            }
        });
        mT1.setValue(temp_trenutna+Math.abs(temperaturaProv_min));
////////////////////////////////////////////////////////////////////////////
        mMaxNaprezanje = (EditText) v.findViewById(R.id.dugme_f5_odaberi_naprezanje);
        mMaxNaprezanje.setHint("Max.naprezanje po projektu");
        mMaxNaprezanje.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                double pomocnaVar =0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<1||pomocnaVar>999){
                    sigma0=0;
                    prikaziToast(getContext(),"Unesi naprezanje od 1 do 99");
                } else
                    sigma0=pomocnaVar;
                 if (BuildConfig.DEBUG)Log.e("TAG","sigma0= " + sigma0);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });
////////////////////////////////////////////////////////////////////////////
        mRaspon = (EditText) v.findViewById(R.id.dugme_f5_unesi_raspon);
        mRaspon.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                 if (BuildConfig.DEBUG)Log.e("TAG","PRE PARSE raspon= " + raspon);
                double pomocnaVar =0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<1||pomocnaVar>999){
                    raspon=raspon_default;
                    prikaziToast(getContext(),"Unesi raspon od 1 do 1000 metara");
                } else
                    raspon=pomocnaVar;
                 if (BuildConfig.DEBUG)Log.e("TAG","raspon= " + raspon);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });
////////////////////////////////////////////////////////////////////////////
        mCosFi = (EditText) v.findViewById(R.id.dugme_f5_unesi_cosFi);
        mCosFi.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                 if (BuildConfig.DEBUG)Log.e("TAG","PRE PARSE cosFi= " + cs);

                double pomocnaVar = 1;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar>0 || pomocnaVar<=1)
                    cosFi=pomocnaVar;
                else{
                    cosFi = cosFi_default;
                    prikaziToast(getContext(), "Unesi cosFi iz intervala (0,1]");
                }

                 if (BuildConfig.DEBUG)Log.e("TAG","cosFi= " + cosFi);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });
////////////////////////////////////////////////////////////////////////////////////
        mLinearLayout = (LinearLayout) v.findViewById(R.id.f5_layout_visine_uzeta);
        mLinearLayout.setVisibility(View.GONE);
////////////////////////////////////////////////////////////////////////////////////
        mCheckBox_visine = (CheckBox) v.findViewById(R.id.prikaziVisineVesanjaUzeta);
        mCheckBox_visine.setChecked(true);
        mCheckBox_visine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton cb, boolean b){
                if (mCheckBox_visine.isChecked()) {
                    mCosFi.setEnabled(true);
                    mCosFi.getText().clear();
                    mKotaU1.getText().clear();
                    mKotaU2.getText().clear();
                    mLinearLayout.setVisibility(View.GONE);
                    visinaU1 = 100;
                    visinaU2 = 100;
                }else {
                    mLinearLayout.setVisibility(View.VISIBLE);
                    mCosFi.setEnabled(false);
                }
            }
        });
//////////////////////////////////////////////////////////////////////
        mKotaU1 = (EditText) v.findViewById(R.id.dugme_f5_visina_uzeta_1);
        mKotaU1.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                double pomocnaVar =0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<1 || pomocnaVar>2117){
                    visinaU1=100;
                    if (mLinearLayout.getVisibility()==View.VISIBLE)
                        prikaziToast(getContext(),"Unesi visinu vesanja uzeta u m - izmedju 1 i 2117");
                } else
                    visinaU1 = pomocnaVar;

                izracunajCosFi();
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });
//////////////////////////////////////////////////////////////////////
        mKotaU2 = (EditText) v.findViewById(R.id.dugme_f5_visina_uzeta_2);
        mKotaU2.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                double pomocnaVar =0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<1 || pomocnaVar>2117){
                    visinaU2=100;
                    if (mLinearLayout.getVisibility()==View.VISIBLE)
                        prikaziToast(getContext(),"Unesi visinu vesanja uzeta u m - izmedju 1 i 2117");
                } else
                    visinaU2 = pomocnaVar;

                izracunajCosFi();
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

////////////////////////////////////////////////////////////////////////////
        mBrojUgiba = (EditText) v.findViewById(R.id.dugme_f5_broj_ugiba);
        mBrojUgiba.setHint("Unesi broj merenih ugiba");
        mBrojUgiba.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                 if (BuildConfig.DEBUG)Log.e("TAG","PRE PARSE brojUgiba= " + cs);

                int pomocnaVar = 1;
                try {
                    pomocnaVar = Integer.parseInt(cs.toString());
                } catch (NumberFormatException e) {
                    pomocnaVar = 1;
                }

                if (pomocnaVar>9){
                    // nije potrebno zbog - android:maxLength="1"
                    brojUgiba=1;
                    prikaziToast(getContext(),"Unesi brojUgiba 1 do 9");
                } else if (pomocnaVar==0)
                    // nije potrebno zbog -  android:digits="123456789"
                    brojUgiba=1;
                else {
                    brojUgiba = pomocnaVar;
                }

                startAdaptera();
                 if (BuildConfig.DEBUG)Log.e("TAG","brojUgiba - startAdaptera = " + brojUgiba);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });
////////////////////////////////////////////////////////////////////////////
        mRecyclerView = (RecyclerView) v.findViewById(R.id.f5_recycle_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
////////////////////////////////////////////////////////////////////////////

        mProracun = (Button) v.findViewById(R.id.dugme_f5_razno);
        mProracun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                izracunajNaprezanja();

                Fragment noviFrag = NaprezanjePrekoUgibaPrikaz_Fragment.noviFragmentNaprezPrekoUgibaPrikaz
                        (t0, temp_trenutna, nizUgiba, nizUdaljenosti, nizNaprezanja_t0, nizNaprezanja_trenTemp,
                                sigma0, sigma_trenutno);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                if (mDaLiJeTablet) {
                    startAdaptera(); // da bi ispisalo vrednosti koje je adapter poslao
                                    // a ne da ostanu unete vrednosti koje mozda nisu prihvatljive
                    fm.beginTransaction()
                            .setCustomAnimations(
                                     odaberiAnimaciju_fragment_IN(),                             odaberiAnimaciju_fragment_OUT(),                             odaberiAnimaciju_fragment_IN(),                             odaberiAnimaciju_fragment_OUT()
                            )
                            .replace(R.id.activity_prikaz_drugog_fragmenta, noviFrag)
                            .commit();
                }
                else
                    fm.beginTransaction()
                            .setCustomAnimations(
                                     odaberiAnimaciju_fragment_IN(),                             odaberiAnimaciju_fragment_OUT(),                             odaberiAnimaciju_fragment_IN(),                             odaberiAnimaciju_fragment_OUT()
                            )
                            .replace(R.id.activity_prikaz_fragmenta, noviFrag)
                            .addToBackStack(null) // back dugme vraca na zamenjeni frag
                            .commit();
            }
        });


        /*
        da ne pokreca adapter pri povratku fragmenta iz STACKa - tada se adapter pokrece zbog
        ponovnog kreiranja onCreateView (odnocno u mBrojUgiba se automatski upisuje prethodna vrednost
        cime se poziva startAdaptera.
        A potrebno je da se izvrsi pri prvom pokretanju jer se tada ne setuje vrednost za mBrojUgiba,
        pa ne dolazi do pozivanja startAdaptera iz mBrojUgiba
        */
        if (prvoPokretanje){
            // if (BuildConfig.DEBUG)Log.e("TAG4","###############prvoPokretanje = "+ prvoPokretanje);
            // ne mora da se pokrece ni samo jednom jer ce pri unosu vrednosti u mBrojUgiba
            // doci do pokretanja adaptera
            //startAdaptera();
            prvoPokretanje=false;
        }

        if (BuildConfig.IS_DEMO) {
            mDodOpt.setValue(1);
            koefDodOpt = 1.6;
            mDodOpt.setEnabled(false);
        }

        animirajTekst_running(getContext(),(TextView) v.findViewById(R.id.text_stepeni),"Al/Ce");

        return v;
    }

    private class HolderUgiba extends RecyclerView.ViewHolder {
        //HOLDER od ADAPTERA dobija layout izgled i podatke iz modela, koje vezuje

        private EditText mEditTextUgib;
        private EditText mEditTextUdaljenost;

        private HolderUgiba(View v) {
            super(v);

            mEditTextUgib = (EditText) v.findViewById(R.id.f2_rasponi_raspon);
            mEditTextUgib.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence cs, int s, int b, int c) {
                     if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE holder ugib= " + cs);

                    double pomocnaVar = nepostoji;
                    try {
                        pomocnaVar = Double.parseDouble(cs.toString());
                    } catch (NumberFormatException e) {}

                    if (pomocnaVar<1||pomocnaVar>99_999){
                        nizUgiba[getAdapterPosition()]=ugib_default;
                        if(mEditTextUgib.didTouchFocusSelect())
                            prikaziToast(getContext(),"Unesi ugib od 1 do 99.999 cm");
                    } else
                        nizUgiba[getAdapterPosition()]=pomocnaVar;

                     if (BuildConfig.DEBUG)Log.e("TAG", "holder ugib= " + nizUgiba[getAdapterPosition()]+" index "+getAdapterPosition());
                }
                @Override
                public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
                @Override
                public void afterTextChanged(Editable e) {}
            });

            mEditTextUdaljenost = (EditText) v.findViewById(R.id.f2_rasponi_cosFi);
            mEditTextUdaljenost.addTextChangedListener(new TextWatcher() { // mora ovako
                @Override
                public void onTextChanged(CharSequence cs, int s, int b, int c) {
                     if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE holder Udaljenost= " + cs);

                    double pomocnaVar = nepostoji; // -1 da bi upao u if a ne u else
                    try {
                        pomocnaVar = Double.parseDouble(cs.toString());
                    } catch (NumberFormatException e) {}

                    if (pomocnaVar<0||pomocnaVar>=raspon) {
                        nizUdaljenosti[getAdapterPosition()] = udaljenost_default;
                        //mEditTextUdaljenost.getText().clear();
                        if(mEditTextUgib.didTouchFocusSelect())
                            prikaziToast(getContext(), "Unesi udaljenost manju od duzine raspona");
                    }else
                        nizUdaljenosti[getAdapterPosition()]=pomocnaVar;

                     if (BuildConfig.DEBUG)Log.e("TAG", "holder nizUdaljenosti= " + nizUdaljenosti[getAdapterPosition()]+" index "+getAdapterPosition());
                }
                @Override
                public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
                @Override
                public void afterTextChanged(Editable e) {}
            });
        }
    }

    private class AdapterUgiba extends RecyclerView.Adapter<HolderUgiba>{

        /*
        kada bi ovde definisao  private int brojUgiba_adapter;
        on bi se pri svakom povratku na fragment iznova postavljao na 0
        Zato je definisan kao gore, i zato zadrzava vrednost pri povratku
        iz prikaza tabele naprezanja
        */

        public AdapterUgiba(int brojUgiba_novo){
            nizUgiba =       Arrays.copyOf(nizUgiba, brojUgiba_novo);
            nizUdaljenosti = Arrays.copyOf(nizUdaljenosti, brojUgiba_novo);
//             if (BuildConfig.DEBUG)Log.e("TAG4","brojUgiba_adapter --- "+ brojUgiba_adapter + " brojUgiba_novo --- "+ brojUgiba_novo );
//             if (BuildConfig.DEBUG)Log.e("TAG4","startAdaptera - nizUgiba staro"+ Arrays.toString(nizUgiba));

            if (brojUgiba_novo > brojUgiba_adapter) {
                Arrays.fill(nizUgiba,       brojUgiba_adapter, brojUgiba_novo, ugib_default);
                Arrays.fill(nizUdaljenosti, brojUgiba_adapter, brojUgiba_novo, udaljenost_default);
            }

//             if (BuildConfig.DEBUG)Log.e("TAG4","startAdaptera - nizUgiba      "+ Arrays.toString(nizUgiba));

//            ovo se na kraju izvrsava!
            brojUgiba_adapter = brojUgiba_novo;
        }

        @Override
        //kreira i salje izgled fragmenta ka holderu
        public HolderUgiba onCreateViewHolder(ViewGroup vg, int i){
            LayoutInflater li = LayoutInflater.from(getActivity());
            View v = li.inflate(R.layout.fragment_recycler_view_po_dva_elementa,vg,false);
            return new HolderUgiba(v);
        }

        @Override
        //salje podatke iz modela ka holderu
        public void onBindViewHolder(HolderUgiba holder, int pozicija){
            if (nizUgiba[pozicija]==ugib_default &&  nizUdaljenosti[pozicija]==udaljenost_default){
                holder.mEditTextUgib.setHint("ugib["+(pozicija+1)+"]");//ugib1, ugib2...lepse za oko
                holder.mEditTextUdaljenost.setHint("udaljenost["+(pozicija+1)+"]");
            } else {
                holder.mEditTextUgib.setText(String.valueOf(nizUgiba[pozicija]));
                holder.mEditTextUdaljenost.setText(String.valueOf(nizUdaljenosti[pozicija]));
            }
             if (BuildConfig.DEBUG)Log.e("TAG4","pozicija ="+pozicija +  " nizUgiba="+nizUgiba[pozicija]+" nizUdaljenosti="+nizUdaljenosti[pozicija]);


            if (pozicija+1 == brojUgiba_adapter){
                holder.mEditTextUdaljenost.setImeOptions(EditorInfo.IME_ACTION_DONE);
            }

        }
        @Override
        //Velicina liste sa kojom treba adapter da radi
        public int getItemCount(){
            return brojUgiba_adapter;
        }
    }

    private void startAdaptera(){
        // if (BuildConfig.DEBUG)Log.e("TAG4","startAdaptera");
        mAdapterUgiba = new AdapterUgiba(brojUgiba);
        mRecyclerView.setAdapter(mAdapterUgiba);
        animacijaZaRecycleView(mRecyclerView, true);
    }

    private void izracunajNaprezanja(){

        nizNaprezanja_trenTemp = new double[brojUgiba];
        nizNaprezanja_t0 = new double[brojUgiba];
        Arrays.fill(nizNaprezanja_t0, 0);
        Arrays.fill(nizNaprezanja_trenTemp, 0);

// onemogucava da se promenom raspona (nakon upisa udaljenosti) dodje u situaciju
// da je raspon manji od udaljenosti
        for (int i = 0; i < nizUdaljenosti.length; i++) {
            if (nizUdaljenosti[i]>=raspon){
                nizUdaljenosti[i]=raspon/2;
                // Toast.LENGTH_LONG
                Toast.makeText(getContext(),"Udaljenost["+(i+1)+"] ne sme biti veca od unetog raspona", Toast.LENGTH_LONG).show();
            }
        }

        JednacinaStanja js = new JednacinaStanja(mTU, sigma0, raspon,
                cosFi, koefDodOpt, temp_trenutna);

        sigma_trenutno = js.getSigmaNOVO();


        if (raspon > js.getAkr())
            t0 = -5;
        else
            t0 = -20;


        for (int i=0; i<brojUgiba; i++){
            nizNaprezanja_trenTemp[i] = JednacinaStanja.naprezanjePrekoUgiba( raspon,  cosFi,
                    nizUgiba[i], mTU.getSpecTezina(), nizUdaljenosti[i], sigma0);

            // racunaj naprezanje pri t0, ako je temp_trenutna temperatura pocetnog stanja
            js = new JednacinaStanja(mTU, nizNaprezanja_trenTemp[i], raspon,
                    cosFi, koefDodOpt, t0 , temp_trenutna);

            nizNaprezanja_t0[i] = js.getSigmaNOVO();

            // if (BuildConfig.DEBUG)Log.e("TAG", "JednacinaStanja pozvan konstruktor" + mTU.getOznakaUzeta() + " " + sigma0 + " " + raspon + " " + cosFi + " " + koefDodOpt + " " + temp_trenutna);
        }
         if (BuildConfig.DEBUG)Log.e("TAG2", "nizUgiba " + nizUgiba );

    }

    private void izracunajCosFi(){
        // if koristim da pri vracanju sa prikaza rezultata ne bi upisivao NAN u cosFI
        if ( ! mCheckBox_visine.isChecked()) {
            cosFi = raspon / Math.sqrt(Math.pow(raspon, 2) +
                    Math.pow(visinaU1 - visinaU2, 2));
            mCosFi.setText(String.format("%.5f", cosFi));
             if (BuildConfig.DEBUG)Log.e("TAG3","raspon = "+raspon+" visinaU1 = "+visinaU1+" visinaU2 = "+ visinaU2+" cosFi = "+cosFi);
        }
    }

}
