package nsp.mpu._5_sigurnosne_visine;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import nsp.mpu.R;
import nsp.mpu.database.PODACI_ListaTipovaKonzola;
import nsp.mpu.database.PODACI_ListaTipovaStubova;
import nsp.mpu.database.PODACI_TipIzolatora;
import nsp.mpu.database.PODACI_TipKonzole;
import nsp.mpu.database.PODACI_TipStuba;
import nsp.mpu.pomocne_klase.AnimacijeKlasa;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu.database.PODACI_ListaTipovaIzolatora.getSingltonIzolatora;
import static nsp.mpu.database.PODACI_ListaTipovaKonzola.getSingltonKonzola;
import static nsp.mpu.database.PODACI_ListaTipovaStubova.getSingltonStubova;
import static nsp.mpu.pomocne_klase.StaticPodaci.elamentPripadaNizu;
import static nsp.mpu.pomocne_klase.StaticPodaci.getNizMaterijalStubova;
import static nsp.mpu.pomocne_klase.StaticPodaci.getNizTipovaStubova;
import static nsp.mpu.pomocne_klase.StaticPodaci.getPozadina;
import static nsp.mpu.pomocne_klase.StaticPodaci.mojHtmlString;
import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziWarnToast;

/**
 * Created by Nikola on 23.3.2018.
 * Kreiranje alert dialoga za prikaz baza stubova, konzola ili izolatora
 */

public class AlertDialog_TipOpreme {
    private static Activity act;
////////////////////////////////////////////////////////////////////////
    // nisu unutar funkcije jer im se pristupa iz anonimus klasa (iz listenera)
    private static PODACI_TipStuba stub;
    private static StaticPodaci.Tip_Stuba tipStuba;
    private static StaticPodaci.MaterijalStuba matStuba;
    public static AlertDialog getDialog_stub(final Activity activity){
        act=activity;

            // inicijani stub je stub koji je prvi u listi podataka
        stub = getSingltonStubova(act).getTipStubaIzListe(0);

        final AlertDialog ad = new AlertDialog.Builder(act).create();
        ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
        ad.setTitle(mojHtmlString("Baza stubova:"));
        LayoutInflater inflater = act.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_dimenzije_stub, null);
        ad.setView(v);

        final Spinner mSpinnerOznakaElementa     = (Spinner)v.findViewById( R.id.dialog_spiner );

        ImageButton deleteButton = (ImageButton)v.findViewById( R.id.dialog_delete );
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = mSpinnerOznakaElementa.getSelectedItem().toString();
                stub =    getSingltonStubova(act).getTipStubaIzListe(s);
                if (stub.getIzmenljivo()) {
                    String pomocniString = stub.getOznaka();
                    if (getSingltonStubova(act).deleteTipStuba(stub)) {
                        prikaziToast(act, "<b>"+pomocniString + " </b>je izbrisan iz baze!");
                        TabelaNaprezanjaStuba_single_tab.spinnerControl(
                                act,
                                mSpinnerOznakaElementa,
                                getSingltonStubova(act).getNizOznakaStub(),
                                getSingltonStubova(act).getTipStubaIzListe(0).getOznaka());

                    }
                    else
                        prikaziWarnToast(act, pomocniString + " - GRESKA nije moguće brisanje iz baze!");
                }else
                    prikaziToast(act,"Nije dozvoljeno brisanje osnovnog tipa!");
            }
        });


        final Spinner mTipStuba = (Spinner) v.findViewById(R.id.odabir_stuba_spiner_tip_stuba);
        final Spinner mMaterijalStuba = (Spinner) v.findViewById(R.id.odabir_stuba_spiner_materijal_stuba);
        final EditText mDialogL = (EditText) v.findViewById(R.id.napr_stuba_dialog_ln);
        final EditText mDialogTu= (EditText) v.findViewById(R.id.napr_stuba_dialog_tu);
        final EditText mDialogD = (EditText) v.findViewById(R.id.napr_stuba_dialog_d_veliko);
        final EditText mDialogd = (EditText) v.findViewById(R.id.napr_stuba_dialog_d_malo);
        final List<View> lista = new ArrayList<>();
        lista.add(mTipStuba );
        lista.add(mMaterijalStuba );
        lista.add(mDialogL );
        lista.add(mDialogTu);
        lista.add(mDialogD );
        lista.add(mDialogd );
        viewEnableTweak(lista, false);

        TabelaNaprezanjaStuba_single_tab.spinnerControl(act, mSpinnerOznakaElementa,     getSingltonStubova(act).getNizOznakaStub(), stub.getOznaka());
        mSpinnerOznakaElementa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String s = adapterView.getSelectedItem().toString();
                stub =    getSingltonStubova(act).getTipStubaIzListe(s);
                TabelaNaprezanjaStuba_single_tab.spinnerControl(act, mTipStuba,     getNizTipovaStubova(), stub.getTip());
                TabelaNaprezanjaStuba_single_tab.spinnerControl(act, mMaterijalStuba,     getNizMaterijalStubova(), stub.getMaterijal());
                /** Locale.US jer parseDouble radi sa tackom kao decSeparator */
                mDialogL.setText(String.format(Locale.US, "%.2f",stub.getDuzina()));
                mDialogTu.setText(String.format(Locale.US,"%.2f",stub.getDubina()));
                mDialogD.setText(String.format(Locale.US, "%.1f",stub.getPrecnikVeci()*100));
                mDialogd.setText(String.format(Locale.US, "%.1f",stub.getPrecnikManji()*100));

            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        TabelaNaprezanjaStuba_single_tab.spinnerControl(act, mMaterijalStuba,     getNizMaterijalStubova(), stub.getTip());
        mMaterijalStuba.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                matStuba = (StaticPodaci.MaterijalStuba) adapterView.getSelectedItem();
                if (matStuba .equals(StaticPodaci.MaterijalStuba.betonski)){
                    mDialogTu.setVisibility(View.VISIBLE);
                    mDialogD.setVisibility(View.VISIBLE);
                    mDialogd.setVisibility(View.VISIBLE);
                }
                else{
                    mDialogTu.setVisibility(View.INVISIBLE);
                    mDialogD.setVisibility( View.INVISIBLE);
                    mDialogd.setVisibility( View.INVISIBLE);
                }
            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });


        TabelaNaprezanjaStuba_single_tab.spinnerControl(act, mTipStuba,     getNizTipovaStubova(), stub.getTip());
        mTipStuba.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tipStuba = (StaticPodaci.Tip_Stuba) adapterView.getSelectedItem();
                }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });


        final LinearLayout LL = (LinearLayout) v.findViewById(R.id.dialog_LL_delete_spiner);
        final EditText mNovTip = (EditText) v.findViewById(R.id.nov_tip_naziv);
        mNovTip.setVisibility(View.GONE);
        final Button novTip = (Button) v.findViewById(R.id.nov_tip);
        novTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LL.setVisibility(View.GONE);
                novTip.setVisibility(View.GONE);
                mNovTip.setVisibility(View.VISIBLE);
                viewEnableTweak(lista,true);
            }
        });


        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (mNovTip.getVisibility() == View.VISIBLE ){

                            String novnaziv = mNovTip.getText().toString();

                            if ( nazivElementaJeOK (getSingltonStubova(activity).getNizOznakaStub(),novnaziv) ){
                                double tu, D, d;

                                if (mDialogTu.getVisibility() == View.VISIBLE)
                                    tu = Double.parseDouble(mDialogTu.getText().toString());
                                else
                                    tu = nepostoji;

                                if (mDialogD.getVisibility() == View.VISIBLE)
                                    D = Double.parseDouble(mDialogD.getText().toString());
                                else
                                    D = nepostoji;

                                if (mDialogd.getVisibility() == View.VISIBLE)
                                    d = Double.parseDouble(mDialogd.getText().toString());
                                else
                                    d = nepostoji;

                                PODACI_TipStuba novStub = new PODACI_TipStuba(
                                    matStuba,
                                    tipStuba,
                                    novnaziv,
                                    Double.parseDouble(mDialogL.getText().toString() ),
                                    tu, D/100, d/100,
                                    nepostoji, true
                                );
                                if (PODACI_ListaTipovaStubova.getSingltonStubova(act).addTipStuba(novStub))
                                    prikaziToast(act, "<b>"+novStub.getOznaka() + "</b> je dodat u bazu!");
                                else
                                    prikaziWarnToast(act, "<b>"+novStub.getOznaka() + "</b> - GRESKA nije dodato u bazu!");
                            }
                        }

                        ad.dismiss();
                    }
                });

        ad.show();
        ad.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        ad.getWindow().setBackgroundDrawable(getPozadina(act));

        return ad;
    }


////////////////////////////////////////////////////////////////////////
    private static PODACI_TipKonzole konzola;
    private static double h12,h23,d1,d2,d3;
    private static StaticPodaci.Raspored_Konzola vrstaKonzole;
    static EditText mDialogH12,mDialogH23,mDialogD1,mDialogD2,mDialogD3;
    public static AlertDialog getDialog_konzola(final Activity activity){
        act=activity;

        konzola = getSingltonKonzola(activity).getTipKonzoleIzListe(0);

        final AlertDialog ad = new AlertDialog.Builder(activity).create();
        ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
        ad.setTitle(mojHtmlString("Baza konzola:"));
        LayoutInflater inflater = activity.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_dimenzije_konzola, null);
        ad.setView(v);

        final Spinner mSpinnerOznakaElementa     = (Spinner)v.findViewById( R.id.dialog_spiner );

        ImageButton deleteButton = (ImageButton)v.findViewById( R.id.dialog_delete );
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = mSpinnerOznakaElementa.getSelectedItem().toString();
                konzola =    getSingltonKonzola(act).getTipKonzoleIzListe(s);
                if (konzola.getIzmenljivo()) {
                    String pomocniString = konzola.getOznaka();
                    if (getSingltonKonzola(act).deleteTipKonzole(konzola)) {
                        prikaziToast(act, "<b>"+pomocniString + " </b>je izbrisan iz baze!");
                        TabelaNaprezanjaStuba_single_tab.spinnerControl(
                                act,
                                mSpinnerOznakaElementa,
                                getSingltonKonzola(act).getNizTipovaKonzola(),
                                getSingltonKonzola(act).getTipKonzoleIzListe(0).getOznaka());

                    }
                    else
                        prikaziWarnToast(act, pomocniString + " - GRESKA nije moguće brisanje iz baze!");
                }else
                    prikaziToast(act,"Nije dozvoljeno brisanje osnovnog tipa!");
            }
        });

        mDialogH12 = (EditText) v.findViewById(R.id.napr_stuba_dialog_h12);
        mDialogH23 = (EditText) v.findViewById(R.id.napr_stuba_dialog_h23);
        mDialogD1  = (EditText) v.findViewById(R.id.napr_stuba_dialog_d1);
        mDialogD2  = (EditText) v.findViewById(R.id.napr_stuba_dialog_d2);
        mDialogD3  = (EditText) v.findViewById(R.id.napr_stuba_dialog_d3);
        final ImageView mImageView = (ImageView) v.findViewById(R.id.napr_stuba_dialog_slika);

        mDialogH12 .setText(String.format(Locale.US, "%.0f",konzola.getH12()));
        mDialogH23.setText(String.format( Locale.US, "%.0f",konzola.getH23()));
        mDialogD1 .setText(String.format( Locale.US, "%.0f",konzola.getD1()));
        mDialogD2 .setText(String.format( Locale.US, "%.0f",konzola.getD2()));
        mDialogD3 .setText(String.format( Locale.US, "%.0f",konzola.getD3()));

        promeniSlikuKonzole(mImageView, konzola.getH12(), konzola.getH23());

        final List<View> lista = new ArrayList<>();
        lista.add(mDialogH12);
        lista.add(mDialogH23);
        lista.add(mDialogD1 );
        lista.add(mDialogD2 );
        lista.add(mDialogD3 );
        viewEnableTweak(lista, false);
        inicijalizujDialogListenere_konzola(lista, mImageView);


        TabelaNaprezanjaStuba_single_tab.spinnerControl(activity, mSpinnerOznakaElementa,     getSingltonKonzola(activity).getNizTipovaKonzola(), konzola.getOznaka());
        mSpinnerOznakaElementa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String s = adapterView.getSelectedItem().toString();
                konzola =    getSingltonKonzola(activity).getTipKonzoleIzListe(s);
                mDialogH12.setText(String.format(Locale.US, "%.0f",konzola.getH12()));
                mDialogH23.setText(String.format(Locale.US, "%.0f",konzola.getH23()));
                mDialogD1 .setText(String.format(Locale.US, "%.0f",konzola.getD1()));
                mDialogD2 .setText(String.format(Locale.US, "%.0f",konzola.getD2()));
                mDialogD3 .setText(String.format(Locale.US, "%.0f",konzola.getD3()));
                promeniSlikuKonzole(mImageView, konzola.getH12(), konzola.getH23());
            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        final LinearLayout LL = (LinearLayout) v.findViewById(R.id.dialog_LL_delete_spiner);
        final EditText mNovTip = (EditText) v.findViewById(R.id.nov_tip_naziv);
        mNovTip.setVisibility(View.GONE);
        final Button novTip = (Button) v.findViewById(R.id.nov_tip);
        novTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LL.setVisibility(View.GONE);
                novTip.setVisibility(View.GONE);
                mNovTip.setVisibility(View.VISIBLE);
                viewEnableTweak(lista,true);
            }
        });

        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (mNovTip.getVisibility() == View.VISIBLE ){

                            String novnaziv = mNovTip.getText().toString();
                            getDimenz();

                            if ( nazivElementaJeOK (getSingltonKonzola(activity).getNizTipovaKonzola(),novnaziv) ){
                                PODACI_TipKonzole novaKonzola = new PODACI_TipKonzole(
                                        StaticPodaci.MaterijalStuba.betonski,
                                        novnaziv,
                                        h12,
                                        h23,
                                        d1 ,
                                        d2 ,
                                        d3 ,
                                        true
                                );
                                if (PODACI_ListaTipovaKonzola.getSingltonKonzola(act).addTipKonzole(novaKonzola))
                                    prikaziToast(act, "<b>"+novaKonzola.getOznaka() + "</b> je dodat u bazu!");
                                else
                                    prikaziWarnToast(act, "<b>"+novaKonzola.getOznaka() + "</b> - GRESKA nije dodato u bazu!");
                            }
                        }

                        ad.dismiss();
                    }
                });

        ad.show();
        ad.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        ad.getWindow().setBackgroundDrawable(getPozadina(act));

        return ad;
    }
    private static void promeniSlikuKonzole(ImageView iw, double h12, double h23){
        if (h12==0) {
            vrstaKonzole = StaticPodaci.Raspored_Konzola.horizontalno;
            iw.setImageResource(R.drawable.slika_konzola_horiz);
            mDialogD1.setVisibility(View.INVISIBLE);
            mDialogD3.setVisibility(View.INVISIBLE);
            mDialogH23.setVisibility(View.INVISIBLE);
        }else if (h23==0) {
            vrstaKonzole = StaticPodaci.Raspored_Konzola.trougao;
            iw.setImageResource(R.drawable.slika_konzola_trougao);
            mDialogD1.setVisibility(View.INVISIBLE);
            mDialogD3.setVisibility(View.INVISIBLE);
            mDialogH23.setVisibility(View.VISIBLE);
        }else{
            vrstaKonzole = StaticPodaci.Raspored_Konzola.koso;
            iw.setImageResource(R.drawable.slika_konzola_koso);
            mDialogD1.setVisibility (View.VISIBLE);
            mDialogD3.setVisibility (View.VISIBLE);
            mDialogH23.setVisibility(View.VISIBLE);
        }
    }
    private static void inicijalizujDialogListenere_konzola(final List lista, final ImageView iw){
        // koristimo radi odredjivanja rasporeda u koznoli
        final TextWatcher dialogTextWatcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int start, int before,int count) {
                double h12;
                double h23;
                try {
                    h12 = Double.parseDouble( mDialogH12.getText().toString() );
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    h12= 0;
                }
                try {
                    h23 = Double.parseDouble( mDialogH23.getText().toString() );
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    h23 = 0;
                }
                promeniSlikuKonzole(iw, h12, h23);
            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void afterTextChanged(Editable s) {}
        };

        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) instanceof EditText)
                ((EditText) lista.get(i)).addTextChangedListener(dialogTextWatcher);
        }
    }
    private static void getDimenz(){
        try {
            h12 = Double.parseDouble( mDialogH12.getText().toString() );
        } catch (NumberFormatException e) {
            e.printStackTrace();
            h12= 0;
        }
        try {
            h23 = Double.parseDouble( mDialogH23.getText().toString() );
        } catch (NumberFormatException e) {
            e.printStackTrace();
            h23 = 0;
        }
        try {
            d1 = Double.parseDouble( mDialogD1.getText().toString() );
        } catch (NumberFormatException e) {
            e.printStackTrace();
            d1= 0;
        }
        try {
            d2 = Double.parseDouble( mDialogD2.getText().toString() );
        } catch (NumberFormatException e) {
            e.printStackTrace();
            d2 = 0;
        }
        try {
            d3 = Double.parseDouble( mDialogD3.getText().toString() );
        } catch (NumberFormatException e) {
            e.printStackTrace();
            d3= 0;
        }

        if ( ! vrstaKonzole . equals( StaticPodaci.Raspored_Konzola.koso)){
            d1 = 0;
            d3 = d2;
        }
    }


////////////////////////////////////////////////////////////////////////
    private static PODACI_TipIzolatora izolator;
    private static StaticPodaci.NaponskiNivo naponskiNivo;
    public static AlertDialog getDialog_izolatora(final Activity activity){
        act=activity;

        izolator = getSingltonIzolatora(activity).getTipIzolatoraIzListe(0);
        naponskiNivo = izolator.getNapon();

        final AlertDialog ad = new AlertDialog.Builder(activity).create();
        ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
        ad.setTitle(mojHtmlString("Baza izolatora:"));
        LayoutInflater inflater = activity.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_izolator, null);
        ad.setView(v);


        final Spinner mSpinnerOznakaIzolatora     = (Spinner)v.findViewById( R.id.dialog_spiner );

        ImageButton deleteButton = (ImageButton)v.findViewById( R.id.dialog_delete );
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = mSpinnerOznakaIzolatora.getSelectedItem().toString();
                izolator =    getSingltonIzolatora(act).getTipIzolatoraIzListe(s);
                if (izolator.getIzmenljivo()) {
                    String pomocniString = izolator.getOznaka();
                    if (getSingltonIzolatora(act).deleteTipIzolatora(izolator)) {
                        prikaziToast(act, "<b>"+pomocniString + " </b>je izbrisan iz baze!");
                        TabelaNaprezanjaStuba_single_tab.spinnerControl(
                                act,
                                mSpinnerOznakaIzolatora,
                                getSingltonIzolatora(act).getNizTipovaIzolatora(),
                                getSingltonIzolatora(act).getTipIzolatoraIzListe(0).getOznaka());

                    }
                    else
                        prikaziWarnToast(act, pomocniString + " - GRESKA nije moguće brisanje iz baze!");
                }else
                    prikaziToast(act,"Nije dozvoljeno brisanje osnovnog tipa!");
            }
        });

        final Spinner mNaponSpiner = (Spinner)v.findViewById( R.id.dialog_napon_spiner );
        final EditText mDialogDuzina = (EditText) v.findViewById(R.id.dialog_izolator_duzina);
        final EditText mDialogTezina = (EditText) v.findViewById(R.id.dialog_izolator_tezina);
        final List<View> lista = new ArrayList<>();
        lista.add(mDialogDuzina);
        lista.add(mDialogTezina);
        lista.add(mNaponSpiner);
        viewEnableTweak(lista, false);

        TabelaNaprezanjaStuba_single_tab.spinnerControl(activity, mSpinnerOznakaIzolatora,     getSingltonIzolatora(activity).getNizTipovaIzolatora(), izolator.getOznaka());
        mSpinnerOznakaIzolatora.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String s = adapterView.getSelectedItem().toString();
                izolator =    getSingltonIzolatora(activity).getTipIzolatoraIzListe(s);
                mDialogDuzina.setText(String.format(Locale.US, "%.2f",izolator.getDuzina()));
                mDialogTezina.setText(String.format(Locale.US, "%.2f",izolator.getTezina()));

            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });


        TabelaNaprezanjaStuba_single_tab.spinnerControl(act, mNaponSpiner,     StaticPodaci.getNizNaponskiNivo(), izolator.getNapon());
        mNaponSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                naponskiNivo = (StaticPodaci.NaponskiNivo) adapterView.getSelectedItem();
            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });


        final LinearLayout LL = (LinearLayout) v.findViewById(R.id.dialog_LL_delete_spiner);
        final EditText mNovTip = (EditText) v.findViewById(R.id.nov_tip_naziv);
        mNovTip.setVisibility(View.GONE);
        final Button novTip = (Button) v.findViewById(R.id.nov_tip);
        novTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LL.setVisibility(View.GONE);
                novTip.setVisibility(View.GONE);
                mNovTip.setVisibility(View.VISIBLE);
                viewEnableTweak(lista,true);
            }
        });

        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (mNovTip.getVisibility() == View.VISIBLE ){

                            String novnaziv = mNovTip.getText().toString();

                            if ( nazivElementaJeOK (getSingltonIzolatora(activity).getNizTipovaIzolatora(),novnaziv) ){
                                PODACI_TipIzolatora novIzolator = new PODACI_TipIzolatora(
                                        StaticPodaci.MaterijalStuba.betonski.toString(),
                                        novnaziv,
                                        naponskiNivo,
                                        Double.parseDouble(mDialogDuzina.getText().toString() ),
                                        Double.parseDouble(mDialogTezina.getText().toString()),
                                        true
                                );
                                if (getSingltonIzolatora(act).addTipIzolatora(novIzolator))
                                    prikaziToast(act, "<b>"+novIzolator.getOznaka() + "</b> je dodat u bazu!");
                                else
                                    prikaziWarnToast(act, "<b>"+novIzolator.getOznaka() + "</b> - GRESKA nije dodato u bazu!");
                            }
                        }

                        ad.dismiss();
                    }
                });

                ad.show();

        ad.show();
        ad.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        ad.getWindow().setBackgroundDrawable(getPozadina(act));

        return ad;

    }


////////////////////////////////////////////////////////////////////////
    private static void viewEnableTweak(List<View> listaET, boolean bool){
        for (View et : listaET) {
            et.setEnabled(bool);
        }
    }

    private static boolean nazivElementaJeOK(String[] postojeceOznake, String novaOznaka){
        if (novaOznaka == null){
            // ako je null odmah vraca vrednost i ne izvrsava dalje (moguc nullExp)
            prikaziWarnToast(act, "GRESKA - unesite naziv elementa!");
            return false;
        }
        else if (novaOznaka.length()<1){
            prikaziWarnToast(act, "GRESKA - unesite naziv elementa!");
            return false;
        }
        else if (elamentPripadaNizu(postojeceOznake, novaOznaka)){
            prikaziWarnToast(act, "GRESKA - element sa nazivom <b>"+novaOznaka+"</b> se već nalazi u bazi!");
            return false;
        }
        return true;
    }


}



//    private static void inicijalizujDialogListenere(){
//
//        TextWatcher dialogTextWatcher = new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence cs, int start, int before,int count) {
//                 if (BuildConfig.DEBUG)Log.e("TAGdialogElem", "cs="+ cs);
//                String s = cs.toString();
//                 if (BuildConfig.DEBUG)Log.e("TAGdialogElem", "s="+ s);
//                s = s.replace('.', StaticPodaci.getDecimalSeparator());
//                 if (BuildConfig.DEBUG)Log.e("TAGdialogElem", "s="+ s);
//                s = s.replace(',', StaticPodaci.getDecimalSeparator());
//                 if (BuildConfig.DEBUG)Log.e("TAGdialogElem", "s="+ s);
//                double pomocnaVar = 0;
//                if (mDialogL.getText().hashCode() == cs.hashCode()){
//                    try {
//                        pomocnaVar = Double.parseDouble(s.toString());
//                         if (BuildConfig.DEBUG)Log.e("TAGdialogElem", "pomocnaVar="+ pomocnaVar);
//                    } catch (NumberFormatException e) {}
//
//                    if (pomocnaVar>=1 && pomocnaVar<=50)
//                        Ln=pomocnaVar;
//                }
//                else if (mDialogTu.getText().hashCode() == cs.hashCode()){
//                    try {
//                        pomocnaVar = Double.parseDouble(s.toString());
//                    } catch (NumberFormatException e) {}
//
//                    if (pomocnaVar>=0.1 && pomocnaVar<10)
//                        tu=pomocnaVar;
//                }
//                else if (mDialogD.getText().hashCode() == cs.hashCode()){
//                    try {
//                        pomocnaVar = Double.parseDouble(s.toString());
//                    } catch (NumberFormatException e) {}
//
//                    if (pomocnaVar>=10 && pomocnaVar<=99)
//                        D=pomocnaVar*0.01;
//                }
//                else if (mDialogd.getText().hashCode() == cs.hashCode()){
//                    try {
//                        pomocnaVar = Double.parseDouble(s.toString());
//                    } catch (NumberFormatException e) {}
//
//                    if (pomocnaVar>=5 && pomocnaVar<=99)
//                        d=pomocnaVar*0.01;
//                }
//            }
//            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
//            @Override public void afterTextChanged(Editable s) {}
//        };
//
//        mDialogL .addTextChangedListener(dialogTextWatcher);
//        mDialogTu.addTextChangedListener(dialogTextWatcher);
//        mDialogD .addTextChangedListener(dialogTextWatcher);
//        mDialogd .addTextChangedListener(dialogTextWatcher);
//
//    }