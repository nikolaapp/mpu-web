package nsp.mpu._5_sigurnosne_visine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;

import static android.graphics.Paint.Align.CENTER;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Activity.sig_visina;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Fragment.linearnaInterpolacija;

@Obfuscate
public class CrtanjeFunkcija extends View {

    private Paint paint_funkcija = new Paint();
    private Paint paint_funkcija_Wycisk = new Paint();
    private Paint paint_stub = new Paint();
    private Paint paint_tlo = new Paint();
    private Paint paint_ordinata = new Paint();

    private final static int margina_razmere = 20; // pomocu nje ostavljamo prazan prostor gore i desno
    private final static int margina_pomeraja = margina_razmere/2; // pomeramo sliku gore i desno (ostavljamo prazan prostor dole i levo)

    //static da bi ih koristili i za konstuktore koji sami ne kreiraju ove varijable
    // (nikad lancanice nece biti crtane a da prethodno nije uneta trase i objekti
    private static float [] nizStacionaza_tlo_X, nizKota_tlo_Y,
                            nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY;
    private static List<float[]> listaObjekti_X, listaObjekti_Y;
    private static List<float[]> listaObjekti_relX, listaObjekti_relY;

    private static int mSlikaSirina, mSlikaVisina;
    private static float razmeraX, razmeraY;
    private static Bitmap bitmap;

    private Canvas mCanvas;

    private float [] nizStacionaza_STUB_X, nizKota_STUBiUZE_Y,
                     nizStacionaza_lancanica_X, nizKota_lancanica_Y;

    private float [] nizStacionaza_STUB_relativnoX, nizKota_STUBiUZE_Y_relativno,
                     nizStacionaza_lancanica_X_rel, nizKota_lancanica_Y_rel;

    private List<float[]> listaWycisk_stacionaze,listaWycisk_kote;

    // za crtanje ordinate
    private  Bitmap pomBitmap;

    private float xVertikala=0;

    private boolean prikaziVisine;

    private float Y_osa_max, Y_osa_min;
    private int indexTlo = 0;
    //private int indexObjekatTla = 0;
    private final int brojAnimacijaZaObjekatTla=10;
    private int index_brojAnimacijaZaObjekatTla = brojAnimacijaZaObjekatTla;
    private int indexStub = 0;
    private int indexLan= 0;

    private boolean crtanjeSaAnimacijom;

    public static boolean omogucenDragZoom;

    public CrtanjeFunkcija(Context context, float[] nizStacinaza, float[] nizKotaTlo,
                           List listaObjekti_X, List listaObjekti_Y,
                           int mSlikaSirina, int mSlikaVisina) {
        // crtanje samo trase, bez stubova i ugiba --- ovo se uvek prvo poziva
        super(context);
        paint_tlo.setColor(Color.BLACK);
        paint_tlo.setStrokeWidth(5);
        paint_tlo.setStrokeCap(Paint.Cap.ROUND);
        paint_tlo.setAntiAlias(true);


        nizStacionaza_tlo_X = nizStacinaza;
        nizKota_tlo_Y = nizKotaTlo;

        /** @#$ 1*/
        //ovo se poziva samo nakon odabira trase, tako da lista nije null nego prazna
        //to znaci da listaObjekti_X.size()>0 moze da se koristi
        CrtanjeFunkcija.listaObjekti_X = listaObjekti_X;
        CrtanjeFunkcija.listaObjekti_Y = listaObjekti_Y;
        listaWycisk_stacionaze = new ArrayList<>(); /** @#$ 5*/
        listaWycisk_kote = new ArrayList<>();

        prikaziVisine = false;
        crtanjeSaAnimacijom = true;
        omogucenDragZoom = true;

        /********
         od ver7.5
         */
        CrtanjeFunkcija.mSlikaSirina = mSlikaSirina;
        CrtanjeFunkcija.mSlikaVisina = mSlikaVisina;
        bitmap = Bitmap.createBitmap(mSlikaSirina, mSlikaVisina,
                Bitmap.Config.ARGB_8888); // ARGB da bi preko PNG imao transparentnu (providnu) pozadinu

        mCanvas = new Canvas(bitmap);
        mCanvas.drawColor(Color.TRANSPARENT); // ako se cuva onda mora u PNG


        izracunajRazmere();

        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
    }



    public CrtanjeFunkcija(Context context,
                           float xVertikala) {
        // crta samo ordinatu za prikaz sig.visina
        super(context);
        paint_ordinata.setColor(Color.BLACK);
        paint_ordinata.setStrokeWidth(1);
        this.xVertikala = xVertikala;
        prikaziVisine=true;
        crtanjeSaAnimacijom = false;
        omogucenDragZoom = false;

        /********
         od ver7.5
         */
        // kada prikazuje sig.visine onda koristi kopiju postojeceg bitmap-a preko koga crta ordinatu Y
        // tako da nema ponovnog proracuna i crtanja ugiba
        pomBitmap = bitmap.copy(bitmap.getConfig(),true);
        mCanvas = new Canvas(pomBitmap);

        mScaleDetector = null;
    }


    private List listaOznakaStubova;
    Paint paint_oznakaStuba = new Paint();
    public CrtanjeFunkcija(Context context,
                           float[] nizStacionazaStub, float[] nizKotaStub,
                           float[] nizStacionaza_lancanica_X, float[] nizKota_lancanica_Y,
                           List listaWycisk_stacionaze, List listaWycisk_kote,
                           boolean crtanjeSaAnimacijom, List listaOznakaStubova
                           ) {
        // crta trasu, objekte, stubove i lancanice
        super(context);
        paint_funkcija.setColor(Color.BLUE);
        paint_funkcija.setStrokeWidth(5);
        paint_funkcija_Wycisk.setColor(Color.RED);
        paint_funkcija_Wycisk.setStrokeWidth(5);
        paint_stub.setColor(Color.BLACK);
        paint_stub.setStrokeWidth(5);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            paint_tlo.setColor(getResources().getColor(R.color.tlo_braon,null));
        else paint_tlo.setColor(getResources().getColor(R.color.tlo_braon));
        paint_tlo.setStrokeWidth(5);

        paint_funkcija.setAntiAlias(true);
        paint_funkcija_Wycisk.setAntiAlias(true);
        paint_stub.setAntiAlias(true);
        paint_tlo.setAntiAlias(true);

        paint_tlo.setStrokeCap(Paint.Cap.ROUND);
        paint_stub.setStrokeCap(Paint.Cap.ROUND);

        paint_oznakaStuba.setAntiAlias(true);
        paint_oznakaStuba.setTextSize(18);
        paint_oznakaStuba.setTextAlign(CENTER);
        paint_oznakaStuba.setColor(Color.GRAY);
        paint_oznakaStuba.setStyle(Paint.Style.STROKE);

        // ne mora .clone jer ne menjamo ove nizove (a i ne sme jer ako je niz null onda sledi exception - animiranje trase)
        this.nizStacionaza_STUB_X = nizStacionazaStub;
        this.nizKota_STUBiUZE_Y = nizKotaStub;
        this.nizStacionaza_lancanica_X=nizStacionaza_lancanica_X;
        this.nizKota_lancanica_Y=nizKota_lancanica_Y;
        this.listaWycisk_stacionaze=kopiranjeArrayList( listaWycisk_stacionaze );
        this.listaWycisk_kote=kopiranjeArrayList( listaWycisk_kote); // mora kopiranje jer menjamo ove liste
        this.listaOznakaStubova=listaOznakaStubova; // NE mora kopiranje jer NE menjamo ovu listu


        this.crtanjeSaAnimacijom=crtanjeSaAnimacijom;
        prikaziVisine=false;
        omogucenDragZoom = true;
        //////// i ovde ih kreiramo jer bi inace crtali ugibe preko slike iz drugog kostruktora

        if (crtanjeSaAnimacijom) {
            bitmap = Bitmap.createBitmap(mSlikaSirina, mSlikaVisina,
                    Bitmap.Config.ARGB_8888); // ARGB da bi preko PNG imao transparentnu (providnu) pozadinu
        }
        mCanvas = new Canvas(bitmap);
        mCanvas.drawColor(Color.TRANSPARENT); // ako se cuva onda mora u PNG

        izracunajRazmere();

        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
    }


    @Override
    public void onDraw(Canvas canvas) {
             if (BuildConfig.DEBUG)Log.e("TAG30", "canvas.getWidth() " + canvas.getWidth()+" canvas.getHeight() " + canvas.getHeight());
             if (BuildConfig.DEBUG)Log.e("TAG30", "mSlikaSirina " + mSlikaSirina+" mSlikaVisina " + mSlikaVisina);
             if (BuildConfig.DEBUG)Log.e("TAG33", "razmeraX " + razmeraX+"  razmeraY " + razmeraY);

        if (prikaziVisine){
            ////////// crtanje samo vertikale | ordinateY/////////
            crtanjeLinije_2tacke(xVertikala * razmeraX, mSlikaVisina,
                    xVertikala * razmeraX, 0, paint_ordinata);

            canvas.drawBitmap(pomBitmap, 0, 0, null); // crta na kopiran/pomocni bitmap
            return;
        }

        if (crtanjeSaAnimacijom) {

             if (BuildConfig.DEBUG)Log.e("TAG34", "nizStacionaza_STUB_X " + nizStacionaza_STUB_X);
             if (BuildConfig.DEBUG)Log.e("TAG34", "nizKota_STUBiUZE_Y " + nizKota_STUBiUZE_Y);
             if (BuildConfig.DEBUG)Log.e("TAG34", "nizStacionaza_lancanica_X " + nizStacionaza_lancanica_X);
             if (BuildConfig.DEBUG)Log.e("TAG34", "nizKota_lancanica_Y " + nizKota_lancanica_Y);
             if (BuildConfig.DEBUG)Log.e("TAG34", "listaWycisk_stacionaze " + listaWycisk_stacionaze);
             if (BuildConfig.DEBUG)Log.e("TAG34", "listaWycisk_kote " + listaWycisk_kote);
             if (BuildConfig.DEBUG)Log.e("TAG34", "listaObjekti_X " + listaObjekti_X);
             if (BuildConfig.DEBUG)Log.e("TAG34", "listaObjekti_Y " + listaObjekti_Y);

            if (indexTlo < (nizStacionaza_tlo_relativnoX.length - 1)) {
                if (nizStacionaza_tlo_X.length<500){
                crtanjeLinije_2tacke(nizStacionaza_tlo_relativnoX[indexTlo], nizKota_tlo_relativnoY[indexTlo],
                        nizStacionaza_tlo_relativnoX[indexTlo + 1], nizKota_tlo_relativnoY[indexTlo + 1],
                        paint_tlo);
                indexTlo++;
                postInvalidateDelayed(2000/nizStacionaza_tlo_relativnoX.length);
                 if (BuildConfig.DEBUG)Log.e("TAG34", "indexTlo " + indexTlo+"  nizStacionaza_tlo_relativnoX " + nizStacionaza_tlo_relativnoX[indexTlo]);
                }
                else {
                    for (int i = 0; i < 50 && indexTlo+i < (nizStacionaza_tlo_relativnoX.length - 1) ; i++) {
                        crtanjeLinije_2tacke(nizStacionaza_tlo_relativnoX[indexTlo+i], nizKota_tlo_relativnoY[indexTlo+i],
                                nizStacionaza_tlo_relativnoX[indexTlo+i + 1], nizKota_tlo_relativnoY[indexTlo+i + 1],
                                paint_tlo);
                    }
                    indexTlo+=50;
                    postInvalidateDelayed(1+  2000/ (nizStacionaza_tlo_relativnoX.length/50) );
                }
            }



            else if (listaObjekti_X.size()>0 && index_brojAnimacijaZaObjekatTla >=0){ /// ne sme relativo // TODO: 16.11.17 koment

                    for (int niz = 0; niz < listaObjekti_relX.size(); niz++) {
                        crtajObjekatIspodTrase(listaObjekti_relX.get(niz),listaObjekti_relY.get(niz), paint_tlo);
                    }

                    index_brojAnimacijaZaObjekatTla--;
                    postInvalidateDelayed(2000/(brojAnimacijaZaObjekatTla+1)); // sve crta za 2 sek
                     if (BuildConfig.DEBUG)Log.e("TAG35", "index_brojAnimacijaZaObjekatTla " + index_brojAnimacijaZaObjekatTla+"  listaObjekti_relX.size() " + listaObjekti_relX.size());

            }


            else if (nizStacionaza_STUB_relativnoX != null &&
                    indexStub < nizStacionaza_STUB_relativnoX.length) {

                float stubnoMesto_Xrel_koordinata = nizStacionaza_STUB_relativnoX[indexStub];
                float stubnoMesto_Yrel_koordinata = linearnaInterpolacija(
                        stubnoMesto_Xrel_koordinata, nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY);

                crtanjeLinije_2tacke(stubnoMesto_Xrel_koordinata, stubnoMesto_Yrel_koordinata,
                        stubnoMesto_Xrel_koordinata, nizKota_STUBiUZE_Y_relativno[indexStub],
                        paint_stub);

                Rect rect = new Rect();
                String tekst = listaOznakaStubova.get(indexStub).toString();
                paint_oznakaStuba.getTextBounds(tekst,0,tekst.length(), rect);

                paint_oznakaStuba.setStyle(Paint.Style.FILL);
                mCanvas.drawText(tekst,
                        stubnoMesto_Xrel_koordinata + margina_pomeraja,
                        mSlikaVisina - (nizKota_STUBiUZE_Y_relativno[indexStub] + 1.5f*razmeraY) - margina_pomeraja,
                        paint_oznakaStuba);


                indexStub++;
                postInvalidateDelayed(250);
                 if (BuildConfig.DEBUG)Log.e("TAG34", "indexStub " + indexStub+"  stubnoMesto_Xrel_koordinata " + stubnoMesto_Xrel_koordinata);
            }

            else if (nizStacionaza_lancanica_X_rel != null
                    && indexLan < nizStacionaza_lancanica_X_rel.length-1) {
                 if (BuildConfig.DEBUG)Log.e("TAG34", "lancanica_crtanje_tacaka(); ");
                lancanica_crtanje_tacaka();
            }
            else if (listaWycisk_stacionaze != null) {
                //// za slucaj da ponovo pokrecemo animaciju
                lancanicaWycisk_crtanje_tacaka();
                 if (BuildConfig.DEBUG)Log.e("TAG34", "lancanicaWycisk_crtanje_tacaka(); ");
            }

             if (BuildConfig.DEBUG)Log.e("TAG34", "drawBitmap");

            canvas.translate(mPosX, mPosY);
            canvas.scale(mScaleFactor, mScaleFactor);
            canvas.drawBitmap(bitmap, 0, 0, null);

        } else{
            // znaci da bitmap vec postoji (vec je bio prikazivan) pa nam ne treba opet animacija
            if (listaWycisk_stacionaze != null) //// FIXME: 11.11.17 ali kada obrisemo lancanicu ona ce i dalje biti prikazana
                lancanicaWycisk_crtanje_tacaka();
             if (BuildConfig.DEBUG)Log.e("TAG34", "else ");
            canvas.translate(mPosX, mPosY);
            canvas.scale(mScaleFactor, mScaleFactor);
            canvas.drawBitmap(bitmap, 0, 0, null);
        }

    }

    private void izracunajRazmere(){
/////////////////////////////// razmera za X osu/////////////////////////////
        razmeraX = (mSlikaSirina - margina_razmere) / nizStacionaza_tlo_X[nizStacionaza_tlo_X.length - 1];
         if (BuildConfig.DEBUG)Log.e("TAG5", "razmeraX = " + razmeraX);

/////////////////////////////// razmera za Y osu/////////////////////////////
        Y_osa_max = nizKota_tlo_Y[0];
        Y_osa_min = nizKota_tlo_Y[0];

        for (int i = 1; i < nizKota_tlo_Y.length; i++) {
            if (nizKota_tlo_Y[i] > Y_osa_max) {
                Y_osa_max = nizKota_tlo_Y[i];
                 if (BuildConfig.DEBUG)Log.e("TAG8", "   Y_osa_max = " + Y_osa_max);
            } else if (nizKota_tlo_Y[i] < Y_osa_min) {
                Y_osa_min = nizKota_tlo_Y[i];
                 if (BuildConfig.DEBUG)Log.e("TAG8", "Y_osa_min = " + Y_osa_min);
            }
        }

             if (BuildConfig.DEBUG)Log.e("TAG34","izracunajRazmere listaObjekti_X = "+listaObjekti_X);
             if (BuildConfig.DEBUG)Log.e("TAG34","izracunajRazmere listaObjekti_Y = " +listaObjekti_Y);

        //// trazimo max visinu objekta/////////
        if (listaObjekti_Y!=null)
            for (int niz = 0; niz < listaObjekti_Y.size(); niz++)
                for (int i = 0; i < listaObjekti_Y.get(niz).length; i++)
                    if ((listaObjekti_Y.get(niz))[i] > Y_osa_max)
                        Y_osa_max = (listaObjekti_Y.get(niz))[i];


        //// trazimo max visinu uzeta(odnosno stuba - to je brze)/////////
        if (nizKota_STUBiUZE_Y!=null)
            for (int i = 0; i < nizKota_STUBiUZE_Y.length; i++)
                if (nizKota_STUBiUZE_Y[i] > Y_osa_max)
                    Y_osa_max = nizKota_STUBiUZE_Y[i] + 7; // stelovano zbog oznake stuba

        float ukupnaVisina = Y_osa_max - Y_osa_min;
        if (ukupnaVisina == 0) // ako je trasa ravna linija
            ukupnaVisina = 1;

        razmeraY = (mSlikaVisina - margina_razmere) / ukupnaVisina;
         if (BuildConfig.DEBUG)Log.e("TAG5", "razmeraY = " + razmeraY);

        prilagodiRazmeri();
    }
    
    private void prilagodiRazmeri(){
        nizStacionaza_tlo_relativnoX =  prilagodiRazmeri_X(nizStacionaza_tlo_X, razmeraX);
        nizKota_tlo_relativnoY =        prilagodiRazmeri_Y(nizKota_tlo_Y, razmeraY,Y_osa_min);

        listaObjekti_relX = new ArrayList<>();
        listaObjekti_relY = new ArrayList<>();
        for (int niz = 0; niz < listaObjekti_X.size(); niz++) {
            listaObjekti_relX.add( prilagodiRazmeri_X(listaObjekti_X.get(niz), razmeraX) );
            listaObjekti_relY.add( prilagodiRazmeri_Y(listaObjekti_Y.get(niz), razmeraY, Y_osa_min) );
        }

        if (nizStacionaza_STUB_X!=null) {
            nizStacionaza_STUB_relativnoX = prilagodiRazmeri_X(nizStacionaza_STUB_X, razmeraX);
            nizKota_STUBiUZE_Y_relativno =  prilagodiRazmeri_Y(nizKota_STUBiUZE_Y, razmeraY,Y_osa_min);

            nizStacionaza_lancanica_X_rel = prilagodiRazmeri_X(nizStacionaza_lancanica_X, razmeraX);
            nizKota_lancanica_Y_rel =       prilagodiRazmeri_Y(nizKota_lancanica_Y, razmeraY,Y_osa_min);

        }

        /** !@#5 ove liste nikad nisu null - mogu samo da budu prazne - size=0*/
        for (int niz = 0; niz < listaWycisk_stacionaze.size(); niz++) {
            listaWycisk_stacionaze. set(niz, prilagodiRazmeri_X(listaWycisk_stacionaze.get(niz), razmeraX) );
            listaWycisk_kote.       set(niz, prilagodiRazmeri_Y(listaWycisk_kote.get(niz), razmeraY,Y_osa_min));
        }
        
    }

    private void lancanica_crtanje_tacaka() {
        Paint paint_pomocno = new Paint();
        paint_pomocno.setStrokeWidth(2);
        paint_pomocno.setAntiAlias(true);
        paint_pomocno.setColor(paint_funkcija.getColor());
        int brojacIsprekidaneLinije=0;

        // crtamo [brojac] tacaka odjednom, jer je i pri postInvalidateDelayed(JEDNA mS) to isuvise sporo
        for (int brojac =0;indexLan < nizStacionaza_lancanica_X_rel.length-1 && brojac<100; brojac++, brojacIsprekidaneLinije++) {
            /*
                y_koord_tlo = ProveraSigVisina_Fragment.linearnaInterpolacija (
                        nizStacionaza_lancanica_X_rel[i] ,nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY );
                y_koord_lanc = ProveraSigVisina_Fragment.linearnaInterpolacija (
                        nizStacionaza_lancanica_X_rel[i] ,nizStacionaza_lancanica_X_rel, nizKota_lancanica_Y_rel );
                */
            crtanjeLinije_2tacke(nizStacionaza_lancanica_X_rel[indexLan],
                    nizKota_lancanica_Y_rel[indexLan],
                    nizStacionaza_lancanica_X_rel[indexLan+1],
                    nizKota_lancanica_Y_rel[indexLan+1],
                    paint_funkcija);

            if (brojacIsprekidaneLinije==75){
                if (paint_pomocno.getColor() == paint_funkcija.getColor()) {
                    paint_pomocno.setColor(Color.TRANSPARENT);
                    brojacIsprekidaneLinije=50;
                }else{
                    paint_pomocno.setColor(paint_funkcija.getColor());
                    brojacIsprekidaneLinije=0;
                }
            }
            crtanjeLinije_2tacke(nizStacionaza_lancanica_X_rel[indexLan],
                    nizKota_lancanica_Y_rel[indexLan]      - sig_visina*razmeraY,
                    nizStacionaza_lancanica_X_rel[indexLan+1],
                    nizKota_lancanica_Y_rel[indexLan+1]    - sig_visina*razmeraY,
                    paint_pomocno);
                /*
                 if (BuildConfig.DEBUG)Log.e("TAGsigVisine", "x_rel= "+ String.format("%.2f",nizStacionaza_lancanica_X_rel[i])+
                        "  x_stacionaza= "+ String.format("%.2f",nizStacionaza_lancanica_X_rel[i]/razmeraX)+
                        "  y_koord_tlo= "+ String.format("%.2f",y_koord_tlo)+
                        "  y_koord_lanc= "+ String.format("%.2f",y_koord_lanc)+
                        "  razlikaY= "+ String.format( "%.2f",(y_koord_lanc-y_koord_tlo)/razmeraY )  +
                        "  sig_visina= "+ String.format("%.2f",sig_visina)+
                        "  razlikaYREl= "+ String.format( "%.2f",(y_koord_lanc-y_koord_tlo))  +
                        "  sig_visinaREL= "+ String.format("%.2f",sig_visina*razmeraY)
                );
                */
            indexLan++;
        }

        if (indexLan < nizStacionaza_lancanica_X_rel.length-1 || listaWycisk_stacionaze != null)
            postInvalidateDelayed(1);

    }

    private void lancanicaWycisk_crtanje_tacaka() {
        for (int niz =0; niz< listaWycisk_stacionaze.size(); niz++) {
            int brojacIsprekidaneLinije=0;
            for (int i = 0; i < listaWycisk_stacionaze.get(niz).length-1; i++,brojacIsprekidaneLinije++) {
                float y_koord_tlo = linearnaInterpolacija (
                        listaWycisk_stacionaze.get(niz)[i] ,nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY );
                float y_koord_lanc = linearnaInterpolacija (
                        listaWycisk_stacionaze.get(niz)[i] ,listaWycisk_stacionaze.get(niz), listaWycisk_kote.get(niz) );
    
                /*if(y_koord_lanc-y_koord_tlo <= sig_visina*razmeraY)
                    color = paint_funkcija_prenisko.getColor();
                else
                    color = paint_funkcija.getColor();*/

                    /*
                     if (BuildConfig.DEBUG)Log.e("TAGsigVisine", "x_rel= "+ String.format("%.2f",nizStacionaza_lancanica_X_rel[i])+
                            "  x_stacionaza= "+ String.format("%.2f",nizStacionaza_lancanica_X_rel[i]/razmeraX)+
                            "  y_koord_tlo= "+ String.format("%.2f",y_koord_tlo)+
                            "  y_koord_lanc= "+ String.format("%.2f",y_koord_lanc)+
                            "  razlikaY= "+ String.format( "%.2f",(y_koord_lanc-y_koord_tlo)/razmeraY )  +
                            "  sig_visina= "+ String.format("%.2f",sig_visina)+
                            "  razlikaYREl= "+ String.format( "%.2f",(y_koord_lanc-y_koord_tlo))  +
                            "  sig_visinaREL= "+ String.format("%.2f",sig_visina*razmeraY)
                    );
                    */

                crtanjeLinije_2tacke(
                        listaWycisk_stacionaze.get(niz)[i], listaWycisk_kote.get(niz)[i],
                        listaWycisk_stacionaze.get(niz)[i+1], listaWycisk_kote.get(niz)[i+1],
                        paint_funkcija_Wycisk);
    
            }
        }
    }

    private void crtanjeLinije_2tacke(float x_stacionaza_tla1, float y_kota_tla1,
                                      float x_stacionaza_tla2, float y_kota_tla2,
                                      Paint boja) {
        float x_start = x_stacionaza_tla1;
        float y_start = mSlikaVisina-y_kota_tla1; // minus jer se pixeli broje od gore na dole
        float x_stop = x_stacionaza_tla2;
        float y_stop = mSlikaVisina-y_kota_tla2;

        mCanvas.drawLine(x_start+margina_pomeraja, y_start-margina_pomeraja,
                         x_stop +margina_pomeraja, y_stop -margina_pomeraja,   boja);

        // if (BuildConfig.DEBUG)Log.e("TAG7","CRTANJE nizStacionaza_STUB_relativnoX= " + Arrays.toString(nizStacionaza_STUB_relativnoX));
        // if (BuildConfig.DEBUG)Log.e("TAG7","crtanjeLinije_2tacke");

    }

    private void crtajObjekatIspodTrase(final float[] niz_X, final float[] niz_Y, final Paint boja) {
                                    //         if (BuildConfig.DEBUG)Log.e("TAG33","nizStacionaza_objekata_relX = " + Arrays.toString(niz_X.toArray()));
                                    //         if (BuildConfig.DEBUG)Log.e("TAG33","nizKota_objekata_relY = " + Arrays.toString(niz_Y.toArray()));
        Path path = new Path();
        Paint boja_pom = boja;
        int bojaOkvira = boja.getColor();
        boja_pom.setAntiAlias(true);
        boja_pom.setStyle(Paint.Style.FILL);
        boja_pom.setColor(Color.LTGRAY);

        ////// TODO: 15.11.17 radi samo ako je niz_X rastuci
        float x_start = niz_X[0];
        float y_start = linearnaInterpolacija (x_start ,nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY ); // minus jer se pixeli broje od gore na dole
        float dodatakZaAnimaciju = Math.abs(niz_Y[0]-y_start);

        path.moveTo(x_start+margina_pomeraja,mSlikaVisina-margina_pomeraja-y_start);

        float pomocnoY, x,y, y_tlo;
        for (int i = 0; i < niz_X.length; i++) {
            x=niz_X[i];
            y=niz_Y[i];
            pomocnoY = y - ( (float/**obavezno*/)index_brojAnimacijaZaObjekatTla / brojAnimacijaZaObjekatTla )*dodatakZaAnimaciju;
            y_tlo= linearnaInterpolacija(x ,nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY );
            if (pomocnoY < y_tlo)
                pomocnoY=y_tlo;

            path.lineTo( x+margina_pomeraja,mSlikaVisina-pomocnoY-margina_pomeraja);
        }

        float x_kraj = niz_X[niz_X.length-1];
        float y_kraj = linearnaInterpolacija (
                                    x_kraj ,nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY );

        path.lineTo(x_kraj + margina_pomeraja,mSlikaVisina-y_kraj-margina_pomeraja);

        path.close();

        mCanvas.drawPath(path,   boja_pom);
        boja_pom.setStyle(Paint.Style.STROKE);
        boja_pom.setStrokeWidth(3);
        boja_pom.setColor(bojaOkvira);
        mCanvas.drawPath(path,   boja_pom);

    }

    public static float[] prilagodiRazmeri_X(final float[] niz, float razmera) {
        float[] niz_pom = niz.clone(); // bez ovoga bi i argument niz[] bio promenjen
        for (int i = 0; i < niz.length; i++)
            niz_pom[i] = niz[i]*razmera;
        return niz_pom;
    }

    public static List<List<Float>> prilagodiRazmeri_X(final List<List<Float>> lista, float razmera) {
        List<List<Float>> novaLista = new ArrayList<>();
        List<Float> deoListe = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            deoListe.clear();
            for (int j = 0; j < lista.get(i).size(); j++) {
                deoListe.add( (lista.get(i)).get(j)  *  razmera  );
                // if (BuildConfig.DEBUG)Log.e("TAG34","i="+i+ " j="+ j);

            }
            novaLista.add(ProveraSigVisina_fragment_proracuni.kopiranjeListe(deoListe));
        }
        return novaLista;
    }

    public static List<List<Float>> prilagodiRazmeri_Y(final List<List<Float>> lista, float razmera, float Y_osa_min ) {
        List<List<Float>> novaLista = new ArrayList<>();
        List<Float> deoListe = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            deoListe.clear();
            for (int j = 0; j < lista.get(i).size(); j++)
                deoListe.add(  (lista.get(i)).get(j)  -  Y_osa_min  );
            novaLista.add(ProveraSigVisina_fragment_proracuni.kopiranjeListe(deoListe));
        }
        return prilagodiRazmeri_X(novaLista,razmera);
    }

    public static float[] prilagodiRazmeri_Y(final float[] niz, float razmera, float Y_osa_min) {
        // ovde imamo i umanjenje za Y_osa_min
        float[] niz_pom = niz.clone();
        for (int i = 0; i < niz_pom.length; i++)        // da na crtezu ne bi imali prazan
            niz_pom[i] = niz_pom[i] - Y_osa_min;        // prostor ispod trase
        niz_pom = prilagodiRazmeri_X(niz_pom, razmera);

        return niz_pom;
    }

    public static List<float[]> kopiranjeArrayList(final List<float[]> originalLista){
        if (originalLista==null) /** @#$ 5*/
            return new ArrayList<>();

        List<float[]> novaLista = new ArrayList<>();
        for (int i = 0; i < originalLista.size(); i++)
            novaLista.add( originalLista.get(i).clone() );
        return novaLista;
    }


/////////////////////////////////////////////////////////////////////////
    private ScaleGestureDetector mScaleDetector;
    private float mScaleFactor = 1.f;
    private float mPosX,mPosY;
    private float mLastTouchX, mLastTouchY;

    private static final int INVALID_POINTER_ID = -1;
    private int mActivePointerId = INVALID_POINTER_ID;


    int clickCount = 0;
    //variable for storing the time of first click
    long startTime;
    //variable for calculating the total time
    long duration;
    //constant for defining the time duration between the click that can be considered as double-tap
    static final int MAX_DURATION = 500;
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (omogucenDragZoom == false) {
             if (BuildConfig.DEBUG)Log.e("TAG35", "bez zoom/drag");
            return false;
        }
        // Let the ScaleGestureDetector inspect all events.
        mScaleDetector.onTouchEvent(ev);

        final int action = ev.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                final float x = ev.getX();
                final float y = ev.getY();

                mLastTouchX = x;
                mLastTouchY = y;
                mActivePointerId = ev.getPointerId(0);

                startTime = System.currentTimeMillis();
                clickCount++;

                break;
            }

            case MotionEvent.ACTION_MOVE: {
                final int pointerIndex = ev.findPointerIndex(mActivePointerId);
                final float x = ev.getX(pointerIndex);
                final float y = ev.getY(pointerIndex);

                // Only move if the ScaleGestureDetector isn't processing a gesture.
                if (!mScaleDetector.isInProgress()) {
                    final float dx = x - mLastTouchX;
                    final float dy = y - mLastTouchY;

                    mPosX += dx;
                    mPosY += dy;

                    invalidate();
                }

                mLastTouchX = x;
                mLastTouchY = y;

                break;
            }

            case MotionEvent.ACTION_UP: {
                mActivePointerId = INVALID_POINTER_ID;
                long time = System.currentTimeMillis() - startTime;
                duration=  duration + time;
                if(clickCount == 2)
                {
                    if(duration<= MAX_DURATION){
                        mScaleFactor=1;
                        mPosX=0; mPosY=0;
                        invalidate();
                    }
                    clickCount = 0;
                    duration = 0;
                    break;
                }
                break;
            }

            case MotionEvent.ACTION_CANCEL: {
                mActivePointerId = INVALID_POINTER_ID;
                break;
            }

            case MotionEvent.ACTION_POINTER_UP: {
                final int pointerIndex = (ev.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK)
                        >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                final int pointerId = ev.getPointerId(pointerIndex);
                if (pointerId == mActivePointerId) {
                    // This was our active pointer going up. Choose a new
                    // active pointer and adjust accordingly.
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mLastTouchX = ev.getX(newPointerIndex);
                    mLastTouchY = ev.getY(newPointerIndex);
                    mActivePointerId = ev.getPointerId(newPointerIndex);
                }
                break;
            }
        }

        return true;
    }
    @Obfuscate//import
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();

            // Don't let the object get too small or too large.
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 5.0f));

            invalidate();
            return true;
        }
    }
/////////////////////////////////////////////////////////////////////////


}




/*
    //crtanje funkcija preko drawPoint - nema niza nego se proracunata tacka odmah crta
    private void lancanica_crtanje_tacaka_drawPoint_(float x_start, float x_stop, float y_kotaVesanjaUze,
                                          float teme_lanc, float parametar) {

        float x_screen;   // x koordinate za prikaz na ekranu [0, canvas.getWidth()]
        float y_screen;   // y koordinate za prikaz na ekranu [0, canvas.getHeight()]
        float x;          // x koordinate za proracun lancanice koja ima teme u tacki: (int teme_lanc)
        float y;          // y = f(x)

        // podizananje lancanice do najvise tacke levog stuba (visine vesanja uzeta) u rasponu (po y-osi)
        float y_dodatak = y_kotaVesanjaUze - lancanica_vredY_u_tackiX(x_start - teme_lanc, parametar);

        // treba ispod za proveru sig.visina i crtanje lancanice razlicitim bojama
        int log1 = 0, log2=0;
        float X_rel, V_rel ;
        float sig_visina_rel = sig_visina*razmeraY;
        boolean sigVisineOk = true;
        float x_niskaVisina_start=0;
        float x_niskaVisina_stop=0;

        for (x_screen = x_start; x_screen <= x_stop; x_screen++) {
            x = x_screen - teme_lanc;
            y = lancanica_vredY_u_tackiX(x, parametar) + y_dodatak;

            if (y < 0)
                y_screen = mSlikaVisina;
            else
                y_screen = (mSlikaVisina - y); // jer je tacka (0,0) u gornjem levom uglu ekrana

            ///////// ako nije zadovoljena sig.visina onda se lanc. boji crveno ///////////////
            // ne koristimo visinaLancaniceUzadatojTacki sa relativnim jed --- jer se tada ne uporedjuje lepo
            // proracunata visina i sig_visina
            X_rel = x_screen/razmeraX;
            V_rel = visinaLancaniceUzadatojTacki(X_rel,parametarLancanice_rel,
                    nizStacionaza_tlo_relativnoX,nizKota_tlo_relativnoY,
                    nizStacionaza_STUB_relativnoX,nizKota_STUBiUZE_Y_relativno);



            if (V_rel < sig_visina_rel) {
                mCanvas.drawPoint(x_screen + margina_pomeraja, y_screen - margina_pomeraja, paint_funkcija_prenisko);
                if ((log1 % 15) == 0) {
                     if (BuildConfig.DEBUG)Log.e("TAG13", "!!!!!!!!!!!!!!!!!!!!!!!x_screen    = " + X_rel + "  visina lanc rel = " + V_rel + "   sig_visina_rel    = " + sig_visina_rel);
                }
                if (sigVisineOk == true)
                    x_niskaVisina_start = X_rel/razmeraX;
                sigVisineOk = false;

            }else {
                mCanvas.drawPoint(x_screen + margina_pomeraja, y_screen - margina_pomeraja, paint_funkcija);
                if ((log2 % 15) == 0) {
                     if (BuildConfig.DEBUG)Log.e("TAG13", "x_screen    = " + X_rel + "  visina lanc rel = " + V_rel + "   sig_visina_rel    = " + sig_visina_rel);
                }
                if (sigVisineOk == false)
                    x_niskaVisina_stop = X_rel/razmeraX;
                sigVisineOk = true;
            }

            log1++;
            log2++;
        }
        if (! (x_niskaVisina_start == x_niskaVisina_stop))
             if (BuildConfig.DEBUG)Log.e("TAGsigVisine", "x_niskaVisina_start= " + x_niskaVisina_start +
                           "  x_niskaVisina_stop= " + x_niskaVisina_stop);



    }

    private  int najblizeTemeLancanice(float x_rel){
        int i_manje = ProveraSigVisina_Fragment.indexSusednog_ManjegClanaNiza(x_rel, nizStacionaza_STUB_X);
        int i_vece = ProveraSigVisina_Fragment.indexSusednog_VecegClanaNiza(x_rel, nizStacionaza_STUB_X);

        if( Math.abs(x_rel- nizStacionaza_STUB_X[i_manje])  <=  Math.abs(x_rel- nizStacionaza_STUB_X[i_vece])) {
            return i_manje;
        }else
            return i_vece;
    }

    private static float prilagodiRazmeri_X(float var, float razmera) {
        return var * razmera;
    }

    private void stub_crtanje_tacaka(float x_pozicija_stuba, float y_pozicija_stuba) {
        float x_start = x_pozicija_stuba;
        float y_start = mSlikaVisina - y_pozicija_stuba;
        float x_stop = x_start;
        float y_stop = y_start - visinaStuba; // minus jer se pixeli broje od gore na dole

        mCanvas.drawLine(x_start, y_start, x_stop, y_stop, paint_stub);
    }

    private static double naKradrat(double x) {
        return x * x;
    }


    private void proveraSigVisina(){
        //provera sigurnosnih visina preko relativni vrednosti

        float proveraXkoord = xVertikala * razmeraX;
        float y_koord_tlo = ProveraSigVisina_Fragment.linearnaInterpolacija (
                proveraXkoord ,nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY );
        float y_koord_lanc = ProveraSigVisina_Fragment.linearnaInterpolacija (
                proveraXkoord ,nizStacionaza_lancanica_X_rel, nizKota_lancanica_Y_rel );

         if (BuildConfig.DEBUG)Log.e("TAGsigVisine", "x_rel= "+ String.format("%.2f",proveraXkoord)+
                "  x_stacionaza= "+ String.format("%.2f",xVertikala)+
                "  y_koord_tlo= "+ String.format("%.2f",y_koord_tlo)+
                "  y_koord_lanc= "+ String.format("%.2f",y_koord_lanc)+
                "  razlikaY= "+ String.format( "%.2f",(y_koord_lanc-y_koord_tlo)/razmeraY )  +
                "  sig_visina= "+ String.format("%.2f", sig_visina)+
                "  sig_visinaREL= "+ String.format("%.2f",sig_visina*razmeraY)
        );

    }
*/