package nsp.mpu._5_sigurnosne_visine;


import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.File;
import java.util.List;

import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu._0MainActivity;
import nsp.mpu.pomocne_klase.AnimacijeKlasa;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Fragment.nazivTXT_trasa;
import static nsp.mpu.pomocne_klase.StaticPodaci.folderUserData;
import static nsp.mpu.pomocne_klase.StaticPodaci.kreirajFolder;

/** @-O-b-f-u-s-c-a-t-e ne radi (nedozvoljava build) zbog implements fragmenta*/
public class ProveraSigVisina_Activity extends AppCompatActivity
implements ProveraSigVisina_fragment_proracuni.DodajStub {

    // ako bi koristili public static iz fragmenta onda drugi fragmenti
    // bi videli samo pocetnu vrednost 6f, a ne bi videli unetu(promenjenu) vrednost

    // protected - Visible to the package and all sub-classes

    // ptn 1-400kV
    protected static float sig_visina=6f; // mesta pristupacna vozilima
    protected static float sig_visinaZgrade=3f; // pri Wycisk!!!
    protected static float sig_visinaPut=7f; // lokalni,regionali,magistalni i auto put
    protected static float sig_visinaNaselje=7f; // lokalni,regionali,magistalni i auto put
    protected static float sig_visinaDV=2.5f; // i za NN i za VN (na gornjem DV ima leda a na donjem nema)

    boolean doubleBackToExitPressedOnce = false;
    private static File folderSigVisine;
    public static File kreirajFajlZaSave(Context context, String fileName){
        String novFolder = nazivTXT_trasa;
        folderSigVisine = kreirajFolder(context, folderUserData, novFolder);
        File novFajl = new File(folderSigVisine + File.separator + fileName);
         if (BuildConfig.DEBUG)Log.e("TAGroot", novFajl.getPath());
        return novFajl;
    }
    @Override
    public void onBackPressed() {
        // treba getSupportFragmentManager a ne getFragmentManager!!!

        if ( ! getSupportActionBar().isShowing()){
            ((ProveraSigVisina_fragment_proracuni)getVisibleFragment()).izmenaLayoutaNaVrhu(ProveraSigVisina_fragment_proracuni.IzmenaLayouta.sakri);
            return;
        }
        if (doubleBackToExitPressedOnce ||
                    (getSupportFragmentManager().getBackStackEntryCount() != 0)) {
            super.onBackPressed();
            AnimacijeKlasa.ParAnimacijaInOut parAnim = new AnimacijeKlasa.ParAnimacijaInOut();
            overridePendingTransition(parAnim.in, parAnim.out);
            return;
        }
        doubleBackToExitPressedOnce = true;

        // prikazuje toast i pri izlasku iz activity...toast.cancel() ne radi
        StaticPodaci.prikaziToast(this, "Pritisni dvaput uzastopno za exit");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 500);
    }

    private Fragment getVisibleFragment(){
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if(fragments != null){
            for(Fragment fragment : fragments){
                if(fragment != null && fragment.isVisible() && fragment instanceof ProveraSigVisina_fragment_proracuni )
                    return fragment;
            }
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ( _0MainActivity.mDaLiJeTablet == true) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        setContentView(R.layout.activity_reference_jedan_frag_ili_oba);

        FragmentManager fragmentManager = getSupportFragmentManager();

        Fragment sigVisineFrag = fragmentManager.findFragmentById(R.id.activity_prikaz_fragmenta);
        if (sigVisineFrag==null) {
            sigVisineFrag = new ProveraSigVisina_Fragment();
            fragmentManager.beginTransaction()

                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .add(R.id.activity_prikaz_fragmenta, sigVisineFrag,"pocetni_fragment").commit();
        }
    }


    @Override
    public void dodajNoviStub(float x, float y) {
         if (BuildConfig.DEBUG)Log.e("TAG40", "act dodajNoviStub "+x+" "+y);
        ProveraSigVisina_Fragment fragment = (ProveraSigVisina_Fragment)
                getSupportFragmentManager().findFragmentByTag("pocetni_fragment");
        fragment.dodajStub(x, y);

    }


    public void setActionBarTitle(String naslov) {
        if ( getSupportActionBar().isShowing()) {
            getSupportActionBar().setTitle(naslov);
        }
    }
}
