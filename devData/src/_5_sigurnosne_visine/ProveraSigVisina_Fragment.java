package nsp.mpu._5_sigurnosne_visine;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.AnimacijeKlasa;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static android.app.Activity.RESULT_OK;
import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Activity.kreirajFajlZaSave;
import static nsp.mpu._5_sigurnosne_visine.StubnoMesto.setBool_potrebanUpdate;
import static nsp.mpu.database.PODACI_ListaTipovaUzadi.getListaUzadi;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animacijaZaRecycleView;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_IN;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_OUT;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.pokreniAnimaciju_toGone;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.pokreniAnimaciju_toVisible;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.pokreniAnimaciju_zamena;
import static nsp.mpu.pomocne_klase.StaticPodaci.folderUserData;
import static nsp.mpu.pomocne_klase.StaticPodaci.getIndexElementaUNizu;
import static nsp.mpu.pomocne_klase.StaticPodaci.getNizNaponskiNivo;
import static nsp.mpu.pomocne_klase.StaticPodaci.getSharPref;
import static nsp.mpu.pomocne_klase.StaticPodaci.mojHtmlString;
import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;
import static nsp.mpu.pomocne_klase.StaticPodaci.pozicijaTipskogOpterUNizu;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziWarnToast;
import static nsp.mpu.pomocne_klase.StaticPodaci.temperaturaProv_default;
import static nsp.mpu.pomocne_klase.StaticPodaci.temperaturaProv_max;
import static nsp.mpu.pomocne_klase.StaticPodaci.temperaturaProv_min;

@Obfuscate
public class ProveraSigVisina_Fragment extends Fragment {


    private Button mUcitajTrasu_TXT;
    private PODACI_TipUzeta mTU;
    private NumberPicker mTipUzeta;
    private EditText mMaxNaprezanje;
    private NumberPicker mDodOpt;
    private EditText mTemperatura;
    private EditText mVisinaDefault;
    private EditText mSigVisina;
    private RecyclerView mRecyclerView;
    private AdapterRaspona mAdapterRaspona;
    public Button mPrikaziTrasu_i_ugibe;
    private Switch mSwitch;
    private LinearLayout mParametriLayout, mRasponiLayout;

    private double sigma0, koefDodOpt;
    private String [] nizOznakaUzadi;
    private int temperatura;
    private int brojStubova;

    /** ovaj string koristimo i za naziv foldera u kojem ce da se cuvaju
     * svi fajlovi kreirani za konkretan profil trase
     * korisi ga activity*/
    public static String nazivTXT_trasa;

    private String nazivTXT_STUBOVI, nazivTXT_STUBOVI_novi;

    private float [] nizStacionaza_tla_X, nizKota_tla_Y,
                     nizStacionaza_stub_X,nizKota_stubova,  nizKota_vesanja_uzeta_Y;

    private ArrayList listaStacionaza_stubova;
    private ArrayList listaVisina_stubova; // visina vesanja najnizeg provodnika na stubu
    private ArrayList listaOznakaStubova;


    private boolean ucitanTXT_trasa_flag, ucitanTXT_STUB_flag;
    private float stacionaza_min, stacionaza_max,
            stacionaza_stuba_default, visina_stuba_default;

    private Uri selectedfile_trasa_uri;
    File fajlSaveStub_txt;
    private boolean FLAG;

    public static final String ARGcuvanjeFrag1 = "ARGcuvanjeFrag1";
    public static final String ARGcuvanjeFrag2 = "ARGcuvanjeFrag2";
    public static final String ARGcuvanjeFrag3 = "ARGcuvanjeFrag3";
    public static final String ARGcuvanjeFrag4 = "ARGcuvanjeFrag4";
    public static final String ARGcuvanjeFrag5 = "ARGcuvanjeFrag5";
    public static final String ARGcuvanjeFrag6 = "ARGcuvanjeFrag6";
    private boolean bool_prvoPokretanje = true;


    @Override
    public void onCreateOptionsMenu(Menu m, MenuInflater i) {
        super.onCreateOptionsMenu(m, i);
        i.inflate(R.menu.meni_sig_visine_unos, m);
        MenuItem open = m.findItem(R.id.menu_sig_visine_open);
        MenuItem save = m.findItem(R.id.menu_sig_visine_save);
        if (mSwitch.isChecked()) {
            open.setVisible(true);
            save.setVisible(true);
        }else{
            open.setVisible(false);
            save.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_sig_visine_open:
                ucitajStuboveIzTXT();
                return true;
            case R.id.menu_sig_visine_save:
                sacuvajStuboveUTXT_dialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void sacuvajStuboveUTXT_dialog(){
        if (!ucitanTXT_trasa_flag) {
            prikaziToast(getContext(),"Ucitaj prvo kote terena");
            return;
        }
        AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
        ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
        ad.setTitle(mojHtmlString("<i>Cuvanje podataka"));
        ad.setMessage(mojHtmlString("Unesi naziv fajla u koji ce podaci biti sacuvani:"));
        final EditText inputEditText = new EditText(getContext());
        ad.setView(inputEditText);
        inputEditText.setText(nazivTXT_trasa+" - raspored stubova ");
        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        nazivTXT_STUBOVI_novi = new String( inputEditText.getText().toString() );

                        fajlSaveStub_txt = kreirajFajlZaSave(getContext(), nazivTXT_STUBOVI_novi + ".txt");
                         if (BuildConfig.DEBUG)Log.e("TAG10", "fajlSaveStub_txt " + fajlSaveStub_txt.getPath());

                        if (fajlSaveStub_txt.exists()){
                            new AlertDialog.Builder(getActivity())
                                    .setIcon(R.mipmap.ic_alert_crveno)
                                    .setTitle(mojHtmlString("<b><i>Paznja!"))
                                    .setMessage(mojHtmlString("Vec postoji fajl <b>" +nazivTXT_STUBOVI_novi+
                                            "</b> Sacuvaj preko njega?"))
                                    .setPositiveButton("DA", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            upisPodatakaUTXT();
                                        }
                                    })
                                    .setNegativeButton("NE", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            sacuvajStuboveUTXT_dialog();
                                        }
                                    })
                                    .show();
                        }
                        else{
                            upisPodatakaUTXT();

                        }

                    }
                });
        ad.setButton(AlertDialog.BUTTON_NEGATIVE, "Odustani",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        ad.show();


    }



    private void upisPodatakaUTXT(){
        try {
            fajlSaveStub_txt.createNewFile();
        } catch (IOException e) {
            prikaziToast(getActivity(),"Greska, fajl <b>" + nazivTXT_STUBOVI_novi +
                    "<\b>ne moze da se kreira!");
             if (BuildConfig.DEBUG)Log.e("TAG10", "Greska, fajl " + nazivTXT_STUBOVI_novi + "bne moze da se kreira!");
            e.printStackTrace();
            return;
        }
        try {
            if ( fajlSaveStub_txt.canWrite() ) {
                FileOutputStream out = new FileOutputStream(fajlSaveStub_txt);
                out.write((";raspored stubova za trasu:  " + nazivTXT_trasa + "\n").getBytes());
                out.write((" ;stacionaza[m]\tvisina[m]\toznaka stuba").getBytes());

                for (int i = 0; i < listaStacionaza_stubova.size(); i++) {
                    out.write((" \n" + listaStacionaza_stubova.get(i)).getBytes());
                    out.write((" \t\t" + listaVisina_stubova.get(i)).getBytes());
                    out.write((" \t" + listaOznakaStubova.get(i).toString().replace(" ", "_") ).getBytes());
                }
                out.flush();
                out.close();

                prikaziToast(getContext(), "Sacuvaj je fajl: "+fajlSaveStub_txt.getName());

                MediaScannerConnection.scanFile(getContext(), new String[]{fajlSaveStub_txt.getAbsolutePath()}, null, null);

            } else {
                prikaziToast(getActivity(),"Greska, u fajl <b>" + nazivTXT_STUBOVI_novi +
                        "<\b>ne mogu da se upisuji podaci!");
            }

        } catch (Exception e) {
             if (BuildConfig.DEBUG)Log.e("TAG10", "greska pri pisanju izlaznog TXT");
            e.printStackTrace();
            return;
        }
    }


    private void ucitajStuboveIzTXT(){
        if (!ucitanTXT_trasa_flag) {
            prikaziToast(getContext(),"Ucitaj prvo kote terena");
            return;
        }
        AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
        ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
        ad.setTitle(mojHtmlString("<b><i>P A Z NJ A"));
        ad.setMessage(mojHtmlString("Otvaranjem novog fajla uneti podaci o stubovima bice obrisani"));
        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent()
                                .setType("text/plain")
                                .setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Odaberi raspored stubova"), 555);
                    }
                });
        ad.setButton(AlertDialog.BUTTON_NEGATIVE, "Odustani",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        ad.setIcon(R.mipmap.ic_alert_crveno);
        ad.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode==123 && resultCode==RESULT_OK) {
            selectedfile_trasa_uri = intent.getData();
                         if (BuildConfig.DEBUG)Log.e("TAG7", "selectedfile_trasa_uri ---   " + selectedfile_trasa_uri.getPath() );

            nazivTXT_trasa = getFileName_fromUri(selectedfile_trasa_uri, getContext()).replace(".txt","");

            ucitajUzduzniProfilTerana(selectedfile_trasa_uri);

            if (ucitanTXT_trasa_flag) {
                nacrtajProfilTrase();
            }
            else{
                mUcitajTrasu_TXT.setText("greska");
            }

        }

        if(requestCode==555 && resultCode==RESULT_OK){
            Uri selectedfile_STUBOVI_uri = intent.getData();

             if (BuildConfig.DEBUG)Log.e("TAG7", "selectedfile_STUBOVI_uri ---   " + selectedfile_STUBOVI_uri.getPath() );

            nazivTXT_STUBOVI = getFileName_fromUri(selectedfile_STUBOVI_uri, getContext()).replace(".txt","");

            ucitajRasporedStubova(selectedfile_STUBOVI_uri);

            if (ucitanTXT_STUB_flag) {
                reStartAdaptera();
                prikaziToast(getContext(), "Broj uspešno učitanih stubova je: <b>"+
                                                            nizStacionaza_stub_X.length);
            }
        }

    }

    private void nacrtajProfilTrase(){
        Fragment prikazCrtanja = ProveraSigVisina_fragment_proracuni
                .nacrtajUzduzniProfilTerena(nizStacionaza_tla_X, nizKota_tla_Y, nazivTXT_trasa);

        FragmentManager fm = getActivity().getSupportFragmentManager();
        if (mDaLiJeTablet)
            fm.beginTransaction()
                    .setCustomAnimations(
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT(),
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_drugog_fragmenta, prikazCrtanja)
                    .commit();
        else
            fm.beginTransaction()
                    .setCustomAnimations(
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT(),
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_fragmenta, prikazCrtanja)
                    .addToBackStack(null) // back dugme vraca na zamenjeni frag
                    .commit();

        prikaziToast(getContext(), "Broj učitanih tačaka: "+nizStacionaza_tla_X.length);
        mUcitajTrasu_TXT.setText(nazivTXT_trasa);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putDouble(ARGcuvanjeFrag1, koefDodOpt);
        outState.putString(ARGcuvanjeFrag2, mTU.getOznakaUzeta());
        outState.putParcelable(ARGcuvanjeFrag3, selectedfile_trasa_uri);
        outState.putInt(ARGcuvanjeFrag4, brojStubova);
        outState.putParcelableArrayList(ARGcuvanjeFrag5, listaStacionaza_stubova );
        outState.putParcelableArrayList(ARGcuvanjeFrag6, listaVisina_stubova );
         if (BuildConfig.DEBUG)Log.e("TAG1", "outState listaStacionaza_stubova  " +listaStacionaza_stubova);
         if (BuildConfig.DEBUG)Log.e("TAG1", "outState listaVisina_stubova  " +listaVisina_stubova);


        outState.putParcelableArrayList("listaOznakaStubova", listaOznakaStubova );

        super.onSaveInstanceState(outState);

    }


    public static StaticPodaci.NaponskiNivo naponskiNivo;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        // jer   class ActivityZaJedanFragment extends AppCompatActivity
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false); // bez up strelice

        /**************** 7.4.6.2
            pomocu setRetainInstance(true) omoguceno je da se fragment sa backstact kreira i
            nakon sto je aktivnost prekinuta pa ponovno kreirana (prikazan grafik - zakljucas ekran - otkljucas
            (i dalje je prikazan grafik) - klikom na back se vracas na unos podataka --- SVI UNETI PODACI
            SU PONOVU TU

            problem je sto se tada pojavljuje i upButton i title u ActionBar, ne znam kako da iskljucim
            (ovo iznad sada ne radi) tako sa sam u manifestu obrisao parentActivityName, tako da nema strelice
            (title je ostao)

            sa setRetainInstance cak moze da se izbaci onSaveInstanceState

            pomocu FLAG se  uklj/isklj  setRetainInstance/onSaveInstanceState
        */
        FLAG =  true;

        setRetainInstance(FLAG);

        ucitanTXT_trasa_flag =false;
        ucitanTXT_STUB_flag =false;

        brojStubova = 0; // jer na kraju onCreate dodajemo stub
        stacionaza_stuba_default = 0.0001f;
        visina_stuba_default = 10.0001f;
        listaStacionaza_stubova = new ArrayList<>();
        listaVisina_stubova = new ArrayList<>();
        listaOznakaStubova = new ArrayList<>();

        nazivTXT_trasa = "Odaberi profil trase";

        nizOznakaUzadi = getListaUzadi(getActivity()).getNizNazivUzadi();
        sigma0=9;
        koefDodOpt=1;
        mTU=getListaUzadi(getActivity()).getTipUzetaIzListe("Al/Ce-70/12");
        temperatura = 40;
        //sig_visina = 6.0f;

        nizStacionaza_stub_X = new float[]{stacionaza_stuba_default};
        //nizVisinaStubova = new float[]{visina_stuba_default};

        String pomocno = getSharPref().getString("naponskiNivo", StaticPodaci.NaponskiNivo._35kV.toString());
        int ind = getIndexElementaUNizu(getNizNaponskiNivo(), pomocno);
        naponskiNivo = getNizNaponskiNivo()[ind];

    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle savedInstanceState) {
        View v = li.inflate(R.layout.fragment_provera_sigurnosnih_visina, vg, false);
        if (!mDaLiJeTablet)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ((ProveraSigVisina_Activity) getActivity()).setActionBarTitle("Uzdužni profil");

        if (FLAG==false) { // znaci da koristimo setRetainInstance pa je ovo nepotrebno
            if (savedInstanceState != null) {
                // Restore last state
                koefDodOpt = savedInstanceState.getDouble(ARGcuvanjeFrag1);
                String s =  savedInstanceState.getString(ARGcuvanjeFrag2);
                    mTU = getListaUzadi(getActivity()).getTipUzetaIzListe(s) ;
                selectedfile_trasa_uri = savedInstanceState.getParcelable(ARGcuvanjeFrag3);
                brojStubova = savedInstanceState.getInt(ARGcuvanjeFrag4);
                listaStacionaza_stubova = savedInstanceState.getParcelableArrayList(ARGcuvanjeFrag5);
                listaVisina_stubova = savedInstanceState.getParcelableArrayList(ARGcuvanjeFrag6);
                 if (BuildConfig.DEBUG)Log.e("TAG1", "onCreateView listaStacionaza_stubova  " +listaStacionaza_stubova);
                 if (BuildConfig.DEBUG)Log.e("TAG1", "onCreateView listaVisina_stubova  " +listaVisina_stubova);


                listaOznakaStubova = savedInstanceState.getParcelableArrayList("listaOznakaStubova");

                if (selectedfile_trasa_uri!=null) {
                    nazivTXT_trasa = getFileName_fromUri(selectedfile_trasa_uri, getContext()).replace(".txt","");
                    ucitajUzduzniProfilTerana(selectedfile_trasa_uri);
                }
            }
        }

         if (BuildConfig.DEBUG)Log.e("TAG9","onCreateView -brojStubova=" + brojStubova);

        mUcitajTrasu_TXT = (Button) v.findViewById(R.id.f7_ulazni_txt);
        mUcitajTrasu_TXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent()
                        .setType("text/plain")
                        .setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Odaberi profil trase"), 123);
            }
        });
        mUcitajTrasu_TXT.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final AlertDialog ad = new AlertDialog.Builder(getContext()).create();
                ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
                ad.setTitle(mojHtmlString("Kreiraj uzdužni profil"));
                //ad.setMessage(/*mojHtmlString*/("Unesi dužinu trase [m]:"));
                final EditText inputEditText = new EditText(ad.getContext());
                ad.setView(inputEditText);
                inputEditText.setHint("Unesi dužinu trase [m]");
                inputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
                inputEditText.setFilters(new InputFilter[] {new InputFilter.LengthFilter(3)});
                ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                int duzinaRavneTrase=100;
                                try {
                                    duzinaRavneTrase = Integer.parseInt(inputEditText.getText().toString());
                                }catch (NumberFormatException e) {}

                                if (duzinaRavneTrase<5 || duzinaRavneTrase>999) {
                                    prikaziWarnToast(getContext(), "Dužina mora biti u intervalu 5-999 m");
                                    ucitanTXT_trasa_flag = false;
                                    ad.dismiss();
                                }
                                else{
                                    nizStacionaza_tla_X = new float[]{0,duzinaRavneTrase};
                                    nizKota_tla_Y = new float[]{0,0};
                                    stacionaza_max = nizStacionaza_tla_X[nizStacionaza_tla_X.length-1];
                                    stacionaza_min = nizStacionaza_tla_X[0];
                                    nazivTXT_trasa ="ravna trasa od "+duzinaRavneTrase+"m";
                                    ucitanTXT_trasa_flag = true;
                                    ad.dismiss();
                                    nacrtajProfilTrase();
                                }
                            }
                        });
                ad.setButton(AlertDialog.BUTTON_NEGATIVE, "Odustani",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            ad.dismiss();
                            }
                        });
                ad.show();
                return true;
            }
        });
        mUcitajTrasu_TXT.setText(nazivTXT_trasa);

        mTipUzeta = (NumberPicker) v.findViewById(R.id.num_picker_uze);
        mTipUzeta.setDisplayedValues(nizOznakaUzadi);
        mTipUzeta.setMinValue(0);
        mTipUzeta.setMaxValue(nizOznakaUzadi.length-1);
        mTipUzeta.setWrapSelectorWheel(true);
        mTipUzeta.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mTipUzeta.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                 if (BuildConfig.DEBUG)Log.e("TAG","newVal= " + newVal);
                mTU= getListaUzadi(getActivity()).getTipUzetaIzListe(nizOznakaUzadi[newVal]);
                 if (BuildConfig.DEBUG)Log.e("TAG","tipUzeta= " + mTU.getOznakaUzeta());
            }
        });
        mTipUzeta.setValue(getListaUzadi(getActivity()).pozicijaUzetaUListi(mTU.getOznakaUzeta()));

        mDodOpt = (NumberPicker) v.findViewById(R.id.num_picker_odo);
        mDodOpt.setDisplayedValues( StaticPodaci.tipskaDodOpt );
        mDodOpt.setMinValue(0);
        mDodOpt.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mDodOpt.setMaxValue(StaticPodaci.tipskaDodOpt.length-1);
        mDodOpt.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                 if (BuildConfig.DEBUG)Log.e("TAG","newVal= " + newVal);
                koefDodOpt =Double.valueOf(StaticPodaci.tipskaDodOpt[newVal]);
                 if (BuildConfig.DEBUG)Log.e("TAG","koefDodOpt= " + koefDodOpt);
            }
        });
        mDodOpt.setValue(pozicijaTipskogOpterUNizu(koefDodOpt));

        ////////// glupi bag koji se javlja na EXPERIA///
        //https://stackoverflow.com/questions/25880638/rotating-android-device-while-viewing-dialog-preference-with-timepicker-or-numbe
        mTipUzeta.setSaveFromParentEnabled(false);
        mTipUzeta.setSaveEnabled(true);
        mDodOpt.setSaveFromParentEnabled(false);
        mDodOpt.setSaveEnabled(true);

        ////////////////////////////////////////////////////

        Spinner mNapon = (Spinner) v.findViewById(R.id.spiner_naponi);
        final ArrayAdapter<StaticPodaci.NaponskiNivo> adapterStub = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, getNizNaponskiNivo());
        mNapon.setAdapter(adapterStub);
        int brojac = 0;
        for (int i = 0; i < getNizNaponskiNivo().length; i++)
            if (getNizNaponskiNivo()[i].equals(naponskiNivo))
                brojac = i;
        mNapon.setSelection(brojac);
        mNapon.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                naponskiNivo = (StaticPodaci.NaponskiNivo) adapterView.getItemAtPosition(i);
                getSharPref().edit().putString("naponskiNivo", naponskiNivo.toString()).apply();
                 if (BuildConfig.DEBUG)Log.e("TAG41", "naponskiNivo=" + naponskiNivo);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        mTemperatura = (EditText) v.findViewById(R.id.f7_odaberi_temp);
        mTemperatura.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                 if (BuildConfig.DEBUG)Log.e("TAG","PRE PARSE temp= " + cs);
                //temperatura je vec inicijalizovana na +40
                try {
                    temperatura = Integer.parseInt(cs.toString());
                } catch (NumberFormatException e) {}

                if (temperatura<temperaturaProv_min||temperatura>temperaturaProv_max){
                    temperatura=temperaturaProv_default;
                    prikaziToast(getContext(),"Unesi temperatutu od "+temperaturaProv_min+" do "+temperaturaProv_max);
                }

            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });

        mMaxNaprezanje = (EditText) v.findViewById(R.id.f7_odaberi_naprezanje);
        mMaxNaprezanje.addTextChangedListener(new TextWatcher(){ // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                //sigma0 je vec inicijalizovana na 9
                try {
                    sigma0 = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (sigma0<0.01||sigma0>99) {
                    sigma0 = 9;
                    prikaziToast(getContext(), "Unesi naprazenje od 0.01 do 99");
                }
                 if (BuildConfig.DEBUG)Log.e("TAG","sigma0= " + sigma0);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });

        mSigVisina = (EditText) v.findViewById(R.id.f7_unesi_sig_visinu);
        mSigVisina.addTextChangedListener(new TextWatcher(){
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                //sigVisina je vec inicijalizovana na 6
                try {
                    ProveraSigVisina_Activity.sig_visina = Float.parseFloat(cs.toString());
                } catch (NumberFormatException e) {}

                if (ProveraSigVisina_Activity.sig_visina<1||ProveraSigVisina_Activity.sig_visina>20) {
                    ProveraSigVisina_Activity.sig_visina = 6;
                    prikaziToast(getContext(), "Unesi sig.visinu od 1 do 20 [m]");
                }
                 if (BuildConfig.DEBUG)Log.e("TAG","sig_visina= " + ProveraSigVisina_Activity.sig_visina);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){}
        });

        mRecyclerView = (RecyclerView) v.findViewById(R.id.f7_recycle_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mPrikaziTrasu_i_ugibe = (Button) v.findViewById(R.id.f7_prikazi_ugibe);
        mPrikaziTrasu_i_ugibe.setVisibility(View.GONE);
        mPrikaziTrasu_i_ugibe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdapterRaspona.getItemCount()<2)
                    prikaziToast(getContext(), "Unesi bar dva stubna mesta");
                else {

                     if (BuildConfig.DEBUG)Log.e("TAG40", "pokreniFragProracuni");
                    konvertovanjeListeXYkoordStubovaUnizove();

                    if (nizJeRastuci(nizStacionaza_stub_X) == false){
                        prikaziToast(getContext(), "Stacionaža sledećeg stuba u nizu mora da " +
                                "bude veća od stacionaže prethodnog");
                        return;
                    }

                    setBool_potrebanUpdate(true);

                     if (BuildConfig.DEBUG)Log.e("TAG40", "mPrikaziTrasu_i_ugibe");

                    Fragment prikazCrtanja = ProveraSigVisina_fragment_proracuni
                            .nacrtajUzduzniProfil_terena_ugiba_jedStanja
                                    (nizStacionaza_tla_X, nizKota_tla_Y, nazivTXT_trasa.replace(".txt", ""),
                                            nizStacionaza_stub_X, nizKota_vesanja_uzeta_Y,
                                            listaOznakaStubova, listaVisina_stubova,
                                            mTU.getOznakaUzeta(), sigma0, koefDodOpt, temperatura);  // "(?i).txt" nalazi .txt i .TXT

                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    if (mDaLiJeTablet) {
                        // !@#4.3.18 --- cistimo back stack da ne bi ostao prethodni fragment proracuna
                        // a mi hocemo da kreiramo nov (prikazivao bi meni sa dupliranim pozicijama)
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        fm.beginTransaction()
                                .setCustomAnimations(
                                        odaberiAnimaciju_fragment_IN(),
                                        odaberiAnimaciju_fragment_OUT(),
                                        odaberiAnimaciju_fragment_IN(),
                                        odaberiAnimaciju_fragment_OUT()
                                )
                                .replace(R.id.activity_prikaz_drugog_fragmenta, prikazCrtanja)
                                .commit();

                      //  /*** ovaj deo je bug - ali mi i ne treba 03.03.2018.  */
//                        ((ProveraSigVisina_fragment_proracuni) fm.findFragmentById(R.id.activity_prikaz_drugog_fragmenta))
//                                .izmenaLayoutaNaVrhu(ProveraSigVisina_fragment_proracuni.IzmenaLayouta.sakri);
                    }else {
                        fm.beginTransaction()
                                .setCustomAnimations(
                                        odaberiAnimaciju_fragment_IN(),
                                        odaberiAnimaciju_fragment_OUT(),
                                        odaberiAnimaciju_fragment_IN(),
                                        odaberiAnimaciju_fragment_OUT()
                                )
                                .replace(R.id.activity_prikaz_fragmenta, prikazCrtanja)
                                .addToBackStack(null) // back dugme vraca na zamenjeni frag
                                .commit();
                    }
                }

            }
        });


        mParametriLayout = (LinearLayout) v.findViewById(R.id.f7_layout_parametara);

        mRasponiLayout = (LinearLayout) v.findViewById(R.id.f7_layout_raspona);
        mRasponiLayout.setVisibility(View.GONE);
        mSwitch = (Switch) v.findViewById(R.id.switch1);
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if ( ! ucitanTXT_trasa_flag ){
                    mSwitch.setChecked(false);
                    prikaziToast(getContext(), "Prvo odaberi profil trase!");
                    return;
                }

                getActivity().invalidateOptionsMenu();
                if (mSwitch.isChecked() ){
                    pokreniAnimaciju_toGone(mUcitajTrasu_TXT, false);
                    pokreniAnimaciju_zamena(mRasponiLayout,mParametriLayout);
                    pokreniAnimaciju_toVisible(mPrikaziTrasu_i_ugibe);

                }else{
                    pokreniAnimaciju_toVisible(mUcitajTrasu_TXT);
                    pokreniAnimaciju_zamena(mParametriLayout,mRasponiLayout);
                    pokreniAnimaciju_toGone(mPrikaziTrasu_i_ugibe, true);

                }

            }
        });


        if (savedInstanceState != null)
            reStartAdaptera();
        else
            dodajStub(0);

        if (BuildConfig.IS_DEMO) {
            mDodOpt.setValue(1);
            koefDodOpt = 1.6;
            mDodOpt.setEnabled(false);
        }

        if(bool_prvoPokretanje && BuildConfig.AUTO_UCITAVANJE) {
            //!@# 14.2.18
            bool_prvoPokretanje = false;

            File file = new File(folderUserData + File.separator+"ZP-8 - raspored stubova.txt");
            ucitajRasporedStubova(Uri.fromFile(file));
            // if (BuildConfig.DEBUG)Log.e("TAGroot", "file0: " + file);

            file = new File(folderUserData + File.separator+"ZP-8.txt");
            ucitajUzduzniProfilTerana(Uri.fromFile(file));
            // if (BuildConfig.DEBUG)Log.e("TAGroot", "file1: " + file);

            if (ucitanTXT_trasa_flag) {
                Fragment prikazCrtanja = ProveraSigVisina_fragment_proracuni
                        .nacrtajUzduzniProfilTerena(nizStacionaza_tla_X, nizKota_tla_Y, nazivTXT_trasa);

                FragmentManager fm = getActivity().getSupportFragmentManager();
                if (mDaLiJeTablet)
                    fm.beginTransaction()
                            .setCustomAnimations(
                                    odaberiAnimaciju_fragment_IN(),
                                    odaberiAnimaciju_fragment_OUT(),
                                    odaberiAnimaciju_fragment_IN(),
                                    odaberiAnimaciju_fragment_OUT()
                            )
                            .replace(R.id.activity_prikaz_drugog_fragmenta, prikazCrtanja)
                            .commit();
                else
                    fm.beginTransaction()
                            .setCustomAnimations(
                                    odaberiAnimaciju_fragment_IN(),
                                    odaberiAnimaciju_fragment_OUT(),
                                    odaberiAnimaciju_fragment_IN(),
                                    odaberiAnimaciju_fragment_OUT()
                            )
                            .replace(R.id.activity_prikaz_fragmenta, prikazCrtanja)
                            .addToBackStack(null) // back dugme vraca na zamenjeni frag
                            .commit();

                prikaziToast(getContext(), "Broj učitanih tačaka: "+nizStacionaza_tla_X.length);
                mUcitajTrasu_TXT.setText(nazivTXT_trasa="ZP-8");
            }else{
                mUcitajTrasu_TXT.setText("greska");
            }

        }


        return v;
    }



    @Obfuscate//import
    private class HolderRaspona extends RecyclerView.ViewHolder {
        //HOLDER od ADAPTERA dobija layout izgled i podatke iz modela, koje vezuje

        private EditText mStacionazaStuba;
        private EditText mVisinaStuba;
        private ImageButton mRasponDodaj, mRasponUkloni;
        private TextView mBrojStubnogMesta;
        private Button mPovacaj, mSmanji;

        private HolderRaspona(View v) {
            super(v);
            mBrojStubnogMesta = (TextView) v.findViewById(R.id.f7_recycle_broj_stubnog_mesta);
            mBrojStubnogMesta.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    izmenaOznakeStuba_dialog(getAdapterPosition());
                    return false;
                }
            });


            mStacionazaStuba = (EditText) v.findViewById(R.id.f7_recycle_view_raspon);
            mStacionazaStuba.addTextChangedListener(new TextWatcher() { // mora ovako
                @Override
                public void onTextChanged(CharSequence cs, int s, int b, int c) {
                     if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE holder Raspon= " + cs);

                    float pomocnaVar =0;
                    try {
                        pomocnaVar = Float.parseFloat(cs.toString());
                    } catch (NumberFormatException e) {}

                    if (pomocnaVar<stacionaza_min || pomocnaVar>stacionaza_max){
                        listaStacionaza_stubova.set(getAdapterPosition(), stacionaza_stuba_default);
                        prikaziToast(getContext(),"Unesi raspon od 1 do "+nizStacionaza_tla_X[nizStacionaza_tla_X.length-1]);
                    } else
                        listaStacionaza_stubova.set(getAdapterPosition(),pomocnaVar);

                     if (BuildConfig.DEBUG)Log.e("TAG", "holder Raspon= " + listaStacionaza_stubova.get(getAdapterPosition())+" index "+getAdapterPosition());
                }
                @Override
                public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
                @Override
                public void afterTextChanged(Editable e) {
                    //index_stuba_kojemSuPromenjeneVred = getAdapterPosition();
                }
            });

            mVisinaStuba = (EditText) v.findViewById(R.id.f7_recycle_view_visina);
            mVisinaStuba.addTextChangedListener(new TextWatcher() { // mora ovako
                @Override
                public void onTextChanged(CharSequence cs, int s, int b, int c) {
                     if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE holder CosFi= " + cs);

                    float pomocnaVar = 1;
                    try {
                        pomocnaVar = Float.parseFloat(cs.toString());
                    } catch (NumberFormatException e) {}

                    if (pomocnaVar<1 || pomocnaVar>50){
                        listaVisina_stubova.set(getAdapterPosition(), visina_stuba_default);
                        prikaziToast(getContext(),"Unesi visnu vesanja od 1 do 50m");
                    }else
                        listaVisina_stubova.set(getAdapterPosition(),pomocnaVar);

                     if (BuildConfig.DEBUG)Log.e("TAG", "holder visina= " + listaVisina_stubova.get(getAdapterPosition())+" index "+getAdapterPosition());
                }
                @Override
                public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
                @Override
                public void afterTextChanged(Editable e) {
                    //index_stuba_kojemSuPromenjeneVred = getAdapterPosition();
                }
            });


            mRasponDodaj = (ImageButton) v.findViewById(R.id.f7_recycle_dodaj_raspon);
            mRasponDodaj.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                     if (BuildConfig.DEBUG)Log.e("TAG6","brojStubova= " + brojStubova);
                    dodajStub(getAdapterPosition()+1); // +1 jer novi stub ce biti na sledecoj (+1) poziciji
                }
            });

            mRasponUkloni = (ImageButton) v.findViewById(R.id.f7_recycle_obrisi_raspon);
            mRasponUkloni.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listaStacionaza_stubova.size() ==1)
                        prikaziToast(getContext(),"Nije dozvoljeno brisanje prvog stuba");
                    else {
                        //brojStubova -= 1;
                        obrisiStub(getAdapterPosition());
                    }
                     if (BuildConfig.DEBUG)Log.e("TAG6","brojStubova= " + brojStubova);
                }
            });

            mSmanji = (Button) v.findViewById(R.id.f7_recycle_smanji_do_kote);
            mSmanji.setOnClickListener(new View.OnClickListener() {
                float staraVred, novaVred;
                int indexNiza, indexStuba;
                String tekst;
                @Override
                public void onClick(View view) {
                    indexStuba = getAdapterPosition();
                    tekst = mStacionazaStuba.getText().toString();
                    staraVred = (tekst.equals("")) ? 0 : Float.parseFloat(tekst);
                    indexNiza = indexSusednog_ManjegClanaNiza(staraVred, nizStacionaza_tla_X);
                    novaVred = nizStacionaza_tla_X[indexNiza];
                    if (staraVred==novaVred && indexNiza>0)
                        novaVred = nizStacionaza_tla_X[--indexNiza];

                    if (indexStuba == 0)
                        mStacionazaStuba.setText(String.valueOf(novaVred));
                    else if (novaVred > Float.parseFloat(listaStacionaza_stubova.get(indexStuba - 1).toString()))
                        mStacionazaStuba.setText(String.valueOf(novaVred));
                    else
                        prikaziToast(getContext(),"Stacionaža stuba mora biti veća od stacionaže prethodnog");
                }
            });

            mPovacaj = (Button) v.findViewById(R.id.f7_recycle_povecaj_do_kote);
            mPovacaj.setOnClickListener(new View.OnClickListener() {
                float staraVred, novaVred;
                int indexNiza, indexStuba;
                String tekst;
                @Override
                public void onClick(View view) {
                    indexStuba = getAdapterPosition();
                    tekst = mStacionazaStuba.getText().toString();
                    staraVred = (tekst.equals("")) ? 0 : Float.parseFloat(tekst);
                    indexNiza = indexSusednog_VecegClanaNiza(staraVred, nizStacionaza_tla_X);
                    novaVred = nizStacionaza_tla_X[indexNiza];
                    if (staraVred==novaVred && indexNiza <(nizStacionaza_tla_X.length-1))
                        novaVred = nizStacionaza_tla_X[++indexNiza];
                                    // if (BuildConfig.DEBUG)Log.e("TAG35", "novaVred=" +novaVred +" staraVred=" +staraVred+" indexStuba=" +indexStuba);

                    if (listaStacionaza_stubova.size() == 1
                            || indexStuba == listaStacionaza_stubova.size()-1)
                        mStacionazaStuba.setText(String.valueOf(novaVred));
                    else if (novaVred < Float.parseFloat(listaStacionaza_stubova.get(indexStuba + 1).toString()))
                        mStacionazaStuba.setText(String.valueOf(novaVred));
                    else
                        prikaziToast(getContext(),"Stacionaža stuba mora biti veća od stacionaže prethodnog");
                }
            });
        }
    }
    @Obfuscate//import
    private class AdapterRaspona extends RecyclerView.Adapter<HolderRaspona>{

        private AdapterRaspona(int index_stuba, int koja_operacija){
            if (koja_operacija == 1) { // dodavanje novog stuba
                listaStacionaza_stubova.add(index_stuba, stacionaza_stuba_default);
                listaVisina_stubova.add(index_stuba, visina_stuba_default);

                /////oznacavanje novog stuba/////////////////////////
                if (listaStacionaza_stubova.size() == 1)
                    listaOznakaStubova.add(index_stuba, 1);
                else {
                    listaOznakaStubova.add(index_stuba, "temp");
                    String pom = listaOznakaStubova.get(index_stuba - 1).toString();
                    int pomInt;
                    try {
                        pomInt = Integer.parseInt(pom);
                        izmeniOznakeStubova(index_stuba, String.valueOf(++pomInt), true);
                    } catch (NumberFormatException e) {
                        izmeniOznakeStubova(index_stuba, pom + "-A", false);
                        e.printStackTrace();
                    }
                }
                //////////////////////////////////////////////////////////

            }else if (koja_operacija == -1){
                listaStacionaza_stubova.remove(index_stuba);
                listaVisina_stubova.remove(index_stuba);
                listaOznakaStubova.remove(index_stuba);
            }

        }

        private AdapterRaspona(float x, float y){
            // if (BuildConfig.DEBUG)Log.e("TAGdodStub", "listaStacionaza_stubova " +listaStacionaza_stubova.toString());
            int index_stuba;
            if (x > nizStacionaza_stub_X[nizStacionaza_stub_X.length-1])
                // debug#15.01.18.
                // jer funkcija indexSusednog_VecegClanaNiza vraca poslednji clan i kada je X van opsega niza
                // i kad je X izmedju poslednjeg i predposlednjeg clana niza
                index_stuba = nizStacionaza_stub_X.length;
            else
                index_stuba = indexSusednog_VecegClanaNiza(x, nizStacionaza_stub_X);

            // if (BuildConfig.DEBUG)Log.e("TAGdodStub", "x=" +x+" index_stuba=" +index_stuba);

            if (index_stuba == nizStacionaza_stub_X.length){
                // debug#15.01.18.
                listaStacionaza_stubova.add(x);
                listaVisina_stubova.add(y);
            }
            else{
                listaStacionaza_stubova.add(index_stuba, x);
                listaVisina_stubova.add(index_stuba, y);
            }

            // if (BuildConfig.DEBUG)Log.e("TAGdodStub", "listaStacionaza_stubova " +listaStacionaza_stubova.toString());

            konvertovanjeListeXYkoordStubovaUnizove();// debug#15.01.18.

            // if (BuildConfig.DEBUG)Log.e("TAG40", "---frag index_stuba " +index_stuba+" listaStacionaza_stubova " + listaStacionaza_stubova.toString());

            /////oznacavanje novog stuba/////////////////////////
            if (index_stuba == 0)
                listaOznakaStubova.add(index_stuba, listaOznakaStubova.get(0).toString()+"-n");
            else {
                listaOznakaStubova.add(index_stuba, "temp");
                String pom = listaOznakaStubova.get(index_stuba - 1).toString();
                int pomInt;
                try {
                    pomInt = Integer.parseInt(pom);
                    izmeniOznakeStubova(index_stuba, String.valueOf(++pomInt), true);
                } catch (NumberFormatException e) {
                    izmeniOznakeStubova(index_stuba, pom + "-A", false);
                    e.printStackTrace();
                }
            }
            //////////////////////////////////////////////////////////

        }


        public AdapterRaspona( ){

        }

        @Override
        public HolderRaspona onCreateViewHolder(ViewGroup vg, int i){
            LayoutInflater li = LayoutInflater.from(getActivity());
            View v = li.inflate(R.layout.fragment_recycler_view_sigurnosne_visine,vg,false);
            return new HolderRaspona(v);
        }

        @Override
        public void onBindViewHolder(HolderRaspona holder, int pozicija){
            holder.mBrojStubnogMesta.setText(mojHtmlString("<u>stubno mesto: "+ listaOznakaStubova.get(pozicija)));
            if ((float)listaStacionaza_stubova.get(pozicija)== stacionaza_stuba_default &&  (float)listaVisina_stubova.get(pozicija)==visina_stuba_default){
                holder.mStacionazaStuba.setHint(mojHtmlString("<small><small>" +
                        "stacionaža" + "</small></small>"));
                holder.mVisinaStuba.setHint(mojHtmlString("<small><small>" +
                        "visina" + "</small></small>"));
            } else {
                holder.mStacionazaStuba.setText(listaStacionaza_stubova.get(pozicija).toString());
                holder.mVisinaStuba.setText(listaVisina_stubova.get(pozicija).toString());
            }
        }

        @Override
        public int getItemCount(){
            return listaStacionaza_stubova.size();
        }
    }

    private void reStartAdaptera(){
        /*
        broj stubova
         */
        mAdapterRaspona = new AdapterRaspona();
        mRecyclerView.setAdapter(mAdapterRaspona);
        animacijaZaRecycleView(mRecyclerView, true);
    }

    public void dodajStub(int indexStuba){
        // dodaje nov stub nakon stuba broj indexStuba
        brojStubova++;
        mAdapterRaspona = new AdapterRaspona(indexStuba, 1);
        mRecyclerView.setAdapter(mAdapterRaspona);
    }

    public void dodajStub(float x, float y){
        // dodaje nov stub sa koord x,y
         if (BuildConfig.DEBUG)Log.e("TAG40", "frag dodajStub "+x+" "+y);
        brojStubova++;
        mAdapterRaspona = new AdapterRaspona(x,y);
        mRecyclerView.setAdapter(mAdapterRaspona);
    }

    private void obrisiStub(int indexStuba){
        // brise stub broj indexStuba
        brojStubova--;
        mAdapterRaspona = new AdapterRaspona(indexStuba, -1);
        mRecyclerView.setAdapter(mAdapterRaspona);
    }

    public  void ucitajUzduzniProfilTerana(Uri selectedfile_trasa_uri){

        long startTime = System.currentTimeMillis();

        InputStream inputStream;
        try {
            inputStream = getActivity().getContentResolver()
                    .openInputStream(selectedfile_trasa_uri);
        } catch (FileNotFoundException e) {
            AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
            ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
            ad.setTitle(mojHtmlString("<b><i>P A Z NJ A"));
            ad.setMessage(mojHtmlString("Fajl <b><i><u>" + nazivTXT_trasa + "</b></i></u> nije moguce otvoriti"));
            ad.show();
            ucitanTXT_trasa_flag = false;
            e.printStackTrace();
            return;
        }

        String redIzFajla="";
        Scanner scanFajla = new Scanner(inputStream);
        Scanner scanStringa = new Scanner(redIzFajla);

        int brojTacaka=0;
        while (scanFajla.hasNext()) {
            redIzFajla = scanFajla.nextLine();
            redIzFajla=redIzFajla.trim();
            if (redIzFajla.startsWith(";") || redIzFajla.equals(""))
                continue;
            brojTacaka++;
        }
        nizStacionaza_tla_X = new float[brojTacaka];
        nizKota_tla_Y = new float[brojTacaka];


        try {   // da bi ponovo mogao da citam iz inputStream
            inputStream = getActivity().getContentResolver()
                    .openInputStream(selectedfile_trasa_uri);
            scanFajla = new Scanner(inputStream);
        } catch (FileNotFoundException e) {return;}


        int i = 0;
        while (scanFajla.hasNext()) {

            redIzFajla = scanFajla.nextLine();
            redIzFajla=redIzFajla.trim();
            redIzFajla = redIzFajla.replace(',' , '.'); //!@#11

            if (redIzFajla.startsWith(";") || redIzFajla.equals(""))
                continue;

            scanStringa = new Scanner(redIzFajla);

            /**
             * scanStringa.nextFloat(); je presporo, zato koristmo parseFloat
             * takodje nextFloat() zavisi od Locale (decimalnog simbola) ->
             *      -> Scanner scanner = new Scanner(redIzFajla).useLocale(Locale.getDefault());
             * a parseFloat uvek trazi . kao decimalni simbol !@#11
            */
            nizStacionaza_tla_X[i]= Float.parseFloat(scanStringa.next());
            nizKota_tla_Y[i]=Float.parseFloat(scanStringa.next());

            i++; // ne radimo sa for jer bi onda brojili i iteracije gde imamo continue
        }


        scanStringa.close();
        scanFajla.close();
        stacionaza_max = nizStacionaza_tla_X[nizStacionaza_tla_X.length-1];
        stacionaza_min = nizStacionaza_tla_X[0];
        ucitanTXT_trasa_flag = true;

         if (BuildConfig.DEBUG)Log.e("TAG36", "ukupno time[s]= " + ( (System.currentTimeMillis()-startTime)/1000  ) );
    }

    public  void ucitajRasporedStubova(Uri selectedfile_STUBOVI_uri){

        InputStream inputStream;
        try {
            inputStream = getActivity().getContentResolver()
                    .openInputStream(selectedfile_STUBOVI_uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
            ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
            ad.setTitle(mojHtmlString("<b><i>P A Z NJ A"));
            ad.setMessage(mojHtmlString("Fajl <b><i><u>" + nazivTXT_STUBOVI + "</b></i></u> nije moguce otvoriti"));
            ad.show();
            e.printStackTrace();
            ucitanTXT_STUB_flag = false;
            return;
        }

        listaStacionaza_stubova = new ArrayList<>();
        listaVisina_stubova = new ArrayList<>();
        listaOznakaStubova = new ArrayList<>();
        Scanner scanFajla, scanStringa;
        String redIzFajla="";

        scanFajla = new Scanner(inputStream);
        scanStringa = new Scanner(redIzFajla);

//////////////////////////provera da li je decimalni znak . ili , ///////v7.0.3//////////////////
        char decimalSeparator = ','; ///// za sve osim eng i usa
        NumberFormat nf = NumberFormat.getNumberInstance();
        if (nf instanceof DecimalFormat){
            DecimalFormatSymbols sym = ((DecimalFormat) nf).getDecimalFormatSymbols();
            decimalSeparator = sym.getDecimalSeparator();
             if (BuildConfig.DEBUG)Log.e("TAG9", "decimalSeparator=  " +decimalSeparator);
        }
        Locale local = Locale.getDefault();
         if (BuildConfig.DEBUG)Log.e("TAG9", "local=  " +local);
/////////////////////////////////////////////////////////////////////////////////
        int indexStuba=0;
        while (scanFajla.hasNext()) {
            redIzFajla = scanFajla.nextLine();
            redIzFajla=redIzFajla.trim();

            if (redIzFajla.startsWith(";") || redIzFajla.equals("")) {
                continue; //Exit this iteration if line starts with ;
            }

            if (decimalSeparator == ',')
                redIzFajla = redIzFajla.replace('.' , ',');

                            // if (BuildConfig.DEBUG)Log.e("TAG7", "redIzFajla  " +redIzFajla);
            scanStringa = new Scanner(redIzFajla);
            listaStacionaza_stubova.add(scanStringa.nextFloat());
            listaVisina_stubova.add(scanStringa.nextFloat());

            indexStuba++;/** pocinje od 1 :) */
            if (scanStringa.hasNext())
                listaOznakaStubova.add(scanStringa.next());
            else
                listaOznakaStubova.add(indexStuba);
        }

        scanStringa.close();
        scanFajla.close();

        nizStacionaza_stub_X = konvertovanje_Float_lista_u_float_niz(listaStacionaza_stubova);
        //nizVisinaStubova = konvertovanje_Float_lista_u_float_niz(listaVisina_stubova);

         if (BuildConfig.DEBUG)Log.e("TAG10", "nizStacionaza_stub_X = " + Arrays.toString(nizStacionaza_stub_X));
        // if (BuildConfig.DEBUG)Log.e("TAG10", "nizVisinaStubova = " + Arrays.toString(nizVisinaStubova));

        ucitanTXT_STUB_flag = nizStacionaza_stub_X.length != 0;
    }

    public static float[] konvertovanje_Float_lista_u_float_niz(List<Float> lista){
        float niz[] = new float[lista.size()];
        for (int i = 0; i < niz.length; i++)
            niz[i] = lista.get(i);
        return niz;
    }

    public static int indexSusednog_ManjegClanaNiza (float tacka, float niz[]){
        /** vazi ako je niz rastuci*/

        if (tacka <= niz[0]) { // van opsega (u minusu)
                            // if (BuildConfig.DEBUG)Log.e("TAGgreska","tacka=" + tacka +" niz[0]="+niz[0]+" return "+0);
            return 0;

        }else if (tacka >= niz[niz.length - 1]) { // van opsega (u plusu)
                            // if (BuildConfig.DEBUG)Log.e("TAGgreska","tacka=" + tacka +" niz[POSLEDNJI]="+niz[niz.length - 1]+" return "+(niz.length - 1));
            return niz.length - 2; // ako vraca -1, pravio bi gresku u  [indexStuba + 1]

        }else{ // u opsegu
            for (int i = 1; i < niz.length; i++) {
                if (niz[i] == tacka) {
                            // if (BuildConfig.DEBUG)Log.e("TAGgreska","tacka=" + tacka +" niz["+i+"]="+niz[i]+" return "+i);
                    return i; // u pitanju je tacka koja se poklapa sa tackom stuba
                }else if (niz[i] > tacka) {
                            // if (BuildConfig.DEBUG)Log.e("TAGgreska","tacka=" + tacka +" niz["+i+"]="+niz[i]+" return "+(i-1));
                    return i - 1;
                }
            }
        }

         if (BuildConfig.DEBUG)Log.e("TAGgreska","GRESKA indexSusednog_ManjegClana()  vraca -1");
        return nepostoji; // ako je neka greska
    }

    public static int indexSusednog_VecegClanaNiza (float tacka, float niz[]){
        // vazi ako je niz rastuci
        if (tacka<=niz[0])
            return 0;

        else if (tacka>=niz[niz.length-1]) {
             if (BuildConfig.DEBUG)Log.e("TAG40", "index tacka="+tacka+"  niz[niz.length-1]="+niz[niz.length-1]);
            return niz.length - 1;
        }
        else{
            for (int i = 1; i < niz.length; i++) {
                if (niz[i] >= tacka)
                    return i ;
            }
        }

         if (BuildConfig.DEBUG)Log.e("TAGgreska","GRESKA indexSusednog_VecegClana()  vraca -1");
        return nepostoji; // ako je neka greska
    }

    public static float linearnaInterpolacija(float tacka_X, float niz_X[], float niz_Y[]){

                    // if (BuildConfig.DEBUG)Log.e("TAG11", "niz_X[]= " + Arrays.toString(niz_X) );
                    // if (BuildConfig.DEBUG)Log.e("TAG11", "niz_Y[]= " + Arrays.toString(niz_Y) );
        int index0 = indexSusednog_ManjegClanaNiza (tacka_X, niz_X);
        int index1 = indexSusednog_VecegClanaNiza (tacka_X, niz_X);

         if (BuildConfig.DEBUG)Log.e("TAG35", "tacka_X= " +tacka_X +" index0= " +index0 + " index1= " +index1 );
        if (index0==index1) // u pitanju je kota stuba koja se poklapa sa unesenom kotom tla
            return niz_Y[index0];

        float x0 = niz_X[index0];
        float x1 = niz_X[index1];

        float y0 = niz_Y[index0];
        float y1 = niz_Y[index1];

        float x = tacka_X;
        float y; // to se trazi

        y = y0 + (x - x0) * ( (y1-y0) / (x1-x0) );

        return y;

    }

    private void konvertovanjeListeXYkoordStubovaUnizove(){

        nizStacionaza_stub_X = konvertovanje_Float_lista_u_float_niz(listaStacionaza_stubova);
        nizKota_stubova = new float[nizStacionaza_stub_X.length];
        for (int i = 0; i < nizKota_stubova.length; i++) {
            nizKota_stubova[i] = linearnaInterpolacija( nizStacionaza_stub_X[i],
                    nizStacionaza_tla_X, nizKota_tla_Y);
        }

        nizKota_vesanja_uzeta_Y = nizKota_stubova.clone();
        for (int i = 0; i < nizKota_vesanja_uzeta_Y.length; i++) {
            nizKota_vesanja_uzeta_Y[i] = nizKota_stubova[i]
                                              + konvertovanje_Float_lista_u_float_niz(listaVisina_stubova)[i];
        }
    }

    public static boolean nizJeRastuci(float [] niz){
        if (niz.length <= 1)
            return true;
        for (int i = 1; i < niz.length; i++)
            if (niz[i-1] >= niz[i]) // nije ni jednako, vec uvek vece
                return false;
        return true;
    }


    public static double[] izracunajCosFi( double [] rasponi, float [] nizStacionaza_uze){

        double [] nizCosFi = new double[rasponi.length];

        for (int i = 0; i < nizCosFi.length ; i++) {
            nizCosFi[i] = rasponi[i] / Math.sqrt(Math.pow(rasponi[i], 2) +
                    Math.pow(nizStacionaza_uze[i] - nizStacionaza_uze[i+1], 2));
        }

        return nizCosFi;
        // if (BuildConfig.DEBUG)Log.e("TAG3","raspon = "+raspon+" visinaU1 = "+visinaU1+" visinaU2 = "+ visinaU2+" cosFi = "+cosFi);
    }

    public static String getFileName_fromUri(Uri uri, Context c) {
        String fileName = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = c.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    fileName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (fileName == null) {
            fileName = uri.getPath();
            int cut = fileName.lastIndexOf('/');
            if (cut != -1) {
                fileName = fileName.substring(cut + 1);
            }
        }
        return fileName;
    }

    public static float[] izracunajVisinskeRazlike( float [] nizKota){

        float [] nizRazlika = new float[nizKota.length-1];

        for (int i = 0; i < nizRazlika.length ; i++) {
            nizRazlika[i] = nizKota[i+1] - nizKota[i];
        }

        return nizRazlika;
        // if (BuildConfig.DEBUG)Log.e("TAG3","raspon = "+raspon+" visinaU1 = "+visinaU1+" visinaU2 = "+ visinaU2+" cosFi = "+cosFi);
    }


    private void izmenaOznakeStuba_dialog(final int indexStuba){
        AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
        ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
        ad.setMessage(mojHtmlString("<i>Izmeni oznaku stuba:"));

        /**dinamicki layout :)*/
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                0, LinearLayout.LayoutParams.MATCH_PARENT);
        params.weight=1;

        final LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);

        final EditText inputEditText = new EditText(getContext());
        inputEditText.setLayoutParams(params);
        inputEditText.setHint(listaOznakaStubova.get(indexStuba).toString());

        final CheckBox checkBox = new CheckBox(getContext());
        checkBox.setLayoutParams(params);
        checkBox.setChecked(false);
        checkBox.setText("prilagodi i naredne");

        linearLayout.addView(inputEditText);
        linearLayout.addView(checkBox);
        ad.setView(linearLayout);

        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        izmeniOznakeStubova(indexStuba,
                                            inputEditText.getText().toString(),
                                            checkBox.isChecked());
                    }
                });
        ad.setButton(AlertDialog.BUTTON_NEGATIVE, "Odustani",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        ad.show();


    }

    private void izmeniOznakeStubova(int indexPocetnogStuba, String oznakaPocetnogStuba,
                                                boolean prilagodiNaredne){
        if (oznakaPocetnogStuba.equals(""))
            return;

        // u pocetni stub upisujemo string, ma kakav
        listaOznakaStubova.set(indexPocetnogStuba, oznakaPocetnogStuba );
        reStartAdaptera();

        if (prilagodiNaredne) {
            // za sledece stubove
            if (indexPocetnogStuba<(listaOznakaStubova.size() - 1/**nije poslednji*/) ) {
                int noveOznake;
                try {
                    noveOznake = Integer.parseInt(oznakaPocetnogStuba);
                    for (int i = indexPocetnogStuba+1; i < listaOznakaStubova.size(); i++)
                        listaOznakaStubova.set(i, ++noveOznake);
                    reStartAdaptera();
                } catch (NumberFormatException e){e.printStackTrace();}
            }
        }
    }

}


/* ne koriscene funkcije
    public String getFolderName_fromUri(Uri contentUri) {
        String result;
        Cursor cursor = getActivity().getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private static float[] konvertovanje_Double_lista_Float_niz( List<Double> lista){
        float niz[] = new float[lista.size()];
        for (int i = 0; i < niz.length; i++)
            niz[i] = lista.get(i).floatValue();
        return niz;
    }

    private static float linearnaInterpolacijaOpste (float x, float x0, float x1, float y0, float y1){
        return y0 + (x - x0) * ( (y1-y0) / (x1-x0) );
    }
*/
