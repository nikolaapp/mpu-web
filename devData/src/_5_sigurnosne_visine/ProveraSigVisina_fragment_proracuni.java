package nsp.mpu._5_sigurnosne_visine;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu._2_tabela_ugiba.TabelaUgibaUnos_Fragment;
import nsp.mpu._2_tabela_ugiba.TabeleUgibaPrikaz_Fragment;
import nsp.mpu.database.PODACI_ListaTipovaUzadi;
import nsp.mpu.database.PODACI_TipStuba;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.AnimacijeKlasa;
import nsp.mpu.pomocne_klase.JednacinaStanja;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static android.app.Activity.RESULT_OK;
import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Activity.kreirajFajlZaSave;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Activity.sig_visina;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Activity.sig_visinaZgrade;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Fragment.indexSusednog_ManjegClanaNiza;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Fragment.indexSusednog_VecegClanaNiza;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Fragment.konvertovanje_Float_lista_u_float_niz;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Fragment.linearnaInterpolacija;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_fragment_proracuni.IzmenaLayouta.sakri;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_fragment_proracuni.IzmenaLayouta.stubovi;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_fragment_proracuni.IzmenaLayouta.visine;
import static nsp.mpu._5_sigurnosne_visine.StubnoMesto.getBool_potrebanUpdate;
import static nsp.mpu._5_sigurnosne_visine.StubnoMesto.getNizStubnimMesta;
import static nsp.mpu._5_sigurnosne_visine.StubnoMesto.setBool_potrebanUpdate;
import static nsp.mpu._5_sigurnosne_visine.StubnoMesto.setNizStubnihMesta;
import static nsp.mpu.database.PODACI_ListaTipovaStubova.getSingltonStubova;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animacijaZaRecycleView;
import static nsp.mpu.pomocne_klase.JednacinaStanja.DodatnoOptSnegLed;
import static nsp.mpu.pomocne_klase.JednacinaStanja.IdealniCosFi;
import static nsp.mpu.pomocne_klase.JednacinaStanja.IdealniRaspon;
import static nsp.mpu.pomocne_klase.StaticPodaci.getDecimalSeparator;
import static nsp.mpu.pomocne_klase.StaticPodaci.getSharPref;
import static nsp.mpu.pomocne_klase.StaticPodaci.mojHtmlString;
import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziWarnToast;
import static nsp.mpu.pomocne_klase.StaticPodaci.stub_12_1000;
import static nsp.mpu.pomocne_klase.StaticPodaci.stub_12_315;

@Obfuscate
public class ProveraSigVisina_fragment_proracuni extends Fragment {
    public static final String ARGProfilTerena1 = "ARGProfilTerena1";
    public static final String ARGProfilTerena2 = "ARGProfilTerena2";
    public static final String ARGProfilTerena3 = "ARGProfilTerena3";
    public static final String ARGProfilTerena4 = "ARGProfilTerena4";
    public static final String ARGProfilTerena5 = "ARGProfilTerena5";
    public static final String ARGProfilTerena6 = "ARGProfilTerena6";
    public static final String ARGProfilTerena7 = "ARGProfilTerena7";
    public static final String ARGProfilTerena8 = "ARGProfilTerena8";
    public static final String ARGProfilTerena9 = "ARGProfilTerena9";
    public static final String ARGProfilTerena10 = "ARGProfilTerena10";
    public static final String ARGProfilTerena11 = "ARGProfilTerena11";
    public static final String ARGProfilTerena12 = "ARGProfilTerena12";
    private float [] nizStacionaza_tlo_X, nizKota_tlo_Y;
    private float [] nizStacionaza_STUB_X, nizKota_STUBiUZE_Y;
    private float [] nizStacionaza_lancanica_X, nizKota_lancanica_Y;
    private float parametarLancanice, parametarLancanice_5;
    private boolean crtanje_ugiba;
    private String naslovCrteza;

    private String oznakaUzeta;
    private double koefDodOpt, sigma0;
    private int temperatura;
    PODACI_TipUzeta mTipUzeta;
    double[] nizRaspona,nizCosFi, nizGravitacionihRaspona;
    //List listaTemenaLanc;

    private ArrayList listaOznakaStubova;
    private ArrayList listaVisina_stubova;

    JednacinaStanja js; // koristimo i za SacuvajGrafik konstruktor

    public static ProveraSigVisina_fragment_proracuni nacrtajUzduzniProfilTerena
            (float[] nizStacionaza, float[] nizKota, String naslovCrteza){
        Bundle args = new Bundle();
        args.putFloatArray(ARGProfilTerena1, nizStacionaza);
        args.putFloatArray(ARGProfilTerena2, nizKota);
        args.putString(ARGProfilTerena4, naslovCrteza);
        args.putBoolean(ARGProfilTerena5, false); // crtamo samo profil

        ProveraSigVisina_fragment_proracuni newFrag = new ProveraSigVisina_fragment_proracuni();
        newFrag.setArguments(args);
        return newFrag;
    }

    public static ProveraSigVisina_fragment_proracuni nacrtajUzduzniProfil_terena_ugiba_jedStanja
            (float[] nizStacionaza_tla, float[] nizKota_tla, String naslovCrteza,
             float[] nizStacionaza_stub, float[] nizKota_stub,
             ArrayList listaOznakaStubova, ArrayList listaVisina_stubova,
             String oznakaUzeta, double sigma0, double koefDodOpt, int temp ){
        Bundle args = new Bundle();
        args.putFloatArray(ARGProfilTerena1, nizStacionaza_tla);
        args.putFloatArray(ARGProfilTerena2, nizKota_tla);
        args.putString(ARGProfilTerena4, naslovCrteza);
        args.putBoolean(ARGProfilTerena5, true); // crtamo i profil i ugibe
        //args.putDouble(ARGProfilTerena6, parametarLancanice);
        args.putFloatArray(ARGProfilTerena7, nizStacionaza_stub);
        args.putFloatArray(ARGProfilTerena8, nizKota_stub);

        args.putString(ARGProfilTerena9, oznakaUzeta);
        args.putDouble(ARGProfilTerena10, sigma0);
        args.putDouble(ARGProfilTerena11, koefDodOpt);
        args.putInt(ARGProfilTerena12, temp);

        args.putParcelableArrayList("listaOznakaStubova",listaOznakaStubova);
        args.putParcelableArrayList("listaVisina_stubova",listaVisina_stubova);


        ProveraSigVisina_fragment_proracuni newFrag = new ProveraSigVisina_fragment_proracuni();
        newFrag.setArguments(args);
        return newFrag;
    }


    private TextView mRezultati, mRezultatiWycisk;
    private EditText mUnosXkoord;
    private  FrameLayout mFL;
    private CrtanjeFunkcija  mFunkcija;

    private float koordX, koordY, razmeraX;
    private String nazivFajlaZaGrafik;
    private File fajlZaGrafik;

    private static List<float[]> listaWycisk_stacionaze,listaWycisk_kote, listaRazlikaKotaWycisk;
    private float [] nizStacionaza_lancanica_X_Wycisk, nizKota_lancanica_Y_Wycisk;
    private float [] nizKota_lancanica_Y_5;
    private float [] nizRazlikaKota_wycisk;

    private static final int crtajTrasu = 0;
    private static final int crtajLancanice = 1;
    private static final int crtajLancaniceAnimiraj = 11;
    private static final int crtajVisine = 2;

    private int visinaGrafika, sirinaGrafika;

    private boolean crtajSamoTrasu;

    private ImageButton mDodajTacku;
    private static List<Float> listaTacakaZaPrikazVisina;


    @Override
    public void onCreateOptionsMenu(Menu m, MenuInflater i) {
        super.onCreateOptionsMenu(m, i);
        i.inflate(R.menu.meni_sig_visine_prikaz, m);
        // ctrl+shift+F <bool name="abc_config_actionMenuItemAllCaps">false</bool>
        MenuItem prikaziVisine = m.findItem(R.id.menu_sig_visine_prikaz_prikazi);
        MenuItem dodajstub = m.findItem(R.id.menu_sig_visine_prikaz_dodajstub);
        MenuItem sacuvajSliku = m.findItem(R.id.menu_sig_visine_prikaz_save);
        MenuItem prikaziTabeluUgiba = m.findItem(R.id.menu_sig_visine_prikaz_tabela_ugiba);
        MenuItem lancanicaWycisk = m.findItem(R.id.menu_sig_visine_prikaz_wycisk_lancanica);
        MenuItem animirajGrafik = m.findItem(R.id.menu_sig_visine_prikaz_ponovo_animiraj);
        MenuItem prikazOdabranihTacaka = m.findItem(R.id.menu_sig_visine_prikaz_tacke_za_grafik);
        MenuItem ucitajObjekte = m.findItem(R.id.menu_sig_visine_ucitaj_objekte);
        MenuItem naprezanjeStuba = m.findItem(R.id.menu_sig_visine_prikaz_naprezanje_stuba);
        MenuItem sigRazmaci = m.findItem(R.id.menu_sig_visine_prikaz_sig_razmaci);
        MenuItem otklonIzolatora = m.findItem(R.id.menu_sig_visine_prikaz_otklon_izolatora);
        MenuItem predmerIpredracun = m.findItem(R.id.menu_sig_visine_prikaz_predmerIpredracun);
        if (crtanje_ugiba) {
            prikaziVisine.setVisible(true);
            dodajstub.setVisible(true);
            sacuvajSliku.setVisible(true);
            prikaziTabeluUgiba.setVisible(true);
            naprezanjeStuba.setVisible(true);
            sigRazmaci.setVisible(true);
            otklonIzolatora.setVisible(true);
            predmerIpredracun.setVisible(true);
            lancanicaWycisk.setVisible(true);
            animirajGrafik.setVisible(true);
            prikazOdabranihTacaka.setVisible(true);
            ucitajObjekte.setVisible(false);
        }else{
            prikaziVisine.setVisible(false);
            dodajstub.setVisible(false);
            prikaziTabeluUgiba.setVisible(false);
            naprezanjeStuba.setVisible(false);
            sigRazmaci.setVisible(false);
            otklonIzolatora.setVisible(false);
            predmerIpredracun.setVisible(false);
            lancanicaWycisk.setVisible(false);
            prikazOdabranihTacaka.setVisible(false);
            animirajGrafik.setVisible(true);
            sacuvajSliku.setVisible(true);
            ucitajObjekte.setVisible(true);
        }
    }

    private Uri selectedfile_objekti_uri;
    private InputStream inputStream_objekti;
    private String nazivTXT_objekti;
    //private boolean ucitanTXT_objekti_flag;
    private static /** @#$ 1*/ List<float[]> listaObjekti_X,listaObjekti_Y;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 999 && resultCode == RESULT_OK) {
            selectedfile_objekti_uri = intent.getData();
            // if (BuildConfig.DEBUG)Log.e("TAG7", "selectedfile_trasa_uri ---   " + selectedfile_trasa_uri );
            try {
                inputStream_objekti = getActivity().getContentResolver()
                        .openInputStream(selectedfile_objekti_uri);

                nazivTXT_objekti = ProveraSigVisina_Fragment.getFileName_fromUri(selectedfile_objekti_uri, getContext()).replace(".txt", "");

                ucitajObjekte(inputStream_objekti);

                nacrtajGrafik(crtajTrasu);



            } catch (FileNotFoundException e) {
                e.printStackTrace();
                AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
                ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
                ad.setTitle(mojHtmlString("<b><i>P A Z NJ A"));
                ad.setMessage(mojHtmlString("Fajl <b><i><u>" + nazivTXT_objekti + "</b></i></u> nije moguce otvoriti"));
                ad.show();
                //mUcitajTrasu_TXT.setText("Greska");
                //mUcitajRaporedStubova_TXT.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_sig_visine_prikaz_prikazi:
                prikaziLayoutVrednostiVisina();
                return true;
            case R.id.menu_sig_visine_prikaz_dodajstub:
                prikaziLayoutDodStub();
                return true;
            case R.id.menu_sig_visine_prikaz_save:
                sacuvajGrafik_dialog();
                return true;
            case R.id.menu_sig_visine_prikaz_tabela_ugiba:
                prikaziTabeluUgiba();
                return true;
            case R.id.menu_sig_visine_prikaz_naprezanje_stuba:
                prikaziTabeluNaprezanjaStuba();
                return true;
            case R.id.menu_sig_visine_prikaz_sig_razmaci:
                prikaziTabeluSigRazmaka();
                return true;
            case R.id.menu_sig_visine_prikaz_otklon_izolatora:
                prikaziDialogOtklonaIzolatora();
                return true;
            case R.id.menu_sig_visine_prikaz_predmerIpredracun:
                prikaziPredmerIPredracun();
                return true;
            case R.id.menu_sig_visine_prikaz_wycisk_lancanica:
                odaberiLancaniceWycisk();
                return true;
            case R.id.menu_sig_visine_prikaz_ponovo_animiraj:
                nacrtajGrafik(crtajLancaniceAnimiraj);
                return true;
            case R.id.menu_sig_visine_prikaz_tacke_za_grafik:
                prikaziTacke_dialog();
                return true;
            case R.id.menu_sig_visine_ucitaj_objekte:
                Intent intent = new Intent()
                        .setType("text/plain")
                        .setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Odaberi fajl sa podacima o objektima na trasi"), 999);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public  void ucitajObjekte(InputStream inputStream){

        char decimalSeparator = getDecimalSeparator();

        listaObjekti_X =  new ArrayList<>();
        listaObjekti_Y =  new ArrayList<>();

        List<Float> lista_stacionaza_X = new ArrayList<>();
        List<Float> lista_kota_Y = new ArrayList<>();

        Scanner scanFajla, scanStringa;
        String redIzFajla="";

        scanFajla = new Scanner(inputStream);
        scanStringa = new Scanner(redIzFajla);

        float pomVar_x, pomVar_y, pomVar_y_visinaTla;
        while (scanFajla.hasNext()) { // dokle god ima sledeci red
            redIzFajla = scanFajla.nextLine();
            redIzFajla=redIzFajla.trim();

            if (redIzFajla.startsWith(";") || redIzFajla.equals(""))
                continue; //Exit this iteration if line starts with ;

            if (decimalSeparator == ',')
                redIzFajla = redIzFajla.replace('.' , ',');

                                                            // if (BuildConfig.DEBUG)Log.e("TAG7", "redIzFajla  " +redIzFajla);
            scanStringa = new Scanner(redIzFajla);

            try {
                lista_stacionaza_X.clear();
                lista_kota_Y.clear();
                while (scanStringa.hasNext()) { // cita do kraja reda (jedan objekat)
                    pomVar_x = scanStringa.nextFloat();
                    lista_stacionaza_X.add(pomVar_x);

                    pomVar_y = scanStringa.nextFloat();
                    pomVar_y_visinaTla = linearnaInterpolacija( pomVar_x,nizStacionaza_tlo_X, nizKota_tlo_Y);
                    if (pomVar_y < pomVar_y_visinaTla) // ako su date visine objekta a ne kote
                        pomVar_y += pomVar_y_visinaTla;
                    lista_kota_Y.add(pomVar_y);
                }
            } catch (Exception e){
                 if (BuildConfig.DEBUG)Log.e("TAG33", "NIJE MOGUCE CITANJE PODATAKA O OBJEKTU: " +redIzFajla);
                prikaziToast(getContext(),"NIJE MOGUCE CITANJE PODATAKA O OBJEKTU: " +redIzFajla );
                e.printStackTrace();
                //ucitanTXT_objekti_flag = false;
                continue; /** da bi nastavio da cita sledeci red*/
            }

            //// mora ovako, inace bi stalno ubacivao reference iste list pa bi sve
            //// pojedinacne liste bile iste
            // posto je i Wycisk lista nizova, onda sam stvaio i ovde isto lista nizova
            listaObjekti_X.add(konvertovanje_Float_lista_u_float_niz(lista_stacionaza_X) );
            listaObjekti_Y.add(konvertovanje_Float_lista_u_float_niz(lista_kota_Y));

        }

        scanStringa.close();
        scanFajla.close();

                                    /*for (int i = 0; i < listaObjekti_X.size(); i++) {
                                         if (BuildConfig.DEBUG)Log.e("TAG34","---ucitajObjekte listaObjekti_X = "+listaObjekti_X);
                                         if (BuildConfig.DEBUG)Log.e("TAG34","---ucitajObjekte listaObjekti_Y = " +listaObjekti_Y);
                                    }*/

        //ucitanTXT_objekti_flag = true;

    }

    private void prikaziTacke_dialog(){
        final AlertDialog dialogBuilder = new AlertDialog.Builder(getActivity()).create();
        dialogBuilder.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
        dialogBuilder.setTitle(mojHtmlString("<i>Odabrane tacke:"));
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.fragment_podaci_o_uzadima, null);
        dialogBuilder.setView(dialogView);
        final RecyclerView recyclerview= (RecyclerView) dialogView.findViewById(R.id.f5_mesto_za_dodavanje_fragmenata);


        @Obfuscate//import
        class HolderDialoga extends RecyclerView.ViewHolder {
            TextView stacionaza;
            ImageButton delete;

            private HolderDialoga(View v) {
                super(v);
                stacionaza = (TextView) this.itemView.findViewById(R.id.f7dialog_stacionaza);

                delete = (ImageButton) this.itemView.findViewById(R.id.f7dialog_delete);
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listaTacakaZaPrikazVisina.remove(getAdapterPosition());
                        // ne radimo update postojeceg dijaloga nego prosto pravimo novi
                        dialogBuilder.dismiss();
                        prikaziTacke_dialog();
                    }
                });

            }
        }
        @Obfuscate//import
        class AdapterDialoga extends RecyclerView.Adapter<HolderDialoga>{

            public AdapterDialoga( ){}

            @Override
            public HolderDialoga onCreateViewHolder(ViewGroup vg, int i){
                LayoutInflater li = LayoutInflater.from(getActivity());
                View v = li.inflate(R.layout.dialog_recycler_view_textview_imagebutton,vg,false);
                return new HolderDialoga(v);
            }

            @Override
            public void onBindViewHolder(HolderDialoga holder, int pozicija){
                holder.stacionaza.setText(String.format("%.2f", listaTacakaZaPrikazVisina.get(pozicija)));

            }

            @Override
            public int getItemCount(){
                return listaTacakaZaPrikazVisina.size();
            }
        }


        AdapterDialoga mAdapterDialoga = new AdapterDialoga();
        recyclerview.setAdapter(mAdapterDialoga);
        animacijaZaRecycleView(recyclerview, true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerview.setLayoutManager(layoutManager);

        dialogBuilder.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialogBuilder.dismiss();
                    }
                });
        dialogBuilder.show();
    }


    private void sacuvajGrafik_dialog(){
        final String tipFajla = getSharPref().getString("PNG_PDF",".png"); // ako nema sacuvana vrednost - default je .png

        AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
        ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
        ad.setTitle(mojHtmlString("<i>Sacuvaj grafik"));
        ad.setMessage(mojHtmlString("Unesi naziv pod kojim ce grafik biti sacuvan:"));

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_radio_edittext, null);
        ad.setView(dialogView);

        final EditText inputEditText = (EditText) dialogView.findViewById(R.id.dialog_edittext_nazivFajla);
        inputEditText.setText("grafik - "+naslovCrteza);

        RadioGroup radioGroup = (RadioGroup) dialogView.findViewById(R.id.dialog_radioGroup);
        switch (tipFajla) {
            case ".png":
                radioGroup.check(R.id.dialog_radioButton_png);
                break;
            case ".pdf":
                radioGroup.check(R.id.dialog_radioButton_pdf);
                break;
            default:
                break;
        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
            {
                public void onCheckedChanged(RadioGroup group, int index)
                {
                    RadioButton odabranButon = (RadioButton)group.findViewById(index);
                    getSharPref().edit().putString( "PNG_PDF", odabranButon.getText().toString() ) .commit();
                }
            });


        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        nazivFajlaZaGrafik = new String( inputEditText.getText().toString() );
                        fajlZaGrafik = kreirajFajlZaSave(getContext(), nazivFajlaZaGrafik + getSharPref().getString("PNG_PDF",".png"));
                         if (BuildConfig.DEBUG)Log.e("TAG10", "fajlSaveStub_txt " + fajlZaGrafik.getAbsolutePath());

                         if (BuildConfig.DEBUG)Log.e("TAG34","sacuvajGrafik_dialog listaObjekti_X = "+listaObjekti_X);
                         if (BuildConfig.DEBUG)Log.e("TAG34","sacuvajGrafik_dialog listaObjekti_Y = " +listaObjekti_Y);
                        if (fajlZaGrafik.exists()){
                            new AlertDialog.Builder(getActivity())
                                    .setIcon(R.mipmap.ic_alert_crveno)
                                    .setTitle(mojHtmlString("<b><i>Paznja!"))
                                    .setMessage(mojHtmlString("Vec postoji fajl <b>" + nazivFajlaZaGrafik +
                                            "</b> Sacuvaj preko njega?"))
                                    .setPositiveButton("DA", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {

                                            nacrtajGrafik(crtajLancaniceAnimiraj);

                                            new SacuvajGrafik( getContext(),
                                                    nizStacionaza_tlo_X, nizKota_tlo_Y,
                                                    listaObjekti_X,listaObjekti_Y,
                                                    nizStacionaza_STUB_X, nizKota_STUBiUZE_Y,
                                                    nizStacionaza_lancanica_X,nizKota_lancanica_Y,
                                                    listaWycisk_stacionaze, listaWycisk_kote,
                                                    fajlZaGrafik, js, listaTacakaZaPrikazVisina,
                                                    listaOznakaStubova, listaVisina_stubova
                                                    );


                                        }
                                    })
                                    .setNegativeButton("NE", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            sacuvajGrafik_dialog();
                                        }
                                    })
                                    .show();
                        }
                        else{
                            nacrtajGrafik(crtajLancaniceAnimiraj);

                            new SacuvajGrafik( getContext(),
                                    nizStacionaza_tlo_X, nizKota_tlo_Y,
                                    listaObjekti_X,listaObjekti_Y,
                                    nizStacionaza_STUB_X, nizKota_STUBiUZE_Y,
                                    nizStacionaza_lancanica_X,nizKota_lancanica_Y,
                                    listaWycisk_stacionaze, listaWycisk_kote,
                                    fajlZaGrafik, js, listaTacakaZaPrikazVisina,
                                    listaOznakaStubova, listaVisina_stubova
                            );

                        }

                    }
                });
        ad.setButton(AlertDialog.BUTTON_NEGATIVE, "Odustani",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        ad.show();
    }

    private void prikaziLayoutVrednostiVisina(){
        izmenaLayoutaNaVrhu(visine);
        if (nizKota_lancanica_Y_Wycisk!=null)
            mRezultatiWycisk.setVisibility(View.VISIBLE);

        //visinePrikazane = true;
        mUnosXkoord.addTextChangedListener(new TextWatcher(){
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                 if (BuildConfig.DEBUG)Log.e("TAG","PRE PARSE temp= " + cs);
                try {
                    koordX = Float.parseFloat (cs.toString());
                } catch (NumberFormatException e) {
                    koordX=nizStacionaza_STUB_X[0];
                }

                if (koordX< nizStacionaza_STUB_X[0]
                        || koordX> nizStacionaza_STUB_X[nizStacionaza_STUB_X.length-1]){

                    koordX=nizStacionaza_STUB_X[0];
                    prikaziToast(getContext(),"Unesi x koordinatu u intervalu stacionaža stubova");
                }

            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override
            public void afterTextChanged(Editable e){
                if (crtanje_ugiba) {
                    ispisiRezultate(koordX);
                    nacrtajGrafik(crtajVisine);
                }
            }
        });

        mFL.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                float x_screen, y_screen;
                final int action = event.getAction();
                switch (action & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_DOWN: {
                        x_screen=event.getX();
                        y_screen=event.getY();
                        mUnosXkoord.setText(""+String.format("%.2f", x_screen*razmeraX));
                         if (BuildConfig.DEBUG)Log.e("TAG15", "x_screen    = "+ x_screen+" y_screen    = "+ y_screen);
                        break;
                    }

                    case MotionEvent.ACTION_MOVE:{
                        x_screen=event.getX();
                        y_screen=event.getY();
                        mUnosXkoord.setText(String.valueOf(x_screen*razmeraX));
                         if (BuildConfig.DEBUG)Log.e("TAG15", "x_screen    = "+ x_screen+" y_screen    = "+ y_screen);
                        break;
                    }
                }
                return true;
            }

        });

        mDodajTacku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (float f: listaTacakaZaPrikazVisina){
                    // ne ubavujemo ako vec postoji u listi
                    if (koordX==f)
                        return;
                }
                listaTacakaZaPrikazVisina.add(Float.valueOf(koordX)); // lista ne moze da sadrzi obican float
                prikaziToast(getContext(), "tacka sa stacionazom "+ String.format("%.2f",koordX) +" je dodata za prikaz na sacuvan grafik");
            }
        });

        //da bi odmah pokrenuli crtanje visine (time stopiramo drag/zoom)
        mUnosXkoord.setText(""+String.format("%.2f", nizStacionaza_STUB_X[0]*razmeraX));

    }

    private float visina_dodatogStuba=7;
    private void prikaziLayoutDodStub(){
        izmenaLayoutaNaVrhu(stubovi);

        //da bi odmah pokrenuli crtanje visine (time stopiramo drag/zoom)
        nacrtajGrafik(crtajVisine);

        visinaStuba.addTextChangedListener(new TextWatcher(){
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c){
                float pomVar;
                try {
                    pomVar = Float.parseFloat (cs.toString());
                    if (pomVar>0 || pomVar<50)
                        visina_dodatogStuba=pomVar;
                } catch (NumberFormatException e) {}
            }
            @Override public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
            @Override public void afterTextChanged(Editable e){}
        });

        mFL.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                float x_screen, y_screen;
                final int action = event.getAction();
                switch (action & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_DOWN: {
                        x_screen=event.getX();
                        y_screen=event.getY();
                        stacionaza.setText(""+String.format("%.2f", x_screen*razmeraX));
                        koordX=x_screen*razmeraX;
                        nacrtajGrafik(crtajVisine);
                         if (BuildConfig.DEBUG)Log.e("TAG15", "x_screen    = "+ x_screen+" y_screen    = "+ y_screen);
                        break;
                    }

                    case MotionEvent.ACTION_MOVE:{
                        x_screen=event.getX();
                        y_screen=event.getY();
                        stacionaza.setText(""+String.format("%.2f", x_screen*razmeraX));
                        koordX=x_screen*razmeraX;
                        nacrtajGrafik(crtajVisine);
                         if (BuildConfig.DEBUG)Log.e("TAG15", "x_screen    = "+ x_screen+" y_screen    = "+ y_screen);
                        break;
                    }
                }
                return true;
            }

        });

        dodajStub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (nizSadrziClan(koordX,nizStacionaza_STUB_X)){
                    prikaziToast(getContext(), "stub sa stacionazom "+ String.format("%.2f",koordX)+
                    " vec postoji!");
                    return;
                }

                setBool_potrebanUpdate(true);
                mCallBack.dodajNoviStub(koordX,visina_dodatogStuba);
                dodajStubUNizove(koordX,visina_dodatogStuba);
                //prikaziLayoutDodStub();
                prikaziToast(getContext(), "dodat je stub sa stacionazom "+ String.format("%.2f",koordX));
            }
        });
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mCallBack = null;
    }

    private void dodajStubUNizove(float x, float y){
        float[] nizXpom = nizStacionaza_STUB_X.clone();
        float[] nizYpom = nizKota_STUBiUZE_Y.clone();
      //  listaVisina_stubova;
        nizStacionaza_STUB_X = new float[nizStacionaza_STUB_X.length+1];
        nizKota_STUBiUZE_Y = new float[nizKota_STUBiUZE_Y.length+1];

        for (int i = 0; i < nizXpom.length; i++) {
            nizStacionaza_STUB_X[i]=nizXpom[i];
            nizKota_STUBiUZE_Y[i]=nizYpom[i];
        }
        nizStacionaza_STUB_X[nizStacionaza_STUB_X.length-1]= x;
        nizKota_STUBiUZE_Y[nizKota_STUBiUZE_Y.length-1]=
                y + linearnaInterpolacija(x, nizStacionaza_tlo_X,nizKota_tlo_Y);

        sortXYnizoveStubova();

        /** ovo mi ne treba - vec se dodaje oznaka na drugome mestu*/
        //listaOznakaStubova.add("novo");


        pripremiParametreZaLancanicu();
        nizKota_lancanica_Y = tackeLancanice(parametarLancanice);


        nacrtajGrafik(crtajLancaniceAnimiraj);
        izmenaLayoutaNaVrhu(stubovi);
        CrtanjeFunkcija.omogucenDragZoom = false;

    }

    DodajStub mCallBack;
    public interface DodajStub{
        void dodajNoviStub(float x, float y);
    }

    private void sortXYnizoveStubova(){
        float tempX, tempY;
         if (BuildConfig.DEBUG)Log.e("TAG40",Arrays.toString(nizStacionaza_STUB_X));
        for (int i = 0; i < nizStacionaza_STUB_X.length; i++)
            for (int j = 0; j < nizStacionaza_STUB_X.length; j++)
                if (nizStacionaza_STUB_X[i]<nizStacionaza_STUB_X[j]){
                    tempX = nizStacionaza_STUB_X[i];
                    tempY = nizKota_STUBiUZE_Y[i];

                    nizStacionaza_STUB_X[i]=nizStacionaza_STUB_X[j];
                    nizKota_STUBiUZE_Y[i]=nizKota_STUBiUZE_Y[j];

                    nizStacionaza_STUB_X[j]=tempX;
                    nizKota_STUBiUZE_Y[j]=tempY;
                }


         if (BuildConfig.DEBUG)Log.e("TAG40",Arrays.toString(nizStacionaza_STUB_X));
    }

    public static boolean nizSadrziClan(float clan , float[] niz){
        for (int i = 0; i < niz.length; i++)
            if (niz[i] == clan)
                return true;

        return false;
    }

    private void prikaziTabeluUgiba(){
        int[] nizTemp;

        nizTemp = mDaLiJeTablet?
                TabelaUgibaUnos_Fragment.pocetniNizTemp_Tablet.clone():
                TabelaUgibaUnos_Fragment.pocetniNizTemp.clone();

        boolean tempJeSadrzanaUNizu = false;
        for (int i = 0; i < nizTemp.length; i++) {
            if (nizTemp[i]== temperatura) {
                tempJeSadrzanaUNizu = true;
                break;
            }
        }
        if (!tempJeSadrzanaUNizu)
            nizTemp[nizTemp.length - 1] = temperatura;

        TabeleUgibaPrikaz_Fragment noviFrag_tabela = TabeleUgibaPrikaz_Fragment.
                noviFragmentTabUgbPrikaz(oznakaUzeta, sigma0, nizRaspona, nizCosFi,
                        koefDodOpt,nizTemp);

        FragmentManager fm = getActivity().getSupportFragmentManager();
        if (mDaLiJeTablet)
            fm.beginTransaction()
                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_drugog_fragmenta, noviFrag_tabela)
                    // !@#4.3.18 ovim je omoguceno da se vratimo na prethodni fragment
                    .addToBackStack(null)
                    .commit();
        else
            fm.beginTransaction()
                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_fragmenta, noviFrag_tabela)
                    .addToBackStack(null) // back dugme vraca na zamenjeni frag
                    .commit();
    }

    private void prikaziTabeluNaprezanjaStuba(){

        if (StubnoMesto.getNizStubnimMesta() == null) {
            prikaziWarnToast(getContext(), "Greška nizStubova je null");
            return;
        }

        TabelaNaprezanjaStuba noviFrag_tabela = TabelaNaprezanjaStuba.
                novFragment(oznakaUzeta,
                        sigma0, koefDodOpt, temperatura,
                        nizRaspona, listaOznakaStubova);

        FragmentManager fm = getActivity().getSupportFragmentManager();
        if (mDaLiJeTablet)
            fm.beginTransaction()
                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_drugog_fragmenta, noviFrag_tabela)
                    // !@#4.3.18 ovim je omoguceno da se vratimo na prethodni fragment
                    .addToBackStack(null)
                    .commit();
        else
            fm.beginTransaction()
                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_fragmenta, noviFrag_tabela)
                    .addToBackStack(null) // back dugme vraca na zamenjeni frag
                    .commit();

    }

    private void prikaziTabeluSigRazmaka(){

        if (StubnoMesto.getNizStubnimMesta() == null) {
            prikaziWarnToast(getContext(), "Greška nizStubova je null");
            return;
        }

        JednacinaStanja js_40 = new JednacinaStanja(mTipUzeta, sigma0,
                IdealniRaspon(nizRaspona,nizCosFi),
                IdealniCosFi(nizRaspona,nizCosFi),
                koefDodOpt, 40);
        double naprezanje_40 = js_40.getSigmaNOVO();
        float parametarLancanice_40 = (float) (naprezanje_40 / mTipUzeta.getSpecTezina());

        double[] nizTemenaLancanice_40  = new double[nizRaspona.length];
        double[] nizUgiba_40            = new double[nizRaspona.length];
        for (int i = 0; i < nizRaspona.length; i++) {
            nizTemenaLancanice_40[i] =  nadjiTemeLancanice(i, parametarLancanice_40,
                    nizStacionaza_STUB_X, nizKota_STUBiUZE_Y );
            nizUgiba_40[i] =  JednacinaStanja.UgibUcm(nizRaspona[i], nizCosFi[i], naprezanje_40, mTipUzeta.getSpecTezina());
        }

        TabelaSigRastojanja noviFrag_tabela = TabelaSigRastojanja.
                novFragment(oznakaUzeta, nizRaspona,
                        nizUgiba_40,listaOznakaStubova);

        FragmentManager fm = getActivity().getSupportFragmentManager();
        if (mDaLiJeTablet)
            fm.beginTransaction()
                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_drugog_fragmenta, noviFrag_tabela)
                    // !@#4.3.18 ovim je omoguceno da se vratimo na prethodni fragment
                    .addToBackStack(null)
                    .commit();
        else
            fm.beginTransaction()
                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_fragmenta, noviFrag_tabela)
                    .addToBackStack(null) // back dugme vraca na zamenjeni frag
                    .commit();

    }

    private void prikaziPredmerIPredracun(){

        if (StubnoMesto.getNizStubnimMesta() == null) {
            prikaziWarnToast(getContext(), "Greška nizStubova je null");
            return;
        }

        TabelaPredmerIPredracun noviFrag = TabelaPredmerIPredracun.
                novFragment(mTipUzeta.getOznakaUzeta(), nizRaspona,nizCosFi);

        FragmentManager fm = getActivity().getSupportFragmentManager();
        if (mDaLiJeTablet)
            fm.beginTransaction()
                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_drugog_fragmenta, noviFrag)
                    .addToBackStack(null)
                    .commit();
        else
            fm.beginTransaction()
                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_fragmenta, noviFrag)
                    .addToBackStack(null)
                    .commit();


    }

    private void prikaziDialogOtklonaIzolatora(){
        AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
        ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
        //ad.setTitle(mojHtmlString("<i>Proračun za viseće izolatore"));
        ad.setMessage(mojHtmlString("Unesi temperaturu za proračun otkolna <b><u>visećih izolatora</b></u> na nosećim stubovima:"));
        final EditText inputEditText = new EditText(getContext());
        inputEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
        inputEditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
        ad.setView(inputEditText);
        final int temperatura = -20;
        inputEditText.setHint(String.valueOf(temperatura));
        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int temperaturaUneto;
                        try {
                            temperaturaUneto = Integer.parseInt(inputEditText.getText().toString());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            temperaturaUneto = temperatura;
                        }
                        if (temperaturaUneto <-40 || temperaturaUneto > 80){
                            temperaturaUneto = temperatura;
                            prikaziToast(getContext(), "Temperatura mora biti u opsegu [-40,80]°C. Proračun je dat za: "+temperatura+"°C !");
                        }
                        prikaziTabeluOtklonaIzolatora(temperaturaUneto);
                    }
                });
        ad.show();
    }
    private void prikaziTabeluOtklonaIzolatora(int temperatura){

        if (StubnoMesto.getNizStubnimMesta() == null) {
            prikaziWarnToast(getContext(), "Greška nizStubova je null");
            return;
        }

        JednacinaStanja js = new JednacinaStanja(mTipUzeta, sigma0,
                IdealniRaspon(nizRaspona,nizCosFi),
                IdealniCosFi(nizRaspona,nizCosFi),
                koefDodOpt, temperatura);
        double naprezanje = js.getSigmaNOVO();

        double rezTezinaUzeta = mTipUzeta.getSpecTezina();
        if (temperatura==-5)
            rezTezinaUzeta += DodatnoOptSnegLed(koefDodOpt,
                    mTipUzeta.getPrecnik(), mTipUzeta.getPresek());

        float parametarLancanice = (float) (naprezanje / rezTezinaUzeta);

        double[] nizTemenaLancaniceOI  = new double[nizRaspona.length];
        //double[] nizUgiba            = new double[nizRaspona.length];
        for (int i = 0; i < nizRaspona.length; i++) {
            nizTemenaLancaniceOI[i] =  nadjiTemeLancanice(i, parametarLancanice,
                    nizStacionaza_STUB_X, nizKota_STUBiUZE_Y );
            //nizUgiba[i] =  JednacinaStanja.UgibUcm(nizRaspona[i], nizCosFi[i], naprezanje, tezinaUzeta);
        }

        double[] gravRaspOI = StaticPodaci.izracunajGravitacRaspone(nizRaspona, nizTemenaLancaniceOI);
        for (int i = 0; i < getNizStubnimMesta().length; i++)
            getNizStubnimMesta()[i].setGravRaspon_otklonIzol(gravRaspOI[i]);

/** mnozi se sa presekom da bi dobili naprezanje u daN/m */
        TabelaOtklonaIzolatora noviFrag = TabelaOtklonaIzolatora.
                novFragment(temperatura, mTipUzeta.getPresek() * rezTezinaUzeta, mTipUzeta.getPrecnik());

        FragmentManager fm = getActivity().getSupportFragmentManager();
        if (mDaLiJeTablet)
            fm.beginTransaction()
                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_drugog_fragmenta, noviFrag)
                    .addToBackStack(null)
                    .commit();
        else
            fm.beginTransaction()
                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_fragmenta, noviFrag)
                    .addToBackStack(null)
                    .commit();

    }


    private void odaberiLancaniceWycisk(){
        prikaziToast(getContext(),"Klikni na raspon za koji treba da se prikaze lancanica po Wycisk");

        CrtanjeFunkcija.omogucenDragZoom = false; /** namucio sam se dok se nisamo ovoga setio */
        mFL.setOnTouchListener(new View.OnTouchListener() {
            boolean ignorisiListener = false; // preko njega stopiramo listener nakon ACTION_DOWN
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (ignorisiListener)
                    return false;
                float x_screen;
                final int action = event.getAction();
                switch (action & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_DOWN: {
                        x_screen=event.getX();
                         if (BuildConfig.DEBUG)Log.e("TAG23", "x_screen    = "+ x_screen*razmeraX);

                        nacrtajWyciskLancanice(x_screen);

                        ignorisiListener = true;
                        return false;
                    }

                    case MotionEvent.ACTION_UP:{
                        ignorisiListener = false;
                        return false;
                    }
                    default:
                        return true;
                }
            }

        });

    }

    private void nacrtajWyciskLancanice(float x_screen){
        if (tackaJeUnutarListe(x_screen, listaWycisk_stacionaze)){
            // // FIXME: 3.11.2017 nece uvek da brise
            int index = indexNizaUnutarListe(x_screen, listaWycisk_stacionaze);
             if (BuildConfig.DEBUG)Log.e("TAG23", "index    = " + index);
            listaWycisk_stacionaze.remove(index);
            listaWycisk_kote.remove(index);
            listaRazlikaKotaWycisk.remove(index);
        }
        else {
            int indexRaspona = ProveraSigVisina_Fragment.indexSusednog_ManjegClanaNiza
                                (x_screen * razmeraX, nizStacionaza_STUB_X);
             if (BuildConfig.DEBUG)Log.e("TAG23", "indexRaspona    = " + indexRaspona);
            nizKota_lancanica_Y_5 = tackeLancanice(parametarLancanice_5);
            kreirajNizoveLancaniceWycisk(indexRaspona, nizStacionaza_lancanica_X, nizKota_lancanica_Y_5, nizStacionaza_STUB_X);
            dodajNizUlistuWycisk();
        }

         if (BuildConfig.DEBUG)Log.e("TAG23", "listaWycisk_stacionaze.size()= " + listaWycisk_stacionaze.size());

        nacrtajGrafik(crtajLancanice);

    }


    boolean prviPoziv = true;
    private void nacrtajGrafik(int i){

        mFL.removeView(mFunkcija);

        if (i==crtajLancanice)
            if (prviPoziv) {
                // sa animacijom (samo prvi put)
                mFunkcija = new CrtanjeFunkcija(getContext(),
                        nizStacionaza_STUB_X, nizKota_STUBiUZE_Y,
                        nizStacionaza_lancanica_X, nizKota_lancanica_Y,
                        listaWycisk_stacionaze, listaWycisk_kote, true , listaOznakaStubova);
                prviPoziv = false;
            }
            else
                // bez inicijalizacijaAnimacija
                mFunkcija = new CrtanjeFunkcija(getContext(),
                        nizStacionaza_STUB_X, nizKota_STUBiUZE_Y,
                        nizStacionaza_lancanica_X, nizKota_lancanica_Y,
                        listaWycisk_stacionaze, listaWycisk_kote, false, listaOznakaStubova);
        else if (i==crtajLancaniceAnimiraj)
            // uvek sa animacijom
            mFunkcija = new CrtanjeFunkcija(getContext(),
                    nizStacionaza_STUB_X, nizKota_STUBiUZE_Y,
                    nizStacionaza_lancanica_X, nizKota_lancanica_Y,
                    listaWycisk_stacionaze, listaWycisk_kote, true, listaOznakaStubova);
        else if (i==crtajVisine)
            mFunkcija = new CrtanjeFunkcija(getContext(),koordX);
        else if (i==crtajTrasu) {
            mFunkcija = new CrtanjeFunkcija(getContext(), nizStacionaza_tlo_X, nizKota_tlo_Y,
                    listaObjekti_X, listaObjekti_Y,
                    sirinaGrafika, visinaGrafika);

        }
        mFL.addView(mFunkcija);

    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // ne znam kako Float listu da ubacim u Bundle, pa listu pretvaram u niz
        if (!listaTacakaZaPrikazVisina.isEmpty()) {
            float[] nizTacaka = new float[listaTacakaZaPrikazVisina.size()];
            for(int i=0; i<nizTacaka.length; i++){
                nizTacaka[i] = listaTacakaZaPrikazVisina.get(i);
            }
            outState.putFloatArray("nizTacaka", nizTacaka);
        }

    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setHasOptionsMenu(true);
        // FIXME: 17.11.17 ovo bi onemogucilo animaciju crtanja - ostaje problem da pri povratku u app ne cuva wycisk lancanice
        //setRetainInstance(true);
         if (BuildConfig.DEBUG)Log.e("TAG33", "onCreate Called");
        // potpuno skriva actionbar
        //((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        nizStacionaza_tlo_X = getArguments().getFloatArray(ARGProfilTerena1);
        nizKota_tlo_Y = getArguments().getFloatArray(ARGProfilTerena2);
        naslovCrteza = getArguments().getString(ARGProfilTerena4);
        crtanje_ugiba = getArguments().getBoolean(ARGProfilTerena5);
        if (crtanje_ugiba){
            parametarLancanice =(float) getArguments().getDouble(ARGProfilTerena6);
            nizStacionaza_STUB_X = getArguments().getFloatArray(ARGProfilTerena7);
            nizKota_STUBiUZE_Y = getArguments().getFloatArray(ARGProfilTerena8);
            oznakaUzeta=getArguments().getString(ARGProfilTerena9);
            sigma0=getArguments().getDouble(ARGProfilTerena10);
            koefDodOpt=getArguments().getDouble(ARGProfilTerena11);
            temperatura=getArguments().getInt(ARGProfilTerena12);
            listaOznakaStubova=getArguments().getParcelableArrayList("listaOznakaStubova");
            listaVisina_stubova=getArguments().getParcelableArrayList("listaVisina_stubova");

            mTipUzeta = PODACI_ListaTipovaUzadi.getListaUzadi(getContext()).getTipUzetaIzListe(oznakaUzeta);

        }
        // TODO: 16.11.17 koment

        koordX=0; koordY=0; razmeraX =0;

//        if (listaWycisk_stacionaze==null){
//            listaWycisk_stacionaze = new ArrayList<>();
//            listaWycisk_kote = new ArrayList<>();
//            listaRazlikaKotaWycisk = new ArrayList<>();
//        }

        listaWycisk_stacionaze = new ArrayList<>();
        listaWycisk_kote = new ArrayList<>();
        listaRazlikaKotaWycisk = new ArrayList<>();



        if (b != null) {                    // ako postoji sacuvan bundle

            float[] nizTacaka = b.getFloatArray("nizTacaka");
            if (nizTacaka!=null){           // ako postoji niz
                listaTacakaZaPrikazVisina = new ArrayList<>(nizTacaka.length);
                for(float f : nizTacaka){
                    listaTacakaZaPrikazVisina.add(f);
                }
            } else {listaTacakaZaPrikazVisina = new ArrayList<>();}
        } else
            listaTacakaZaPrikazVisina = new ArrayList<>();
            //ucitanTXT_objekti_flag = false;

    }


    LinearLayout linerlayout_visine, linerlayout_dodstub;
    EditText visinaStuba;
    Button povecajStac, smanjiStac, dodajStub;
    TextView stacionaza;
    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
         if (BuildConfig.DEBUG)Log.e("TAG33", "onCreateView Called");
        View v = li.inflate(R.layout.fragment_jedna_funkcija, vg, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        ((ProveraSigVisina_Activity) getActivity()).setActionBarTitle("Uzdužni profil");

        if (crtanje_ugiba == false ) {
            // samo ako crtamo trasu jer tada odabiramo objekte
            // kada crtamo i lancanice onda su objekti vec ucitani (static)
            // tako da ne mora u bundle (a i neznam kako bi to uradio za listu
            /** @#$ 1*/
            /** @#$ 06.05.2018.
             * nije u onCreate jer se isti poziva i pri napustanju ovog fragmenta
             * i onda bi (posto je crtanje_ugiba=false) obrisao ucitane objekte*/
            listaObjekti_X =  new ArrayList<>();
            listaObjekti_Y =  new ArrayList<>();
        }

        linerlayout_dodstub = (LinearLayout) v.findViewById(R.id.f7_crtanje_layout_dodstuba);
        visinaStuba = (EditText) v.findViewById(R.id.f7_crtanje_dodstuba_visina);
        stacionaza = (TextView) v.findViewById(R.id.f7_crtanje_dodstuba_stacionaza);
        povecajStac = (Button) v.findViewById(R.id.f7_crtanje_dodstuba_povecaj);
        smanjiStac = (Button) v.findViewById(R.id.f7_crtanje_dodstuba_smanji);
        dodajStub = (Button) v.findViewById(R.id.f7_crtanje_dodstuba_dodaj);

        linerlayout_visine = (LinearLayout) v.findViewById(R.id.f7_crtanje_layout_visine);
        mUnosXkoord = (EditText) v.findViewById(R.id.f7_unos_x_koord);
        mRezultati = (TextView) v.findViewById(R.id.f7_rezultati);
        mRezultatiWycisk = (TextView) v.findViewById(R.id.f7_rezultati_wycisk);
        mDodajTacku = (ImageButton) v.findViewById(R.id.f7_rezultati_button);

        povecajStac.setOnClickListener(new View.OnClickListener() {
            float staraVred, novaVred;
            int indexNiza;
            @Override
            public void onClick(View view) {
                staraVred = koordX;
                indexNiza = indexSusednog_VecegClanaNiza(staraVred, nizStacionaza_tlo_X);
                novaVred = nizStacionaza_tlo_X[indexNiza];
                if (staraVred==novaVred && indexNiza <(nizStacionaza_tlo_X.length-1))
                    novaVred = nizStacionaza_tlo_X[++indexNiza];

                koordX = novaVred;
                stacionaza.setText(String.valueOf(novaVred));
                nacrtajGrafik(crtajVisine);
            }
        });
        smanjiStac.setOnClickListener(new View.OnClickListener() {
            float staraVred, novaVred;
            int indexNiza;
            @Override
            public void onClick(View view) {
                staraVred = koordX;
                indexNiza = indexSusednog_ManjegClanaNiza(staraVred, nizStacionaza_tlo_X);
                novaVred = nizStacionaza_tlo_X[indexNiza];
                if (staraVred==novaVred && indexNiza > 0)
                    novaVred = nizStacionaza_tlo_X[--indexNiza];

                koordX = novaVred;
                stacionaza.setText(String.valueOf(novaVred));
                nacrtajGrafik(crtajVisine);
            }
        });

        mFL = (FrameLayout) v.findViewById(R.id.f7_crtanje_mesto_za_funkciju);

        if (nizStacionaza_STUB_X!=null) {

            pripremiParametreZaLancanicu();
            nizKota_lancanica_Y = tackeLancanice(parametarLancanice);

            // ako bi stavili crtajLancaniceAnimiraj onda bi animiralo i pri povratku iz tabele ugiba
            nacrtajGrafik(crtajLancanice);

        }else {
            // ako bi ovde nacrtajGrafik(crtajTrasu); onda bi poslate dimenzije slike bile 0
            // jer se one odredjuju u onViewCreated
            crtajSamoTrasu = true;
        }

        linerlayout_visine.setVisibility(View.GONE);
        linerlayout_dodstub.setVisibility(View.GONE);

        return v;
    }

    private void kreirajNizStubnimMesta(float parametar, float parametar_5){
        int brojStubova = nizStacionaza_STUB_X.length;
        int brojRaspona = brojStubova - 1;

        double[] nizTemenaLancanice  = new double[brojRaspona];
        double[] nizTemenaLancanice_5  = new double[brojRaspona];
        for (int i = 0; i < brojRaspona; i++) {
            nizTemenaLancanice[i] =  nadjiTemeLancanice(i, parametar,
                    nizStacionaza_STUB_X, nizKota_STUBiUZE_Y );

            nizTemenaLancanice_5[i] =  nadjiTemeLancanice(i, parametar_5,
                    nizStacionaza_STUB_X, nizKota_STUBiUZE_Y );
        }

        StubnoMesto[] nizStubnihMesta = new StubnoMesto[brojStubova];

        //StubnoMesto.Tip_Stuba tip;
        double[] sredRasp =   StaticPodaci.izracunajSrednjeRaspone(nizRaspona);
        double[] gravRasp =   StaticPodaci.izracunajGravitacRaspone(nizRaspona, nizTemenaLancanice);
        double[] gravRasp_5 = StaticPodaci.izracunajGravitacRaspone(nizRaspona, nizTemenaLancanice_5);

        PODACI_TipStuba mStub;

        for (int i = 0; i < nizStubnihMesta.length; i++) {

            if (i == 0  ||  i == brojStubova-1) // prvi i poslednji --- dakle zatezni
                mStub = getSingltonStubova(getContext()).getTipStubaIzListe(stub_12_1000);
            else        //stub u sredini --- znaci noseci
                mStub = getSingltonStubova(getContext()).getTipStubaIzListe(stub_12_315);

             if (BuildConfig.DEBUG)Log.e("TAGklasaStubova", "listaOznakaStubova="+ listaOznakaStubova.get(i)
            + " tip="+ mStub.toString()
            + " \nsredRasp[i]="+ sredRasp[i]
            + " \ngravRasp   ="+ gravRasp[i]
            + " \ngravRasp-5 ="+ gravRasp_5[i]
            );

            nizStubnihMesta[i] = new StubnoMesto(
                    getContext(),
                    listaOznakaStubova.get(i).toString(),
                    mStub,
                    sredRasp[i],
                    gravRasp[i],
                    gravRasp_5[i]
            );
        }

        setNizStubnihMesta(nizStubnihMesta);

    }

    enum IzmenaLayouta {visine, stubovi, sakri}
    public void izmenaLayoutaNaVrhu(IzmenaLayouta flag){
        if (flag == visine){

            linerlayout_visine.setVisibility(View.VISIBLE);
            mUnosXkoord.setVisibility(View.VISIBLE);
            mRezultati.setVisibility(View.VISIBLE);
            mRezultatiWycisk.setVisibility(View.VISIBLE);
            mDodajTacku.setVisibility(View.VISIBLE);

            ((AppCompatActivity) getActivity()).getSupportActionBar().hide();


            linerlayout_dodstub.setVisibility(View.GONE);
        }
        else if (flag == stubovi){
            linerlayout_dodstub.setVisibility(View.VISIBLE);
            visinaStuba.setVisibility(View.VISIBLE);
            stacionaza.setVisibility(View.VISIBLE);
            povecajStac.setVisibility(View.VISIBLE);
            smanjiStac.setVisibility(View.VISIBLE);
            dodajStub.setVisibility(View.VISIBLE);

            ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

            linerlayout_visine.setVisibility(View.GONE);

        }
        else if (flag == sakri){
            // prekidam Listener-e
            mUnosXkoord.addTextChangedListener(new TextWatcher(){
                @Override
                public void onTextChanged(CharSequence cs, int s, int b, int c){ }
                @Override
                public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
                @Override
                public void afterTextChanged(Editable e){}

            });
            visinaStuba.addTextChangedListener(new TextWatcher(){
                @Override
                public void onTextChanged(CharSequence cs, int s, int b, int c){ }
                @Override
                public void beforeTextChanged(CharSequence cs, int s, int b, int c){}
                @Override
                public void afterTextChanged(Editable e){}

            });
            mFL.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });

            linerlayout_visine.setVisibility(View.GONE);
            linerlayout_dodstub.setVisibility(View.GONE);

            ((AppCompatActivity) getActivity()).getSupportActionBar().show();

            nacrtajGrafik(crtajLancanice);

        }

    }

    @Override
    public void onViewCreated(final View view, Bundle saved) {
        // za odredjivanje sirine slike --- jer mFL.getWidth() u OnCreate uvek daje 0
         if (BuildConfig.DEBUG)Log.e("TAGfragment", "onViewCreated Called");
        super.onViewCreated(view, saved);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                // zaustavljamo Listener jer nam nije vise potreban
                // dovoljno je da se samo jednom pokrene kako bi dobili dimenzije
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                else
                    view.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                visinaGrafika = mFL.getHeight();
                sirinaGrafika = mFL.getWidth();

                razmeraX = nizStacionaza_tlo_X[nizStacionaza_tlo_X.length-1] / sirinaGrafika;

                 if (BuildConfig.DEBUG)Log.e("TAG30", "razmeraX="+razmeraX+" sirinaGrafika="+sirinaGrafika+" visinaGrafika="+visinaGrafika);

                if (crtajSamoTrasu && visinaGrafika>0 && sirinaGrafika>0) {
                    nacrtajGrafik(crtajTrasu);
                    crtajSamoTrasu = false;
                }

            }
        });
    }

    private void ispisiRezultate(float x){
        float visinaObjekta=0;
        CharSequence text="";
        //text = mojHtmlString("<u>stacionaza: "+String.format("%.2f", x)+"   ");
        float visinaUzeta = visinaLancaniceUzadatojTacki(x ,parametarLancanice,
                nizStacionaza_tlo_X,nizKota_tlo_Y,nizStacionaza_STUB_X,nizKota_STUBiUZE_Y);

        if (tackaJeUnutarListe(x, listaObjekti_X) ) {
            // za slucaj da imamo objekat ispod
            int indexNiza = indexNizaUnutarListe(x, listaObjekti_X);
            float kotaTla_y = linearnaInterpolacija(x, nizStacionaza_tlo_X, nizKota_tlo_Y);
            float kotaObjekta_y = linearnaInterpolacija(x, listaObjekti_X.get(indexNiza), listaObjekti_Y.get(indexNiza));
            visinaObjekta = kotaObjekta_y - kotaTla_y;
            visinaUzeta = visinaUzeta - visinaObjekta;
            if (visinaUzeta > sig_visinaZgrade)
                text = mojHtmlString(text + "<u>visina uzeta: " + String.format("%.2f", visinaUzeta));
            else
                text = mojHtmlString(text + "<u> <font color=\"red\">visina uzeta: " + String.format("%.2f", visinaUzeta));

        }else {
            if (visinaUzeta > sig_visina)
                text = mojHtmlString(text + "<u>visina uzeta: " + String.format("%.2f", visinaUzeta));
            else
                text = mojHtmlString(text + "<u> <font color=\"red\">visina uzeta: " + String.format("%.2f", visinaUzeta));
        }

        mRezultati.setText( text );


        if (tackaJeUnutarListe(x, listaWycisk_stacionaze)) {
            int indexNiza = indexNizaUnutarListe(x, listaWycisk_stacionaze);

            float visinaUzeta_5 = visinaLancaniceUzadatojTacki
                    (x ,parametarLancanice_5,nizStacionaza_tlo_X,nizKota_tlo_Y,
                                             nizStacionaza_STUB_X,nizKota_STUBiUZE_Y);
            float razlikaVisinaWycisk = linearnaInterpolacija
                    (x, listaWycisk_stacionaze.get(indexNiza), listaRazlikaKotaWycisk.get(indexNiza));

            float visinaUzeta_5_wycisk = visinaUzeta_5 - razlikaVisinaWycisk;

            if (tackaJeUnutarListe(x, listaObjekti_X) )
                visinaUzeta_5_wycisk = visinaUzeta_5_wycisk - visinaObjekta;


            if (visinaUzeta_5_wycisk > sig_visinaZgrade)
                text = mojHtmlString("<u>visina uzeta Wycisk: "+String.format("%.2f", visinaUzeta_5_wycisk) );
            else
                text = mojHtmlString("<u><font color=\"red\">visina uzeta Wycisk: "+String.format("%.2f", visinaUzeta_5_wycisk) );

            mRezultatiWycisk.setText(text);

        } else mRezultatiWycisk.setText("");

    }

    private float[] tackeLancanice(float parametarLancanice){
        // racuna sve Y tacke lancanice (jedan niz za sve raspone)

        // ovo ispod je zbog kvaliteta crteza - povecavamo broj tacaka za koje racunamo Y vrednosti
        final int brojMedjuTacaka = 10; // ne menjaj!!!
        int brojTacaka = (Math.round( nizStacionaza_STUB_X[nizStacionaza_STUB_X.length-1] -
                 nizStacionaza_STUB_X[0])  ); // preciznije sa Math.round

        nizStacionaza_lancanica_X = new float[brojTacaka*brojMedjuTacaka];
        float [] nizKota_lancanica_Y = new float[brojTacaka*brojMedjuTacaka];
        // if (BuildConfig.DEBUG)Log.e("TAG17","brojTacaka= "+ brojTacaka);

        int index_x_koor;
        int indexStuba;
        int indexStuba_prethodnaIteracija=-1; // da bi se indeksi razlikovali u prvoj iteraciji
        float teme_lanc_X=0, kotaVesanjaUze_Y, y_dodatak=0;
        //double[] nizTemenaLancanica = nizTemenaLancanica(parametarLancanice,nizStacionaza_STUB_X,nizKota_STUBiUZE_Y);
        for (int i = 0; i <  brojTacaka; i++) {
            for (int j = 0; j < brojMedjuTacaka; j++) {
                // if (BuildConfig.DEBUG)Log.e("TAG17","i= "+ i +" j= "+ j  );

                index_x_koor = i*brojMedjuTacaka + j; //  povecavano broj stacionaza lancanice na ukupno brojTacaka*brojMedjuTacaka
                float dodatak = (i+j/(float)brojMedjuTacaka); //    j/10 vraca int -> ne bi imao decimalnu vrednost

                // kako ide petlja tako se krecemo od pocetnog stuba, tacku po tacku
                nizStacionaza_lancanica_X[index_x_koor] = nizStacionaza_STUB_X[0] + dodatak;

                // ovo je moćno
                indexStuba = ProveraSigVisina_Fragment.indexSusednog_ManjegClanaNiza(nizStacionaza_lancanica_X[index_x_koor], nizStacionaza_STUB_X);

                // neke velicine se ne menjaju za tacke koje su u istom rasponu
                if (indexStuba_prethodnaIteracija != indexStuba) {

                    teme_lanc_X = (float) nadjiTemeLancanice(indexStuba, parametarLancanice,
                                            nizStacionaza_STUB_X,nizKota_STUBiUZE_Y );

                    kotaVesanjaUze_Y = nizKota_STUBiUZE_Y[indexStuba];

                    y_dodatak = kotaVesanjaUze_Y -
                            lancanica_vredY_u_tackiX(nizStacionaza_STUB_X[indexStuba] - teme_lanc_X, parametarLancanice);
                }

                nizKota_lancanica_Y[index_x_koor] = y_dodatak +
                        lancanica_vredY_u_tackiX( nizStacionaza_lancanica_X[index_x_koor] - teme_lanc_X, parametarLancanice );

                indexStuba_prethodnaIteracija = indexStuba;
              }

        }
        // if (BuildConfig.DEBUG)Log.e("TAG17","nizStacionaza_lancanica_X.length= "+ nizStacionaza_lancanica_X.length);
        // if (BuildConfig.DEBUG)Log.e("TAG17","nizStacionaza_lancanica_X "+ Arrays.toString(nizStacionaza_lancanica_X));

        return nizKota_lancanica_Y;
    }

    private static double nadjiTemeLancanice(int i, float parametarLancanice,
                    float[] nizStacionaza_STUB_X, float[] nizKota_STUBiUZE_Y){
        /** !@# 01.03.2018-3 */
        // i - index temena u zateznom polju koji nam treba
        double[] nizRaspona = konvertovanje_stacionaza_u_raspone(nizStacionaza_STUB_X);

        /******
         ovo je nedovoljno precizno - ne poklapa se kraj lancanice sa visinom drugog stuba:
         nizTemenaLanc_rel[i] = nizStubova_X[i] + nizRaspona[i]/2 - parametarLancanice*h[i+1]/nizRaspona[i];


         ovo je ok - u skladu sa Alen Hatibović !@# vidi pdf u assets_no_build:
         "ODREĐIVANJE JEDNAČINA VODA I UGIBA NA OSNOVU ZADANOG PARAMETRA LANČANICE"
         racuna odstojanje temena lancanice od pocetno stuba
         */
            return nizStacionaza_STUB_X[i] + nizRaspona[i] / 2
                    - parametarLancanice * arcSinH((nizKota_STUBiUZE_Y[i + 1] - nizKota_STUBiUZE_Y[i])
                            / (2 * parametarLancanice * Math.sinh(nizRaspona[i] / (2 * parametarLancanice))
                    )
            );

    }

    public static float visinaLancaniceUzadatojTacki(float x_koor_lancanice,
                                                     float parametarLancanice,
                                                     float []nizStacionaza_tlo_X,
                                                     float []nizKota_tlo_Y,
                                                     float []nizStacionaza_STUB_X,
                                                     float []nizKota_STUBiUZE_Y){

        float teren_y = linearnaInterpolacija (
                x_koor_lancanice ,nizStacionaza_tlo_X, nizKota_tlo_Y );

        int indexNiza = ProveraSigVisina_Fragment.indexSusednog_ManjegClanaNiza(x_koor_lancanice, nizStacionaza_STUB_X);

        float teme_lanc_X = (float) nadjiTemeLancanice(indexNiza, parametarLancanice,
                nizStacionaza_STUB_X,nizKota_STUBiUZE_Y );

        // if (BuildConfig.DEBUG)Log.e("TAG12", "teme_lanc_X    = "+ teme_lanc_X +"   indexNiza    = "+indexNiza);
        float kotaVesanjaUze_Y = nizKota_STUBiUZE_Y[indexNiza];

        float y_dodatak = kotaVesanjaUze_Y -
                lancanica_vredY_u_tackiX(nizStacionaza_STUB_X[indexNiza] - teme_lanc_X, parametarLancanice);


        float lanc_y = y_dodatak + lancanica_vredY_u_tackiX(x_koor_lancanice - teme_lanc_X, parametarLancanice);

        return lanc_y - teren_y;
    }

    public static float lancanica_vredY_u_tackiX(float x, float p) {
        // x je rastojanje od temena lancanice
        // p = parametar;
        //teme lancanice je u [0,p]             --- p * Math.cosh(x/p)
        // teme lancanice je u koord. pocetku   --- p * Math.cosh(x/p) - p
        return p * (float) Math.cosh(x / p) - p;
    }

    public static float arcSinH(double x) {
        return (float) Math.log(x + Math.sqrt(x * x + 1.0));
    }

    private void pripremiParametreZaLancanicu(){

        nizRaspona = konvertovanje_stacionaza_u_raspone(nizStacionaza_STUB_X);
        nizCosFi = ProveraSigVisina_Fragment.izracunajCosFi(nizRaspona, nizKota_STUBiUZE_Y);

        double idealniRaspon = IdealniRaspon(nizRaspona,nizCosFi);
        double idealniCosFi = IdealniCosFi(nizRaspona,nizCosFi);

        // za za Wycisk
        JednacinaStanja js_5 = new JednacinaStanja(mTipUzeta, sigma0, idealniRaspon,
                idealniCosFi, koefDodOpt, -5);

        // za prikaz koef i naprezanja, i za pozivanje SacuvajGrafik
        js = new JednacinaStanja(mTipUzeta, sigma0, idealniRaspon,
                idealniCosFi, koefDodOpt, temperatura);

        double specTezina, dodTezina, rezTezina;
        specTezina = mTipUzeta.getSpecTezina();
        dodTezina = DodatnoOptSnegLed(koefDodOpt, mTipUzeta.getPrecnik(), mTipUzeta.getPresek());
        rezTezina =specTezina+dodTezina;

        // za Wycisk
        parametarLancanice_5 = (float) (js_5.getSigmaNOVO() / rezTezina);

        if(temperatura==-5)
            parametarLancanice = parametarLancanice_5;
        else
            parametarLancanice = (float) (js.getSigmaNOVO() / specTezina);

         if (BuildConfig.DEBUG)Log.e("TAG15", "naprezanje= "+js.getSigmaNOVO());
         if (BuildConfig.DEBUG)Log.e("TAG15", "rezTezina= "+rezTezina);

        float[] niz = ProveraSigVisina_Fragment. izracunajVisinskeRazlike(nizKota_STUBiUZE_Y);
         if (BuildConfig.DEBUG)Log.e("TAG20", "visinske razlike = "+Arrays.toString(niz));

        if (StubnoMesto.getNizStubnimMesta() == null ||
                getBool_potrebanUpdate())
            kreirajNizStubnimMesta(parametarLancanice, parametarLancanice_5);
            setBool_potrebanUpdate(false);
    }


    public static double[] konvertovanje_stacionaza_u_raspone(float[] niz_kote){
        double[] niz_raspona = new double[niz_kote.length-1]; // ima jedan raspon manje od br.stubova

        for (int i = 0; i < niz_raspona.length; i++) {
            niz_raspona[i] = niz_kote[i+1] - niz_kote[i];
        }

        return niz_raspona;
    }

    private double ugibWycisk (int indexRaspona, double ugib, double[] nizRaspona,PODACI_TipUzeta tipUzeta){

        if (nizRaspona.length==1){
            // samo jedan raspon u polju (nema neravnomerne raspodele leda u polju)
            return ugib;
        }

        double prelazniRaspon = nizRaspona[indexRaspona];
        int nL = indexRaspona; // broj neopterecenih raspona sa leve strane
        int nD = nizRaspona.length - 1 - indexRaspona; // broj neopterecenih raspona sa leve strane

        if (nL>6)
            nL=6;
        if (nD>6)
            nD=6;

        double raspon_sr_L=0;
        double raspon_sr_D=0;
        for (int i = 0; i < nizRaspona.length ; i++) {
            if (i<indexRaspona)
                raspon_sr_L += nizRaspona[i];
            else if (i>indexRaspona)
                raspon_sr_D += nizRaspona[i];
        }
        if (raspon_sr_L != 0)
            raspon_sr_L /= nL;
        if (raspon_sr_D != 0)
            raspon_sr_D /= nD;

        double optUzeta = tipUzeta.getSpecTezina();
        double optUzetaSaLedom = optUzeta + JednacinaStanja.DodatnoOptSnegLed(koefDodOpt,
                tipUzeta.getPrecnik(), tipUzeta.getPresek());

///////////////////////// keoficijenti Kl i Kd///////////////////////////////
        double koef_kL = -1, koef_kD = -1;
        if (nL == 1 && nD==1){
            // ima tri raspona a prelazni je u sredini
            koef_kL=1.1;
            koef_kD=1.1;
        }
        else if (nL == 0){
            // prelazni raspon je na pocetku
            koef_kL=0;
            if (nD == 1)
                        koef_kD = 2;
            else if (nD==2)
                        koef_kD = 1.94;
            else
                        koef_kD = 1.8; // za cist aluminijum je 1.86 ali to nije potrebno
        }

        else if (nD == 0){
            // prelazni raspon je na kraju
            koef_kD=0;
            if (nL == 1)
                        koef_kL = 2;
            else if (nL==2)
                        koef_kL = 1.94;
            else
                        koef_kL = 1.8; // za cist aluminijum je 1.86 ali to nije potrebno
        }

        else if (nL == 1){
            koef_kL=0.97;
            koef_kD=0.95;
        }

        else if (nD == 1){
            koef_kL=0.95;
            koef_kD=0.97;
        }
        else{
            koef_kL=0.95;
            koef_kD=0.95;
        }
///////////////////////////////////////////////////////

        double ugibW =
                0.5 * ugib * koef_kL * razlomakWycisk(nL,prelazniRaspon,raspon_sr_L,optUzeta,optUzetaSaLedom)
               +0.5 * ugib * koef_kD * razlomakWycisk(nD,prelazniRaspon,raspon_sr_D,optUzeta,optUzetaSaLedom) ;


         if (BuildConfig.DEBUG)Log.e("TAG20", "ugib = "+ugib + " ugibW = "+ugibW);
        return ugibW;
    }

    private static double razlomakWycisk(int n, double prelazniRaspon, double raspon_sr,
                                         double specTezina, double specTezinaSaLedom){
        double gore = 1 + n*Math.pow(raspon_sr/prelazniRaspon,3);
        double dole = 1 + n*Math.pow(raspon_sr/prelazniRaspon,3)
                                *Math.pow(specTezina/specTezinaSaLedom,2);
        return Math.sqrt(gore/dole);
    }

    private  void kreirajNizoveLancaniceWycisk(int indexRaspona,
                                               float[] nizStacionaza_lancanica_X,
                                               float[] nizKota_lancanica_Y_5,
                                               float[] nizStacionaza_STUB_X) {
        // proracun lancanice kada u posmatranom rasponu ima leda a u ostalim ne
        // za ukrastanje sa NN i SN mrezom, objektima

        float pocetak_x = nizStacionaza_STUB_X[indexRaspona];
        float kraj_x = nizStacionaza_STUB_X[indexRaspona+1];

        int index_pocetak_x=ProveraSigVisina_Fragment.indexSusednog_VecegClanaNiza(pocetak_x, nizStacionaza_lancanica_X);
        int index_kraj_x=ProveraSigVisina_Fragment.indexSusednog_ManjegClanaNiza(kraj_x, nizStacionaza_lancanica_X);

         if (BuildConfig.DEBUG)Log.e("TAG21", "pocetak_x = "+pocetak_x+ "  kraj_x = "+kraj_x);
         if (BuildConfig.DEBUG)Log.e("TAG21", "index_pocetak_x = "+index_pocetak_x+ "  index_kraj_x = "+index_kraj_x);


        nizStacionaza_lancanica_X_Wycisk = Arrays.copyOfRange(
                nizStacionaza_lancanica_X, index_pocetak_x, index_kraj_x);

        nizKota_lancanica_Y_Wycisk = nizStacionaza_lancanica_X_Wycisk.clone();

        nizRazlikaKota_wycisk = nizKota_lancanica_Y_Wycisk.clone();
        double ugib, ugibWycisk; // ugib sa ledom kada na susednim rasponima nema leda
        float koteLinijeIzmedjuTacakaVesanja_Y; // ugib sa ledom kada na susednim rasponima nema leda
        for (int i = 0; i <nizKota_lancanica_Y_Wycisk.length; i++) {
            koteLinijeIzmedjuTacakaVesanja_Y = linearnaInterpolacija(
                    nizStacionaza_lancanica_X[index_pocetak_x+i],
                    nizStacionaza_STUB_X,
                    nizKota_STUBiUZE_Y);

            ugib = koteLinijeIzmedjuTacakaVesanja_Y - nizKota_lancanica_Y_5[index_pocetak_x + i];

            ugibWycisk = ugibWycisk(indexRaspona, ugib, nizRaspona, mTipUzeta);

            nizRazlikaKota_wycisk[i]= (float)(ugibWycisk-ugib);

            nizKota_lancanica_Y_Wycisk[i] = nizKota_lancanica_Y_5[index_pocetak_x + i] -
                    nizRazlikaKota_wycisk[i];

        }

    }

    public static boolean tackaJeUnutarListe(float x, List<float[]> lista){
        return indexNizaUnutarListe(x, lista) != nepostoji;
    }

    public static int indexNizaUnutarListe(float x, List<float[]> lista){
        if (lista==null)
            return nepostoji;
        for (int indexNiza = 0; indexNiza < lista.size(); indexNiza++) {
            if (x >= (lista.get(indexNiza))[0] &&
                    x <= (lista.get(indexNiza))[lista.get(indexNiza).length-1]) {
                 if (BuildConfig.DEBUG)Log.e("TAG23", "x= " + x);
                 if (BuildConfig.DEBUG)Log.e("TAG23", "indexNiza= " + indexNiza);
                 if (BuildConfig.DEBUG)Log.e("TAG23", "xMIN= " + lista.get(indexNiza)[0]);
                 if (BuildConfig.DEBUG)Log.e("TAG23", "xMAX= " + lista.get(indexNiza)[lista.get(indexNiza).length-1]);
                return indexNiza;
            }
        }
        return nepostoji;
    }

    private  void dodajNizUlistuWycisk() {
        listaWycisk_stacionaze.add(nizStacionaza_lancanica_X_Wycisk);
        listaWycisk_kote.add(nizKota_lancanica_Y_Wycisk);
        listaRazlikaKotaWycisk.add(nizRazlikaKota_wycisk);

        for (int i = 0; i < listaWycisk_stacionaze.size(); i++) {
             if (BuildConfig.DEBUG)Log.e("TAG25", "listaWycisk_stacionaze    = "+ Arrays.toString(listaWycisk_stacionaze.get(i)));
             if (BuildConfig.DEBUG)Log.e("TAG25", "listaWycisk_kote    = "+ Arrays.toString(listaWycisk_kote.get(i)));

        }
    }

    private static float[] konvertovanje_duplihListi_u_dupliNiz(List<List<Double>> lista){
        int brojTacaka=lista.size();
        for (int i = 0; i < lista.size() ; i++) {
            brojTacaka+=lista.get(i).size();
        }

        float[] nizObjekata = new float[brojTacaka];
        for (int i = 0; i < lista.size() ; i++) {
            for (int j = 0; j < (lista.get(i).size()+1); j++) {
                if (j==lista.get(i).size()) // izasli smo iz domena
                    nizObjekata[i+j] = Float.MAX_VALUE; //razgranicenje objekata
                else
                    nizObjekata[i+j] = Float.parseFloat ( (lista.get(i)).get(j).toString() );
            }
        }
         if (BuildConfig.DEBUG)Log.e("TAG33", "konvertovanje_duplihListi_u_dupliNiz = " + Arrays.toString(nizObjekata));
        return nizObjekata;
    }


    public static List kopiranjeListe(List<Float> lista){
        List<Float> novaLista=new ArrayList();
        for (int i = 0; i < lista.size() ; i++) {
            novaLista.add(lista.get(i));
        }
        return novaLista;
    }


    @Override
    public void onAttach(Context contex) {
        super.onAttach(contex);
        mCallBack = (DodajStub)contex;
    }
}
 /*

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onAttach(Context c) {
        super.onAttach(c);
         if (BuildConfig.DEBUG)Log.e("TAG33", "onAttach Called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
         if (BuildConfig.DEBUG)Log.e("TAG33", "onDestroy Called");
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
         if (BuildConfig.DEBUG)Log.e("TAG33", "onDestroyView Called");
    }

    @Override
    public void onDetach() {
        super.onDetach();
         if (BuildConfig.DEBUG)Log.e("TAG33", "onDetach Called");
    }

    @Override
    public void onPause() {
        super.onPause();
         if (BuildConfig.DEBUG)Log.e("TAG33", "onPause Called");
    }

    @Override
    public void onResume() {
        super.onResume();
         if (BuildConfig.DEBUG)Log.e("TAG33", "onResume Called");
    }

    @Override
    public void onStart() {
        super.onStart();
         if (BuildConfig.DEBUG)Log.e("TAG33", "onStart Called");
    }

    @Override
    public void onStop() {
        super.onStop();
         if (BuildConfig.DEBUG)Log.e("TAG33", "onStop Called");
    }
*/