package nsp.mpu._5_sigurnosne_visine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Spanned;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.JednacinaStanja;

import static android.graphics.Paint.Align.CENTER;
import static android.graphics.Paint.Align.LEFT;
import static android.graphics.Paint.Align.RIGHT;
import static nsp.mpu._5_sigurnosne_visine.CrtanjeFunkcija.kopiranjeArrayList;
import static nsp.mpu._5_sigurnosne_visine.CrtanjeFunkcija.prilagodiRazmeri_X;
import static nsp.mpu._5_sigurnosne_visine.CrtanjeFunkcija.prilagodiRazmeri_Y;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Activity.sig_visina;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Fragment.indexSusednog_ManjegClanaNiza;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Fragment.linearnaInterpolacija;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_fragment_proracuni.indexNizaUnutarListe;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_fragment_proracuni.tackaJeUnutarListe;
import static nsp.mpu.pomocne_klase.JednacinaStanja.DodatnoOptSnegLed;
import static nsp.mpu.pomocne_klase.StaticPodaci.mojHtmlString;

@Obfuscate
public class SacuvajGrafik {

    private Paint paint_funkcija = new Paint();
    private Paint paint_stub = new Paint();
    private Paint paint_tlo = new Paint();
    private Paint paint_funkcija_Wycisk = new Paint();
    private Paint paint_text = new Paint();
    private Paint paint_kotiranje = new Paint();
    Canvas mCanvas;
    private float razmeraX, razmeraY;

    private float margina_5mm;
    private float margina_okvira;
    private float margina_okvira_secenje;

    private float referencaX_pocetak_okvir, referencaY_pocetak_okvir_isprek,
                  referencaX_kraj_okvir, referencaY_kraj_okvir_isprek;

    private float referencaX_pocetak_tabeleStac, referencaY_pocetak_tabeleStac,
                  referencaX_kraj_tabeleStac, referencaY_kraj_tabeleStac;

    private float referencaX_pocetakGrafika, referencaY_pocetakGrafika,
            referencaX_krajGrafika, referencaY_krajGrafika;

    private float referencaX_pocetak_prednjeSTR, referencaY_pocetak_prednjeSTR,
                  referencaX_kraj_prednjeSTR, referencaY_kraj_prednjeSTR;

    private float referencaX_pocetak,  referencaX_kraj;
    private float referencaY_pocetak_okvir_PUNA,  referencaY_kraj_okvir_PUNA;



    Context mContext;
    private  Bitmap mBitmap;
    private float[] nizStacionaza_tlo_X, nizKota_tlo_Y, nizStacionaza_STUB_X, nizKota_STUBiUZE_Y,
            nizStacionaza_lancanica_X, nizKota_lancanica_Y;
    private float[] nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY,
            nizStacionaza_STUB_relativnoX, nizKota_STUBiUZE_Y_relativno,
            nizStacionaza_lancanica_X_rel, nizKota_lancanica_Y_rel;

    private List<float[]> listaWycisk_stacionaze,listaWycisk_kote;


    private float pixelaUcm_x, pixelaUcm_Y;
    private int velicinaUkupno_x_pix, velicinaUkupno_y_pix;
    //private int velicinaGrafika_x_pix, velicinaGrafika_y_pix;
    private final float cm_u_inchu = 2.56f;
    private float Y_osa_max,Y_osa_min;

    private File fajlZaGrafik;
    //private int rezolucija_slike;

    private double koefDodOpt, kriticniRaspon, idealniRaspon;
    double specTezina, dodTezina, rezTezina;
    private int temperatura;
    PODACI_TipUzeta mTipUzeta;
    //int sirinaPrveStrane;
    int uvecanje;
    float marginaText;

    JednacinaStanja js;
    private List<Float> listaTacakaZaPrikazVisina, listaTacakaZaPrikaz_minVisina;
    float odstojanjeOznakeOdStuba, odstojanjeOznakeOdStuba_rel;

    private List<float[]> listaObjekti_X, listaObjekti_Y;
    private List<float[]> listaObjekti_relX, listaObjekti_relY;
    private List listaOznakaStubova;
    private List<Float> listaVisina_stubova;

    private float velicinaTeksta_malo = 17;
    private float velicinaTeksta_srednje = 20;
    private float velicinaTeksta_VELIKO = 30;
    private float visinaProfila;

    public SacuvajGrafik(Context context,
                         float[] nizStacionazaTlo, float[] nizKotaTlo,
                         List listaObjekti_X, List listaObjekti_Y,
                         float[] nizStacionazaStub, float[] nizKotaStub,
                         float[] nizStacionaza_lancanica_X, float[] nizKota_lancanica_Y,
                         List listaWycisk_stacionaze, List listaWycisk_kote,
                         File fajl, JednacinaStanja js,
                         List listaTacakaZaPrikazVisina,
                         List listaOznakaStubova, List listaVisina_stubova) {
        mContext = context;
        this.nizKota_STUBiUZE_Y = nizKotaStub;
        this.nizKota_tlo_Y = nizKotaTlo;
        this.nizKota_lancanica_Y = nizKota_lancanica_Y;
        this.nizStacionaza_STUB_X = nizStacionazaStub;
        this.nizStacionaza_tlo_X = nizStacionazaTlo;
        this.nizStacionaza_lancanica_X = nizStacionaza_lancanica_X;
        this.fajlZaGrafik =fajl;

        /** @#$ 3
         * kopiramo da ne bi menjali primarnu/poslatu listu
         *  posto nikad ne vraca null onda slobodno koristimo .size()>0 uslov */
        this.listaWycisk_stacionaze= kopiranjeArrayList( listaWycisk_stacionaze );
        this.listaWycisk_kote=kopiranjeArrayList( listaWycisk_kote);
        this.js=js;
        this.listaTacakaZaPrikazVisina=listaTacakaZaPrikazVisina;
        this.listaObjekti_X = listaObjekti_X;
        this.listaObjekti_Y = listaObjekti_Y;
        this.listaOznakaStubova = listaOznakaStubova;
        this.listaVisina_stubova = listaVisina_stubova;

        odstojanjeOznakeOdStuba = 5; //stelovano, više od visine stuba

         if (BuildConfig.DEBUG)Log.e("TAG34","SG listaObjekti_X = "+listaObjekti_X);
         if (BuildConfig.DEBUG)Log.e("TAG34","SG listaObjekti_Y = " +listaObjekti_Y);



        /*prikaziToast(mContext,"maxMemory " + Runtime.getRuntime().maxMemory()/1_000_000 +
                                "   getMemoryClass " + ((ActivityManager)mContext
                                .getSystemService(Context.ACTIVITY_SERVICE))
                                .getMemoryClass());*/

        izracunajRazmere();
        prilagodiRazmeri();
        odrediMargine(0);


        paint_text.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD_ITALIC));
        paint_text.setTextSize(velicinaTeksta_srednje);


        // !@# 5.2.18-3 297mm je izmedju isprek linija, a nije uk.visina crteza
        float ukupnaVisinaCrteza = referencaY_pocetak_okvir_isprek +margina_5mm;
        float ukupnaVisina_izmedju_isprek= ukupnaVisinaCrteza - 2 *margina_5mm; //po 5mm gore i dole za odsecanje
        if (ukupnaVisina_izmedju_isprek < 29.7f*pixelaUcm_Y) {
            odrediMargine(29.7f * pixelaUcm_Y - ukupnaVisina_izmedju_isprek); // min visina je A4 format
            ukupnaVisinaCrteza = referencaY_pocetak_okvir_isprek +margina_5mm;
        }
        try {

            mBitmap = Bitmap.createBitmap(  (int)(referencaX_kraj_okvir+margina_5mm),
                                            (int)   ukupnaVisinaCrteza,
                                            Bitmap.Config.ARGB_8888);

            mBitmap.setDensity(bitmap_tablica.getDensity());
        } catch (OutOfMemoryError e) {
             if (BuildConfig.DEBUG)Log.e("TAG15", "-------------OutOfMemoryError------------");
            return ; // greska u kreiranju constuktora
        }
        mCanvas = new Canvas(mBitmap);

        //mCanvas.drawColor(Color.LTGRAY);
        mCanvas.drawColor(Color.TRANSPARENT);

        crtanje();
        sacuvajSliku();
    }

    //private BitmapDrawable drawable;
    private Bitmap bitmap_tablica;
    private void izracunajRazmere(){
        final float razmera_za_duzine_vodova_X = 2000f; // 1:2000
        final float razmera_za_visine_vodova_Y = 500f; // 1:500

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled=false;
        bitmap_tablica = BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.tablica_18cm, options );



        //sirinaPrveStrane = bitmap_tablica.getWidth();

        uvecanje = 1; //// // FIXME: 12.11.2017 obrisi


        pixelaUcm_x = bitmap_tablica.getWidth()/18.0f;  /** !@# 5.2.18-2 mora 18.0f inace bi imali int deljenje [pix/cm] */

        razmeraX = 100 * pixelaUcm_x /razmera_za_duzine_vodova_X; //[pix/m]

        /* piksel = pix/cm * 100cm * m/2000
           mnozi sa100 jer pretvaramo cm u m (2000 je bez dimenzija)*/
        //velicinaGrafika_x_pix = uvecanje*(int) (razmeraX*nizStacionaza_tlo_X[nizStacionaza_tlo_X.length-1]);


        Y_osa_max = nizKota_tlo_Y[0];
        Y_osa_min = nizKota_tlo_Y[0];

        for (int i = 1; i < nizKota_tlo_Y.length; i++)
            if      (nizKota_tlo_Y[i] > Y_osa_max)   Y_osa_max = nizKota_tlo_Y[i];
            else if (nizKota_tlo_Y[i] < Y_osa_min)   Y_osa_min = nizKota_tlo_Y[i];


        //// trazimo max visinu objekta/////////
        if (listaObjekti_Y!=null)
            for (int i = 0; i < listaObjekti_Y.size(); i++)
                for (int j = 0; j < listaObjekti_Y.get(i).length; j++)
                    if ((listaObjekti_Y.get(i))[j] > Y_osa_max)
                        Y_osa_max = (listaObjekti_Y.get(i))[j];


        if (nizKota_STUBiUZE_Y != null) { // brze je sa stubovima a lancanica je uvek niza
            for (int i = 0; i < nizKota_STUBiUZE_Y.length; i++)
                if (nizKota_STUBiUZE_Y[i] > Y_osa_max)
                    Y_osa_max = nizKota_STUBiUZE_Y[i];
            Y_osa_max += 2*odstojanjeOznakeOdStuba; // nije sigurno vece od ovog
        }

        visinaProfila = Y_osa_max  - Y_osa_min;
        if (visinaProfila == 0) // ako je trasa ravna linija
            visinaProfila = 10;


        pixelaUcm_Y = bitmap_tablica.getHeight()/5.0f; /** !@# 5.2.18-2 mora 5.0f inace bi imali int deljenje [pix/cm] */

        razmeraY = 100 * pixelaUcm_Y /razmera_za_visine_vodova_Y;

        //velicinaGrafika_y_pix = uvecanje*(int) (razmeraY * ukupnaVisina); // dodaje se margina_razmere da bi lepo cuvao sliku trase voda
                                  // jer ako je trasa male visine_rel onda je moguce da je npr
                                    // visina slike 10px a kota terena 10+px


         if (BuildConfig.DEBUG)Log.e("TAGconstruktorSlike", "----------------------------------------");
//         if (BuildConfig.DEBUG)Log.e("TAGconstruktorSlike", "drawable.getIntrinsicWidth() = " + drawable.getIntrinsicWidth() +
//               " drawable.getIntrinsicHeight() = " + drawable.getIntrinsicHeight());
//         if (BuildConfig.DEBUG)Log.e("TAGconstruktorSlike", "bitmap_tablica_pom.getWidth() = " + bitmap_tablica_pom.getWidth() +
//                " bitmap_tablica_pom.getHeight() = " + bitmap_tablica_pom.getHeight());
//         if (BuildConfig.DEBUG)Log.e("TAGconstruktorSlike", ".getScaledWidth(mCanvas) = " + bitmap_tablica.getScaledWidth(mCanvas)
//                + "  .getScaledHeight(mCanvas) = " + bitmap_tablica.getScaledHeight(mCanvas));
         if (BuildConfig.DEBUG)Log.e("TAGconstruktorSlike", "bitmap_tablica.getWidth() = " + bitmap_tablica.getWidth() +
                " bitmap_tablica.getHeight() = " + bitmap_tablica.getHeight());
         if (BuildConfig.DEBUG)Log.e("TAGconstruktorSlike", "pixelaUcm_x = " + pixelaUcm_x + "  pixelaUcm_Y = " + pixelaUcm_Y);
         if (BuildConfig.DEBUG)Log.e("TAGconstruktorSlike", "razmeraX = " + razmeraX + "  razmeraY = " + razmeraY);
    }

    private float  sirinaDodatkaTabele;
    private void odrediMargine(float dodatak){
        float visinaTabele, sirinaGrafika, sirinaTabele=0;


        margina_5mm = 0.5f * pixelaUcm_x;
        margina_okvira = margina_5mm; // margina okvira je 5mm

        referencaX_pocetak_okvir = margina_okvira;
        referencaX_pocetak = referencaX_pocetak_okvir + 2*pixelaUcm_x; // margina sa leve strane je 2cm

        referencaX_pocetak_tabeleStac = referencaX_pocetak + margina_5mm;
        if (nizStacionaza_STUB_relativnoX!=null) {
            sirinaTabele = nizStacionaza_STUB_relativnoX[nizStacionaza_STUB_relativnoX.length - 1]
                            -nizStacionaza_STUB_relativnoX[0] ;
            sirinaDodatkaTabele = 9*pixelaUcm_x;
        }
        referencaX_kraj_tabeleStac = referencaX_pocetak_tabeleStac + sirinaDodatkaTabele+sirinaTabele;


        referencaX_pocetakGrafika = referencaX_pocetak_tabeleStac+ sirinaDodatkaTabele;
        sirinaGrafika = nizStacionaza_tlo_relativnoX[nizStacionaza_tlo_relativnoX.length-1];
        referencaX_krajGrafika = referencaX_pocetakGrafika + sirinaGrafika;


        referencaX_pocetak_prednjeSTR = referencaX_krajGrafika + 3*pixelaUcm_Y;
        referencaX_kraj_prednjeSTR = referencaX_pocetak_prednjeSTR + bitmap_tablica.getWidth();
        referencaX_kraj=referencaX_kraj_prednjeSTR;
        referencaX_kraj_okvir = referencaX_kraj+ margina_5mm;


        /***** ide obrnuto -> kraj je na vrhu a pocetak na dnu ****/
        referencaY_kraj_okvir_isprek = margina_okvira;
        referencaY_kraj_okvir_PUNA = referencaY_kraj_okvir_isprek +  margina_5mm;
        referencaY_krajGrafika = referencaY_kraj_okvir_PUNA + dodatak;
        referencaY_kraj_prednjeSTR = referencaY_kraj_okvir_PUNA;
        referencaY_pocetakGrafika = referencaY_krajGrafika + visinaProfila*razmeraY;

        referencaY_kraj_tabeleStac = referencaY_pocetakGrafika + 3*pixelaUcm_Y; //3 cm raspojanje izmedju tabele i grafika

        if (nizStacionaza_STUB_X!=null) {
            // dva reda od po 1cm (rasponi i visine) i dva reda po 2cm(kote i stac)
            visinaTabele =  (1+1+2+2) * pixelaUcm_Y;
        }else visinaTabele=0;

        referencaY_pocetak_tabeleStac = referencaY_kraj_tabeleStac + visinaTabele;
        referencaY_pocetak_okvir_PUNA = referencaY_pocetak_tabeleStac+ margina_5mm;
        referencaY_pocetak_prednjeSTR= referencaY_pocetak_okvir_PUNA;
        referencaY_pocetak_okvir_isprek = referencaY_pocetak_okvir_PUNA + margina_5mm;

    }

    private void prilagodiRazmeri(){
        nizStacionaza_tlo_relativnoX =  prilagodiRazmeri_X(nizStacionaza_tlo_X, razmeraX);
        nizKota_tlo_relativnoY =        prilagodiRazmeri_Y(nizKota_tlo_Y, razmeraY,Y_osa_min);

        listaObjekti_relX = new ArrayList<>();
        listaObjekti_relY = new ArrayList<>();
        for (int niz = 0; niz < listaObjekti_X.size(); niz++) {
            listaObjekti_relX.add( prilagodiRazmeri_X(listaObjekti_X.get(niz), razmeraX) );
            listaObjekti_relY.add( prilagodiRazmeri_Y(listaObjekti_Y.get(niz), razmeraY, Y_osa_min) );
        }


        if (nizStacionaza_STUB_X!=null) {
            nizStacionaza_STUB_relativnoX = prilagodiRazmeri_X(nizStacionaza_STUB_X, razmeraX);
            nizKota_STUBiUZE_Y_relativno =  prilagodiRazmeri_Y(nizKota_STUBiUZE_Y, razmeraY,Y_osa_min);

            nizStacionaza_lancanica_X_rel = prilagodiRazmeri_X(nizStacionaza_lancanica_X, razmeraX);
            nizKota_lancanica_Y_rel =       prilagodiRazmeri_Y(nizKota_lancanica_Y, razmeraY,Y_osa_min);

            odstojanjeOznakeOdStuba_rel = odstojanjeOznakeOdStuba*razmeraY;
        }

        if (listaWycisk_stacionaze.size()>0) {
            for (int niz = 0; niz < listaWycisk_stacionaze.size(); niz++) {
                listaWycisk_stacionaze. set(niz, prilagodiRazmeri_X(listaWycisk_stacionaze.get(niz), razmeraX) );
                listaWycisk_kote.       set(niz, prilagodiRazmeri_Y(listaWycisk_kote.get(niz), razmeraY,Y_osa_min));
            }
            /*for (int i = 0; i < listaWycisk_stacionaze.size(); i++) {
                 if (BuildConfig.DEBUG)Log.e("TAG25", "CF listaWycisk_stacionaze    = "+ Arrays.toString(listaWycisk_stacionaze.get(i)));
                 if (BuildConfig.DEBUG)Log.e("TAG25", "CF listaWycisk_kote    = "+ Arrays.toString(listaWycisk_kote.get(i)));
            }*/
        }

    }

    private void crtanje(){
        paint_funkcija.setColor(Color.BLUE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            paint_tlo.setColor(mContext.getResources().getColor(R.color.tlo_braon, null));
            paint_stub.setColor(mContext.getResources().getColor(R.color.stub_dark_grey, null));
        }else{
            paint_tlo.setColor(mContext. getResources().getColor(R.color.tlo_braon));
            paint_stub.setColor(mContext. getResources().getColor(R.color.stub_dark_grey));
        }
        paint_funkcija_Wycisk.setColor(Color.RED);
        paint_text.setColor(Color.GRAY);

        paint_funkcija.setStrokeWidth(3);
        paint_tlo.setStrokeWidth(5);
        paint_stub.setStrokeWidth(5);
        paint_funkcija_Wycisk.setStrokeWidth(3);
        paint_text.setStrokeWidth(1);

        paint_funkcija.setAntiAlias(true);
        paint_tlo.setAntiAlias(true);
        paint_stub.setAntiAlias(true);
        paint_funkcija_Wycisk.setAntiAlias(true);
        paint_text.setAntiAlias(true);

        paint_tlo.setStrokeCap(Paint.Cap.ROUND);
        paint_stub.setStrokeCap(Paint.Cap.ROUND);

        ///////////////////////////////crtanje okvira/////////////////////////////////////////
        Paint paint_okvir= new Paint();
        paint_okvir.setPathEffect(new DashPathEffect(new float[]{30,10}, 0));
        paint_okvir.setStyle(Paint.Style.STROKE); // da ne ispunjava pravougaonik bojom, vec boji samo okvir
        paint_okvir.setStrokeWidth(1);
        paint_okvir.setColor(Color.DKGRAY);
        mCanvas.drawRect(referencaX_pocetak_okvir,
                referencaY_kraj_okvir_isprek,
                referencaX_kraj_okvir,
                referencaY_pocetak_okvir_isprek,
                paint_okvir);

        paint_okvir.setPathEffect(null);
        paint_okvir.setStrokeWidth(3);
        mCanvas.drawRect(referencaX_pocetak,
                referencaY_kraj_okvir_PUNA,
                referencaX_kraj,
                referencaY_pocetak_okvir_PUNA,
                paint_okvir);
        mCanvas.drawLine(referencaX_pocetak_prednjeSTR-1,
                referencaY_kraj_okvir_PUNA,
                referencaX_pocetak_prednjeSTR-1,
                referencaY_pocetak_okvir_PUNA,
                paint_okvir);

        ///////////////////////////////////////
        crtajPrvuStranu();

        ///////////////////////////////crtanje profila trase//////////////////////////////////
        for (int i = 0; i < nizStacionaza_tlo_relativnoX.length-1; i++) {
            crtanjeLinije_2tacke_grafik(nizStacionaza_tlo_relativnoX[i], nizKota_tlo_relativnoY[i],
                    nizStacionaza_tlo_relativnoX[i+1], nizKota_tlo_relativnoY[i+1],
                    paint_tlo);
            // if (BuildConfig.DEBUG)Log.e("TAG5","nizKota_tlo_relativnoY= "+ nizKota_tlo_relativnoY[i]);
            // if (BuildConfig.DEBUG)Log.e("TAG5","nizKota_tlo_Y= "+ nizKota_tlo_Y[i]);
        }
        ///////////////////////////////crtanje objekata ispod trase//////////////////////////////////
        if (listaObjekti_relX != null) {

             if (BuildConfig.DEBUG)Log.e("TAG34","SG listaObjekti_relX = "+listaObjekti_relX);
             if (BuildConfig.DEBUG)Log.e("TAG34","SG listaObjekti_relY = " +listaObjekti_relY);
        for (int niz = 0; niz < listaObjekti_relX.size(); niz++) {
            crtajObjekatIspodTrase(listaObjekti_relX.get(niz),listaObjekti_relY.get(niz), paint_tlo);
            }
        }
        ///////////////////////////////crtanje stubova//////////////////////////////////
        if (nizStacionaza_STUB_relativnoX != null) {
            float x, y;
            Paint paint_oznakaStuba = new Paint();
            paint_oznakaStuba.setAntiAlias(true);
            paint_oznakaStuba.setTextSize(velicinaTeksta_srednje);
            paint_oznakaStuba.setTextAlign(CENTER);
            paint_oznakaStuba.setColor(Color.GRAY);
            paint_oznakaStuba.setStyle(Paint.Style.STROKE);
            for (int i = 0; i < nizStacionaza_STUB_relativnoX.length; i++) {
                x = nizStacionaza_STUB_relativnoX[i];
                y = linearnaInterpolacija(
                        x, nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY);

                crtanjeLinije_2tacke_grafik(x, y,
                        x, nizKota_STUBiUZE_Y_relativno[i],
                        paint_stub);

                Rect rect = new Rect();
                String s = listaOznakaStubova.get(i).toString();
                paint_oznakaStuba.getTextBounds(s,0,s.length(), rect);


                paint_oznakaStuba.setStrokeWidth(2);
                paint_oznakaStuba.setStyle(Paint.Style.STROKE);
                mCanvas.drawCircle(referencaX_pocetakGrafika + x,
                        referencaY_pocetakGrafika - (nizKota_STUBiUZE_Y_relativno[i] + odstojanjeOznakeOdStuba_rel/2 +rect.height()/2),
                        2*razmeraY, paint_oznakaStuba);


                paint_oznakaStuba.setStyle(Paint.Style.FILL);
                mCanvas.drawText(listaOznakaStubova.get(i).toString(),
                        referencaX_pocetakGrafika + x,
                        referencaY_pocetakGrafika - (nizKota_STUBiUZE_Y_relativno[i] + odstojanjeOznakeOdStuba_rel/2),
                        paint_oznakaStuba);

                Spanned ss ; // ne radi na mojHtmlString drawText
                //idea is to print integers stored as doubles as if they are integers, and otherwise print the doubles with the minimum necessary precision
                if(listaVisina_stubova.get(i) == (long) listaVisina_stubova.get(i).floatValue())
                    ss = mojHtmlString("H<sub>uže</sub>="+ String.format("%d[m]",(long)listaVisina_stubova.get(i ).floatValue()));
                else
                    ss = mojHtmlString("H<sub>uže</sub>="+String.format("%s[m]",listaVisina_stubova.get(i ).floatValue()));
                mCanvas.drawText(ss.toString(),
                        referencaX_pocetakGrafika + x,
                        referencaY_pocetakGrafika - (nizKota_STUBiUZE_Y_relativno[i] + odstojanjeOznakeOdStuba_rel+10),
                        paint_oznakaStuba);
            }
        }
        ///////////////////////////////crtanje lancanice//////////////////////////////////
        if (nizStacionaza_lancanica_X_rel != null){
            Paint paint_pomocno = new Paint();
            paint_pomocno.setStrokeWidth(2);
            paint_pomocno.setAntiAlias(true);
            paint_pomocno.setColor(paint_funkcija.getColor());
            listaTacakaZaPrikaz_minVisina = new ArrayList<>();
            float y_koord_tlo, y_koord_lanc, tackaX=0, visinaLanc_min=1234567; //neki veliki broj, cisto da prodje if u prvom prelazu
            int brojacIsprekidaneLinije=0;
            int indexStuba=1;
            boolean prviPut = true;

            for (int i = 0; i < nizStacionaza_lancanica_X_rel.length-1; i++,brojacIsprekidaneLinije++) {

                y_koord_tlo = linearnaInterpolacija (
                        nizStacionaza_lancanica_X_rel[i] ,nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY );
                y_koord_lanc = nizKota_lancanica_Y_rel[i];
                if ( (y_koord_lanc - y_koord_tlo) < visinaLanc_min) {
                    visinaLanc_min = y_koord_lanc - y_koord_tlo;
                    tackaX = nizStacionaza_lancanica_X_rel[i];
                }
                /** !@#170518 debug
                 * upgrade
                 * */
                if ( nizStacionaza_lancanica_X_rel[i]>=nizStacionaza_STUB_relativnoX[indexStuba] || i==nizStacionaza_lancanica_X_rel.length-2){
                    listaTacakaZaPrikaz_minVisina.add(tackaX);
                    indexStuba++;
                    visinaLanc_min=1234567;
                }

                crtanjeLinije_2tacke_grafik(nizStacionaza_lancanica_X_rel[i],
                                            nizKota_lancanica_Y_rel[i],
                                            nizStacionaza_lancanica_X_rel[i+1],
                                            nizKota_lancanica_Y_rel[i+1],
                                            paint_funkcija);

                if (brojacIsprekidaneLinije==75){
                    if (paint_pomocno.getColor() == paint_funkcija.getColor()) {
                        paint_pomocno.setColor(Color.TRANSPARENT);
                        brojacIsprekidaneLinije=50;
                    }else{
                        paint_pomocno.setColor(paint_funkcija.getColor());
                        brojacIsprekidaneLinije=0;
                    }
                }
                crtanjeLinije_2tacke_grafik(nizStacionaza_lancanica_X_rel[i],
                                            nizKota_lancanica_Y_rel[i]      - sig_visina*razmeraY,
                                            nizStacionaza_lancanica_X_rel[i+1],
                                            nizKota_lancanica_Y_rel[i+1]    - sig_visina*razmeraY,
                                            paint_pomocno);

                /*
                     if (BuildConfig.DEBUG)Log.e("TAGconstruktorSlike", "x_rel= "+ String.format("%.2f",nizStacionaza_lancanica_X_rel[i])+
                            "  x_stacionaza= "+ String.format("%.2f",nizStacionaza_lancanica_X_rel[i]/razmeraX)+
                            "  y_koord_tlo= "+ String.format("%.2f",y_koord_tlo)+
                            "  y_koord_lanc= "+ String.format("%.2f",y_koord_lanc)+
                            "  razlikaY= "+ String.format( "%.2f",(y_koord_lanc-y_koord_tlo)/razmeraY )  +
                            "  sig_visina= "+ String.format("%.2f",sig_visina)+
                            "  razlikaYREl= "+ String.format( "%.2f",(y_koord_lanc-y_koord_tlo))  +
                            "  sig_visinaREL= "+ String.format("%.2f",sig_visina*razmeraY)
                    );
                    */

            }
        }

        ///////////////////////////////crtanje lancanice WYCISK//////////////////////////////////
        if (listaWycisk_stacionaze.size()>0){
            for (int niz =0; niz< listaWycisk_stacionaze.size(); niz++) {
                int brojacIsprekidaneLinije=0;
                for (int i = 0; i < listaWycisk_stacionaze.get(niz).length-1; i++,brojacIsprekidaneLinije++) {
                    float y_koord_tlo = linearnaInterpolacija (
                            listaWycisk_stacionaze.get(niz)[i] ,nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY );
                    float y_koord_lanc = linearnaInterpolacija (
                            listaWycisk_stacionaze.get(niz)[i] ,listaWycisk_stacionaze.get(niz), listaWycisk_kote.get(niz) );

                    /*if(y_koord_lanc-y_koord_tlo <= sig_visina*razmeraY)
                        paint_pomocno = paint_funkcija_prenisko_Wycisk;
                    else
                        paint_pomocno = paint_funkcija_Wycisk;*/


                    crtanjeLinije_2tacke_grafik(
                            listaWycisk_stacionaze.get(niz)[i], listaWycisk_kote.get(niz)[i],
                            listaWycisk_stacionaze.get(niz)[i+1], listaWycisk_kote.get(niz)[i+1],
                            paint_funkcija_Wycisk);

                }
            }
        }
        /////////////////////////crtanje tabele ispod grafika//////////////////////////////////
        if (nizStacionaza_STUB_relativnoX != null){
            crtajTabelu();
            nacrtajKoordSis(new PointF(referencaX_pocetak+2*pixelaUcm_x,referencaY_kraj_tabeleStac-3*pixelaUcm_Y),
                    40,10);
        }

    }

    private void crtajPrvuStranu(){
        String text;
        //int velicinaNaslova = velicinaGrafika_y_pix /20;
        float x,y; //koordinate pocetka teksta


        paint_text.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD_ITALIC));
        paint_text.setAlpha(128);
        paint_text.setTextSize(velicinaTeksta_VELIKO);
        //!@# 5.2.18-1
        text = /*mContext.getString(R.string.app_name)
                .toUpperCase() +*/
                "MPU "+BuildConfig.VERSION_NAME + " (api_" + Build.VERSION.SDK_INT + ")";

        x = referencaX_kraj_prednjeSTR - paint_text.measureText(text) - 1*pixelaUcm_x;
        y = referencaY_pocetak_prednjeSTR - 25*pixelaUcm_Y;
        paint_text.measureText(text);
        mCanvas.drawText(text, x, y, paint_text);

        x = referencaX_pocetak_prednjeSTR +  2*pixelaUcm_x;
        y += 7*pixelaUcm_Y;
        paint_text.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        paint_text.setTextSize(velicinaTeksta_VELIKO);
        paint_text.setAlpha(255); // iskljucena prozirnost [0max - 255min]
        String adresaFajla = fajlZaGrafik.toString();
        text = "Uzdužni profil trase: " + adresaFajla.substring(adresaFajla.lastIndexOf("/")+1)
                                                    .replace("grafik - ","")
                                                    .replace(".png","")
                                                    .replace(".pdf","")
                                                    .toUpperCase();
        String text_drugiRed="";
        String text_rec;
        while (referencaX_kraj_prednjeSTR-x < paint_text.measureText(text)){
            text_rec = text.substring(text.lastIndexOf(" "));
            text_drugiRed = text_rec + text_drugiRed;
            text = text.replace(text_rec, "");
        }
        mCanvas.drawText(text, x, y, paint_text);
        text_drugiRed = text_drugiRed.trim();
        if (text_drugiRed.length()>0) {
            y += paint_text.descent() - paint_text.ascent();
            mCanvas.drawText(text_drugiRed, x, y, paint_text);
        }


        ////////////////////ispisuje parametre lancanice////////////////////////////////
        if (nizStacionaza_STUB_X!=null)
            ispisiParametrePrveStrane(x, y + 2*pixelaUcm_Y);

        ////////////////////crta tablicu na prednjoj strani/////////////////////////////
        Rect rect = new Rect((int)referencaX_pocetak_prednjeSTR,
                (int)(referencaY_pocetak_prednjeSTR - bitmap_tablica.getHeight()),
                (int)(referencaX_pocetak_prednjeSTR + bitmap_tablica.getWidth()),
                (int)referencaY_pocetak_prednjeSTR);

        mCanvas.drawBitmap(bitmap_tablica,
                null,
                rect,
                null);

    }

    private  void sacuvajSliku() {
        new SaveImageAsync(mContext, mBitmap, fajlZaGrafik).execute();


    }

    private void crtanjeLinije_2tacke_grafik(float x_stacionaza_tla1, float y_kota_tla1,
                                      float x_stacionaza_tla2, float y_kota_tla2,
                                      Paint boja) {
        float x_start = referencaX_pocetakGrafika + x_stacionaza_tla1;
        float y_start = referencaY_pocetakGrafika - y_kota_tla1; // minus jer se pixeli broje od gore na dole
        float x_stop =  referencaX_pocetakGrafika + x_stacionaza_tla2;
        float y_stop =  referencaY_pocetakGrafika - y_kota_tla2;

        mCanvas.drawLine(x_start, y_start, x_stop, y_stop,   boja);

    }


    //////////////////////////////crtanje tabele/////////////////////////////////////////////////
    private float apcisa_0razVis;
    private float apcisa_1rasponi;
    private float apcisa_2stac;
    private float apcisa_3kote;
    private float apcisa_4krajnja;
    //private float konstantaZaPath_Y;
    private void crtajTabelu(){
        paint_kotiranje.setColor(Color.rgb(255,69,0)); //orange
        paint_kotiranje.setStrokeWidth(1);
        paint_kotiranje.setStyle(Paint.Style.STROKE);
        paint_kotiranje.setAntiAlias(true);
        paint_kotiranje.setTextAlign(CENTER);

        //konstantaZaPath_Y = (velicinaGrafika_y_pix +visinaTabele) - margina_pomeraja;

        apcisa_0razVis = 0;       // pocetna apsisa, iznad nje pisemo razlike visina izmedju stubova u rasponu
        apcisa_1rasponi = apcisa_0razVis + 1*pixelaUcm_Y;  // iznad nje pisemo raspone
        apcisa_2stac = apcisa_1rasponi + 1*pixelaUcm_Y;     // iznad nje pisemo stacionaze (na ordinati)
        apcisa_3kote = apcisa_2stac + 2*pixelaUcm_Y;   // iznad nje pisemo kote (na ordinati)
        apcisa_4krajnja = apcisa_3kote + 2*pixelaUcm_Y;     // poslednja, cisto zbog izgleda

        crtanjeApcise(); // crtamo apcise za prikaz razlike visina i raspona
        crtanjeApcise(apcisa_2stac, "stacionaža[m]");
        crtanjeApcise(apcisa_3kote, "kota[m]");
        crtanjeApcise(apcisa_4krajnja, "");

        // granica sa leve strane tabele
        mCanvas.drawLine(referencaX_pocetak_tabeleStac,
                referencaY_pocetak_tabeleStac,
                referencaX_pocetak_tabeleStac,
                referencaY_kraj_tabeleStac,
                paint_kotiranje);


        for (int i = 0; i < nizStacionaza_STUB_relativnoX.length; i++) {
            // crtamo ordinate za svaki stub, one su nam i granice za prikaz raspona
            // i razlika visina susednih stubova (visina vesanja uzeta)
            crtanjeOrdinate_stub(nizStacionaza_STUB_relativnoX[i]);
        }

        for (float f: listaTacakaZaPrikaz_minVisina){
            // crtamo ordinate za tacke sa min visinom u svakom rasponu
            crtanjeOrdinate_lanc(f);
        }

        for (float f: listaTacakaZaPrikazVisina){
            // crtamo ordinate za tacke koje je izabrao korisnik
            crtanjeOrdinate_lanc(f*razmeraX);
        }

    }

    private void crtanjeApcise() {
        // crta apcise apcisa_1rasponi i apcisa_0razVis, i tekst na njima
        Path path = new Path();
        paint_kotiranje.setTextSize(velicinaTeksta_srednje);
        paint_kotiranje.setTextAlign(LEFT);
        float raspon, razlVisina;

        path.moveTo(referencaX_pocetak_tabeleStac, referencaY_pocetak_tabeleStac -  apcisa_0razVis);
        path.lineTo(referencaX_pocetak_tabeleStac+sirinaDodatkaTabele, referencaY_pocetak_tabeleStac- apcisa_0razVis);
        mCanvas.drawPath(path, paint_kotiranje);
        mCanvas.drawTextOnPath("razlika u visini vesanja užeta [m]",
                path ,
                0.5f*pixelaUcm_Y,
                -0.3f*pixelaUcm_Y, // 2mm iznad apcise
                paint_kotiranje);
        path.reset();

        path.moveTo(referencaX_pocetak_tabeleStac, referencaY_pocetak_tabeleStac -  apcisa_1rasponi);
        path.lineTo(referencaX_pocetak_tabeleStac+sirinaDodatkaTabele, referencaY_pocetak_tabeleStac - apcisa_1rasponi);
        mCanvas.drawPath(path, paint_kotiranje);
        mCanvas.drawTextOnPath("raspon [m]",
                path ,
                0.5f*pixelaUcm_Y,
                -0.3f*pixelaUcm_Y, // 2mm iznad apcise
                paint_kotiranje);
        path.reset();


        paint_kotiranje.setTextAlign(CENTER);
        for (int i = 0; i < nizStacionaza_STUB_relativnoX.length-1; i++) {

            pathMoveToTabela(nizStacionaza_STUB_relativnoX[i],   apcisa_0razVis, path);
            pathLineToTabela(nizStacionaza_STUB_relativnoX[i+1], apcisa_0razVis, path);
            razlVisina = nizKota_STUBiUZE_Y[i+1]-nizKota_STUBiUZE_Y[i];
            mCanvas.drawPath(path, paint_kotiranje);
            mCanvas.drawTextOnPath(String.format("%.2f",razlVisina), /** !@# 01.03.2018-1 ne treba da se deli sa razmerom jer nisu relativne vrednosti */
                    path ,
                    0,
                    -0.2f*pixelaUcm_Y, // 2mm iznad apcise
                    paint_kotiranje);
            path.reset();

            pathMoveToTabela(nizStacionaza_STUB_relativnoX[i],  apcisa_1rasponi, path);
            pathLineToTabela(nizStacionaza_STUB_relativnoX[i+1],apcisa_1rasponi, path);
            raspon = nizStacionaza_STUB_relativnoX[i+1]-nizStacionaza_STUB_relativnoX[i];
            mCanvas.drawPath(path, paint_kotiranje);
            mCanvas.drawTextOnPath(String.format("%.2f",(raspon/razmeraX)),
                    path ,
                    0,
                    -0.2f*pixelaUcm_Y, // 2mm iznad apcise
                    paint_kotiranje);
            path.reset();
        }
    }

    private void pathMoveToTabela(float x, float y, Path path){
        path.moveTo(referencaX_pocetak_tabeleStac+sirinaDodatkaTabele +x ,
                    referencaY_pocetak_tabeleStac -y);

    }
    private void pathLineToTabela(float x, float y, Path path){
        path.lineTo(referencaX_pocetak_tabeleStac +sirinaDodatkaTabele+x ,
                referencaY_pocetak_tabeleStac -y);

    }

    private void crtanjeApcise(float visina_Y, String text) {
        // samo crtanje linija, bez teksta
        float x_start = referencaX_pocetak_tabeleStac;
        float y_start = referencaY_pocetak_tabeleStac -visina_Y; // minus jer se pixeli broje od gore na dole
        float x_stop = referencaX_kraj_tabeleStac;
        float y_stop = y_start;

        Path path = new Path();
        paint_kotiranje.setTextAlign(LEFT);

        path.moveTo(x_start, y_start);
        path.lineTo(x_start+sirinaDodatkaTabele,y_stop);
        mCanvas.drawPath(path, paint_kotiranje);
        mCanvas.drawTextOnPath(text,
                path ,
                0.5f*pixelaUcm_Y,
                -0.8f*pixelaUcm_Y, // 2mm iznad apcise
                paint_kotiranje);

        mCanvas.drawLine(x_start,y_start,x_stop,y_stop,paint_kotiranje);
    }



    //private boolean prvaOrdinata=true;
    private void crtanjeOrdinate_stub(float stacionaza_X) {
        Path path = new Path(); // zbog drawTextOnPath
        float offsetVer = -5;
        paint_kotiranje.setTextSize(velicinaTeksta_malo);
        paint_kotiranje.setTextAlign(CENTER);

        float xPocetak = referencaX_pocetak_tabeleStac + sirinaDodatkaTabele + stacionaza_X;
        float xKraj = xPocetak;
        float yPocetak = referencaY_pocetak_tabeleStac;
        // (- visina) tebele jer crtanjeLinije_2tacke dodaje samo (velicinaGrafika_y_pix)
        float yKraj = referencaY_pocetakGrafika - linearnaInterpolacija(
                stacionaza_X, nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY);

        // crtamo ordinate na osnovu izracunatih granica, bez teksta
        mCanvas.drawLine(xPocetak,  yPocetak, xKraj,  yKraj, paint_kotiranje);

        // upisujemo stacionaze iznad ordinate
        path.moveTo(xPocetak, yPocetak - apcisa_2stac);
        path.lineTo(xPocetak, yPocetak - apcisa_3kote);
        mCanvas.drawTextOnPath(" "+String.format("%.2f",(stacionaza_X/razmeraX)), path ,0,offsetVer,paint_kotiranje);

        //upisujemo kote iznat ordinate
        path.reset();
        path.moveTo(xPocetak, yPocetak - apcisa_3kote);
        path.lineTo(xPocetak, yPocetak - apcisa_4krajnja);
        mCanvas.drawTextOnPath(" "+String.format("%.2f",((referencaY_pocetakGrafika-yKraj)/razmeraY + Y_osa_min)), path ,0,offsetVer,paint_kotiranje);

    }

    private void crtanjeOrdinate_lanc(float stacionaza_X) {
        float xPocetak,xKraj,yPocetak,yKraj,tlo,visina;
        Path path = new Path();
        paint_kotiranje.setTextSize(velicinaTeksta_malo);
        paint_kotiranje.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
        float offsetVer = -5;


        for (int i = 0; i < nizStacionaza_STUB_relativnoX.length; i++) {
            // ako je data stac. stuba - onda ne treba da ove racunamo, jer je vec prikazana ordinata stuba
            if (stacionaza_X==nizStacionaza_STUB_relativnoX[i])
                return;
        }

        // da se ne bi preklapali sa ciframa od stubova
        int indexPocetnogStuba = indexSusednog_ManjegClanaNiza(stacionaza_X, nizStacionaza_STUB_relativnoX);
        if ( (stacionaza_X - nizStacionaza_STUB_relativnoX[indexPocetnogStuba])>0
                && (stacionaza_X - nizStacionaza_STUB_relativnoX[indexPocetnogStuba])  < 11*razmeraX ) {
            offsetVer = 15; // stelovani brojevi 15 i 11
            paint_kotiranje.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.ITALIC));
        }

//////////////// za slucaj kada imamo objekat////////////////////////////////
        if (tackaJeUnutarListe(stacionaza_X, listaObjekti_relX) ){

            xPocetak = referencaX_pocetakGrafika+ stacionaza_X;
            xKraj = xPocetak;
            int indexNiza = indexNizaUnutarListe(stacionaza_X, listaObjekti_relX);
            yPocetak = referencaY_pocetakGrafika - linearnaInterpolacija( stacionaza_X,
                    listaObjekti_relX.get(indexNiza), listaObjekti_relY.get(indexNiza));

            if (tackaJeUnutarListe(stacionaza_X, listaWycisk_stacionaze) ) {
                // ako imamo Wycisk lancanicu onda merimo u odnosu na nju
                indexNiza = indexNizaUnutarListe(stacionaza_X, listaWycisk_stacionaze);
                yKraj = referencaY_pocetakGrafika - linearnaInterpolacija(stacionaza_X,
                        listaWycisk_stacionaze.get(indexNiza), listaWycisk_kote.get(indexNiza));
            }else
                yKraj = referencaY_pocetakGrafika - linearnaInterpolacija( stacionaza_X,
                        nizStacionaza_lancanica_X_rel, nizKota_lancanica_Y_rel);

            mCanvas.drawLine(xPocetak,  yPocetak , xKraj,  yKraj, paint_kotiranje);

            visina = yPocetak - yKraj;
            String string = String.format("%.2f",(visina/razmeraY));
            paint_kotiranje.setTextSize(velicinaTeksta_malo);
            if (visina < paint_kotiranje.measureText(string)){
                // ako nema mesta da stane broj izmedju lancanice i objekta, onda ga pisemo iznad lanc
                path.reset();
                path.moveTo(xPocetak, yKraj);
                path.lineTo(xPocetak, yKraj-3/2*paint_kotiranje.measureText(string));
                mCanvas.drawPath(path,paint_kotiranje);
                paint_kotiranje.setTextSize(velicinaTeksta_malo-5);
                mCanvas.drawTextOnPath(string, path ,0,offsetVer,paint_kotiranje);
                crtajStrelice(path, xPocetak, yPocetak, yKraj, true); // kotiranje sa spoljne strane
            }else{
                path.reset();
                path.moveTo(xPocetak, yPocetak);
                path.lineTo(xPocetak, yKraj);
                mCanvas.drawTextOnPath(string, path ,0,offsetVer,paint_kotiranje);
                crtajStrelice(path, xPocetak, yPocetak, yKraj, false);
            }

            return;
        }
/////////////////////////////////////////////////////////////////////

        xPocetak = referencaX_pocetakGrafika+ stacionaza_X;
        xKraj = xPocetak;
        yPocetak = referencaY_pocetak_tabeleStac;
        // ovo je ustavari visina lancanice u tacki stacionaza_X --- proracunaa preko relativnih vrednosti
        yKraj = referencaY_pocetakGrafika - linearnaInterpolacija( stacionaza_X,
                                      nizStacionaza_lancanica_X_rel, nizKota_lancanica_Y_rel);
        tlo = referencaY_pocetakGrafika - linearnaInterpolacija( stacionaza_X,
                                      nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY);
        visina = tlo - yKraj;

        //crtanje linije bez teksta
        mCanvas.drawLine(xPocetak,  yPocetak - apcisa_2stac, xKraj,  yKraj, paint_kotiranje);

        //crta tekst (stacionaza) i liniju ispod teksta (od apcisa_2stac do apcisa_3kote)
        path.moveTo(xPocetak, yPocetak - apcisa_2stac);
        path.lineTo(xPocetak, yPocetak - apcisa_3kote);
        mCanvas.drawTextOnPath(" "+String.format("%.2f",(stacionaza_X/razmeraX)), path ,0,offsetVer,paint_kotiranje);

        //crta tekst (kota terena) i liniju ispod teksta (od apcisa_3kote do apcisa_4krajnja)
        path.reset();
        path.moveTo(xPocetak, yPocetak - apcisa_3kote);
        path.lineTo(xPocetak, yPocetak - apcisa_4krajnja);
        mCanvas.drawTextOnPath(" "+String.format("%.2f",((referencaY_pocetakGrafika-tlo)/razmeraY + Y_osa_min)), path ,0,offsetVer,paint_kotiranje);

        //crta tekst (visina lancanice) i liniju ispod teksta (od lancanice do tla)
        path.reset();
        path.moveTo(xPocetak, tlo);
        path.lineTo(xPocetak, yKraj);
        mCanvas.drawTextOnPath(String.format("%.2f",(visina/razmeraY)), path ,0,offsetVer,paint_kotiranje);


        /////////////////////////crtanje strelica za kotiranje visine uzeta////////////////////
        crtajStrelice(path, xPocetak, tlo, yKraj, false);

    }

    private void crtajStrelice(Path path, float x, float dole, float gore, boolean spoljnjeKotiranje){
        Paint arrow = new Paint();
        arrow.setColor(paint_kotiranje.getColor());
        arrow.setStyle(Paint.Style.FILL);
        arrow.setAntiAlias(true);

        float sirinaStrelice = 4;
        float visinaStrelice = 5;

        if (spoljnjeKotiranje){
            /////prekratka je duzina, pa kotiramo sa spoljnje strane, a tekst pisemo iznad (produzavamo duz sa gornje strane)
            mCanvas.drawLine(x,dole+3*visinaStrelice, x, gore-3*visinaStrelice, arrow);
            float pom=gore;
            gore=dole;
            dole=pom;
        }


        path.reset(); // strelica na dole
        path.moveTo(x, dole);
        path.rLineTo(-sirinaStrelice/2, -visinaStrelice);
        path.rLineTo(sirinaStrelice,0);
        path.close();
        mCanvas.drawPath(path,arrow);

        path.reset(); // strelica na gore
        path.moveTo(x, gore);
        path.rLineTo(-sirinaStrelice/2, visinaStrelice);
        path.rLineTo(sirinaStrelice,0);
        path.close();
        mCanvas.drawPath(path,arrow);
    }

    private void crtajObjekatIspodTrase(final float[] niz_X, final float[] niz_Y, final Paint boja) {
        Path path = new Path();
        Paint boja_pom = boja;
        int bojaOkvira = boja.getColor();
        boja_pom.setAntiAlias(true);
        boja_pom.setStyle(Paint.Style.FILL);
        boja_pom.setColor(Color.LTGRAY);

        ////// TODO: 15.11.17 radi samo ako je list_X rastuci
        float x_start = niz_X[0];
        float y_start = linearnaInterpolacija (
                x_start ,nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY ); // minus jer se pixeli broje od gore na dole


        path.moveTo(x_start+referencaX_pocetakGrafika, referencaY_pocetakGrafika-y_start);
        float x,y;
        for (int i = 0; i < niz_X.length; i++) {
            x=niz_X[i];
            y=niz_Y[i];
            path.lineTo( x + referencaX_pocetakGrafika,
                    referencaY_pocetakGrafika - y);
        }

        float x_kraj = niz_X[niz_X.length-1];
        float y_kraj = linearnaInterpolacija (
                x_kraj ,nizStacionaza_tlo_relativnoX, nizKota_tlo_relativnoY ); // minus jer se pixeli broje od gore na dole

        path.lineTo(x_kraj+referencaX_pocetakGrafika, referencaY_pocetakGrafika-y_kraj);

        path.close();

        mCanvas.drawPath(path,   boja_pom);
        boja_pom.setStyle(Paint.Style.STROKE);
        boja_pom.setStrokeWidth(3);
        boja_pom.setColor(bojaOkvira);
        mCanvas.drawPath(path,   boja_pom);

    }

    private void ispisiParametrePrveStrane(float x, float y){
        paint_text.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
        paint_text.setUnderlineText(true);
        paint_text.setTextSize(velicinaTeksta_srednje);

        mTipUzeta = js.getTipUzeta();
        koefDodOpt=js.getKoefDodOpt();
        temperatura=js.getTemperatura();
        idealniRaspon = js.getRaspon();
        kriticniRaspon = js.getAkr();
        specTezina = mTipUzeta.getSpecTezina();
        dodTezina = DodatnoOptSnegLed(koefDodOpt, mTipUzeta.getPrecnik(), mTipUzeta.getPresek());
        rezTezina =specTezina+dodTezina;

        String text = "parametri proračuna:                                     ";
        mCanvas.drawText(text, x, y, paint_text);

        float duzinaZatPolja = nizStacionaza_STUB_X[nizStacionaza_STUB_X.length-1]- nizStacionaza_STUB_X[0];
        paint_text.setUnderlineText(false);
        text =    "     tip užeta: " + mTipUzeta.getOznakaUzeta() +
                "\n     dužina zateznog polja = " +String.format("%.2f",duzinaZatPolja)+" [m]"+
                "\n     spec.tezina = " +String.format("%.6f",specTezina)+" [daN/m\u2219mm\u00B2]"+
                "\n     koef.ODO = " +koefDodOpt+
                "\n     rez. tezina = " +String.format("%.6f",rezTezina)+ " [daN/m\u2219mm\u00B2]"+
                "\n     temperatura = " + temperatura+" [\u00B0C]"+
                "\n     kriticni raspon = " + String.format("%3.2f",kriticniRaspon) + " [m]"+
                "\n     idealni raspon = " +String.format("%3.2f",idealniRaspon)+ " [m]"+
                "\n     sigurnosna visina(isprek.linija) = " + sig_visina+" [m]"+
                "\n     razmera za dužine 1:2000, za visine 1:500";


        for (String red: text.split("\n")) {
            y += paint_text.descent() - paint_text.ascent();
            mCanvas.drawText(red, x, y, paint_text);
        }

        if (listaWycisk_stacionaze.size()>0){ // @#$ 3 nikad nije null zbog kopiranjeArrayList
            text =    "     Crveno obojena linija predstavlja lančanicu pri -5[°C], kada u" +
                    "\n     datom rasponu ima leda, a u ostalim ne (proračun po WYCISK-u)!";
            y+=1*pixelaUcm_Y;
            for (String red: text.split("\n")) {
                y += paint_text.descent() - paint_text.ascent();
                mCanvas.drawText(red, x, y, paint_text);
            }
        }

    }

    private void nacrtajKoordSis(final PointF tacka0, float duzina_uMetrima_X, float duzina_uMetrima_Y){

        float duzinaOrdinataX = 2*duzina_uMetrima_X*razmeraX;
        float duzinaOrdinataY = 2*duzina_uMetrima_Y*razmeraY;

        Paint paint_linije = new Paint();
        paint_linije .setStyle(Paint.Style.STROKE);
        paint_linije .setStrokeWidth(1);
        paint_linije .setColor(Color.BLACK);
        paint_linije .setStrokeCap(Paint.Cap.SQUARE);
        paint_linije .setTextAlign(RIGHT);

        Paint paint_text = new Paint();
        paint_text .setStyle(Paint.Style.FILL_AND_STROKE);
        paint_text .setColor(Color.BLACK);
        paint_text .setTextSize(velicinaTeksta_srednje);
        paint_text .setAntiAlias(true);

        Paint paint_arrow = new Paint();
        paint_arrow. setColor(Color.BLACK);
        paint_arrow. setStyle(Paint.Style.FILL);
        paint_arrow. setAntiAlias(true);
        float sirinaStrelice = 8;
        float visinaStrelice = 10;
        Path path = new Path();
        PointF tackaX_kraj = new PointF();

////////////////////// crtanje x ordinate////////////
        tackaX_kraj.set(tacka0.x + duzinaOrdinataX, tacka0.y);
        path.moveTo(tacka0.x, tacka0.y);
        path.lineTo(tackaX_kraj.x, tackaX_kraj.y);
        mCanvas.drawPath(path, paint_linije);
        path.reset();
        path.moveTo(tackaX_kraj.x, tackaX_kraj.y);
        path.rLineTo(100, 0);
        paint_text.setTextAlign(LEFT);
        mCanvas.drawTextOnPath("stac.",
                path ,
                0,
                +0.6f*pixelaUcm_Y,
                paint_text);

        path.reset();
        path.moveTo(tacka0.x, tacka0.y);
        path.rMoveTo(duzina_uMetrima_X * razmeraX, -sirinaStrelice/2);
        path.rLineTo(0, sirinaStrelice);
        mCanvas.drawPath(path,paint_linije);
        paint_text.setTextAlign(CENTER);
        mCanvas.drawText((long)duzina_uMetrima_X+"m",
                tacka0.x+duzina_uMetrima_X * razmeraX,
                tacka0.y+0.6f*pixelaUcm_Y,
                paint_text
                );

        path.reset();
        path.moveTo(tackaX_kraj.x+visinaStrelice, tackaX_kraj.y);
        path.rLineTo(-visinaStrelice, -sirinaStrelice/2);
        path.rLineTo(0,sirinaStrelice);
        path.close();
        mCanvas.drawPath(path,paint_arrow);

////////////////////// crtanje y ordinate////////////
        path.reset();
        path.moveTo(tacka0.x, tacka0.y);
        tackaX_kraj.set(tacka0.x, tacka0.y - duzinaOrdinataY);
        path.lineTo(tackaX_kraj.x, tackaX_kraj.y);
        mCanvas.drawPath(path, paint_linije);
        path.reset();
        path.moveTo(tackaX_kraj.x, tackaX_kraj.y);
        path.rLineTo(0, -100);
        paint_text.setTextAlign(LEFT);
        mCanvas.drawTextOnPath("kote",
                path ,
                0,
                -0.2f*pixelaUcm_x,
                paint_text);

        path.reset();
        path.moveTo(tacka0.x, tacka0.y);
        path.rMoveTo(-sirinaStrelice/2, -duzina_uMetrima_Y*razmeraY);
        path.rLineTo(sirinaStrelice, 0);
        mCanvas.drawPath(path,paint_linije);
        paint_text.setTextAlign(LEFT);
        mCanvas.drawText((long)duzina_uMetrima_Y+"m",
                tacka0.x -sirinaStrelice -paint_text.measureText((long)duzina_uMetrima_Y+"m"),
                tacka0.y -duzina_uMetrima_Y*razmeraY + 0.1f*pixelaUcm_Y,
                paint_text
        );

        path.reset();
        path.moveTo(tackaX_kraj.x, tackaX_kraj.y-visinaStrelice);
        path.rLineTo( -sirinaStrelice/2,+visinaStrelice);
        path.rLineTo(sirinaStrelice,0);
        path.close();
        mCanvas.drawPath(path,paint_arrow);




    }



}
