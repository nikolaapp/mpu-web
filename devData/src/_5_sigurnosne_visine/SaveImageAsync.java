package nsp.mpu._5_sigurnosne_visine;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;

import static nsp.mpu.pomocne_klase.StaticPodaci.getSharPref;

@Obfuscate
public class SaveImageAsync extends AsyncTask<Void, String, Void> {

    private Context mContext;
    private Bitmap mBitmap;
    private File fajlZaGrafik;

    public boolean cuvanjeJeUspesno;

    private ProgressDialog mProgressDialog;
    private String tipFajla;

    public SaveImageAsync(Context context, Bitmap bitmap, File fajl) {
        mContext = context;
        mBitmap = bitmap;
        fajlZaGrafik = fajl;
        cuvanjeJeUspesno = false;
        tipFajla = getSharPref().getString("PNG_PDF",".png");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("Cuvanje grafika u toku...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... filePath) {
        if (tipFajla.equals(".png")) {
            try {
                FileOutputStream ostream = new FileOutputStream(fajlZaGrafik);
                /*quality:
                Hint to the compressor, 0 - 100.
                0 meaning compress for small size,
                100 meaning compress for max quality.
                Some formats, like PNG which is lossless, will ignore the quality setting*/
                mBitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);
                ostream.flush();
                ostream.close();

                /**odmah cistimo*/ mBitmap.recycle();mBitmap = null; ostream=null;System.gc();
                // FIXME: 19.11.17 ne radi lepo
                 if (BuildConfig.DEBUG)Log.e("TAG35", "mBitmap.recycle();mBitmap = null; ostream=null;System.gc();");

                //ako bi ovde prikazao toast doslo do runtimeexaption - ne bi stigao da ga prikaze a progres bar bi vec bio dismissed
                //Toast.makeText(mContext, "Grafik je sacuvan: " + PNGfajl, Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                 if (BuildConfig.DEBUG)Log.e("TAG35", "GRESKA - ne moze da se kreira png fajl");
                cuvanjeJeUspesno = false;
                e.printStackTrace();
                return null;
            }
        }
        else if (tipFajla.equals(".pdf")){
            Document document;
            Image image;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                mBitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
                mBitmap.recycle(); mBitmap = null; System.gc();  if (BuildConfig.DEBUG)Log.e("TAG35", "mBitmap.recycle()");

                image = Image.getInstance(byteArrayOutputStream.toByteArray());
                byteArrayOutputStream = null; System.gc(); if (BuildConfig.DEBUG)Log.e("TAG35", "byteArrayOutputStream = null;");

                document = new Document(image);
                PdfWriter.getInstance(document, new FileOutputStream(fajlZaGrafik));
                document.open();
                image.setAbsolutePosition(0, 0);
                document.add(image);
                document.close();

                image = null; document = null;System.gc(); if (BuildConfig.DEBUG)Log.e("TAG35", "image = null; document = null;System.gc()");
            } catch (Exception e) {
                cuvanjeJeUspesno = false;
                e.printStackTrace();
                 if (BuildConfig.DEBUG)Log.e("TAG35", "GRESKA - ne moze da se kreira pdf fajl");
                return null;
            }
        }

        cuvanjeJeUspesno = true;
        return null;
    }

    @Override
    protected void onPostExecute(Void filename) {
        mProgressDialog.dismiss();
        mProgressDialog = null;


        if (cuvanjeJeUspesno) {
            // odmah prikazujemo kreiran fajl
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(fajlZaGrafik.toString()));
            if (fajlZaGrafik.getAbsolutePath().endsWith("png")) {
                intent.setType("image/*");

                /***da bi se sacuvana slika odmah pojavila u albumu*/
                MediaScannerConnection.scanFile(mContext, new String[]{fajlZaGrafik.getAbsolutePath()}, null, null);

            }else if (fajlZaGrafik.getAbsolutePath().endsWith("pdf")) {
                intent.setType("application/pdf");
            }

            //samo ako postoji instaliran(registrovan) app za otvaranje png/pdf fajlova
            List<ResolveInfo> list = mContext.getPackageManager().queryIntentActivities(intent, 0);
            if (list.size() > 0){
                intent = Intent.createChooser(intent, "Odaberi program za pregled slike:");
                mContext.startActivity(intent);
            }
            else {
                Toast.makeText(mContext, "Grafik je sacuvan: " + fajlZaGrafik, Toast.LENGTH_LONG).show();
                return;
            }

        }else{
            Toast.makeText(mContext, "GREŠKA - ne može da se kreira "+tipFajla+" fajl", Toast.LENGTH_LONG).show();
        }

    }
}
