package nsp.mpu._5_sigurnosne_visine;

import android.content.Context;
import android.util.Log;

import java.util.Arrays;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.database.PODACI_ListaTipovaIzolatora;
import nsp.mpu.database.PODACI_ListaTipovaKonzola;
import nsp.mpu.database.PODACI_TipIzolatora;
import nsp.mpu.database.PODACI_TipKonzole;
import nsp.mpu.database.PODACI_TipStuba;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu.pomocne_klase.StaticPodaci.izolator_nije_odabrano;
import static nsp.mpu.pomocne_klase.StaticPodaci.konzola_trougao;

/**
 * Created by Nikola on 11.3.2018.
 */

@Obfuscate
public class StubnoMesto {

    // oznaka stubnog mesta
    public String oznaka;

    // rasponi
    public double srednjiRaspon, gravitacRaspon,gravitacRaspon_minus5;
    private double gravitacioniRaspon_otklonIzolatora;
    public double getGravRaspon_otklonIzol() {
        return gravitacioniRaspon_otklonIzolatora;
    }
    public void setGravRaspon_otklonIzol(double gravitacioniRaspon_otklonIzolatora) {
        this.gravitacioniRaspon_otklonIzolatora = gravitacioniRaspon_otklonIzolatora;
    }

    //raspored konzola na
    public StaticPodaci.Raspored_Konzola rasporedKonzola;

    // sile prema PTN - za cel.res stubove gde racunamo sile
    // pojedenacno, i to u svim pravcima, bez svodjenja na vrh stuba
    public double[] nizFa, nizFb, nizFc, nizFd, nizFe;

    // sile prema PTN - za betosnke stubove gde vertikalne sile racunamo
    // pojedenacno, a horizontalne svodimo na vrh stuba
    public double[] nizF1a, nizF1b, nizF2a;

    // sile za otklon izolatora - za visece izolatore na nosecim stubovima
    private double Fhz, Fvt;
    public double getFhOI() {return Fhz;}
    public void setFhOI(double fhz) {Fhz = fhz;}
    public double getFvOI() {return Fvt;}
    public void setFvOI(double fvt) {Fvt = fvt;}

    //ugao skretanja trase
    public int ugao;


    private PODACI_TipStuba mStub;
    public PODACI_TipStuba getStub(){
        return mStub;
    }
    public void setStub(PODACI_TipStuba stub){
         if (BuildConfig.DEBUG)Log.e("TAGstub", "setStub-"+ stub.toString());
        mStub = stub;
    }

    private PODACI_TipKonzole mKonzola;
    public PODACI_TipKonzole getKonzola(){
        return mKonzola;
    }
    public void setKonzola(PODACI_TipKonzole konzola){
         if (BuildConfig.DEBUG)Log.e("TAGstub", "setKonzola-"+ konzola.toString());
        mKonzola = konzola;
    }

    private PODACI_TipIzolatora mIzolator;
    public PODACI_TipIzolatora getIzolator(){
        return mIzolator;
    }
    public void setIzolator(PODACI_TipIzolatora izolator){
         if (BuildConfig.DEBUG)Log.e("TAGstub", "setIzolator-"+ izolator.toString());
        mIzolator = izolator;
    }

    //koristimo za proveru sig.razmaka
    /** inicijalizujemo sa true, a menjamo jedino ako je false*/
    public boolean bool_minRazmakJeZadovoljen = true;
    public double minRastojanjeLevo, minRastojanjeDesno;
    public double d12L, d23L, d13L, d12D, d23D, d13D;

    //Tip_Stuba tipStuba;
    public StubnoMesto(Context context, String oznaka, PODACI_TipStuba stub,
                       double srednjiRaspon, double gravitacRaspon, double gravitacRaspon_minus5) {
        this.oznaka = oznaka;
        this.srednjiRaspon = srednjiRaspon;
        this.gravitacRaspon = gravitacRaspon;
        this.gravitacRaspon_minus5 = gravitacRaspon_minus5;
        this.mStub = stub;

        mKonzola = PODACI_ListaTipovaKonzola.getSingltonKonzola(context).getTipKonzoleIzListe(konzola_trougao);
        mIzolator = PODACI_ListaTipovaIzolatora.getSingltonIzolatora(context).getTipIzolatoraIzListe(izolator_nije_odabrano);

        nizFa = new double[4]; // sile po x,y,z koordinati i Frez
        nizFb = new double[4];
        nizFc = new double[4];
        nizFd = new double[4];
        nizFe = new double[4];

        nizF1a = new double[2]; // vert. i horiz. sila - za betonske stubove
        nizF1b = new double[2]; // gde imamo svodjenje horiz. sila
        nizF2a = new double[2];

        rasporedKonzola = StaticPodaci.Raspored_Konzola.koso;

    }

    @Override
    public String toString() {
        return "*****"+oznaka
                +" stub:    "+ mStub.getOznaka()
                +" konzola: "+ mKonzola.getOznaka()
                +" izolator:"+ mIzolator.getOznaka()
                +" srednjiRas="+ String.format("%.2f", srednjiRaspon)
                +" gravitacRas="+String.format("%.2f", gravitacRaspon)
                +"\n Fa: "+ Arrays.toString(nizFa)
                +"\n Fb: "+Arrays.toString(nizFb)
                +"\n Fc: "+Arrays.toString(nizFc)
                +"\n Fd: "+Arrays.toString(nizFd)
                +"\n Fe: "+Arrays.toString(nizFe)
                ;
    }

/** pomocu bool_potrebanUpdate odredjejemo kada ce da se ponovo kreira
 * nizStubova - samo kada se doda nov stub iz menija, ili kada se
 * kliklne na prikazi ugibe*/
    private static boolean bool_potrebanUpdate;
    public static boolean getBool_potrebanUpdate(){
        return bool_potrebanUpdate;
    }
    public static void setBool_potrebanUpdate(boolean t){
        bool_potrebanUpdate=t;
    }

    // kreiranje niza stubnih mesta - za svaki stub koji po jedno stubno mesto
    private static StubnoMesto[] nizStubnihMesta;
    public static StubnoMesto[] getNizStubnimMesta() {
        return nizStubnihMesta;
    }
    public static void setNizStubnihMesta(StubnoMesto[] niz){
        nizStubnihMesta = niz.clone();
    }

}
