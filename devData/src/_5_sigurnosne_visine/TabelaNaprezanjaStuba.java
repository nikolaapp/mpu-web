package nsp.mpu._5_sigurnosne_visine;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import io.michaelrocks.paranoid.Obfuscate;
import jxl.write.WritableSheet;
import jxl.write.WriteException;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_ListaTipovaUzadi;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.StaticPodaci;
import nsp.mpu.pomocne_klase.ViewPager_ispravanWrapHeight;

import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Activity.kreirajFajlZaSave;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animirajTekst;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_alertDialog;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_IN;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_OUT;
import static nsp.mpu.pomocne_klase.StaticPodaci.Tip_Stuba.krajnji;
import static nsp.mpu.pomocne_klase.StaticPodaci.Tip_Stuba.noseći;
import static nsp.mpu.pomocne_klase.StaticPodaci.Tip_Stuba.zatezni;
import static nsp.mpu.pomocne_klase.StaticPodaci.getNizMaterijalStubova;
import static nsp.mpu.pomocne_klase.StaticPodaci.getSharPref;
import static nsp.mpu.pomocne_klase.StaticPodaci.mojHtmlString;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;

@Obfuscate
public class TabelaNaprezanjaStuba extends Fragment {
    private PODACI_TipUzeta mTU;
    private double sigma0, koefDodOpt;
    private double[] nizRaspona;
    private ArrayList listaOznakaStubova;
    private int brojStubova, brojRaspona;
    //public static double tez_izolator;
    public static int pv;
//    public static String materijalUze;
    public static String materijalStub;
    private static PagerAdapter pagerAdapter;
    private static ViewPager_ispravanWrapHeight viewPager;
    public static int temperatura;

    public static TabelaNaprezanjaStuba novFragment
            (String tipUzeta, double sigma0, double koefDodOpt, int temperatura,
             double[] nizRaspona, ArrayList listaOznakaStubova){
        Bundle args = new Bundle();
        args.putString("tipUzeta", tipUzeta);
        args.putDouble("sigma0", sigma0);
        args.putDouble("koefDodOpt", koefDodOpt);
        args.putInt("temperatura", temperatura);
        args.putDoubleArray("nizRaspona", nizRaspona);
        args.putParcelableArrayList("listaOznakaStubova",listaOznakaStubova);

         if (BuildConfig.DEBUG)Log.e("TAG41", "listaOznakaStubova 0= "+ listaOznakaStubova.toString());

        TabelaNaprezanjaStuba newFrag = new TabelaNaprezanjaStuba();
        newFrag.setArguments(args);
        return newFrag;
    }


    @Override
    public void onCreateOptionsMenu(Menu m, MenuInflater i) {
        super.onCreateOptionsMenu(m, i);
        i.inflate(R.menu.meni_naprezanje_stubova, m);
        // ctrl+shift+F <bool name="abc_config_actionMenuItemAllCaps">false</bool>
//        MenuItem export = m.findItem(R.id.menu_naprez_stub_export);
//        MenuItem help = m.findItem(R.id.menu_naprez_stub_help);
//        MenuItem find = m.findItem(R.id.menu_naprez_stub_find_a);
        findC = m.findItem(R.id.menu_naprez_stub_find_c);
        findE = m.findItem(R.id.menu_naprez_stub_find_e);
        resetujMeni();
    }

    MenuItem findC, findE;
    private void resetujMeni(){
        if (findC == null || findE == null)
            return;

        if (materijalStub .equals(StaticPodaci.MaterijalStuba.betonski.toString())){
                findC.setVisible(false);
                findE.setVisible(false);
        }
        else{
            findC.setVisible(true);
            findE.setVisible(true);
        }
    }

    int indexA, indexB, indexC, indexD, indexE;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        nadjiKojiSuTipoviStubaPrimenjeni();
         if (BuildConfig.DEBUG)Log.e("TAG10", "nadjiKojiSuTipoviStubaPrimenjeni");
        switch (item.getItemId()) {
            case R.id.menu_naprez_stub_export:
                exportUxls_dialog();
                return true;
            case R.id.menu_naprez_stub_help:
                prikaziHelpInfo();
                return true;
            case R.id.menu_naprez_stub_find_a:
                 if (BuildConfig.DEBUG)Log.e("TAG10", "--------------indexA= " + indexA);
                indexA++;
                 if (BuildConfig.DEBUG)Log.e("TAG10", "indexA++= " + indexA);
                indexA = indexA % listaPrimenjenihStubova.size();
                 if (BuildConfig.DEBUG)Log.e("TAG10", "indexA%= " + indexA + " lista=" +listaPrimenjenihStubova.toString());
                nadjiFmaxA(listaPrimenjenihStubova.get(indexA));
                 if (BuildConfig.DEBUG)Log.e("TAG10", "stub= " + listaPrimenjenihStubova.get(indexA));
                return true;
            case R.id.menu_naprez_stub_find_b:
                indexB++;
                indexB = indexB % listaPrimenjenihStubova.size();
                nadjiFmaxB(listaPrimenjenihStubova.get(indexB));
                return true;
            case R.id.menu_naprez_stub_find_c:
                indexC++;
                indexC = indexC % listaPrimenjenihStubova.size();
                nadjiFmaxC(listaPrimenjenihStubova.get(indexC));
                return true;
            case R.id.menu_naprez_stub_find_d:
                do{
                    indexD++;
                    indexD = indexD % listaPrimenjenihStubova.size();
                }while (listaPrimenjenihStubova.get(indexD) . equals(noseći));

                nadjiFmaxD(listaPrimenjenihStubova.get(indexD));
                return true;
            case R.id.menu_naprez_stub_find_e:
                indexE++;
                indexE = indexE % listaPrimenjenihStubova.size();
                nadjiFmaxE(listaPrimenjenihStubova.get(indexE));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    List<StaticPodaci.Tip_Stuba> listaPrimenjenihStubova = new ArrayList();
    private void nadjiKojiSuTipoviStubaPrimenjeni(){
        listaPrimenjenihStubova.clear();
        if (tipStubaPostojiUnizu(noseći))
            listaPrimenjenihStubova.add(noseći);
        if (tipStubaPostojiUnizu(zatezni))
            listaPrimenjenihStubova.add(zatezni);
        if (tipStubaPostojiUnizu(krajnji))
            listaPrimenjenihStubova.add(krajnji);
    }
    private boolean tipStubaPostojiUnizu(StaticPodaci.Tip_Stuba tipStuba){
        for (int i = 0; i < nizStubnihMesta.length; i++)
            if (nizStubnihMesta[i].getStub().getTip() . equals(tipStuba) )
                return true;

        return false;
    }

    private void nadjiFmaxA(StaticPodaci.Tip_Stuba tipStuba) {
        //int[] indexi = new int[nizStubova.length];
        double[] niz = new double[nizStubnihMesta.length];
        int indexMax=0;
        double maxValue=0;
        final TextView textView;

        if(materijalStub .equals(StaticPodaci.MaterijalStuba.betonski.toString())){
            for (int i = 0; i < niz.length; i++)
                if (nizStubnihMesta[i].getStub().getTip() . equals(tipStuba) &&
                        maxValue < nizStubnihMesta[i].nizF1a[1]){
                    maxValue = nizStubnihMesta[i].nizF1a[1];
                    indexMax = i;
                }

            viewPager.setCurrentItem(indexMax);
            textView = ((TabelaNaprezanjaStuba_single_tab)
                    pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                    .A1h;
        }
        else{
            for (int i = 0; i < niz.length; i++)
                if (nizStubnihMesta[i].getStub().getTip() . equals(tipStuba) &&
                        maxValue < nizStubnihMesta[i].nizFa[3]){
                    maxValue = nizStubnihMesta[i].nizFa[3];
                    indexMax = i;
                }

            viewPager.setCurrentItem(indexMax);
            textView = ((TabelaNaprezanjaStuba_single_tab)
                    pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                    .AFr;
        }

        textView.requestFocus();
        animirajTekst(textView);

    }
    private void nadjiFmaxB(StaticPodaci.Tip_Stuba tipStuba) {
        double[] niz = new double[nizStubnihMesta.length];
        int indexMax=0;
        double maxValue=0;
        final TextView textView;

        if(materijalStub .equals(StaticPodaci.MaterijalStuba.betonski.toString())){
            for (int i = 0; i < niz.length; i++)
                if (nizStubnihMesta[i].getStub().getTip() . equals(tipStuba) &&
                        maxValue < nizStubnihMesta[i].nizF1b[1]){
                    maxValue = nizStubnihMesta[i].nizF1b[1];
                    indexMax = i;
                }

            viewPager.setCurrentItem(indexMax);
            textView = ((TabelaNaprezanjaStuba_single_tab)
                    pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                    .B1h;
        }
        else{
            for (int i = 0; i < niz.length; i++)
                if (nizStubnihMesta[i].getStub().getTip() . equals(tipStuba) &&
                        maxValue < nizStubnihMesta[i].nizFb[3]){
                    maxValue = nizStubnihMesta[i].nizFb[3];
                    indexMax = i;
                }

            viewPager.setCurrentItem(indexMax);
            textView = ((TabelaNaprezanjaStuba_single_tab)
                    pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                    .BFr;
        }

        textView.requestFocus();
        animirajTekst(textView);
    }
    private void nadjiFmaxC(StaticPodaci.Tip_Stuba tipStuba) {
        double[] niz = new double[nizStubnihMesta.length];
        int indexMax=0;
        double maxValue=0;
        for (int i = 0; i < niz.length; i++)
            if (nizStubnihMesta[i].getStub().getTip() . equals(tipStuba) &&
                    maxValue < nizStubnihMesta[i].nizFc[3]){
                maxValue = nizStubnihMesta[i].nizFc[3];
                indexMax = i;
            }

        viewPager.setCurrentItem(indexMax);
        final TextView textView = ((TabelaNaprezanjaStuba_single_tab)
                pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                .CFr;
        textView.requestFocus();
        animirajTekst(textView);
    }
    private void nadjiFmaxD(StaticPodaci.Tip_Stuba tipStuba) {
        double[] niz = new double[nizStubnihMesta.length];
        int indexMax=0;
        double maxValue=0;
        final TextView textView;

        if(materijalStub .equals(StaticPodaci.MaterijalStuba.betonski.toString())){
            for (int i = 0; i < niz.length; i++)
                if (nizStubnihMesta[i].getStub().getTip() . equals(tipStuba) &&
                        maxValue < nizStubnihMesta[i].nizF2a[1]){
                    maxValue = nizStubnihMesta[i].nizF2a[1];
                    indexMax = i;
                }
            viewPager.setCurrentItem(indexMax);
            textView = ((TabelaNaprezanjaStuba_single_tab)
                    pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                    .A2h;
        }
        else{
            for (int i = 0; i < niz.length; i++)
                if (nizStubnihMesta[i].getStub().getTip() . equals(tipStuba) &&
                        maxValue < nizStubnihMesta[i].nizFd[3]){
                    maxValue = nizStubnihMesta[i].nizFd[3];
                    indexMax = i;
                }
            viewPager.setCurrentItem(indexMax);
            textView = ((TabelaNaprezanjaStuba_single_tab)
                    pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                    .DFr;
        }

        textView.requestFocus();
        animirajTekst(textView);
    }
    private void nadjiFmaxE(StaticPodaci.Tip_Stuba tipStuba) {
        double[] niz = new double[nizStubnihMesta.length];
        int indexMax=0;
        double maxValue=0;
        for (int i = 0; i < niz.length; i++)
            if (nizStubnihMesta[i].getStub().getTip() . equals(tipStuba) &&
                    maxValue < nizStubnihMesta[i].nizFe[3]){
                maxValue = nizStubnihMesta[i].nizFe[3];
                indexMax = i;
            }

        viewPager.setCurrentItem(indexMax);
        final TextView textView = ((TabelaNaprezanjaStuba_single_tab)
                pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                .EFr;
        textView.requestFocus();
        animirajTekst(textView);
    }

    private void prikaziHelpInfo(){
        AlertDialog ad = new AlertDialog.Builder(getContext()).create();
        ad.getWindow().setWindowAnimations(odaberiAnimaciju_alertDialog());
        ad.setTitle(mojHtmlString("<b>Pojašnjenje:"));
        ad.setIcon(R.drawable.ic_notify_help);

        ad.setCancelable(true); //on back click
        ad.setCanceledOnTouchOutside(true); //on outside click

        String string_odrOdg =
                "<html><body><p align=\"justify\">" +
                        "Prikazan je prora&#269un sila kojom provodnik deluje na " +
                        "konzolu, odnosno stub. Prora&#269un je u skladu sa <b>PTN za izgradnju nadzemnih EE vodova za Un=1-400kV " +
                        "(&#269l. 67 &#247 70)</b> ."+
                        "<br>Oznake 1a,1b,1c,2a,a odnose se na razli&#269ite vrste optere&#263enja, u skladu sa PTN." +

                        "<br><br><u>Za &#269eli&#269no - re&#353etkaste stubove</u>:" +
                        "<br>Ra&#269unate su sve tri komponente rezultantne sile (<b>Frez</b>) kojima svaki provodnik " +
                        "pojedina&#269no deluje na mesto u&#269vr&#353&#263enja na konzolu ili stub." +
                        "<br><b>Fx</b> - sila koja deluje upravno na pravac trase, odnosno upravno na simetralu ugla skretanja trase;" +
                        "<br><b>Fy</b> - sila koja deluje u pravcu trase, odnosno u pravcu simetrale ugla skretanja trase;" +
                        "<br><b>Fz</b> - sila koja deluje vertikalno nani&#382e, a usled te&#382ine u&#382eta i izolatora sa priborom."+

                        "<br><br><u>Za betonske stubove</u>:" +
                        "<br>Ra&#269unate su <b>vertikalne</b> sile kojima svaki provodnik pojedina&#269no deluje na konzolu(stub);" +
                        "<br>i <b>horizontalne</b> sile (prema TP10) od sva tri provodnika zajedno, svedene na vrh stuba." +
                "</p></body></html>";

        final WebView webView = new WebView(getContext());
        webView.loadData( string_odrOdg, "text/html", "utf-8");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            webView.setBackgroundColor(getResources().getColor(android.R.color.darker_gray,null));
        else
            webView.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));

        ad.setView(webView);

        ad.setButton(AlertDialog.BUTTON_POSITIVE, "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}
                });
        ad.show();

        ad.getButton(DialogInterface.BUTTON_POSITIVE).setAllCaps(false);
        ad.getWindow().setBackgroundDrawableResource(android.R.color.darker_gray);

        ((TextView) ad.findViewById(android.R.id.message)).setTextSize(R.dimen.text_size_velika);

    }

    String fileName;
    private void exportUxls_dialog(){
        AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
        ad.getWindow().setWindowAnimations(odaberiAnimaciju_alertDialog());
        ad.setTitle(mojHtmlString("<i>Sacuvaj tabele"));
        ad.setMessage(mojHtmlString("Unesi naziv pod kojim ce tabele biti sacuvane:"));

        final EditText inputEditText = new EditText(getContext());
        ad.setView(inputEditText);
        inputEditText.setText("staticki proracun stubova");


        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        fileName = inputEditText.getText().toString();
                        final File fajlExcel = kreirajFajlZaSave(getContext(), fileName+ ".xls");
                         if (BuildConfig.DEBUG)Log.e("TAG10", "fajlExcel " + fajlExcel.getAbsolutePath());

                        if (fajlExcel.exists()){
                            new AlertDialog.Builder(getActivity())
                                    .setIcon(R.mipmap.ic_alert_crveno)
                                    .setTitle(mojHtmlString("<b><i>Paznja!"))
                                    .setMessage(mojHtmlString("Vec postoji fajl <b>" +fileName+ ".xls"+
                                            "</b> Sacuvaj preko njega?"))
                                    .setPositiveButton("DA", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            kreirajTabele(fajlExcel);
                                        }
                                    })
                                    .setNegativeButton("NE", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            exportUxls_dialog();
                                        }
                                    })
                                    .show();
                        }
                        else{
                            kreirajTabele(fajlExcel);

                        }

                    }
                });
        ad.setButton(AlertDialog.BUTTON_NEGATIVE, "Odustani",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        ad.show();

    }

    private void kreirajTabele(File file){

        try {
            WriteExcel excelTabela = new WriteExcel(getContext(),file);
            WritableSheet sheet;
            boolean bool_noseci;
            int trenutniTab = viewPager.getCurrentItem();

            for (int i = 0; i < brojStubova ; i++) {

                /** kreiramo tabove pre nego sto uzmemo njegove varijable */
                viewPager.setCurrentItem(i);

                bool_noseci = nizStubnihMesta[i].getStub().getTip() . equals(StaticPodaci.Tip_Stuba.noseći);
                sheet = excelTabela.kreirajSheet(listaOznakaStubova.get(i).toString(),bool_noseci);
                excelTabela.upisiPodatke(sheet, nizStubnihMesta[i]);
            }

             if (BuildConfig.DEBUG)Log.e("TAGanim","getSheetNames="+ Arrays.toString(excelTabela.workbook.getSheetNames()));
             if (BuildConfig.DEBUG)Log.e("TAGanim","getNumberOfSheets="+excelTabela.workbook.getNumberOfSheets());

            viewPager.setCurrentItem(trenutniTab);
            excelTabela.workbook.write();
            excelTabela.workbook.close();
            prikaziToast(getContext(), "Excel tabela je kreirana");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }


    public StubnoMesto[] nizStubnihMesta;
    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setHasOptionsMenu(true);
        ((ProveraSigVisina_Activity) getActivity()).setActionBarTitle(getString(R.string.tabela_naprezanja_stubova));

        String s = getArguments().getString("tipUzeta");
        mTU= PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).getTipUzetaIzListe(s);
        sigma0 = getArguments().getDouble("sigma0");
        temperatura = getArguments().getInt("temperatura");
        nizRaspona = getArguments().getDoubleArray("nizRaspona");
        koefDodOpt = getArguments().getDouble("koefDodOpt");
        listaOznakaStubova=getArguments().getParcelableArrayList("listaOznakaStubova");
//         if (BuildConfig.DEBUG)Log.e("TAG41", "mTU = "+ mTU.getOznakaUzeta());
//         if (BuildConfig.DEBUG)Log.e("TAG41", "sigma0 = "+ sigma0);
//         if (BuildConfig.DEBUG)Log.e("TAG41", "koefDodOpt = "+ koefDodOpt);
//         if (BuildConfig.DEBUG)Log.e("TAG41", "nizRaspona = "+ Arrays.toString(nizRaspona));
//         if (BuildConfig.DEBUG)Log.e("TAG41", "nizTemenaLanc = "+ Arrays.toString(nizTemenaLanc));
//         if (BuildConfig.DEBUG)Log.e("TAG41", "listaOznakaStubova = "+ listaOznakaStubova.toString());

        brojStubova = listaOznakaStubova.size();
        brojRaspona = nizRaspona.length;

//         if (BuildConfig.DEBUG)Log.e("TAG10", "brojRaspona " + brojRaspona+
//                        "\n "+Arrays.toString(nizRaspona));
//         if (BuildConfig.DEBUG)Log.e("TAG10", "broj temena " + nizTemenaLanc.length+
//                "\n "+Arrays.toString(nizTemenaLanc));
//         if (BuildConfig.DEBUG)Log.e("TAG10", "lista oznaka " + listaOznakaStubova.size()+
//                "\n "+listaOznakaStubova.toString());

        if (brojRaspona != brojStubova-1){
            getActivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT(),
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT()
                    )
                    .remove(this).commit();
            prikaziToast(getContext(), "Greška, vrati se nazad!!!");
        }else {

            /** ucitavomo vec postojeci niz*/
            nizStubnihMesta = StubnoMesto.getNizStubnimMesta();


//            materijalUze = "provodnik";
            //materijalStub = "betonski";
            materijalStub = getSharPref().getString("materijalStub", StaticPodaci.MaterijalStuba.betonski.toString());
            pv = getSharPref().getInt("pv",60);

        }

    }

    EditText Vetar;
    //Spinner mUze;
    Spinner mStub;
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b){
        View v = li.inflate(R.layout.fragment_tabela_naprezanja_stuba, vg, false);
        if ( ! mDaLiJeTablet)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Vetar = (EditText) v.findViewById(R.id.napr_stuba_vred_vetar);
        mStub = (Spinner) v.findViewById(R.id.napr_stuba_vred_stub);

        Vetar.addTextChangedListener(generalTextWatcher);

        Vetar.setHint(String.format(Locale.getDefault(),"%d",pv));

//        final ArrayAdapter<String> adapterUze = new ArrayAdapter<>(getContext(),
//                android.R.layout.simple_spinner_dropdown_item, new String[]{"provodnik", "zaš.uže"});
//        mUze.setAdapter(adapterUze);
//        mUze.setSelection(0);
//        mUze.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                materijalUze = adapterView.getItemAtPosition(i).toString();
//                resetujTabAdapter();
//                 if (BuildConfig.DEBUG)Log.e("TAG41", "materijalUze="+ materijalUze);
//
//            }
//            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
//        });

        final ArrayAdapter<StaticPodaci.MaterijalStuba> adapterStub = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, getNizMaterijalStubova());
        mStub.setAdapter(adapterStub);
        int brojac=0;
        for (int i = 0; i < getNizMaterijalStubova().length; i++)
            if ((getNizMaterijalStubova()[i]).toString() .equals(materijalStub))
                brojac = i;
        mStub.setSelection(brojac);
        mStub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                materijalStub = adapterView.getItemAtPosition(i).toString();
                getSharPref().edit().putString( "materijalStub", materijalStub) .apply();
                resetujTabAdapter(brojStubova);
                resetujMeni();
                 if (BuildConfig.DEBUG)Log.e("TAG41", "materijalStub="+ materijalStub);

            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });


        pagerAdapter = new
                PagerAdapter(getChildFragmentManager());
        viewPager =
                (ViewPager_ispravanWrapHeight) v.findViewById(R.id.tab_layout_viewpager);
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tab_layout_tabs);
        tabLayout.setupWithViewPager(viewPager);


        //posto prikazuje prazan frag sve dok se ne odradi prvi swipe
        //onda ovako sumuliramo swipe da bi odmah dobio popunjen layout
        viewPager.setCurrentItem(brojStubova-1);
        viewPager.postDelayed(new Runnable() {
            @Override public void run() {
                for (int i = brojStubova-1; i >=0; i--)
                    viewPager.setCurrentItem(i);
            }
        },100);


        return v;
    }


    @Obfuscate//import
    private class PagerAdapter extends FragmentPagerAdapter {
        final int TAB_COUNT = brojStubova;

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public Fragment getItem(int index) {
//            boolean is_zatezni = false;
//            if (nizStubova[index].tipStuba.equals(zatezni))
//                is_zatezni=true;
            return TabelaNaprezanjaStuba_single_tab
                    .novFragment(mTU.getOznakaUzeta(),
                            sigma0, koefDodOpt,
                            index);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return listaOznakaStubova.get(position).toString() ;
        }
    }

    /** jedan TextWatcher za vise EditText :) */
    private TextWatcher generalTextWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before,int count) {

            if (Vetar.getText().hashCode() == s.hashCode()){
                int pomocnaVar = 0;
                try {
                    pomocnaVar = Integer.parseInt(s.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<50||pomocnaVar>150){
                    pv= getSharPref().getInt("pv",60);
                }else{
                    pv = pomocnaVar;
                }
                getSharPref().edit().putInt( "pv", pv) .apply();
                 if (BuildConfig.DEBUG)Log.e("TAG", "pv= " + pv);
            }
            resetujTabAdapter();
        }
        @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        @Override public void afterTextChanged(Editable s) {}

    };

    public static void resetujTabAdapter(){
        int n = viewPager.getCurrentItem();
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(n);
    }
    public static void resetujTabAdapter(final int brojTabova){
        final int n = viewPager.getCurrentItem();
        viewPager.setAdapter(pagerAdapter);

        viewPager.setCurrentItem(0);
        viewPager.postDelayed(new Runnable() {
            @Override public void run() {
                for (int i =0; i <brojTabova; i++)
                    viewPager.setCurrentItem(i);
                viewPager.setCurrentItem(n);
            }
        },100);

    }


    public static int[] nadjiIndekseOpadajucegNiza(double[] niz){
        // if (BuildConfig.DEBUG)Log.e("TAG10", "------------------niz " + Arrays.toString(niz));
        int[] indeksi = new int[niz.length];
        for (int i = 0; i < indeksi.length; i++)
            indeksi[i]=i;
        double[] opadajuciNiz = niz.clone();
        double temp;
        int tempInt;
        for(int i=0; i<opadajuciNiz.length; i++)
            for (int j = i + 1; j < opadajuciNiz.length; j++)
                if (opadajuciNiz[i] < opadajuciNiz[j]) {
                    // if (BuildConfig.DEBUG)Log.e("TAG10", "i" + i+ "="+opadajuciNiz[i]+" j" + j+ "="+opadajuciNiz[j] );
                    // if (BuildConfig.DEBUG)Log.e("TAG10", "indeksi " + Arrays.toString(indeksi));
                    tempInt = indeksi[j];
                    indeksi[j] = indeksi[i];
                    indeksi[i] = tempInt;

                    temp = opadajuciNiz[j];
                    opadajuciNiz[j] = opadajuciNiz[i];
                    opadajuciNiz[i] = temp;
                }

//         if (BuildConfig.DEBUG)Log.e("TAG10", "***indeksi " + Arrays.toString(indeksi));
//         if (BuildConfig.DEBUG)Log.e("TAG10", "opadajuciNiz       " + Arrays.toString(opadajuciNiz));
//        for (int i = 0; i < indeksi.length; i++)
//             if (BuildConfig.DEBUG)Log.e("TAG10", "opNiz preko indexa " + niz[indeksi[i]]);

        return indeksi;
    }

}
