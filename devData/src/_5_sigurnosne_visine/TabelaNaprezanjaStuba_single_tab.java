package nsp.mpu._5_sigurnosne_visine;

/**
 * Created by nikola.s.pavlovic on 14.2.2018.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_ListaTipovaUzadi;
import nsp.mpu.database.PODACI_TipIzolatora;
import nsp.mpu.database.PODACI_TipKonzole;
import nsp.mpu.database.PODACI_TipStuba;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.JednacinaStanja;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static android.view.View.GONE;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;
import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu._5_sigurnosne_visine.AlertDialog_TipOpreme.getDialog_izolatora;
import static nsp.mpu._5_sigurnosne_visine.AlertDialog_TipOpreme.getDialog_konzola;
import static nsp.mpu._5_sigurnosne_visine.TabelaNaprezanjaStuba.materijalStub;
import static nsp.mpu._5_sigurnosne_visine.TabelaNaprezanjaStuba.pv;
import static nsp.mpu.database.PODACI_ListaTipovaIzolatora.getSingltonIzolatora;
import static nsp.mpu.database.PODACI_ListaTipovaKonzola.getSingltonKonzola;
import static nsp.mpu.database.PODACI_ListaTipovaStubova.getSingltonStubova;
import static nsp.mpu.pomocne_klase.StaticPodaci.Tip_Stuba.krajnji;
import static nsp.mpu.pomocne_klase.StaticPodaci.Tip_Stuba.noseći;
import static nsp.mpu.pomocne_klase.StaticPodaci.Tip_Stuba.zatezni;
import static nsp.mpu.pomocne_klase.StaticPodaci.mojHtmlString;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;

@Obfuscate
public class TabelaNaprezanjaStuba_single_tab extends Fragment {

    PODACI_TipUzeta mTU;
    String oznakaStubnogMesta;
    double sigma0, koefDodOpt, srednjiRaspon,  gravitacRaspon;
    double tezUze_bezLeda;  // tezina samo uzeta
    double tezUzeLed; // tezina samo leda
    int ugao; // ugao skretanja trase
    double presek, precnik, specTezUze, specTezUzeLed;
    //boolean is_zatezni;
    int indexStuba;
    boolean bool_debug = false;
    private double gravitacRaspon_5;

    public static TabelaNaprezanjaStuba_single_tab novFragment
            (String tipUzeta, double sigma0, double koefDodOpt,int indexStubaUNizu){
        Bundle args = new Bundle();
        args.putInt("indexStubaUNizu",indexStubaUNizu);
        args.putString("tipUzeta", tipUzeta);
        args.putDouble("sigma0", sigma0);
        args.putDouble("koefDodOpt", koefDodOpt);

        TabelaNaprezanjaStuba_single_tab newFrag = new TabelaNaprezanjaStuba_single_tab();
        newFrag.setArguments(args);
        return newFrag;
    }

    StubnoMesto mStubnoMesto;
    PODACI_TipStuba mStub;
    PODACI_TipKonzole mKonzola;
    PODACI_TipIzolatora mIzolator;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sigma0 =        getArguments().getDouble("sigma0");
        koefDodOpt =    getArguments().getDouble("koefDodOpt");
        
        indexStuba=getArguments().getInt("indexStubaUNizu");
        mStubnoMesto = StubnoMesto.getNizStubnimMesta()[indexStuba];
        mStub = mStubnoMesto.getStub();
        mKonzola = mStubnoMesto.getKonzola();
        mIzolator = mStubnoMesto.getIzolator();
         if (BuildConfig.DEBUG)Log.e("TAGdebug", " mStub= " + mStub.getOznaka()+
                " mKonzola= " + mKonzola.getOznaka()+
                " mIzolator= " + mIzolator.getOznaka());


        srednjiRaspon =     mStubnoMesto.srednjiRaspon;
        gravitacRaspon=     mStubnoMesto.gravitacRaspon;
        gravitacRaspon_5=   mStubnoMesto.gravitacRaspon_minus5;
        oznakaStubnogMesta =        mStubnoMesto.oznaka;

        String s = getArguments().getString("tipUzeta");
        mTU= PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).getTipUzetaIzListe(s);
        presek = mTU.getPresek();
        precnik = mTU.getPrecnik();
        specTezUze = mTU.getSpecTezina();
        specTezUzeLed = specTezUze + JednacinaStanja.DodatnoOptSnegLed(koefDodOpt, precnik, presek);

        
        /** mnozi se sa presekom da bi dobili naprezanje u daN/m */
        tezUze_bezLeda = presek * specTezUze;
        tezUzeLed = presek * specTezUzeLed;

        ugao = mStubnoMesto.ugao;

        if (bool_debug){
            srednjiRaspon = 280;
            gravitacRaspon = 400;
            koefDodOpt = 1;
            sigma0 = 9;
            presek = 105;
            precnik = 13.4;
            tezUze_bezLeda = 0.368;
            tezUzeLed = 1.026;
            //tipStuba = Tip_Stuba.zatezni;
            //ugao = 50;


            if (true)
            {
                sigma0 = 9;
                srednjiRaspon = 120;//280
                gravitacRaspon = 200;//400
                presek = 81.3;//105;
                precnik = 11.7;//13.4;
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b){
        View v = li.inflate(R.layout.fragment_tabela_naprezanja_stuba_single_tab, vg, false);
        if ( ! mDaLiJeTablet)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        inicijalizujView(v);

        if (mStub.getTip() .equals(noseći)) {
            LL_D.setVisibility(GONE);
            //LL_ugao.setVisibility(GONE);
        }

        proracuni();
        return v;
    }

    double gravitacTezinaUze_bezLeda, gravitacTezinaUze_led;
    double tez_izol_ukupno; // za zatezne stubove uzimamo 2 komada po konzoli
    private double optZatezanje_1prov; // opterecenje stuba usled zatezanja uzeta sa jedne strane
    private double optZatezanje_RezProv; // opterecenje stuba usled zatezanja uzeta sa obe strane (isto sigma na obe strane)
    private double optVetar_prov; // opterecenje stuba usled dejstva vetra na materijalUze
    double optZatezanje_2prov;
    private void proracuni(){
         if (BuildConfig.DEBUG)Log.e("TAG41", "********************************"
                +"\noznakaStubnogMesta="+ oznakaStubnogMesta +" tipStuba="+mStub.getOznaka()
                +"\npresek="+presek+" tez_izol_rez="+tez_izol_ukupno
                +"\ntezUze_bezLeda="+ tezUze_bezLeda +" tezUzeLed="+tezUzeLed
                +"\nkoefDodOpt="+koefDodOpt+" sigma0="+sigma0+" ugao="+ugao
                +"\ngravitacRaspon="+gravitacRaspon+" gravitacRaspon_5="+gravitacRaspon_5
                +"\nsrednjiRaspon="+srednjiRaspon);

        optZatezanje_1prov = sigma0*presek;
        optZatezanje_2prov = 2 * optZatezanje_1prov * sin(toRadians(ugao/2.0));

        gravitacTezinaUze_bezLeda = gravitacRaspon * tezUze_bezLeda;
        gravitacTezinaUze_led = gravitacRaspon_5 * tezUzeLed;

        int brojIzolatoraNaKonzoli = mStub.getTip().equals(zatezni) ? 2 : 1;
        tez_izol_ukupno = brojIzolatoraNaKonzoli * mIzolator.getTezina();

        /** PTN cl.11 */
        final double kvp = 1;
        optVetar_prov = kvp * pv * (precnik*0.001) * srednjiRaspon;

        /*** nije bitan redosled */
        racunajA(); // normalno opt - tezina uzeta sa ledom
        racunajB();
        racunajC();
        if ( ! mStub.getTip() . equals(noseći) )
            racunajD(); // normalno opt - samo zatezni
        racunajE(); // vanredno opt

        // neka racuna uvek i F(A,B,C,D) za betonske i za cel.res stubove
        // mada ne mora u praksi - lakse mi u algoritmu bez jos if.ova
        // ako je krajnji stub onda je zatezanje samo sa jedne strane
        optZatezanje_RezProv = mStub.getTip().equals(krajnji) ? optZatezanje_1prov : optZatezanje_2prov;
        racunaj1a();
        racunaj1b();
        if ( ! mStub.getTip() . equals(noseći) )
            racunaj2a(); // normalno opt - samo zatezni

    }
    private void racunajA(){
        double x,y,z,rez;
        if (mStub.getTip() . equals(krajnji)) {
            x= optZatezanje_1prov * sin(toRadians(ugao/2.0)) ;
            y= optZatezanje_1prov * cos(toRadians(ugao/2.0)) ;
        }
        else {
            x= optZatezanje_2prov;
            y=0;
        }
        z= tez_izol_ukupno + gravitacTezinaUze_led;
        rez = Math.sqrt(x*x + y*y + z*z);

        AFx.setText(x==0?"":String.format("%d", Math.round(x)));
        AFy.setText(y==0?"":String.format("%d", Math.round(y)));
        AFz.setText(String.format("%d", Math.round(z)));
        AFr.setText(String.format("%d", Math.round(rez)));

        posaljiSile(indexStuba, mStubnoMesto.nizFa, x,y,z,rez);

    }
    private void racunajB(){
        double x,y,z,rez;
        if (mStub.getTip() . equals(krajnji)) {
            x= optVetar_prov + 2/3.0 * ( optZatezanje_1prov * sin(toRadians(ugao/2.0)) );
            y= 2/3.0 * ( optZatezanje_1prov * cos(toRadians(ugao/2.0)) );
        }
        else {
            x= optVetar_prov + 2/3.0 * optZatezanje_2prov; /** 2/3 bi dao int vred 0 ---bug */
            y=0;
        }

        z = tez_izol_ukupno + gravitacTezinaUze_bezLeda; //bez leda
        rez = Math.sqrt(x*x + y*y + z*z);

        BFx.setText(x==0?"":String.format("%d", Math.round(x)));
        BFy.setText(y==0?"":String.format("%d", Math.round(y)));
        BFz.setText(String.format("%d", Math.round(z)));
        BFr.setText(String.format("%d", Math.round(rez)));

        posaljiSile(indexStuba, mStubnoMesto.nizFb, x,y,z,rez);

    }
    private void racunajC(){
        double vredSinUgla = sin(toRadians(ugao/2.0));
        double minRedukcija = 0.25; // prema cl.70-3
        if (vredSinUgla<minRedukcija)
            vredSinUgla=minRedukcija;

        double x,y,z,rez;
        if (mStub.getTip() . equals(krajnji)) {
            x= 2/3.0 * ( optZatezanje_1prov * sin(toRadians(ugao/2.0)) );
            y= optVetar_prov * vredSinUgla + 2/3.0 * ( optZatezanje_1prov * cos(toRadians(ugao/2.0)) );
        }
        else {
            x= 2/3.0 * optZatezanje_2prov; /** 2/3 bi dao int vred 0 ---bug */
            y= optVetar_prov * vredSinUgla;
        }


        z = tez_izol_ukupno + gravitacTezinaUze_bezLeda; //bez leda
        rez = Math.sqrt(x*x + y*y + z*z);

        CFx.setText(x==0?"":String.format("%d", Math.round(x)));
        CFy.setText(y==0?"":String.format("%d", Math.round(y)));
        CFz.setText(String.format("%d", Math.round(z)));
        CFr.setText(String.format("%d", Math.round(rez)));

        posaljiSile(indexStuba, mStubnoMesto.nizFc, x,y,z,rez);

    }
    private void racunajD(){
        double x,y,z,rez;
        // posto ne znamo kakav je stub (mozda nije prstenasti) pa ne znamo
        // da li stub ima max vrednost Frez u svim pravcima ili se razlikuje
        // (zbog rasporeda temelja) za x i y pravac
        // zato racunamo obe komponente horizontalne sile -> i Fx i Fy
        x= 2/3.0 * ( optZatezanje_1prov * sin(toRadians(ugao/2.0)) );
        y= 2/3.0 * ( optZatezanje_1prov * cos(toRadians(ugao/2.0)) );

        z = tez_izol_ukupno + gravitacTezinaUze_bezLeda; //bez leda
        rez = Math.sqrt(x*x + y*y + z*z);

        DFx.setText(x==0?"":String.format("%d", Math.round(x)));
        DFy.setText(y==0?"":String.format("%d", Math.round(y)));
        DFz.setText(String.format("%d", Math.round(z)));
        DFr.setText(String.format("%d", Math.round(rez)));

        posaljiSile(indexStuba, mStubnoMesto.nizFd, x,y,z,rez);

    }
    private void racunajE(){
        double x, y, z, rez;

        /** PTN cl.69 */
        double koefOpterecenja;
        if (mStub.getTip() .equals (noseći))
            koefOpterecenja = 0.5;
        else
            koefOpterecenja = 1;


        x = koefOpterecenja * (optZatezanje_1prov * sin(toRadians(ugao / 2.0)));
        y = koefOpterecenja * (optZatezanje_1prov * cos(toRadians(ugao / 2.0)));
        z = tez_izol_ukupno + gravitacTezinaUze_led; //SA ledom
        rez = Math.sqrt(x * x + y * y + z * z);

        EFx.setText(x==0?"":String.format("%d", Math.round(x)));
        EFy.setText(y==0?"":String.format("%d", Math.round(y)));
        EFz.setText(String.format("%d", Math.round(z)));
        EFr.setText(String.format("%d", Math.round(rez)));

        posaljiSile(indexStuba, mStubnoMesto.nizFe, x,y,z,rez);

    }
    private void posaljiSile(int indexStuba,double [] niz, double x, double y, double z, double rez){
       niz[0] = Math.round(x);
       niz[1] = Math.round(y);
       niz[2] = Math.round(z);
       niz[3] = Math.round(rez);
       mStubnoMesto.ugao = ugao;

         if (BuildConfig.DEBUG)Log.e("TAGexcel","---------posaljiSile "+mStubnoMesto.oznaka+
                "\n niz: "+Arrays.toString(niz));
    }

    private double svediNaVrhStuba(){
        return (1+ 2*(1 - mKonzola.getH12()*0.01/mStub.getDuzina()));
    }
    private void racunaj1a(){
        double x,z;

        x= optZatezanje_RezProv * svediNaVrhStuba();

        z= tez_izol_ukupno + gravitacTezinaUze_led;

        A1v.setText(String.format("%d", Math.round(z)));
        A1h.setText(String.format("%d", Math.round(x)));

        posaljiSile(indexStuba, mStubnoMesto.nizF1a, z,x);

    }
    private void racunaj1b(){
        double x,z;

        /** PTN cl.11 */
        final double kv = 0.7;

        double Fvetar_prov,Fvetar_stub;

        /** sila vetra koja deluje na prstenasti betonski stub */
        Fvetar_stub = pritisakVetraNaStub(pv, kv,
                                mStub.getDuzina(),mStub.getDubina(),
                                mStub.getPrecnikVeci(), mStub.getPrecnikManji());

        /** svedene sile od sva tri provodnika na vrh stuba */
        Fvetar_prov = optVetar_prov * svediNaVrhStuba();

        x= 2/3.0 * optZatezanje_RezProv*svediNaVrhStuba()
                + Fvetar_prov + Fvetar_stub;

        // if (BuildConfig.DEBUG)Log.e("TAGstub", mStubnoMesto.toStringDimenz());
//         if (BuildConfig.DEBUG)Log.e("TAGstub", oznakaStubnogMesta +" x="+x+" optZatezanje="+2/3.0 * optZatezanje_RezProv+
//                " Fvetar_prov="+Fvetar_prov+" Fvetar_stub="+Fvetar_stub);

        z= tez_izol_ukupno + gravitacTezinaUze_bezLeda;

        B1v.setText(String.format("%d", Math.round(z)));
        B1h.setText(String.format("%d", Math.round(x)));

        posaljiSile(indexStuba, mStubnoMesto.nizF1b, z,x);

    }
    private void racunaj2a(){
        double x,z;

        x= 2/3.0 * optZatezanje_1prov * svediNaVrhStuba();

        z = tez_izol_ukupno + gravitacTezinaUze_bezLeda; //bez leda

        A2v.setText(String.format("%d", Math.round(z)));
        A2h.setText(String.format("%d", Math.round(x)));

        posaljiSile(indexStuba, mStubnoMesto.nizF2a, z,x);

    }
    private void posaljiSile(int indexStuba,double [] niz, double vert, double horiz){
        niz[0] = Math.round(vert);
        niz[1] = Math.round(horiz);
        mStubnoMesto.ugao = ugao;

         if (BuildConfig.DEBUG)Log.e("TAGexcel","---------posaljiSile "+mStubnoMesto.oznaka+
                "\n niz: "+Arrays.toString(niz));
    }

    private double pritisakVetraNaStub(double pv, double k, double Ln, double tu, double D, double d){
        double a,b,h,d1;
        d1 = D - tu*(D-d)/Ln;
        h  = Ln - tu;
        a  = d * h * (h/2.0 + tu);
        b  = h * (d1 - d)/2.0 * (h/3.0 + tu);
        return (1/Ln) * pv * k * (a + b);
    }

    public TextView SrRaspon, GrRaspon,
            AFx,AFy,AFz,AFr,
            BFx,BFy,BFz,BFr,
            CFx,CFy,CFz,CFr,
            DFx,DFy,DFr,DFz,
            EFx,EFy,EFr,EFz;
    public TextView A1v, A1h, B1v, B1h, A2v, A2h;
    private Spinner mSpinnerOznakaStuba;
    private Spinner mSpinnerOznakaKonzole;
    private Spinner mSpinnerOznakaIzolatora;
    private LinearLayout LL_D, LL_2a, LL_vanred_opt,
            LL_normalno_opt_cr, LL_normalno_opt_beton,
            LL_stub, LL_konzole, LL_izolator;
    EditText mUgao;
    //TextView mDimenzijeStuba;
    //public static Tip_Stuba[] itemS;
    private void inicijalizujView(View v){

        findViews(v);

        if (materijalStub . equals(StaticPodaci.MaterijalStuba.betonski.toString()))
            spinnerControl(getContext(), mSpinnerOznakaStuba,     getSingltonStubova(getContext()).getNizOznakaStub(StaticPodaci.MaterijalStuba.betonski), mStub.getOznaka());
        else
            spinnerControl(getContext(), mSpinnerOznakaStuba,     getSingltonStubova(getContext()).getNizOznakaStub(StaticPodaci.MaterijalStuba.čel_rešet), mStub.getOznaka());

        spinnerControl(getContext(), mSpinnerOznakaKonzole,   getSingltonKonzola(getContext()).getNizTipovaKonzola(StaticPodaci.MaterijalStuba.betonski), mKonzola.getOznaka());
        spinnerControl(getContext(), mSpinnerOznakaIzolatora, getSingltonIzolatora(getContext()).getNizTipovaIzolatora(), mIzolator.getOznaka());

        mSpinnerOznakaStuba.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String s = adapterView.getSelectedItem().toString();
                mStub =    getSingltonStubova(getContext()).getTipStubaIzListe(s);
                mStubnoMesto.setStub(mStub);
                tweakTabelu();
                proracuni();
                if (BuildConfig.DEBUG){
                    prikaziToast(getContext(),mStub.toString());
                }
            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });
        mSpinnerOznakaKonzole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String s = adapterView.getSelectedItem().toString();
                mKonzola = getSingltonKonzola(getContext()).getTipKonzoleIzListe(s);
                mStubnoMesto.setKonzola(mKonzola);
                tweakTabelu();
                proracuni();
                if (BuildConfig.DEBUG){
                    prikaziToast(getContext(),mKonzola.toString());
                }
            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });
        mSpinnerOznakaIzolatora.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String s = adapterView.getSelectedItem().toString();
                mIzolator = getSingltonIzolatora(getContext()).getTipIzolatoraIzListe(s);
                mStubnoMesto.setIzolator(mIzolator);
                tweakTabelu();
                proracuni();
                if (BuildConfig.DEBUG){
                    prikaziToast(getContext(),mIzolator.toString());
                }
            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        mSpinnerOznakaStuba.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog ad = AlertDialog_TipOpreme.getDialog_stub(getActivity());
                ad.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        spinnerControl(getContext(), mSpinnerOznakaStuba,     getSingltonStubova(getContext()).getNizOznakaStub(StaticPodaci.MaterijalStuba.betonski), mStub.getOznaka());
                    }
                });
                return true; // zavrsio sam sa longclick, ne treba mi i obican click
            }
        });
        mSpinnerOznakaKonzole.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog ad = getDialog_konzola(getActivity());
                ad.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        spinnerControl(getContext(), mSpinnerOznakaKonzole,     getSingltonKonzola(getContext()).getNizTipovaKonzola(StaticPodaci.MaterijalStuba.betonski), mKonzola.getOznaka());
                    }
                });
                return true;
            }
        });
        mSpinnerOznakaIzolatora.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog ad = getDialog_izolatora(getActivity());
                ad.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        spinnerControl(getContext(), mSpinnerOznakaIzolatora,     getSingltonIzolatora(getContext()).getNizTipovaIzolatora(), mIzolator.getOznaka());
                    }
                });
                return true;
            }
        });


        mUgao.setText( ugao==0 ? "" : (ugao+"") );
        mUgao.addTextChangedListener(new TextWatcher() {
            @Override public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                int pomocnaVar = -1;
                try {
                    pomocnaVar = Integer.parseInt(s.toString());
                } catch (NumberFormatException e) {
                }

                if (pomocnaVar < 0 || pomocnaVar > 90){
                    ugao = 0;
                }else{
                    ugao = pomocnaVar;
                    proracuni();
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "ugao= " + ugao);
            }
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override public void afterTextChanged(Editable editable) {}
        });

        SrRaspon.setText(String.format("%.2f", srednjiRaspon));
        GrRaspon.setText(String.format("%.2f", gravitacRaspon));

        tweakTabelu();
        
    }

    public static void spinnerControl(Context context, Spinner spinner, final Object[] items, Object oznaka ){
                 if (BuildConfig.DEBUG)Log.e("TAGstub","oznaka-"+oznaka.toString()+ " itemS-"+ Arrays.toString(items));
        ArrayAdapter adapter = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_dropdown_item, items);
        spinner.setAdapter(adapter);
        int poz = StaticPodaci.getIndexElementaUNizu( items, oznaka.toString());
        spinner.setSelection(poz);

    }

    private void findViews(View v){
        LL_D = (LinearLayout) v.findViewById( R.id.napr_stuba_single_ll_d );
        LL_2a = (LinearLayout) v.findViewById( R.id.napr_stuba_single_ll_2a );
        //LL_dimenz = (LinearLayout) v.findViewById( R.id.napr_stuba_single_ll_dimenz );
        LL_vanred_opt = (LinearLayout) v.findViewById( R.id.napr_stuba_single_ll_vanredno_opt);
        LL_normalno_opt_cr = (LinearLayout) v.findViewById( R.id.napr_stuba_single_ll_normalno_opt_cel_res);
        LL_normalno_opt_beton = (LinearLayout) v.findViewById( R.id.napr_stuba_single_ll_normalno_opt_betonski);
        LL_stub = (LinearLayout) v.findViewById( R.id.odabir_stuba_ll);
        LL_konzole = (LinearLayout) v.findViewById( R.id.odabir_konzole_ll);
        LL_izolator = (LinearLayout) v.findViewById( R.id.odabir_izolatora_ll);

        mSpinnerOznakaStuba     = (Spinner)v.findViewById( R.id.odabir_stuba_spiner );
        mSpinnerOznakaKonzole   = (Spinner)v.findViewById( R.id.odabir_konzole_spiner);
        mSpinnerOznakaIzolatora = (Spinner)v.findViewById( R.id.odabir_izolatora_spiner);

        mUgao = (EditText) v.findViewById( R.id.napr_stuba_single_unos_ugao );
        SrRaspon = (TextView)v.findViewById( R.id.napr_stuba_single_vred_srednji_raspon );
        GrRaspon = (TextView)v.findViewById( R.id.napr_stuba_single_vred_gravitac_raspon );

        AFx = (TextView)v.findViewById( R.id.napr_stuba_single_vred_a_fx );
        AFy = (TextView)v.findViewById( R.id.napr_stuba_single_vred_a_fy );
        AFz = (TextView)v.findViewById( R.id.napr_stuba_single_vred_a_fz );
        AFr = (TextView)v.findViewById( R.id.napr_stuba_single_vred_a_fr );
        BFx = (TextView)v.findViewById( R.id.napr_stuba_single_vred_b_fx );
        BFy = (TextView)v.findViewById( R.id.napr_stuba_single_vred_b_fy );
        BFz = (TextView)v.findViewById( R.id.napr_stuba_single_vred_b_fz );
        BFr = (TextView)v.findViewById( R.id.napr_stuba_single_vred_b_fr );
        CFx = (TextView)v.findViewById( R.id.napr_stuba_single_vred_c_fx );
        CFy = (TextView)v.findViewById( R.id.napr_stuba_single_vred_c_fy );
        CFz = (TextView)v.findViewById( R.id.napr_stuba_single_vred_c_fz );
        CFr = (TextView)v.findViewById( R.id.napr_stuba_single_vred_c_fr );
        DFx = (TextView)v.findViewById( R.id.napr_stuba_single_vred_d_fx );
        DFy = (TextView)v.findViewById( R.id.napr_stuba_single_vred_d_fy );
        DFz = (TextView)v.findViewById( R.id.napr_stuba_single_vred_d_fz );
        DFr = (TextView)v.findViewById( R.id.napr_stuba_single_vred_d_fr );
        EFx = (TextView)v.findViewById( R.id.napr_stuba_single_vred_e_fx );
        EFy = (TextView)v.findViewById( R.id.napr_stuba_single_vred_e_fy );
        EFz = (TextView)v.findViewById( R.id.napr_stuba_single_vred_e_fz );
        EFr = (TextView)v.findViewById( R.id.napr_stuba_single_vred_e_fr );

        A1v = (TextView)v.findViewById( R.id.napr_stuba_single_vred_1a_vert);
        A1h = (TextView)v.findViewById( R.id.napr_stuba_single_vred_1a_horiz);
        B1v = (TextView)v.findViewById( R.id.napr_stuba_single_vred_1b_vert);
        B1h = (TextView)v.findViewById( R.id.napr_stuba_single_vred_1b_horiz);
        A2v = (TextView)v.findViewById( R.id.napr_stuba_single_vred_2a_vert);
        A2h = (TextView)v.findViewById( R.id.napr_stuba_single_vred_2a_horiz);
    }

    private void tweakTabelu(){
        // if (BuildConfig.DEBUG)Log.e("TAGstub", "mStubnoMesto.oznaka-"+ mStubnoMesto.oznaka + " mStub.getOznaka()-" + mStub.getOznaka()+" mStub.getOznaka()-" + mStub.getOznaka());
        if (mStub.getTip() . equals(noseći)) {
            LL_D.setVisibility(View.GONE);
            LL_2a.setVisibility(View.GONE);
        }else {
            LL_D.setVisibility(View.VISIBLE);
            LL_2a.setVisibility(View.VISIBLE);
        }

        if (materijalStub . equals(StaticPodaci.MaterijalStuba.betonski.toString())) {
            LL_normalno_opt_beton.setVisibility(View.VISIBLE);
            LL_konzole.setVisibility(View.VISIBLE);
            //LL_stub.setVisibility(View.VISIBLE);
            LL_normalno_opt_cr.setVisibility(View.GONE);
            LL_vanred_opt.setVisibility(View.GONE);
        }else {
            LL_normalno_opt_beton.setVisibility(View.GONE);
            LL_konzole.setVisibility(View.GONE);
            //LL_stub.setVisibility(View.GONE);
            LL_normalno_opt_cr.setVisibility(View.VISIBLE);
            LL_vanred_opt.setVisibility(View.VISIBLE);
        }
    }

    private Spanned ispisiDimenzijeStuba(){
        Spanned s= mojHtmlString( "<u>***"+mStub.getDuzina()
                +"; "+mKonzola.getH12()*0.01
                +"; "+mStub.getDubina()
                +"; "+mStub.getPrecnikVeci()
                +"; "+mStub.getPrecnikManji()
        );
        prikaziToast(getContext(),s.toString());

        return s;
    }





}





