package nsp.mpu._5_sigurnosne_visine;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu._5_sigurnosne_visine.AlertDialog_TipOpreme.getDialog_izolatora;
import static nsp.mpu._5_sigurnosne_visine.AlertDialog_TipOpreme.getDialog_konzola;
import static nsp.mpu._5_sigurnosne_visine.TabelaNaprezanjaStuba_single_tab.spinnerControl;
import static nsp.mpu.database.PODACI_ListaTipovaIzolatora.getSingltonIzolatora;
import static nsp.mpu.database.PODACI_ListaTipovaKonzola.getSingltonKonzola;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animacijaZaRecycleView;
import static nsp.mpu.pomocne_klase.StaticPodaci.getSharPref;

/**
 * Created by Nikola on 26.3.2018.
 */

@Obfuscate
public class TabelaOtklonaIzolatora extends Fragment {

    private int pv, temperatura;
    private double rezTezUzeta;
    private double precnik;

    public static TabelaOtklonaIzolatora novFragment
            (int temperatura, double rezTezUzeta, double precnik){
        Bundle args = new Bundle();
        args.putInt("temperatura", temperatura);
        args.putDouble("rezTezUzeta", rezTezUzeta);
        args.putDouble("precnik", precnik);
        TabelaOtklonaIzolatora newFrag = new TabelaOtklonaIzolatora();
        newFrag.setArguments(args);
        return newFrag;
    }


    boolean bool_editText_changed;

    StubnoMesto[] nizNosecihStubnihMesta;
    int brojNosecihStubova;
    @Override
    public void onCreate(Bundle b){
        super.onCreate(b);
        ((ProveraSigVisina_Activity) getActivity()).setActionBarTitle(getString(R.string.tabela_otklona_izolatora));

        List<StubnoMesto> listaNosStMesta = new ArrayList();
        StubnoMesto[] nizStubnihMesta = StubnoMesto.getNizStubnimMesta();
        for (int i = 0; i < nizStubnihMesta.length; i++)
            if (nizStubnihMesta[i].getStub().getTip() . equals(StaticPodaci.Tip_Stuba.noseći))
                listaNosStMesta.add(nizStubnihMesta[i]);

        brojNosecihStubova = listaNosStMesta.size();

        nizNosecihStubnihMesta = new StubnoMesto[brojNosecihStubova];
        for (int i = 0; i < listaNosStMesta.size(); i++)
            nizNosecihStubnihMesta[i] = listaNosStMesta.get(i);

        temperatura = getArguments().getInt("temperatura");
        rezTezUzeta = getArguments().getDouble("rezTezUzeta");
        precnik = getArguments().getDouble("precnik");
        pv = getSharPref().getInt("pv", 60);

    }

    EditText mVetar, mTemperatura;
    RecyclerView mRecyclerView;
    AdapterStubnihMesta mAdapterRW;
    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b){
        View v;
        v = li.inflate(R.layout.fragment_otklon_izolatora, vg, false);
        if ( ! mDaLiJeTablet)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mTemperatura = (EditText) v.findViewById(R.id.otklon_izolatora_temperatura);
        mTemperatura.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) {
                if ( ! hasFocus && bool_editText_changed) {
                    startAdaptera();
                    bool_editText_changed = false;
                }
            }
        });
        mTemperatura.setHint("-20");
        mTemperatura.setText(temperatura+"");
        mTemperatura.setEnabled(false);

        mVetar = (EditText) v.findViewById(R.id.otklon_izolatora_vetar);
        mVetar.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) {
                if ( ! hasFocus && bool_editText_changed) {
                    startAdaptera();
                    bool_editText_changed = false;
                }
            }
        });
        mVetar.setHint(String.format(Locale.getDefault(), "%d", pv));

        /** dodaje se tek kada su findViewById svi editText !!! */
        mVetar.addTextChangedListener(generalTextWatcher);
        mTemperatura.addTextChangedListener(generalTextWatcher);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.tabela_otklona_recycle_view);

        startAdaptera();
        return v;
    }
    private TextWatcher generalTextWatcher = new TextWatcher() {
        @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
            bool_editText_changed = true;
             if (BuildConfig.DEBUG)Log.e("TAGgeneralTextWatcher", "bool_editText_changed= " +bool_editText_changed);
            if (mVetar.getText().hashCode() == s.hashCode()) {
                int pomocnaVar = 0;
                try {
                    pomocnaVar = Integer.parseInt(s.toString());
                } catch (NumberFormatException e) {}
                if (pomocnaVar < 50 || pomocnaVar > 150) {
                    pv = getSharPref().getInt("pv", 60);
                } else {
                    pv = pomocnaVar;
                }
                getSharPref().edit().putInt("pv", pv).apply();
                 if (BuildConfig.DEBUG)Log.e("TAG", "pv= " + pv);
            }
            else if (mTemperatura.getText().hashCode() == s.hashCode()) {
                int pomocnaVar = 0;
                try {
                    pomocnaVar = Integer.parseInt(s.toString());
                } catch (NumberFormatException e) {}
                if (pomocnaVar < -40 || pomocnaVar > 80) {
                    temperatura = -20;
                } else {
                    temperatura = pomocnaVar;
                }
                 if (BuildConfig.DEBUG)Log.e("TAG", "temperatura= " + temperatura);
            }
        }
        @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        @Override public void afterTextChanged(Editable s) {}
    };


    @Obfuscate//import
    private class HolderStubnihMesta extends RecyclerView.ViewHolder {
        private TextView mTextRasponSR;
        private TextView mTextRasponGR;
        private TextView mTextStub;
        private TextView mTextFv;
        private TextView mTextFh;
        private TextView mTextOtklon;
        private TextView mTextOdstStub;
        private TextView mTextOdstKonz;
        private Spinner mSpinerKonzola;
        private Spinner mSpinerIzolator;

        private StubnoMesto stubnoMesto;
        private String[] nizTipovaKonzola;
        private String[] nizTipovaIzolatora;


        private HolderStubnihMesta(View v) {
            super(v);
            mTextRasponSR   = (TextView) v.findViewById(R.id.rw_otklon_izol_sr_raspon);
            mTextRasponGR   = (TextView) v.findViewById(R.id.rw_otklon_izol_gr_raspon);
            mTextStub       = (TextView) v.findViewById(R.id.rw_otklon_izol_stub);
            mTextFv         = (TextView) v.findViewById(R.id.rw_otklon_izol_sila_vert);
            mTextFh         = (TextView) v.findViewById(R.id.rw_otklon_izol_sila_horiz);
            mTextOtklon     = (TextView) v.findViewById(R.id.rw_otklon_izol_otklon);
            mTextOdstStub   = (TextView) v.findViewById(R.id.rw_otklon_izol_odst_stub);
            mTextOdstKonz   = (TextView) v.findViewById(R.id.rw_otklon_izol_odst_konzola);

            mSpinerKonzola  = (Spinner) v.findViewById(R.id.rw_otklon_izol_konzola);
            mSpinerIzolator = (Spinner) v.findViewById(R.id.rw_otklon_izol_izolator);

            nizTipovaKonzola =  getSingltonKonzola(getContext()).getNizTipovaKonzola();
            nizTipovaIzolatora = getSingltonIzolatora(getContext()).getNizTipovaIzolatora();

            /** ne moze ovako jer getAdapterPosition je dostupan tek nakon onBIndholer
             * znaci moze da se koristi samo unutar listenera - jer listener je pokrenut iz vec kreiranog holera */
            //final int index = getAdapterPosition();

            /** kreiramo spinere, ali zbog ovog gore ne mozemo da mu odma upisemo odgovarajucu inicijalizaciju
             * vec ga setujemo na [0] - a u samom onbindholder mu upisemo pravu vrednost*/
            spinnerControl(getContext(), mSpinerKonzola,  nizTipovaKonzola, nizTipovaKonzola[0]);
            spinnerControl(getContext(), mSpinerIzolator, nizTipovaIzolatora, nizTipovaIzolatora[0]);

            mSpinerKonzola.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    String s = adapterView.getSelectedItem().toString();
                    stubnoMesto.setKonzola( getSingltonKonzola(getContext()).getTipKonzoleIzListe(s) );
                    mAdapterRW.notifyItemChanged(getAdapterPosition());
                     if (BuildConfig.DEBUG)Log.e("TAGspiner", "sel.ed item= " +s+ " new.holder.val="+nizNosecihStubnihMesta[getAdapterPosition()].getKonzola().getOznaka());

                }
                @Override public void onNothingSelected(AdapterView<?> adapterView) {}
            });
            mSpinerIzolator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    String s = adapterView.getSelectedItem().toString();
                    stubnoMesto.setIzolator( getSingltonIzolatora(getContext()).getTipIzolatoraIzListe(s) );
                    mAdapterRW.notifyItemChanged(getAdapterPosition());
                     if (BuildConfig.DEBUG)Log.e("TAGspiner", "sel.ed item= " +s+ " new.holder.val="+nizNosecihStubnihMesta[getAdapterPosition()].getIzolator().getOznaka());

                }
                @Override public void onNothingSelected(AdapterView<?> adapterView) {}
            });

            mSpinerKonzola.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    AlertDialog ad = getDialog_konzola(getActivity());
                    ad.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            spinnerControl(getContext(), mSpinerKonzola,     getSingltonKonzola(getContext()).getNizTipovaKonzola(), nizNosecihStubnihMesta[getAdapterPosition()].getKonzola().getOznaka());
                        }
                    });
                    return true;
                }
            });
            mSpinerIzolator.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    AlertDialog ad = getDialog_izolatora(getActivity());
                    ad.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            spinnerControl(getContext(), mSpinerIzolator,     getSingltonIzolatora(getContext()).getNizTipovaIzolatora(), nizNosecihStubnihMesta[getAdapterPosition()].getIzolator().getOznaka());
                        }
                    });
                    return true;
                }
            });


        }
    }
    @Obfuscate//import
    private class AdapterStubnihMesta extends RecyclerView.Adapter<TabelaOtklonaIzolatora.HolderStubnihMesta>{
        int brojNosecihStubova;

            public AdapterStubnihMesta(int brojNosecihStubova) {
            this.brojNosecihStubova = brojNosecihStubova;
        }

        @Override
        public TabelaOtklonaIzolatora.HolderStubnihMesta onCreateViewHolder(ViewGroup vg, int i){
            LayoutInflater li = LayoutInflater.from(getActivity());
            View v = li.inflate(R.layout.rw_otklon_izolatora,vg,false);
            v.setBackgroundResource(R.drawable.rw_pozadina_plavo);
            return new TabelaOtklonaIzolatora.HolderStubnihMesta(v);
        }

        @Override
        public void onBindViewHolder(final TabelaOtklonaIzolatora.HolderStubnihMesta holder, final int index){
            /** !@#160518
             * u onBindViewHolder ne traba da stoje nikakve alokacije memorije ---usporava
             * vec se to radi u HolderStubnihMesta
             * ovde se samo upisuje vrednosti u vec kreirane varijable i koriste
             * se vec kreirani listeneri*/
            holder.stubnoMesto = nizNosecihStubnihMesta[index];
            int poz = StaticPodaci.getIndexElementaUNizu( holder.nizTipovaKonzola, holder.stubnoMesto.getKonzola().getOznaka());
            holder.mSpinerKonzola.setSelection(poz);
            poz = StaticPodaci.getIndexElementaUNizu( holder.nizTipovaIzolatora, holder.stubnoMesto.getIzolator().getOznaka());
            holder.mSpinerIzolator.setSelection(poz);


            holder.mTextStub    .setText(holder.stubnoMesto.oznaka);
            holder.mTextRasponSR.setText(String.format("%.0f",holder.stubnoMesto.srednjiRaspon) );
            holder.mTextRasponGR.setText(String.format("%.0f",holder.stubnoMesto.gravitacRaspon) );

            double Fv = holder.stubnoMesto.getGravRaspon_otklonIzol() * rezTezUzeta
                        +holder.stubnoMesto.getIzolator().getTezina();
            holder.stubnoMesto.setFvOI(Fv);

            /** PTN cl.34 */
            final double kvp_70posto = 0.7;
            double Fh = kvp_70posto * pv * (precnik*0.001) * holder.stubnoMesto.srednjiRaspon;
            holder.stubnoMesto.setFhOI(Fh);

            holder.mTextFv      .setText(String.format("%.0f",holder.stubnoMesto.getFvOI()) );
            holder.mTextFh      .setText(String.format("%.0f",holder.stubnoMesto.getFhOI()) );

            double ugaoOtklona = Math.toDegrees( Math.atan(Fh/Fv) );
            holder.mTextOtklon  .setText(String.format("%.0f", ugaoOtklona) );

            double najkracaKonzola = Math.min(holder.stubnoMesto.getKonzola().getD2(),
                                                holder.stubnoMesto.getKonzola().getD2());
            if (holder.stubnoMesto.getKonzola().getD1() != 0)
                najkracaKonzola = Math.min(holder.stubnoMesto.getKonzola().getD1(),
                                            najkracaKonzola);


            //double pom_rezSila = Math.sqrt(Fh*Fh + Fv*Fv);
            double odstojanjeOdStuba = najkracaKonzola
                    - holder.stubnoMesto.getIzolator().getDuzina() * Math.sin( Math.toRadians(ugaoOtklona) );

            double odstojanjeOdKonzole = holder.stubnoMesto.getIzolator().getDuzina() * Math.cos( Math.toRadians(ugaoOtklona) );

            holder.mTextOdstStub.setText(String.format("%.1f",odstojanjeOdStuba) );
            holder.mTextOdstKonz.setText(String.format("%.1f",odstojanjeOdKonzole) );



        }
        @Override public int getItemCount(){
            return brojNosecihStubova;
        }
    }

    private void startAdaptera(){
        mAdapterRW = new TabelaOtklonaIzolatora.AdapterStubnihMesta(brojNosecihStubova);
        mRecyclerView.setAdapter(mAdapterRW);
        animacijaZaRecycleView(mRecyclerView, true);
    }

}
