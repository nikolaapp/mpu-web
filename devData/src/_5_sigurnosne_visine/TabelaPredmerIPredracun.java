package nsp.mpu._5_sigurnosne_visine;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_ListaTipovaIzolatora;
import nsp.mpu.database.PODACI_ListaTipovaKonzola;
import nsp.mpu.database.PODACI_ListaTipovaStubova;
import nsp.mpu.database.PODACI_ListaTipovaUzadi;
import nsp.mpu.database.PODACI_TipIzolatora;
import nsp.mpu.database.PODACI_TipKonzole;
import nsp.mpu.database.PODACI_TipStuba;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.StaticPodaci;
import nsp.mpu.pomocne_klase.StaticPodaciPredmer;

import static android.graphics.Color.BLACK;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animacijaZaRecycleView;
import static nsp.mpu.pomocne_klase.JednacinaStanja.IdealniCosFi;
import static nsp.mpu.pomocne_klase.StaticPodaci.MaterijalStuba.čel_rešet;
import static nsp.mpu.pomocne_klase.StaticPodaci.ispisiU_0dec;
import static nsp.mpu.pomocne_klase.StaticPodaci.ispisiU_1dec;
import static nsp.mpu.pomocne_klase.StaticPodaci.ispisiU_2dec;
import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.KM;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.KOM;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.KOMPLET;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.M3;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.PODNASLOV;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.daN_kg;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.geodSnimanje_stub;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.geodSnimanje_trasa;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.imovPravOdnosi;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.jCenaBetona;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.jCenaIskopa;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.koefPovecanja_def;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.m_km;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.ostaliRadovi;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.pripremaTrase;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.projektovanje_km;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.projektovanje_min;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.sitanMaterijal;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.uzemljenje_1prsten;
import static nsp.mpu.pomocne_klase.StaticPodaciPredmer.uzemljenje_2prsten;

/**
 * Created by Nikola on 26.3.2018.
 */

@Obfuscate
public class TabelaPredmerIPredracun extends Fragment {

    public static TabelaPredmerIPredracun novFragment
            (String tipUzeta, double[] nizRaspona, double[] nizCosFi){
        Bundle args = new Bundle();
        args.putString("tipUzeta", tipUzeta);
        args.putDoubleArray("nizRaspona", nizRaspona);
        args.putDoubleArray("nizCosFi", nizCosFi);
        TabelaPredmerIPredracun newFrag = new TabelaPredmerIPredracun();
        newFrag.setArguments(args);
        return newFrag;
    }


    List<ObjPredracun> listaPozicijaZaPrikaz;
    List<ObjPredmera> listaPozicijaZaPredmer_iskop;
    List<ObjPredmera> listaPozicijaZaPredmer_beton;
    StubnoMesto[] nizStubnihMesta;
    int brojStubova;
    @Override
    public void onCreate(Bundle b){
        super.onCreate(b);
        ((ProveraSigVisina_Activity) getActivity()).setActionBarTitle(getString(R.string.predmer_i_predracun));

        nizStubnihMesta = StubnoMesto.getNizStubnimMesta();
        brojStubova = nizStubnihMesta.length;

        listaPozicijaZaPrikaz = new ArrayList<>();
        listaPozicijaZaPredmer_iskop = new ArrayList<>();
        listaPozicijaZaPredmer_beton = new ArrayList<>();

        String s = getArguments().getString("tipUzeta");
        PODACI_TipUzeta tipUzeta = PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).getTipUzetaIzListe(s);
        double[] nizRaspona = getArguments().getDoubleArray("nizRaspona");
        double[] nizCosFi = getArguments().getDoubleArray("nizCosFi");
        float duzinaPolja_km = 0;
        for (double d : nizRaspona)
            duzinaPolja_km += d;
        duzinaPolja_km *= m_km;
        float cosFid = (float) IdealniCosFi(nizRaspona,nizCosFi);

        List<PODACI_TipStuba> listaStubovaBaze = PODACI_ListaTipovaStubova.getListuStubova();
        int[] nizStubova_UkBrojaPoIndeksima = new int[listaStubovaBaze.size()];
        List<PODACI_TipKonzole> listaKonzoleBaze = PODACI_ListaTipovaKonzola.getListuTipovaKonzola();
        int[] nizKonzola_UkBrojaPoIndeksima = new int[listaKonzoleBaze.size()];
        List<PODACI_TipIzolatora> listaIzolatoraBaze = PODACI_ListaTipovaIzolatora.getListuTipovaIzolatora();
        int[] nizIzolatora_UkBrojaPoIndeksima = new int[listaIzolatoraBaze.size()];
        
        int pomStub,pomKonz,pomIzol;
        for (int i = 0; i < brojStubova; i++) {
            pomStub = StaticPodaci.getIndexElementaUNizu(listaStubovaBaze, nizStubnihMesta[i].getStub().getOznaka() );
            pomKonz = StaticPodaci.getIndexElementaUNizu(listaKonzoleBaze, nizStubnihMesta[i].getKonzola().getOznaka() );
            pomIzol = StaticPodaci.getIndexElementaUNizu(listaIzolatoraBaze, nizStubnihMesta[i].getIzolator().getOznaka() );
            try {
                nizStubova_UkBrojaPoIndeksima[pomStub]++;
                nizKonzola_UkBrojaPoIndeksima[pomKonz]++;
                nizIzolatora_UkBrojaPoIndeksima[pomIzol]++;
            } catch(ArrayIndexOutOfBoundsException e) {
                 if (BuildConfig.DEBUG)Log.e("TAGpredmer", "greska stub index= " +i);
                e.printStackTrace();
            }
        }
         if (BuildConfig.DEBUG)Log.e("TAGpredmer", Arrays.toString(nizStubova_UkBrojaPoIndeksima));

        //long time=System.currentTimeMillis();


        int tezinaUzeta = tezinaUzeta(tipUzeta, duzinaPolja_km, cosFid);

        listaPozicijaZaPrikaz.add(new ObjPredracun("DOBRA", PODNASLOV , 0, 0));


        listaPozicijaZaPrikaz.add(new ObjPredracun(tipUzeta.getOznakaUzeta(), StaticPodaciPredmer.KG , tezinaUzeta, tipUzeta.getJcena()));
        listaPozicijaZaPrikaz.addAll( kreirajNizObjPoz(listaStubovaBaze, nizStubova_UkBrojaPoIndeksima));
        listaPozicijaZaPrikaz.addAll( kreirajNizObjPoz(listaKonzoleBaze, nizKonzola_UkBrojaPoIndeksima));
        listaPozicijaZaPrikaz.addAll( kreirajNizObjPoz(listaIzolatoraBaze, nizIzolatora_UkBrojaPoIndeksima));

        float ukSredMaterijal = ukSredstva(); // sredstva koja se odnose na materijal sa ugradnjom
        listaPozicijaZaPrikaz.add(new ObjPredracun("sitan materijal", KOMPLET , 1, sitanMaterijal*ukSredMaterijal));

        listaPozicijaZaPrikaz.add(new ObjPredracun("RADOVI", PODNASLOV , 0, 0));

        float ukIskop = 0;
        for (ObjPredmera obj : listaPozicijaZaPredmer_iskop)
            ukIskop += obj.getUkKol();
        listaPozicijaZaPrikaz.add(new ObjPredracun("zemljani radovi na izdradi temelja", M3 , ukIskop, jCenaIskopa));

        float ukBeton = 0;
        for (ObjPredmera obj : listaPozicijaZaPredmer_beton)
            ukBeton += obj.getUkKol();
        listaPozicijaZaPrikaz.add(new ObjPredracun("betoniranje temelja sa MB20", M3 , ukBeton, jCenaBetona));

        int stub_2prsten = (int) (0.2*brojStubova);
        int stub_1prsten = brojStubova - stub_2prsten;
        listaPozicijaZaPrikaz.add(new ObjPredracun("uzemljenje stuba jednim prstenom", KOM , stub_1prsten, uzemljenje_1prsten));
        listaPozicijaZaPrikaz.add(new ObjPredracun("uzemljenje stuba primenom dva prstena", KOM , stub_2prsten, uzemljenje_2prsten));

        listaPozicijaZaPrikaz.add(new ObjPredracun("radovi na uredjivanju trase", KM , duzinaPolja_km, pripremaTrase ));

        float ukSredRadovi = ukSredstva() - ukSredMaterijal; // sredstva koja se odnose radove
        listaPozicijaZaPrikaz.add(new ObjPredracun("ostali radovi", KOMPLET , 1, ostaliRadovi*ukSredRadovi ));

        listaPozicijaZaPrikaz.add(new ObjPredracun("USLUGE", PODNASLOV , 0, 0));

        listaPozicijaZaPrikaz.add(new ObjPredracun("eksproprijacija, odštete, rešavanje imovinsko pravnih odnosa", KM , duzinaPolja_km, imovPravOdnosi ));

        listaPozicijaZaPrikaz.add(new ObjPredracun("geodetsko obeležavanje stubnih mesta ", KOM , brojStubova, geodSnimanje_stub ));
        listaPozicijaZaPrikaz.add(new ObjPredracun("geodetsko snimanje trase voda sa izradom KTP", KM , duzinaPolja_km, geodSnimanje_trasa ));

        float projektovanje = duzinaPolja_km * projektovanje_km;
        if (projektovanje<projektovanje_min)
            projektovanje=projektovanje_min;
        listaPozicijaZaPrikaz.add(new ObjPredracun("projektna dokumen.", KOMPLET , 1, projektovanje ));


        // if (BuildConfig.DEBUG)Log.e("TAGpredmer","********* msec:"+ (System.currentTimeMillis() - time));

    }

    private int tezinaUzeta(PODACI_TipUzeta tipUzeta, double duzinaPolja_km, double cosFi){
        // spec.tezina u kg/m
        double tezina = tipUzeta.getSpecTezina()*tipUzeta.getPresek()/daN_kg;

        // ukupna tezina u m
        tezina *= 3*duzinaPolja_km*1000;//tezina *= 3*9151.35;

        // povecanje zbog ugla nagiba, minimum 5%
        float koefPovecanja = (float) (1 / cosFi);
        if ( koefPovecanja < koefPovecanja_def)
            koefPovecanja = koefPovecanja_def;
        tezina *= koefPovecanja;

        // ceil - zaokruzuje na prvu vecu vrednost
        return (int) Math.ceil (tezina);
    }

    private List<ObjPredracun> kreirajNizObjPoz(@NonNull final List lista, @NonNull final int [] niz){
        List<ObjPredracun> outLista = new ArrayList<>();
        if (lista == null ||  niz==null){
            StaticPodaci.prikaziWarnToast(getContext(),"Greska 120518-1, nije kreirana niti jedna pozicija!");
            return outLista;
        }
        else if (lista.size()==0 ||  niz.length==0){
            StaticPodaci.prikaziWarnToast(getContext(),"Greska 120518-2, kreirano je 0 pozicija!");
            return outLista;
        }


        if (lista.get(0) instanceof PODACI_TipStuba){
            PODACI_TipStuba tipStuba;
            for (int i = 0; i < niz.length; i++)
                if (niz[i]>0){
                    tipStuba = (PODACI_TipStuba)lista.get(i);
                    outLista.add(new ObjPredracun(
                                "stub: " + tipStuba.getOznaka(),
                                KOM ,
                                niz[i],
                                tipStuba.getJcena()
                    ));
                    listaPozicijaZaPredmer_iskop.add(new ObjPredmera(
                            "zemljani radovi: " + tipStuba.getOznaka(),
                            niz[i],
                            tipStuba.getIskop(),
                            M3
                    ));
                    listaPozicijaZaPredmer_beton.add(new ObjPredmera(
                            "betoniranje temelja: " + tipStuba.getOznaka(),
                            niz[i],
                            tipStuba.getBeton(),
                            M3
                    ));
                }
        }
        else if(lista.get(0) instanceof PODACI_TipKonzole){
            PODACI_TipKonzole tipKonzole;
            for (int i = 0; i < niz.length; i++)
                if (niz[i]>0){
                    tipKonzole = (PODACI_TipKonzole)lista.get(i);
                    if ( ! tipKonzole.getMaterijal() .equals(čel_rešet)) {
                        // ako je čel_rešet, onda ona se vec sadrzi u stubu
                        outLista.add(new ObjPredracun(
                                "konzola: " + tipKonzole.getOznaka(),
                                KOM,
                                niz[i],
                                tipKonzole.getJcena()
                        ));
                    }
                }
        }
        else if(lista.get(0) instanceof PODACI_TipIzolatora){
            PODACI_TipIzolatora tipIzolatora;
            for (int i = 0; i < niz.length; i++)
                if (niz[i]>0){
                    tipIzolatora = (PODACI_TipIzolatora)lista.get(i);
                    outLista.add(new ObjPredracun(
                            "izolator: " + tipIzolatora.getOznaka(),
                            KOM,
                            niz[i] * 3, /** po 3 izolatora na stubu*/
                            tipIzolatora.getJcena()
                    ));
                }
        }
        else{
            StaticPodaci.prikaziWarnToast(getContext(),"Greska 120518-3, nepoznat tip pozicije!");
            return outLista;
        }

         if (BuildConfig.DEBUG)Log.e("TAGpredmer", outLista.toString());

        return outLista;

    }

    RecyclerView mRecyclerView;
    AdapterPredmera mAdapterRW;
    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b){
        View v;
        v = li.inflate(R.layout.fragment_predmer_predracun, vg, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.predmer_recycleview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        tw_sredUk = (TextView) v.findViewById(R.id.predmer_tw_ukupno_sredstva);


        tw_sredUk.setText( "ukupno bez pdv [€]: " + String.format("%,.0f",ukSredstva()) );

        startAdaptera();
        return v;
    }
    static TextView tw_sredUk;
    private float ukSredstva(){
        float uk_sredstva = 0;
        for (ObjPredracun obj : listaPozicijaZaPrikaz)
            uk_sredstva += obj.getSredstva();
        return uk_sredstva;
    }

    @Obfuscate//import
    private class HolderPredmera extends RecyclerView.ViewHolder {
        private TextView mTextNaziv;
        private EditText mETkolicina;
        private EditText mETjedCena;
        private TextView mTextSredstva;
        private int index;
        //private ObjPredmera objPredmera;
        private HolderPredmera(View v) {
            super(v);
            mTextNaziv      = (TextView) v.findViewById(R.id.rw_predmer_tw_pozicija);
            mETkolicina     = (EditText) v.findViewById(R.id.rw_predmer_et_kolicine);
            mETjedCena      = (EditText) v.findViewById(R.id.rw_predmer_et_jcena);
            mTextSredstva   = (TextView) v.findViewById(R.id.rw_predmer_tw_sredstva);

            mETkolicina.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    // mora unutar onTextChanged
                    ObjPredracun objekat = listaPozicijaZaPrikaz.get(index);

                    float pomocno = nepostoji;

                    if (s.length()==0) { /** s.equals("") ne radi jer s nije string!!!*/
                        pomocno = 0;
                    } else {
                        try {
                            pomocno = Float.parseFloat(s.toString());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }
                    if (pomocno >= 0 && pomocno < 1_000_000){
                        objekat.setKol(pomocno);
                        mTextSredstva.setText(ispisiU_1dec(objekat.getSredstva()));
                        tw_sredUk.setText( "ukupno bez pdv [€]: " + ispisiU_0dec(ukSredstva()) );
                    }
                }
                @Override public void afterTextChanged(Editable s) {}
                @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            });
            mETjedCena.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    // mora unutar onTextChanged
                    ObjPredracun objekat = listaPozicijaZaPrikaz.get(index);

                    float pomocno = nepostoji;

                    if (s.length()==0) { /** s.equals("") ne radi jer s nije string!!!*/
                        pomocno = 0;
                    } else {
                        try {
                            pomocno = Float.parseFloat(s.toString());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }
                    if (pomocno >= 0 && pomocno < 1_000_000){
                        objekat.setJcena(pomocno);
                        mTextSredstva.setText(ispisiU_1dec(objekat.getSredstva()));
                        tw_sredUk.setText( "ukupno bez pdv [€]: " + String.format("%,.0f",ukSredstva()) );
                    }
                }
                @Override public void afterTextChanged(Editable s) {}
                @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            });

        }
        void updateIndex(int index){
            this.index=index;
        }
    }
    @Obfuscate//import
    private class AdapterPredmera extends RecyclerView.Adapter<TabelaPredmerIPredracun.HolderPredmera>{
        int rwBrojPozicija;
        public AdapterPredmera(int brojPoz) {
            rwBrojPozicija = brojPoz;
        }

        @Override
        public TabelaPredmerIPredracun.HolderPredmera onCreateViewHolder(ViewGroup vg, int i){
            LayoutInflater li = LayoutInflater.from(getActivity());
            View v = li.inflate(R.layout.rw_predmer_predracun,vg,false);
            //v.setBackgroundResource(R.drawable.rw_pozadina_plavo);
            return new TabelaPredmerIPredracun.HolderPredmera(v);
        }

        @Override
        public void onBindViewHolder(final TabelaPredmerIPredracun.HolderPredmera holder, final int index){

            holder.updateIndex(index);

            final ObjPredracun objekat = listaPozicijaZaPrikaz.get(index);

            if (objekat.getJedMere() . equals(PODNASLOV)){
                holder.mTextNaziv.setText( objekat.getNazivPoz() );
                holder.mTextNaziv.setTextColor(BLACK);
                holder.mTextNaziv.setTypeface(null, Typeface.ITALIC);
                holder.mTextNaziv.setBackgroundColor(Color.LTGRAY);
                holder.mTextNaziv.setPadding(0,25,0,0);
                holder.mETkolicina.setVisibility(View.GONE);
                holder.mETjedCena.setVisibility(View.GONE);
                holder.mTextSredstva.setVisibility(View.GONE);
            }
            else {
                holder.mTextNaziv.setTextColor(Color.DKGRAY);
                holder.mTextNaziv.setTypeface(null, Typeface.NORMAL);
                holder.mTextNaziv.setBackgroundColor(Color.TRANSPARENT);
                holder.mTextNaziv.setPadding(0,0,0,0);
                holder.mETkolicina.setVisibility(View.VISIBLE);
                holder.mETjedCena.setVisibility(View.VISIBLE);
                holder.mTextSredstva.setVisibility(View.VISIBLE);

                holder.mTextNaziv.setText(objekat.getNazivPoz() + " " + StaticPodaci.mojHtmlString("<b>" + objekat.getJedMereZadrade() + "</b>")); // FIXME: 13.5.2018 nece da boldira
                holder.mETkolicina.setText(ispisiU_2dec(objekat.getKol()));
                holder.mETjedCena.setText(ispisiU_1dec(objekat.getJcena()));
                holder.mTextSredstva.setText(ispisiU_0dec(objekat.getSredstva()));
            }
        }
        @Override public int getItemCount(){
            return rwBrojPozicija;
        }
    }

    private void startAdaptera(){
        mAdapterRW = new TabelaPredmerIPredracun.AdapterPredmera(listaPozicijaZaPrikaz.size());
        mRecyclerView.setAdapter(mAdapterRW);
        animacijaZaRecycleView(mRecyclerView, true);
    }

    @Obfuscate//import
    class ObjPredracun{
        String nazivPoz;
        String jedMere;
        float kol, jcena, sredstva;

        ObjPredracun(String nazivPoz, String jedMere, float kol, float jcena) {
            this.nazivPoz = new String(nazivPoz) ;
            this.jedMere = new String(jedMere) ;
            this.kol = kol;
            this.jcena = jcena;
            sredstva = kol*jcena;
        }

        @Override
        public String toString() {
            return nazivPoz + " "+jedMere+" "+kol+" "+jcena+" "+sredstva;
        }

        public String getNazivPoz() {
            return nazivPoz;
        }
        public String getJedMere() {
            return  jedMere;
        }
        public String getJedMereZadrade() {
            return  "["+jedMere+"]";
        }
        public float getKol() {
            return kol;
        }
        public float getJcena() {
            return jcena;
        }
        public float getSredstva() {
            return kol*jcena;
        }

        // todo: 14.5.2018 neka cuva vrednosti radova i usluga u SharPref
        // todo: 14.5.2018 neka cuva vrednosti materijala u bazi
        public void setKol(float kol) {
            this.kol = kol;
        }
        public void setJcena(float jcena) {
            this.jcena = jcena;
        }
    }

    @Obfuscate//import
    class ObjPredmera{
        String nazivPoz;
        int komada;
        float jKol;
        float ukKol;
        String jMere;

        public ObjPredmera(String nazivPoz, int komada, float jKol, String jMere) {
            this.nazivPoz = nazivPoz;
            this.komada = komada;
            this.jKol = jKol;
            this.jMere = jMere;
            ukKol = komada * jKol;
        }

        public String getNazivPoz() {
            return nazivPoz;
        }

        public void setNazivPoz(String nazivPoz) {
            this.nazivPoz = nazivPoz;
        }

        public int getKomada() {
            return komada;
        }

        public void setKomada(int komada) {
            this.komada = komada;
        }

        public float getjKol() {
            return jKol;
        }

        public void setjKol(float jKol) {
            this.jKol = jKol;
        }

        public float getUkKol() {
            return ukKol;
        }

        public void setUkKol(float ukKol) {
            this.ukKol = ukKol;
        }
    }


}
