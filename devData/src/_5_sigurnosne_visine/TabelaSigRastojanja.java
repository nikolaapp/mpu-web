package nsp.mpu._5_sigurnosne_visine;
/**
 * Created by nikola.s.pavlovic on 16.2.2018.
 * Radi zajedno sa {@link nsp.mpu._5_sigurnosne_visine.TabelaSigRastojanja_single_tab}
 */

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import io.michaelrocks.paranoid.Obfuscate;
import jxl.write.WritableSheet;
import jxl.write.WriteException;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_ListaTipovaUzadi;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.ViewPager_ispravanWrapHeight;

import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Activity.kreirajFajlZaSave;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_alertDialog;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_IN;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_OUT;
import static nsp.mpu.pomocne_klase.StaticPodaci.getSharPref;
import static nsp.mpu.pomocne_klase.StaticPodaci.mojHtmlString;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;

@Obfuscate
public class TabelaSigRastojanja extends Fragment {
    private PODACI_TipUzeta mTU;
    private double[] nizUgiba;
    private ArrayList listaOznakaStubova;
    private int brojStubova, brojRaspona;
    public int temperatura;

    // static var da bi mogli da im pristupa childFragment
    public static double duz_izolator;
    public static int pv;
    private static PagerAdapter pagerAdapter;
    private static ViewPager_ispravanWrapHeight viewPager;
    public static TabelaSigRastojanja novFragment
            (String tipUzeta,
             double[] nizRaspona,
             double[] nizUgiba,
             ArrayList listaOznakaStubova) {
        Bundle args = new Bundle();
        args.putString("tipUzeta", tipUzeta);
        args.putDoubleArray("nizRaspona", nizRaspona);
        args.putDoubleArray("nizUgiba", nizUgiba);
        args.putParcelableArrayList("listaOznakaStubova", listaOznakaStubova);

         if (BuildConfig.DEBUG)Log.e("TAG41", "listaOznakaStubova 0= " + listaOznakaStubova.toString());

        TabelaSigRastojanja newFrag = new TabelaSigRastojanja();
        newFrag.setArguments(args);
        return newFrag;
    }


    public StubnoMesto[] nizStubova;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setHasOptionsMenu(true);
        ((ProveraSigVisina_Activity) getActivity()).setActionBarTitle(getString(R.string.tabela_sigurnosnih_rastojanja));

        String s = getArguments().getString("tipUzeta");
        mTU = PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).getTipUzetaIzListe(s);
        temperatura = 40;
        double[] nizRaspona = getArguments().getDoubleArray("nizRaspona");
        nizUgiba = getArguments().getDoubleArray("nizUgiba");
        listaOznakaStubova = getArguments().getParcelableArrayList("listaOznakaStubova");
//         if (BuildConfig.DEBUG)Log.e("TAG41", "mTU = "+ mTU.getOznakaUzeta());
//         if (BuildConfig.DEBUG)Log.e("TAG41", "sigma0 = "+ sigma0);
//         if (BuildConfig.DEBUG)Log.e("TAG41", "koefDodOpt = "+ koefDodOpt);
//         if (BuildConfig.DEBUG)Log.e("TAG41", "nizRaspona = "+ Arrays.toString(nizRaspona));
//         if (BuildConfig.DEBUG)Log.e("TAG41", "nizTemenaLanc = "+ Arrays.toString(nizTemenaLanc));
//         if (BuildConfig.DEBUG)Log.e("TAG41", "listaOznakaStubova = "+ listaOznakaStubova.toString());

        brojStubova = listaOznakaStubova.size();
        brojRaspona = nizRaspona.length;

//         if (BuildConfig.DEBUG)Log.e("TAG10", "brojRaspona " + brojRaspona+
//                        "\n "+Arrays.toString(nizRaspona));
//         if (BuildConfig.DEBUG)Log.e("TAG10", "broj temena " + nizTemenaLanc.length+
//                "\n "+Arrays.toString(nizTemenaLanc));
//         if (BuildConfig.DEBUG)Log.e("TAG10", "lista oznaka " + listaOznakaStubova.size()+
//                "\n "+listaOznakaStubova.toString());

        if (brojRaspona != brojStubova - 1) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT(),
                            odaberiAnimaciju_fragment_IN(),
                            odaberiAnimaciju_fragment_OUT()
                    )
                    .remove(this).commit();
            prikaziToast(getContext(), "Greška, vrati se nazad!!!");
        } else {

            nizStubova = StubnoMesto.getNizStubnimMesta();
            duz_izolator = Double.parseDouble(getSharPref().getString("duz_izolator", "0"));
            pv = getSharPref().getInt("pv", 60);

        }

    }

    EditText Vetar;

    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b) {
        View v = li.inflate(R.layout.fragment_tabela_sig_razmaka, vg, false);
        if (!mDaLiJeTablet)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Vetar = (EditText) v.findViewById(R.id.napr_stuba_vred_vetar);
        Vetar.addTextChangedListener(generalTextWatcher);
        Vetar.setHint(String.format(Locale.getDefault(), "%d", pv));

        pagerAdapter = new
                PagerAdapter(getChildFragmentManager());
        viewPager =
                (ViewPager_ispravanWrapHeight) v.findViewById(R.id.tab_layout_viewpager);
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tab_layout_tabs);
        tabLayout.setupWithViewPager(viewPager);


        //TextView mTRezNaslov = (TextView) v.findViewById(R.id.tab_layout_naslov);
        //mTRezNaslov.setText("Tabela naprezanja stubova ");


        //posto prikazuje prazan frag sve dok se ne odradi prvi swipe
        //onda ovako sumuliramo swipe da bi odmah dobio popunjen layout
        viewPager.setCurrentItem(brojStubova - 1);
        viewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = brojStubova - 1; i >= 0; i--)
                    viewPager.setCurrentItem(i);
            }
        }, 100);


        return v;
    }

    private TextWatcher generalTextWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (Vetar.getText().hashCode() == s.hashCode()) {
                int pomocnaVar = 0;
                try {
                    pomocnaVar = Integer.parseInt(s.toString());
                } catch (NumberFormatException e) {}
                if (pomocnaVar < 50 || pomocnaVar > 150) {
                    pv = getSharPref().getInt("pv", 60);
                } else {
                    pv = pomocnaVar;
                }
                getSharPref().edit().putInt("pv", pv).apply();
                 if (BuildConfig.DEBUG)Log.e("TAG", "pv= " + pv);
            }
            resetujTabAdapter();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }

    };

    public static void resetujTabAdapter() {
        int n = viewPager.getCurrentItem();
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(n);
    }

    @Obfuscate//import
    private class PagerAdapter extends FragmentPagerAdapter {
        final int TAB_COUNT = brojStubova;

        PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public Fragment getItem(int index) {
            double nepostoji = 0;
            double ugibLevi, ugibDesni;
            if (index==0) { // prvi stub - gledamo samo jedan ugib
                ugibLevi = nepostoji;
                ugibDesni = nizUgiba[index];
            }else if (index==brojStubova-1) { // poslednji stub
                ugibLevi = nizUgiba[index - 1];
                ugibDesni = nepostoji;
            }else{ // stub u sredini - uzimamo veci ugib
                ugibLevi = nizUgiba[index-1];
                ugibDesni = nizUgiba[index];
            }
             if (BuildConfig.DEBUG)Log.e("TAGrastojanje", "ugibi ="+ Arrays.toString(nizUgiba)
                    +"\nindex="+index+
                    " ugibLevi="+ugibLevi+" ugibDesni="+ugibDesni);
            return TabelaSigRastojanja_single_tab
                    .novFragment(mTU.getOznakaUzeta(),
                            ugibLevi,ugibDesni,
                            index);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return listaOznakaStubova.get(position).toString();
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu m, MenuInflater i) {
        super.onCreateOptionsMenu(m, i);
        i.inflate(R.menu.meni_sig_rastojanja, m);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_min_rast_stub_find_export:
                exportUxls_dialog();
                return true;
            case R.id.menu_min_rast_stub_find_help:
                prikaziHelpInfo();
                return true;
            case R.id.menu_min_rast_stub_find_nedoz:
                if (nadjiNedozvRazmake())
                    prikaziToast(getContext(),"Nema razmaka manjih od dozvoljenih!");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    private void prikaziHelpInfo(){
        AlertDialog ad = new AlertDialog.Builder(getContext()).create();
        ad.getWindow().setWindowAnimations(odaberiAnimaciju_alertDialog());
        ad.setTitle(mojHtmlString("<b>Pojašnjenje:"));
        ad.setIcon(R.drawable.ic_notify_help);

        ad.setCancelable(true); //on back click
        ad.setCanceledOnTouchOutside(true); //on outside click

        String string_odrOdg =
                "<html><body><p align=\"justify\">" +
                        "Prikazana je provera sigurnosnih razmaka izme&#273u provodnika. " +
                        "Prora&#269un je u skladu sa <b>PTN za izgradnju nadzemnih EE vodova za Un=1-400kV " +
                        "(&#269l. 28 &#247 34)</b> ."+
                        "<br>Sigurnostni razmaci su ra&#269unati prema ugibu u sredini raspona, " +
                        "a rastojanja izme&#273u provodnika prema njihovom rasporedu na konzolama " +
                        "susednih stubova.";

        final WebView webView = new WebView(getContext());
        webView.loadData( string_odrOdg, "text/html", "utf-8");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            webView.setBackgroundColor(getResources().getColor(android.R.color.darker_gray,null));
        else
            webView.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));

        ad.setView(webView);

        ad.setButton(AlertDialog.BUTTON_POSITIVE, "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}
                });
        ad.show();

        ad.getButton(DialogInterface.BUTTON_POSITIVE).setAllCaps(false);
        ad.getWindow().setBackgroundDrawableResource(android.R.color.darker_gray);

        if ((ad.findViewById(android.R.id.message)) != null) {
            ((TextView) ad.findViewById(android.R.id.message)).setTextSize(R.dimen.text_size_velika);
        }

    }
    private boolean nadjiNedozvRazmake() {
        // false - ima nedozvoljenih razmaka, prikazuje ih jedan po jedan
        // true  - ako nema nedozvoljenih razmaka
        boolean bool_nemaNedozRaz = true;

        kreirajSveTabove();

        Handler handler = new Handler();

        int brojSekundi = 1;
        for (int i = 0; i < nizStubova.length; i++) {
            if ( ! nizStubova[i].bool_minRazmakJeZadovoljen ) {
                bool_nemaNedozRaz = false;

                final int finalI = i;
                handler.postDelayed(new Runnable() {
                    public void run() {
                        viewPager.setCurrentItem(finalI);
                    }
                }, (brojSekundi++) * 1000);
            }
        }
        return bool_nemaNedozRaz;
    }

    String fileName;
    private void exportUxls_dialog(){
        AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
        ad.getWindow().setWindowAnimations(odaberiAnimaciju_alertDialog());
        ad.setTitle(mojHtmlString("<i>Sacuvaj tabele"));
        ad.setMessage(mojHtmlString("Unesi naziv pod kojim ce tabele biti sacuvane:"));

        final EditText inputEditText = new EditText(getContext());
        ad.setView(inputEditText);
        inputEditText.setText("provera sig.razmaka");


        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        fileName = inputEditText.getText().toString();
                        final File fajlExcel = kreirajFajlZaSave(getContext(), fileName+ ".xls");
                        // if (BuildConfig.DEBUG)Log.e("TAG10", "fajlExcel " + fajlExcel.getAbsolutePath());

                        if (fajlExcel.exists()){
                            new AlertDialog.Builder(getActivity())
                                    .setIcon(R.mipmap.ic_alert_crveno)
                                    .setTitle(mojHtmlString("<b><i>Paznja!"))
                                    .setMessage(mojHtmlString("Vec postoji fajl <b>" +fileName+ ".xls"+
                                            "</b> Sacuvaj preko njega?"))
                                    .setPositiveButton("DA", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            kreirajTabele(fajlExcel);
                                        }
                                    })
                                    .setNegativeButton("NE", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            exportUxls_dialog();
                                        }
                                    })
                                    .show();
                        }
                        else{
                            kreirajTabele(fajlExcel);

                        }

                    }
                });
        ad.setButton(AlertDialog.BUTTON_NEGATIVE, "Odustani",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        ad.show();

    }

    private void kreirajTabele(File file){
        try {
            WriteExcel excelTabela = new WriteExcel(getContext(),file);
            WritableSheet sheet;
            int trenutniTab = viewPager.getCurrentItem();

            for (int i = 0; i < brojStubova ; i++) {

                /** kreiramo tabove pre nego sto uzmemo njegove varijable */
                viewPager.setCurrentItem(i);

                sheet = excelTabela.kreirajSheet_razmaci(listaOznakaStubova.get(i).toString());
                excelTabela.upisiPodatke_razmaci(sheet, nizStubova[i]);
            }

             if (BuildConfig.DEBUG)Log.e("TAGanim","getSheetNames="+ Arrays.toString(excelTabela.workbook.getSheetNames()));
             if (BuildConfig.DEBUG)Log.e("TAGanim","getNumberOfSheets="+excelTabela.workbook.getNumberOfSheets());

            viewPager.setCurrentItem(trenutniTab);
            excelTabela.workbook.write();
            excelTabela.workbook.close();
            prikaziToast(getContext(), "Excel tabela je kreirana");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    private void kreirajSveTabove(){
        /** kreiramo tabove pre nego sto uzmemo njegove varijable */
        int trenutniTab = viewPager.getCurrentItem();
        for (int i = 0; i < brojStubova ; i++)
            viewPager.setCurrentItem(i);
        viewPager.setCurrentItem(trenutniTab);
    }
}


    /*


    MenuItem findC, findE;
    private void resetujMeni(){
        if (findC == null || findE == null)
            return;

        if (materijalStub .equals("betonski")){
            findC.setVisible(false);
            findE.setVisible(false);
        }
        else{
            findC.setVisible(true);
            findE.setVisible(true);
        }
    }








    List<StubnoMesto.Tip_Stuba> listaPrimenjenihStubova = new ArrayList();
    private void nadjiKojiSuTipoviStubaPrimenjeni(){
        listaPrimenjenihStubova.clear();
        if (tipStubaPostojiUnizu(noseći))
            listaPrimenjenihStubova.add(noseći);
        if (tipStubaPostojiUnizu(zatezni))
            listaPrimenjenihStubova.add(zatezni);
        if (tipStubaPostojiUnizu(krajnji))
            listaPrimenjenihStubova.add(krajnji);
    }
    private boolean tipStubaPostojiUnizu(StubnoMesto.Tip_Stuba tipStuba){
        for (int i = 0; i < nizStubova.length; i++)
            if (nizStubova[i].tipStuba.equals(tipStuba))
                return true;

        return false;
    }

    private void nadjiFmaxA(StubnoMesto.Tip_Stuba tipStuba) {
        //int[] indexi = new int[nizStubova.length];
        double[] niz = new double[nizStubova.length];
        int indexMax=0;
        double maxValue=0;
        final TextView textView;

        if(materijalStub .equals("betonski")){
            for (int i = 0; i < niz.length; i++)
                if (nizStubova[i].tipStuba.equals(tipStuba) &&
                        maxValue < nizStubova[i].nizF1a[1]){
                    maxValue = nizStubova[i].nizF1a[1];
                    indexMax = i;
                }

            viewPager.setCurrentItem(indexMax);
            textView = ((TabelaNaprezanjaStuba_single_tab)
                    pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                    .A1h;
        }
        else{
            for (int i = 0; i < niz.length; i++)
                if (nizStubova[i].tipStuba.equals(tipStuba) &&
                        maxValue < nizStubova[i].nizFa[3]){
                    maxValue = nizStubova[i].nizFa[3];
                    indexMax = i;
                }

            viewPager.setCurrentItem(indexMax);
            textView = ((TabelaNaprezanjaStuba_single_tab)
                    pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                    .AFr;
        }

        textView.requestFocus();
        animirajTekst(textView);

    }
    private void nadjiFmaxB(StubnoMesto.Tip_Stuba tipStuba) {
        double[] niz = new double[nizStubova.length];
        int indexMax=0;
        double maxValue=0;
        final TextView textView;

        if(materijalStub .equals("betonski")){
            for (int i = 0; i < niz.length; i++)
                if (nizStubova[i].tipStuba.equals(tipStuba) &&
                        maxValue < nizStubova[i].nizF1b[1]){
                    maxValue = nizStubova[i].nizF1b[1];
                    indexMax = i;
                }

            viewPager.setCurrentItem(indexMax);
            textView = ((TabelaNaprezanjaStuba_single_tab)
                    pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                    .B1h;
        }
        else{
            for (int i = 0; i < niz.length; i++)
                if (nizStubova[i].tipStuba.equals(tipStuba) &&
                        maxValue < nizStubova[i].nizFb[3]){
                    maxValue = nizStubova[i].nizFb[3];
                    indexMax = i;
                }

            viewPager.setCurrentItem(indexMax);
            textView = ((TabelaNaprezanjaStuba_single_tab)
                    pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                    .BFr;
        }

        textView.requestFocus();
        animirajTekst(textView);
    }
    private void nadjiFmaxC(StubnoMesto.Tip_Stuba tipStuba) {
        double[] niz = new double[nizStubova.length];
        int indexMax=0;
        double maxValue=0;
        for (int i = 0; i < niz.length; i++)
            if (nizStubova[i].tipStuba.equals(tipStuba) &&
                    maxValue < nizStubova[i].nizFc[3]){
                maxValue = nizStubova[i].nizFc[3];
                indexMax = i;
            }

        viewPager.setCurrentItem(indexMax);
        final TextView textView = ((TabelaNaprezanjaStuba_single_tab)
                pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                .CFr;
        textView.requestFocus();
        animirajTekst(textView);
    }
    private void nadjiFmaxD(StubnoMesto.Tip_Stuba tipStuba) {
        double[] niz = new double[nizStubova.length];
        int indexMax=0;
        double maxValue=0;
        final TextView textView;

        if(materijalStub .equals("betonski")){
            for (int i = 0; i < niz.length; i++)
                if (nizStubova[i].tipStuba.equals(tipStuba) &&
                        maxValue < nizStubova[i].nizF2a[1]){
                    maxValue = nizStubova[i].nizF2a[1];
                    indexMax = i;
                }
            viewPager.setCurrentItem(indexMax);
            textView = ((TabelaNaprezanjaStuba_single_tab)
                    pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                    .A2h;
        }
        else{
            for (int i = 0; i < niz.length; i++)
                if (nizStubova[i].tipStuba.equals(tipStuba) &&
                        maxValue < nizStubova[i].nizFd[3]){
                    maxValue = nizStubova[i].nizFd[3];
                    indexMax = i;
                }
            viewPager.setCurrentItem(indexMax);
            textView = ((TabelaNaprezanjaStuba_single_tab)
                    pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                    .DFr;
        }

        textView.requestFocus();
        animirajTekst(textView);
    }
    private void nadjiFmaxE(StubnoMesto.Tip_Stuba tipStuba) {
        double[] niz = new double[nizStubova.length];
        int indexMax=0;
        double maxValue=0;
        for (int i = 0; i < niz.length; i++)
            if (nizStubova[i].tipStuba.equals(tipStuba) &&
                    maxValue < nizStubova[i].nizFe[3]){
                maxValue = nizStubova[i].nizFe[3];
                indexMax = i;
            }

        viewPager.setCurrentItem(indexMax);
        final TextView textView = ((TabelaNaprezanjaStuba_single_tab)
                pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem()))
                .EFr;
        textView.requestFocus();
        animirajTekst(textView);
    }
*/
