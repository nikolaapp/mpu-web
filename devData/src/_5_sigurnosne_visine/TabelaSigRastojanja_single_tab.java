package nsp.mpu._5_sigurnosne_visine;

/**
 * Created by nikola.s.pavlovic on 14.2.2018.
 * Proracun razmaka provodnika - bez zas.uzadi
 * Proracunava se razmak izmedju prov. u sredini raspona
 * tako sto se nadju ostojanja u glavi stuba i uzme se srednja vrednost
 * susednih stubova.
 * Te vrednosti se uporedjuju sa min.doz. razmakom koji se racuna preko ugiba
 * (posebno sa leve, posebno sa desne strane stuba)
 */

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_ListaTipovaUzadi;
import nsp.mpu.database.PODACI_TipIzolatora;
import nsp.mpu.database.PODACI_TipKonzole;
import nsp.mpu.database.PODACI_TipStuba;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.StaticPodaci;
import nsp.mpu.pomocne_klase.StaticPodaci.NaponskiNivo;

import static android.view.View.INVISIBLE;
import static java.lang.Math.cos;
import static java.lang.Math.max;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static java.lang.Math.toDegrees;
import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu._5_sigurnosne_visine.AlertDialog_TipOpreme.getDialog_konzola;
import static nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Fragment.naponskiNivo;
import static nsp.mpu._5_sigurnosne_visine.TabelaNaprezanjaStuba_single_tab.spinnerControl;
import static nsp.mpu._5_sigurnosne_visine.TabelaSigRastojanja.pv;
import static nsp.mpu.database.PODACI_ListaTipovaKonzola.getSingltonKonzola;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animirajTekst2;
import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;

@Obfuscate
public class TabelaSigRastojanja_single_tab extends Fragment {

    PODACI_TipUzeta mTU;
    String oznakaStuba;
    double ugibLevi, ugibDesni;
    double tezUze_bezLeda;  // tezina samo uzeta
    int ugao; // ugao skretanja trase
    double presek, precnik, specTezUze;
    int indexStuba;
    boolean bool_debug = false;

    public static TabelaSigRastojanja_single_tab novFragment
            (String tipUzeta, double ugibLevi, double ugibDesni ,int indexStubaUNizu){
        Bundle args = new Bundle();
        args.putInt("indexStubaUNizu",indexStubaUNizu);
        args.putString("tipUzeta", tipUzeta);
        args.putDouble("ugibLevi", ugibLevi);
        args.putDouble("ugibDesni", ugibDesni);

        TabelaSigRastojanja_single_tab newFrag = new TabelaSigRastojanja_single_tab();
        newFrag.setArguments(args);
        return newFrag;
    }

    StubnoMesto mStubnoMesto;
    PODACI_TipStuba mStub;
    PODACI_TipKonzole mKonzola;
    PODACI_TipIzolatora mIzolator;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        indexStuba=getArguments().getInt("indexStubaUNizu");
        mStubnoMesto = StubnoMesto.getNizStubnimMesta()[indexStuba];
        oznakaStuba=        mStubnoMesto.oznaka;

        mStub = mStubnoMesto.getStub();
        mKonzola = mStubnoMesto.getKonzola();
        mIzolator = mStubnoMesto.getIzolator();

        String s = getArguments().getString("tipUzeta");
        mTU= PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).getTipUzetaIzListe(s);
        presek = mTU.getPresek();
        precnik = mTU.getPrecnik();
        specTezUze = mTU.getSpecTezina();

        ugibLevi =        getArguments().getDouble("ugibLevi");
        ugibDesni =        getArguments().getDouble("ugibDesni");


        /** mnozi se sa presekom da bi dobili naprezanje u daN/m */
        tezUze_bezLeda = presek * specTezUze;

        ugao = mStubnoMesto.ugao;

        if (bool_debug){
            presek = 105;
            precnik = 13.4;
            tezUze_bezLeda = 0.368;
        }


    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle b){
        View v = li.inflate(R.layout.fragment_tabela_sig_razmaka_single_tab, vg, false);
        if ( ! mDaLiJeTablet)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        inicijalizujView(v);
        proracuni();
        return v;
    }

    private double optProv; // opterecenje stuba usled zatezanja uzeta sa jedne strane
    private double optVetar; // opterecenje stuba usled dejstva vetra na materijalUze
    double otklon, minRastojanjeLevo, minRastojanjeDesno;
    private void proracuni(){

        /** PTN cl.34 */
        double kvp_70posto = 0.7;
        // horizontalno opterecenje  [daN/m]
        optVetar = kvp_70posto * pv * (precnik*0.001);

        // vertikalno opterecenje  [daN/m]
        optProv = tezUze_bezLeda;
        
        otklon = toDegrees( Math.atan(optVetar/optProv) );

        if (otklon == (int) otklon) // znaci da nema decimale
            Otklon.setText(String.format("%d", otklon));
        else // ima decimale - zaokruzujemo na dve
            Otklon.setText(String.format("%.2f", otklon));

        /** PTN cl.29 */
        int sigRazmak=100; // [cm]
        if (     naponskiNivo. equals(NaponskiNivo._10kV))
            sigRazmak = 10;
        else if (naponskiNivo. equals(NaponskiNivo._20kV))
            sigRazmak = 20;
        else if (naponskiNivo. equals(NaponskiNivo._35kV))
            sigRazmak = 25;
        else if (naponskiNivo. equals(NaponskiNivo._110kV))
            sigRazmak = 80;

        double k;  // koeficijent prema PTN
        double Dmin; // min. sigurnosni razmak prema PTN
        if ( (mStubnoMesto.rasporedKonzola) .equals (StaticPodaci.Raspored_Konzola.koso)
            && ! naponskiNivo.equals(NaponskiNivo._10kV)
            && ! naponskiNivo.equals(NaponskiNivo._20kV)){
                if ((mKonzola.getD1()-mKonzola.getD3())<sigRazmak){
                    /** PTN cl.33 - vert.raspored */
                    k = max(4+otklon/5, 14);
                    Dmin = 140;
                }else{
                    /** PTN cl.32 - kosi raspored */
                    k = max(2+otklon/10, 7);
                    Dmin = 70;
                }
        }
        else{
            /** PTN cl.31 - horiz.raspored */
            k = max(4+otklon/25, 6);
            Dmin = 60;
        }

        // upisujemo nule da bi bilo preglednije pri debug
        if (ugibLevi==0)
            minRastojanjeLevo=0;
        else
            minRastojanjeLevo = k * sqrt( ugibLevi + TabelaSigRastojanja.duz_izolator)
                        + sigRazmak;

        if (ugibDesni==0)
            minRastojanjeDesno=0;
        else
            minRastojanjeDesno = k * sqrt( ugibDesni + TabelaSigRastojanja.duz_izolator)
                + sigRazmak;

        // ako je nula onda ostaje nula - da znamo da treba zanemariti
        if (minRastojanjeLevo < Dmin && minRastojanjeLevo!=0)
            minRastojanjeLevo = Dmin;
        if (minRastojanjeDesno < Dmin && minRastojanjeDesno!=0)
            minRastojanjeDesno = Dmin;

        SigRazmak_levi.setText(String.format("%.0f", minRastojanjeLevo));
        SigRazmak_desni.setText(String.format("%.0f", minRastojanjeDesno));


        double d12L, d23L, d13L;
        double d12D, d23D, d13D;
        if (ugibLevi==0) { // prvi stub - gledamo samo desni ugib
            d12L = d13L = d23L = nepostoji;
            // indexStuba == 0
            d12D = 0.5 * (proracunD12NaKonzoli(indexStuba)+proracunD12NaKonzoli(indexStuba+1) );
            d13D = 0.5 * (proracunD13NaKonzoli(indexStuba)+proracunD13NaKonzoli(indexStuba+1) );
            d23D = 0.5 * (proracunD23NaKonzoli(indexStuba)+proracunD23NaKonzoli(indexStuba+1) );
        }else if (ugibDesni==0) { // poslednji stub - gledamo levi ugib
            d12D = d13D = d23D = nepostoji;
            d12L = 0.5 * (proracunD12NaKonzoli(indexStuba-1)+proracunD12NaKonzoli(indexStuba) );
            d13L = 0.5 * (proracunD13NaKonzoli(indexStuba-1)+proracunD13NaKonzoli(indexStuba) );
            d23L = 0.5 * (proracunD23NaKonzoli(indexStuba-1)+proracunD23NaKonzoli(indexStuba) );
        }else{ // stub u sredini - uzimamo oba ugiba
            d12L = 0.5 * (proracunD12NaKonzoli(indexStuba-1)+proracunD12NaKonzoli(indexStuba) );
            d13L = 0.5 * (proracunD13NaKonzoli(indexStuba-1)+proracunD13NaKonzoli(indexStuba) );
            d23L = 0.5 * (proracunD23NaKonzoli(indexStuba-1)+proracunD23NaKonzoli(indexStuba) );

            d12D = 0.5 * (proracunD12NaKonzoli(indexStuba)+proracunD12NaKonzoli(indexStuba+1) );
            d13D = 0.5 * (proracunD13NaKonzoli(indexStuba)+proracunD13NaKonzoli(indexStuba+1) );
            d23D = 0.5 * (proracunD23NaKonzoli(indexStuba)+proracunD23NaKonzoli(indexStuba+1) );
        }

        mD12_levi .setText(String.format("%.0f", d12L));
        mD13_levi .setText(String.format("%.0f", d13L));
        mD23_levi .setText(String.format("%.0f", d23L));
        mD12_desni.setText(String.format("%.0f", d12D));
        mD13_desni.setText(String.format("%.0f", d13D));
        mD23_desni.setText(String.format("%.0f", d23D));

        boolean bool_razmaciSuOk = true ;
        // ako je bilo koji razmak nedozvoljen - krajnji bool je false
        bool_razmaciSuOk &= proveriRezultate(d12L,minRastojanjeLevo,mD12_levi);
        bool_razmaciSuOk &= proveriRezultate(d13L,minRastojanjeLevo,mD13_levi);
        bool_razmaciSuOk &= proveriRezultate(d23L,minRastojanjeLevo,mD23_levi);
        bool_razmaciSuOk &= proveriRezultate(d12D,minRastojanjeDesno,mD12_desni);
        bool_razmaciSuOk &= proveriRezultate(d13D,minRastojanjeDesno,mD13_desni);
        bool_razmaciSuOk &= proveriRezultate(d23D,minRastojanjeDesno,mD23_desni);

        posaljiRezultate(minRastojanjeLevo, minRastojanjeDesno,
                d12L,d13L,d23L,d12D,d13D,d23D,ugao,bool_razmaciSuOk);

         if (BuildConfig.DEBUG)Log.e("TAGrastojanje", "********************************"
                +"\noznakaStubnogMesta="+oznakaStuba+" tipStuba="+mStubnoMesto.getStub().getTip()
                +"\npresek="+presek
                +"\ntezUze_bezLeda="+ tezUze_bezLeda
                +"\nugao="+ugao
                +"\nugibLevi="+ugibLevi
                +"\nugibDesni="+ugibDesni
                +"\nsigRazmak="+sigRazmak
                +"\noptVetar="+optVetar
                +"\noptProv="+optProv
                +"\notklon="+otklon
                +"\nminRastojanjeLevo="+minRastojanjeLevo
                +"\nminRastojanjeDesno="+minRastojanjeDesno

        );
    }

    // proracunava se razmak provodnika na stubu prema dimenzijama
    // unetim za konzole
    private double proracunD12NaKonzoli(int indexStuba){
        StubnoMesto stubnoMesto = StubnoMesto.getNizStubnimMesta()[indexStuba];
        PODACI_TipKonzole konzola = stubnoMesto.getKonzola();
        double cos_na2 = pow( cos(ugao/2), 2 );
        return sqrt( pow(konzola.getH12(), 2) + cos_na2 * pow(konzola.getD1() + konzola.getD2(), 2));
    }
    private double proracunD23NaKonzoli(int indexStuba){
        StubnoMesto stubnoMesto = StubnoMesto.getNizStubnimMesta()[indexStuba];
        PODACI_TipKonzole konzola = stubnoMesto.getKonzola();
        double cos_na2 = pow( cos(ugao/2), 2 );
        return sqrt( pow(konzola.getH23(), 2) + cos_na2 * pow(konzola.getD2() + konzola.getD3(), 2));
    }
    private double proracunD13NaKonzoli(int indexStuba){
        StubnoMesto stubnoMesto = StubnoMesto.getNizStubnimMesta()[indexStuba];
        PODACI_TipKonzole konzola = stubnoMesto.getKonzola();
        double cos_na2 = pow( cos(ugao/2), 2 );
        return sqrt( pow(konzola.getH12() + konzola.getH23(), 2) + cos_na2 * pow(konzola.getD3() - konzola.getD1(), 2));
    }


    private boolean proveriRezultate(double d, double minD, TextView textView){
        if (d>=minD || d==nepostoji) {
            return true;
        }
        else{
            animirajTekst2(textView);
            return false;
        }
    }

    private void posaljiRezultate(double minRastojanjeLevo,double minRastojanjeDesno,
            double d12L,double d13L,double d23L,
            double d12D,double d13D,double d23D,
            int ugao, boolean bool){
        mStubnoMesto.minRastojanjeLevo = minRastojanjeLevo;
        mStubnoMesto.minRastojanjeDesno = minRastojanjeDesno;
        mStubnoMesto.d12L = d12L;
        mStubnoMesto.d13L = d13L;
        mStubnoMesto.d23L = d23L;
        mStubnoMesto.d12D = d12D;
        mStubnoMesto.d13D = d13D;
        mStubnoMesto.d23D = d23D;
        mStubnoMesto.ugao = ugao;
        mStubnoMesto.bool_minRazmakJeZadovoljen = bool;
    }


    TextView Otklon, SigRazmak_levi, SigRazmak_desni;
    EditText mUgao;
    TextView mD12_levi, mD13_levi, mD23_levi;
    TextView mD12_desni, mD13_desni, mD23_desni;
    Spinner mSpinnerOznakaKonzole;
    private void inicijalizujView(View v){

        LinearLayout LL_stub = (LinearLayout) v.findViewById( R.id.odabir_stuba_ll);
        LinearLayout LL_izolator = (LinearLayout) v.findViewById( R.id.odabir_izolatora_ll);
        LL_stub.setVisibility(View.GONE);
        LL_izolator.setVisibility(View.GONE);

        mSpinnerOznakaKonzole   = (Spinner)v.findViewById( R.id.odabir_konzole_spiner);
        spinnerControl(getContext(), mSpinnerOznakaKonzole,   getSingltonKonzola(getContext()).getNizTipovaKonzola(), mKonzola.getOznaka());
        mSpinnerOznakaKonzole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String s = adapterView.getSelectedItem().toString();
                mKonzola = getSingltonKonzola(getContext()).getTipKonzoleIzListe(s);
                mStubnoMesto.setKonzola(mKonzola);
                proracuni();
                if (BuildConfig.DEBUG){
                    prikaziToast(getContext(),mKonzola.toString());
                }
            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });
        mSpinnerOznakaKonzole.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog ad = getDialog_konzola(getActivity());
                ad.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        spinnerControl(getContext(), mSpinnerOznakaKonzole,     getSingltonKonzola(getContext()).getNizTipovaKonzola(StaticPodaci.MaterijalStuba.betonski), mKonzola.getOznaka());
                    }
                });
                return true;
            }
        });

        mUgao = (EditText) v.findViewById( R.id.napr_stuba_single_unos_ugao );
        mUgao.setText( ugao==0 ? "" : (ugao+"") );
        mUgao.addTextChangedListener(new TextWatcher() {
            @Override public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                int pomocnaVar = nepostoji;
                try {
                    pomocnaVar = Integer.parseInt(s.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<0||pomocnaVar>90)
                    ugao=0;
                else{
                    ugao = pomocnaVar;
                    proracuni();
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "ugao= " + ugao);
            }
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override public void afterTextChanged(Editable editable) {}
        });

        Otklon = (TextView)v.findViewById( R.id.napr_stuba_single_vred_otklon_prov );

        SigRazmak_levi = (TextView)v.findViewById( R.id.napr_stuba_single_vred_sig_razmak_levi );
        mD12_levi = (TextView)v.findViewById( R.id.napr_stuba_single_vred_rast12_levi);
        mD13_levi = (TextView)v.findViewById( R.id.napr_stuba_single_vred_rast13_levi);
        mD23_levi = (TextView)v.findViewById( R.id.napr_stuba_single_vred_rast23_levi);

        SigRazmak_desni = (TextView)v.findViewById( R.id.napr_stuba_single_vred_sig_razmak_desni);
        mD12_desni = (TextView)v.findViewById( R.id.napr_stuba_single_vred_rast12_desni);
        mD13_desni = (TextView)v.findViewById( R.id.napr_stuba_single_vred_rast13_desni);
        mD23_desni = (TextView)v.findViewById( R.id.napr_stuba_single_vred_rast23_desni);

        if (ugibLevi==0){
            SigRazmak_levi.setVisibility(INVISIBLE);
            mD12_levi.setVisibility(INVISIBLE);
            mD13_levi.setVisibility(INVISIBLE);
            mD23_levi.setVisibility(INVISIBLE);
        }else if (ugibDesni == 0){
            SigRazmak_desni.setVisibility(INVISIBLE);
            mD12_desni.setVisibility(INVISIBLE);
            mD13_desni.setVisibility(INVISIBLE);
            mD23_desni.setVisibility(INVISIBLE);
        }
    }

}

/*
        private Spanned ispisiDimenzijeStuba(){
            return mojHtmlString( "<u>"+mKonzola.getH12()
                    +"; "+mKonzola.getH23()
                    +"; "+mKonzola.getD1()
                    +"; "+mKonzola.getD2()
                    +"; "+mKonzola.getD3()
            );
        }

        EditText mDialogH12,mDialogH23,mDialogD1,mDialogD2,mDialogD3;
        ImageView mImageView;
        private void inicijalizujDialogView(View v){

            mDialogH12 .setHint(String.valueOf(mStubnoMesto. h12));
            mDialogH23.setHint(String.valueOf(mKonzola.getH23()));
            mDialogD1.setHint(String.valueOf(mKonzola.getD1()));
            mDialogD2 .setHint(String.valueOf(mKonzola.getD2()));
            mDialogD3 .setHint(String.valueOf(mKonzola.getD3()));
            if (mKonzola.getH23()==0)
                mImageView.setImageResource(R.drawable.slika_konzola_trougao);
            else if (mKonzola.getH12()==0)
                mImageView.setImageResource(R.drawable.slika_konzola_horiz);
            else
                mImageView.setImageResource(R.drawable.slika_konzola_koso);
    *//*

        final int maxDuzina = 1_000; // 10metara
        TextWatcher dialogTextWatcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,int count) {
                int pomocnaVar = nepostoji;
                if (mDialogH12.getText().hashCode() == s.hashCode()){
                    try {
                        pomocnaVar = Integer.parseInt(s.toString());
                    } catch (NumberFormatException e) {}

                    if (pomocnaVar>=0 && pomocnaVar<=maxDuzina) {
                        mKonzola.getH12() = pomocnaVar;
                        dialogTweak();
                    }
                     if (BuildConfig.DEBUG)Log.e("TAGdialogStub", "h12= " + mKonzola.getH12());
                }
                else if (mDialogH23.getText().hashCode() == s.hashCode()){
                    try {
                        pomocnaVar = Integer.parseInt(s.toString());
                    } catch (NumberFormatException e) {}

                    if (pomocnaVar>=0 && pomocnaVar<=maxDuzina) {
                        mKonzola.getH23() = pomocnaVar;
                        dialogTweak();
                    }
                     if (BuildConfig.DEBUG)Log.e("TAGdialogStub", "h23= " + mKonzola.getH23());
                }
                else if (mDialogD1.getText().hashCode() == s.hashCode()){
                    try {
                        pomocnaVar = Integer.parseInt(s.toString());
                    } catch (NumberFormatException e) {}

                    if (pomocnaVar>=0 && pomocnaVar<=maxDuzina)
                        mKonzola.getD1()=pomocnaVar;
                     if (BuildConfig.DEBUG)Log.e("TAGdialogStub", "d1= " + mKonzola.getD1());
                }
                else if (mDialogD2.getText().hashCode() == s.hashCode()){
                    try {
                        pomocnaVar = Integer.parseInt(s.toString());
                    } catch (NumberFormatException e) {}

                    if (pomocnaVar>=10 && pomocnaVar<=maxDuzina)
                        mKonzola.getD2()=pomocnaVar;
                     if (BuildConfig.DEBUG)Log.e("TAGdialogStub", "d2= " + mKonzola.getD2());
                }
                else if (mDialogD3.getText().hashCode() == s.hashCode()){
                    try {
                        pomocnaVar = Integer.parseInt(s.toString());
                    } catch (NumberFormatException e) {}

                    if (pomocnaVar>=10 && pomocnaVar<=maxDuzina)
                        mKonzola.getD3()=pomocnaVar;
                     if (BuildConfig.DEBUG)Log.e("TAGdialogStub", "d3= " + mKonzola.getD3());
                }

            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void afterTextChanged(Editable s) {}
        };
*//*
*//*

        mDialogH12 .addTextChangedListener(dialogTextWatcher);
        mDialogH23.addTextChangedListener(dialogTextWatcher);
        mDialogD1.addTextChangedListener(dialogTextWatcher);
        mDialogD2 .addTextChangedListener(dialogTextWatcher);
        mDialogD3 .addTextChangedListener(dialogTextWatcher);
*//*

        dialogTweak();

    }
*/
    /*private void dialogTweak(){
        if (mKonzola.getH12() ==0) {        // horizontalno
            mStubnoMesto.rasporedKonzola = StaticPodaci.Raspored_Konzola.horizontalno;
            mKonzola.getD1() = 0;
            mKonzola.getD3() = mKonzola.getD2();
            mKonzola.getH23() =0;
            mDialogH12.setVisibility(VISIBLE);
            mDialogH23.setVisibility(INVISIBLE);
            mDialogD1 .setVisibility(INVISIBLE);
            mDialogD2 .setVisibility(VISIBLE);
            mDialogD3 .setVisibility(INVISIBLE);
            mImageView.setImageResource(R.drawable.slika_konzola_horiz);
        }
        else if (mKonzola.getH23() == 0) {      // trougao
            mStubnoMesto.rasporedKonzola = StaticPodaci.Raspored_Konzola.trougao;
            mKonzola.getD1() = 0;
            mKonzola.getD3() = mKonzola.getD2();
            mDialogH12.setVisibility(VISIBLE);
            mDialogH23.setVisibility(VISIBLE);
            mDialogD1 .setVisibility(INVISIBLE);
            mDialogD2 .setVisibility(VISIBLE);
            mDialogD3. setVisibility(INVISIBLE);
            mImageView.setImageResource(R.drawable.slika_konzola_trougao);
        }
        else{       // koso
            mStubnoMesto.rasporedKonzola = StaticPodaci.Raspored_Konzola.koso;
            mDialogH12.setVisibility(VISIBLE);
            mDialogH23.setVisibility(VISIBLE);
            mDialogD1. setVisibility(VISIBLE);
            mDialogD2. setVisibility(VISIBLE);
            mDialogD3. setVisibility(VISIBLE);
            mImageView.setImageResource(R.drawable.slika_konzola_koso);
        }

    }
*/

/*

        mDimenzijeStuba = (TextView) v.findViewById( R.id.napr_stuba_single_dimenz );
        mDimenzijeStuba.setText(ispisiDimenzijeStuba());
        mDimenzijeStuba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
                ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
                ad.setTitle(mojHtmlString("<b>Unesi odstojanja:"));
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View v = inflater.inflate(R.layout.dialog_stub_odstojanja, null);
                ad.setView(v);
                inicijalizujDialogView(v);
                ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mDimenzijeStuba.setText(ispisiDimenzijeStuba());
                                dialogTweak(); // da bi upisao d2 u d3
                                resetujTabAdapter();
                                ad.dismiss();
                                 if (BuildConfig.DEBUG)Log.e("TAGstub", "Ln="+ mKonzola.getH12()
                                        +" h23="+mKonzola.getH23()
                                        +" d1="+mKonzola.getD1()
                                        +" d2="+mKonzola.getD2()
                                        +" d3="+mKonzola.getD3());
                            }
                        });
                ad.setButton(AlertDialog.BUTTON_NEUTRAL, "primeni na\nsve stubove",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialogTweak(); // da bi upisao d2 u d3
                                StubnoMesto[] niz_stub = StubnoMesto.getNizStubnimMesta();
                                for (int i = 0; i < niz_stub.length; i++) {
                                    niz_stub[i].h12 = mKonzola.getH12();
                                    niz_stub[i].h23 = mKonzola.getH23();
                                    niz_stub[i].d1 = mKonzola.getD1();
                                    niz_stub[i].d2 = mKonzola.getD2();
                                    niz_stub[i].d3 = mKonzola.getD3();
                                }
                                resetujTabAdapter();
                                ad.dismiss();
                                 if (BuildConfig.DEBUG)Log.e("TAGstub", "h12="+ mKonzola.getH12()
                                        +" h23="+mKonzola.getH23()
                                        +" d1="+mKonzola.getD1()
                                        +" d2="+mKonzola.getD2()
                                        +" d3="+mKonzola.getD3());
                            }
                        });
                ad.show();
                ad.getButton(DialogInterface.BUTTON_NEUTRAL).setAllCaps(false);
                ad.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.BLACK);
                //ad.getWindow().setBackgroundDrawableResource(android.R.color.darker_gray);
            }
        });
*/