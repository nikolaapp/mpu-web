package nsp.mpu._5_sigurnosne_visine;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.InputStream;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;

@Obfuscate
public class UcitajTrasuAsync extends AsyncTask<Void, String, Void> {

    private Context mContext;
    private float[] nizStacionaza_tla_X, nizKota_tla_Y;
    private InputStream inputStream;

    private ProgressDialog mProgressDialog;

    public UcitajTrasuAsync(Context context, Uri selectedfile_trasa_uri , float[] nizStacionaza_tla_X, float[] nizKota_tla_Y) {
        mContext = context;
        this.nizStacionaza_tla_X=nizStacionaza_tla_X;
        this.nizKota_tla_Y=nizKota_tla_Y;
        try {   // da bi ponovo mogao da citam iz inputStream
            inputStream = context.getContentResolver()
                    .openInputStream(selectedfile_trasa_uri);
        } catch (FileNotFoundException e) {
             if (BuildConfig.DEBUG)Log.e("TAG36", "FileNotFoundException");
        }
    }

    public UcitajTrasuAsync(Context context) {
        mContext = context;
    }


    @Override
    protected void onPreExecute() {
         if (BuildConfig.DEBUG)Log.e("TAG36", "onPreExecute " );
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("Ucitavanje tacaka...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... filePath) {
        if (mProgressDialog.isShowing() )
             if (BuildConfig.DEBUG)Log.e("TAG36", "doInBackground - mProgressDialog.isShowing() " );
        else
             if (BuildConfig.DEBUG)Log.e("TAG36", "doInBackground - NO mProgressDialog.isShowing() " );

        int i = 0;
        while (true){
            i++;
            i--;
        }
        //return null;
    }


    @Override
    protected void onPostExecute(Void filename) {
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        mProgressDialog = null;

         if (BuildConfig.DEBUG)Log.e("TAG36", "onPostExecute " );
    }
}
