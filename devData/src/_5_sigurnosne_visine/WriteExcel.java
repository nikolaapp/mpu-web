package nsp.mpu._5_sigurnosne_visine;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import io.michaelrocks.paranoid.Obfuscate;
import jxl.CellView;
import jxl.SheetSettings;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu._5_sigurnosne_visine.TabelaNaprezanjaStuba.temperatura;
import static nsp.mpu._5_sigurnosne_visine.WriteExcel.Kolona.A;
import static nsp.mpu._5_sigurnosne_visine.WriteExcel.Kolona.B;
import static nsp.mpu._5_sigurnosne_visine.WriteExcel.Kolona.C;
import static nsp.mpu._5_sigurnosne_visine.WriteExcel.Kolona.D;
import static nsp.mpu._5_sigurnosne_visine.WriteExcel.Kolona.E;
import static nsp.mpu._5_sigurnosne_visine.WriteExcel.Kolona.F;

/**
 * Created by Nikola on 18.2.2018.
 */

@Obfuscate
public class WriteExcel {
    private WritableCellFormat format_bold;
    private WritableCellFormat format_osnovni;
    private WritableCellFormat format_osnovni_border;
    private WritableCellFormat format_osnovni_border_desno;
    private WritableCellFormat format_bold_border;
    public WritableWorkbook workbook;
    private int brojSheeta=0;
    Context context;
    private boolean bool_betonski;

    public WriteExcel(Context context, File file) throws IOException {

        this.context=context;

        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("en", "EN"));

        workbook = Workbook.createWorkbook(file, wbSettings);

         if (BuildConfig.DEBUG)Log.e("TAGexcel","kreiran je excel fajl: "+file.getPath());

        }

    public WritableSheet kreirajSheet(String nazivSheeta, boolean bool_noseci) throws WriteException {
        bool_betonski = TabelaNaprezanjaStuba.materijalStub.equals(StaticPodaci.MaterijalStuba.betonski.toString());

        WritableSheet sheet = workbook.createSheet(nazivSheeta, brojSheeta++);// 0-red.broj sheet-a
        formatiranje(sheet);
        napraviMaskuSheeta(sheet, bool_noseci);

         if (BuildConfig.DEBUG)Log.e("TAGexcel","kreiran je excel sheet: "+nazivSheeta);

        return sheet;
    }
    public WritableSheet kreirajSheet_razmaci(String nazivSheeta) throws WriteException {
        WritableSheet sheet = workbook.createSheet(nazivSheeta, brojSheeta++);// 0-red.broj sheet-a
        formatiranje(sheet);
        napraviMaskuSheeta_razmaci(sheet);

         if (BuildConfig.DEBUG)Log.e("TAGexcel","kreiran je excel sheet: "+nazivSheeta);

        return sheet;
    }

    private void napraviMaskuSheeta(WritableSheet sheet, boolean bool_noseci) throws WriteException {
        int brojac_redova=0;
        upisiTekst(sheet, A,++brojac_redova, "oznaka stuba:");
        //upisiTekst(sheet, A,++brojac_redova, "proračun za:");
        upisiTekst(sheet, A,++brojac_redova, R.string.tip_stuba);
        upisiTekst(sheet, A,++brojac_redova, R.string.pritisak_vetra_dan_m);
        upisiTekst(sheet, A,++brojac_redova, R.string.tezina_izolatora_dan);
        upisiTekst(sheet, A,++brojac_redova, R.string.skretanje_trase);
        upisiTekst(sheet, A,++brojac_redova, R.string.srednji_raspon_m);
        upisiTekst(sheet, A,++brojac_redova, "gravitac. raspon pri "+temperatura+"°C [m]:");
        upisiTekst(sheet, A,++brojac_redova, "gravitac. raspon pri -5°C [m]:");

        ++brojac_redova;
        upisiTekst(sheet, A,++brojac_redova, R.string.normalno_opterecenje_dan, format_bold);
        if (bool_betonski){
            upisiTekst(sheet, C, brojac_redova, R.string.vertikalno, format_bold_border);
            upisiTekst(sheet, D, brojac_redova, R.string.horizontalno, format_bold_border);

        }
        else {
            upisiTekst(sheet, C, brojac_redova, R.string.fx, format_bold_border);
            upisiTekst(sheet, D, brojac_redova, R.string.fy, format_bold_border);
            upisiTekst(sheet, E, brojac_redova, R.string.fz, format_bold_border);
            upisiTekst(sheet, F, brojac_redova, R.string.frez, format_bold_border);
        }

        if (bool_betonski){
            upisiTekst(sheet, B, ++brojac_redova, R.string.a, format_bold_border);
            upisiTekst(sheet, B, ++brojac_redova, R.string.b, format_bold_border);
            if (!bool_noseci)
                upisiTekst(sheet, B, ++brojac_redova, R.string.d, format_bold_border);
        }
        else {
            upisiTekst(sheet, B, ++brojac_redova, R.string.a, format_bold_border);
            upisiTekst(sheet, B, ++brojac_redova, R.string.b, format_bold_border);
            upisiTekst(sheet, B, ++brojac_redova, R.string.c, format_bold_border);
            if (!bool_noseci)
                upisiTekst(sheet, B, ++brojac_redova, R.string.d, format_bold_border);
        }

        if ( ! bool_betonski) {
            ++brojac_redova;
            upisiTekst(sheet, A, ++brojac_redova, R.string.vanredno_opterecenje_dan, format_bold);
            upisiTekst(sheet, C, brojac_redova, R.string.fx, format_bold_border);
            upisiTekst(sheet, D, brojac_redova, R.string.fy, format_bold_border);
            upisiTekst(sheet, E, brojac_redova, R.string.fz, format_bold_border);
            upisiTekst(sheet, F, brojac_redova, R.string.frez, format_bold_border);

            upisiTekst(sheet, B, ++brojac_redova, R.string.e, format_bold_border);
        }

    }
    private void napraviMaskuSheeta_razmaci(WritableSheet sheet) throws WriteException {
        int brojac_redova=0;
        upisiTekst(sheet, A,++brojac_redova, "oznaka stuba:");
        //upisiTekst(sheet, A,++brojac_redova, "proračun za:");
        upisiTekst(sheet, A,++brojac_redova, R.string.tip_stuba);
        upisiTekst(sheet, A,++brojac_redova, R.string.pritisak_vetra_dan_m);
        upisiTekst(sheet, A,++brojac_redova, R.string.duzina_izolatora_cm);
        upisiTekst(sheet, A,++brojac_redova, R.string.skretanje_trase);

        ++brojac_redova;
        upisiTekst(sheet, A,++brojac_redova, R.string.raspon, format_osnovni_border_desno);
        upisiTekst(sheet, B, brojac_redova, R.string.levi, format_osnovni_border);
        upisiTekst(sheet, C, brojac_redova, R.string.desni, format_osnovni_border);

        upisiTekst(sheet, A, ++brojac_redova, R.string.min_sigurnosni_razmak_m, format_bold_border);
        upisiTekst(sheet, A, ++brojac_redova, R.string.rastojanje_prov_1_2_cm, format_osnovni_border);
        upisiTekst(sheet, A, ++brojac_redova, R.string.rastojanje_prov_1_3_cm, format_osnovni_border);
        upisiTekst(sheet, A, ++brojac_redova, R.string.rastojanje_prov_2_3_cm, format_osnovni_border);

    }

    public void upisiPodatke(WritableSheet sheet, StubnoMesto stubnoMesto)
            throws WriteException, IOException {
        int brojac=0;
        upisiTekst(sheet, B,++brojac,stubnoMesto.oznaka, format_bold);
        //upisiTekst(sheet, B,++brojac,TabelaNaprezanjaStuba.materijalUze);
        upisiTekst(sheet, B,++brojac,stubnoMesto.getStub().getTip().toString());

        upisiBroj (sheet, B,++brojac,TabelaNaprezanjaStuba.pv);
        upisiBroj (sheet, B,++brojac,stubnoMesto.getIzolator().getTezina());

        upisiBroj (sheet, B,++brojac,stubnoMesto.ugao);
        upisiBroj (sheet, B,++brojac,stubnoMesto.srednjiRaspon);
        upisiBroj (sheet, B,++brojac,stubnoMesto.gravitacRaspon);
        upisiBroj (sheet, B,++brojac,stubnoMesto.gravitacRaspon_minus5);

        int brojRacunatihSila = bool_betonski?2:4;
        brojac=11;
        for (int i = 0; i < brojRacunatihSila; i++)
                upisiBroj (sheet, C.ordinal()+i,brojac,bool_betonski?stubnoMesto.nizF1a[i]:stubnoMesto.nizFa[i],format_osnovni_border);
        brojac++;
        for (int i = 0; i < brojRacunatihSila; i++)
                upisiBroj (sheet, C.ordinal()+i,brojac,bool_betonski?stubnoMesto.nizF1b[i]:stubnoMesto.nizFb[i],format_osnovni_border);
        if ( ! bool_betonski) {
            brojac++;
            for (int i = 0; i < brojRacunatihSila; i++)
                upisiBroj(sheet, C.ordinal() + i, brojac, stubnoMesto.nizFc[i], format_osnovni_border);
        }
        if ( ! stubnoMesto.getStub().getTip().equals(StaticPodaci.Tip_Stuba.noseći)) {
            brojac++;
            for (int i = 0; i < brojRacunatihSila; i++)
                    upisiBroj(sheet, C.ordinal() + i, brojac,bool_betonski?stubnoMesto.nizF2a[i]:stubnoMesto.nizFd[i],format_osnovni_border);
        }

        if ( ! bool_betonski) {
            brojac = brojac + 3; // tri reda ispod
            for (int i = 0; i < brojRacunatihSila; i++)
                //if (stubnoMesto.nizFe[i] !=0)
                upisiBroj(sheet, C.ordinal() + i, brojac, stubnoMesto.nizFe[i], format_osnovni_border);
        }

         if (BuildConfig.DEBUG)Log.e("TAGexcel","stubnoMesto: "+stubnoMesto.toString());


    }
    public void upisiPodatke_razmaci(WritableSheet sheet, StubnoMesto stubnoMesto)
            throws WriteException, IOException {
        int brojac=0;
        upisiTekst(sheet, B,++brojac,stubnoMesto.oznaka, format_bold);
        upisiTekst(sheet, B,++brojac,stubnoMesto.getStub().getTip().toString());

        upisiBroj (sheet, B,++brojac, TabelaSigRastojanja.pv);
        upisiBroj (sheet, B,++brojac, TabelaSigRastojanja.duz_izolator);

        upisiBroj (sheet, B,++brojac,stubnoMesto.ugao);

        int brojProveraRazmaka = 2; // levo i desno
        brojac+=3; // jedan red prazan, jedan za tekst, pa onda podaci
        double pom;

        for (int i = 0; i < brojProveraRazmaka; i++) {
            pom = (i == 0) ? stubnoMesto.minRastojanjeLevo : stubnoMesto.minRastojanjeDesno;
            pom = Math.round(pom);
            upisiBroj(sheet, B.ordinal() + i, brojac, pom, format_bold_border);
        }
        brojac++;

        for (int i = 0; i < brojProveraRazmaka; i++) {
            pom = (i == 0) ? stubnoMesto.d12L : stubnoMesto.d12D;
            pom = Math.round(pom);
            upisiBroj(sheet, B.ordinal() + i, brojac, pom, format_osnovni_border);
        }
        brojac++;

        for (int i = 0; i < brojProveraRazmaka; i++) {
            pom = (i == 0) ? stubnoMesto.d13L : stubnoMesto.d13D;
            pom = Math.round(pom);
            upisiBroj(sheet, B.ordinal() + i, brojac, pom, format_osnovni_border);
        }
        brojac++;

        for (int i = 0; i < brojProveraRazmaka; i++) {
            pom = (i == 0) ? stubnoMesto.d23L : stubnoMesto.d23D;
            pom = Math.round(pom);
            upisiBroj(sheet, B.ordinal() + i, brojac, pom, format_osnovni_border);
        }

         if (BuildConfig.DEBUG)Log.e("TAGexcel","stubnoMesto: "+stubnoMesto.toString());


    }


    enum Kolona {A,B,C,D,E,F}

    private void formatiranje(WritableSheet sheet) throws WriteException {

        SheetSettings sheetMargine = sheet.getSettings();
        sheetMargine.setLeftMargin(0.5); // 0.5 incha
        sheetMargine.setRightMargin(0.5);

        // create a font
        WritableFont font_arial16 = new WritableFont(WritableFont.ARIAL, 16);
        // Define the cell format
        format_osnovni = new WritableCellFormat(font_arial16);
        // Lets automatically wrap the cells
        //format_osnovni.setWrap(true);

        WritableFont font_arial16_b = new WritableFont(
                WritableFont.ARIAL, 16, WritableFont.BOLD, false,
                UnderlineStyle.NO_UNDERLINE);
        format_bold = new WritableCellFormat(font_arial16_b);
        format_bold.setAlignment(Alignment.CENTRE);
        //format_bold.setWrap(true);

        CellView cv = new CellView();
        //cv.setFormat(format_osnovni);
        cv.setAutosize(true);
        sheet.setColumnView(A.ordinal(),cv);
        sheet.setColumnView(B.ordinal(),cv);
        sheet.setColumnView(C.ordinal(),cv);
        sheet.setColumnView(D.ordinal(),cv);

        format_osnovni_border = new WritableCellFormat(format_osnovni);
        format_osnovni_border.setAlignment(Alignment.CENTRE);
        format_osnovni_border.setBorder(Border.ALL, BorderLineStyle.THIN);

        format_osnovni_border_desno = new WritableCellFormat(format_osnovni_border);
        format_osnovni_border_desno.setAlignment(Alignment.RIGHT);

        format_bold_border = new WritableCellFormat(format_bold);
        format_bold_border.setBorder(Border.ALL, BorderLineStyle.THIN);

    }

    private void upisiBroj(WritableSheet sheet,Kolona kolona,int row, double broj)
            throws WriteException {
        if (broj%1 != 0) // ako nije ceo broj onda zaokruzujemo na dve decimale
            broj = Math.round(100*broj) / 100.0;
        Number number;
        /** row-1  ----- da bi redovi pocinjali od 1(kao u excelu)
         *  a ne od 0 (sto trazi java)*/
        number = new Number(kolona.ordinal(), row-1, broj, format_osnovni);
        sheet.addCell(number);
    }
    private void upisiBroj(WritableSheet sheet,int kolona,int row, double broj, WritableCellFormat format)
            throws WriteException {
        if (broj%1 != 0) // ako nije ceo broj onda zaokruzujemo na dve decimale
            broj = Math.round(100*broj) / 100.0;
        Number number;
        number = new Number(kolona, row-1, broj, format);
        sheet.addCell(number);
    }
    private void upisiTekst(WritableSheet sheet, Kolona kolona, int row, String tekst , WritableCellFormat format_teksta)
            throws WriteException {
        Label label;
        label = new Label(kolona.ordinal(), row-1, tekst, format_teksta);
        sheet.addCell(label);
    }
    private void upisiTekst(WritableSheet sheet, Kolona kolona, int row,  int  resId , WritableCellFormat format_teksta)
            throws WriteException {
        upisiTekst(sheet,kolona, row,  context.getString(resId) , format_teksta);
    }
    private void upisiTekst(WritableSheet sheet,Kolona kolona, int row,  String tekst) throws WriteException {
        upisiTekst(sheet,kolona, row,  tekst , format_osnovni);
    }
    private void upisiTekst(WritableSheet sheet,Kolona kolona, int row,  int  resId) throws WriteException {
        upisiTekst(sheet,kolona, row,  context.getString(resId) , format_osnovni);
    }

}
/*
        // Lets calculate the sum of it
        StringBuffer buf = new StringBuffer();
        buf.append("SUM(A2:A10)");
        Formula f = new Formula(0, 10, buf.toString());
        prviSheet.addCell(f);
        buf = new StringBuffer();
        buf.append("SUM(B2:B10)");
        f = new Formula(1, 10, buf.toString());
        prviSheet.addCell(f);
*/
