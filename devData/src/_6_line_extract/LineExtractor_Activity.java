package nsp.mpu._6_line_extract;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.opengl.GLES10;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu._5_sigurnosne_visine.ProveraSigVisina_Fragment;
import nsp.mpu.pomocne_klase.AnimacijeKlasa;

import static android.content.DialogInterface.BUTTON_NEGATIVE;
import static android.content.DialogInterface.BUTTON_POSITIVE;
import static nsp.mpu.pomocne_klase.StaticPodaci.folderUserData;
import static nsp.mpu.pomocne_klase.StaticPodaci.kreirajFolder;
import static nsp.mpu.pomocne_klase.StaticPodaci.mojHtmlString;
import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;
import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;

@Obfuscate
public class LineExtractor_Activity extends AppCompatActivity {
    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        // treba getSupportFragmentManager a ne getFragmentManager!!!
        if (doubleBackToExitPressedOnce ||
                (getSupportFragmentManager().getBackStackEntryCount() != 0)) {
            super.onBackPressed();
//            problem je sto tranzicija landscape/portret
//            AnimacijeKlasa.ParAnimacijaInOut parAnim = new AnimacijeKlasa.ParAnimacijaInOut();
//            overridePendingTransition(parAnim.in, parAnim.out);
            return;
        }

        doubleBackToExitPressedOnce = true;

        // prikazuje toast i pri izlasku iz activity...toast.cancel() ne radi
        prikaziToast(this, "Pritisni dvaput uzastopno za exit");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 500);
    }

    static {
        // zezanje
        // Static block is used for initializing the static variables.
        // This block gets executed when the class is loaded in the memory.
        // The code block with the static modifier signifies a class initializer;
        // without the static modifier the code block is an instance initializer.
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);
    }
    BitmapFactory.Options options = new BitmapFactory.Options();
    static Bitmap  mBitmap;

    Uri selectedFile_uri;
    static TouchImageView mImageView;
    static String picturePath;
    private Context context;

    boolean slikaJeUcitana = false;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode==555 && resultCode==RESULT_OK) {
            selectedFile_uri = intent.getData();
            try {

                String fileType = getFileType_fromUri(selectedFile_uri, context);

                if (! fileType.equals("png"))
                    Toast.makeText (this,"Rad sa slikama koje nisu u .PNG formatu nije preporučljiv",Toast.LENGTH_LONG).show();

                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = getContentResolver().query(selectedFile_uri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();

                /*Drawable draw = new BitmapDrawable(getResources(),BitmapFactory.decodeFile(picturePath));
                draw = resize(draw);*/


                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(picturePath, options);

                options.inSampleSize = calculateInSampleSize(options, maxImageSize(),maxImageSize());
                options.inJustDecodeBounds = false;

                mBitmap = BitmapFactory.decodeFile(picturePath, options);
                mImageView.setImageBitmap(mBitmap);


                //Toast.makeText(this, "Slika je ocitana", Toast.LENGTH_SHORT).show();
                //Toast.makeText(this, "mBitmap.getWidth() "+mBitmap.getWidth()+" mBitmap.getWidth() "+mBitmap.getHeight(), Toast.LENGTH_SHORT).show();
                //Toast.makeText(this, "maxSize[0] "+maxImageSize(), Toast.LENGTH_SHORT).show();

                lista.clear();
                bool_dodaj=bool_obrisi=false;
                mImageView.dragEnabled = true;
                mImageView.resetZoom();

                slikaJeUcitana=true;

            } catch (Exception e) {
                e.printStackTrace();
                AlertDialog ad = new AlertDialog.Builder(this).create();
                ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
                ad.setTitle("P A Z NJ A");
                ad.setMessage("Fajl " +  " nije moguce otvoriti");
                ad.show();
            }
        }

        if (requestCode == 333 && resultCode == RESULT_OK) {
            float defValue = -1f;
            x_start=intent.getFloatExtra("x_start",defValue);
            x_kraj=intent.getFloatExtra("x_kraj",defValue);
            y_min=intent.getFloatExtra("y_min",defValue);
            y_max=intent.getFloatExtra("y_max",defValue);
            x1=intent.getFloatExtra("x1",defValue);
            y1=intent.getFloatExtra("y1",defValue);
            x2=intent.getFloatExtra("x2",defValue);
            y2=intent.getFloatExtra("y2",defValue);

             if (BuildConfig.DEBUG)Log.e("TAG40", "x_start="+x_start+
                    " x_kraj="+x_kraj+
                    " y_min="+y_min+
                    " y_max="+y_max+
                    " x1="+x1+
                    " y1="+y1+
                    " x2="+x2+
                    " y2="+y2
            );


            sacuvajTXT_dialog();
        }
    }

    private float x_start, x_kraj,y_min, y_max,x1,y1, x2, y2;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("picturePath",picturePath);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        super.onCreateOptionsMenu(m);
        MenuInflater findMenuItems = getMenuInflater();
        findMenuItems.inflate(R.menu.meni_line_extractor, m);
        MenuItem save = m.findItem(R.id.meni_toolbar_save);
        MenuItem omoguciUklanjanje = m.findItem(R.id.meni_toolbar_ukloni);
        MenuItem omoguciDodavanje = m.findItem(R.id.meni_toolbar_dodaj);
        MenuItem dodajT = m.findItem(R.id.meni_toolbar_dodaj_tacku);
        MenuItem ukloniT = m.findItem(R.id.meni_toolbar_ukloni_tacke);
        if (bool_obrisi || bool_dodaj) {
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            this.setTitle("povratak");
            toolbar_layout_0.setVisibility(View.GONE);
            save.   setVisible(false);
            omoguciUklanjanje. setVisible(false);
            omoguciDodavanje.  setVisible(false);
            if(bool_dodaj) dodajT.setVisible(true);
                else dodajT.setVisible(false);
            if(bool_obrisi) ukloniT.setVisible(true);
                else ukloniT.setVisible(false);
        }else{
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            toolbar_layout_0.setVisibility(View.VISIBLE);
            save.   setVisible  (true);
            omoguciUklanjanje. setVisible(true);
            omoguciDodavanje.  setVisible (true);
            dodajT. setVisible (false);
            ukloniT.setVisible(false);
        }
        return true;
    }

    static Bitmap pomBitmap;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.meni_toolbar_save:
                if (lista.size()>0) {
                    final Intent intent = new Intent(this, LineExtractor_Activity_parametri.class);
                    startActivityForResult(intent, 333);
                }
                return true;
            case R.id.meni_toolbar_ukloni:
                if (lista.size()>0) {
                    if (bool_obrisi == false) {
                        mImageView.dragEnabled = false;
                        bool_obrisi = true;
                    }
                    else {
                        pomBitmap = nacrtajIzracunateTacke(mBitmap, lista, NacinCrtanja.tackasto);
                        mImageView.setImageBitmap(pomBitmap);
                        mImageView.dragEnabled = true;
                        bool_obrisi = false;
                    }
                    invalidateOptionsMenu();
                }
                return true;
            case R.id.meni_toolbar_ukloni_tacke:
                if (lista.size()>0) {
                        izbrisiTacku();
                }
                return true;
            case R.id.meni_toolbar_dodaj:
                if (lista.size()>0) {
                    if (bool_dodaj == false) {
                        mImageView.dragEnabled = false;
                        bool_dodaj = true;
                    }
                    else {
                        pomBitmap = nacrtajIzracunateTacke(mBitmap, lista, NacinCrtanja.tackasto);
                        mImageView.setImageBitmap(pomBitmap);
                        mImageView.dragEnabled = true;
                        bool_dodaj = false;
                    }
                    invalidateOptionsMenu();
                }
                return true;
            case R.id.meni_toolbar_dodaj_tacku:
                if (lista.size()>0) {
                    dodajTacku();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    Toolbar toolbar;
    LinearLayout toolbar_layout_0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.line_extractor);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        context = this;

        Toast.makeText (this,"Ova funkcionalnost je još u eksperimentalnoj fazi!",Toast.LENGTH_LONG).show();


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bool_dodaj=bool_obrisi=false;
                mImageView.dragEnabled = true;
                invalidateOptionsMenu();
                mImageView.setImageBitmap( nacrtajIzracunateTacke(mBitmap, lista, NacinCrtanja.tackasto) );
            }
        });

        toolbar_layout_0 = (LinearLayout) findViewById(R.id.toolbar_layout_0);

        Button open = (Button) findViewById(R.id.toolbar_open);
        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent()
                        .setType("image/*")
                        .setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Odaberi sliku u .png formatu"), 555);
            }
        });

        Button oznaci = (Button) findViewById(R.id.toolbar_oznaci);
        oznaci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (slikaJeUcitana) {
                    //mImageView.enableOsetljjivostNaDodir();
                    mBitmap = BitmapFactory.decodeFile(picturePath, options);
                    mImageView.setImageBitmap(mBitmap);
                    start = true;
                    mImageView.dragEnabled = false;

                    (new Handler()).postDelayed(new Runnable() {
                        public void run() {
                            lista = izracunajTacke(mBitmap, pocetakGL, krajDD);
                            mImageView.setImageBitmap(nacrtajIzracunateTacke(mBitmap, lista, NacinCrtanja.tackasto));
                        }
                    }, 2000);//fixme nespretan algoritam
                }
            }
        });
        oznaci.setOnLongClickListener(new View.OnLongClickListener() {
            int pomocnaVar = maxSuma;
            @Override
            public boolean onLongClick(View v) {
                AlertDialog ad = new AlertDialog.Builder(LineExtractor_Activity.this).create();
                ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
                ad.setMessage("Unesi osetljivost pretrage tacaka od 1 do 10:");
                EditText edittext = new EditText(ad.getContext());
                edittext.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
                edittext.setHint(String.valueOf(maxSuma));
                edittext.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after){}
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count){
                        try {
                            pomocnaVar = Integer.parseInt(s.toString());
                            if (pomocnaVar<1||pomocnaVar>10) {
                                Toast.makeText(context, "Unesi ceo broj od 1 do 10", Toast.LENGTH_SHORT).show();
                                pomocnaVar = maxSuma;
                            }
                        } catch (NumberFormatException e) {}
                    }
                    @Override
                    public void afterTextChanged(Editable s){}
                });
                ad.setView(edittext);
                ad.setButton(BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        maxSuma = pomocnaVar;
                    }
                });
                ad.setButton(BUTTON_NEGATIVE, "cancel", new DialogInterface.OnClickListener() {
                    @Override public void onClick(DialogInterface dialog, int which) {} });
                ad.show();
                return true; /** ako vraca false onda bi aktivirao i obican click (true - zavrseno sa svim listener.ima)*/
            }
        });

        final ToggleButton LinijeIliTacke = (ToggleButton) findViewById(R.id.meni_toolbar_lin_v_tac);
        LinijeIliTacke.setChecked(false);
        LinijeIliTacke.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (lista.size()>0) {
                    if (isChecked) {
                        mImageView.setImageBitmap(nacrtajIzracunateTacke(mBitmap, lista, NacinCrtanja.linije));
                        LinijeIliTacke.setChecked(true);
                    }
                    else {
                        mImageView.setImageBitmap(nacrtajIzracunateTacke(mBitmap, lista, NacinCrtanja.tackasto));
                        LinijeIliTacke.setChecked(false);
                    }
                }
            }
        });


        options.inMutable=true;
        options.inScaled=false;
        options.inJustDecodeBounds = false;

        mImageView = (TouchImageView) findViewById(R.id.imageView);
        mImageView.setMaxZoom(20);

        if (savedInstanceState!=null) {
            picturePath = savedInstanceState.getString("picturePath");
            try {
                mBitmap = BitmapFactory.decodeFile(picturePath, options);
                mImageView.setImageBitmap(mBitmap);
            } catch (Exception e) {
                Toast.makeText(this, " no bitmap ", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

    }

    private enum NacinCrtanja {tackasto, linije}

    static List<PointF> lista = new ArrayList<>(); // nikad nije null
    static PointF pocetakGL = new PointF();
    static PointF krajDD = new PointF();
////////////////////////////////////////////////////////////////////
    public static boolean start = false;
    public static boolean bool_obrisi = false;
    public static boolean bool_dodaj = false;
    static RectF rect = new RectF();

    public static void pomerajTarget(PointF tacka) {
         if (BuildConfig.DEBUG)Log.e("TAG10", "pomerajRect");
        pomBitmap = nacrtajIzracunateTacke(mBitmap, lista, NacinCrtanja.tackasto);
        mImageView.setImageBitmap(pomBitmap);

        Canvas canvas = new Canvas(pomBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.YELLOW);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAlpha(200);

        float uvecanje = mImageView.getMaxZoom() / mImageView.getCurrentZoom();

        rect.set(0,0,6*uvecanje,6*uvecanje);
        tacka.offset(0,-15*uvecanje);
        rect.offsetTo(tacka.x-3*uvecanje, tacka.y-3*uvecanje);

        canvas.drawRect(rect, paint);
        canvas.drawLine(0,       tacka.y,  pomBitmap.getWidth(), tacka.y,               paint);
        canvas.drawLine(tacka.x, 0,        tacka.x,              pomBitmap.getHeight(), paint);
        mImageView.setImageBitmap(pomBitmap);
    }
    private void dodajTacku(){
        PointF point = new PointF(rect.centerX(), rect.centerY());
        if (point.x > lista.get(lista.size()-1).x)
            lista.add(point);
        else
            for (int i = 0; i < lista.size(); i++) {
                if (point.x < lista.get(i).x){
                    lista.add(i,point);
                    break;
                }
            }
        mImageView.setImageBitmap( nacrtajIzracunateTacke(mBitmap, lista, NacinCrtanja.tackasto) );
    }

    public static void pomerajRect(PointF tacka) {
         if (BuildConfig.DEBUG)Log.e("TAG10", "pomerajRect");
        pomBitmap = nacrtajIzracunateTacke(mBitmap, lista, NacinCrtanja.tackasto);
        mImageView.setImageBitmap(pomBitmap);

        Canvas canvas = new Canvas(pomBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.YELLOW);
        paint.setStyle(Paint.Style.FILL);
        paint.setAlpha(128);

        float uvecanje = mImageView.getMaxZoom() / mImageView.getCurrentZoom();

        rect.set(0,0,3*uvecanje,3*uvecanje);
        rect.offsetTo(tacka.x-2*uvecanje, tacka.y-15*uvecanje);

        canvas.drawRect(rect, paint);
        mImageView.setImageBitmap(pomBitmap);
    }
    private void izbrisiTacku(){
        List<PointF> pomLista =  new LinkedList<>();
        for (PointF point : lista)
            if (rect.contains(point.x, point.y))
                pomLista.add(point); // ne moze direktno da se brise jer bi se javila greska u iteraciji zbog promene broja elemenata
        lista.removeAll(pomLista);
        mImageView.setImageBitmap( nacrtajIzracunateTacke(mBitmap, lista, NacinCrtanja.tackasto) );
    }

    public static void nacrtajDuz(PointF pocetak, PointF kraj) {
         if (BuildConfig.DEBUG)Log.e("TAG10", "nacrtajDuz");
        pomBitmap = mBitmap.copy(Bitmap.Config.ARGB_8888,true);
        Canvas canvas = new Canvas(pomBitmap);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(1);
        paint.setColor(Color.RED);
        canvas.drawLine(pocetak.x, pocetak.y, kraj.x, kraj.y, paint);
        mImageView.setImageBitmap(pomBitmap);
        pocetakGL.set(Math.min(pocetak.x, kraj.x), Math.min(pocetak.y, kraj.y) );
        krajDD.set(Math.max(pocetak.x, kraj.x), Math.max(pocetak.y, kraj.y) );


    }
////////////////////////////////////////////////////////////////////
    private PointF nadjiPocetakLinije(Bitmap bitmap, PointF pocetnaTacka, PointF krajnjaTacka){
        int color_pocetno = bitmap.getPixel((int)pocetnaTacka.x, (int)pocetnaTacka.y);
        int color = color_pocetno;
        PointF point = new PointF();
        boolean flag = false;
        List<PointF> lista = new ArrayList<>();
        for (int x = (int)pocetnaTacka.x; x <= (int)krajnjaTacka.x; x++) {
            for (int y = (int)pocetnaTacka.y; y <= (int)krajnjaTacka.y; y++) {
                if (color != tamnijaBoja(bitmap.getPixel(x,y) , color) ){
                    //flag = true;
                    color = bitmap.getPixel(x,y);
                    point.set(x,y);
                     if (BuildConfig.DEBUG)Log.e("TAG3", "point "+point.toString());
                }
            }
            lista.add(point);
            color = color_pocetno;
        }

        int brojac =0;
        int index=0;
        for (PointF p : lista) {
            if (brojac < brojIstihElemenataUListi(p, lista)){
                brojac = brojIstihElemenataUListi(p, lista);
                index = lista.indexOf(p);
            }
        }

        //NadjiTacke.setBackgroundColor(bitmap.getPixel((int)lista.get(index).x, (int)lista.get(index).y));
         if (BuildConfig.DEBUG)Log.e("TAG3", "lista.get(index) " +lista.get(index) );
        return lista.get(index);
    }
    private static int maxSuma = 10;       //podesavanje osetljivosti pretrage
    private static final int maxBrojSusednihTacaka_Y = 100;
    private PointF nadjiDesniNastavak(final Bitmap bitmap, final PointF pocetak){
         if (BuildConfig.DEBUG)Log.e("TAGd", "----------------------- "+ pocetak.toString());

        int pocetnaBoja = bitmap.getPixel((int)pocetak.x, (int)pocetak.y);
        PointF novaTacka = new PointF();
        float x = pocetak.x+1, y = pocetak.y; // tacka od koje procinjemo pretragu

        //
        //broj tacaka po y osi (x vred. se ne menja) za koje se proverava da li predstavljaju nastavak
        int brojSusednihTacaka_Y = 15;
        // ako sledeca tacka za datu X koord. nije pronadjena (za x=const, y=f(brojSusednihTacaka_Y) ),
        // onda povecavamo x koord. i pokusavamo da nadjemo narednu (preskacemo prekid trase na tom mestu)
        int brojac_X=0;
        // za odredjivanje da li je Y koord. (od 0 do brojSusednihTacaka_Y) ispod, u visini, ili iznad
        // prethodno pronjdjene tacke
        int visina_Y=0;
        //
        // broj nadjenih tacaka iznad/ispod visina_Y
        int sumaGore, sumaDole;
        do {
            sumaGore=sumaDole=0;
            for (int i = 0; i < brojSusednihTacaka_Y; i++) {
                if (bitmap.getWidth() <= (x+brojac_X)   ||    bitmap.getHeight() <= (y+ i - (brojSusednihTacaka_Y)/2 )  ||
                        (y + i - (brojSusednihTacaka_Y)/2 ) <= 0) {
                    return pocetak;
                }
                if (pocetnaBoja == bitmap.getPixel( (int)x + brojac_X,
                                        (int)y + i - (brojSusednihTacaka_Y)/2 )) {

                    visina_Y =  i - (brojSusednihTacaka_Y)/2;
                    if (visina_Y   <  0) {
                        sumaGore++;
                    }else{
                        sumaDole++;
                    }

                }
            }
            if (sumaDole+sumaGore==1){
                novaTacka.set(x + brojac_X,  y + visina_Y);
                return novaTacka;
            }
            else if (sumaDole+sumaGore>0 ){
                if (sumaGore < maxSuma && sumaDole < maxSuma){//// todo: 2.12.17 sredi
                    novaTacka.set(x + brojac_X,  y + visina_Y);
                    return novaTacka;
                }

            }
             if (BuildConfig.DEBUG)Log.e("TAGd","x "+(x+brojac_X) +" y "+(y )
                          +"  brojac_X " +brojac_X +"  suma " +(sumaDole+sumaGore) );
            brojSusednihTacaka_Y +=2; // dodajemo po dve tacke (jedna gore i jedan dole)
            brojac_X++;
        }while (brojSusednihTacaka_Y<maxBrojSusednihTacaka_Y);

        return pocetak;
    }
    private PointF nadjiLeviNastavak(final Bitmap bitmap, final PointF pocetak){
         if (BuildConfig.DEBUG)Log.e("TAGl", "----------------------- "+ pocetak.toString());
        int pocetnaBoja = bitmap.getPixel((int)pocetak.x, (int)pocetak.y);
        PointF novaTacka = new PointF();
        float x = pocetak.x-1, y = pocetak.y; // tacka od koje procinjemo pretragu

        //
        //broj tacaka po y osi (x vred. se ne menja) za koje se proverava da li predstavljaju nastavak
        int brojSusednihTacaka_Y = 15;
        // ako sledeca tacka za datu X koord. nije pronadjena (za x=const, y=f(brojSusednihTacaka_Y) ),
        // onda povecavamo x koord. i pokusavamo da nadjemo narednu (preskacemo prekid trase na tom mestu)
        int brojac_X=0;
        // za odredjivanje da li je Y koord. (od 0 do brojSusednihTacaka_Y) ispod, u visini, ili iznad
        // prethodno pronjdjene tacke
        int visina_Y=0;
        //
        // broj nadjenih tacaka iznad/ispod visina_Y
        int sumaGore, sumaDole;
        do {
            sumaGore=sumaDole=0;
            for (int i = 0; i < brojSusednihTacaka_Y; i++) {
                if (0 >= (x-brojac_X)   ||    bitmap.getHeight() <= (y+ i - (brojSusednihTacaka_Y)/2 )  ||
                        (y + i - (brojSusednihTacaka_Y)/2 ) <= 0) {
                    return pocetak;
                }
                if (pocetnaBoja == bitmap.getPixel((int)x - brojac_X,
                                                   (int)y + i - (brojSusednihTacaka_Y)/2 )) {

                    visina_Y =  i - (brojSusednihTacaka_Y)/2;
                    if (visina_Y   <  0) {
                        sumaGore++;
                    }else{
                        sumaDole++;
                    }

                }
            }
            if (sumaDole+sumaGore==1){
                novaTacka.set(x - brojac_X,  y + visina_Y);
                return novaTacka;
            }
            else if (sumaDole+sumaGore>0 ){
                if (sumaGore < maxSuma && sumaDole < maxSuma){//// todo: 2.12.17 sredi
                    novaTacka.set(x - brojac_X, y + visina_Y);
                    return novaTacka;
                }

            }
             if (BuildConfig.DEBUG)Log.e("TAGl","x "+(x-brojac_X) +" y "+(y )
                    +"  brojac " +brojac_X +"  suma " +(sumaDole+sumaGore) );
            brojSusednihTacaka_Y +=2;
            brojac_X++;
        }while (brojSusednihTacaka_Y<maxBrojSusednihTacaka_Y);

        return pocetak;
    }
////////////////////////////////////////////////////////////////////
    private boolean bojaJeUListi(List<Integer> lista, int boja){
        for (int i:lista)
            if (i == boja)
                return true;

        return false;
    }
    public int tamnijaBoja(int color1, int color2){
        // if (BuildConfig.DEBUG)Log.e("TAG3", "*************tamnijaBoja " +color1+ " "+ color2);
        double darkness1 = 1-(0.299* Color.red(color1) + 0.587*Color.green(color1) + 0.114*Color.blue(color1))/255;
        double darkness2 = 1-(0.299* Color.red(color2) + 0.587*Color.green(color2) + 0.114*Color.blue(color2))/255;
        if(darkness1>=darkness2){
            return color1;
        }else{
            return color2;
        }
    }
    public int svetlijaBoja(int color1, int color2){
        double darkness1 = 1-(0.299* Color.red(color1) + 0.587*Color.green(color1) + 0.114*Color.blue(color1))/255;
        double darkness2 = 1-(0.299* Color.red(color2) + 0.587*Color.green(color2) + 0.114*Color.blue(color2))/255;
        if(darkness1>=darkness2){
            return color2; // It's a light color
        }else{
            return color1; // It's a dark color
        }
    }
    public Bitmap toGrayscale(Bitmap bmpOriginal){
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }
    private List nadjiBoje(Bitmap bitmap){
        List lista = new ArrayList();
        int dimX, dimY;
        dimX = bitmap.getWidth();
        dimY = bitmap.getHeight();
        int color = bitmap.getPixel(0,0);
        for (int x = 0; x < dimX; x++) {
            for (int y = 0; y < dimY; y++) {
                if (color != bitmap.getPixel(x,y)){
                    color = bitmap.getPixel(x,y);
                    if (!bojaJeUListi(lista, color)){
                        lista.add(color);
                    }
                }
            }
        }
        return lista;
    }
    public int najtamnijaBojaUListi(List<Integer> lista){
        int najtamnije = lista.get(0);
        int index=0;
        for (int i = 0; i < lista.size(); i++) {
            if (najtamnije != tamnijaBoja(lista.get(i), najtamnije)){
                najtamnije = lista.get(i);
                index = i;
            }
        }


         if (BuildConfig.DEBUG)Log.e("TAG2", "najtamnijaBojaUListi "+lista.get(index));
        return index;
    }
    public int najsvetlijaBojaUListi(List<Integer> lista){
        int najsvetlija = lista.get(0);
        int index=0;
        for (int i = 0; i < lista.size(); i++) {
            if (najsvetlija != svetlijaBoja(lista.get(i), najsvetlija)){
                najsvetlija = lista.get(i);
                index = i;
            }
        }

         if (BuildConfig.DEBUG)Log.e("TAG2", "najsvetlijaBojaUListi "+lista.get(index));
        return index;
    }
////////////////////////////////////////////////////////////////////

    private List izracunajTacke(Bitmap mBitmap, PointF pocetakGL, PointF krajDD){

        PointF pocetakLinije = nadjiPocetakLinije(mBitmap, pocetakGL, krajDD);

        int br=0;
        List<PointF> listaDesno = new ArrayList<>();
        PointF pom1=pocetakLinije;
        PointF pom2;
        while (true) {
             if (BuildConfig.DEBUG)Log.e("TAGd", "while desno "+ (br++));
            pom2 = nadjiDesniNastavak(mBitmap, pom1);
             if (BuildConfig.DEBUG)Log.e("TAGd", "pom2 "+pom2);
            if ( ! pom1.equals(pom2.x, pom2.y) && br<10_000 ) {
                pom1=pom2;
                listaDesno.add(new PointF(pom2.x, pom2.y /** -5*/));
            }
            else
                break;
        }

        br=0;
        List<PointF> listaLevo = new ArrayList<>();
        pom1=pocetakLinije;
        while (true) {
             if (BuildConfig.DEBUG)Log.e("TAGl", "while levo "+ (br++));
            pom2 = nadjiLeviNastavak(mBitmap, pom1);
             if (BuildConfig.DEBUG)Log.e("TAGl", "pom2 "+pom2);
            if ( ! pom1.equals(pom2.x, pom2.y) && br<10_000 ) {
                pom1=pom2;
                listaLevo.add(new PointF(pom2.x, pom2.y /** -5*/));
            }
            else
                break;
        }

        List<PointF> newList = new ArrayList<>();
        Collections.reverse(listaLevo);
        newList.addAll(listaLevo);
        newList.addAll(listaDesno);

        return newList;
    }

    private static Bitmap nacrtajIzracunateTacke(Bitmap mBitmap, List<PointF> lista, NacinCrtanja nacinCrtanja){

        Paint alphaPaint = new Paint();
        int currentAlpha = 64;
        alphaPaint.setAlpha(currentAlpha);

        Bitmap novBitmap = mBitmap.copy(Bitmap.Config.ARGB_8888, true);
        novBitmap.eraseColor(Color.TRANSPARENT);
        Canvas canvas = new Canvas(novBitmap);
        canvas.drawBitmap(mBitmap, 0,0, alphaPaint);

        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(1);
        if (nacinCrtanja == NacinCrtanja.tackasto)
            for (int i = 0; i < lista.size(); i++)
                canvas.drawPoint(lista.get(i).x,lista.get(i).y,paint);

        else if (nacinCrtanja == NacinCrtanja.linije)
            for (int i = 0; i < lista.size()-1; i++)
                canvas.drawLine(lista.get(i).x,   lista.get(i).y,
                                lista.get(i+1).x, lista.get(i+1).y, paint);

        return  novBitmap;
    }

    private void sacuvajSliku(Bitmap mBitmap) {
        File fajlZaGrafik = new File(folderOutput + File.separator + nazivIzlaznogFajla + " -novo" + ".png");
        try {
            FileOutputStream ostream = new FileOutputStream(fajlZaGrafik);
            mBitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);
            ostream.flush();
            ostream.close();
            MediaScannerConnection.scanFile(this, new String[]{fajlZaGrafik.getAbsolutePath()}, null, null);
            prikaziToast(this, "Grafik je sacuvan: " + fajlZaGrafik.getName());
        } catch (IOException e) {
             if (BuildConfig.DEBUG)Log.e("TAG35", "GRESKA - ne moze da se kreira .png fajl");
            e.printStackTrace();
        }

    }


    private float[] konvertujUStacionaze_X (List<PointF> lista,  float pocetak, float kraj){
        float[] niz = new float[lista.size()];
        if (lista.size()==0)
            return niz;
        float rasponPiksela = lista.get(lista.size()-1).x - lista.get(0).x;
        float rasponStacionaza = Math.abs(kraj-pocetak);
        float razmera = rasponStacionaza / rasponPiksela;

        for (int i = 0; i < niz.length; i++){
            niz[i] = (lista.get(i).x - lista.get(0).x) * razmera;
        }

        return niz;
    }
    private float[] konvertujUKote_Y (List<PointF> lista, float[] nizX, PointF ref_1, PointF ref_2){
        float[] niz_Y = new float[lista.size()];
        if (lista.size()==0)
            return niz_Y;

        for (int i = 0; i < niz_Y.length; i++)
            niz_Y[i] = mBitmap.getHeight() - lista.get(i).y;

        float minY = 10_000;
        for (float clanNiza : niz_Y)
            if (clanNiza < minY)
                minY = clanNiza;

        for (int i = 0; i < niz_Y.length; i++)
            niz_Y[i]-=minY;

        float sirinaOrg_y = ref_1.y - ref_2.y;
        float sirinaPix_y = linearnaInterpolacija(ref_1.x, nizX, niz_Y)
                            - linearnaInterpolacija(ref_2.x, nizX, niz_Y);

        float razmera = sirinaOrg_y / sirinaPix_y;

        for (int i = 0; i < niz_Y.length; i++)
            niz_Y[i] = niz_Y[i] * razmera +317.34f;

        return niz_Y;
    }
    private float[] konvertujUKote_Y (List<PointF> lista, float[] nizX, float y_min, float y_max){
        float[] niz_Y = new float[lista.size()];
        if (lista.size()==0)
            return niz_Y;

        for (int i = 0; i < niz_Y.length; i++)
            niz_Y[i] = mBitmap.getHeight() - lista.get(i).y;

        float minY = 10_000;
        float maxY = 0;
        for (int i = 0; i < niz_Y.length; i++) {
            if (niz_Y[i] < minY)
                minY = niz_Y[i];
            if (niz_Y[i] > maxY)
                maxY = niz_Y[i];
        }
        for (int i = 0; i < niz_Y.length; i++)
            niz_Y[i]-=minY;

        float sirinaOrg_y = Math.abs(y_max - y_min);
        float sirinaPix_y = maxY - minY;

        float razmera = sirinaOrg_y / sirinaPix_y;

        for (int i = 0; i < niz_Y.length; i++)
            niz_Y[i] = niz_Y[i] * razmera + y_min;

        return niz_Y;
    }
    String nazivIzlaznogFajla;
    File fajlSave, folderOutput;
    private void sacuvajTXT_dialog(){
        String nazivOtvSlike = ProveraSigVisina_Fragment.getFileName_fromUri(selectedFile_uri, this);
        nazivOtvSlike = nazivOtvSlike.replace(".png","").replace(".jpg","").replace(".jpeg","");

        AlertDialog ad = new AlertDialog.Builder(LineExtractor_Activity.this).create();
        ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
        ad.setTitle(mojHtmlString("<intent>Cuvanje podataka"));
        ad.setMessage(/*mojHtmlString*/("Unesi naziv fajla u koji ce podaci biti sacuvani:"));
        final EditText inputEditText = new EditText(ad.getContext());
        ad.setView(inputEditText);
        inputEditText.setText(nazivOtvSlike);
        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        nazivIzlaznogFajla = inputEditText.getText().toString();
                        folderOutput = kreirajFolder(context, folderUserData, nazivIzlaznogFajla);
                        fajlSave = new File(folderOutput + File.separator + nazivIzlaznogFajla + ".txt");

                        if (fajlSave.exists()){
                            new AlertDialog.Builder(LineExtractor_Activity.this)
                                    /*.setIcon(R.mipmap.ic_alert_crveno)*/
                                    .setTitle(/*mojHtmlString*/("<b><intent>Paznja!"))
                                    .setMessage(/*mojHtmlString*/("Vec postoji fajl <b>" + nazivIzlaznogFajla +
                                            "</b> Sacuvaj preko njega?"))
                                    .setPositiveButton("DA", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            upisPodatakaUTXT();
                                            sacuvajSliku(nacrtajIzracunateTacke(mBitmap, lista, NacinCrtanja.linije));
                                        }
                                    })
                                    .setNegativeButton("NE", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            sacuvajTXT_dialog();
                                        }
                                    })
                                    .show();
                        }
                        else{
                            upisPodatakaUTXT();
                            sacuvajSliku(nacrtajIzracunateTacke(mBitmap, lista, NacinCrtanja.linije));
                        }
                    }
                });
        ad.setButton(AlertDialog.BUTTON_NEGATIVE, "Odustani",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        ad.show();


    }

    private void upisPodatakaUTXT(){
        try {
            fajlSave.createNewFile();
        } catch (IOException e) {
            /*prikaziToast(getActivity(),"Greska, fajl <b>" + nazivTXT_STUBOVI_novi +
                    "<\b>ne moze da se kreira!");*/
             if (BuildConfig.DEBUG)Log.e("TAG10", "Greska, fajl " + nazivIzlaznogFajla + "ne moze da se kreira!");
            e.printStackTrace();
            return;
        }

        float[] nizX = konvertujUStacionaze_X(lista, x_start, x_kraj);

        float[] nizY;
        if (y_min==nepostoji)
            nizY = konvertujUKote_Y(lista, nizX, new PointF(x1, y1), new PointF(x2, y2));
        else
            nizY = konvertujUKote_Y(lista, nizX, y_min, y_max);

        ///////new PointF(482.91f, 317.34f), new PointF(193.74f, 317.34f)
         if (BuildConfig.DEBUG)Log.e("TAG10", "nizX " + nizX.length+ "   nizY "+nizY.length);

        try {
            if ( fajlSave.canWrite() ) {
                FileOutputStream out = new FileOutputStream(fajlSave);
                out.write((";uzduzni profil trase: " + nazivIzlaznogFajla + "\n").getBytes());
                out.write((";stacionaza[m]  kota[m]").getBytes());

                float pom = mBitmap.getHeight();
                for (int i = 0; i < nizX.length; i++) {
                    out.write((" \n" + nizX[i]).getBytes());
                    out.write((" \t" + nizY[i]).getBytes());
                    //out.write((" \t" + (pom - lista.get(i).y)).getBytes());
                }
                out.flush();
                out.close();

                MediaScannerConnection.scanFile(this, new String[]{fajlSave.getAbsolutePath()}, null, null);

                Toast.makeText(this,"Zavrseno", Toast.LENGTH_SHORT).show();

            } else {
                /*prikaziToast(getActivity(),"Greska, u fajl <b>" + nazivTXT_STUBOVI_novi +
                        "<\b>ne mogu da se upisuji podaci!");*/
            }

        } catch (Exception e) {
             if (BuildConfig.DEBUG)Log.e("TAG10", "greska pri pisanju izlaznog TXT");
            e.printStackTrace();
            return;
        }
    }
////////////////////pomocne metode//////////////////////////////////////
public static int indexSusednog_ManjegClanaNiza (float tacka, float niz[]){
    /** vazi ako je niz rastuci*/

    if (tacka <= niz[0]) { // van opsega (u minusu)
        // if (BuildConfig.DEBUG)Log.e("TAGgreska","tacka=" + tacka +" niz[0]="+niz[0]+" return "+0);
        return 0;

    }else if (tacka >= niz[niz.length - 1]) { // van opsega (u plusu)
        // if (BuildConfig.DEBUG)Log.e("TAGgreska","tacka=" + tacka +" niz[POSLEDNJI]="+niz[niz.length - 1]+" return "+(niz.length - 1));
        return niz.length - 2; // ako vraca -1, pravio bi gresku u  [indexStuba + 1]

    }else{ // u opsegu
        for (int i = 1; i < niz.length; i++) {
            if (niz[i] == tacka) {
                // if (BuildConfig.DEBUG)Log.e("TAGgreska","tacka=" + tacka +" niz["+i+"]="+niz[i]+" return "+i);
                return i; // u pitanju je tacka koja se poklapa sa tackom stuba
            }else if (niz[i] > tacka) {
                // if (BuildConfig.DEBUG)Log.e("TAGgreska","tacka=" + tacka +" niz["+i+"]="+niz[i]+" return "+(i-1));
                return i - 1;
            }
        }
    }

     if (BuildConfig.DEBUG)Log.e("TAGgreska","GRESKA indexSusednog_ManjegClana()  vraca -1");
    return nepostoji; // ako je neka greska
}

    public static int indexSusednog_VecegClanaNiza (float tacka, float niz[]){
        // vazi ako je niz rastuci
        if (tacka<=niz[0])
            return 0;

        else if (tacka>=niz[niz.length-1])
            return niz.length-1;

        else{
            for (int i = 1; i < niz.length; i++) {
                if (niz[i] >= tacka)
                    return i ;
            }
        }

         if (BuildConfig.DEBUG)Log.e("TAGgreska","GRESKA indexSusednog_VecegClana()  vraca -1");
        return nepostoji; // ako je neka greska
    }
    public static float linearnaInterpolacija(float tacka_X, float niz_X[], float niz_Y[]){

    // if (BuildConfig.DEBUG)Log.e("TAG11", "niz_X[]= " + Arrays.toString(niz_X) );
    // if (BuildConfig.DEBUG)Log.e("TAG11", "niz_Y[]= " + Arrays.toString(niz_Y) );
    int index0 = indexSusednog_ManjegClanaNiza (tacka_X, niz_X);
    int index1 = indexSusednog_VecegClanaNiza (tacka_X, niz_X);

     if (BuildConfig.DEBUG)Log.e("TAG35", "tacka_X= " +tacka_X +" index0= " +index0 + " index1= " +index1 );
    if (index0==index1) // u pitanju je kota stuba koja se poklapa sa unesenom kotom tla
        return niz_Y[index0];

    float x0 = niz_X[index0];
    float x1 = niz_X[index1];

    float y0 = niz_Y[index0];
    float y1 = niz_Y[index1];

    float x = tacka_X;
    float y; // to se trazi

    y = y0 + (x - x0) * ( (y1-y0) / (x1-x0) );

    return y;

}
    int brojIstihElemenataUListi(PointF element, List<PointF> lista){
    int brojac =0;
    for (PointF p : lista) {
        if (p.equals(element))
            brojac++;
    }
    return brojac;
}
    private static int maxImageSize(){
        // na osnovu ovog utvrdjujemo kolika je max dozvoljena dimenzija bitmap.a u pix
        // obicno je to 2024x2024 ili 4048x4048
        //https://stackoverflow.com/questions/26985858/gles10-glgetintegerv-returns-0-in-lollipop-only
        EGL10 egl = (EGL10) EGLContext.getEGL();

        EGLDisplay dpy = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        int[] vers = new int[2];
        egl.eglInitialize(dpy, vers);

        int[] configAttr = {
                EGL10.EGL_COLOR_BUFFER_TYPE, EGL10.EGL_RGB_BUFFER,
                EGL10.EGL_LEVEL, 0,
                EGL10.EGL_SURFACE_TYPE, EGL10.EGL_PBUFFER_BIT,
                EGL10.EGL_NONE
        };
        EGLConfig[] configs = new EGLConfig[1];
        int[] numConfig = new int[1];
        egl.eglChooseConfig(dpy, configAttr, configs, 1, numConfig);
        if (numConfig[0] == 0) {
            // TROUBLE! No config found.
        }
        EGLConfig config = configs[0];

        int[] surfAttr = {
                EGL10.EGL_WIDTH, 64,
                EGL10.EGL_HEIGHT, 64,
                EGL10.EGL_NONE
        };
        EGLSurface surf = egl.eglCreatePbufferSurface(dpy, config, surfAttr);
        final int EGL_CONTEXT_CLIENT_VERSION = 0x3098;  // missing in EGL10
        int[] ctxAttrib = {
                EGL_CONTEXT_CLIENT_VERSION, 1,
                EGL10.EGL_NONE
        };
        EGLContext ctx = egl.eglCreateContext(dpy, config, EGL10.EGL_NO_CONTEXT, ctxAttrib);
        egl.eglMakeCurrent(dpy, surf, surf, ctx);
        int[] maxSize = new int[1];
        GLES10.glGetIntegerv(GLES10.GL_MAX_TEXTURE_SIZE, maxSize, 0);
        egl.eglMakeCurrent(dpy, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE,
                EGL10.EGL_NO_CONTEXT);
        egl.eglDestroySurface(dpy, surf);
        egl.eglDestroyContext(dpy, ctx);
        egl.eglTerminate(dpy);

        return maxSize[0];
    }
    private static int calculateInSampleSize(BitmapFactory.Options options, int maxWidth, int maxHeight) {
        // vraca koliko puta treba da se smanji slika da bi bila njene dimenzije bile manje od max dozvoljenih
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        //Toast.makeText(this, "height " + height+" width " + width+" maxHeight " + maxHeight+" maxWidth " + maxWidth, Toast.LENGTH_SHORT).show();

        if (height > maxHeight || width > maxWidth) {

            final int halfHeight = height / inSampleSize;
            final int halfWidth = width / inSampleSize;

            while ((halfHeight / inSampleSize) >= maxHeight
                    || (halfWidth / inSampleSize) >= maxWidth) {
                inSampleSize *= 2;
            }
        }

        //Toast.makeText(this, "inSampleSize " + inSampleSize, Toast.LENGTH_SHORT).show();

        return inSampleSize;
    }

    public static String getFileType_fromUri(Uri uri, Context c) {
        String fileType = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = c.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    fileType = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (fileType == null) {
            fileType = uri.getPath();
            int cut = fileType.lastIndexOf('.');
            if (cut != -1) {
                fileType = fileType.substring(cut + 1);
            }
        }
        return fileType;
    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
        if(mBitmap!=null){
            mBitmap.recycle();
            mBitmap=null;
        }
        if(pomBitmap!=null){
            pomBitmap.recycle();
            pomBitmap=null;
        }
        System.gc();

    }
}
