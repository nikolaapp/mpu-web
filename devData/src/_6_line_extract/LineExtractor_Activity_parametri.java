package nsp.mpu._6_line_extract;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;

@Obfuscate
public class LineExtractor_Activity_parametri extends Activity {

    private float x_start, x_kraj,y_min, y_max,x1,y1, x2, y2;

    LinearLayout linearLayout_Y_maxmin, linearLayout_Y_t1t2;
    EditText editText_x1,editText_y1,editText_x2,editText_y2,
            editText_x_start,editText_x_kraj,editText_y_min,editText_y_max;
    Button button;
    ToggleButton toggleButton ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.line_extractor_parametri);

        findViews();
        kreiranjeListenera();
        linearLayout_Y_t1t2.setVisibility(GONE);



    }

    private void kreiranjeListenera(){
        editText_x_start.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                float pomocnaVar;
                try {
                    pomocnaVar = Float.parseFloat(s.toString());
                    if (pomocnaVar<0) {
                        pomocnaVar=0;
                        Toast.makeText(getApplicationContext(), "Mora biti pozitivna vrednost", Toast.LENGTH_SHORT).show();
                    }
                    x_start = pomocnaVar;
                } catch (NumberFormatException e) {}
            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void afterTextChanged(Editable s) {}
        });
        editText_x_kraj.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                float pomocnaVar;
                try {
                    pomocnaVar = Float.parseFloat(s.toString());
                    if (pomocnaVar<=x_start ||pomocnaVar>x_start+3000) {
                        pomocnaVar=x_start+100;
                        Toast.makeText(getApplicationContext(), "vece od x_start, manje od x_start+5000 !!!", Toast.LENGTH_SHORT).show();
                    }
                    x_kraj = pomocnaVar;
                } catch (NumberFormatException e) {}
            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void afterTextChanged(Editable s) {}
        });

        editText_y_min.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                float pomocnaVar = nepostoji;
                try {
                    pomocnaVar = Float.parseFloat(s.toString());
                    if (pomocnaVar<0) {
                        pomocnaVar=0;
                        Toast.makeText(getApplicationContext(), "Mora biti pozitivna vrednost", Toast.LENGTH_SHORT).show();
                    }
                    y_min = pomocnaVar;
                } catch (NumberFormatException e) {}
            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void afterTextChanged(Editable s) {}
        });
        editText_y_max.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                float pomocnaVar;
                try {
                    pomocnaVar = Float.parseFloat(s.toString());
                    if (pomocnaVar<=y_min ||pomocnaVar>y_min+1000) {
                        pomocnaVar=x_start+100;
                        Toast.makeText(getApplicationContext(), "vece od Ymin, manje od Ymin+1000 !!!", Toast.LENGTH_SHORT).show();
                    }
                    y_max = pomocnaVar;
                } catch (NumberFormatException e) {}
            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void afterTextChanged(Editable s) {}
        });

        editText_x1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                float pomocnaVar;
                try {
                    pomocnaVar = Float.parseFloat(s.toString());
                    if (pomocnaVar<x_start || pomocnaVar>=x_kraj) {
                        pomocnaVar=x_start;
                        Toast.makeText(getApplicationContext(), "Mora biti u intervalu [x_min,x_max)", Toast.LENGTH_SHORT).show();
                    }
                    x1 = pomocnaVar;
                } catch (NumberFormatException e) {}
            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void afterTextChanged(Editable s) {}
        });
        editText_y1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                float pomocnaVar;
                try {
                    pomocnaVar = Float.parseFloat(s.toString());
                    if (pomocnaVar<0) {
                        pomocnaVar=0;
                        Toast.makeText(getApplicationContext(), "Mora biti pozitivna vrednost", Toast.LENGTH_SHORT).show();
                    }
                    y1 = pomocnaVar;
                } catch (NumberFormatException e) {}
            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void afterTextChanged(Editable s) {}
        });

        editText_x2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                float pomocnaVar;
                try {
                    pomocnaVar = Float.parseFloat(s.toString());
                    if (pomocnaVar<=x1 || pomocnaVar>x_kraj) {
                        pomocnaVar=x_kraj;
                        Toast.makeText(getApplicationContext(), "Mora biti u intervalu (x2,x_max]", Toast.LENGTH_SHORT).show();
                    }
                    x2 = pomocnaVar;
                } catch (NumberFormatException e) {}
            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void afterTextChanged(Editable s) {}
        });
        editText_y2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                float pomocnaVar;
                try {
                    pomocnaVar = Float.parseFloat(s.toString());
                    if (pomocnaVar<0) {
                        pomocnaVar=y1+100;
                        Toast.makeText(getApplicationContext(), "Mora biti pozitivna vrednost", Toast.LENGTH_SHORT).show();
                    }
                    y2 = pomocnaVar;
                } catch (NumberFormatException e) {}
            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void afterTextChanged(Editable s) {}
        });

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if ( ! isChecked){
                    linearLayout_Y_maxmin.setVisibility(VISIBLE);
                    linearLayout_Y_t1t2.setVisibility(GONE);
                }else{
                    linearLayout_Y_maxmin.setVisibility(GONE);
                    linearLayout_Y_t1t2.setVisibility(VISIBLE);
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("x_start",x_start);
                returnIntent.putExtra("x_kraj",x_kraj);
                
                if (toggleButton.isChecked()){
                    returnIntent.putExtra("x1",x1);
                    returnIntent.putExtra("y1",y1);
                    returnIntent.putExtra("x2",x2);
                    returnIntent.putExtra("y2",y2);
                }else{
                    returnIntent.putExtra("y_min",y_min);
                    returnIntent.putExtra("y_max",y_max);                    
                }
                setResult(Activity.RESULT_OK,returnIntent);
                finish();

            }
        });
    }


    private void findViews(){

        linearLayout_Y_maxmin = (LinearLayout) findViewById(R.id.line_extrator_param_layouty_y_maxmin);
        linearLayout_Y_t1t2 = (LinearLayout) findViewById(R.id.line_extrator_param_layouty_y_t1t2);

        editText_x_start = (EditText) findViewById(R.id.x_pocetak);
        editText_x_kraj = (EditText) findViewById(R.id.x_kraj);

        editText_y_min = (EditText) findViewById(R.id.y_min);
        editText_y_max = (EditText) findViewById(R.id.y_max);

        editText_x1 = (EditText) findViewById(R.id.x1);
        editText_y1 = (EditText) findViewById(R.id.y1);

        editText_x2 = (EditText) findViewById(R.id.x2);
        editText_y2 = (EditText) findViewById(R.id.y2);

        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);

        button= (Button) findViewById(R.id.line_extrator_param_sacuvaj);
    }
}
