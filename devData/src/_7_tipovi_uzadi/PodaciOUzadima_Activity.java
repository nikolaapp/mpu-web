package nsp.mpu._7_tipovi_uzadi;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v4.app.Fragment;

import nsp.mpu.R;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.ActivityZaJedanFragment;
import nsp.mpu.pomocne_klase.AnimacijeKlasa;


/** @-O-b-f-u-s-c-a-t-e ne radi (nedozvoljava build) zbog implements fragmenta*/
public class PodaciOUzadima_Activity extends ActivityZaJedanFragment
        implements PodaciOUzadima_Fragment.Komunikacija_Frag_Act,
                    PodaciOUzadima_Pager_Fragment.Komunikacija_Frag_Act {

    @Override
    protected Fragment KreirajFragment(){
        if ( ! (findViewById(R.id.activity_prikaz_drugog_fragmenta)==null))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        return new PodaciOUzadima_Fragment();
    }

////////////////Prikaz dva fragmenta 4.0.2//////////////////////
    @Override
    protected int postaviNovLayout(){
        return R.layout.activity_reference_jedan_frag_ili_oba;
    }

    @Override
    public void onTipSelected(PODACI_TipUzeta tip){
        if ( findViewById(R.id.activity_prikaz_drugog_fragmenta)==null ){
            Intent i = new Intent(this,PodaciOUzadima_Pager_Activity.class);
            i.putExtra(PodaciOUzadima_Pager_Activity.TAGoznakaTipa,
                    tip.getOznakaUzeta());
            startActivity(i);
        } else {
            Fragment frag = PodaciOUzadima_Pager_Fragment.
                    noviFragmentZaPager(tip.getOznakaUzeta());
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .replace(R.id.activity_prikaz_drugog_fragmenta, frag)
                    .commit();
        }
    }

    @Override
    public void onTipUpdated(){
        PodaciOUzadima_Fragment lista = (PodaciOUzadima_Fragment)
                getSupportFragmentManager().findFragmentById(R.id.activity_prikaz_fragmenta);
                lista.startAdaptera(true);
    }

    @Override
    public void indexPrikazanogTipa(int index) {
        PodaciOUzadima_Fragment fragment = (PodaciOUzadima_Fragment)
                getSupportFragmentManager().findFragmentById(R.id.activity_prikaz_fragmenta);
        if (fragment.mRecyclerView.findViewHolderForAdapterPosition(index) != null)
            fragment.mRecyclerView.findViewHolderForAdapterPosition(index)
                .itemView.setBackgroundColor(Color.GRAY);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AnimacijeKlasa.ParAnimacijaInOut parAnim = new AnimacijeKlasa.ParAnimacijaInOut();
        overridePendingTransition(parAnim.in, parAnim.out);
    }
//////////////////////////////////////////////////////////////////////
}
