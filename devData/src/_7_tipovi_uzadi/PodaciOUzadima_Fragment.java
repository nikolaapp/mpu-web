package nsp.mpu._7_tipovi_uzadi;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.ObradaBazePodataka;
import nsp.mpu.database.PODACI_ListaTipovaUzadi;
import nsp.mpu.database.PODACI_TipUzeta;

import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animacijaZaRecycleView;

@Obfuscate
public class PodaciOUzadima_Fragment extends Fragment {

    public RecyclerView mRecyclerView;
    private AdapterUzadi mAdapter;

///////////////////Komunikacija_Frag_Act 4.0.2//////////////
    private Komunikacija_Frag_Act mCallbacks;
    public interface Komunikacija_Frag_Act{
        void onTipSelected(PODACI_TipUzeta tip);
    }
    @Override
    public void onAttach(Context c){
        super.onAttach(c);
        mCallbacks = (Komunikacija_Frag_Act) c;
    }
    @Override
    public void onDetach(){
        super.onDetach();
        mCallbacks = null;
    }
////////////////////////////////////////////////////////////

    @Override
    //ucitavanje promena nastalih pri prelasku na drugi activity
    public void onResume(){
        super.onResume();
        startAdaptera(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu m, MenuInflater i) {
        super.onCreateOptionsMenu(m, i);
        i.inflate(R.menu.meni_tipovi_uzadi, m);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_tip:
                Intent i = new Intent(getActivity(), PodaciOUzadima_NovTip_Activity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSubtitle() {
        int brojTipova = PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).getListuTipovaUzadi().size();
        AppCompatActivity a = (AppCompatActivity) getActivity();
        a.getSupportActionBar().setTitle("Podaci o tipovima uzadi"  + " (v."
                + ObradaBazePodataka.DATABASE_VERSION + ")");
        a.getSupportActionBar().setSubtitle("Broj unetih tipova: "+brojTipova);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle savedInstanceState) {

        View v = li.inflate(R.layout.fragment_podaci_o_uzadima, vg, false);

        //oznacavamo gde ce u layout-u da se dodaju fragmenti
        mRecyclerView = (RecyclerView) v.findViewById(R.id.f5_mesto_za_dodavanje_fragmenata);
        //mora preko setLayoutManager da se koristi RecyclerView
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        startAdaptera(true);

        return v;
    }

    public void startAdaptera(boolean isAnimating){
        List<PODACI_TipUzeta> lista = PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).getListuTipovaUzadi();
         if (BuildConfig.DEBUG)Log.e("TAG", "startAdaptera lista.size() = " + lista.size());
        if(mAdapter==null){
            mAdapter = new AdapterUzadi(lista);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setListuDogAdaptera(lista);
            mAdapter.notifyDataSetChanged();//sve proverava
        }
        updateSubtitle();
        animacijaZaRecycleView(mRecyclerView, isAnimating);

    }

    @Obfuscate//import
    private class HolderUzadi extends RecyclerView.ViewHolder
            implements View.OnClickListener{

        private TextView mNazivUzeta;
        private PODACI_TipUzeta tipUzeta;

        public HolderUzadi(View v){
            super(v);
            v.setOnClickListener(this);
            mNazivUzeta = (TextView) v.findViewById(R.id.f51_naziv_uzeta);
            mNazivUzeta.setAllCaps(false);
        }

        private void prenosPodatakaOdAdapteraKaHolderu (PODACI_TipUzeta tip){
            tipUzeta = tip;
            mNazivUzeta.setText(tipUzeta.getOznakaUzeta());
        }

        @Override
        public void onClick(View v){
            if ( mDaLiJeTablet ) {
                startAdaptera(false);
            }else{
                ColorDrawable[] color = {new ColorDrawable(v.getSolidColor()), new ColorDrawable(Color.GREEN)};
                TransitionDrawable trans = new TransitionDrawable(color);
                v.setBackground(trans);
                trans.startTransition(500);
                trans.reverseTransition(500);
            }

            mCallbacks.onTipSelected(tipUzeta);
        }
    }

    @Obfuscate//import
    private class AdapterUzadi extends RecyclerView.Adapter<HolderUzadi>{

        private List<PODACI_TipUzeta> lista;

        public AdapterUzadi(List<PODACI_TipUzeta> lista){
            this.lista = lista;
        }

        public void setListuDogAdaptera(List<PODACI_TipUzeta> lista){
            this.lista = lista;
        }

        @Override
        public HolderUzadi onCreateViewHolder(ViewGroup vg, int i){
            LayoutInflater li = LayoutInflater.from(getActivity());
            View v = li.inflate(R.layout.fragment_podaci_o_uzadima_adapter,vg,false);
            return new HolderUzadi(v);
        }
        @Override
        public void onBindViewHolder(HolderUzadi holder, int pozicija){
            PODACI_TipUzeta tip = lista.get(pozicija);
             if (BuildConfig.DEBUG)Log.e("TAGbazaUze", "onBindViewHolder tip = " + tip.getOznakaUzeta() + " cena="+ tip.getJcena());
            holder.prenosPodatakaOdAdapteraKaHolderu(tip);
        }
        @Override
        public int getItemCount(){
            return lista.size();
        }
    }


}
