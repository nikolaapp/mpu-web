package nsp.mpu._7_tipovi_uzadi;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu._0MainActivity;
import nsp.mpu.database.PODACI_ListaTipovaUzadi;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.PrikazNumTastature;

import static android.view.View.GONE;
import static android.widget.Toast.LENGTH_SHORT;

@Obfuscate
public class PodaciOUzadima_NovTip_Activity extends AppCompatActivity {

    private EditText mNaziv,  mPresek, mPrecnik, mTezina, mE, mI, mAlfa;
    private Button mSacuvaj;

    private String naziv="";
    private double presek, precnik, tezina, alfa;
    private int E,I;

    private PODACI_TipUzeta novTipUzeta;
    private String[] nizTipovaUzadi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nov_tip_uzeta);
        this.setTitle("ponisti");
        if ( _0MainActivity.mDaLiJeTablet )
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        nizTipovaUzadi = PODACI_ListaTipovaUzadi.getListaUzadi(getApplicationContext()).getNizNazivUzadi();


        mNaziv = (EditText) findViewById(R.id.f52_naziv_uzeta);
        mNaziv.requestFocus();
        mNaziv.setHint("Unesi oznaku novog uzeta");
        mNaziv.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE  naziv= " + cs);

                try {
                    naziv = cs.toString();
                } catch (NumberFormatException e) {
                    naziv = "";
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "naziv= " + naziv);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mPresek = (EditText) findViewById(R.id.f52_presek);
        mPresek.setKeyListener(new PrikazNumTastature(false,true));
        mPresek.setHint("Unesi presek [mm2]");
        mPresek.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE presek= " + cs);

                double pomocnaVar = 0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar==0||pomocnaVar>1000){
                    presek=0;
                    mPresek.setTextColor(Color.RED);
                }else{
                    presek = pomocnaVar;
                    mPresek.setTextColor(Color.BLACK);
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "presek= " + presek);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mPrecnik = (EditText) findViewById(R.id.f52_precnik);
        mPrecnik.setKeyListener(new PrikazNumTastature(false,true));
        mPrecnik.setHint("Unesi precnik [mm]");
        mPrecnik.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE precnik= " + cs);

                double pomocnaVar = 0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<1||pomocnaVar>100){
                    precnik=0;
                    mPrecnik.setTextColor(Color.RED);
                } else {
                    precnik = pomocnaVar;
                    mPrecnik.setTextColor(Color.BLACK);
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "precnik= " + precnik);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mTezina = (EditText) findViewById(R.id.f52_tezina);
        mTezina.setKeyListener(new PrikazNumTastature(false,true));
        mTezina.setHint("Unesi spec.tezinu [daN/m mm2]");
        mTezina.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE tezina= " + cs);

                double pomocnaVar = 0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar==0||pomocnaVar>0.1){
                    tezina=0;
                    mTezina.setTextColor(Color.RED);
                } else {
                    tezina=pomocnaVar;
                    mTezina.setTextColor(Color.BLACK);
                }
                 if (BuildConfig.DEBUG)Log.e("TAG", "tezina= " + tezina);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mE = (EditText) findViewById(R.id.f52_E);
        mE.setKeyListener(new PrikazNumTastature(false,false));
        mE.setHint("Unesi koef.E [daN/mm2]");
        mE.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE E= " + cs);

                int pomocnaVar = 0;
                try {
                    pomocnaVar = Integer.parseInt(cs.toString());
                } catch (NumberFormatException e) {
                }

                if (pomocnaVar < 1000 || pomocnaVar > 100_000) {    // :)
                    E = 0;
                    mE.setTextColor(Color.RED);
                } else{
                    E = pomocnaVar;
                    mE.setTextColor(Color.BLACK);
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "E= " + E);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });


        mI = (EditText) findViewById(R.id.f52_I);
        mI.setKeyListener(new PrikazNumTastature(false,false));
        mI.setHint("Unesi In [A]");
        mI.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE I= " + cs);

                int pomocnaVar = 0;
                try {
                    pomocnaVar = Integer.parseInt(cs.toString());
                } catch (NumberFormatException e) {
                }

                if (pomocnaVar < 1 || pomocnaVar > 2000) {    // :)
                    I = 0;
                    mI.setTextColor(Color.RED);
                } else{
                    I = pomocnaVar;
                    mI.setTextColor(Color.BLACK);
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "I= " + I);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mAlfa = (EditText) findViewById(R.id.f52_alfa);
        mAlfa.setKeyListener(new PrikazNumTastature(false,true));
        mAlfa.setHint("Unesi koef.alfa [1/C]");
        mAlfa.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE alfa= " + cs);

                double pomocnaVar = 0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar==0||pomocnaVar>0.0001){
                    alfa=0;
                    mAlfa.setTextColor(Color.RED);
                }else {
                    alfa = pomocnaVar;
                    mAlfa.setTextColor(Color.BLACK);
                }
                 if (BuildConfig.DEBUG)Log.e("TAG", "alfa= " + alfa);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mSacuvaj = (Button) findViewById(R.id.f52_sacuvaj_novo);
        mSacuvaj.setText("Sacuvaj nov tip");
        mSacuvaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean flag = true;

                 if (BuildConfig.DEBUG)Log.e("TAG", "naziv= " + naziv);

                for (int i = 0; i< nizTipovaUzadi.length; i++) {
                    if (naziv.equals(nizTipovaUzadi[i])) {
                        Toast.makeText(getApplicationContext(),
                                "Vec postoji tip sa istom oznakom!", LENGTH_SHORT).show();
                        flag = false;
                        break;
                    }
                }

                if (naziv.equals("") || presek == 0 || precnik == 0 || tezina == 0 || E == 0 || I == 0 || alfa == 0) {
                    Toast.makeText(getApplicationContext(),
                            "Proveri unete parametre!", LENGTH_SHORT).show();
                    flag = false;
                }

                if (flag){
                    novTipUzeta = new PODACI_TipUzeta(naziv, presek, precnik,tezina, E, alfa, I, true);
                    PODACI_ListaTipovaUzadi.getListaUzadi(getApplicationContext()).addTipUzeta(novTipUzeta);
                    finish();
                }
            }
        });

        mSacuvaj = (Button) findViewById(R.id.f52_sacuvaj_izmene);
        mSacuvaj.setVisibility(GONE);
    }
}
