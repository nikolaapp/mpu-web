package nsp.mpu._7_tipovi_uzadi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_ListaTipovaUzadi;
import nsp.mpu.database.PODACI_TipUzeta;

@Obfuscate
public class PodaciOUzadima_Pager_Activity extends AppCompatActivity
    implements PodaciOUzadima_Pager_Fragment.Komunikacija_Frag_Act{

    private ViewPager mPager;
    private List<PODACI_TipUzeta> mLista;
    public static final String TAGoznakaTipa = "TAG oznaka Tipa";


    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_pager_tip_uzeta);
        this.setTitle("nazad");

        String oznakaTipaUzeta = getIntent().getStringExtra(TAGoznakaTipa);

        mLista = PODACI_ListaTipovaUzadi.getListaUzadi(this).getListuTipovaUzadi();

        // isklj up  button - mora ovde ne moze u fragmentu
        //getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        mPager = (ViewPager) findViewById(R.id.f53_activity_pager);
        FragmentManager fm = getSupportFragmentManager();
        mPager.setAdapter(new FragmentStatePagerAdapter(fm) {
            @Override
            public Fragment getItem(int position) {
                PODACI_TipUzeta tipUzeta = mLista.get(position);
                return PodaciOUzadima_Pager_Fragment.noviFragmentZaPager(tipUzeta.getOznakaUzeta());
            }

            @Override
            public int getCount() {
                return mLista.size();
            }
        });

        //////tek na kraju odredjujes tacan index frag. koji prikazujes
        for (int i = 0; i < mLista.size(); i++) {
            if (mLista.get(i).getOznakaUzeta().equals(oznakaTipaUzeta)) {
                mPager.setCurrentItem(i);
                break;
            }
        }
    }

    // PodaciOUzadima_
    // Pager_Fragment mCallbacks = (Komunikacija_Frag_Act) act;
    // prijavljuje gresku bez ovoga
    // SVE ACTIVITY MORAJU DA IMPLEMENTIRAJU INTERFEJS (ako postoji) FRAGMENTA KOJI KORISTE
    @Override
    public void onTipUpdated(){
    }

    @Override
    public void indexPrikazanogTipa(int i) {
    }
}