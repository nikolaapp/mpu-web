package nsp.mpu._7_tipovi_uzadi;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.math.BigDecimal;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu._0MainActivity;
import nsp.mpu.database.PODACI_ListaTipovaUzadi;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.AnimacijeKlasa;
import nsp.mpu.pomocne_klase.PrikazNumTastature;

import static android.view.View.GONE;
import static android.widget.Toast.LENGTH_SHORT;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_IN;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_fragment_OUT;
import static nsp.mpu.pomocne_klase.StaticPodaci.mojHtmlString;

@Obfuscate
public class PodaciOUzadima_Pager_Fragment extends Fragment {

    private EditText mNaziv,  mPresek, mPrecnik, mTezina, mE, mAlfa, mI;
    private Button mSacuvajNovi, mSacuvajIzmene;

    private String naziv;
    private double presek, precnik, tezina, alfa;
    private int E,I;

    private PODACI_TipUzeta tipUzeta;
    private String[] nizTipovaUzadi;

    private static final String TAGoznakaTipa = PodaciOUzadima_Pager_Activity.TAGoznakaTipa;

    ///////////////////Komunikacija_Frag_Act 4.0.2//////////////
    private Komunikacija_Frag_Act mCallbacks;
    public interface Komunikacija_Frag_Act{
        void onTipUpdated();
        void indexPrikazanogTipa(int i);
    }
    @Override
    public void onAttach(Context c){
        super.onAttach(c);
        mCallbacks = (Komunikacija_Frag_Act) c;
    }
    @Override
    public void onDetach(){
        super.onDetach();
        mCallbacks = null;
    }
    ////////////////////////////////////////////////////////////

    public static PodaciOUzadima_Pager_Fragment noviFragmentZaPager(String s){
        Bundle args = new Bundle();
        args.putString(TAGoznakaTipa, s);

        PodaciOUzadima_Pager_Fragment f = new PodaciOUzadima_Pager_Fragment();
        f.setArguments(args);
        return f;
    }

    private void pokreniEdit(){
        if (tipUzeta.getIzmenljivo()){
            editTextMoguceIzmene(true);
            mNaziv.setEnabled(false);
            mSacuvajIzmene.setVisibility(View.VISIBLE);
            mSacuvajIzmene.setText("Sacuvaj izmene");
        } else {
            Toast.makeText(getActivity(),
                    "Nije dozvoljena izmena osnovnih tipova uzadi!", Toast.LENGTH_LONG).show();
        }
    }

    private void pokreniCopy(){
        editTextMoguceIzmene(true);
        mSacuvajNovi.setVisibility(View.VISIBLE);
        mNaziv.setText(tipUzeta.getOznakaUzeta()+"-c");
        mSacuvajNovi.setText("Sacuvaj kao nov tip");
    }

    private void pokreniDelete(){
        if (tipUzeta.getIzmenljivo()){

            AlertDialog ad = new AlertDialog.Builder(getContext()).create();
            ad.getWindow().setWindowAnimations(AnimacijeKlasa.odaberiAnimaciju_alertDialog());
            ad.setTitle("Upozorenje:");
            ad.setMessage(mojHtmlString("Sigurno zelite da obrisete <br><b>"+
                    tipUzeta.getOznakaUzeta() +"</b><br>iz baze podataka?"));
            ad.setButton(AlertDialog.BUTTON_NEGATIVE, "cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).deleteTipUzeta(tipUzeta);
                            if (_0MainActivity.mDaLiJeTablet){
                                mCallbacks.onTipUpdated();
                                // postavlja prazan frag umesto originala
                                getActivity().getSupportFragmentManager()
                                        .beginTransaction()
                                        .setCustomAnimations(
                                                odaberiAnimaciju_fragment_IN(),
                                                odaberiAnimaciju_fragment_OUT(),
                                                odaberiAnimaciju_fragment_IN(),
                                                odaberiAnimaciju_fragment_OUT()
                                        )
                                        .remove(PodaciOUzadima_Pager_Fragment.this)
                                        .commit();
                            } else
                                getActivity().finish();
                        }
                    });
            ad.show();

        } else
            Toast.makeText(getActivity(),
                    "Nije dozvoljeno brisanje osnovnih tipova uzadi!", Toast.LENGTH_LONG).show();
    }

    private void pokreniSend(){

        String s =
                "Oznaka uzeta: "+ tipUzeta.getOznakaUzeta()+
                "\n  presek [mm2] = "+ tipUzeta.getPresek()+
                "\n  precnik [mm] = "+ tipUzeta.getPrecnik()+
                "\n  tezina [daN/m*mm2] = "+ tipUzeta.getSpecTezina()+
                "\n  E [daN/mm2] = "+ tipUzeta.getModulElast()+
                "\n  I [A] = "+ tipUzeta.getI()+
                "\n  alfa [1/C] = "+ tipUzeta.getKoefAlfa()
                ;

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "Parametri uzeta");
        i.putExtra(Intent.EXTRA_TEXT, s);
        i=Intent.createChooser(i, "Odaberi nacin slanja:");
        startActivity(i);
    }

    @Override
    public void onCreateOptionsMenu(Menu m, MenuInflater i) {
        super.onCreateOptionsMenu(m, i);
        i.inflate(R.menu.meni_tip_uzeta, m);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit:
                pokreniEdit();
                return true;
            case R.id.menu_copy:
                pokreniCopy();
                return true;
            case R.id.menu_delete:
                pokreniDelete();
                return true;
            case R.id.menu_send:
                pokreniSend();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        String oznakaTipa = getArguments().getString(TAGoznakaTipa);
        tipUzeta = PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).getTipUzetaIzListe(oznakaTipa);
        nizTipovaUzadi = PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).getNizNazivUzadi();

        mCallbacks.indexPrikazanogTipa(PODACI_ListaTipovaUzadi.pozicijaUzetaUListi(oznakaTipa));
    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle savedInstanceState) {

        View v = li.inflate(R.layout.activity_nov_tip_uzeta, vg, false);

        if (_0MainActivity.mDaLiJeTablet)
            v.setBackgroundColor(Color.LTGRAY);

        mNaziv = (EditText) v.findViewById(R.id.f52_naziv_uzeta);
        mNaziv.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE  naziv= " + cs);

                try {
                    naziv = cs.toString();
                } catch (NumberFormatException e) {
                    naziv = "";
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "naziv= " + naziv);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mPresek = (EditText) v.findViewById(R.id.f52_presek);
        mPresek.setKeyListener(new PrikazNumTastature(false,true));
        mPresek.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE presek= " + cs);

                double pomocnaVar = 0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar==0||pomocnaVar>1000){
                    presek=0;
                    mPresek.setTextColor(Color.RED);
                }else{
                    presek = pomocnaVar;
                    mPresek.setTextColor(Color.BLACK);
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "presek= " + presek);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mPrecnik = (EditText) v.findViewById(R.id.f52_precnik);
        mPrecnik.setKeyListener(new PrikazNumTastature(false,true));
        mPrecnik.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE precnik= " + cs);

                double pomocnaVar = 0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar<1||pomocnaVar>100){
                    precnik=0;
                    mPrecnik.setTextColor(Color.RED);
                } else {
                    precnik = pomocnaVar;
                    mPrecnik.setTextColor(Color.BLACK);
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "precnik= " + precnik);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mTezina = (EditText) v.findViewById(R.id.f52_tezina);
        mTezina.setKeyListener(new PrikazNumTastature(false,true));
        mTezina.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE tezina= " + cs);

                double pomocnaVar = 0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar==0||pomocnaVar>0.1){
                    tezina=0;
                    mTezina.setTextColor(Color.RED);
                } else {
                    tezina=pomocnaVar;
                    mTezina.setTextColor(Color.BLACK);
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "tezina= " + tezina);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mE = (EditText) v.findViewById(R.id.f52_E);
        mE.setKeyListener(new PrikazNumTastature(false,false));
        mE.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE E= " + cs);

                int pomocnaVar = 0;
                try {
                    pomocnaVar = (int) Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar < 1000 || pomocnaVar > 100_000) {    // :)
                    E = 0;
                    mE.setTextColor(Color.RED);
                } else{
                    E = pomocnaVar;
                    mE.setTextColor(Color.BLACK);
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "E= " + E);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mI = (EditText) v.findViewById(R.id.f52_I);
        mI.setKeyListener(new PrikazNumTastature(false,false));
        mI.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE I= " + cs);

                int pomocnaVar = 0;
                try {
                    pomocnaVar = (int) Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar < 1 || pomocnaVar > 2000) {    // :)
                    I = 0;
                    mI.setTextColor(Color.RED);
                } else{
                    I = pomocnaVar;
                    mI.setTextColor(Color.BLACK);
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "I= " + I);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mAlfa = (EditText) v.findViewById(R.id.f52_alfa);
        mAlfa.setKeyListener(new PrikazNumTastature(false,true));
        mAlfa.addTextChangedListener(new TextWatcher() { // mora ovako
            @Override
            public void onTextChanged(CharSequence cs, int s, int b, int c) {
                 if (BuildConfig.DEBUG)Log.e("TAG", "PRE PARSE alfa= " + cs);

                double pomocnaVar = 0;
                try {
                    pomocnaVar = Double.parseDouble(cs.toString());
                } catch (NumberFormatException e) {}

                if (pomocnaVar==0||pomocnaVar>0.0001){
                    alfa=0;
                    mAlfa.setTextColor(Color.RED);
                }else {
                    alfa = pomocnaVar;
                    mAlfa.setTextColor(Color.BLACK);
                }

                 if (BuildConfig.DEBUG)Log.e("TAG", "alfa= " + alfa);
            }
            @Override
            public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override
            public void afterTextChanged(Editable e) {}
        });

        mSacuvajIzmene = (Button) v.findViewById(R.id.f52_sacuvaj_izmene);
        mSacuvajIzmene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (naziv.equals("") || presek == 0 || precnik == 0 || tezina == 0 || E == 0 || I == 0 || alfa == 0) {
                    Toast.makeText(getActivity(),
                            "Proveri unete parametre!", LENGTH_SHORT).show();
                }else{
                    tipUzeta = new PODACI_TipUzeta(naziv, presek, precnik,tezina, E, alfa, I, true);
                    PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).updateTipUzeta(tipUzeta);
                    editTextMoguceIzmene(false);
                }
            }
        });

        mSacuvajNovi = (Button) v.findViewById(R.id.f52_sacuvaj_novo);
        mSacuvajNovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean flag = true;

                 if (BuildConfig.DEBUG)Log.e("TAG", "naziv= " + naziv);

                for (int i = 0; i < nizTipovaUzadi.length; i++) {
                    if (naziv.equals(nizTipovaUzadi[i])) {
                        Toast.makeText(getActivity(),
                                "Vec postoji tip sa istom oznakom!", LENGTH_SHORT).show();
                        flag = false;
                        break;
                    }
                }

                if (naziv.equals("") || presek == 0 || precnik == 0 || tezina == 0 || E == 0 ||I == 0 || alfa == 0) {
                    Toast.makeText(getActivity(),
                            "Proveri unete parametre!", LENGTH_SHORT).show();
                    flag = false;
                }

                if (flag) {
                    PODACI_TipUzeta novTipUzeta = new PODACI_TipUzeta(naziv, presek, precnik, tezina, E, alfa,I, true);
                    PODACI_ListaTipovaUzadi.getListaUzadi(getActivity()).addTipUzeta(novTipUzeta);

                    //ako stoji .finish() onda bi se app srusila NA TABLETU
                    if (_0MainActivity.mDaLiJeTablet){
                        mCallbacks.onTipUpdated();
                        // postavlja nov frag umesto originala
                        getActivity().getSupportFragmentManager().
                                beginTransaction()
                                .setCustomAnimations(
                                        odaberiAnimaciju_fragment_IN(),
                                        odaberiAnimaciju_fragment_OUT(),
                                        odaberiAnimaciju_fragment_IN(),
                                        odaberiAnimaciju_fragment_OUT()
                                )
                                .replace(R.id.activity_prikaz_drugog_fragmenta,
                                        noviFragmentZaPager(novTipUzeta.getOznakaUzeta()))
                                .commit();
                    } else
                        getActivity().finish();
                }
            }
        });

        editTextMoguceIzmene(false);

        return v;
    }


    private void editTextMoguceIzmene(boolean flag){
        if (flag){
            if(_0MainActivity.mDaLiJeTablet) { // ako nije tablet onda ne moze da prisupa
                PodaciOUzadima_Fragment lista = (PodaciOUzadima_Fragment)
                        getActivity().getSupportFragmentManager().findFragmentById(R.id.activity_prikaz_fragmenta);
                lista.setHasOptionsMenu(false);
            }
            setHasOptionsMenu(false);
            getActivity().setTitle("ponisti");

            mNaziv.setEnabled(true);
            mNaziv.setText(tipUzeta.getOznakaUzeta());
            mPresek.setEnabled(true);
            mPresek.setText(String.valueOf(tipUzeta.getPresek()));
            mPrecnik.setEnabled(true);
            mPrecnik.setText(String.valueOf(tipUzeta.getPrecnik()));
            mTezina.setEnabled(true);
            mTezina.setText(String.valueOf(tipUzeta.getSpecTezina()));
            mE.setEnabled(true);
            mE.setText(String.valueOf(tipUzeta.getModulElast()));
            mI.setEnabled(true);
            mI.setText(String.valueOf(tipUzeta.getI()));
            mAlfa.setEnabled(true);
            mAlfa.setText(String.valueOf( // izbegavanje prikaza broja u exp formatu
                    BigDecimal.valueOf(tipUzeta.getKoefAlfa())));

            adapterSwipeIskljceno(true);

        }else{
            setHasOptionsMenu(true);

            mNaziv.setEnabled(false);
            mNaziv.setText(tipUzeta.getOznakaUzeta());
            mPresek.setEnabled(false);
            mPresek.setText("presek [mm²] = "+tipUzeta.getPresek());
            mPrecnik.setEnabled(false);
            mPrecnik.setText("prečnik [mm] = "+String.valueOf(tipUzeta.getPrecnik()));
            mTezina.setEnabled(false);
            mTezina.setText("težina[daN/m∙mm²]="+String.valueOf(tipUzeta.getSpecTezina()));
            mE.setEnabled(false);
            mE.setText("E [daN/mm²] = "+String.valueOf(tipUzeta.getModulElast()));
            mI.setEnabled(false);
            mI.setText("In [A] = "+tipUzeta.getI());
            mAlfa.setEnabled(false);
            mAlfa.setText("alfa [1/°C] = "+String.valueOf(tipUzeta.getKoefAlfa()));
            mSacuvajIzmene.setVisibility(GONE);
            mSacuvajNovi.setVisibility(GONE);

            // jer zbog setovanja "presek = "+Str... boje se u crveno
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mPresek.setTextColor(getResources().getColor(R.color.disabled_color,null));
                mPrecnik.setTextColor(getResources().getColor(R.color.disabled_color,null));
                mTezina.setTextColor(getResources().getColor(R.color.disabled_color,null));
                mE.setTextColor(getResources().getColor(R.color.disabled_color,null));
                mI.setTextColor(getResources().getColor(R.color.disabled_color,null));
                mAlfa.setTextColor(getResources().getColor(R.color.disabled_color,null));
            }else{
                mPresek.setTextColor(getResources().getColor(R.color.disabled_color));
                mPrecnik.setTextColor(getResources().getColor(R.color.disabled_color));
                mTezina.setTextColor(getResources().getColor(R.color.disabled_color));
                mE.setTextColor(getResources().getColor(R.color.disabled_color));
                mI.setTextColor(getResources().getColor(R.color.disabled_color));
                mAlfa.setTextColor(getResources().getColor(R.color.disabled_color));
            }


            adapterSwipeIskljceno(false);
        }
    }

    private void adapterSwipeIskljceno(boolean ukljuciUsporenSwipe){
        // http://stackoverflow.com/a/13473683
        ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.f53_activity_pager);
        if (viewPager!=null) { // jeste null kada je prikaz na tabletu
            if (ukljuciUsporenSwipe) {
                viewPager.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View arg0, MotionEvent arg1) {
                        return true;
                    }
                });
            } else
                viewPager.setOnTouchListener(null);
        }
    }
}
