package nsp.mpu._8_temp_provodnika;

import static java.lang.Math.pow;

/**
 * Created by nikola.s.pavlovic on 23.4.2018.
 */

public abstract class ConvectiveHeatLoss_natural {

    /**
     * Pc_natural convective heat loss when wind is zero
     * @param lf thermal conductivity of the air
     * @param Tc temperature of conductor
     * @param Ta temperature of air
     * @param Nu Nusselt number
     * @return Pc_natural [W/m]
     */
    public static double convectiveHeatLoss_natural(double lf, double Tc, double Ta, double Nu){
        double pi = 3.14;
        return pi * lf * (Tc-Ta) * Nu;
    }

    /**
     * Nu Nusselt number in the case of zero wind
     * @param A coefficient A
     * @param Gr dimensionless Grashof number
     * @param Pr dimensionless Prandtl number
     * @param m coefficient A
     * @param beta Inclination of a conductor to the horizontal
     * @return Nu[noDim]
     */
    public static double nusseltNumber_natural(double A, double Gr, double Pr, double m, double beta){
        double Nu_beta0 = A*pow(Gr*Pr,m);
        return Nu_beta0 * (1 - 6.76*pow(10,-6)*pow(beta,2.5));
    }

    /**
     * koef A from eq -> Nu = A * (Gr*Pr)^m
     * @param Gr Grashof number
     * @param Pr Prandtl number
     * @return A[noDim]
     */
    public static double getKoef_A(double Gr, double Pr){
        if (Gr*Pr<=100)
            return 1.02;
        else if (Gr*Pr<=pow(10,4))
            return 0.85;
        else if (Gr*Pr<=pow(10,7))
            return 0.48;
        else
            return 0.125;
    }

    /**
     * koef m from eq -> Nu = A * (Gr*Pr)^m
     * @param Gr Grashof number
     * @param Pr Prandtl number
     * @return m[noDim]
     */
    public static double getKoef_m(double Gr, double Pr){
        if (Gr*Pr<=100)
            return 0.148;
        else if (Gr*Pr<=pow(10,4))
            return 0.188;
        else if (Gr*Pr<=pow(10,7))
            return 0.25;
        else
            return 0.333;
    }

    /**
     * Gr  Grashof Number
     * @param D diameter
     * @param Tc temperature of conducture
     * @param Ta temperature of air
     * @param Tf the temperature of the film of air in contact with the surface
     * @param vf kinematic viscosity
     * @return Gr[noDim]
     */
    public static double grashofNumber(double D, double Tc, double Ta,
                                       double Tf, double vf){
        double g = 9.807;
        double up = pow(D,3) * (Tc-Ta) * g;
        double down = (Tf+273)*vf*vf;
        return up/down;
    }

    /**
     * Pr Prandtl Number
     * @param uf dinamicViscosity
     * @param lf thermal conductivity of the air
     * @return Pr[noDim]
     */
    public static double prandtlNumber(double uf, double lf){
        double cf = 1005; // air's specific heat [ J/(kg·K) ]
        return cf*uf/lf;
    }

}
