package nsp.mpu._8_temp_provodnika;

import static java.lang.Math.pow;

/**
 * Created by nikola.s.pavlovic on 23.4.2018.
 */

public abstract class ConvectiveHeatLoss_wind {

    /**
     * the temperature of the film of air in contact with the surface
     * @param Ts temperature of the conductor surface
     * @param Ta temperatures of the air
     * @return mean value [degC]
     */
    public static double filmTemperature(double Ts, double Ta){
        return (Ts+Ta)/2.0;
    }

    /**
     * lc the thermal conductivity of the air at Tf
     * @param Tf the temperature of the film of air in contact with the surface
     * @return [W/(K*m)]
     */
    public static double thermalConductivity(double Tf){
        return 2.368*pow(10,-2) + 7.23*pow(10,-5)*Tf - 2.763*pow(10,-8)*Tf*Tf;
    }

    /**
     * uf dynamic viscosity of the air at the film temperature
     * @param Tf film temperature
     * @return uf [kg / m*s]
     */
    public static double dynamicViscosity(double Tf){
        return (17.239 + 4.635*pow(10,-2)*Tf - 2.03*pow(10,-5)*Tf*Tf ) * pow(10,-6);
    }

    /**
     * vf the kinematic viscosity of the air at the film temperature,
     * at elevation of the conductor above sea level
     * @param uf dynamicViscosity
     * @param Tf film temperature
     * @param heigh elevation of the conductor above sea level
     * @return vf [m2 / s]
     */
    public static double kinematicViscosity(double uf, double Tf, double heigh){
                double airDensity; /** [kg / m3] */
        airDensity = (1.293 - 1.525*pow(10,-4)*heigh + 6.379*pow(10,-9)*heigh*heigh )
                / ( 1 + 0.00367*Tf);

        return uf/airDensity;
    }

    /**
     * Rs roughness of the surface of the conductor
     * @param D is the overall diameter
     * @param d is the diameter of the wires in the outermost layer
     * @return Rs[noDim]
     */
    public static double roughnessOfSurface(double D, double d){
        return d / ( 2*(D-d));
    }

    /**
     * Re Reynolds number
     * @param V is the wind speed (m/s),
     * @param D is the diameter of the conductor (m),
     * @param vf is the kinematic viscosity (m2/s) of the air
     * @return Re [noDim]
     */
    public static double reynoldsNumber(double V, double D, double vf){
        return V*D/vf;
    }

    /**
     * koef B from eq -> Nu = B * Re^n
     * @param Rs roughness of the surface of the conductor
     * @param Re Reynolds number
     * @return B[noDim]
     */
    public static double getKoef_B(double Rs, double Re){
        if (Rs<=0.05){
            if (Re<=2650)   return 0.641;
            else            return 0.178;
        }
        else{
            if (Re<=2650)   return 0.641;
            else            return 0.048;
        }
    }

    /**
     * koef n from eq -> Nu = B * Re^n
     * @param Rs roughness of the surface of the conductor
     * @param Re Reynolds number
     * @return n[noDim]
     */
    public static double getKoef_n(double Rs, double Re){
        if (Rs<=0.05){
            if (Re<=2650)   return 0.471;
            else            return 0.633;
        }
        else{
            if (Re<=2650)   return 0.471;
            else            return 0.800;
        }
    }

    /**
     * Nu Nusselt number
     * @param B eq parametar
     * @param n eq parametar
     * @param Re Reynolds number
     * @param windAngle is the angle between wind and line direction
     * @return Nu [noDim]
     */
    public static double nusseltNumber(double B, double Re, double n, double windAngle){
        double N_90deg = B * pow(Re,n);
        if (windAngle <= 24)
            return N_90deg * ( 0.42 + 0.68*pow(TemperaturaProvodnikaActivity. sin(windAngle),1.08));
        else
            return N_90deg * ( 0.42 + 0.58*pow(TemperaturaProvodnikaActivity. sin(windAngle),0.9));
    }

    /**
     * Pc_wind convective heat loss expressed as a function of the dimensionless Nusselt number
     * @param lf is the thermal conductivity of the air
     * @param Ts the temperature of the conductor surface
     * @param Ta the temperature of the air
     * @param Nu Nusselt number
     * @return Pc_wind [W/m]
     */
    public static double convectiveHeatLoss_wind(double lf, double Ts, double Ta, double Nu){
        double pi = 3.14;
        return pi * lf * (Ts - Ta) * Nu;
    }


}
