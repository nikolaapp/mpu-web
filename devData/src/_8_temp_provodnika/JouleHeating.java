package nsp.mpu._8_temp_provodnika;


import static java.lang.Math.pow;

/**
 * Created by nikola.s.pavlovic on 23.4.2018.
 */

public abstract class JouleHeating {

    /**
     * Pj Joule heating per unit length of the conductor (W/m)
     * @param I is the total current (A)
     * @param R is the current resistance per unit length (ohm/m)
     * @return Pj [W/m]
     */
    public static double jouleHeating( double I, double R) {
        return R*I*I;
    }

    /**
     * Rac(T) interpolated value of ac resistance for specific temperature T
     * @param T new temperature
     * @return R[ohm/m]
     */
    public static double acResistance(double T){
        double T1, T2, R1, R2; /** za interpolaziju R pri zadatoj T  */
        T1=25;
        T2=75;
        R1=7.283*pow(10,-5);
        R2=8.688*pow(10,-5);
        return R1 + (T-T1)*( (R1-R2) / (T1-T2) );
    }


    /**
     * alfa_20C = 0.00403 [1/°C]
     * temperature coeficient of resistance variation
     * SRPS EN 60889:2010 */
    private static final double alfa_20C = 0.00403;

    /**
     * alfa_20C = 8x10-7 [1/(°C)²]
     * temperature coeficient of resistance variation
     * SRPS EN 60889:2010 */
    private static final double eps_20C = 8*Math.pow(10,-7);

    /** ro_20C = 0.028264 [Ω∙mm²/m]
     * resistivity
     * SRPS EN 60889:2010 */
    private static final double ro_20C = 0.028264;

    /**
     * Rac(T) - ac resistance for specific temperature T
     * @param T is conductor temperature [°C]
     * @param S is conductor cross section [mm²]
     * @return R[Ω/m]
     */
    public static double acResistance(double T, double S){
        double dT = T -20;
        S = 0.86*S; /** actualy, we need cross section from Al part of ACSR */
        return (ro_20C/S) * ( 1 + alfa_20C*dT + eps_20C*dT*dT );
    }
}
