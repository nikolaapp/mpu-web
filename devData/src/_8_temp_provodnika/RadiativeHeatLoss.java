package nsp.mpu._8_temp_provodnika;

import static java.lang.Math.pow;

/**
 * Created by nikola.s.pavlovic on 23.4.2018.
 */

public abstract class RadiativeHeatLoss {

    /**
     * Pr radiative heat loss
     * @param D diametar
     * @param es is the emissivity of the conductor surface
     * @param Tc temperature of conductor
     * @param Ta temperature of air
     * @return Pr [W/m]
     */
    public static double radiativeHeatLoss(double D, double es, double Ta, double Tc){
        double stef_boltz = 5.6697*pow(10,-8);
        double pi = 3.14;
        double pom = pow(Tc+273, 4) - pow(Ta+273, 4);
        return pi * stef_boltz * D * es * pom;
    }


}
