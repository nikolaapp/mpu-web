package nsp.mpu._8_temp_provodnika;

import static java.lang.Math.pow;
import static nsp.mpu._8_temp_provodnika.TemperaturaProvodnikaActivity.acos;
import static nsp.mpu._8_temp_provodnika.TemperaturaProvodnikaActivity.asin;
import static nsp.mpu._8_temp_provodnika.TemperaturaProvodnikaActivity.cos;
import static nsp.mpu._8_temp_provodnika.TemperaturaProvodnikaActivity.sin;

/**
 * Created by nikola.s.pavlovic on 23.4.2018.
 */

public abstract class SolarPowerReceived {

    /**
     * The formula for the total solar power received per unit length of the conductor (W/m)
     * @return power [W/m]
     */
    public static double solarPowerReceived( double D, double koef_as,
                                             double Ib,double Id, double n,
                                            double F, double Hs) {

        final double pi = 3.14;
        double It = Ib * (sin(n)  +  F*pi/2 * sin(Hs)) +
                Id * (1 + F*pi/2);

        double Ps = koef_as * D * It;


        return Ps;
    }

    /**
     * The declination of the sun can be defined as the angle between the equator and a line
     * drawn from the centre of the earth to the centre of the sun.
     *
     * @param N day of year (1st January = 1)
     * @return declination in [deg]
     */
    public static double declination(int N) {
        double pi = 180;
        return 23.3 * sin(2 * pi * (284.0 + N)/365 );
    }

    /**
     * Hs is the solar altitude
     *
     * @param lat       latitude in deg - 43deg for Kraljevo
     * @param dec       The declination of the sun
     * @param hourAngle hour angle of the Sun
     * @return solar altitude [deg]
     */
    public static double solarAltitude(double lat, double dec, double hourAngle) {
//        double a1=sin(lat);
//        double a2=sin(dec);
//        double a3=cos(lat);
//        double a4=cos(dec);
//        if (hourAngle<-90)
//            hourAngle = -90;
//        else if (hourAngle>90)
//            hourAngle = 90;
//        double a5=cos(hourAngle);
//        double aa = asin(a1*a2+a3*a4*a5);
        return asin(sin(lat) * sin(dec)
                + cos(lat) * cos(dec) * cos(hourAngle));
    }

    /**
     * hour angle of the Sun in degrees, with given in hours
     *
     * @param hour time given in hours, from 0 to 24
     * @return hour angle of the Sun [deg]
     */
    public static double hourAngleSun(int hour) {
        return 15 * (12 - hour);
    }

    /**
     * An equation to calculate the direct solar radiation at sea level
     * @param Ns is a clearness ratio
     *           having the value of 1.0 for the standard atmosphere,
     *           0.8 to 1.2 for clear skies with decreasing amounts of dust and aerosols,
     *           0.5 for an industrial atmosphere
     *           and less than 0.5 for a cloudy or overcast sky.
     *           0 with thick cloud
     * @param Hs is the solar altitude
     * @return direct solar radiation at sea level [W/m2]
     */
    public static double directBeamRadiation(double Ns, double Hs) {
        return Ns * 1280*sin(Hs) / (sin(Hs)+0.314);
    }

    /**
     * The direct beam radiation increases with increasing height above sea level,
     * according to the following equation     *
     * @param Ib     The direct beam radiation at see lavel
     * @param height see lavel in m
     * @return Ib(height) [W/m2]
     */
    public static double directBeamRadiation_overSeeLavel(double Ib, double height) {
        if (Ib == 0) // ako je potpuno oblacno onda je Ib = 0
            return 0;
        return Ib * (1 + 1.4*pow(10, -4) * height * (1367 / Ib - 1));
    }

    /**
     * diffuse solar radiation intensity for all skies
     * @param Ib direct (beam) solar radiation intensity
     * @param Hs is the solar altitude
     * @return diffuse solar radiation intensity [W/m2]
     */
    public static double diffuseBeamRadiation(double Ib, double Hs) {
        return (430.5-0.3288*Ib)*sin(Hs);
    }

    /**
     * azimuthOfSun
     * @param dec declination of the sun
     * @param Z hour angle of the Sun
     * @param Hs is the solar altitude
     * @return azimuthOfSun [deg]
     */
    public static double azimuthOfSun(double dec, double Z, double Hs) {
        return asin( cos(dec)*sin(Z) / cos(Hs) );
    }

    /**
     * angle of solar beam with respect to the axis of the conductor
     * @param Hs is the solar altitude
     * @param azimuthSun azimuth of the Sun
     * @param azimuthCond azimuth of the conductor
     * @return angle of solar beam [deg]
     */
    public static double angleOfSolarBeam(double Hs, double azimuthSun, double azimuthCond) {
        // if (BuildConfig.DEBUG)Log.e("tag","cos(Hs)=" +cos(Hs));
        // if (BuildConfig.DEBUG)Log.e("tag","cos(azimuthSun-azimuthCond)=" +cos(azimuthSun-azimuthCond));
        return acos( cos(Hs) * cos(azimuthSun-azimuthCond) );
    }

    /**
     * The albedo (F) is reflectance of the ground, approximately:
     *      0.05 for a water surface,
     *      0.1 for forests,
     *      0.15 for urban areas,
     *      0.2 for soil, grass and crops,
     *      0.3 for sand,
     *      0.4 to 0.6 for ice
     *      and 0.6 to 0.8 for snow
     * @return albedo value
     */
    public static double getAlbedo() {
        return Integer.MIN_VALUE;
    }
}
