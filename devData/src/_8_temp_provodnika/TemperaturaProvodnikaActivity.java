package nsp.mpu._8_temp_provodnika;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Locale;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;
import nsp.mpu.database.PODACI_TipUzeta;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static java.lang.Math.abs;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;
import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_natural.convectiveHeatLoss_natural;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_natural.getKoef_A;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_natural.getKoef_m;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_natural.grashofNumber;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_natural.nusseltNumber_natural;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_natural.prandtlNumber;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_wind.convectiveHeatLoss_wind;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_wind.dynamicViscosity;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_wind.filmTemperature;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_wind.getKoef_B;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_wind.getKoef_n;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_wind.kinematicViscosity;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_wind.nusseltNumber;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_wind.reynoldsNumber;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_wind.roughnessOfSurface;
import static nsp.mpu._8_temp_provodnika.ConvectiveHeatLoss_wind.thermalConductivity;
import static nsp.mpu._8_temp_provodnika.JouleHeating.acResistance;
import static nsp.mpu._8_temp_provodnika.JouleHeating.jouleHeating;
import static nsp.mpu._8_temp_provodnika.RadiativeHeatLoss.radiativeHeatLoss;
import static nsp.mpu._8_temp_provodnika.SolarPowerReceived.angleOfSolarBeam;
import static nsp.mpu._8_temp_provodnika.SolarPowerReceived.azimuthOfSun;
import static nsp.mpu._8_temp_provodnika.SolarPowerReceived.declination;
import static nsp.mpu._8_temp_provodnika.SolarPowerReceived.diffuseBeamRadiation;
import static nsp.mpu._8_temp_provodnika.SolarPowerReceived.directBeamRadiation;
import static nsp.mpu._8_temp_provodnika.SolarPowerReceived.directBeamRadiation_overSeeLavel;
import static nsp.mpu._8_temp_provodnika.SolarPowerReceived.hourAngleSun;
import static nsp.mpu._8_temp_provodnika.SolarPowerReceived.solarAltitude;
import static nsp.mpu._8_temp_provodnika.SolarPowerReceived.solarPowerReceived;
import static nsp.mpu.database.PODACI_ListaTipovaUzadi.getListaUzadi;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.animirajTekst_running;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_alertDialog;
import static nsp.mpu.pomocne_klase.StaticPodaci.getIndexElementaUNizu;
import static nsp.mpu.pomocne_klase.StaticPodaci.getNizNaponskiNivo;
import static nsp.mpu.pomocne_klase.StaticPodaci.getSharPref;
import static nsp.mpu.pomocne_klase.StaticPodaci.mojHtmlString;
import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;
import static nsp.mpu.pomocne_klase.StaticPodaci.temperaturaProv_min;
import static nsp.mpu.pomocne_klase.StaticPodaci.tipUzeta_default;
@Obfuscate
public class TemperaturaProvodnikaActivity extends AppCompatActivity {

    // ukljuci kada hoces da uporedis sa primerom iz knjige
    private final static boolean bool_provera = false;

    public final static int S_drake = 468;  //conductor "Drake" cross section

    private PODACI_TipUzeta tipUzeta_def;
    private final int I_def = 100;
    private final double wind_def = 0.61;
    private final int visina_def = 500;
    private final double albedo_def = 0.15;
    private final double oblacnost_def = 1.0;



    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        super.onCreateOptionsMenu(m);
        MenuInflater findMenuItems = getMenuInflater();
        findMenuItems.inflate(R.menu.meni_temp_prov, m);
        //MenuItem info = m.findItem(R.id.menu_info);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog ad = new AlertDialog.Builder(activity).create();
        ad.getWindow().setWindowAnimations(odaberiAnimaciju_alertDialog());
        //ad.setTitle(mojHtmlString("<i>info"));
        String s="Represents: ";
        switch (item.getItemId()) {
            case R.id.menu_osnovni_parametri:
                ad.setTitle(getResources().getString(R.string.osnovni_parametri));
                s += "basic parametars for calculation:"+
                        "<br>- type of overhead stranded conductor" +
                        "<br>- ambient temperature at [°C]"+
                        "<br>- date and hour at which parameters are acquired";
                break;

            case R.id.menu_opterecenje:
                ad.setTitle(getResources().getString(R.string.opterecenje));
                s += "steady state current ampacity [A]";
                s += "<br><br><i>default value: "+I_def;
                break;

            case R.id.menu_brzina_vetra:
                ad.setTitle(getResources().getString(R.string.brzina_vetra_m_s));
                s += "wind speed [m/s] for observed section of line";
                s += "<br><br><i>default value: "+wind_def;
                break;
            case R.id.menu_nad_visina:
                ad.setTitle(getResources().getString(R.string.nadmorska_visina_m));
                s += "height above sea level [m]";
                s += "<br><br><i>default value: "+visina_def;
                break;

            case R.id.menu_koef_albedo:
                ad.setTitle(getResources().getString(R.string.koef_refleksije));
                s += "reflectance of the ground (albedo) [dimensionless]: " +
                        "<br>- 0.05 for a water" +
                        "<br>- 0.1 for forests" +
                        "<br>- 0.15 for urban areas" +
                        "<br>- 0.2 for soil, grass and crops" +
                        "<br>- 0.3 for sand" +
                        "<br>- 0.4 to 0.6 for ice" +
                        "<br>- 0.6 to 0.8 for snow";
                s += "<br><br><i>default value: "+albedo_def;
                break;

            case R.id.menu_koef_oblacnosti:
                ad.setTitle(getResources().getString(R.string.koef_oblacnosti));
                s += "atmosphere clearness ratio [dimensionless]: " +
                        "<br>- 0 for thick cloud" +
                        "<br>- 0÷0.5 for a cloudy or overcast sky" +
                        "<br>- 0.5 for an industrial atmosphere" +
                        "<br>- 0.8÷1.2 for clear skies with decreasing amounts of dust and aerosols" +
                        "<br>- 1.0 for the standard atmosphere";
                s += "<br><br><i>default value: "+oblacnost_def;
                break;

            case R.id.menu_pre_defined:
                ad.setTitle(getResources().getString(R.string.predefinisane_vrednosti));
                s = "Pre-defined values: " +
                        "<br>- Wind angle of attack: " + windAngle+"°"+
                        "<br>- Inclination of a conductor to the horizontal: " + beta+"°"+
                        "<br>- Azimuth of line: " + azimuthCond+"°"+
                        "<br>- Latitude: " + latitude+"°"+
                        "<br>- Solar absorptivity of conductor: " + koef_as+
                        "<br>- Emissivity of conductor: " + es+
                        "<br>- Outer strand diameter: " + d*1000+"mm";
                break;

            default:
                return super.onOptionsItemSelected(item);
        }

        ad.setMessage(mojHtmlString(s));
        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}
                });
        ad.show();

        ( (TextView) ad.findViewById(android.R.id.message))
                .setTextIsSelectable(true);

        ad.getWindow().setBackgroundDrawableResource(android.R.color.darker_gray);


        return false;
    }

    private Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        setContentView(R.layout.activity_temp_provodnika);
        //setTitle("ver "+ BuildConfig.VERSION_NAME);

        if ( mDaLiJeTablet)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        else
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        I = getSharPref().getInt("I", I_def);
        wind = getSharPref().getFloat("wind", (float) wind_def);
        nadmVisina = getSharPref().getInt("nadmVisina", visina_def);
        albedo = getSharPref().getFloat("albedo", (float) albedo_def);
        oblacnost = getSharPref().getFloat("oblacnost", (float) oblacnost_def);

        timeDan = getSharPref().getInt("timeDan", 100);
        timeSat = getSharPref().getInt("timeSat", 12);
        Ta = getSharPref().getInt("Ta", 20);
        Tc = Ta + 30;

        tipUzeta_def = getListaUzadi(activity).getTipUzetaIzListe(tipUzeta_default);
        tipUzeta = getListaUzadi(activity).getTipUzetaIzListe(tipUzeta_default);

        D = tipUzeta_def.getPrecnik()*0.001;
        S = tipUzeta_def.getPresek();

        if (bool_provera){
            // vrednosti za proveru, prema primeru A
            es=koef_as = 0.8;
            latitude = 30;
            azimuthCond = 90;
            d=0.00444;
            windAngle=60;
            beta=0;

            D = 0.0281;
            S = S_drake;

        }
        else {
            //vrednosti za realnu upotrebu
            es=koef_as = 0.85;
            latitude = 44;
            azimuthCond = 45;
            d=nadji_d(S);
            windAngle=45;
            beta=5;

        }

        findViews();

        listeners();

        resiJed(); // resava jednacinu i prikazuje rez
    }

    private TextView tv_day, tv_hour, tv_I;
    private EditText mI;
    private NumberPicker mTipUzeta, mTempAmbijenta;
    SeekBar mSeekBarDay, mSeekBarHour;
    EditText mVetar, mVisina, mAlbedo, mOblacnost;
    private void findViews(){
        mI = (EditText) findViewById(R.id.et_I);
        tv_day = (TextView) findViewById(R.id.textview_day);
        tv_hour = (TextView) findViewById(R.id.textview_hour);
        tv_I = (TextView) findViewById(R.id.textview_current);
        mTipUzeta = (NumberPicker) findViewById(R.id.num_picker_uze);
        mTempAmbijenta = (NumberPicker) findViewById(R.id.num_picker_temp);
        findViewById(R.id.num_picker_odo).setVisibility(View.GONE);
        findViewById(R.id.text_xodo).setVisibility(View.GONE);
        mVetar      = (EditText) findViewById(R.id.et_wind);
        mVisina     = (EditText) findViewById(R.id.et_nad_visina);
        mAlbedo     = (EditText) findViewById(R.id.et_albedo);
        mOblacnost  = (EditText) findViewById(R.id.et_oblacnost);
        mSeekBarHour = (SeekBar) findViewById(R.id.seekBar_hour);
        mSeekBarDay = (SeekBar) findViewById(R.id.seekBar_day);


        mI        .setHint(I_def+"");
        mVetar    .setHint(wind_def+"");
        mVisina   .setHint(visina_def+"");
        mAlbedo   .setHint(albedo_def+"");
        mOblacnost.setHint(oblacnost_def+"");

        mI        .setText(I+"");
        mVetar    .setText(wind+"");
        mVisina   .setText(nadmVisina +"");
        mAlbedo   .setText(albedo+"");
        mOblacnost.setText(oblacnost+"");

        mSeekBarDay .setProgress(timeDan);
        mSeekBarHour.setProgress(timeSat);

        Calendar kalendar = Calendar.getInstance();
        kalendar.set(Calendar.DAY_OF_YEAR,  timeDan);
        tv_day.setText(kalendar.get(Calendar.DAY_OF_MONTH)+"."+(kalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())));

        tv_hour.setText(timeSat+"h");

        animirajTekst_running(activity,(TextView) findViewById(R.id.text_stepeni),"vazduh");

    }
    PODACI_TipUzeta tipUzeta;
    private void listeners(){

        final String [] nizOznakaUzadi = getListaUzadi(activity).getNizNazivUzadi();
        mTipUzeta.setDisplayedValues(nizOznakaUzadi);
        mTipUzeta.setMinValue(0);
        mTipUzeta.setMaxValue(nizOznakaUzadi.length-1);
        mTipUzeta.setWrapSelectorWheel(true);
        mTipUzeta.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mTipUzeta.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                tipUzeta = getListaUzadi(activity).getTipUzetaIzListe(nizOznakaUzadi[newVal]);
                S=tipUzeta.getPresek();
                D=tipUzeta.getPrecnik()*0.001;
                d=nadji_d(S);
                resiJed();
            }
        });
        mTipUzeta.setValue(getListaUzadi(activity).pozicijaUzetaUListi(tipUzeta_default));


        mTempAmbijenta.setMinValue(0);
        mTempAmbijenta.setMaxValue(100); // 100-40 =60 - temp vazduha
        mTempAmbijenta.setWrapSelectorWheel(true);
        mTempAmbijenta.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mTempAmbijenta.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int i) {
                if (i == Math.abs(temperaturaProv_min)) return "0";
                else return String.valueOf(i + temperaturaProv_min);
            }
        });
        mTempAmbijenta.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                Ta=newVal+ temperaturaProv_min;
                getSharPref().edit().putInt( "Ta", Ta) .apply();
                 if (BuildConfig.DEBUG)Log.e("TAGtemp","Ta= " + Ta);
                resiJed();
            }
        });
        mTempAmbijenta.setValue(Ta+40);


        mSeekBarDay.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                timeDan=i+1;
                getSharPref().edit().putInt( "timeDan", timeDan) .apply();
                Calendar kalendar = Calendar.getInstance();
                kalendar.set(Calendar.DAY_OF_YEAR,  timeDan);
                tv_day.setText(kalendar.get(Calendar.DAY_OF_MONTH)+"."+(kalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())));
                resiJed();
            }
            @Override public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        mSeekBarHour.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                timeSat = i;
                getSharPref().edit().putInt( "timeSat", timeSat) .apply();
                tv_hour.setText(timeSat+"h");
                resiJed();
            }
            @Override public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override public void onStopTrackingTouch(SeekBar seekBar) {}
        });


        tv_I.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                AlertDialog ad = new AlertDialog.Builder(activity).create();
                ad.getWindow().setWindowAnimations(odaberiAnimaciju_alertDialog());
                ad.setTitle(mojHtmlString("<i>odredi I[A]:"));
                LayoutInflater inflater = activity.getLayoutInflater();
                View v = inflater.inflate(R.layout.dialog_procenat_struja, null);
                ad.setView(v);

                Spinner mSpinnerNapon = (Spinner) v.findViewById(R.id.spiner_naponi);
                SeekBar mSeekBarI = (SeekBar) v.findViewById(R.id.seekBar_I);
                TextView tv_procenat = (TextView) v.findViewById(R.id.textview_procenat);
                final TextView tv_struja = (TextView) v.findViewById(R.id.textview_struja);

                dialog_views(mSpinnerNapon, mSeekBarI, tv_procenat,tv_struja);


                ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String s = String.format("%.0f",racunajI(tv_struja,procenat,U));
                                mI.setText(s);
                                mI.requestFocus();
                                resiJed();
                            }
                        });
                ad.setButton(AlertDialog.BUTTON_NEGATIVE, "Odustani",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {}
                        });
                ad.show();

                ad.getWindow().setBackgroundDrawableResource(android.R.color.darker_gray);

            }
        });

        mI.addTextChangedListener(new TextWatcher() {
            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    I = Integer.parseInt(charSequence.toString());
                } catch (NumberFormatException e) {
                    I=100;
                }
                getSharPref().edit().putInt( "I", I) .apply();

            }
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override public void afterTextChanged(Editable editable) {}
        });
        focusListener(mI);

        mVetar.addTextChangedListener(new TextWatcher() {
            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    wind = Double.parseDouble(charSequence.toString());
                } catch (NumberFormatException e) {
                    wind=wind_def;
                }
                getSharPref().edit().putFloat( "wind", (float)wind) .apply();
            }
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override public void afterTextChanged(Editable editable) {}
        });
        focusListener(mVetar);

        mVisina.addTextChangedListener(new TextWatcher() {
            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    nadmVisina = Integer.parseInt(charSequence.toString());
                } catch (NumberFormatException e) {
                    nadmVisina =visina_def;
                }
                getSharPref().edit().putInt( "nadmVisina", nadmVisina) .apply();
            }
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override public void afterTextChanged(Editable editable) {}
        });
        focusListener(mVisina);

        mAlbedo.addTextChangedListener(new TextWatcher() {
            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    albedo = Double.parseDouble(charSequence.toString());
                } catch (NumberFormatException e) {
                    albedo=albedo_def;
                }
                getSharPref().edit().putFloat( "albedo", (float)albedo) .apply();
            }
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override public void afterTextChanged(Editable editable) {}
        });
        focusListener(mAlbedo);

        mOblacnost.addTextChangedListener(new TextWatcher() {
            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    oblacnost = Double.parseDouble(charSequence.toString());
                } catch (NumberFormatException e) {
                    oblacnost=oblacnost_def;
                }
                getSharPref().edit().putFloat( "oblacnost", (float)oblacnost) .apply();
            }
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override public void afterTextChanged(Editable editable) {}
        });
        focusListener(mOblacnost);

    }
    private void focusListener(final EditText editText){
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    editText.clearFocus();
                    InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    resiJed();
                    tv_hour.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    /** metoda polovljenja intervala */
    private void resiJed(){
        if (bool_provera) {
            D = 0.0281;
            S = S_drake;
        }

        int MAX_iter = 30;
        double minT = Ta; // bez ovoga stvara bug - mora da bude Tc>Ta
        double maxT = 300;
        double preciznost = 0.1;


        double Ps, Pj, Pc, Pr;
        double sumaP;
        int i = 0;

        Tc = nepostoji;
        do {
            if (i >= MAX_iter){
                Tc = nepostoji;

                break;
            }

            Tc = (minT+maxT)/2;
            Ps = ispisiPs();
            Pj = ispisiPj();
            Pc = ispisiPc();
            Pr = ispisiPr();
            sumaP = Ps + Pj - Pc - Pr;

            if (sumaP>=0) minT = Tc;
            else maxT = Tc;

            i++;

             if (BuildConfig.DEBUG)Log.e("tag","i="+i + " Tc="+Tc + " "+minT+":"+maxT + " sumaP=" +(Ps+Pj-Pc-Pr));
             if (BuildConfig.DEBUG)Log.e("tag","Ps="+Ps +" Pj="+Pj + " Pc="+Pc +" Pr="+Pr);

        }while (abs(sumaP) > preciznost);



        String tekst = "Tc="+Tc +" I="+I+" wind="+wind+" nadmVisina="+ nadmVisina +" albedo="+albedo+" oblacnost="+oblacnost+
                "\ntimeDan="+timeDan +" timeSat="+timeSat + " Ta="+Ta + " D="+D;

         if (BuildConfig.DEBUG)Log.e("tag",tekst);

        if (Tc == nepostoji){
            setTitle(mojHtmlString("<font color='red'>proveri parametre!</font>"));
        }
        else
            setTitle("temp.provodnika je "+ Math.round(Tc)+"°C");
    }


    StaticPodaci.NaponskiNivo naponskiNivo;
    int procenat=50;
    int U;
    private void dialog_views(Spinner mSpinnerNapon, SeekBar mSeekBarI, final TextView tv_procenat, final TextView tv_struja){
        String pomocno = getSharPref().getString("naponskiNivo", StaticPodaci.NaponskiNivo._35kV.toString());
        int ind = getIndexElementaUNizu(getNizNaponskiNivo(), pomocno);
        naponskiNivo = getNizNaponskiNivo()[ind];

        final ArrayAdapter<StaticPodaci.NaponskiNivo> adapterStub = new ArrayAdapter<>(activity,
                android.R.layout.simple_spinner_dropdown_item, getNizNaponskiNivo());
        mSpinnerNapon.setAdapter(adapterStub);
        int brojac = 0;
        for (int i = 0; i < getNizNaponskiNivo().length; i++)
            if (getNizNaponskiNivo()[i].equals(naponskiNivo))
                brojac = i;
        mSpinnerNapon.setSelection(brojac);
        mSpinnerNapon.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                naponskiNivo = (StaticPodaci.NaponskiNivo) adapterView.getItemAtPosition(i);
                U = naponskiNivo.naponUkV();
                racunajI(tv_struja,procenat, U);
            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        mSeekBarI.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                procenat = i*5;
                tv_procenat.setText("I / In = "+procenat+"%");
                racunajI(tv_struja,procenat, U);
            }
            @Override public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mSeekBarI.setProgress(10,true);
        }else mSeekBarI.setProgress(10);
    }

    private double racunajI(TextView tv_struja, double procenat,  double U){
        double In = tipUzeta.getI();
        double I = procenat*In/100;
        double Snaga = 1.73*U*I/1000; // [MVA]
        tv_struja.setText(
                "presek="+S +" mm²"+
                "\nUn="+String.format("%.0f",U)+" kV"+
                "  In="+ String.format("%.0f",In) +" A"+
                "\nSnaga="+ String.format("%.1f",Snaga) +" MVA"+
                "  I="+String.format("%.0f",I)+" A"
        );
        return I;
    }

    int I; /** strujno opterecenje uzeta */
    private double Tc; /** proracunata temp uzeta */
    private double D;/** diameter of the conductor [m] */
    private double S;/** diameter of the conductor [mm2] */
    private int nadmVisina; /** nadmorska visina */

    ///////////// Ps ///////////////
    private static double koef_as; /** absorptivity of conductor surface */
    private static double albedo; /** odsjaj okoline  */
    private static double latitude; /** geo.sirina */
    private static double azimuthCond; /** pravac trase (sever-jug) */
    private static double oblacnost; /** Ns clearness ratio*/
    private static int timeSat ; /** vreme - sat */
    private static int timeDan; /** vreme - redni broj dana u godini */

    ///////////// Pc ///////////////
    private int Ta; /** temp ambijenta  */
    private double d; /** precik zice u spolj.sloju uzeta  */
    private double windAngle; /** napadni ugao vetra  */
    private double beta; /** nagib uzeta  */
    private double wind; /** brzina vetra  */

    ///////////// Pr ///////////////
    private double es; /** koef - uze je svelije/tamnije  */

    ///////////// Pj ///////////////

    private double ispisiPs(){

        if (timeSat<6 || timeSat>18)
            return 0; // noc ili slabo sunce

        final double bs = declination(timeDan);
        final double Z = hourAngleSun(timeSat);
        final double Hs = solarAltitude(latitude,bs,Z);
        final double Ib0 = directBeamRadiation(oblacnost,Hs);
        final double Ib = directBeamRadiation_overSeeLavel(Ib0, nadmVisina);
        final double Id = diffuseBeamRadiation(Ib,Hs);
        final double F = albedo;
        final double ys = azimuthOfSun(bs,Z, Hs);
        final double n_deg = angleOfSolarBeam(Hs, ys , azimuthCond);
        final double Ps = solarPowerReceived(D,koef_as,Ib,Id, n_deg,F, Hs);

//        String output = "bs = "+stringOutput(bs)
//                        +"\nHs = "+stringOutput(Hs)
//                        +"\nZ = "+stringOutput(Z)
//                        +"\nys = "+stringOutput(ys)
//                        +"\nn = "+stringOutput(n_deg)
//                        +"\nIb0 = "+stringOutput(Ib0)
//                        +"\nIb = "+stringOutput(Ib)
//                        +"\nId = "+stringOutput(Id)
//                            +"\nPs = "+stringOutput(Ps) ;
        return Ps;
    }
    private double ispisiPc(){
        final double Tf = filmTemperature(Tc, Ta);
        final double lf = thermalConductivity(Tf);
        final double uf = dynamicViscosity(Tf);
        final double vf = kinematicViscosity(uf,Tf, nadmVisina);
        final double Rs = roughnessOfSurface(D, d);
        final double Re = reynoldsNumber(wind,  D,  vf);
        final double B = getKoef_B(Rs,Re);
        final double n = getKoef_n(Rs,Re);
        final double Nu_fors = nusseltNumber(B, Re, n,  windAngle);
        final double Pc_wind = convectiveHeatLoss_wind( lf,  Tc,  Ta,  Nu_fors);

        final double Gr = grashofNumber(D, Tc, Ta,Tf, vf);
        final double Pr = prandtlNumber(uf, lf);
        final double A = getKoef_A(Gr, Pr);
        final double m = getKoef_m(Gr, Pr);
        final double Nu_nat = nusseltNumber_natural(A, Gr, Pr, m, beta);
        final double Pc_nat = convectiveHeatLoss_natural(lf, Tc, Ta, Nu_nat);


//        String output = "Tf = "+     stringOutput (Tf)
//                        +"\nlf = "+     stringOutput (lf)
//                        +"\nvf= "+      stringOutput (vf)
//                        +"\nRs = "+     stringOutput (Rs)
//                        +"\nRe= "+      stringOutput (Re)
//                        +"\nB = "+      stringOutput (B)
//                        +"\nn = "+      stringOutput (n)
//                        +"\nNu = "+     stringOutput (Nu_fors)
//                        +"\nPc_wind = "+stringOutput(Pc_wind)
//                        +"\n"
//                        +"\nGr= "+      stringOutput (Gr)
//                        +"\nPr= "+      stringOutput (Pr)
//                        +"\nA  = "+     stringOutput (A )
//                        +"\nm  = "+     stringOutput (m )
//                        +"\nNu_nat = "+     stringOutput (Nu_nat)
//                            +"\nPc_nat = "+stringOutput(Pc_nat);
        return Math.max(Pc_wind,Pc_nat);
    }
    private double ispisiPr(){
        final double Pr = radiativeHeatLoss( D,  es,  Ta,  Tc);

//        String output = "Pr = "+stringOutput(Pr);
        return Pr;
    }
    private double ispisiPj(){
        // nemam T1,2 i R1,2 za svaki tip provodnika posebno
        //final double R = acResistance(Tc);
        final double R = acResistance(Tc,S);
        final double Pj = jouleHeating( I, R);

//        String output = "R = "+stringOutput(R)+
//                        "\nPj = "+stringOutput(Pj);

        return Pj;
    }

    private double nadji_d(double presek){
        double d;
        if (presek<30)
            d = 2;
        else if (presek<60)
            d = 3;
        else if (presek<120)
            d = 2;
        else if (presek<200)
            d = 2.5;
        else
            d = 3.4;

        return d/1000;
    }

    //////////////////////////// pomocne metode //////////////////
    public static String stringOutput(double d){
        if (d==d)
            return String.format("%.0f",d);
        else if (d>10 || d<0)
            return String.format("%.2f",d);
        else if (d<0.0001)
            return String.format("%6.3e",d);
        else
            return String.format("%.4f",d);

    }
    public static double sin(double d){return Math.sin( toRadians(d) );}
    public static double cos(double d){
        return Math.cos( toRadians(d) );
    }
    public static double asin(double d){return toDegrees(Math.asin(d));}
    public static double acos(double d){return toDegrees(Math.acos(d));}



}
