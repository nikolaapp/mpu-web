package nsp.mpu.database;

import io.michaelrocks.paranoid.Obfuscate; @Obfuscate
public final class BazaIzolatora {
    public static final String NASLOV_TABELE = "TabelaIzolatora";
    @Obfuscate//import
    public static final class Kolona{
        public static final String KOL_MATERIJAL    = "materijal";
        public static final String KOL_TIP          = "tip";
        public static final String KOL_NAPON        = "napon";
        public static final String KOL_DUZINA       = "duzina";
        public static final String KOL_TEZINA       = "dubina";
        public static final String KOL_CENA         = "cena";
        public static final String KOL_IZMENLJIVO   = "izmenljivo";
    }
}