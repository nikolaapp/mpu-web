package nsp.mpu.database;

import io.michaelrocks.paranoid.Obfuscate; @Obfuscate
public final class BazaKonzola {
    public static final String NASLOV_TABELE = "TabelaKonzola";
    @Obfuscate//import
    public static final class Kolona{
        public static final String KOL_MATERIJAL    = "materijal";
        public static final String KOL_TIP          = "tip";
        public static final String KOL_H12          = "h12";
        public static final String KOL_H23          = "h23";
        public static final String KOL_D1           = "d1";
        public static final String KOL_D2           = "d2";
        public static final String KOL_D3           = "d3";
        public static final String KOL_CENA         =  "cena";
        public static final String KOL_IZMENLJIVO   = "izmenljivo";
    }
}