package nsp.mpu.database;

import io.michaelrocks.paranoid.Obfuscate; @Obfuscate
public final class BazaStubova {
    public static final String NASLOV_TABELE = "TabelaStubova";
    @Obfuscate//import
    public static final class Kolona{
        public static final String KOL_MATERIJAL    = "materijal";
        public static final String KOL_TIP          = "tip";
        public static final String KOL_OZNAKA       = "oznaka";
        public static final String KOL_DUZINA       = "duzina";
        public static final String KOL_DUBINA       = "dubina";
        public static final String KOL_PRECNIK_VECI = "precnik_veci";
        public static final String KOL_PRECNIK_MANJI= "precnik_manji";
        public static final String KOL_SILA         = "sila";
        public static final String KOL_CENA         =  "cena";
        public static final String KOL_ISKOP        = "iskop";
        public static final String KOL_BETON        = "beton";
        public static final String KOL_IZMENLJIVO   = "izmenljivo";
    }
}