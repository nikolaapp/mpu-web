package nsp.mpu.database;

import io.michaelrocks.paranoid.Obfuscate; @Obfuscate
public final class BazaUzadi {
    public static final String NASLOV_TABELE = "TabelaUzadi";
    @Obfuscate//import
    public static final class Kolona{
        public static final String KOL_NAZIV = "naziv";
        public static final String KOL_PRESEK = "presek";
        public static final String KOL_PRECNIK = "precnik";
        public static final String KOL_TEZINA = "tezina";
        public static final String KOL_E = "E";
        public static final String KOL_ALFA = "alfa";
        public static final String KOL_I = "I";
        public static final String KOL_CENA = "cena";
        public static final String KOL_IZMENLJIVO = "izmenljivo";
    }
}