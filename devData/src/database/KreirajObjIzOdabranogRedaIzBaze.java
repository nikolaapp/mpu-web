package nsp.mpu.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import io.michaelrocks.paranoid.Obfuscate; @Obfuscate
public class KreirajObjIzOdabranogRedaIzBaze extends CursorWrapper {
    public KreirajObjIzOdabranogRedaIzBaze(Cursor c){
        super(c);
    }

    public PODACI_TipUzeta iscitajTipUzetaIzBaze() {
        //String uuidString = getString(getColumnIndex(TabelaDogadjaja.Kolona.KOL_UUID));
        String oznakaUzeta = getString(getColumnIndex(BazaUzadi.Kolona.KOL_NAZIV));
        double presek = getDouble(getColumnIndex(BazaUzadi.Kolona.KOL_PRESEK));
        double precnik = getDouble(getColumnIndex(BazaUzadi.Kolona.KOL_PRECNIK));
        double tezina = getDouble(getColumnIndex(BazaUzadi.Kolona.KOL_TEZINA));
        double E = getDouble(getColumnIndex(BazaUzadi.Kolona.KOL_E));
        double alfa = getDouble(getColumnIndex(BazaUzadi.Kolona.KOL_ALFA));
        int I = getInt(getColumnIndex(BazaUzadi.Kolona.KOL_I));
        float jcena = getFloat(getColumnIndex(BazaUzadi.Kolona.KOL_CENA));
        int izmenljivo = getInt(getColumnIndex(BazaUzadi.Kolona.KOL_IZMENLJIVO));

        return new PODACI_TipUzeta(oznakaUzeta, presek, precnik,
                tezina, E,  alfa, I, jcena, izmenljivo != 0);
    }
}
