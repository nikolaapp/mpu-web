package nsp.mpu.database;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu.pomocne_klase.StaticPodaci.getIndexElementaUNizu;
import static nsp.mpu.pomocne_klase.StaticPodaci.getNizNaponskiNivo;

@Obfuscate
public class KreirajObjIzOdabranogRedaIzBaze_izolator extends CursorWrapper {
    public KreirajObjIzOdabranogRedaIzBaze_izolator(Cursor c){
        super(c);
    }

    public PODACI_TipIzolatora iscitajTipIzolatoraIzBaze() {
        String materijal = getString(getColumnIndex(BazaIzolatora.Kolona.KOL_MATERIJAL));
        String tip = getString(getColumnIndex(BazaIzolatora.Kolona.KOL_TIP));
        String napon = getString(getColumnIndex(BazaIzolatora.Kolona.KOL_NAPON));
        double duzina = getDouble(getColumnIndex(BazaIzolatora.Kolona.KOL_DUZINA));
        double tezina = getDouble(getColumnIndex(BazaIzolatora.Kolona.KOL_TEZINA));
        float jcena = getFloat(getColumnIndex(BazaIzolatora.Kolona.KOL_CENA));
        int izmenljivo = getInt(getColumnIndex(BazaIzolatora.Kolona.KOL_IZMENLJIVO));

        int indexNapona = getIndexElementaUNizu(getNizNaponskiNivo(), napon);
        StaticPodaci.NaponskiNivo naponN = getNizNaponskiNivo()[indexNapona];
         if (BuildConfig.DEBUG)Log.e("TAGobs", "indexNapona="+indexNapona
                +" naponN="+naponN);

        return new PODACI_TipIzolatora(materijal, tip,naponN, duzina,
                tezina, jcena, izmenljivo != 0);
    }
}
