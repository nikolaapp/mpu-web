package nsp.mpu.database;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu.pomocne_klase.StaticPodaci.getIndexElementaUNizu;
import static nsp.mpu.pomocne_klase.StaticPodaci.getNizMaterijalStubova;

@Obfuscate
public class KreirajObjIzOdabranogRedaIzBaze_konzola extends CursorWrapper {
    public KreirajObjIzOdabranogRedaIzBaze_konzola(Cursor c){
        super(c);
    }

    public PODACI_TipKonzole iscitajTipKonzoleIzBaze() {
        String materijal = getString(getColumnIndex(BazaKonzola.Kolona.KOL_MATERIJAL));
        String tip = getString(getColumnIndex(BazaKonzola.Kolona.KOL_TIP));
        double h12      = getDouble(getColumnIndex(BazaKonzola.Kolona.KOL_H12 ));
        double h23      = getDouble(getColumnIndex(BazaKonzola.Kolona.KOL_H23 ));
        double d1      = getDouble(getColumnIndex(BazaKonzola.Kolona .KOL_D1  ));
        double d2      = getDouble(getColumnIndex(BazaKonzola.Kolona .KOL_D2  ));
        double d3      = getDouble(getColumnIndex(BazaKonzola.Kolona .KOL_D3  ));
        float jcena      = getFloat(getColumnIndex(BazaKonzola.Kolona .KOL_CENA  ));
        int izmenljivo          = getInt(getColumnIndex(BazaKonzola.Kolona.KOL_IZMENLJIVO));

        int indexMaterijala = getIndexElementaUNizu(getNizMaterijalStubova(), materijal);
        StaticPodaci.MaterijalStuba materS = getNizMaterijalStubova()[indexMaterijala];
         if (BuildConfig.DEBUG)Log.e("TAGobs", "indexMaterijala="+indexMaterijala
                +" materS="+materS);

        return new PODACI_TipKonzole(materS, tip, h12,
                h23, d1,  d2, d3, jcena, izmenljivo != 0);
    }
}
