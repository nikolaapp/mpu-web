package nsp.mpu.database;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu.pomocne_klase.StaticPodaci.getIndexElementaUNizu;
import static nsp.mpu.pomocne_klase.StaticPodaci.getNizMaterijalStubova;
import static nsp.mpu.pomocne_klase.StaticPodaci.getNizTipovaStubova;

@Obfuscate
public class KreirajObjIzOdabranogRedaIzBaze_stub extends CursorWrapper {
    public KreirajObjIzOdabranogRedaIzBaze_stub(Cursor c){
        super(c);
    }

    public PODACI_TipStuba iscitajTipStubaIzBaze() {
        //String uuidString = getString(getColumnIndex(TabelaDogadjaja.Kolona.KOL_UUID));
        String materijal = getString(getColumnIndex(BazaStubova.Kolona.KOL_MATERIJAL));
        String tip = getString(getColumnIndex(BazaStubova.Kolona.KOL_TIP));
        String oznaka = getString(getColumnIndex(BazaStubova.Kolona.KOL_OZNAKA));
        double duzina = getDouble(getColumnIndex(BazaStubova.Kolona.KOL_DUZINA));
        double dubina = getDouble(getColumnIndex(BazaStubova.Kolona.KOL_DUBINA));
        double precnik_veci = getDouble(getColumnIndex(BazaStubova.Kolona.KOL_PRECNIK_VECI));
        double precnik_manji = getDouble(getColumnIndex(BazaStubova.Kolona.KOL_PRECNIK_MANJI));
        double sila = getDouble(getColumnIndex(BazaStubova.Kolona.KOL_SILA));
        float cena =  getFloat(getColumnIndex(BazaStubova.Kolona.KOL_CENA));
        float iskop = getFloat(getColumnIndex(BazaStubova.Kolona.KOL_ISKOP));
        float beton = getFloat(getColumnIndex(BazaStubova.Kolona.KOL_BETON));
        int izmenljivo = getInt(getColumnIndex(BazaStubova.Kolona.KOL_IZMENLJIVO));

        int indexMaterijala = getIndexElementaUNizu(getNizMaterijalStubova(), materijal);
        StaticPodaci.MaterijalStuba materS = getNizMaterijalStubova()[indexMaterijala];
         if (BuildConfig.DEBUG)Log.e("TAGobs", "indexMaterijala="+indexMaterijala
                +" materS="+materS);

        int indexTipa = getIndexElementaUNizu(getNizTipovaStubova(), tip);
        StaticPodaci.Tip_Stuba tipS = getNizTipovaStubova()[indexTipa];
         if (BuildConfig.DEBUG)Log.e("TAGobs", "indexTipa="+indexMaterijala
                +" tipS="+tipS);

        return new PODACI_TipStuba(materS, tipS, oznaka, duzina,dubina,
                    precnik_veci, precnik_manji,  sila, cena, iskop, beton, izmenljivo != 0);
    }
}
