package nsp.mpu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;

@Obfuscate
public class ObradaBazePodataka extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    //public static final int DATABASE_VERSION = 2;/** izmene naziva Al/C -> Al/Ce */

    private static final String DATABASE_NAME = "bazaUzadi.db";

    Context mContext;

    public ObradaBazePodataka(Context contx) {
        super(contx, DATABASE_NAME, null, DATABASE_VERSION);
        mContext=contx;
         if (BuildConfig.DEBUG)Log.e("TAG", "ObradaBazePodataka");
    }

    public void onOpen( SQLiteDatabase db){
        super.onOpen(db);
         if (BuildConfig.DEBUG)Log.e("TAG", "ObradaBazePodataka onOpen zavrseno");
    }

    @Override
    public void onCreate( SQLiteDatabase db) {
         if (BuildConfig.DEBUG)Log.e("TAG", "ObradaBazePodataka onCreate");
        db.execSQL("create table " + BazaUzadi.NASLOV_TABELE + "(" +
                " _id integer primary key autoincrement, " +
                BazaUzadi.Kolona.KOL_NAZIV + " UNIQUE NOT NULL, " +
                BazaUzadi.Kolona.KOL_PRESEK + ", " +
                BazaUzadi.Kolona.KOL_PRECNIK + ", " +
                BazaUzadi.Kolona.KOL_TEZINA + ", " +
                BazaUzadi.Kolona.KOL_E + ", " +
                BazaUzadi.Kolona.KOL_ALFA + ", " +
                BazaUzadi.Kolona.KOL_I + ", " +
                BazaUzadi.Kolona.KOL_CENA + ", " +
                BazaUzadi.Kolona.KOL_IZMENLJIVO +
                ")"
        );

        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-16/2.5",    17.85, 5.4, 0.00354187144, 8100, 0.0000192,   105,  6.67f, false));
        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-25/4",      27.8, 6.8,  0.003558002614, 8100, 0.0000192,  140,  6.67f, false));
        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-35/6",      40, 8.1,    0.003569006745, 8100, 0.0000192,   170, 6.67f, false));
        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-50/8",      56.3, 9.6,  0.003549988947, 8100, 0.0000192,   210, 6.67f, false));
        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-70/12",     81.3, 11.7, 0.003562108296, 7700, 0.0000189,   290, 6.67f, false));
        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-95/15",     109.7, 13.6, 0.003560176022, 7700, 0.0000189,  350, 6.67f, false));
        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-120/20",    141.4, 15.5, 0.003562516331, 7700, 0.0000189,  410, 6.67f, false));
        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-150/25",    173.1, 17.1, 0.003563999473, 7700, 0.0000189,  470, 6.67f, false));
        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-240/40",    282.5, 21.9, 0.003562689919, 7700, 0.0000189,  645, 6.67f, false));
        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-360/57",    416.5, 26.4, 0.003530446048, 7700, 0.0000189,  750, 6.67f, false));
        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-490/65",    553.9, 30.6, 0.003435259891, 7000, 0.0000193,  960, 6.67f, false));
        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("OPGW-G 34.4",      34.4, 8.3,  0.006847512942, 14000, 0.0000134, 150, 10.0f, false));
        db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("OPGW-D 49.5",      49.5, 10,   0.006676446566, 14000, 0.0000134, 200, 10.0f, false));
        //db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-240/40 -NS",    282.5, 21.9, 0.003426, 7700, 0.0000189, true));
        //db.insert(BazaUzadi.NASLOV_TABELE, null, inicijalizacijaBaze("Al/Ce-490/65 -NS",    553.9, 30.6, 0.003304, 7000, 0.0000193, true));

    }

    @Override
    public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion) {
        // ovim bi izbrisali db, i kreirali novu
//        db.execSQL("DROP TABLE IF EXISTS " + BazaUzadi.NASLOV_TABELE);
//        onCreate(db);

        if (oldVersion < 2) {
            /////// VAZI SAMO ZA OVAJ SLUCAJ
            //preimenujemo postojecu DB u staraDB
            //kreiramo novu kao da je prva inicijalizacija, i sada imamo dve DB
            //kopiramo sve redove iz noveDB u staraDB --- mada je dole obrnuto!?!
            //brisemo staraDB
            db.execSQL("ALTER TABLE "+ BazaUzadi.NASLOV_TABELE +" RENAME TO " + "staraDB");
            onCreate(db);
            db.execSQL("INSERT OR IGNORE INTO "+ BazaUzadi.NASLOV_TABELE  +" SELECT * FROM  " + "staraDB");
            db.execSQL("DROP TABLE staraDB");

        }

        if (oldVersion < 3) {
            //IZVRSAVA PRVO <2 PA ONDA OVO, ITD
        }


    }

    private ContentValues inicijalizacijaBaze(String oznakaUzeta, double presek,double precnik,
                                     double tezina, double E, double  alfa, int I, float jcena, boolean izmenljivo){
        ContentValues cv = new ContentValues();
        cv.put(BazaUzadi.Kolona.KOL_NAZIV, oznakaUzeta);
        cv.put(BazaUzadi.Kolona.KOL_PRESEK, presek);
        cv.put(BazaUzadi.Kolona.KOL_PRECNIK, precnik);
        cv.put(BazaUzadi.Kolona.KOL_TEZINA, tezina);
        cv.put(BazaUzadi.Kolona.KOL_E, E);
        cv.put(BazaUzadi.Kolona.KOL_ALFA, alfa);
        cv.put(BazaUzadi.Kolona.KOL_I, I);
        cv.put(BazaUzadi.Kolona.KOL_CENA, jcena);
        cv.put(BazaUzadi.Kolona.KOL_IZMENLJIVO,izmenljivo?1:0);
        return cv;
    }
}
