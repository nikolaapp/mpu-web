package nsp.mpu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu.pomocne_klase.StaticPodaci.izolator_nije_odabrano;

@Obfuscate
public class ObradaBazePodataka_izolator extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 4;

    private static final String DATABASE_NAME = "bazaIzolatora.db";

    Context mContext;

    public ObradaBazePodataka_izolator(Context contx) {
        super(contx, DATABASE_NAME, null, DATABASE_VERSION);
        mContext=contx;
         if (BuildConfig.DEBUG)Log.e("TAG", "ObradaBazePodataka_izolatora");
    }

    public void onOpen( SQLiteDatabase db){
        super.onOpen(db);
         if (BuildConfig.DEBUG)Log.e("TAG", "ObradaBazePodataka_izolatora onOpen zavrseno");
    }

    @Override
    public void onCreate( SQLiteDatabase db) {
         if (BuildConfig.DEBUG)Log.e("TAG", "ObradaBazePodataka_izolatora onCreate");
        db.execSQL("create table " + BazaIzolatora.NASLOV_TABELE + "(" +
                " _id integer primary key autoincrement, " +
                BazaIzolatora.Kolona.KOL_MATERIJAL    + ", " +
                BazaIzolatora.Kolona.KOL_TIP          + " UNIQUE NOT NULL, " +
                BazaIzolatora.Kolona.KOL_NAPON          + ", " +
                BazaIzolatora.Kolona.KOL_DUZINA         + ", " +
                BazaIzolatora.Kolona.KOL_TEZINA           + ", " +
                BazaIzolatora.Kolona.KOL_CENA           + ", " +
                BazaIzolatora.Kolona.KOL_IZMENLJIVO   +
                ")"
        );

        db.insert(BazaIzolatora.NASLOV_TABELE, null, inicijalizacijaBaze("nepostoji",izolator_nije_odabrano,  StaticPodaci.NaponskiNivo._35kV.toString(),     0, 0,     0, false));
        db.insert(BazaIzolatora.NASLOV_TABELE, null, inicijalizacijaBaze("silikon","ZNSI-N-35",  StaticPodaci.NaponskiNivo._35kV.toString(),                56.5, 4.9,  300,false));
        db.insert(BazaIzolatora.NASLOV_TABELE, null, inicijalizacijaBaze("silikon","ZNSI-Np-35", StaticPodaci.NaponskiNivo._35kV.toString(),                 60.5, 4.9,  330,false));
        db.insert(BazaIzolatora.NASLOV_TABELE, null, inicijalizacijaBaze("silikon","ZNSI-DNp-35",StaticPodaci.NaponskiNivo._35kV.toString(),                91.0, 20.9,  399,false));

    }

    @Override
    public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion) {
         if (BuildConfig.DEBUG)Log.e("TAGdb", "ObradaBazePodataka_izolatora onUpgrade");
        db.execSQL("DROP TABLE IF EXISTS " + BazaIzolatora.NASLOV_TABELE);
        onCreate(db);
    }

    private ContentValues inicijalizacijaBaze
            (String oznakaStuba,String tip,String napon, double duzina,double tezina, float jcena, boolean izmenljivo){
        ContentValues cv = new ContentValues();
        cv.put(BazaIzolatora.Kolona.KOL_MATERIJAL    , oznakaStuba);
        cv.put(BazaIzolatora.Kolona.KOL_TIP          , tip);
        cv.put(BazaIzolatora.Kolona.KOL_NAPON          , napon);
        cv.put(BazaIzolatora.Kolona.KOL_DUZINA, duzina );
        cv.put(BazaIzolatora.Kolona.KOL_TEZINA, tezina );
        cv.put(BazaIzolatora.Kolona.KOL_CENA, jcena );
        cv.put(BazaIzolatora.Kolona.KOL_IZMENLJIVO   ,izmenljivo?1:0);
        return cv;
    }
}
