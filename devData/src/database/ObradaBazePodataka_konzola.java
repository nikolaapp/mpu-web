package nsp.mpu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu.pomocne_klase.StaticPodaci.konzola_trougao;
import static nsp.mpu.pomocne_klase.StaticPodaci.konzola_vrsna;

@Obfuscate
public class ObradaBazePodataka_konzola extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 2;

    private static final String DATABASE_NAME = "bazaKonzola.db";

    Context mContext;

    public ObradaBazePodataka_konzola(Context contx) {
        super(contx, DATABASE_NAME, null, DATABASE_VERSION);
        mContext=contx;
         if (BuildConfig.DEBUG)Log.e("TAGdb", "ObradaBazePodataka_konzola");
    }

    public void onOpen( SQLiteDatabase db){
        super.onOpen(db);
         if (BuildConfig.DEBUG)Log.e("TAGdb", "ObradaBazePodataka_konzola onOpen zavrseno");
    }

    @Override
    public void onCreate( SQLiteDatabase db) {
         if (BuildConfig.DEBUG)Log.e("TAGdb", "ObradaBazePodataka_konzola onCreate");
        db.execSQL("create table " + BazaKonzola.NASLOV_TABELE + "(" +
                " _id integer primary key autoincrement, " +
                BazaKonzola.Kolona.KOL_MATERIJAL    + ", " +
                BazaKonzola.Kolona.KOL_TIP          + " UNIQUE NOT NULL, " +
                BazaKonzola.Kolona.KOL_H12          + ", " +
                BazaKonzola.Kolona.KOL_H23          + ", " +
                BazaKonzola.Kolona.KOL_D1           + ", " +
                BazaKonzola.Kolona.KOL_D2           + ", " +
                BazaKonzola.Kolona.KOL_D3           +", "  +
                BazaKonzola.Kolona.KOL_CENA           +", "  +
                BazaKonzola.Kolona.KOL_IZMENLJIVO   +
                ")"
        );


        db.insert(BazaKonzola.NASLOV_TABELE, null, inicijalizacijaBaze(StaticPodaci.MaterijalStuba.betonski.toString(),konzola_vrsna,0, 0,  0, 200, 200,   90,false));
        db.insert(BazaKonzola.NASLOV_TABELE, null, inicijalizacijaBaze(StaticPodaci.MaterijalStuba.betonski.toString(),konzola_trougao,260, 0,  0, 200, 200,  100, false));
        db.insert(BazaKonzola.NASLOV_TABELE, null, inicijalizacijaBaze(StaticPodaci.MaterijalStuba.čel_rešet.toString(),"2005", 260, 195,  180, 200, 210,   0, false));
        db.insert(BazaKonzola.NASLOV_TABELE, null, inicijalizacijaBaze(StaticPodaci.MaterijalStuba.čel_rešet.toString(),"2006", 260, 195,  180, 200, 210,   0, false));

    }

    @Override
    public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion) {
         if (BuildConfig.DEBUG)Log.e("TAGdb", "ObradaBazePodataka_konzola onUpgrade");
        db.execSQL("DROP TABLE IF EXISTS " + BazaKonzola.NASLOV_TABELE);
        onCreate(db);
    }

    private ContentValues inicijalizacijaBaze
            (String oznakaStuba,String tip, double h12,double h23,
             double d1, double d2, double  d3, float jcena, boolean izmenljivo){
        ContentValues cv = new ContentValues();
        cv.put(BazaKonzola.Kolona.KOL_MATERIJAL    , oznakaStuba);
        cv.put(BazaKonzola.Kolona.KOL_TIP          , tip);
        cv.put(BazaKonzola.Kolona.KOL_H12, h12 );
        cv.put(BazaKonzola.Kolona.KOL_H23, h23 );
        cv.put(BazaKonzola.Kolona.KOL_D1 , d1  );
        cv.put(BazaKonzola.Kolona.KOL_D2 , d2  );
        cv.put(BazaKonzola.Kolona.KOL_D3 , d3  );
        cv.put(BazaKonzola.Kolona.KOL_CENA , jcena  );
        cv.put(BazaKonzola.Kolona.KOL_IZMENLJIVO   ,izmenljivo?1:0);
        return cv;
    }
}
