package nsp.mpu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;
import static nsp.mpu.pomocne_klase.StaticPodaci.stub_12_1000;
import static nsp.mpu.pomocne_klase.StaticPodaci.stub_12_315;

@Obfuscate
public class ObradaBazePodataka_stub extends SQLiteOpenHelper {

    //public static final int DATABASE_VERSION = 1;
    public static final int DATABASE_VERSION = 8;

    private static final String DATABASE_NAME = "bazaStubova.db";

    Context mContext;

    public ObradaBazePodataka_stub(Context contx) {
        super(contx, DATABASE_NAME, null, DATABASE_VERSION);
        mContext=contx;
         if (BuildConfig.DEBUG)Log.e("TAG", "ObradaBazePodataka_stub");
    }

    public void onOpen( SQLiteDatabase db){
        super.onOpen(db);
         if (BuildConfig.DEBUG)Log.e("TAG", "ObradaBazePodataka_stub onOpen zavrseno");
    }

    @Override
    public void onCreate( SQLiteDatabase db) {
         if (BuildConfig.DEBUG)Log.e("TAG", "ObradaBazePodataka_stub onCreate");
        db.execSQL("create table " + BazaStubova.NASLOV_TABELE + "(" +
                " _id integer primary key autoincrement, " +
                BazaStubova.Kolona.KOL_MATERIJAL    + ", " +
                BazaStubova.Kolona.KOL_TIP          + ", " +
                BazaStubova.Kolona.KOL_OZNAKA       + " UNIQUE NOT NULL, " +
                BazaStubova.Kolona.KOL_DUZINA       + ", " +
                BazaStubova.Kolona.KOL_DUBINA       + ", " +
                BazaStubova.Kolona.KOL_PRECNIK_VECI + ", " +
                BazaStubova.Kolona.KOL_PRECNIK_MANJI+ ", " +
                BazaStubova.Kolona.KOL_SILA + ", "  +
                BazaStubova.Kolona.KOL_CENA + ", "  +
                BazaStubova.Kolona.KOL_ISKOP + ", "  +
                BazaStubova.Kolona.KOL_BETON + ", "  +
                BazaStubova.Kolona.KOL_IZMENLJIVO   +
                ")"
        );

        int no = nepostoji;
        db.insert(BazaStubova.NASLOV_TABELE, null, inicijalizacijaBaze(StaticPodaci.MaterijalStuba.čel_rešet.toString(), StaticPodaci.Tip_Stuba.noseći.toString(), "es 2005",       13, no,  no,   no,  no,   3900, 19.8f,  5.4f, false));
        db.insert(BazaStubova.NASLOV_TABELE, null, inicijalizacijaBaze(StaticPodaci.MaterijalStuba.čel_rešet.toString(), StaticPodaci.Tip_Stuba.zatezni.toString(),"es 2006",       13, no,  no,   no,  no,   5850, 20,     7.9f, false));
        db.insert(BazaStubova.NASLOV_TABELE, null, inicijalizacijaBaze(StaticPodaci.MaterijalStuba.betonski.toString(),  StaticPodaci.Tip_Stuba.noseći.toString(), stub_12_315,     12, 2,  0.31, 0.13, 315,   600, 1.2f,   0.8f, false));
        db.insert(BazaStubova.NASLOV_TABELE, null, inicijalizacijaBaze(StaticPodaci.MaterijalStuba.betonski.toString(),  StaticPodaci.Tip_Stuba.zatezni.toString(),stub_12_1000,    12, 2,  0.31, 0.13, 1000, 1025, 2,      1,    false));

    }

    @Override
    public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion) {
         if (BuildConfig.DEBUG)Log.e("TAG", "ObradaBazePodataka_stub onUpgrade");
        db.execSQL("DROP TABLE IF EXISTS " + BazaStubova.NASLOV_TABELE);
        onCreate(db);
    }

    private ContentValues inicijalizacijaBaze
            (String oznakaStuba, String tip, String oznaka, double duzina,double dubina,
             double precenk_veci, double precenk_manji, double  sila,
             float cena, float iskop, float beton, boolean izmenljivo){
        ContentValues cv = new ContentValues();
        cv.put(BazaStubova.Kolona.KOL_MATERIJAL    , oznakaStuba);
        cv.put(BazaStubova.Kolona.KOL_TIP          , tip);
        cv.put(BazaStubova.Kolona.KOL_OZNAKA       , oznaka);
        cv.put(BazaStubova.Kolona.KOL_DUZINA       , duzina);
        cv.put(BazaStubova.Kolona.KOL_DUBINA       , dubina);
        cv.put(BazaStubova.Kolona.KOL_PRECNIK_VECI , precenk_veci);
        cv.put(BazaStubova.Kolona.KOL_PRECNIK_MANJI, precenk_manji);
        cv.put(BazaStubova.Kolona.KOL_SILA         , sila);
        cv.put(BazaStubova.Kolona.KOL_CENA         , cena);
        cv.put(BazaStubova.Kolona.KOL_ISKOP         , iskop);
        cv.put(BazaStubova.Kolona.KOL_BETON         , beton);
        cv.put(BazaStubova.Kolona.KOL_IZMENLJIVO   ,izmenljivo?1:0);
        return cv;
    }
}
