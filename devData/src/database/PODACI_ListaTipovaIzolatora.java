package nsp.mpu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;

@Obfuscate
public class PODACI_ListaTipovaIzolatora {
    private static Context mContext;
    private static PODACI_ListaTipovaIzolatora sSinglton;
    public static PODACI_ListaTipovaIzolatora getSingltonIzolatora(Context contx) {
        if(sSinglton ==null) {
            if (contx == null)  if (BuildConfig.DEBUG)Log.e("TAG", "111contex je null");
            sSinglton = new PODACI_ListaTipovaIzolatora(contx);

        }
        mContext = contx;
        return sSinglton;
    }

    // $$$ 5.1.18.
    // odavde dobijam context
    public static Context getContext(){
        return mContext;
    }

    private static SQLiteDatabase mDatabase;

    private PODACI_ListaTipovaIzolatora(Context contx) {
         if (BuildConfig.DEBUG)Log.e("TAG", "pre new ObradaBazePodataka");
        mDatabase = new ObradaBazePodataka_izolator(contx).getWritableDatabase();
         if (BuildConfig.DEBUG)Log.e("TAG", "posle new ObradaBazePodataka");
    }

    private static KreirajObjIzOdabranogRedaIzBaze_izolator queryUpitZaCitanjeBaze (String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                BazaIzolatora.NASLOV_TABELE,
                null, // Columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        );
        return  new KreirajObjIzOdabranogRedaIzBaze_izolator(cursor);
    }


    public static List<PODACI_TipIzolatora> getListuTipovaIzolatora(){
        List<PODACI_TipIzolatora> lista = new ArrayList<>();
        KreirajObjIzOdabranogRedaIzBaze_izolator cursor = queryUpitZaCitanjeBaze(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                lista.add(cursor.iscitajTipIzolatoraIzBaze());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return lista;
    }


    public PODACI_TipIzolatora getTipIzolatoraIzListe(String s){
        List<PODACI_TipIzolatora> lista = getListuTipovaIzolatora();
        int i = StaticPodaci.getIndexElementaUNizu(lista, s);
        return lista.get(i);
    }

    public PODACI_TipIzolatora getTipIzolatoraIzListe(int i){
        List<PODACI_TipIzolatora> lista = getListuTipovaIzolatora();
        if (i>=lista.size())
            i=0;
        return lista.get(i);
    }
    
    public String[] getNizTipovaIzolatora(){
        List<PODACI_TipIzolatora> lista = getListuTipovaIzolatora();
        String[] niz = new String[lista.size()];

        int i =0;
        for(PODACI_TipIzolatora ts : lista) {
            niz[i]=ts.getOznaka();
            i++;
        }
        return niz;
    }

    public static int pozicijaIzolatoraUListi(String s){
        List<PODACI_TipIzolatora> lista = getListuTipovaIzolatora();
        int i =0;
        for(PODACI_TipIzolatora tu : lista){
            if (tu.getOznaka().equals(s))
                return i;
            i++;
        }
        return nepostoji;

    }

    public boolean addTipIzolatora (PODACI_TipIzolatora tipIzolatora){
        long i = mDatabase.insert(BazaIzolatora.NASLOV_TABELE,
                null, ucitajJedanDogRadiUpisaUBazu(tipIzolatora ));
        return i >= 0;
    }

    public void updateTipIzolatora(PODACI_TipIzolatora tipIzolatora){
        mDatabase.update(BazaIzolatora.NASLOV_TABELE, //u koju tabelu upisujes
                ucitajJedanDogRadiUpisaUBazu(tipIzolatora),        //sta upisujed
                BazaIzolatora.Kolona.KOL_TIP + " = ?",   // u koji red upisujes
                new String[] { tipIzolatora.getOznaka() } );     //ono sto ce da stoji iznad umesto ?
    }

    public boolean deleteTipIzolatora (PODACI_TipIzolatora tipIzolatora){
        int i = mDatabase.delete(BazaIzolatora.NASLOV_TABELE, //u koju tabelu upisujes
                BazaIzolatora.Kolona.KOL_TIP + " = ?",   // koji red brises
                new String[] { tipIzolatora.getOznaka() });
        return i != 0;
    }

    private static ContentValues ucitajJedanDogRadiUpisaUBazu(PODACI_TipIzolatora tipIzolatora) {
        ContentValues values = new ContentValues();
        values.put(BazaIzolatora.Kolona.KOL_MATERIJAL      , tipIzolatora.getMaterijal());
        values.put(BazaIzolatora.Kolona.KOL_TIP            , tipIzolatora.getOznaka()  );
        values.put(BazaIzolatora.Kolona.KOL_NAPON             , tipIzolatora.getNapon().toString()  );
        values.put(BazaIzolatora.Kolona.KOL_DUZINA            , tipIzolatora.getDuzina()  );
        values.put(BazaIzolatora.Kolona.KOL_TEZINA             , tipIzolatora.getTezina()   );
        values.put(BazaIzolatora.Kolona.KOL_CENA             , tipIzolatora.getJcena()   );
        values.put(BazaIzolatora.Kolona.KOL_IZMENLJIVO     , tipIzolatora.getIzmenljivo()?1:0);
        return values;
    }
}
