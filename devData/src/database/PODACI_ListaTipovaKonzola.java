package nsp.mpu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.pomocne_klase.StaticPodaci;

import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;

@Obfuscate
public class PODACI_ListaTipovaKonzola {
    private static Context mContext;
    private static PODACI_ListaTipovaKonzola sSinglton;
    public static PODACI_ListaTipovaKonzola getSingltonKonzola(Context contx) {
        if(sSinglton ==null) {
            if (contx == null)  if (BuildConfig.DEBUG)Log.e("TAG", "111contex je null");
            sSinglton = new PODACI_ListaTipovaKonzola(contx);

        }
        mContext = contx;
        return sSinglton;
    }

    // $$$ 5.1.18.
    // odavde dobijam context
    public static Context getContext(){
        return mContext;
    }

    private static SQLiteDatabase mDatabase;

    private PODACI_ListaTipovaKonzola(Context contx) {
         if (BuildConfig.DEBUG)Log.e("TAG", "pre new ObradaBazePodataka");
        mDatabase = new ObradaBazePodataka_konzola(contx).getWritableDatabase();
         if (BuildConfig.DEBUG)Log.e("TAG", "posle new ObradaBazePodataka");
    }

    private static KreirajObjIzOdabranogRedaIzBaze_konzola queryUpitZaCitanjeBaze (String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                BazaKonzola.NASLOV_TABELE,
                null, // Columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        );
        return  new KreirajObjIzOdabranogRedaIzBaze_konzola(cursor);
    }


    public static List<PODACI_TipKonzole> getListuTipovaKonzola(){
        List<PODACI_TipKonzole> lista = new ArrayList<>();
        KreirajObjIzOdabranogRedaIzBaze_konzola cursor = queryUpitZaCitanjeBaze(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                lista.add(cursor.iscitajTipKonzoleIzBaze());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return lista;
    }


    public PODACI_TipKonzole getTipKonzoleIzListe(String s){
        List<PODACI_TipKonzole> lista = getListuTipovaKonzola();
        int i = StaticPodaci.getIndexElementaUNizu(lista, s);
        return lista.get(i);
    }

    public PODACI_TipKonzole getTipKonzoleIzListe(int i){
        List<PODACI_TipKonzole> lista = getListuTipovaKonzola();
        if (i>=lista.size())
            i=0;
        return lista.get(i);
    }
    
    public String[] getNizTipovaKonzola(){
        List<PODACI_TipKonzole> listaKonzola = getListuTipovaKonzola();
        String[] niz = new String[listaKonzola.size()];

        int i =0;
        for(PODACI_TipKonzole ts : listaKonzola) {
            niz[i]=ts.getOznaka();
            i++;
        }
        return niz;
    }

    public String[] getNizTipovaKonzola(StaticPodaci.MaterijalStuba materijal){
        List<PODACI_TipKonzole> listaKonzola = getListuTipovaKonzola();
        List<PODACI_TipKonzole> pomocnaLista = new ArrayList<>();

        for(PODACI_TipKonzole tk : listaKonzola)
            if (tk.getMaterijal() . equals(materijal))
                pomocnaLista.add(tk);

        if (pomocnaLista.size()==0) {
            return new String[]{"GRESKA 03"};
        }

        String[] niz = new String[pomocnaLista.size()];
        for (int i = 0; i < niz.length; i++) {
            niz[i] = pomocnaLista.get(i).getOznaka();
        }

        return niz;
    }

    public static int pozicijaKonzoleUListi(String s){
        List<PODACI_TipKonzole> lista = getListuTipovaKonzola();
        int i =0;
        for(PODACI_TipKonzole tu : lista){
            if (tu.getOznaka().equals(s))
                return i;
            i++;
        }
        return nepostoji;

    }

    public boolean addTipKonzole (PODACI_TipKonzole tipKonzole){
        long i = mDatabase.insert(BazaKonzola.NASLOV_TABELE,
                null, ucitajJedanDogRadiUpisaUBazu(tipKonzole ));
        return i >= 0;
    }

    public void updateTipKonzole(PODACI_TipKonzole tipKonzole){
        mDatabase.update(BazaKonzola.NASLOV_TABELE, //u koju tabelu upisujes
                ucitajJedanDogRadiUpisaUBazu(tipKonzole),        //sta upisujed
                BazaKonzola.Kolona.KOL_TIP + " = ?",   // u koji red upisujes
                new String[] { tipKonzole.getOznaka() } );     //ono sto ce da stoji iznad umesto ?
    }

    public boolean deleteTipKonzole (PODACI_TipKonzole tipKonzole){
        int i = mDatabase.delete(BazaKonzola.NASLOV_TABELE, //u koju tabelu upisujes
                BazaKonzola.Kolona.KOL_TIP + " = ?",   // koji red brises
                new String[] { tipKonzole.getOznaka() });
        return i != 0;
    }

    private static ContentValues ucitajJedanDogRadiUpisaUBazu(PODACI_TipKonzole tipKonzole) {
        ContentValues values = new ContentValues();
        values.put(BazaKonzola.Kolona.KOL_MATERIJAL      , tipKonzole.getMaterijal().toString());
        values.put(BazaKonzola.Kolona.KOL_TIP            , tipKonzole.getOznaka()  );
        values.put(BazaKonzola.Kolona.KOL_H12            , tipKonzole.getH12()  );
        values.put(BazaKonzola.Kolona.KOL_H23            , tipKonzole.getH23()  );
        values.put(BazaKonzola.Kolona.KOL_D1             , tipKonzole.getD1()   );
        values.put(BazaKonzola.Kolona.KOL_D2             , tipKonzole.getD2()   );
        values.put(BazaKonzola.Kolona.KOL_D3             , tipKonzole.getD3()   );
        values.put(BazaKonzola.Kolona.KOL_CENA             , tipKonzole.getJcena()   );
        values.put(BazaKonzola.Kolona.KOL_IZMENLJIVO     , tipKonzole.getIzmenljivo()?1:0);
        return values;
    }
}
