package nsp.mpu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.pomocne_klase.StaticPodaci;

@Obfuscate
public class PODACI_ListaTipovaStubova {
    private static Context mContext;
    private static PODACI_ListaTipovaStubova sSinglton;
    public static PODACI_ListaTipovaStubova getSingltonStubova(Context contx) {
        if(sSinglton ==null) {
            if (contx == null)  if (BuildConfig.DEBUG)Log.e("TAG", "111contex je null");
            sSinglton = new PODACI_ListaTipovaStubova(contx);

        }
        mContext = contx;
        return sSinglton;
    }

    // $$$ 5.1.18.
    // odavde dobijam context
    private static Context getContext(){
        return mContext;
    }

    private static SQLiteDatabase mDatabase;

    private PODACI_ListaTipovaStubova(Context contx) {
         if (BuildConfig.DEBUG)Log.e("TAG", "pre new ObradaBazePodataka");
        mDatabase = new ObradaBazePodataka_stub(contx).getWritableDatabase();
         if (BuildConfig.DEBUG)Log.e("TAG", "posle new ObradaBazePodataka");
    }

    private static KreirajObjIzOdabranogRedaIzBaze_stub queryUpitZaCitanjeBaze (String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                BazaStubova.NASLOV_TABELE,
                null, // Columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        );
        return  new KreirajObjIzOdabranogRedaIzBaze_stub(cursor);
    }


    public static List<PODACI_TipStuba> getListuStubova(){
        List<PODACI_TipStuba> lista = new ArrayList<>();
        KreirajObjIzOdabranogRedaIzBaze_stub cursor = queryUpitZaCitanjeBaze(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                lista.add(cursor.iscitajTipStubaIzBaze());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return lista;
    }


    public PODACI_TipStuba getTipStubaIzListe(String s){
        List<PODACI_TipStuba> lista = getListuStubova();
        int i = StaticPodaci.getIndexElementaUNizu(lista, s);
        return lista.get(i);
    }

    public PODACI_TipStuba getTipStubaIzListe(int i){
        List<PODACI_TipStuba> lista = getListuStubova();
        if (i>=lista.size())
            i=0;
        return lista.get(i);
    }

    public String[] getNizOznakaStub(){
        List<PODACI_TipStuba> listaUzadi = getListuStubova();
        String[] niz = new String[listaUzadi.size()];

        int i =0;
        for(PODACI_TipStuba ts : listaUzadi) {
            niz[i]=ts.getOznaka();
            i++;
        }
        return niz;
    }

    public String[] getNizOznakaStub(StaticPodaci.MaterijalStuba materijal){
        List<PODACI_TipStuba> listaUzadi = getListuStubova();
        List<PODACI_TipStuba> pomocnaLista = new ArrayList<>();

        for(PODACI_TipStuba ts : listaUzadi)
            if (ts.getMaterijal() . equals(materijal))
                pomocnaLista.add(ts);

        if (pomocnaLista.size()==0) {
            return new String[]{"GRESKA 03"};
        }

        String[] niz = new String[pomocnaLista.size()];
        for (int i = 0; i < niz.length; i++) {
            niz[i] = pomocnaLista.get(i).getOznaka();
        }

        return niz;
    }

//    private static int pozicijaStubaUListi(String s){
//        List<PODACI_TipStuba> listaUzadi = getListuStubova();
//         if (BuildConfig.DEBUG)Log.e("TAGstub", "s "+s+ " \nlistaUzadi "+ listaUzadi);
//
//        int i =0;
//        for(PODACI_TipStuba tu : listaUzadi){
//            if (tu.getOznaka().equals(s))
//                return i;
//            i++;
//        }
//        return -1;
//
//    }

    public boolean addTipStuba (PODACI_TipStuba tipStuba){
        // index rede u koji je upisan podatak, ako je greska onda vraca -1
        long i = mDatabase.insert(BazaStubova.NASLOV_TABELE,
                null, ucitajJedanDogRadiUpisaUBazu(tipStuba ));
        return i >= 0;
    }

    public void updateTipStuba(PODACI_TipStuba tipStuba){
        mDatabase.update(BazaStubova.NASLOV_TABELE, //u koju tabelu upisujes
                ucitajJedanDogRadiUpisaUBazu(tipStuba),        //sta upisujed
                BazaStubova.Kolona.KOL_OZNAKA + " = ?",   // u koji red upisujes
                new String[] { tipStuba.getTip().toString() } );     //ono sto ce da stoji iznad umesto ?
    }

    public boolean deleteTipStuba (PODACI_TipStuba tipStuba){
        // i oznacava koliko redova u bazi je obrisano
        int i = mDatabase.delete(BazaStubova.NASLOV_TABELE, //u koju tabelu upisujes
                BazaStubova.Kolona.KOL_OZNAKA + " = ?",   // koji red brises
                new String[] { tipStuba.getOznaka()});
        return i != 0;
    }

    private static ContentValues ucitajJedanDogRadiUpisaUBazu(PODACI_TipStuba tipStuba) {
        ContentValues values = new ContentValues();
        values.put(BazaStubova.Kolona.KOL_MATERIJAL      , tipStuba.getMaterijal().toString());
        values.put(BazaStubova.Kolona.KOL_TIP            , tipStuba.getTip().toString());
        values.put(BazaStubova.Kolona.KOL_OZNAKA         , tipStuba.getOznaka());
        values.put(BazaStubova.Kolona.KOL_DUZINA         , tipStuba.getDuzina());
        values.put(BazaStubova.Kolona.KOL_DUBINA         , tipStuba.getDubina());
        values.put(BazaStubova.Kolona.KOL_PRECNIK_MANJI  , tipStuba.getPrecnikManji());
        values.put(BazaStubova.Kolona.KOL_PRECNIK_VECI   , tipStuba.getPrecnikVeci());
        values.put(BazaStubova.Kolona.KOL_SILA           , tipStuba.getSila());
        values.put(BazaStubova.Kolona.KOL_IZMENLJIVO     , tipStuba.getIzmenljivo()?1:0);
        return values;
    }
}
