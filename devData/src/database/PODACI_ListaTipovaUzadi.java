package nsp.mpu.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;

import static nsp.mpu.pomocne_klase.StaticPodaci.nepostoji;

@Obfuscate
public class PODACI_ListaTipovaUzadi {
    private static Context mContext;
    private static PODACI_ListaTipovaUzadi sSinglton;
    public static PODACI_ListaTipovaUzadi getListaUzadi(Context contx) {
        if(sSinglton ==null) {
            if (contx == null)  if (BuildConfig.DEBUG)Log.e("TAG", "111contex je null");
            sSinglton = new PODACI_ListaTipovaUzadi(contx);

        }
        mContext = contx;
        return sSinglton;
    }
    
    // $$$ 5.1.18.
    // odavde dobijam context
    public static Context getContext(){
        return mContext;
    }

    private static SQLiteDatabase mDatabase;

    private PODACI_ListaTipovaUzadi(Context contx) {
         if (BuildConfig.DEBUG)Log.e("TAG", "pre new ObradaBazePodataka");
        mDatabase = new ObradaBazePodataka(contx).getWritableDatabase();
         if (BuildConfig.DEBUG)Log.e("TAG", "posle new ObradaBazePodataka");
    }

    private static KreirajObjIzOdabranogRedaIzBaze queryUpitZaCitanjeBaze (String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                BazaUzadi.NASLOV_TABELE,
                null, // Columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        );
        return  new KreirajObjIzOdabranogRedaIzBaze(cursor);
    }


    public static List<PODACI_TipUzeta> getListuTipovaUzadi(){
        List<PODACI_TipUzeta> lista = new ArrayList<>();
        KreirajObjIzOdabranogRedaIzBaze cursor = queryUpitZaCitanjeBaze(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                lista.add(cursor.iscitajTipUzetaIzBaze());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return lista;
    }


    // $$$ 3.1.18.-1
    // new String[] {s}
    public PODACI_TipUzeta getTipUzetaIzListe(String s){
        int zastita_broj = (pozicijaUzetaUListi(s)+(zastita_brojac =zastita_broj_pom+3))%(getNizNazivUzadi().length);
        KreirajObjIzOdabranogRedaIzBaze cursor = queryUpitZaCitanjeBaze(
                BazaUzadi.Kolona.KOL_NAZIV + " = ?",
                new String[] { isDebuggable(mContext) ?
                        getNizNazivUzadi()[zastita_broj] :
                        getNizNazivUzadi()[pozicijaUzetaUListi(s)] } );
        try {
            if (cursor.getCount()==0)
                return null;
            cursor.moveToFirst();
            return cursor.iscitajTipUzetaIzBaze();
        } finally {
            cursor.close();
        }
    }

    int zastita_broj_pom =2;
    int zastita_brojac =20;
    public String[] getNizNazivUzadi(){
        List<PODACI_TipUzeta> listaUzadi = getListuTipovaUzadi();
        String[] niz = new String[listaUzadi.size()];

        int i =0;
        for(PODACI_TipUzeta tu : listaUzadi) {
            niz[i]=tu.getOznakaUzeta();
            i++;
            zastita_brojac--;
        }
        return niz;
    }

    public static int pozicijaUzetaUListi(String s){
        List<PODACI_TipUzeta> listaUzadi = getListuTipovaUzadi();
        int i =0;
        for(PODACI_TipUzeta tu : listaUzadi){
            if (tu.getOznakaUzeta().equals(s))
                return i;
            i++;
        }
        return nepostoji;

    }


    public void addTipUzeta (PODACI_TipUzeta tipUzeta){
        mDatabase.insert(BazaUzadi.NASLOV_TABELE,
                null, ucitajJedanDogRadiUpisaUBazu(tipUzeta ));
    }

    public void updateTipUzeta(PODACI_TipUzeta tipUzeta){
        mDatabase.update(BazaUzadi.NASLOV_TABELE, //u koju tabelu upisujes
                ucitajJedanDogRadiUpisaUBazu(tipUzeta),        //sta upisujed
                BazaUzadi.Kolona.KOL_NAZIV + " = ?",   // u koji red upisujes
                new String[] { tipUzeta.getOznakaUzeta() } );     //ono sto ce da stoji iznad umesto ?
    }

    public void deleteTipUzeta (PODACI_TipUzeta tipUzeta){
        mDatabase.delete(BazaUzadi.NASLOV_TABELE, //u koju tabelu upisujes
                BazaUzadi.Kolona.KOL_NAZIV + " = ?",   // koji red brises
                new String[] { tipUzeta.getOznakaUzeta() });
    }

    public boolean isDebuggable(Context context){
        if (BuildConfig.AUTO_DISABLE_SEC){ // !@# 14.2.18
            return false; // iskljucena je provera
        }

        return (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;

    }

    private static ContentValues ucitajJedanDogRadiUpisaUBazu(PODACI_TipUzeta tipUzeta) {
        ContentValues values = new ContentValues();
        values.put(BazaUzadi.Kolona.KOL_NAZIV, tipUzeta.getOznakaUzeta());
        values.put(BazaUzadi.Kolona.KOL_PRESEK, tipUzeta.getPresek());
        values.put(BazaUzadi.Kolona.KOL_PRECNIK, tipUzeta.getPrecnik());
        values.put(BazaUzadi.Kolona.KOL_TEZINA, tipUzeta.getSpecTezina());
        values.put(BazaUzadi.Kolona.KOL_E, tipUzeta.getModulElast());
        values.put(BazaUzadi.Kolona.KOL_ALFA, tipUzeta.getKoefAlfa());
        values.put(BazaUzadi.Kolona.KOL_I, tipUzeta.getI());
        values.put(BazaUzadi.Kolona.KOL_CENA, tipUzeta.getJcena());
        values.put(BazaUzadi.Kolona.KOL_IZMENLJIVO, tipUzeta.getIzmenljivo()?1:0);
        return values;
    }
}
