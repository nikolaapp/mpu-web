package nsp.mpu.database;

import java.util.UUID;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.pomocne_klase.StaticPodaci;

@Obfuscate
public class PODACI_TipIzolatora {
    private UUID mId;
    private String mMaterijal;
    private String mTip;
    private StaticPodaci.NaponskiNivo mNapon;
    private double mDuzina;
    private double mTezina ;
    private boolean mIzmenljivo;
    private float mJcena;


    public PODACI_TipIzolatora(String materijal, String tip, StaticPodaci.NaponskiNivo napon,
                               double duzina, double tezina, float cena, boolean izmenljivo) {

        mId = UUID.randomUUID();
        mMaterijal = materijal;
        mTip = tip;
        mNapon = napon;
        mDuzina = duzina;
        mTezina = tezina;
        mJcena = cena;
        mIzmenljivo = izmenljivo;
    }

    public PODACI_TipIzolatora(String materijal, String tip, StaticPodaci.NaponskiNivo napon,
                               double duzina, double tezina, boolean izmenljivo) {

        mId = UUID.randomUUID();
        mMaterijal = materijal;
        mTip = tip;
        mNapon = napon;
        mDuzina = duzina;
        mTezina = tezina;
        mJcena = 0;
        mIzmenljivo = izmenljivo;
    }

    @Override
    public String toString(){
        return getMaterijal()
                +"\ntip "+ getOznaka()
                +"\nnapon "+ getNapon()
                +"\nduzina "+ getDuzina()
                +"\ntezina "+ getTezina()
                +"\njcena "+ getJcena()
                +"\nbool "+ getIzmenljivo()
                ;
    }

    public float getJcena() {
        return mJcena;
    }

    public void setJcena(float jcena) {
        mJcena = jcena;
    }

    public String getMaterijal() {
        return mMaterijal;
    }

    public String getOznaka() {
        return mTip;
    }

    public StaticPodaci.NaponskiNivo getNapon() {
        return mNapon;
    }

    public double getDuzina() {
        return mDuzina;
    }

    public double getTezina() {
        return mTezina;
    }


    public boolean getIzmenljivo() {
        return mIzmenljivo;
    }
}

