package nsp.mpu.database;

import java.util.UUID;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.pomocne_klase.StaticPodaci;

@Obfuscate
public class PODACI_TipKonzole {
    private UUID mId;
    private StaticPodaci.MaterijalStuba mMaterijal;
    private String mOznaka;
    private double mH12 ;
    private double mH23 ;
    private double mD1  ;
    private double mD2  ;
    private double mD3  ;
    private float mJcena  ;
    private boolean mIzmenljivo;

    public  PODACI_TipKonzole(StaticPodaci.MaterijalStuba materijal, String naziv, double h12, double h23,
                              double d1, double d2, double d3, boolean izmenljivo) {
        mId = UUID.randomUUID();
        mMaterijal = materijal;
        mOznaka = naziv;
        mH12 = h12;
        mH23 = h23;
        mD1 = d1;
        mD2 = d2;
        mD3 = d3;
        mJcena = 0;
        mIzmenljivo = izmenljivo;
    }

    public  PODACI_TipKonzole(StaticPodaci.MaterijalStuba materijal, String naziv, double h12, double h23,
                             double d1, double d2, double d3, float jcena, boolean izmenljivo) {
        mId = UUID.randomUUID();
        mMaterijal = materijal;
        mOznaka = naziv;
        mH12 = h12;
        mH23 = h23;
        mD1 = d1;
        mD2 = d2;
        mD3 = d3;
        mJcena = jcena;
        mIzmenljivo = izmenljivo;
    }

    @Override
    public String toString(){
        return getMaterijal()
                +"\ntip "+ getOznaka()
                +"\nh12 "+ getH12()
                +"\nh23 "+ getH23()
                +"\nd1 "+ getD1()
                +"\nd2 "+ getD2()
                +"\nd3 "+ getD3()
                +"\njcena "+ getJcena()
                +"\nbool "+ getIzmenljivo()
                ;
    }

    public float getJcena() {
        return mJcena;
    }

    public void setJcena(float jcena) {
        mJcena = jcena;
    }

    public StaticPodaci.MaterijalStuba getMaterijal() {
        return mMaterijal;
    }

    public String getOznaka() {
        return mOznaka;
    }

    public double getH12() {
        return mH12;
    }

    public double getH23() {
        return mH23;
    }

    public double getD1() {
        return mD1;
    }

    public double getD2() {
        return mD2;
    }

    public double getD3() {
        return mD3;
    }

    public boolean getIzmenljivo() {
        return mIzmenljivo;
    }
}

