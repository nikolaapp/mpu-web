package nsp.mpu.database;

import java.util.UUID;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.pomocne_klase.StaticPodaci;

@Obfuscate
public class PODACI_TipStuba {
    private UUID mId;
    private StaticPodaci.MaterijalStuba mMaterijal;
    private StaticPodaci.Tip_Stuba mTip;
    private String mOznaka;
    private double mDuzina;
    private double mDubina;
    private double mPrecnikManji;
    private double mPrecnikVeci;
    private double mSila;
    private boolean mIzmenljivo;
    private float mJcena, mIskop, mBeton;

    public PODACI_TipStuba(StaticPodaci.MaterijalStuba materijal, StaticPodaci.Tip_Stuba tip, String oznaka,
                           double duzina, double dubina, double precnikVeci, double precnikManji,  double sila, boolean izmenljivo) {
        mId = UUID.randomUUID();
        mMaterijal = materijal;
        mTip = tip;
        mOznaka = oznaka;
        mDuzina = duzina;
        mDubina = dubina;
        mPrecnikVeci = precnikVeci;
        mPrecnikManji = precnikManji;
        mSila = sila;
        mIzmenljivo = izmenljivo;
        mJcena =0;
        mIskop=0;
        mBeton=0;
    }


    public PODACI_TipStuba(StaticPodaci.MaterijalStuba materijal, StaticPodaci.Tip_Stuba tip, String oznaka,
                           double duzina, double dubina, double precnikVeci, double precnikManji,  double sila,
                           float jcena,float iskop, float beton, boolean izmenljivo) {
        mId = UUID.randomUUID();
        mMaterijal = materijal;
        mTip = tip;
        mOznaka = oznaka;
        mDuzina = duzina;
        mDubina = dubina;
        mPrecnikVeci = precnikVeci;
        mPrecnikManji = precnikManji;
        mSila = sila;
        mIzmenljivo = izmenljivo;
        mJcena =jcena;
        mIskop=iskop;
        mBeton=beton;
    }

    @Override
    public String toString(){
        return getMaterijal()
                +"\ntip "+ getTip()
                +"\noznaka "+ getOznaka()
                +"\nduzina "+ getDuzina()
                +"\nduzina "+ getDubina()
                +"\nD "+ getPrecnikVeci()
                +"\nd "+ getPrecnikManji()
                +"\nF "+ getSila()
                +"\ncena "+ getJcena()
                +"\niskop "+ getIskop()
                +"\nbeton "+ getBeton()
                +"\nbool "+ getIzmenljivo()
                ;
    }

    public float getJcena() {
        return mJcena;
    }

    public void setJcena(float jcena) {
        mJcena = jcena;
    }

    public float getIskop() {
        return mIskop;
    }

    public void setIskop(float iskop) {
        mIskop = iskop;
    }

    public float getBeton() {
        return mBeton;
    }

    public void setBeton(float beton) {
        mBeton = beton;
    }

    public StaticPodaci.MaterijalStuba getMaterijal() {
        return mMaterijal;
    }

    public StaticPodaci.Tip_Stuba getTip() {
        return mTip;
    }

    public String getOznaka() {
        return mOznaka;
    }

    public double getDuzina() {
        return mDuzina;
    }

    public double getDubina() {
        return mDubina;
    }

    public double getPrecnikManji() {
        return mPrecnikManji;
    }

    public double getPrecnikVeci() {
        return mPrecnikVeci;
    }

    public double getSila() {
        return mSila;
    }

    public boolean getIzmenljivo() {
        return mIzmenljivo;
    }
}

