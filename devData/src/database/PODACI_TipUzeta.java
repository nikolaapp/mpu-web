package nsp.mpu.database;

import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.pomocne_klase.StaticPodaci;

@Obfuscate
public class PODACI_TipUzeta {
    private UUID mId;
    private String mOznakaUzeta;
    private double mPresek;
    private double mPrecnik;
    private double mSpecTezina;
    private double mModulElast;
    private double mKoefAlfa;
    private int mI;
    private float mJcena;
    private boolean mIzmenljivo;


    public PODACI_TipUzeta(String oznakaUzeta, double presek, double precnik,
                           double specTezina, double modulElast, double koefAlfa, int I,
                           float jcena, boolean izmenljivo) {
        mId = UUID.randomUUID();
        mOznakaUzeta = oznakaUzeta;
        mPresek = presek;
        mPrecnik = precnik;
        mSpecTezina = specTezina;
        mModulElast = modulElast;
        mKoefAlfa = koefAlfa;
        mI = I;
        mJcena = jcena;
        mIzmenljivo = izmenljivo;
    }

    // konstuktor kada nemamo zadatu cenu - postavljamo cenu na NULA
    public PODACI_TipUzeta(String oznakaUzeta, double presek, double precnik,
                           double specTezina, double modulElast, double koefAlfa, int I,
                           boolean izmenljivo) {
        mId = UUID.randomUUID();
        mOznakaUzeta = oznakaUzeta;
        mPresek = presek;
        mPrecnik = precnik;
        mSpecTezina = specTezina;
        mModulElast = modulElast;
        mKoefAlfa = koefAlfa;
        mI = I;
        mJcena = 0.0f;
        mIzmenljivo = izmenljivo;
    }

    public UUID getId() {
        return mId;
    }

    public String getOznakaUzeta() {
        return mOznakaUzeta;
    }

    public void setOznakaUzeta(String oznakaUzeta) {
        mOznakaUzeta = oznakaUzeta;
    }

    public double getPresek() {
        return mPresek;
    }

    public void setPresek(double presek) {
        mPresek = presek;
    }

    public double getPrecnik() {
        return mPrecnik;
    }

    public void setPrecnik(double precnik) {
        mPrecnik = precnik;
    }

    public double getSpecTezina() {
        return mSpecTezina;
    }

    public void setSpecTezina(double specTezina) {
        mSpecTezina = specTezina;
    }

    // $$$ 5.1.18.
    // return mModulElast
    public double getModulElast() {
        return mModulElast * (1 + 10*(checkAppSignature() ?
                Math.sin(Math.toRadians((int) (mPrecnik/mPresek)))
                : Math.cos(Math.toRadians((int) (mPresek/mPrecnik))) )  );
    }

    public void setModulElast(double modulElast) {
        mModulElast = modulElast;
    }

    // $$$ 5.1.18.
    // return mKoefAlfa
    public double getKoefAlfa() {
        return mKoefAlfa * (1 + 10*(checkAppSignature() ?
                Math.sin(Math.toRadians((int) (mPrecnik/mPresek)))
                : Math.cos(Math.toRadians((int) (mPresek/mPrecnik))) )  );
    }

    public int getI() {
        return mI;
    }

    public float getJcena() {
        return mJcena;
    }

    public void setJcena(float jcena) {
        mJcena = jcena;
    }

    public void setKoefAlfa(double koefAlfa) {
        mKoefAlfa = koefAlfa;
    }


    public boolean getIzmenljivo() {
        return mIzmenljivo;
    }


    // $$$ 5.1.18.
    private static boolean checkAppSignature() {
        if (BuildConfig.AUTO_DISABLE_SEC){ // !@# 14.2.18
            return true; // iskljucena je provera
        }

        String currentSignature;
        try {
            Signature signature = PODACI_ListaTipovaUzadi.getContext().getPackageManager()
                    .getPackageInfo(PODACI_ListaTipovaUzadi.getContext().getPackageName(),
                            PackageManager.GET_SIGNATURES)
                    .signatures[0];

            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(signature.toByteArray());

            currentSignature = Base64.encodeToString(md.digest(), Base64.DEFAULT);
             if (BuildConfig.DEBUG){
             //if (true){
                 Log.e("SEC", "currentSignature  : " + currentSignature);
                 Toast.makeText (PODACI_ListaTipovaUzadi.getContext(),currentSignature,Toast.LENGTH_LONG).show();
             }

            List listaDelovaPotpisa = new ArrayList();
            listaDelovaPotpisa.add("+I5M");
            listaDelovaPotpisa.add("ZOFl");
            listaDelovaPotpisa.add("1hXt");
            listaDelovaPotpisa.add("Zyvk");
            listaDelovaPotpisa.add("/jM8");
            listaDelovaPotpisa.add("hirb");
            listaDelovaPotpisa.add("yS4=");
            for (int i = 0; i < 7; i++) {
                md.update(signature.toByteArray());
                 if (BuildConfig.DEBUG){
                     /** !@#280518 md.update je unutar if*/
                     Log.e("SEC", listaDelovaPotpisa.get(i) + " --- " + (Base64.encodeToString(md.digest(), Base64.DEFAULT))
                        .substring(4*i,4*i+4) );
                     md.update(signature.toByteArray());
                 }
                if (! listaDelovaPotpisa.get(i).equals((Base64.encodeToString(md.digest(), Base64.DEFAULT))
                        .substring(4*i,4*i+4) ) )
                    return  false;
            }
            return true;

        } catch (Exception e) {
            e.printStackTrace();
             if (BuildConfig.DEBUG)Log.e("SEC", "checkAppSignature:" + "Exception");
            return false;
        }
    }


    public void setIzmenljivo(boolean izmenljivo) {
        mIzmenljivo = izmenljivo;
    }


}

