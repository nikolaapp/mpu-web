package nsp.mpu.pomocne_klase;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;

@Obfuscate
public abstract class ActivityZaJedanFragment extends AppCompatActivity {

    protected abstract Fragment KreirajFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(postaviNovLayout());

        // pokreni FragmentManager
        FragmentManager menager = getSupportFragmentManager();

        // kreiraj i povezi fragment sa aktivnosti preko menagera
        Fragment f = menager.findFragmentById(R.id.activity_prikaz_fragmenta);
        if (f==null) {
            f = KreirajFragment(); // nema NEW
            menager.beginTransaction()
                    .setCustomAnimations(
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_IN(),
                            AnimacijeKlasa.odaberiAnimaciju_fragment_OUT()
                    )
                    .add(R.id.activity_prikaz_fragmenta, f).commit();
        }
    }

    // omogucava da subklase preko @Override promene layout koji ce biti prikazan
    @LayoutRes
    protected int postaviNovLayout(){
        return R.layout.activity_prikaz_fragmenta;
    }
}
