package nsp.mpu.pomocne_klase;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LayoutAnimationController;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;

/**
 * Created by Nikola on 5.3.2018.
 */

@Obfuscate
public abstract class AnimacijeKlasa {

    public static int odaberiAnimaciju_alertDialog(){
        List<Integer> listaAnimacijaDialoga = new ArrayList<>();
        listaAnimacijaDialoga.add(R.style.DialogAnimation0);
        listaAnimacijaDialoga.add(R.style.DialogAnimation1);
        listaAnimacijaDialoga.add(R.style.DialogAnimation2);
        listaAnimacijaDialoga.add(R.style.DialogAnimation3);

        return listaAnimacijaDialoga.get(new Random().nextInt(listaAnimacijaDialoga.size()));
    }

    public static Animation getButtonRotAnim(Context context){
        return AnimationUtils.loadAnimation(context, R.anim.anim_rotate_inf);
    }
    public static Animation odaberiAnimaciju_button(Context context){
        List<Animation> listaAnimacija_button = new ArrayList<>();
        Animation animTranslate = AnimationUtils.loadAnimation(context, R.anim.anim_translate);
        Animation animAlpha = AnimationUtils.loadAnimation(context, R.anim.anim_alpha);
        Animation animScale = AnimationUtils.loadAnimation(context, R.anim.anim_scale);
        Animation animRotate = AnimationUtils.loadAnimation(context, R.anim.anim_rotate);
        Animation animRotateInf = AnimationUtils.loadAnimation(context, R.anim.anim_rotate_inf);
        Animation animBounce = AnimationUtils.loadAnimation(context, R.anim.button_bounce);
                  animBounce.setInterpolator(new Interpolator() {
            double mAmplitude = 0.2;
            double mFrequency = 20;
            @Override
            public float getInterpolation(float time) {
                return (float) (-1 * Math.pow(Math.E, -time/ mAmplitude) *
                        Math.cos(mFrequency * time) + 1);
            }
        });

        listaAnimacija_button.add(animTranslate);
        listaAnimacija_button.add(animAlpha);
        listaAnimacija_button.add(animScale);
        listaAnimacija_button.add(animRotate);
        listaAnimacija_button.add(animBounce);
        //listaAnimacija_button.add(animRotateInf); // samo za app info

        return listaAnimacija_button.get(new Random().nextInt( listaAnimacija_button.size() ));
    }

    public static int odaberiAnimaciju_fragment_IN(){
        List<Integer> listaAnimacija_fragment_IN = new ArrayList<>();
        listaAnimacija_fragment_IN.add(R.anim.act_in_right);
        listaAnimacija_fragment_IN.add(R.anim.act_in_bottom);
        listaAnimacija_fragment_IN.add(R.anim.act_in_scale);
        listaAnimacija_fragment_IN.add(R.anim.act_in_fade);

        return listaAnimacija_fragment_IN.get(new Random().nextInt(listaAnimacija_fragment_IN.size()));
    }
    public static int odaberiAnimaciju_fragment_OUT(){
        List<Integer> listaAnimacija_fragment_OUT = new ArrayList<>();
        listaAnimacija_fragment_OUT.add(R.anim.act_out_left);
        listaAnimacija_fragment_OUT.add(R.anim.act_out_top);
        listaAnimacija_fragment_OUT.add(R.anim.act_out_scale);
        listaAnimacija_fragment_OUT.add(R.anim.act_out_fade);

        return listaAnimacija_fragment_OUT.get(new Random().nextInt(listaAnimacija_fragment_OUT.size()));
    }
    @Obfuscate//import
    public static class ParAnimacijaInOut{
        public int in;
        public int out;

        public ParAnimacijaInOut(int in, int out) {
            this.in = in;
            this.out = out;
        }

        public ParAnimacijaInOut() {
            this.in =  odaberiAnimaciju_fragment_IN();
            this.out = odaberiAnimaciju_fragment_OUT();
        }
    }

    public static void animacijaZaRecycleView(final RecyclerView recyclerView, boolean isAnimating) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.recycleview_layout_animation_fall_down);

        int[] niz = new int[]{
                LayoutAnimationController.ORDER_NORMAL,
                LayoutAnimationController.ORDER_REVERSE,
                LayoutAnimationController.ORDER_RANDOM
        };
        controller.setOrder(new Random().nextInt(niz.length));

        if (isAnimating) {
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }

    public static void animirajTekst(final TextView textView){
        final int bojaPom = textView.getCurrentTextColor();
        textView.setAlpha(0);
        textView.setTextColor(Color.RED);
        textView.animate()
                .alphaBy(1)
                .rotationX(360)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        textView.setTextColor(bojaPom);
                        //animirajTekst(textView);
                    }
                })
                .setDuration(2000)
                .start();
    }
    public static void animirajTekst1(final TextView textView){
        textView.animate()
                .rotationX(360)
                .setDuration(5000)
                .setInterpolator(odaberiInterpolator())
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        textView.animate().rotationY(360);
                    }
                })
                .start();
    }
    public static void animirajTekst2(final TextView textView){
        final int bojaPom = textView.getCurrentTextColor();
        textView.setAlpha(0);
        textView.setTextColor(Color.RED);
        textView.animate()
                .alphaBy(1)
                .rotationX(360)
                .setDuration(2000)
                .start();
    }
    public static void animirajTekst_running(Context context ,final TextView textView, String string){
        textView.setText(string);
        textView.setAnimation(AnimationUtils.loadAnimation(context,R.anim.anim_text_running));
        textView.animate().setStartDelay(3000)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText("°C");
                    }
                })
                .start();

    }


    private static TimeInterpolator odaberiInterpolator() {
        List<TimeInterpolator> listaInterpolatora = new ArrayList<>();
        listaInterpolatora.add(new AccelerateDecelerateInterpolator());
        listaInterpolatora.add(new AccelerateInterpolator());
        listaInterpolatora.add(new AnticipateInterpolator());
        listaInterpolatora.add(new AnticipateOvershootInterpolator());
        listaInterpolatora.add(new BounceInterpolator());
        listaInterpolatora.add(new DecelerateInterpolator());
        listaInterpolatora.add(new LinearInterpolator());
        listaInterpolatora.add(new OvershootInterpolator());

        return listaInterpolatora.get(new Random().nextInt(listaInterpolatora.size()));
    }
    /** view animacije - textview, linearlayout... */
    private static final int trajanjeAnimacije = 1000;
    public static void pokreniAnimaciju_toVisible(final View v){
        v.setAlpha(0f);
        v.setVisibility(View.VISIBLE);
        v.animate()
                .setInterpolator(odaberiInterpolator())
                .scaleX(1f)
                .scaleY(1f)
                .translationX(0)
                .translationY(0)
                .rotationX(0)
                .rotationY(0)
                .alpha(1.0f)
                .setDuration(trajanjeAnimacije)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.animate().setListener(null);
                    }
                })
        ;
    }
    public static void pokreniAnimaciju_toGone(final View v, final boolean invisible){
        v.animate()
                .setInterpolator(odaberiInterpolator())
                .scaleX(0.1f)
                .scaleY(0.1f)
                .translationX((new Random().nextInt(3)-1) * v.getWidth())
                .translationY((new Random().nextInt(3)-1) * v.getHeight())
                .rotationX(new Random().nextInt(180) -90)
                .rotationY(new Random().nextInt(180) -90)
                .alpha(0.0f)
                .setDuration(trajanjeAnimacije)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (invisible)
                            v.setVisibility(View.INVISIBLE);
                        else
                            v.setVisibility(View.GONE);
                        v.animate().setListener(null);
                    }
                })
        ;
    }
    public static void pokreniAnimaciju_zamena(final View vPrikazi, final View vSakrij){
        vSakrij.animate()
                .setInterpolator(odaberiInterpolator())
                .scaleX(0.1f)
                .scaleY(0.1f)
                .translationX((new Random().nextInt(3)-1) * vSakrij.getWidth())
                .translationY((new Random().nextInt(3)-1) * vSakrij.getHeight())
                .rotationX(new Random().nextInt(180) -90)
                .rotationY(new Random().nextInt(180) -90)
                .alpha(0.0f)
                .setDuration(trajanjeAnimacije)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        vSakrij.setVisibility(View.GONE);
                        vSakrij.animate().setListener(null);
                        pokreniAnimaciju_toVisible(vPrikazi);
                    }
                })
        ;
    }
}
