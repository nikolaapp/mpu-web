package nsp.mpu.pomocne_klase;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;

import static nsp.mpu.pomocne_klase.StaticPodaci.prikaziToast;

@Obfuscate
public abstract class AppSecurity{
//    private static final String mojSIGNATURE_relese_cired = "+I5MZOFl1hXtZyvk/jM8hirbyS4=";
//    private static final String mojSIGNATURE_debug1 = "5Rhi"; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_debug2 = "R/Wa"; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_debug3 = "C/79"; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_debug4 = "gwun"; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_debug5 = "f+Ai"; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_debug6 = "lL2R"; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_debug7 = "rRI="; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_relese1 = "+I5M"; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_relese2 = "ZOFl"; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_relese3 = "1hXt"; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_relese4 = "Zyvk"; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_relese5 = "/jM8"; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_relese6 = "hirb"; // $$$ 5.1.18.
//    private static final String mojSIGNATURE_relese7 = "yS4="; // $$$ 5.1.18.

    static Context mContext;

    public static boolean proveraZastite(Context context){

        mContext=context;

        if(BuildConfig.AUTO_DISABLE_SEC){
            // !@# 14.2.18
            Toast.makeText(mContext,"AUTO_DISABLE_SEC je aktivan, zastita je blokirana", Toast.LENGTH_LONG).show();
            return false;
        }

        if (BuildConfig.DEBUG) {
        //if (true) {
             if (BuildConfig.DEBUG)Log.e("SEC", "isDebuggable: " + isDebuggable(context)
                    + "  checkAppSignature: " + checkAppSignature(context));

            prikaziToast(context, "isDebuggable: " + (isDebuggable(context)?"debug":"OK")
                    + "  checkAppSignature: " + (checkAppSignature(context)?"OK":"sign netacan"));
        }

        return ( (!isDebuggable(context)) && (checkAppSignature()) );
    }

    public  static boolean checkAppSignature(){
        return checkAppSignature(mContext);
    }

    private static boolean checkAppSignature(Context context) {
        String currentSignature;

        try {
            Signature signature = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(),
                            PackageManager.GET_SIGNATURES)
                                .signatures[0];

            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(signature.toByteArray());

            currentSignature = Base64.encodeToString(md.digest(), Base64.DEFAULT);
             if (BuildConfig.DEBUG)Log.e("SEC", "currentSignature  : " + currentSignature);

            List listaDelovaPotpisa = new ArrayList();
            listaDelovaPotpisa.add("+I5M");
            listaDelovaPotpisa.add("ZOFl");
            listaDelovaPotpisa.add("1hXt");
            listaDelovaPotpisa.add("Zyvk");
            listaDelovaPotpisa.add("/jM8");
            listaDelovaPotpisa.add("hirb");
            listaDelovaPotpisa.add("yS4=");
            for (int i = 0; i < 7; i++) {
                md.update(signature.toByteArray());
                 if (BuildConfig.DEBUG){
                     /** !@#280518 md.update je unutar if*/
                     Log.e("SEC", listaDelovaPotpisa.get(i) + " --- " + (Base64.encodeToString(md.digest(), Base64.DEFAULT))
                             .substring(4*i,4*i+4) );
                     md.update(signature.toByteArray());
                 }
                if (! listaDelovaPotpisa.get(i).equals((Base64.encodeToString(md.digest(), Base64.DEFAULT))
                        .substring(4*i,4*i+4) ) )
                    return  false;
            }
            return true;

            /**
             * mojOrigString = isDebuggable(context) ? mojSIGNATURE_debug : mojSIGNATURE_relese;
             * jer ako je debag onda ce takodje potpis biti razlicit od originalnog(release)
             */
           /* mojOrigString=mojSIGNATURE_relese;
            if (currentSignature.trim().equals(mojOrigString))
                return true;*/

        } catch (Exception e) {
            e.printStackTrace();
             if (BuildConfig.DEBUG)Log.e("SEC", "checkAppSignature:" + "Exception");
            return false;
        }

        /*
         if (BuildConfig.DEBUG)Log.e("SEC", "mojOrigString: " + mojOrigString);
         if (BuildConfig.DEBUG)Log.e("SEC", "currentSignature  : " + currentSignature);
        return false;
        */
    }

    public static boolean isDebuggable(Context context){
        //return true ako JESTE debug
        return (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        //return  false;
    }
}
