package nsp.mpu.pomocne_klase;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.R;

import static nsp.mpu._0MainActivity.mDaLiJeTablet;
import static nsp.mpu.pomocne_klase.AnimacijeKlasa.odaberiAnimaciju_alertDialog;
import static nsp.mpu.pomocne_klase.StaticPodaci.mojHtmlString;

@Obfuscate
public class DialogFragment_main_activity extends DialogFragment {

    public interface Callback {
        // preko ovoga prekidamo animaciju AppInfo !@#1.1.18
        void callbackFunc();//or whatever args you want
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
        ad.getWindow().setWindowAnimations(odaberiAnimaciju_alertDialog());

        ad.setTitle(mojHtmlString("<b>MPU " + ((AppCompatActivity)getActivity()).getSupportActionBar().getSubtitle()));

        ad.setIcon(R.mipmap.ikonica_krug_crveno_plavi_zoom);

        String s1 = "Za dodatne informacije ";
        String s2 = "obratite se autoru ";
        String s3 = "aplikacije na mejl:\n ";
        SpannableString s = new SpannableString(s1+s2+s3+"nikola.app.mpu@gmail.com");
        Linkify.addLinks(s, Linkify.EMAIL_ADDRESSES);
        ad.setMessage(s);

        ad.getWindow().getAttributes().verticalMargin = -0.15F;
        //ad.getWindow().setGravity(Gravity.TOP);

        ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });

        ad.show();

        ( (TextView) ad.findViewById(android.R.id.message)) . setTextIsSelectable(true); //mora posle .show

////////////////////postavka pozadine i okvira////////////////////////////
        Drawable drawable;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
            drawable = getResources().getDrawable(R.drawable.button_round, null);
        else
            drawable = getResources().getDrawable(R.drawable.button_round);

        ad.getWindow().setBackgroundDrawable(drawable); //mora posle .show

/////////////////////////centriranje OK na sredinu//////////////////////////////////

        Button positiveButton = ad.getButton(AlertDialog.BUTTON_POSITIVE);
        LinearLayout.LayoutParams parametri = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
        parametri.width = ViewGroup.LayoutParams.MATCH_PARENT;
        positiveButton.setLayoutParams(parametri);

//////////////////////////////////////////////////////////////////////////
        if (mDaLiJeTablet)
            ((TextView) ad.findViewById(android.R.id.message)).setTextSize(20);
        else
            // 6.0.2
            // omogucava slanje mejla klikom na hyperlink
            // na tabletu api_17 bi javio gresku jer nema email client
            //     --- na emulatoru ne prijavljuje gresku, ali tu i ne treba slanje mejla
            ((TextView) ad.findViewById(android.R.id.message))
                    .setMovementMethod(LinkMovementMethod.getInstance());


        return ad;
    }

    @Override
    public void onDismiss(DialogInterface dialog){
        // kada preknemo dialog onda pozivamo funkciju definisanu preko callback
        Activity activity = getActivity();
        if(activity instanceof Callback)
            ((Callback)activity).callbackFunc();
        super.onDismiss(dialog);
    }
}