package nsp.mpu.pomocne_klase;

import android.util.Log;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.database.PODACI_TipUzeta;

import static java.lang.Math.pow;

@Obfuscate
public class JednacinaStanja {

    private double mSigmaNOVO, mKoefA, mKoefB, mAkr, mUgib;

    private PODACI_TipUzeta mTipUzeta;
    private double sigmaMax, raspon, cosFi,  koefDodOpt;
    private int t1,t0;
    private double mRezTezinaUzeta; //koristi samo UgibX

    public static final int TEMP_MINUS5_BEZ_LEDA = -55;
    public static final int NIJE_SETOVANO_T0 = -105;
    //public static int TEMPERATURA_ZA_KOJU_SE_NE_RACUNA_UGIB = -100;


    //kostruktor BEZ postavljanja temperature t0
    public JednacinaStanja(PODACI_TipUzeta TipUzeta, double sigmaMax, double raspon,
                           double cosFi, double koefDodOpt, int t1) {
        this(TipUzeta, sigmaMax, raspon, cosFi, koefDodOpt, t1, NIJE_SETOVANO_T0);
    }

    //kostruktor za postavljanje temperature t0
    public JednacinaStanja(PODACI_TipUzeta TipUzeta, double sigmaMax, double raspon,
                           double cosFi, double koefDodOpt, int t1, int t0) {
        this.mTipUzeta = TipUzeta;
        this.sigmaMax = sigmaMax;
        this.raspon = raspon;
        this.cosFi = cosFi;
        this.koefDodOpt = koefDodOpt;
        this.t1 = t1;
        this.t0 = t0;

         if (BuildConfig.DEBUG)Log.e("TAGjedStanja","mTipUzeta-"+mTipUzeta.getOznakaUzeta() +
                " sigma0-"+sigmaMax +
                " raspon-"+raspon +
                " cosFi-"+cosFi +
                " koefDodOpt-"+koefDodOpt +
                " t1-"+t1 +
                " t0-"+t0);

        postaviJednacinuStanja();
    }

    public PODACI_TipUzeta getTipUzeta() {
        return mTipUzeta;
    }

    public double getSigma0() {
        return sigmaMax;
    }

    public double getRaspon() {
        return raspon;
    }

    public double getKoefDodOpt() {
        return koefDodOpt;
    }

    public int getTemperatura() {
        return t1;
    }

    public double getSigmaNOVO() {
        return mSigmaNOVO;
    }

    public double getKoefA() {
        return mKoefA;
    }

    public double getKoefB() {
        return mKoefB;
    }

    public double getAkr() {
        return mAkr;
    }

    public double getUgib() {
        return mUgib;
    }

    public double getUgibX(double b) {
        return UgibUcm (raspon, cosFi, mSigmaNOVO, mRezTezinaUzeta, b);
    }

    private void postaviJednacinuStanja() {
        double alfa = mTipUzeta.getKoefAlfa();
        double E = mTipUzeta.getModulElast();
        double optUzeta = mTipUzeta.getSpecTezina();

        double mDodOpt = DodatnoOptSnegLed(koefDodOpt,
                mTipUzeta.getPrecnik(), mTipUzeta.getPresek());

        mAkr = KriticanRaspon(sigmaMax, cosFi, alfa, optUzeta+mDodOpt, optUzeta);

        double opterecenje0, opterecenje1;

        //////////////////// parameti pocetnog stanja
        if (t0==NIJE_SETOVANO_T0) {
            if (raspon > mAkr) {
                t0 = -5;
                opterecenje0 = optUzeta + mDodOpt;
                } else {
                t0 = -20;
                opterecenje0 = optUzeta;
                }
        }else // t0 je setovano u construktoru
            opterecenje0 = optUzeta; // bez snega i leda


        //////////////////// parameti krajnjeg stanja
        if (t1 == -5)
            opterecenje1 = optUzeta + mDodOpt;
        else if (t1 == TEMP_MINUS5_BEZ_LEDA) { // proracun za bez leda
            t1 = -5;
            opterecenje1 = optUzeta;
        }
        else opterecenje1 = optUzeta;
        ///////////////////////////

        mRezTezinaUzeta=opterecenje1;

        mKoefA = (t1 - t0) * alfa * E * cosFi
                + (raspon*raspon * opterecenje0*opterecenje0 * pow(cosFi, 3) * E)
                / (24 * sigmaMax * sigmaMax)
                - sigmaMax;

        mKoefB = raspon*raspon * opterecenje1*opterecenje1 *
                pow(cosFi, 3) * E / 24;

        //koef B se racuna na osnovu jedn x3+Ax2=B
        //a posto je NRmetoda x3 + Ax2 + B = 0
        //onda stavljamo -mKoefB
        mSigmaNOVO = NewtonRaphsonMetoda(mKoefA,-mKoefB, sigmaMax);

        mUgib = UgibUcm ( raspon,  cosFi,  mSigmaNOVO,  opterecenje1);
    }

    private static double NewtonRaphsonMetoda (double A, double B, double sigma0){
// x^3 + A*x^2 + B = 0  UMESTO: - B ---OVAKO JE OPSTIJE
        return NewtonRaphsonMetoda(1,A,B,sigma0);
    }

    private static double NewtonRaphsonMetoda (double A3, double A, double B, double sigma0){
// A3*x^3 + A*x^2 + B = 0
        double epsilon_x=0.00001;
        double epsilon_y=0.00001;
        int Nmax=10000;
        double x=sigma0;
        double dx;
        int brojac_iteracija;
        double vrednost_f, vrednost_izv_f;

        for (brojac_iteracija=1; brojac_iteracija<=Nmax; brojac_iteracija++){

            vrednost_f = A3* pow(x, 3) + A* pow(x, 2) + B;
            vrednost_izv_f = 3*A3* pow(x, 2) + 2*A*x;

            dx = -vrednost_f/vrednost_izv_f;
            x += dx;

            vrednost_f = A3* pow(x, 3) + A* pow(x, 2) + B;

            if ((Math.abs(dx) < epsilon_x) && (Math.abs(vrednost_f) < epsilon_y))
                break;
        }

        return x;
    }

    public static double KriticanRaspon (double KRsigma0, double KRcosFi, double KRalfa,
                                         double optRez, double optUzeta){
        return (KRsigma0 / KRcosFi)
                * Math.sqrt(    (360 * KRalfa) /
                                (optRez*optRez - optUzeta*optUzeta));
    }


    /** !@# 01.03.2018-2 proracun je prema TP10*/
    private static double IdealniRasponStaro (double rasponi[], double cosFi[]){
        double sum_gore=0, sum_dole=0;
        double sum_gore_podKorenom=0, sum_dole_podKorenom=0;
        for (int i = 0; i < rasponi.length; i++) {
            sum_gore += rasponi[i] / pow(cosFi[i],3);
            sum_dole += rasponi[i] / pow(cosFi[i],2);

            sum_gore_podKorenom += pow(rasponi[i],3);
            sum_dole_podKorenom += rasponi[i] / pow(cosFi[i],2);
        }
        return (sum_gore/sum_dole)
                * Math.sqrt(sum_gore_podKorenom/sum_dole_podKorenom);
    }
    /** !@# 02.05.2018 u skladu sa --- Milenko Djuric - Elementi EES-a 2009  pages34-48*/
    public static double IdealniRaspon (double rasponi[], double cosFi[]){
        double sum_gore=0, sum_dole=0;
        for (int i = 0; i < rasponi.length; i++) {
            sum_gore += pow(rasponi[i],3) * pow(cosFi[i],2);
            sum_dole += rasponi[i];
        }
        return (1/IdealniCosFi(rasponi, cosFi)) * Math.sqrt(sum_gore/sum_dole);
    }

    /** !@# 02.05.2018 u skladu sa --- Milenko Djuric - Elementi EES-a 2009  pages34-48*/
    public static double IdealniCosFi (double rasponi[], double cosFi[]){
        double sum_gore=0, sum_dole=0;
        for (int i = 0; i < rasponi.length; i++) {
            sum_gore += rasponi[i];
            sum_dole += rasponi[i]/cosFi[i];
        }
        return sum_gore/sum_dole;
    }

    public static double UgibUcm (double a, double cos, double naprezanje, double tezina){
        return 100 * ((tezina * a*a)/(8*naprezanje*cos)  +
                ( (pow(tezina,3) * pow(a,4) *cos) / (384* pow(naprezanje,3) )));
    }

    public static double UgibUcm (double a, double cos, double naprezanje, double tezina, double b){
        // ugib_na_udaljenosti_b_od_stuba
        double ugib_na_sredini = UgibUcm (a, cos, naprezanje, tezina);
        return 4*b*(a-b)*ugib_na_sredini/(a*a);
    }

    public static double DodatnoOptSnegLed (double koef, double d, double S){
        return koef * 0.18 * Math.sqrt(d) / S;
    }

    public static double naprezanjePrekoUgiba (double a, double cosFi, double ugib_b,
                                               double tezinaUzeta, double b, double sigmaPocetno){
        double ugib_na_sredini = ugib_b*(a*a)/(4*b*(a-b));
        double A3 = cosFi*ugib_na_sredini/100;
        double A = a*a*tezinaUzeta/8;
        double B = pow(a,4) * pow(tezinaUzeta,3) / 384;
        return NewtonRaphsonMetoda(A3,-A,-B,sigmaPocetno);
    }

}