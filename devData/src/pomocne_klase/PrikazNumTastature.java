package nsp.mpu.pomocne_klase;

import android.text.InputType;
import android.text.method.DigitsKeyListener;

import io.michaelrocks.paranoid.Obfuscate;

/**
 * Prikaz numericke umesto standardne tastature za unos brojeva
 * http://stackoverflow.com/questions/2830664/how-do-i-use-inputtype-numberdecimal-with-the-phone-soft-keypad
 * 4.0.1
 */

@Obfuscate
public class PrikazNumTastature extends DigitsKeyListener
    {
        /**
         * @param sign Dozvoljen unos negativnog broja?
         * @param decimal Dozvoljen unos decimalnog broja?
         */
        public PrikazNumTastature(boolean sign, boolean decimal) {
            super(sign, decimal);
        }

        public int getInputType() {
            return InputType.TYPE_CLASS_PHONE;
        }
    }