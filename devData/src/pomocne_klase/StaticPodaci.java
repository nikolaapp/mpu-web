package nsp.mpu.pomocne_klase;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Environment;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.michaelrocks.paranoid.Obfuscate;
import nsp.mpu.BuildConfig;
import nsp.mpu.R;

import static android.widget.Toast.LENGTH_SHORT;
import static nsp.mpu.pomocne_klase.StaticPodaci.MaterijalStuba.betonski;
import static nsp.mpu.pomocne_klase.StaticPodaci.MaterijalStuba.čel_rešet;
import static nsp.mpu.pomocne_klase.StaticPodaci.NaponskiNivo._10kV;
import static nsp.mpu.pomocne_klase.StaticPodaci.NaponskiNivo._110kV;
import static nsp.mpu.pomocne_klase.StaticPodaci.NaponskiNivo._20kV;
import static nsp.mpu.pomocne_klase.StaticPodaci.NaponskiNivo._35kV;
import static nsp.mpu.pomocne_klase.StaticPodaci.Tip_Stuba.krajnji;
import static nsp.mpu.pomocne_klase.StaticPodaci.Tip_Stuba.noseći;
import static nsp.mpu.pomocne_klase.StaticPodaci.Tip_Stuba.zatezni;
import static nsp.mpu.pomocne_klase.StaticPodaci.godDoba.jesen;
import static nsp.mpu.pomocne_klase.StaticPodaci.godDoba.leto;
import static nsp.mpu.pomocne_klase.StaticPodaci.godDoba.prolece;
import static nsp.mpu.pomocne_klase.StaticPodaci.godDoba.zima;

/**
 * Created by Nikola on 4.3.2018.
 */

@Obfuscate
public abstract class StaticPodaci {

    public static int brojPokretanjaApp;

    public final static String tipUzeta_default = "Al/Ce-70/12";


    public final static String stub_12_315 = "12/315";
    public final static String stub_12_1000 = "12/1000";
    public final static String konzola_trougao = "trougao";
    public final static String konzola_vrsna = "vršna";
    public final static String konzola_koso = "ukoso";
    public final static String izolator_nije_odabrano = "nije odabran";

    public final static int nepostoji = -101;

    public final static int temperaturaProv_min = -40;
    public final static int temperaturaProv_max = 200;
    public final static int temperaturaProv_default = 40;

    public static char getDecimalSeparator() {
        /** za englesko govorno podrucje se koristi tacka
         * a za ostale zatez kao decimalni separator */
        char decimalSeparator = ',';

        Locale local = Locale.getDefault();
        if (local.getLanguage() . equals("en"))
            decimalSeparator = '.';

        // ako ovo prodje onde se uzima vrednos definisana iznad
        NumberFormat nf = NumberFormat.getNumberInstance();
        if (nf instanceof DecimalFormat) {
            DecimalFormatSymbols sym = ((DecimalFormat) nf).getDecimalFormatSymbols();
            decimalSeparator = sym.getDecimalSeparator();
             if (BuildConfig.DEBUG)Log.e("TAGdec_sep", "decimalSeparator=  " + decimalSeparator);
        }
        return decimalSeparator;
    }


    /** global  SharedPreferences ***************************/
    private static SharedPreferences mSharedPreferences;
    public static SharedPreferences getSharPref(){
        return mSharedPreferences;
    }
    public static void setSharPref(SharedPreferences sp){
        mSharedPreferences =  sp;
    }


    /** kreiranje foldera ***************************/
    public static File folderUserData;
    public static File kreirajFolder(Context context, File parentFolder, String newChildFolder){
        boolean uspesnoKreiranFolder;
        File defaultFolder = Environment.getExternalStorageDirectory();
        String tekst;

        newChildFolder = newChildFolder.replaceAll("[\\\\/:*?\"<>|] ", "_");

        /** provera da li postoji/moze da se napravi parentFolder */
        if ( ! parentFolder.exists() || ! parentFolder.isDirectory()) {
            uspesnoKreiranFolder = parentFolder.mkdirs();
            if ( ! uspesnoKreiranFolder) {
                tekst = "GRESKA ne moze da se kreira parent folder: " + parentFolder;
                prikaziWarnToast(context, tekst);
                 if (BuildConfig.DEBUG)Log.e("TAGroot", tekst);
                return defaultFolder;
            }
        }

        File noviFolder = new File(parentFolder + File.separator + newChildFolder);

        /** provera da li postoji/moze da se napravi newChildFolder */
        if ( ! noviFolder.exists() || ! noviFolder.isDirectory()) {
            uspesnoKreiranFolder = noviFolder.mkdir(); /** ne mkdirs */
            if (uspesnoKreiranFolder){
                tekst = "Uspesno je kreiran folder: " + newChildFolder;
                prikaziToast(context, tekst);
                 if (BuildConfig.DEBUG)Log.e("TAGroot", tekst);
            }else{
                tekst = "GRESKA ne moze da se kreira child folder: " + newChildFolder;
                prikaziWarnToast(context, tekst);
                 if (BuildConfig.DEBUG)Log.e("TAGroot", tekst);
                return defaultFolder;
            }
        }else{ // folder vec postoji i ispisujemo obavestennje
            if (BuildConfig.DEBUG) {
                tekst = "Vec postoji folder: " + newChildFolder;
                prikaziToast(context, tekst);
                 if (BuildConfig.DEBUG)Log.e("TAGroot", tekst);
            }
        }

        // nisam proverio svrsishodnost
        //noviFolder.setReadable(true);
        //noviFolder.setWritable(true);

        return noviFolder;

    }


    /** tipskaDodOpt ***************************/
    public static String[] tipskaDodOpt = new String[]{"1", "1.6", "2.5", "4", "8"};
    public static int tipskaDodOpt_pozUlisti(double koef){
        for (int i = 0; i < tipskaDodOpt.length; i++) {
            double dod = Double.valueOf(tipskaDodOpt[i]);
            if (koef == dod)
                return i;
        }

        return nepostoji;
    }
    public static int pozicijaTipskogOpterUNizu(double dodOpt){
        // pretvaranja dodOpt u String nije dobro jer bi pri konverziji 8-pretvorio u 8.0
        // i tada ne bi imao jednakost "4"=="4.0"
        double[] pomNiz = new double[tipskaDodOpt.length];
        for (int i = 0; i <tipskaDodOpt.length ; i++) {
            pomNiz[i]= Double.valueOf(tipskaDodOpt[i]);
            if (pomNiz[i]==dodOpt)
                return i;
        }
        return nepostoji;
    }


    /** toast obavestenja i html string ***************************/
    private static Toast obavestenje;
    private static void prikaziToast_master(Context c, String s, int color, int gravity) {
        //"Toast toast" is declared in the class
        s = s.replaceAll("\n", "<br>");
        try {
            obavestenje.getView().isShown();     // true if visible
            obavestenje.setText(mojHtmlString(s));
        } catch (Exception e) {         // invisible if exception
            obavestenje = Toast.makeText(c, s, LENGTH_SHORT);
        }
        TextView tv = (TextView) obavestenje.getView().findViewById(android.R.id.message);
        if( tv != null) { // !@# 14.2.18-2
            tv.setGravity(Gravity.CENTER); // ovo je za centriranje teksta unutar toast
            tv.setTextColor(color);
        }
        if (gravity != Integer.MIN_VALUE)
            obavestenje.setGravity(gravity,0,0); //za centriranje toast na sredinu ekarana
        obavestenje.show();
    }
    public static void prikaziToast(Context c, String s) {
        prikaziToast_master(c,s, Color.YELLOW, Integer.MIN_VALUE);
    }
    public static void prikaziWarnToast(Context c, String s) {
        prikaziToast_master(c,s,Color.rgb(204,0,0),Gravity.CENTER);
    }
    public static Spanned mojHtmlString(String s) {
        // Verzije do API24 ne mogu da koriste Html.fromHtml(s, 0)
        // <b> BOLD </b>
        // <i> italic </i>
        // <u> underline </u>
        // <br> == \n
        // <font color='red'> fafadsnlgkjagnk </font>
        Spanned mSpanned;
        if (Build.VERSION.SDK_INT<24)
            mSpanned= Html.fromHtml(s);
        else
            mSpanned=Html.fromHtml(s, 0); // ne znam sta je 0

        return mSpanned;
    }


    /** sistem info ***************************/
    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }
    public static String getAndroidName() {
        StringBuilder builder = new StringBuilder();
        builder.append(Build.VERSION.RELEASE);

        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            int fieldValue = nepostoji;

            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append(" - ").append(fieldName).append(" - ");
                builder.append("sdk_").append(fieldValue);
            }
        }

        return builder.toString();
    }
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }
    public static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }


    /** drawable pozadina - leto,zima,prolece-jesen ***************************/
    public static Drawable getPozadinaStaro(Context context){
        Drawable drawable;
        int godDoba;
        switch (getSeason()) {
            case zima:
                godDoba = R.drawable.pozadina_zima;
                break;
            case prolece:
                godDoba = R.drawable.pozadina_prolece_jesen;
                break;
            case leto:
                godDoba = R.drawable.pozadina_leto;
                break;
            case jesen:
                godDoba = R.drawable.pozadina_prolece_jesen;
                break;
            default:
                godDoba = R.drawable.pozadina_prolece_jesen;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            drawable = context.getResources().getDrawable(godDoba, null);
        else
            drawable = context.getResources().getDrawable(godDoba);

        return drawable;
    }
    /** drawable pozadina - leto,zima,prolece-jesen ***************************/
    public static TransitionDrawable getPozadina(Context context){
        Drawable drawable_main, drawable_padavine;
        int drawIndex_main, drawIndex_padavine;
        switch (getSeason()) {
            case zima:
                drawIndex_main = R.drawable.pozadina_zima;
                drawIndex_padavine = R.drawable.pozadina_padavine_zima;
                break;
            case prolece:
                drawIndex_main = R.drawable.pozadina_prolece_jesen;
                drawIndex_padavine = R.drawable.pozadina_padavine_prolece_jesen;
                break;
            case leto:
                drawIndex_main = R.drawable.pozadina_leto;
                drawIndex_padavine = R.drawable.pozadina_padavine_leto;
                break;
            case jesen:
                drawIndex_main = R.drawable.pozadina_prolece_jesen;
                drawIndex_padavine = R.drawable.pozadina_padavine_prolece_jesen;
                break;
            default:
                drawIndex_main = R.drawable.pozadina_prolece_jesen;
                drawIndex_padavine = R.drawable.pozadina_padavine_prolece_jesen;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawable_main = context.getResources().getDrawable(drawIndex_main, null);
            drawable_padavine = context.getResources().getDrawable(drawIndex_padavine, null);
        }else {
            drawable_main = context.getResources().getDrawable(drawIndex_main);
            drawable_padavine = context.getResources().getDrawable(drawIndex_padavine);
        }

        Drawable[] layers = new Drawable[2];
        layers[0]=drawable_main;
        layers[1]=drawable_padavine;
        TransitionDrawable transition = new TransitionDrawable(layers);
        return transition;
    }
    public static godDoba getSeason() {
        final godDoba nizGodDoba[] = {
                zima, zima,
                prolece, prolece,prolece,
                leto,leto,leto,
                jesen,jesen,jesen,
                zima };
        return nizGodDoba[ Calendar.getInstance().get(Calendar.MONTH) ];
    }
    public enum godDoba {zima,prolece,leto,jesen}

    public enum NaponskiNivo{
        _10kV("10kV"),
        _20kV("20kV"),
        _35kV("35kV"),
        _110kV("110kV");

        private final String naziv;
        NaponskiNivo(String s) {
            naziv = s;
        }

        public boolean equalsName(String otherName){
            return naziv.equals(otherName);
        }

        public int naponUkV(){
            switch (naziv){
                case "10kV":
                    return 10;
                case "20kV":
                    return 20;
                case "35kV":
                    return 35;
                case "110kV":
                    return 110;
                default:
                    return 0;
                }

        }

        @Override
        public String toString(){
            return this.naziv;
        }
    }
    private static NaponskiNivo[] naponskiNivo =
            new NaponskiNivo[]{_10kV,_20kV,_35kV,_110kV};
    public static NaponskiNivo[] getNizNaponskiNivo(){
        return naponskiNivo;
    }

    public enum MaterijalStuba{betonski, čel_rešet}
    private static MaterijalStuba[] materijalStubova =
            new MaterijalStuba[]{betonski,čel_rešet};
    public static MaterijalStuba[] getNizMaterijalStubova(){
        return materijalStubova;
    }


    public enum Tip_Stuba {noseći, zatezni, krajnji}
    private static Tip_Stuba[] tipoviStubova =
            new Tip_Stuba[]{noseći,zatezni,krajnji};
    public static Tip_Stuba[] getNizTipovaStubova(){
        return tipoviStubova;
    }

    public enum Raspored_Konzola {horizontalno, koso, trougao}

    public static int getIndexElementaUNizu(Object[] e, String naziv){
        /** radi za string i enum */
        for (int i = 0; i < e.length; i++) {
             if (BuildConfig.DEBUG)Log.e("TAGstatic", "e[i].toString()="+e[i].toString()
                    +" naziv="+naziv);
            if (e[i].toString() . equals(naziv))
                return i;
        }
        return nepostoji;
    }
    public static int getIndexElementaUNizu(List e, String naziv){
        /** za liste elemenata sutbnog mesta --- stub, konzola, izolator*/
        for (int i = 0; i < e.size(); i++) {
             if (BuildConfig.DEBUG)Log.e("TAGstatic", "e[i].toString()=\n"+e.get(i).toString()
                    +" \n***naziv="+naziv);
            if (e.get(i).toString() . contains(naziv)) /** contains !!! */
                return i;
        }
        return nepostoji;
    }

    public static boolean elamentPripadaNizu(Object[] e, String naziv){
        /** radi za string i enum */
        return getIndexElementaUNizu(e, naziv) != nepostoji;
    }


    public static void pauzirajIzvrsavanje(long time){
        int sum;
        for (int y = 1; y < time; y++){
            sum = y+ (int) Math.sqrt(y);
        }
    }


    public static double[] izracunajSrednjeRaspone
            (double[] nizRaspona){
        /** ima ih koliko i stubova (raspona +1) */
        int noStub = nizRaspona.length + 1;
        double [] nizSredRaspona = new double[noStub];
        double sredRaspon;

        for (int i = 0; i < noStub; i++) {

            if (i == 0) { // prvi stub
                sredRaspon = nizRaspona[0]/2;
            }
            else if (i == noStub-1){ // poslednji stub
                sredRaspon = nizRaspona[nizRaspona.length-1]/2;
            }
            else { // stubovi u sredini
                sredRaspon = (nizRaspona[i-1]+nizRaspona[i])/2;
            }

            nizSredRaspona[i]=sredRaspon;
             if (BuildConfig.DEBUG)Log.e("TAGklasaStubova", "i="+ i +" sredRaspon="+sredRaspon);

        }

         if (BuildConfig.DEBUG)Log.e("TAGklasaStubova", "nizSredRaspona="+ Arrays.toString(nizSredRaspona));

        return nizSredRaspona;
    }

    public static double[] izracunajGravitacRaspone
            (double[] nizRaspona, double[] nizTemenaLanc){
        /** ima ih koliko i stubova (raspona +1) */
        int noStub = nizRaspona.length + 1;
        double [] nizGravitacRaspona = new double[noStub];

        double stacionaza=0,
                gravitacRasp_LEVI, gravitacRasp_DESNI;

        for (int i = 0; i < noStub; i++) {

            if (i == 0) { // prvi stub
                stacionaza = 0;
                gravitacRasp_LEVI   = 0;
                gravitacRasp_DESNI   = nizTemenaLanc[0]   - stacionaza;
            }

            else if (i == noStub-1){ // poslednji stub
                stacionaza += nizRaspona[nizRaspona.length-1];
                gravitacRasp_LEVI   = stacionaza - nizTemenaLanc[nizRaspona.length-1];
                gravitacRasp_DESNI   = 0;
            }

            else { // stubovi u sredini
                stacionaza += nizRaspona[i-1];
                gravitacRasp_LEVI   = stacionaza - nizTemenaLanc[i-1];
                gravitacRasp_DESNI   = nizTemenaLanc[i]   - stacionaza;
            }

            nizGravitacRaspona[i]=gravitacRasp_LEVI+gravitacRasp_DESNI;

             if (BuildConfig.DEBUG)Log.e("TAGklasaStubova", "i="+ i +" gravRaspon="+gravitacRasp_LEVI+gravitacRasp_DESNI);
        }

         if (BuildConfig.DEBUG)Log.e("TAGklasaStubova", "nizGravitacRaspona="+ Arrays.toString(nizGravitacRaspona));
        return nizGravitacRaspona;
    }

    public static String ispisiU_0dec (double vrednost){
        return String.format("%,.0f",vrednost);
    }
    public static String ispisiU_1dec (double vrednost){
        return ispisiUdec("%."+1+"f",vrednost);
    }
    public static String ispisiU_2dec (double vrednost){
        return ispisiUdec("%."+2+"f",vrednost);
    }
    private static String ispisiUdec (String s ,double vrednost){
        if (vrednost == (int) vrednost)
            return String.format("%d",(int)vrednost);
        else
            return String.format(s,vrednost);
    }


}
