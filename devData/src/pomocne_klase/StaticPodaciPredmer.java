package nsp.mpu.pomocne_klase;

/**
 * Created by Nikola on 4.3.2018.
 */
import io.michaelrocks.paranoid.Obfuscate; @Obfuscate
public abstract class StaticPodaciPredmer {
    public final static String KOM = "kom";
    public final static String KG = "kg";
    public final static String KM = "km";
    public final static String KOMPLET = "komplet";
    public final static String M3 = "m³";
    public final static String PODNASLOV = "podnaslov";
    public final static double kg_daN = 0.980665;
    public final static double daN_kg = 1/kg_daN;
    public final static double km_m = 1000;
    public final static double m_km = 1/km_m;

    public final static float koefPovecanja_def = 1.05f;
    public final static float sitanMaterijal = 0.1f;
    public final static float ostaliRadovi = 0.1f;

    public final static float jCenaIskopa = 25;
    public final static float jCenaBetona = 225;
    public final static float uzemljenje_1prsten = 267;
    public final static float uzemljenje_2prsten = 460;
    public final static float pripremaTrase = 830;
    public final static float imovPravOdnosi = 1250;
    public final static float geodSnimanje_stub = 25;
    public final static float geodSnimanje_trasa = 250;
    public final static float projektovanje_min = 2000;
    public final static float projektovanje_km = 500;



}
