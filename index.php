<!DOCTYPE html>
<html lang="en">
<head> 
    <!-- The spider will not look at this page but will crawl through the rest of the pages on your website. -->   
    <meta name="robots" content="noindex, follow">
</head>
</html>

<?php
    exit(header("location: view/home.php"));
?>