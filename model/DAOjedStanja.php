<?php
/**
 * DataAccessObject za pristup bazi
 * table name: jedStanja
 * table name: pdfcreated
 *
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      27.09.2018.
 * @version    fileVer 1.0
 */
require_once ROOT.'model/access_file.php';
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}

f_fileTestAndRequire(ROOT.'model/db.php');
class DAOjedStanja{
    private $db, $statement;    
    public function __construct(){
        $this->db = DB::createInstance();
    }

//////////////////////////////////////////////////////////////////
    private $SELECT_JS_by_ID = "SELECT data FROM jedStanja WHERE id=? AND userId = ?";
    public function selectJSById($id, $userId){
        try {
            $statement = $this->db->prepare($this->SELECT_JS_by_ID);
            $statement->bindValue(1,$id);
            $statement->bindValue(2,$userId);
            $statement->execute();
            return $statement->fetch();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        } 
    }
    
    private $SELECT_JS_by_lastID = "SELECT id FROM jedStanja WHERE userId = ? ORDER by id DESC LIMIT 1";
    public function getIDofLastJS($userId){
        try {
            $statement = $this->db->prepare($this->SELECT_JS_by_lastID);
            $statement->bindValue(1,$userId);
            $statement->execute();
            return $statement->fetch();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        } 
    }
    
    private $INSERT_PROJ = "INSERT INTO jedStanja(userId, data) VALUES (?,?)";
    function insert_JS($userId, $JSONdata){
            $daoMsg = "";
            try {
                $statement = $this->db->prepare($this->INSERT_PROJ);
                $statement->bindValue(1,$userId);
                $statement->bindValue(2,$JSONdata);
                $statement->execute();
                return $statement->rowCount(); //broj promenjenjih unosa/redova
            } catch (Exception $e) {
                f_debug($e->getCode(),$e->getMessage());
            } 
    }    
    
    private $INSERT_PDF = "INSERT INTO pdfcreated (userId, jsId) VALUES (?,?)";
    function insert_PDF($userId, $jsID){
        $daoMsg = "";
        try {
            $statement = $this->db->prepare($this->INSERT_PDF);
            $statement->bindValue(1,$userId);
            $statement->bindValue(2,$jsID);
            $statement->execute();
            return $statement->rowCount(); //broj promenjenjih unosa/redova
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        } 
    }
    
    
}
?>