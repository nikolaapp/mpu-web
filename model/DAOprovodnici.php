<?php
/**
 * DataAccessObject za pristup bazi 
 * table name: provodnici
 * 
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      27.09.2018.
 * @version    fileVer 1.0
 */

require_once ROOT.'model/access_file.php';
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}

f_fileTestAndRequire(ROOT.'model/db.php');
class DAOprovodnici{
    private $db, $statement;    
    public function __construct(){
        $this->db = DB::createInstance();
    }

////////////////////////provodnici//////////////////////////////////////
    private $SELECT_PROV_by_OZNAKA = "SELECT * FROM provodnici WHERE (userId=? OR userId = -1) AND visible<>0 AND oznaka = ?";
    public function selectProvByOZNAKA($userId, $oznaka){
        try {
            $statement = $this->db->prepare($this->SELECT_PROV_by_OZNAKA);
            $statement->bindValue(1,$userId);
            $statement->bindValue(2,$oznaka);
            $statement->execute();
            return $statement->fetch();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }
    }    
    
    private $SELECT_PROV_by_ID = "SELECT * FROM provodnici WHERE id = ? AND (userId=? OR userId = -1)  AND visible<>0";
    public function selectProvByID($id, $userId){
        try {
           $statement = $this->db->prepare($this->SELECT_PROV_by_ID);
            $statement->bindValue(1,$id);
            $statement->bindValue(2,$userId);
            $statement->execute();
            return $statement->fetch(); 
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }        
    }
    
    private $SELECT_PROV_LIST_by_USER = "SELECT id,oznaka FROM provodnici WHERE (userId=? OR userId = -1) AND visible<>0 ORDER BY vreme DESC";
    public function get_provList_user($userId){
        try {
            $statement = $this->db->prepare($this->SELECT_PROV_LIST_by_USER);
            $statement->bindValue(1,$userId);
            $statement->execute();
            return $statement->fetchAll();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }
        
    }
    
    private $INSERT_NEW_PROV = "INSERT INTO provodnici(userId, oznaka, presek, precnik, tezina, modul, alfa, struja) VALUES (?,?,?,?, ?,?,?,?)";
    public function saveNewProv($userId,$provName,$provPresek,$provPrecnik,$provTezina,
                                $provE,$provAlfa,$provI){
        try {
            $statement = $this->db->prepare($this->INSERT_NEW_PROV);
            $statement->bindValue(1,$userId);
            $statement->bindValue(2,$provName);
            $statement->bindValue(3,$provPresek);
            $statement->bindValue(4,$provPrecnik);
            $statement->bindValue(5,$provTezina);
            $statement->bindValue(6,$provE);
            $statement->bindValue(7,$provAlfa);
            $statement->bindValue(8,$provI);
            $statement->execute();
            return $statement->rowCount(); //broj promenjenjih unosa/redova
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }        
    }
    
    private $UPDATE_PROV_by_ID = "UPDATE provodnici SET presek=?, precnik=?, tezina=?, modul=?, alfa=?, struja=? WHERE (userId=? AND userId <> -1) AND id = ? AND visible<>0";
    public function editProvByID($userId,$provId,$provPresek,$provPrecnik,$provTezina,
                                    $provE,$provAlfa,$provI){
        //ustvarni ne brise proj iz baze vec ga samo korisniku cini nedostupnim
        try {
            $statement = $this->db->prepare($this->UPDATE_PROV_by_ID);
            $statement->bindValue(1,$provPresek);
            $statement->bindValue(2,$provPrecnik);
            $statement->bindValue(3,$provTezina);
            $statement->bindValue(4,$provE);
            $statement->bindValue(5,$provAlfa);
            $statement->bindValue(6,$provI);
            $statement->bindValue(7,$userId, PDO::PARAM_INT );
            $statement->bindValue(8,$provId, PDO::PARAM_INT );
            $statement->execute();
            return $statement->rowCount();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }
    }
    
    private $UPDATE_PROV_visible_by_ID = "UPDATE provodnici SET visible=0 WHERE (userId=? AND userId <> -1) AND id = ?";
    public function deleteProvByID($userID,$id){
        //ustvarni ne brise proj iz baze vec ga samo korisniku cini nedostupnim
        try {
            $statement = $this->db->prepare($this->UPDATE_PROV_visible_by_ID);
            $statement->bindValue(1,$userID, PDO::PARAM_INT );
            $statement->bindValue(2,$id, PDO::PARAM_INT );
            $statement->execute();
            return $statement->rowCount();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }
    }
    
    
}
?>