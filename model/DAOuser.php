<?php
/**
 * DataAccessObject za pristup bazi
 * table name: provodnici
 *
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      27.09.2018.
 * @version    fileVer 1.0
 */

require_once ROOT.'model/access_file.php';
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}

f_fileTestAndRequire(ROOT.'model/db.php');
class DAOuser{
    // ako nema private/public onda je po def public
    private $db;
    private $SELECT_USERS_BY_USERNAME_AND_PASSWORD;
    private $username, $password;
    
    // vraca posledni insertovan ID u tabelu
    public function lastInsertId(){
        return $this->db->lastInsertId();
    }
    
    function __construct($heder){
        $this->db = DB::createInstance();
        
        if (is_array($this->db)){
            f_debug($this->db["statusErr"], 
                    $this->db["msgErr"],
                    $heder
            );
        }
        
        $this->SELECT_USERS_BY_USERNAME_AND_PASSWORD = $this->db->prepare("SELECT * FROM lozinke WHERE username = ? AND password = ? AND isVisible <> 0 AND isActive <> 0");
        //bindParam  vezujemo preko reference, a ne preko vrednosti
        $this->SELECT_USERS_BY_USERNAME_AND_PASSWORD -> bindParam (1,$this->username, PDO::PARAM_STR);
        $this->SELECT_USERS_BY_USERNAME_AND_PASSWORD -> bindParam (2,$this->password, PDO::PARAM_STR);
     }
////////////////////////lozinke//////////////////////////////////////
     // urajdeno preko referenci - bindParam
    function selectUserByUsernameAndPassword($username, $password){
        $this->username = $username;
        $this->password = $password;
        try {
            $this->SELECT_USERS_BY_USERNAME_AND_PASSWORD->execute();        
            return $this->SELECT_USERS_BY_USERNAME_AND_PASSWORD->fetch();
        } catch (Exception $e) {            
            f_debug($e->getCode(),$e->getMessage());            
        }        
    }
    
    private $SELECT_USER_BY_ID = "SELECT * FROM lozinke WHERE id = ?";
    function selectUserById($id){
        try {
            $statement = $this->db->prepare($this->SELECT_USER_BY_ID);
            $statement->bindValue(1,$id);
            $statement->execute();
            return $statement->fetch();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }        
    }    
    
    private $SELECT_USER_BY_email_hash = "SELECT id, isActive FROM lozinke WHERE email = ? AND hash = ?";
    function selectUserFlagByMailAndHash($email, $hash){
        try {
            $statement = $this->db->prepare($this->SELECT_USER_BY_email_hash);
            $statement->bindValue(1,$email);
            $statement->bindValue(2,$hash);
            $statement->execute();
            return $statement->fetch();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }
    }
    
    private $NOof_USERNAMES = "SELECT COUNT(*) AS broj FROM lozinke WHERE username = ?";
    /**
     * Vraca uk.broj redova sa datim username
     * @return Array ["broj"]
     */
    function doesUsernameExist($username){
        try {
            $statement = $this->db->prepare($this->NOof_USERNAMES);        
            $statement->bindValue(1,$username);              
            $statement->execute();        
            return $statement->fetch();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());  
        }
         
    }
    
    private $NOof_EMAILS = "SELECT COUNT(*) AS broj FROM lozinke WHERE email = ?";
    /**
     * Vraca uk.broj redova sa datim mejlom
     * @return Array ["broj"]
     */
    function doesEmailExist($email){
        try {
            $statement = $this->db->prepare($this->NOof_EMAILS);
            $statement->bindValue(1,$email);
            $statement->execute();
            return $statement->fetch(); 
        }
        catch (Exception $e){
            f_debug($e->getCode(),$e->getMessage());  
        }
    }
    
    private $SELECT_ALL_USERS = "SELECT * FROM lozinke";
    function getAll_users(){
        try {
            $statement = $this->db->prepare($this->SELECT_ALL_USERS);        
            $statement->execute();        
            return $statement->fetchAll(); 
        }
        catch (Exception $e){
            f_debug($e->getCode(),$e->getMessage());
        }
    }
        
    private $INSERT_USER = "INSERT INTO lozinke(ime, username, password, email, hash) VALUES (?,?,?,?,?)";
    function insert_user($ime, $username, $password, $email, $hash){
        try {  
            $statement = $this->db->prepare($this->INSERT_USER);        
            $statement->bindValue(1,$ime);
            $statement->bindValue(2,$username);
            $statement->bindValue(3,$password);
            $statement->bindValue(4,$email);
            $statement->bindValue(5,$hash);        
            $statement->execute();
            return $statement->rowCount();            
        } catch (Exception $e) {
            f_debug(3119,$e->getMessage(), "location: ../view/login.php?tab=register");  
        }
        
    }
    
    
    private $UPDATE_USER_VERIFY = "UPDATE lozinke SET isActive=? WHERE id = ?";
    function update_user_verify( $id,$isActive){
        try {
            $statement = $this->db->prepare($this->UPDATE_USER_VERIFY );
            $statement->bindValue(1,$isActive);
            $statement->bindValue(2,$id);
            $statement->execute();        
            if ($statement->rowCount() == 1) {
                return true;
            }
            else {
                return false;
            }
            
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }
    }
    
    
/*     
    private $SELECT_USERS_BY_USERNAME_AND_PASSWORD1 = "SELECT * FROM lozinke WHERE username = ? AND password = ?";
    function selectUserByUsernameAndPassword1($username, $password){
        $statement = $this->db->prepare($this->SELECT_USERS_BY_USERNAME_AND_PASSWORD1);
        
        $statement->bindValue(1,$username);  // po jedan red za svaki znak pitanja
        $statement->bindValue(2,$password);
        
        $statement->execute();
        
        $result = $statement->fetch();
        
        return $result;
    }
 
*/
    
}
?>