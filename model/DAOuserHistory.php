<?php
/**
 * DataAccessObject za pristup bazi
 * table name: userprojects 
 * table name: userlevel
 *
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      27.09.2018.
 * @version    fileVer 1.0
 */
require_once ROOT.'model/access_file.php';
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}

f_fileTestAndRequire(ROOT.'model/db.php');
class DAOuserHistory{
    private $db, $statement;    
    public function __construct(){
        $this->db = DB::createInstance();
    }

//////////////////////////////////////////////////////////////////
    private $GET_TOTAL_USER_PROJ = "SELECT COUNT(id) AS broj FROM userprojects WHERE userId = ? AND vreme > DATE_SUB(NOW(), INTERVAL ? DAY)";
    /**
     * Vraca ukupan broj projekata koje je sacuvao user
     * @param int $userId
     * @return Array ["broj"]
     */
    public function getTotalUserProj($userId, $brojDana){
        try {
            $statement = $this->db->prepare($this->GET_TOTAL_USER_PROJ);
            $statement->bindValue(1,$userId);
            $statement->bindValue(2,$brojDana);
            $statement->execute();
            return $statement->fetch();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }  
    }
    
    private $GET_TOTAL_USER_JS = "SELECT COUNT(id) AS broj FROM jedstanja WHERE userId = ? AND vreme > DATE_SUB(NOW(), INTERVAL ? DAY)";
    /**
     * Vraca ukupan broj jednacina stanja(lancanica) koje je kreirao user
     * @param int $userId
     * @return Array ["broj"]
     */
    public function getTotalUserJS($userId, $brojDana){
        try {
            $statement = $this->db->prepare($this->GET_TOTAL_USER_JS);
            $statement->bindValue(1,$userId);
            $statement->bindValue(2,$brojDana);
            $statement->execute();
            return $statement->fetch();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }  
    }
  
    private $GET_TOTAL_USER_PDF = "SELECT COUNT(id) AS broj FROM pdfcreated WHERE userId = ? AND vreme > DATE_SUB(NOW(), INTERVAL ? DAY)";
    /**
     * Vraca ukupan broj pdf fajlovakoje je kreirao user
     * @param int $userId
     * @return Array ["broj"]
     */
    public function getTotalUserPDF($userId, $brojDana){
        try {
            $statement = $this->db->prepare($this->GET_TOTAL_USER_PDF);
            $statement->bindValue(1,$userId);
            $statement->bindValue(2,$brojDana);
            $statement->execute();
            return $statement->fetch();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }  
    }
     
    private $GET_USER_LEVEL_QUOTS = "SELECT * FROM userlevel WHERE name = ?";
    /**
     * Vraca kvote za zadati level name
     * @param string $levelName free, lite, pro
     * @return array
     */
    public function getUserLevelQuota($levelName){
        try {
            $statement = $this->db->prepare($this->GET_USER_LEVEL_QUOTS);
            $statement->bindValue(1,$levelName);
            $statement->execute();
            return $statement->fetch();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }  
    }
    
}
?>