<?php
/**
 * DataAccessObject za pristup bazi
 * table name: userprojects
 *
 * @author     Nikola Pavlovic, Kraljevo
 * @copyright  Nikola Pavlovic, Kraljevo
 * @since      27.09.2018.
 * @version    fileVer 1.0
 */
require_once ROOT.'model/access_file.php';
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}

f_fileTestAndRequire(ROOT.'model/db.php');
class DAOuserProjekti{
    private $db, $statement;    
    public function __construct(){
        $this->db = DB::createInstance();
    }

////////////////////////userprojects//////////////////////////////////////
    
    private $SELECT_ALL_PROJ_LIST_by_USER = "SELECT id, naziv FROM userprojects WHERE (userId = ? OR userId=-1 ) AND visible <> 0 ORDER BY vreme DESC ";
    public function getList_proj_user($userid){
        try {
            $statement = $this->db->prepare($this->SELECT_ALL_PROJ_LIST_by_USER);
            $statement->bindValue(1,$userid, PDO::PARAM_INT );
            $statement->execute();
            return $statement->fetchAll();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }
    }
    
    private $SELECT_PROJ_by_ID = "SELECT * FROM userprojects WHERE (userId = ? OR userId=-1 ) AND visible <> 0  AND id = ?";
    public function selectProjByID($userID,$id){
        try {
            $statement = $this->db->prepare($this->SELECT_PROJ_by_ID);
            $statement->bindValue(1,$userID, PDO::PARAM_INT );
            $statement->bindValue(2,$id, PDO::PARAM_INT );
            $statement->execute();
            return $statement->fetch();
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }
    }
    
    private $INSERT_PROJ = "INSERT INTO userprojects(userId, naziv, oznakaProv, odo, temperatura, naprezanje, tackeTlaJson, objektiTraseJson, stubnaMestaJson, tackeLancJson, jsID) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
    function insert_userProj($userId, $nazivProj, $oznakaProv, $odo, $temperatura,$naprezanje,
        $tackeTlaJson = NULL, $objektiTrase = NULL, $stubnaMestaJson = NULL, $tackeLancJson = NULL, $jsId){
        try {
            $statement = $this->db->prepare($this->INSERT_PROJ);
            $statement->bindValue(1,$userId);
            $statement->bindValue(2,$nazivProj);
            $statement->bindValue(3,$oznakaProv);
            $statement->bindValue(4,$odo);
            $statement->bindValue(5,$temperatura);
            $statement->bindValue(6,$naprezanje);
            $statement->bindValue(7,$tackeTlaJson);
            $statement->bindValue(8,$objektiTrase);
            $statement->bindValue(9,$stubnaMestaJson);
            $statement->bindValue(10,$tackeLancJson);
            $statement->bindValue(11,$jsId);
            $statement->execute();
            return $statement->rowCount(); //broj promenjenjih unosa/redova
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }  
    }
        
    private $UPDATE_PROJ = "UPDATE userprojects SET naziv=?, oznakaProv=?,odo=?,temperatura=?,naprezanje=?,tackeTlaJson=?,objektiTraseJson=?,stubnaMestaJson=?,tackeLancJson=?, jsID=? WHERE id = ? AND userId = ?";
    function update_userProj($id, $userId, $nazivProj, $oznakaProv, $odo, $temperatura,$naprezanje,
        $tackeTlaJson = NULL, $objektiTraseJson = NULL,  $stubnaMestaJson = NULL, $tackeLancJson = NULL, $jsId){
            try {
                $statement = $this->db->prepare($this->UPDATE_PROJ);
                $statement->bindValue(1,$nazivProj);
                $statement->bindValue(2,$oznakaProv);
                $statement->bindValue(3,$odo);
                $statement->bindValue(4,$temperatura);
                $statement->bindValue(5,$naprezanje);
                $statement->bindValue(6,$tackeTlaJson);
                $statement->bindValue(7,$objektiTraseJson);
                $statement->bindValue(8,$stubnaMestaJson);
                $statement->bindValue(9,$tackeLancJson);
                $statement->bindValue(10,$jsId);
                $statement->bindValue(11,$id);
                $statement->bindValue(12,$userId);
                $statement->execute();
                return $statement->rowCount(); //broj promenjenjih unosa/redova
            } catch (Exception $e) {
                f_debug($e->getCode(),$e->getMessage());
            }  
    }
    
    private $DELETE_PROJ_by_ID = "UPDATE userprojects SET visible=0 WHERE userId=? AND id = ?";
    public function deleteProjByID($userID,$id){        
        //ustvarni ne brise proj iz baze vec ga samo korisniku cini nedostupnim
        try {
            $statement = $this->db->prepare($this->DELETE_PROJ_by_ID);
            $statement->bindValue(1,$userID, PDO::PARAM_INT );
            $statement->bindValue(2,$id, PDO::PARAM_INT );
            $statement->execute();
            return $statement->rowCount(); //broj promenjenjih unosa/redova
        } catch (Exception $e) {
            f_debug($e->getCode(),$e->getMessage());
        }  
    }
    
}
?>