<?php
require_once ROOT.'model/access_file.php';
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}

require_once ROOT.'model/DAOprovodnici.php';

const TEMP_MINUS5_BEZ_LEDA = -155;
const NIJE_SETOVANO_T0 = -105;

/**
 * @author Nikola Pavlovic
 *
 */
class JednacinaStanja implements JsonSerializable  {
    
    private $mSigmaNOVO, $mKoefA, $mKoefB, $mAkr, $mUgib;
    
    private $mTipProv;
    private $sigmaMax, $raspon, $cosFi,  $koefDodOpt;
    private $t0; // temperatura pocetnog stanja {-5, -20}
    private $t1; // temperatura krajnjeg stanja {bilo koja}
    private $tezinaProv_t0; //tezina provodnika pri temp t0 [daN/(m*mm2)]
    private $tezinaProv_t1; //tezina provodnika pri temp t1 [daN/(m*mm2)]
    private $tezinaProv_minus5led; //tezina provodnika pri -5 sa ledom [daN/(m*mm2)]
    
    //public static int TEMPERATURA_ZA_KOJU_SE_NE_RACUNA_UGIB = -100;
    
    
    private function __construct($tipProv, $sigmaMax, $raspon,
        $cosFi, $koefDodOpt, $t1, $t0) {
            $this->mTipProv = $tipProv;
            $this->sigmaMax = (float)  $sigmaMax;
            $this->raspon = (float) $raspon;
            $this->cosFi = (float) $cosFi;
            $this->koefDodOpt = (float)$koefDodOpt;
            $this->t1 = (int) $t1;
            $this->t0 = (int) $t0;   
            
//echo "<br><br>konstuktor<br>$tipProv<br><br>";
//var_dump($tipProv);
            $this->postaviJednacinuStanja();
    }
    
    
    /**
     * kostruktor BEZ postavljanja pocetne temperature t0
     * @param array $tipProv 
     * @param float $sigmaMax [daN/mm2]
     * @param float $raspon [m]
     * @param float $cosFi [rel]
     * @param float $koefDodOpt [rel]
     * @param int $t1 [&deg;C]
     * @return JednacinaStanja
     */
    public static function create($tipProv, $sigmaMax, $raspon,
        $cosFi, $koefDodOpt, $t1) {            
        $instanca = new self($tipProv, $sigmaMax, $raspon, $cosFi, $koefDodOpt, $t1, NIJE_SETOVANO_T0);
        return $instanca;
    }
    
    /**
     * kostruktor
     *
     * sa postavljanom pocetnom temperaturom t0
     *
     */
    public static function create_withT0($tipProv, $sigmaMax, $raspon,
        $cosFi, $koefDodOpt, $t1, $t0) {
        
        $instanca = new self($tipProv, $sigmaMax, $raspon, $cosFi, $koefDodOpt, $t1, $t0);            
        return $instanca;
    }
    
    public function getTipProv() {return $this->mTipProv;}    
    public function getSigma0() {return $this->sigmaMax;}    
    public function getRaspon() {return $this->raspon;}
    public function getCosFi() {return $this->cosFi;}    
    public function getKoefDodOpt() {return $this->koefDodOpt;}    
    public function getTemperatura() {return $this->t1;}   
    /**     * 
     * @return float [daN/mm2]
     */
    public function getSigmaNOVO() {return $this->mSigmaNOVO;}    
    public function getKoefA() {return $this->mKoefA;}    
    public function getKoefB() {return $this->mKoefB;}    
    public function getAkr() {return $this->mAkr;}    
    public function getUgib() {return $this->mUgib;}
    /**
    * tezina prov. pri temperaturi -5 sa ledom
    * @return float
    */
    public function getTezinaProv_minus5led() {return $this->tezinaProv_minus5led;}
    /**
     * tezina prov. pri temperaturi t0 {-5, -20} - pocetno stanje JS
     * @return float
     */
    public function getTezinaProv_t0() {return $this->tezinaProv_t1;}
    /**
     * tezina prov. pri temperaturi t1 - krajnje stanje JS
     * @return float 
     */
    public function getTezinaProv_t1() {return $this->tezinaProv_t1;}
    public function getParametarLancanice() {return $this->mSigmaNOVO / $this->tezinaProv_t1;}  
    //public function getUgibX($b) {return UgibUcm ($raspon, $cosFi, $mSigmaNOVO, $mRezTezinaProv, $b);}
    
    
    private function postaviJednacinuStanja() { 
        
        $mDodOpt = $this->DodatnoOptSnegLed($this->koefDodOpt, $this->mTipProv['precnik'], $this->mTipProv['presek']);
                
        $this->tezinaProv_minus5led = $this->mTipProv['tezina']+$mDodOpt;
                
        $this->mAkr = $this->KriticanRaspon(
            $this->sigmaMax, 
            $this->cosFi, 
            $this->mTipProv['alfa'], 
            $this->tezinaProv_minus5led, 
            $this->mTipProv['tezina']
            );
//echo $this->mAkr;
        
              
        //////////////////// parameti pocetnog stanja
        if ($this->t0 == NIJE_SETOVANO_T0) {
            if ($this->raspon > $this->mAkr) {
                $this->t0 = -5;
                $this->tezinaProv_t0 = $this->tezinaProv_minus5led;
            } else {
                $this->t0 = -20;
                $this->tezinaProv_t0 = $this->mTipProv['tezina'];
            }
        }else // t0 je setovano u construktoru
            $this->tezinaProv_t0 = $this->mTipProv['tezina']; // bez snega i leda
            
            
        //////////////////// parameti krajnjeg stanja
        if ($this->t1 == -5)
            $this->tezinaProv_t1 = $this->tezinaProv_minus5led;
        elseif ($this->t1 == TEMP_MINUS5_BEZ_LEDA) { // za proracun  bez leda
            $this->t1 = -5;
            $this->tezinaProv_t1 = $this->mTipProv['tezina'];
        }
        else $this->tezinaProv_t1 = $this->mTipProv['tezina'];

        
//echo $tezinaProv_t0."<br>";
//echo $tezinaProv_t1;
                
        $this->mKoefA = ($this->t1 - $this->t0) * $this->mTipProv['alfa'] * $this->mTipProv['modul'] * $this->cosFi
        + ( pow($this->raspon,2) * pow($this->tezinaProv_t0,2) * pow($this->cosFi, 3) * $this->mTipProv['modul'])
        / (24 * pow($this->sigmaMax,2) )
        - $this->sigmaMax;
        
        $this->mKoefB = pow($this->raspon,2) * pow($this->tezinaProv_t1,2) *
        pow($this->cosFi, 3) * $this->mTipProv['modul'] / 24;
        
//echo $this->mKoefA."<br>";
//echo $this->mKoefB;
        
        ### koef B se racuna na osnovu jedn x3+Ax2=B
        ### a posto je NRmetoda x3 + Ax2 + B = 0
        ### onda stavljamo -$mKoefB
        $this->mSigmaNOVO = $this->NewtonRaphsonMetoda($this->mKoefA,-$this->mKoefB, $this->sigmaMax);
        
        $this->mUgib = $this->UgibUcm ( $this->raspon,  $this->cosFi,  $this->mSigmaNOVO,  $this->tezinaProv_t1);
    }
    
    private function NewtonRaphsonMetoda ($A, $B, $sigma){
        // x^3 + A*x^2 + B = 0  UMESTO: - B ---OVAKO JE OPSTIJE
        return $this->NewtonRaphsonMetodaOPSTE(1,$A,$B,$sigma);
    }
    
    private function NewtonRaphsonMetodaOPSTE ($A3, $A, $B, $sigma){
        // A3*x^3 + A*x^2 + B = 0
        $epsilon_x=0.00001;
        $epsilon_y=0.00001;
        $Nmax=10000;
        $x=$sigma;
        $dx;
        $vrednost_f; $vrednost_izv_f;
        
        for ($brojac_iteracija=1; $brojac_iteracija<=$Nmax; $brojac_iteracija++){
            
            $vrednost_f = $A3* pow($x, 3) + $A* pow($x, 2) + $B;
            $vrednost_izv_f = 3*$A3* pow($x, 2) + 2*$A*$x;
            
            $dx = -$vrednost_f/$vrednost_izv_f;
            $x += $dx;
            
            $vrednost_f = $A3* pow($x, 3) + $A* pow($x, 2) + $B;
            
            if ((abs($dx) < $epsilon_x) && (abs($vrednost_f) < $epsilon_y))
                break;
        }
        
        return $x;
    }
    
    private function KriticanRaspon ($KRsigma0, $KRcosFi, $KRalfa,
        $optRez, $tezina){
            return ($KRsigma0 / $KRcosFi)* sqrt(    (360 * $KRalfa) /
                ($optRez*$optRez - $tezina*$tezina));
    }
    
    
    ### !@# 01.03.2018-2 proracun je prema TP10 ###
    private function IdealniRasponStaro($nizRaspona, $nizCosFi){
        $sum_gore=0; $sum_dole=0;
        $sum_gore_podKorenom=0; $sum_dole_podKorenom=0;
        for ($i = 0; $i < $nizRaspona.length; $i++) {
            $sum_gore += $nizRaspona[$i] / pow($nizCosFi[$i],3);
            $sum_dole += $nizRaspona[$i] / pow($nizCosFi[$i],2);
            
            $sum_gore_podKorenom += pow($nizRaspona[$i],3);
            $sum_dole_podKorenom += $nizRaspona[$i] / pow($nizCosFi[$i],2);
        }
        return ($sum_gore/$sum_dole)
        * sqrt($sum_gore_podKorenom/$sum_dole_podKorenom);
    }
    
    
    public static function UgibUcm ($a, $cos, $naprezanje, $tezina){
        return 100 * (($tezina * $a*$a)/(8*$naprezanje*$cos)  +
            ( (pow($tezina,3) * pow($a,4) *$cos) / (384* pow($naprezanje,3) )));
    }
    
    public static function UgibUcm_b ($a, $cos, $naprezanje, $tezina, $b){
        // ugib_na_udaljenosti_b_od_stuba
        $ugib_na_sredini = UgibUcm ($a, $cos, $naprezanje, $tezina);
        return 4*$b*($a-$b)*$ugib_na_sredini/($a*$a);
    }
    
    public static function DodatnoOptSnegLed ($koef, $d, $S){
        return $koef * 0.18 * sqrt($d) / $S;
    }
    
    public static function naprezanjePrekoUgiba($a, $cos, $ugib_b,
        $tezinaProv, $b, $sigmaPocetno){
            $ugib_na_sredini = $ugib_b*($a*$a)/(4*$b*($a-$b));
            $A3 = $cos*$ugib_na_sredini/100;
            $A = $a*$a*$tezinaProv/8;
            $B = pow($a,4) * pow($tezinaProv,3) / 384;
            return NewtonRaphsonMetoda($A3,-$A,-$B,$sigmaPocetno);
    }
    
    
    public function jsonSerialize(){
        return get_object_vars($this);
    }

    
    
}
?>