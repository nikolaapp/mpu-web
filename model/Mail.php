<?php
require_once ROOT.'model/access_file.php';
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}


require_once 'PHPMailer/PHPMailer.php';
require_once 'PHPMailer/SMTP.php';

/**
 * Kreira i salje mejl
 * -vraca 0 ako je sve ok
 * -vraca poruku o grasci
 * @param string $ime
 * @param string $username
 * @param string $password
 * @param string $email
 * @param string $hash
 * @return number|string
 */
function f_createMail($ime, $username, $password,  $email, $hash)
{
    $mail = new PHPMailer(true); // Passing `true` enables exceptions
    try {
        // Server settings
        $mail->SMTPDebug = 0; // Enable verbose debug output
        $mail->isSMTP(); // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = 'mpu.webapp'; // SMTP username
        $mail->Password = 'Nikola.036'; // SMTP password
        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587 ; // TCP port to connect to
                           
        // Recipients
        $mail->setFrom('mpu@gmail.com', 'MPU webApp registration');
        // $mail->addAddress('joe@example.net', 'Joe User'); // Add a recipient
        $mail->addAddress($email); // Name is optional
                                                       // $mail->addReplyTo('info@example.com', 'Information');
        //$mail->addCC('nikola.aoe2@gmail.com');
        $mail->addBCC('mpu.webapp@gmail.com');
        
        // Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz'); // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg'); // Optional name
         
        $tekstMejla = tekstMejla($ime, $username, $password,  $email, $hash);
        
        // Content
        $mail->isHTML(true); // Set email format to HTML
        $mail->Subject = 'MPU webApp registracija';
        //$mail->Body = 'tekst poruke <div style="color:red"><strong>'+$userMail+' '+$hash+'</strong></div>';
        $mail->Body = $tekstMejla;
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
        $mail->send();
        return 0;

    } catch (Exception $e) {
        return 'Message could not be sent. Mailer Error: '. $mail->ErrorInfo;
    }
}

function tekstMejla($ime, $username, $password,  $email, $hash){
    $tekstMejla = "";
    $tekstMejla = "<h2>Registracija za koriscenje MPU webAPP aplikacije</h2>";
    $tekstMejla .= "<p>Uneli ste sledece podatke:</p>";
    $tekstMejla .=
        "<ul><li>ime: <strong>"
            . $ime."</strong></li><li>username: <strong>"
            . $username."</strong></li><li>password: <strong>"
            . $password
        ."</strong></li></ul>";
    
    $tekstMejla .= "<p>Da bi ih verifikovali kliknite na link ispod:</p>";
    
    // define constanta mora pod navodnicima
    if ( defined('IS_LOCAL')  ) {
        $link = "http://localhost/mpuWEB/view/verify.php?email=$email&hash=$hash";
    }else{
        $link = "https://mpu.azurewebsites.net/view/verify.php?email=$email&hash=$hash";
    }
    
    $tekstMejla .= "<a href='$link' target='_blank'>verifikuj nalog na MPU webApp</a>";
    
    $tekstMejla .= "<br><br><hr>";
    $tekstMejla .= "<div style='font-size:0.8em;'>"
        ."Upozorenje:<br>"
        ."Ova elektronska poruka i svi podaci/prilozi koji se putem nje prenose su poverljivi i namenjeni iskljucivo primaocu, odnosno pojedincu i/ili licima na koje je adresirana. Ova poruka mo�e sadr�ati poverljive i/ili privilegovane informacije koje su zakonski za�ticene od objavljivanja. Ukoliko ste ovu elektronsku poruku dobili gre�kom, obave�tavamo Vas da je bilo kakvo kori�cenje, umno�avanje, �irenje ili cuvanje ove poruke najstro�e zabranjeno. Molimo da u tom slucaju odmah obavestite po�iljaoca poruke putem elektronske po�te ili telefonom, a zatim izbri�ete poruku i sve njene priloge."    
            
        ."<br><br>"
        ."Warning:<br>"
        ."This electronic message and all data / contributions transmitted through it are confidential and are intended solely for the recipient, that is, the individual and / or the persons to whom it is addressed. This message may contain confidential and / or privileged information that is legally protected from publication. If you have received this email by mistake, we would like to inform you that any use, duplication, extension or storage of this message is strictly prohibited. In this case, please immediately inform the sender of the message by e-mail or phone, then delete the message and all its attachments."
    ."</div>";
    
    return $tekstMejla;
    
}



?>  