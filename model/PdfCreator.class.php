<?php
require_once ROOT.'model/access_file.php';
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}

require_once('../view/libraries/tcpdf/tcpdf_import.php');

/**
 * @author Nikola Pavlovic
 * Kreiranje pdf fajla
 */
class PdfCreator {
    private $tlo, $stubovi, $lanc, $objJS;
    private $tlo_rel, $stubovi_rel, $lanc_rel; 
    private $projName;
    private $rasponiAps;
    private $sigVisina;
    private $minStacRel_stub;
    private $username,$password,$userLevel;
    private $pdfUserPass, $pdfMasterPass;
    
    /**
     * class konstuktor
     */
    private function __construct($user,$projName,$tlo, $stubovi, $lanc, $objJS, $sigVisina=6) {
        $this->tlo = $tlo;
        $this->stubovi = $stubovi;
        $this->lanc = $lanc;
        $this->objJS = $objJS;
        $this->projName = $projName;
        
        $this->username = $user['username'];
        $this->password = $user['password'];;
        $this->userLevel = $user['level'];
        $this->pdfUserPass = $user['username'];
        $this->pdfMasterPass = $user['username'].$user['password'].$user['username'];
        
        $this->sigVisina = $sigVisina;
        
        $this->tlo_rel = $this->f_skalirajNiz($tlo);
        $this->stubovi_rel = $this->f_skalirajNiz($stubovi);
        $this->lanc_rel = $this->f_skalirajNiz($lanc);
        
        
        // ako stacionaze ne pocinju od nule
        $minStacRel_tlo = $this->tlo_rel[0]["x"];
        if ($minStacRel_tlo != 0)
            foreach ($this->tlo_rel as &$tacka)
                $tacka["x"] -= $minStacRel_tlo;
        
        // ako stubovi ne pocinju od x=nula
        $this->minStacRel_stub = $this->stubovi_rel[0]["x"];
        
        $this->f_algoritamStart();
    }    
        
    
    /**
     * Kreiranje pdf fajla na osnovu nizova
     * @param array $user
     * @param string $projName
     * @param array $tlo
     * @param array $stubovi
     * @param array $lanc
     * @param array $objJS niz/objekat jed.stanja
     * @return PdfCreator salje klijentu pdf fajl
     */
    public static function create($user,$projName,$tlo, $stubovi, $lanc, $objJS) {
        $instanca = new self($user,$projName,$tlo, $stubovi, $lanc, $objJS);        
        return $instanca;
    }
        
    /////////////////////KONSTANTE///////////////////////////
    const RAZMERA_X = 2000; // razmera za stacionaze (x)
    const RAZMERA_Y = 500; // razmera za kote (y,h)
    const UNIT_CONVERSION = 1000; // [m]->[mm] --- zadati su nizovi u [m] a kreiramo pdf u [mm]
        
    //@var array offset za pozicioniranje grafika    
    private $VAGRES_GRAFIK;
    
    const PDF_ORIENTATION = 'L'; //landscape
    const PDF_PAGE_UNIT = "mm";    
    
    const MARGIN_LEFT = 20;
    const MARGIN_REST = 5;
    const PADDING_X = 20; // padding unutrasnje stranice
    const PADDING_TOP = 30 ; // padding unutrasnje stranice
    const PADDING_BOTTOM = 10 ; // padding unutrasnje stranice
    const FRONT_SIDE_width = 180;
    const TABELA_TEXT_width = 60;
    const TABELA_TEXT_height = 60;
    const TABELA_GRAFIK_diff = 30;
    
    /////////////////////////////////////////////////////
    private $pdf; // var za pdf koji se kreira   
    private function f_algoritamStart() {
        
        $this->f_kreirajPDFstranicu( $this->f_nadjiDimStr() );
        $this->f_crtajOkvire();
        $this->f_crtajLogo();
        $this->f_crtajTabelu();
        $this->f_crtajOrdinateStub();
        $this->f_crtajGrafik();
        $this->f_crtajFrontPage();  
        
        ob_end_clean();// bez ovoga dobijam fatal error pri pozivu output.a
        $this->pdf->Output("$this->projName.pdf", 'I'); // I- kao inline - user ga pregleda ga u browseru, ili preko saveAs
          
    }
    ////////////////////////////////////////////////////
    private function f_nadjiDimStr(){
        $sirina = self::MARGIN_LEFT + self::MARGIN_REST;
        $sirina += 2 * self::PADDING_X;
        $sirina += self::FRONT_SIDE_width;        
        $sirina += self::TABELA_TEXT_width + end($this->tlo_rel)["x"];
        
        $visina = 2 * self::MARGIN_REST;
        $visina += self::PADDING_TOP + self::PADDING_BOTTOM ;
        $visina += self::TABELA_TEXT_height;
        $visina += self::TABELA_GRAFIK_diff;
        $visina += max( array_column($this->tlo_rel, 'y') ); // visina grafika
        
        if ($visina<297)
            $visina = 297;
        
        
        return array(round($sirina),round($visina)); 
        
    }
    private function f_kreirajPDFstranicu($dimStr) {
        $this->pdf = new TCPDF(self::PDF_ORIENTATION, self::PDF_PAGE_UNIT, $dimStr , true, 'UTF-8', false);
        $this->setPdfSecurity();
        
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('MPU webAPP');
        $this->pdf->SetTitle($this->projName);
        $this->pdf->SetSubject('uzduzni profil trase DV');
        $this->pdf->SetKeywords('MPU webAPP, mehanicki proracun uzadi, provodnik, lancanica, uzduzni profil');
        
        $this->pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
        $this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(false);        
        $this->pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);
        $this->pdf->AddPage();
    }
    private function setPdfSecurity(){        
        switch ($this->userLevel){
            case 'free':                
                $this->pdf->SetProtection(array('print', 'modify', 'copy', 'annot-forms', 'fill-forms', 'extract', 'assemble', 'print-high'),
                    $this->pdfUserPass, $this->pdfMasterPass, 3);
                break;
            case 'lite':                
                $this->pdf->SetProtection(array(/*'print', */'modify', /*'copy',*/ 'annot-forms', 'fill-forms', 'extract', 'assemble' /*, 'print-high'*/),
                            "", $this->pdfMasterPass, 3);
                break;
                
            case 'pro':
                //bez zakljucavanja fajla
                break;
            
            default: // isto kao free
                $this->pdf->SetProtection(array('print', 'modify', 'copy', 'annot-forms', 'fill-forms', 'extract', 'assemble', 'print-high'),
                               $this->pdfUserPass, $this->pdfMasterPass, 3);                
                
        }
        
    }
    private function f_crtajOkvire(){
        $margina = 0;
        
        // spoljasnji okvir
        $this->pdf->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'phase' => 0, 'color' => array(0, 0, 0)));        
        $this->pdf->Rect($margina, $margina, 
            $this->f_getSirinaPdf()-2*$margina, 
            $this->f_getVisinaPdf()-2*$margina, 
            'D');
        
        // unutrasnji okvir    
        $this->pdf->SetLineStyle(array('width' => 0.7, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'phase' => 0, 'color' => array(0, 0, 0)));
        $this->pdf->Rect(
            self::MARGIN_LEFT, 
            self::MARGIN_REST,
            $this->f_getSirinaPdf() - self::MARGIN_LEFT -self::MARGIN_REST,
            $this->f_getVisinaPdf() - self::MARGIN_REST -self::MARGIN_REST,
            'D');
        
        //separator
        $x = $this->f_getSirinaPdf() - self::MARGIN_REST - self::FRONT_SIDE_width;
        $this->pdf->Line(
            $x,
            self::MARGIN_REST,
            $x,
            $this->f_getVisinaPdf() - self::MARGIN_REST,
            'D');
                    
        // set image scale factor
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    
        $y = $this->f_getVisinaPdf() - self::MARGIN_REST - 50;
        $this->pdf->Image(ROOT.'model/tablica.jpg', $x, $y, 180, 50, '', '', 'T', true, 300, '', false, false, 1, false, false, false);
        
    }
    private function f_crtajLogo(){
        switch ($this->userLevel){
            case 'free':
                $this->crtajWatermark();
                $this->crtajAppTekst("MPU webApp free");                
                break;
            case 'lite':
                $this->crtajAppTekst("MPU webApp lite");  
                break;
                
            case 'pro':
                $this->crtajAppTekst("MPU webApp pro");  
                break;
                
            default: // isto kao free
                $this->crtajWatermark();
                $this->crtajAppTekst();  
        }
        
    }
    private function crtajAppTekst($tekst){
        $startX = $this->f_getSirinaPdf() - self::MARGIN_REST - self::FRONT_SIDE_width/2;
        $startY = $this->f_getVisinaPdf() - self::MARGIN_REST - self::PADDING_BOTTOM - 50 - 200;
        
        $this->pdf->SetXY($startX,$startY);
        $this->pdf->SetFont ('helvetica', 'BI', 26);
        
        switch ($this->userLevel){
            case 'free': $alpha = 0.8; break;
            case 'lite': $alpha = 0.5; break;                
            case 'pro':  $alpha = 0.2; break;                
            default:     $alpha = 0.8;
        }
        $this->pdf->SetAlpha($alpha);
        
        $this->pdf->Cell(self::FRONT_SIDE_width/2 -15,20, $tekst ,0,2,'R', 0,'',0,false,'T','C');
        $this->pdf->SetFont ('helvetica', '', 12);
        $this->pdf->SetAlpha(1);
    }
    private function crtajWatermark(){        
        $ImageW = 152.4;
        $ImageH = 152.4;
        // Find the middle of the page and adjust.
        $startX = ($this->f_getSirinaPdf() - self::FRONT_SIDE_width)/ 2  - $ImageW/2;
        $startY = $this->f_getVisinaPdf()/ 2  - $ImageH/2 - 20;
        
        $this->pdf->SetAlpha(0.20);
        $this->pdf->Image(ROOT.'model/logo_bw2_realJpg.jpg', $startX, $startY, $ImageW, $ImageH, 'JPEG', '', '', true, 150);
        $this->pdf->SetAlpha(1);
    }
    private function f_crtajTabelu(){
                
        $startX = self::MARGIN_LEFT + self::PADDING_X;        
        $startY = self::MARGIN_REST + self::PADDING_BOTTOM + self::TABELA_TEXT_height;
        $startY = $this->f_getVisinaPdf() - $startY;
        
        $this->pdf->SetLineWidth(0.4);
        
        $this->rasponiRel = array();
        $this->rasponiAps = array();
        $dHaps = array();
        for ($i = 0; $i < sizeof($this->stubovi_rel)-1; $i++) {
            array_push($this->rasponiRel, $this->stubovi_rel[$i+1]["x"] - $this->stubovi_rel[$i]["x"]);
            array_push($this->rasponiAps, $this->stubovi[$i+1]["x"] - $this->stubovi[$i]["x"]);
            array_push($dHaps, round( $this->stubovi[$i+1]["y"]+$this->stubovi[$i+1]["h"]
                - $this->stubovi[$i]["y"]-$this->stubovi[$i]["h"], 2));
            
        }
        $this->pdf->SetXY($startX,$startY);
        $this->f_crtajRedTabele(20, "kota [m]");
        
        $startY += 20;
        $this->pdf->SetXY($startX,$startY);
        $this->f_crtajRedTabele(20, "stacionaza [m]");
        
        $startY += 20;
        $this->pdf->SetXY($startX,$startY);
        $this->f_crtajRedTabele(10, "raspon [m]" , $this->rasponiAps);
        
        $startY += 10;
        $this->pdf->SetXY($startX,$startY);
        $this->f_crtajRedTabele(10, "razlika u visini vesanja [m]" , $dHaps);
            
    }
    private function f_crtajRedTabele($visina, $hederText, $tekstArray=NULL){
        $this->pdf->Cell(
            self::TABELA_TEXT_width, /* cell width*/
            $visina,  /* cell height*/
            $hederText, /* cell text*/
            1, /* border */
            0,  /* 	Indicates where the current position should go after the call */
            'L',  /* align */
            0,  /* fill */
            '',  /* link */
            0,   /* strech */
            false,
            'T', /**/
            'C' /**/
            );
        if ($this->minStacRel_stub != 0) {
            $this->pdf->Cell($this->minStacRel_stub,$visina,"",1,0,'C', 0,'',0,false,'T','C');
        }
        for ($i = 0; $i < sizeof($this->rasponiRel); $i++) {
            $this->pdf->Cell($this->rasponiRel[$i],$visina,$tekstArray[$i],1,0,'C', 0,'',0,false,'T','C');
        }
    }
    private function f_crtajGrafik(){       
        
        $startX = self::MARGIN_LEFT + self::PADDING_X + self::TABELA_TEXT_width;
        $startY = self::MARGIN_REST + self::PADDING_BOTTOM + self::TABELA_TEXT_height + self::TABELA_GRAFIK_diff;
                
        $this->VAGRES_GRAFIK = array("x"=>$startX, "y"=>$startY );
        
        $stilTlo = array('width' => 1.2, 'cap' => 'butt', 'join' => 'round', 'dash' => '0', 'phase' => 0, 'color' => array(0, 0, 0));
        $this->f_crtajPoliliniju($this->tlo_rel,$stilTlo);
        
        $stilStub = array('width' => 1.0, 'cap' => 'round', 'join' => 'round', 'dash' => '0', 'phase' => 0, 'color' => array(85, 85, 85));
        $this->f_crtajStubove($this->stubovi_rel,$stilStub);
        
        $stilLanc = array('width' => 0.5, 'cap' => 'round', 'join' => 'round', 'dash' => '0', 'phase' => 0, 'color' => array(0, 0, 255));
        $this->f_crtajPoliliniju($this->lanc_rel,$stilLanc);
                
        $stilLanc_dash = array('width' => 0.5, 'cap' => 'round', 'join' => 'round', 'dash' => '7 3', 'phase' => 0, 'color' => array(0, 0, 255));
        $lanc_rel_dash = $this->lanc_rel; // kopira po vrednosti
        
        $sigVisina_rel = $this->f_skalirajH($this->sigVisina);
        foreach ($lanc_rel_dash as &$tacka)
            $tacka["y"] -= $sigVisina_rel;
        $this->f_crtajPoliliniju($lanc_rel_dash,$stilLanc_dash);
        
        
    }
    private function f_crtajOrdinateStub(){
        
        $startX = self::MARGIN_LEFT + self::PADDING_X + self::TABELA_TEXT_width;
        $startX += $this->minStacRel_stub;
        $startY = self::MARGIN_REST + self::PADDING_BOTTOM + self::TABELA_TEXT_height - 20 -20;
        $startY = $this->f_getVisinaPdf() - $startY;
        $this->pdf->SetXY($startX, $startY);
        
        $cell_border = array('B' => array('width' => 0.3, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'square'));
                
        for ($i = 0; $i < sizeof($this->stubovi_rel); $i++) {
            $this->pdf->StartTransform();
            $this->pdf->Rotate(90,$startX,$startY);
            $this->pdf->Cell(20, 10,round($this->stubovi[$i]["x"], 2),$cell_border,0,'C', 0,'',0,false,'B','B');
            $this->pdf->Cell(20, 10,round($this->stubovi[$i]["y"], 2),$cell_border,0,'C', 0,'',0,false,'B','B');
            $this->pdf->Cell(self::TABELA_GRAFIK_diff+$this->stubovi_rel[$i]["y"], 10,NULL,$cell_border,0,'C', 0,'',0,false,'B','B');
            $this->pdf->StopTransform();   
            
            if($i+1 >= sizeof($this->stubovi_rel))
                break;
            $startX += $this->stubovi_rel[$i+1]["x"] - $this->stubovi_rel[$i]["x"];
            $this->pdf->SetXY($startX, $startY);
        }
    }
    private function f_crtajFrontPage(){
        
        $this->pdf->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'phase' => 0, 'color' => array(0, 0, 0)));
    
        $startX = $this->f_getSirinaPdf() - self::MARGIN_REST - self::FRONT_SIDE_width + self::PADDING_X;
        $startY = $this->f_getVisinaPdf() - self::MARGIN_REST - self::PADDING_BOTTOM - 50 - 150;
        
        $this->pdf->SetXY($startX,$startY);
        $this->pdf->SetFont ('helvetica', 'B', 14);
        $tekst = "Uzduzni profil trase: ".$this->projName;
        $this->pdf->Cell(100,10, $tekst ,0,2,'L', 0,'',0,false,'T','C');
        
        $this->pdf->SetFont ('helvetica', '', 12);        
        $cell_border = array('B' => array('width' => 0.3, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'square'));
        $this->pdf->Cell(100,10, "parametri proracuna:" ,$cell_border,2,'L', 0,'',0,false,'T','B');
        
        $this->pdf->SetXY($startX+10,$startY+20);         
        
        
        $this->f_crtajTekstFrontPage("tip provodnika: ".$this->objJS["mTipProv"]["oznaka"]);
        $this->f_crtajTekstFrontPage("duzina zateznog polja: ".round(array_sum($this->rasponiAps),2)." [m]");
        $this->f_crtajTekstFrontPage("spec.tezina provodnika: ".round($this->objJS["mTipProv"]["tezina"], 6)." [daN/m*mm2]");
        $this->f_crtajTekstFrontPage("tezina provodnika sa ledom: ".round($this->objJS["tezinaProv_minus5led"], 6)." [daN/m*mm2]");
        $this->f_crtajTekstFrontPage("koef. ODO: ".$this->objJS["koefDodOpt"]);
        $this->f_crtajTekstFrontPage("temperatura: ".$this->objJS["t1"]." [degC]");
        $this->f_crtajTekstFrontPage("kritican raspon: ".round($this->objJS["mAkr"],2)." [m]");
        $this->f_crtajTekstFrontPage("idealan raspon: ".round($this->objJS["raspon"])." [m]");
        $this->f_crtajTekstFrontPage("sigurnosna visina: ".$this->sigVisina." [m]");
        $this->f_crtajTekstFrontPage("razmera za duzine: 1:".self::RAZMERA_X);
        $this->f_crtajTekstFrontPage("razmera za visine: 1:".self::RAZMERA_Y);
        
    }
    private function f_crtajTekstFrontPage($tekst){        
        $this->pdf->Cell(100,7, $tekst ,0,2,'L', 0,'',0,false,'T','C');        
    }
    ////////////////////////////////////////////////////
    /**
     * ukupna sirina pdf fajla
     * @return number
     */
    private function f_getSirinaPdf(){
        return $this->pdf->getPageWidth();
    }
    
    /**
     * ukupna visina pdf fajla
     * @return number 
     */
    private function f_getVisinaPdf(){
        return $this->pdf->getPageHeight(0);
    }
        
    /**
     * Vraca minimalnu kotu tla u [m]
     * @return number
     */
    private function f_minKotaTla(){
        return min( array_column($this->tlo, 'y') );
    }
    
    ////////////////////////////SKALIRANJE////////////    
    /**
     * Skalira x vrednost
     * @param number $varX_aps vrednost u [m]
     * @return number vrednost u [rel]
     */
    private function f_skalirajX($varX_aps){
        return    self::UNIT_CONVERSION * $varX_aps/self::RAZMERA_X;
    }
    
    /**
     * Skalira h vrednost
     * @param number $varH_aps vrednost u [m]
     * @return number vrednost u [rel]
     */
    private function f_skalirajH($varH_aps){
        return    self::UNIT_CONVERSION * $varH_aps/self::RAZMERA_Y;
    }
    
    /**
     * Skalira y vrednost
     * @param number $varY_aps vrednost u [m]
     * @return number vrednost u [rel]
     */
    private function f_skalirajY($varY_aps){
        return    $this->f_skalirajH($varY_aps - $this->f_minKotaTla());
    }
    
    /**
     * Skaliranje niza elemenata - pojedinacno skaliranje x,y,h vrednosti
     * @param array $niz niz sa elementima sa key= [x,y,h] vrenosti
     * @return array $niz sa skaliranim key= [x,y,h]
     */
    private function f_skalirajNiz(array $niz){
        if (array_key_exists("h",$niz[0])) {
            foreach ($niz as &$tacka){ // uzimamo referencu da bi birektno menjeli elemente $niz.a
                $tacka["x"] = $this->f_skalirajX($tacka["x"]);
                $tacka["y"] = $this->f_skalirajY($tacka["y"]);
                $tacka["h"] = $this->f_skalirajH($tacka["h"]);
            }
        }
        else{
            foreach ($niz as &$tacka){
                $tacka["x"] = $this->f_skalirajX($tacka["x"]);
                $tacka["y"] = $this->f_skalirajY($tacka["y"]);
            }
        }
        return $niz;
    }
    
    ////////////////////////////////////////////// 
    /**
     * Kreiranje niza brojeva [2,3,4,5...] iz niza obj [[2,3],[4,5]]
     * sto nam treba za crtanje polilinije
     * @param array $nizObjekata sa [x,y] vrenostima
     * @return array niz brojeva [1,2,3,4,5...]
     */
    private function f_createNizBroj_od_nizaObj(array $nizObjekata){
        $nizBrojeva = array();
        foreach ($nizObjekata as $obj)
            array_push($nizBrojeva, $obj["x"], $obj["y"] );
            return $nizBrojeva;
    }
    
    /**
     * Shift(offset) koordinata x,y za sve objekte u nizu
     * @param Array $nizObjekata niz objekata koji se siftuje [rel]
     * @param Array x,y $offsetArray offset vrednost [x,y]
     * @return Array $nizObjekata sa siftovanim vrednostima za [x,y]
     */
    private function f_offsetXY(array $nizObjekata, array $offsetArray){
        $visinaPdf = $this->f_getVisinaPdf();
        foreach ($nizObjekata as &$obj){
            $obj["x"] += $offsetArray["x"];
            $obj["y"] = $visinaPdf - $offsetArray["y"] - $obj["y"];
        }
        return $nizObjekata;
    }
    
    /**
     * Crta linije za sve stubove iz niza stubova u [mm]
     * @param Array stubova $nizStubova_aps u [mm]
     * @return void
     */
    private function f_crtajStubove($nizStubova_rel, $stilLinije){
        $nizStubova_rel = $this->f_offsetXY($nizStubova_rel, $this->VAGRES_GRAFIK);
        
        $this->pdf->SetLineStyle($stilLinije);
                
        for ($i = 0; $i < sizeof($nizStubova_rel); $i++) {
            $x = $nizStubova_rel[$i]["x"];
            $y = $nizStubova_rel[$i]["y"];
            $h = $nizStubova_rel[$i]["h"];
            
            $this->pdf->SetLineStyle($stilLinije);
            $this->pdf->Line($x, $y, $x, $y-$h);
            
            $this->pdf->SetXY($x - 20, $y-$h-10);
            $this->pdf->Cell(40, 10,"Hprov=".round($this->stubovi[$i]["h"], 2)."[m]",NULL,2,'C', 0,'',0,false,'B','B');
            $this->pdf->SetXY($x - 20, $y-$h-5);
            $this->pdf->Cell(40, 10,"r.b.".$this->stubovi[$i]["oznaka"],NULL,2,'C', 0,'',0,false,'B','B');
        }
    }
    
    /**
     * Crta poliliniju na osnovu zadatog niza tacaka u [mm]
     * @param Array $nizTacaka_rel sa vrednostima u [mm]
     * @return void
     */
    private function f_crtajPoliliniju($nizTacaka_rel, $stilLinije){
        $nizTacaka_rel = $this->f_offsetXY($nizTacaka_rel,$this->VAGRES_GRAFIK);
        $nizTacaka_rel = $this->f_createNizBroj_od_nizaObj($nizTacaka_rel);
        
        $this->pdf->SetLineStyle($stilLinije);
        $this->pdf->PolyLine($nizTacaka_rel, 'D');
    }
         
}

/*
/**
     * Kreiranje pdf fajla na osnovu json nizova
     * @param string $projName
     * @param JsonSerializable $JSONtlo
     * @param JsonSerializable $JSONstubovi
     * @param JsonSerializable $JSONlanc
     * @return PdfCreator pdf fajl
     */
/*
    public static function createFromJSON($projName,$JSONtlo, $JSONstubovi, $JSONlanc, $JSONobjJS) {            
        $instanca = new self(
            $projName,
            json_decode($JSONtlo,true),
            json_decode($JSONstubovi,true),
            json_decode($JSONlanc,true),
            json_decode($JSONobjJS,true) );
        
         return $instanca;
    }
*/
?>