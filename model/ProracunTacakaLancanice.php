<?php
require_once ROOT.'model/access_file.php';
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}

$err_msg;
const ERR_FLAG = -9919; 

/** 
 * Kreiranje niza raspona na osnovu niza stubnih mesta
 * @author Nikola Pavlovic
 * @param array $nizStubnihMesta Niz stubnih mesta
 * @return array raspona u zateznom polju
 */ 
function f_kreiraj_nizRaspona(array $nizStubnihMesta){
    if( ! is_array($nizStubnihMesta) ){
        $err_msg = "nizStubnihMesta nije niz";
        return FALSE;
    }elseif (sizeof($nizStubnihMesta) < 2 ){
        $err_msg = "nizStubnihMesta ima samo jedan clan";
        return FALSE;
    }
    $niz_raspona = array();    
    // $nizStubnihMesta.length - 1  !!!
    for ($i = 0; $i < sizeof($nizStubnihMesta) - 1; $i++) {
        $niz_raspona[] = $nizStubnihMesta[$i+1]["x"] - $nizStubnihMesta[$i]["x"];
    }    
    return $niz_raspona;
}

/**
 * Kreiranje niza cosFi za svaki od raspona
 * @author Nikola Pavlovic
 * @param array $rasponi niz raspona u celom zat.polju
 * @param array $nizStubnihMesta niz svih stubnih mesta
 * @return array niz cosFi
 */
function f_kreiraj_nizCosFi(array $rasponi, array $nizStubnihMesta){
    $niz_cosFi = array();    
    // cosFi imamo za svaki raspon
    for ($i = 0; $i < sizeof($rasponi); $i++) {
        $visVesanja0 = $nizStubnihMesta[$i]["y"] + $nizStubnihMesta[$i]["h"];
        $visVesanja1 = $nizStubnihMesta[$i+1]["y"] + $nizStubnihMesta[$i+1]["h"];
        $niz_cosFi[] = $rasponi[$i] / sqrt( pow( $rasponi[$i], 2) +
            pow( $visVesanja1 - $visVesanja0, 2) );
    }    
    return $niz_cosFi;
}

/**
 * Racuna idealni raspon u zateznom polju u skladu sa --- Milenko Djuric - Elementi EES-a 2009  pages34-48 
 * 
 * koristi funkciju f_idealniCosFi() u toku proracuna
 * 
 * @author Nikola Pavlovic
 * @param array $nizRaspona niz raspona u zateznom polju
 * @param array $nizCosFi niz cosFi u zateznom polju
 * @return number vrednost idealnog raspona
 */
function f_idealniRaspon(array $nizRaspona, array $nizCosFi){
    $sum_gore=0; $sum_dole=0;
    for ($i = 0; $i < sizeof($nizRaspona); $i++) {
        $sum_gore += pow($nizRaspona[$i],3) * pow($nizCosFi[$i],2);
        $sum_dole += $nizRaspona[$i];
    }
    return (1/f_idealniCosFi($nizRaspona, $nizCosFi)) * sqrt($sum_gore/$sum_dole);
}

/**
 * Racuna idealni cosFi u zateznom polju u skladu sa --- Milenko Djuric - Elementi EES-a 2009  pages34-48
 *
 * @author Nikola Pavlovic
 * @param array $nizRaspona niz raspona u zateznom polju
 * @param array $nizCosFi niz cosFi u zateznom polju
 * @return number vrednost idealnog cosFi
 */
function f_idealniCosFi(array $nizRaspona, array $nizCosFi){
    $sum_gore=0; $sum_dole=0;
    for ($i = 0; $i < sizeof($nizRaspona); $i++) {
        $sum_gore += $nizRaspona[$i];
        $sum_dole += $nizRaspona[$i]/$nizCosFi[$i];
    }
    return $sum_gore/$sum_dole;
}

function f_kreirajNizStacProv(array $nizStubnihMesta){
    $nizStac_prov_X = array();
    
    $prvaStac = $nizStubnihMesta[0]["x"];
    $drugaStac = (ceil($prvaStac)==$prvaStac) ? $prvaStac+1 : ceil($prvaStac);
    $poslednjaStac = end($nizStubnihMesta)["x"];  
    
    $nizStac_prov_X[]=array("x" => $prvaStac);
    for ($i = $drugaStac; $i < $poslednjaStac; $i++)
        $nizStac_prov_X[]=array("x" => $i);
        $nizStac_prov_X[]=array("x" => $poslednjaStac);
    
    return $nizStac_prov_X;
}

function f_nadjiTemeLancanice(/*int*/ $i, /*float*/ $parametarLancanice,
                                      array $nizStubnihMesta){
        $nizRaspona = f_kreiraj_nizRaspona($nizStubnihMesta);
        
        $visinaVesanjaProv1 = $nizStubnihMesta[$i]["y"] + $nizStubnihMesta[$i]["h"];
        $visinaVesanjaProv2 = $nizStubnihMesta[$i+1]["y"] + $nizStubnihMesta[$i+1]["h"];
        $dH = $visinaVesanjaProv2 - $visinaVesanjaProv1;
        //echo "eeeeeeeee ".$nizStubnihMesta[$i+1]["h"];
        
        return 
        $nizStubnihMesta[$i]["x"] 
        + $nizRaspona[$i] / 2
        - $parametarLancanice 
        * asinh(
           $dH / (2 * $parametarLancanice 
               * sinh($nizRaspona[$i] / (2 * $parametarLancanice))
                 )
        );
        
}

function f_lancanica_vredY_u_tackiX(/*float*/ $x, /*float*/ $p) {
    // x je rastojanje od temena lancanice
    // p = parametar;
    //teme lancanice je u [0,p]             --- p * Math.cosh(x/p)
    // teme lancanice je u koord. pocetku   --- p * Math.cosh(x/p) - p
    return $p*cosh($x/$p) - $p;
}

function f_tackeLancanice(/*float*/ $parametarLancanice, array $nizStubnihMesta){
    
    $nizTacaka_lanc = array();
    
    $nizTacaka_lanc = f_kreirajNizStacProv($nizStubnihMesta);
    
    $dodatak_Y;$teme_lanc_X;
    $indexRaspona=0;
    $indexRaspona_prethodnaIteracija=-1; // da bi se indeksi razlikovali u pocetnoj iteraciji
    for ($i = 0; $i <  sizeof($nizTacaka_lanc)-1; $i++) {
        
        if($nizTacaka_lanc[$i]["x"] >= $nizStubnihMesta[$indexRaspona]["x"]){
            $teme_lanc_X = f_nadjiTemeLancanice($indexRaspona, $parametarLancanice, $nizStubnihMesta);
            $kotaVesanja = $nizStubnihMesta[$indexRaspona]["y"]+$nizStubnihMesta[$indexRaspona]["h"];
            $dodatak_Y = $kotaVesanja 
                    - f_lancanica_vredY_u_tackiX($nizStubnihMesta[$indexRaspona]["x"] - $teme_lanc_X, $parametarLancanice);
            $indexRaspona++;
        }
        
        $nizTacaka_lanc[$i]["y"] = $dodatak_Y +
        f_lancanica_vredY_u_tackiX( $nizTacaka_lanc[$i]["x"] - $teme_lanc_X, $parametarLancanice );
        
    }
    # end($nizTacaka_lanc)["y"] --- ne bi radalo
    $nizTacaka_lanc[sizeof($nizTacaka_lanc)-1]["y"] = $dodatak_Y +
    f_lancanica_vredY_u_tackiX( end($nizTacaka_lanc)["x"] - $teme_lanc_X, $parametarLancanice );

    
    return $nizTacaka_lanc;

}
 /*   
    int index_x_koor;
    int indexStuba;
    int indexStuba_prethodnaIteracija=-1; // da bi se indeksi razlikovali u prvoj iteraciji
    float teme_lanc_X=0, kotaVesanjaUze_Y, y_dodatak=0;
    //double[] nizTemenaLancanica = nizTemenaLancanica(parametarLancanice,nizStacionaza_STUB_X,nizKota_STUBiUZE_Y);
    for ($i = 0; $i <  $brojTacaka; $i++) {
        for (int j = 0; j < brojMedjuTacaka; j++) {
            // if (BuildConfig.DEBUG)Log.e("TAG17","i= "+ i +" j= "+ j  );
            
            index_x_koor = i*brojMedjuTacaka + j; //  povecavano broj stacionaza lancanice na ukupno brojTacaka*brojMedjuTacaka
            float dodatak = (i+j/(float)brojMedjuTacaka); //    j/10 vraca int -> ne bi imao decimalnu vrednost
            
            // kako ide petlja tako se krecemo od pocetnog stuba, tacku po tacku
            nizStacionaza_lancanica_X[index_x_koor] = nizStacionaza_STUB_X[0] + dodatak;
            
            // ovo je mo�no
            indexStuba = ProveraSigVisina_Fragment.indexSusednog_ManjegClanaNiza(nizStacionaza_lancanica_X[index_x_koor], nizStacionaza_STUB_X);
            
            // neke velicine se ne menjaju za tacke koje su u istom rasponu
            if (indexStuba_prethodnaIteracija != indexStuba) {
                
                teme_lanc_X = (float) nadjiTemeLancanice(indexStuba, parametarLancanice,
                    nizStacionaza_STUB_X,nizKota_STUBiUZE_Y );
                
                kotaVesanjaUze_Y = nizKota_STUBiUZE_Y[indexStuba];
                
                y_dodatak = kotaVesanjaUze_Y -
                lancanica_vredY_u_tackiX(nizStacionaza_STUB_X[indexStuba] - teme_lanc_X, parametarLancanice);
            }
            
            nizKota_lancanica_Y[index_x_koor] = y_dodatak +
            lancanica_vredY_u_tackiX( nizStacionaza_lancanica_X[index_x_koor] - teme_lanc_X, parametarLancanice );
            
            indexStuba_prethodnaIteracija = indexStuba;
        }
        
    }
    // if (BuildConfig.DEBUG)Log.e("TAG17","nizStacionaza_lancanica_X.length= "+ nizStacionaza_lancanica_X.length);
    // if (BuildConfig.DEBUG)Log.e("TAG17","nizStacionaza_lancanica_X "+ Arrays.toString(nizStacionaza_lancanica_X));
    
    return nizKota_lancanica_Y;
    
}
*/

function f_printAsocNiz(array $niz){
    echo "<br><br>";
    $i=0;
    foreach ($niz as $obj) {
        echo "<br>no$i ";
        $i++;
        foreach ($obj as $key => $value)
            echo "  [$key] = $value  ";
    }
}







?>