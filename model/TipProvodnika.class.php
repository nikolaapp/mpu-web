<?php
require_once ROOT.'model/access_file.php';
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}

require_once ROOT.'model/DAOprovodnici.php';

/**
 *
 * @author Nikola
 *        
 */
class TipProvodnika
{
    private $mId;
    private $mOznakaProv;
    private $mPresek;
    private $mPrecnik;
    private $mSpecTezina;
    private $mModulElast;
    private $mKoefAlfa;
    private $mI;
    private $mJcena;
    private $mIsProtected;
    
    /**
     */
    public function __construct($oznakaUzeta, $presek, $precnik,
        $specTezina, $modulElast, $koefAlfa, $I, $jcena, $izmenljivo) {
            $this->mId = UUID.randomUUID();
            $this->mOznakaUzeta = oznakaUzeta;
            $this->mPresek = presek;
            $this->mPrecnik = precnik;
            $this->mSpecTezina = specTezina;
            $this->mModulElast = modulElast;
            $this->mKoefAlfa = koefAlfa;
            $this->mI = I;
            $this->mJcena = jcena;
            $this->$mIsProtected = izmenljivo;
    }
}

