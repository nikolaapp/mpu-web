<?php

/**
 * PROVERA PRISTUPA FAJLOVIMA
 * definicija TOP_FILE varijable za potrebe svih kontrolera
 * a ostali fajlovi ce imati definisan TOP_FILE samo ako su includovani 
 * preko kontolera
 * ako nisu onda TOP_FILE nije definisan i pokrece se zastita iz access_file
 */
define('TOP_FILE',"topFileConstDEFINED");

/**
 * 1-ispisuje gresku u error.php
 * 0-ispisuje gresku na datoj stranici preko $_SESSION['msg']
 * @var int 
 */
define('DEBUG_LEVEL',0);

/**
 * Ispisuje gresku na stranici koju odredjuje DEBUG_LEVEL
 
 * @param string|int $errKlijent - ispisujemo kod klijenta
 * @param string $errServer - ispisuje na error.php, pri DEBUG_LEVEL>0
 * @param string $heder - pokazuje gde vrsimo redirekciju
 */
function f_debug( $errKlijent, $errServer, $heder=NULL){
    unset($_SESSION['errMsg']);
    unset($_SESSION['errStatus']);
    unset($_SESSION['msg']);
    unset($_SESSION['status']);
    
    if (DEBUG_LEVEL===1){
        header("location: ../view/error.php");
        $_SESSION['errStatus']  = $errServer;
        $_SESSION['errMsg']  = $errKlijent;
    }
    else if (DEBUG_LEVEL===0){
        if ($heder) {
            header($heder);
        }
        // produckijsi nivo - klijentu vracamo samo status 
        $_SESSION['msg']  = $errKlijent;
        
    }
    
    exit();
}

?>