<?php

/**
 * PROVERA PRISTUPA FAJLOVIMA
 */

if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    // url adresa se poklata sa adresom fajla
    // ne radi kada se includuje u drugi fajl - pa mora da se stavlja u svaki fajl posebno
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}
elseif (! defined('TOP_FILE' /*ovde mora pod navodnicima*/) ){
    // nije definisana konstanta TOP_FILE - definise se preko kontrolera
    neovlascenPristup("TOP _ FILE");
}
elseif( ! isset($_SESSION['user']) )  {
    // korisnik nije logovan   
    //exit(header("location: ../view/login.php"));
}

/**
 * Akcija pri neovljascenom pristupu fajlu
 */
function neovlascenPristup($msg){
    //echo "<br><br>$msg<br><br>";
    
    //Up to you which header to send, some prefer 404 even if
    //the files does exist for security
    header( 'HTTP/1.0 404 Not Found', TRUE, 404 );
    
    // choose the appropriate page to redirect users
    exit( header( 'location: ../error.html' ) );
}
?>