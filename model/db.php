<?php
require_once ROOT.'model/access_file.php';
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}

class DB{
    private static $factory;

    public static function createInstance($config = null){ 
                
        // define constanta mora pod navodnicima
        if ( defined('IS_LOCAL')  ) {
            //xampp localhost 
            $settings['dbname'] = 'mpu';
            $settings['dbhost'] = '127.0.0.1';		
            $settings['dbuser'] = 'root';
            $settings['dbpass'] = '';	
        }
        else{                
    		//azure
            $settings['dbname'] = 'mpu';
            $settings['dbhost'] = '127.0.0.1:53777'; //PORT NIJE FIKSAN NA AZURE !?? - greska 1819
            $settings['dbuser'] = 'azure';
            $settings['dbpass'] = '6#vWHD_$';
        }
        
        	
        

        try{
            $dsn = 'mysql:dbname=' . $settings['dbname'] . ';host=' . $settings['dbhost'];
            $pdo = new PDO($dsn, $settings['dbuser'], $settings['dbpass'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            self::$factory[$config] = $pdo;

            return self::$factory[$config];
        }
        catch (PDOException $e) {
            $greska = array(
                /*"statusErr"=>$e->getCode(),*///maskiram status
                "statusErr"=>1819,
                "msgErr"=>$e->getMessage() );
            return $greska;
        }
    }
}
?>
