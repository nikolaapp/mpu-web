<?php

require_once ROOT.'model/access_file.php';

const NOV_RED = "<br><br>";
const STATUS_SUCCESS = "success";
const NIZ_TEMPERATURA = [-20,-10,0,10,20,40,60,80,-5]; ### bitno je da -5 bude na kraju

/**
 * Provera poslate parametre - zastita W3S
 * @todo omoguci provery za nizove
 * @param variant $data
 * @return variant checked data
 */
function check_input($data) {
    if (! is_array($data) ){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        // nece na azure da radi a i ne treba mi jer koristip PDO prepared statments
        //$data = mysql_real_escape_string($data);
    }
    else {
        ### ??????
    }
    return $data;
}

/**
 * Ocitavanje parametra poslatog POST metodom
 * @param string $name
 * @param bool $isJson
 * @return string
 */
function readPost($name, $isJson=false) {
    if (is_string($name)) 
        if ($isJson)
            return isset($_POST[$name]) ? check_input(json_decode($_POST[$name],true)) : "";
        else 
            return isset($_POST[$name]) ? check_input($_POST[$name]) : "";
    else
        return "";    
}

/**
 * Ocitavanje parametra poslatog GET metodom
 * @param string $name
 * @return string
 */
function readGet($name) {
    if (is_string($name))
        return isset($_GET[$name]) ? check_input($_GET[$name]) : "";
    else
        return "";
}

/**
 * Proverava da li postoji provodnik sa datom oznakom u bazi
 * @param string $oznakaProv
 * @return boolean|array false ako nema i $tipProv ako je pronadjen
 */
function check_oznakaProv($userId, $oznakaProv){
    require_once ROOT.'model/DAOprovodnici.php';
    $dao = new DAOprovodnici();
    $tipProv = $dao->selectProvByOZNAKA((int)$userId,$oznakaProv);
    
    if (is_null($tipProv["presek"])){
        return false;
    }
    else{
        return $tipProv;
    }
}

/**
 * AJAX - Forma povratnih rezultata od servera ka useru 
 * @param string $status
 * @param string $msg
 * @param variant $data
 */
function posaljiRezAjax($status, $msg=NULL, $data=NULL){
    $jsonObj = array();
    $jsonObj["status"] = $status;
    $jsonObj["msg"] = $msg;
    $jsonObj["data"] = $data;
    echo json_encode($jsonObj);
}


/**
 * provera zajednickih variajbli za save/update
 * @return boolean|string false(greska) | poruka sve je ok
 */
function f_proveriUnoseProjekat(){
    global $projName,$projId,$jsID,$tackeTla,$objektiTrase;
    global $stubnaMesta,$tackeLanc,$oznakaProv,$odo,$temperatura, $naprezanje_max;
    global $userId;
    $msg="";
    
    if (empty($projName) || empty($oznakaProv) || empty($odo)
        || (empty($temperatura)&&$temperatura!=0)
        || empty($naprezanje_max)){
            posaljiRezAjax('errorS1',"nisu uneta sva polja potrebna za kreiranje lancanice",$projName);
            return false;
    }
    
    if ( !empty($jsID) && !is_numeric($jsID)){ // ako postoji a nije broj
        posaljiRezAjax('errorS1-s',"nepravilna oznaka jednacine");
        return false;
    }
    
    if ( !check_oznakaProv($userId,$oznakaProv) || !is_numeric($odo) || !is_numeric($temperatura) || !is_numeric($naprezanje_max)){
        $ms="nepravilni unosi parametara lancanice"
            ." ".check_oznakaProv($userId, $oznakaProv)." ".is_numeric($odo)
            ." ".is_numeric($temperatura)." ".is_numeric($naprezanje_max);
            posaljiRezAjax('errorS2',$ms);
            return false;
    }
    
    
    if(empty($tackeTla))
        $tackeTla = NULL; //ali nema break;
        elseif( ! is_array($tackeTla) || sizeof($tackeTla)<2){
            posaljiRezAjax('errorS3',"\nmoraju biti unete bar dve tacke uzduznog profila");
            return false;
        }
        else
            $msg .= "\nsacuvan je profil trase, noTacaka:".sizeof($tackeTla);
            
        if(empty($objektiTrase))
            $objektiTrase = NULL;
            elseif( ! is_array($objektiTrase) || sizeof($objektiTrase)<1){
                posaljiRezAjax('errorS3-1',"\nmora biti unet bar jedan objekat");
                return false;
            }
        else
            $msg .= "\nsacuvni su objekti duz trase, noObjekata:".sizeof($objektiTrase);
                    
        if(empty($stubnaMesta))
            $stubnaMesta = NULL;
            elseif( ! is_array($stubnaMesta) || sizeof($stubnaMesta)<1){
                posaljiRezAjax('errorS4',"\nmora biti uneta bar jedno stubno mesto");
                return false;
            }
        else
            $msg .= "\nsacuvana su stubna mesta, noStubnihMesta:".sizeof($stubnaMesta);
                
        if(empty($tackeLanc))
            $tackeLanc = NULL;
            elseif( ! is_array($tackeLanc) || sizeof($tackeLanc)<2){
                posaljiRezAjax('errorS5',"\nmoraju biti unete bar dve tacke lancanice");
                return false;
            }
        else
            $msg .= "\nsacuvane su tacke lancanice, noTacakaLanc:".sizeof($tackeLanc);
                        
        return $msg;
                                    
}

/**
 * test i handle require_once naredbe
 * @param string $path do fajla koji ucitavamo
 */
function f_fileTestAndRequire($path){
    if (is_readable($path) ){
        require_once $path;
    }
    else {
        posaljiRezAjax("ErrorRq1", 'nedostupni podaci', $path);
        exit();
    }
}

/**
 * zbog nastale greske pri radu sa bazom unset-ovan je user
 * vise mu nije dostpna baza, moze samo da koristi klijentske servise
 */
function f_userUnsetAjaxCall(){
    // promenjeno je vise unosa - ALERT nesto nije uredu
    unset($_SESSION['user']); // izbrisan loged in user
    // user vise nista ne moze da koristi, moze samo da se izloguje
    posaljiRezAjax("PAZNJA", "SERVIS VISE NIJE DOSTUPAN");
    
    
}



?>