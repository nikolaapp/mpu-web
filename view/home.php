<!DOCTYPE html>
<html lang="sr">

<head>
    
    <?php   include_once '../view/headData.html';   ?>     
 
    <!-- CSS -->
    <link rel="stylesheet" href="css/loader.css">
    <link rel="stylesheet" href="libraries/fonts/fontawesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/slicknav.min.css">
    <link rel="stylesheet" href="assets/css/magnificpopup.css">
    <link rel="stylesheet" href="assets/css/jquery.mb.YTPlayer.min.css">
    <link rel="stylesheet" href="assets/css/typography.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <style>
        
    </style>

</head>

<body>
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- NAV area start -->
    <header id="header">
        <div class="header-area">
            <div class="container">
                <div class="row">
                    <div class="menu-area" style="width:100%;">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="logo">
                                <a href="../index.php">
                                    <img style="max-height: 70px;" src="media/logo1.png" alt="MPU mehanicki proracun uzadi logo">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-9 hidden-xs hidden-sm">
                            <div class="main-menu">
                                <nav class="nav-menu">
                                    <ul>
                                        <li class="active">
                                            <a href="#home" class="text-center">Home</a>
                                        </li>
                                        <li>
                                            <a href="#appInfo">App info</a>
                                        </li>
                                        <li>
                                            <a href="#webApp">Web app</a>
                                        </li>
                                        <li>
                                            <a href="#androidApp">Andoid app</a>
                                        </li>
                                        <!-- <li><a href="#pricing">Pricing</a></li> -->
                                        <!-- <li><a href="#team">Team</a></li> -->
                                        <li>
                                            <a href="#download">
                                                <img src="media/app_load.gif" alt="app load">
                                                <!-- <span class="badge" style="background-color: whitesmoke;font-size: 1em;color: #5c1ef9;">
                                                    App load
                                                </span> -->
                                            </a>
                                        </li>
                                        <!-- <li><a href="#blog">Blog</a></li> -->
                                        <li>
                                            <a href="#contact">Contact</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12 visible-sm visible-xs">
                            <div class="mobile_menu"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- NAV area end -->
    <!-- HOME area start -->
    <section id="home" class="slider-area image-background parallax" data-speed="3" data-bg-image="assets/img/bg/slider-bg.jpg">
        <div class="container">
            <div class="col-md-6 col-sm-6 hidden-xs">
                <div class="row">
                    <div class="slider-img">
                        <img src="media/profil trase.png" alt="slider image">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="slider-inner text-right">
                        <h2>Mehanički Proračun Užadi</h2>
                        <h5>nadzemnih vodova</h5>
                        <a class="expand-video" href="https://www.youtube.com/watch?v=Wi1TOvYwzE8">
                            <i class="fas fa-play"></i>web app</a>
                        <a class="expand-video" href="https://www.youtube.com/watch?v=4_9BOiOq86c">
                            <i class="fas fa-play"></i>android app</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- HOME area end -->
    <!-- service area start -->
    <div class="service-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 col-12">
                    <div class="service-single">
                        <a href="#webApp" data-placement="bottom" data-toggle="tooltip" title="Pristupa se na ovom sajtu">
                            <img src="assets/img/service/service-img2.png" alt="service image">
                            <h2>Web app</h2>
                            <p>Aplikacija za ON-line rad na internet    pretrazivacima</p>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12  col-12">
                    <div class="service-single">
                        <a href="#androidApp" data-placement="bottom" data-toggle="tooltip" title="Dostuno na google playstore">
                            <img src="assets/img/service/service-img1.png" alt="service image">
                            <h2>Android app</h2>
                            <p>Aplikacija za OFF-line rad na android uredjima</p>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 col-12">
                    <div class="service-single">
                        <a href="#twitterFeed" data-placement="bottom" data-toggle="tooltip" title="Pogledaj twitter feed na dnu stranice">
                            <img style="max-width: 200px;" src="media/logo-twitter.png" alt="twiter image">
                            <h2>MPU na tviteru</h2>
                            <p>Vesti, komentari, predlozi, najave...</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- service area end -->
    <!-- APP INFO area start -->
    <section class="about-area ptb--100" id="appInfo">
        <div class="container">
            <div class="section-title">
                <h2>Više o MPU</h2>
                <p>dve nezavisne aplikacije, za Android i za web sisteme</p>
            </div>
            <div class="row d-flex flex-center">
                <div class="col-md-6 col-sm-6 hidden-xs">
                    <div class="about-left-img">
                        <img src="assets/img/about/abt-left-img1.png" alt="image">
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 d-flex flex-center">
                    <div class="about-content">
                        <p>
                            <span class="badge" style="background-color: dodgerblue;font-size: 1em;">MPU</span> je android/web aplikacija koja izvršava mehanični proračun zaštitnih užadi, ili faznih
                            provodnika, nadzemnih vodova, kao i brojne proračune vezane za projektovanje dalekovoda naponskog
                            nivoa ≤110 kV, sa rasponima ≤500m i uglom nagiba u rasponu ≤30°.
                        </p>
                        <p>
                            Android aplikacija se instalira se na android telefonu/tabletu (ili android emulatoru na kompjuteru). Podržava sve android
                            verzije počev od „jelly_bean“ (API nivo: 16), odnosno 99% svih android uređaja koji su danas
                            u upotrebi. Nakon istaliranja nije potrebna internet konekcija za rad aplikacije.
                        </p>
                        <p>
                            Web aplikaciji se pristupa preko web pretraživača (Chrome, Mozilla, Opera). Ne zahteva brzu internet vezu, niti zauzaima
                            mnogo resursa pri korišćenju. Svi proračuni se izvršavaju na serverskoj strani.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- APP INFO area end -->

    <!-- WEB APP area start -->
    <section class="video-area ptb--100" id="webApp">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
                    <h2>web app</h2>
                    <p>
                        Izvođenje mehaničkih proračuna na širokom ekranu, koristeći internet pretraživače
                        <strong>Chrome/Mozilla/Opera</strong> za pristup web aplikaciji.

                    </p>
                    <p>
                        Ispod se nalazi video pojašnjenje mogućnosti i načina upotrebe web aplikacije
                    </p>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLR704GUNgXYgVKwmkQCHvZcJZlcWjr9XX" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    <!-- <a class="expand-video" href="https://www.youtube.com/watch?v=8qs2dZO6wcc">
                        <i class="fas fa-play"></i>
                    </a> -->
                </div>
            </div>
        </div>
    </section>
    <!-- WEB APP area end -->

    <!-- ANDROID APP area start -->
    <section class="feature-area bg-gray ptb--100" id="androidApp">
        <div class="container">
            <div class="section-title">
                <h2>Android app</h2>
                <p>više nezavisnih modula u okviru jedne aplikacije</p>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="ft-content rtl">
                        <div class="ft-single">
                            <img src="assets/img/icon/feature/1.png" alt="icon">
                            <div class="meta-content">
                                <a target="_blank" href="https://mpuapp.wordpress.com/2018/05/18/jednacina-stanja-2/">
                                    <h2>Jednačina stanja</h2>
                                    <p>Proračun naprezanja provodnika i ugiba rešavanjem jednačine stanja za zadate paremetre</p>
                                </a>
                            </div>
                        </div>
                        <div class="ft-single">
                            <img src="assets/img/icon/feature/2.png" alt="icon">
                            <div class="meta-content">
                                <a target="_blank" href="https://mpuapp.wordpress.com/2018/05/18/ukrstan%d1%98e-dva-dalekovoda-2/">
                                    <h2>Ukrštanјe dva dalekovoda</h2>
                                    <p>Proračun vertikalnog rastojanja između provodnika dva dalekovoda na mestu ukrštanja</p>
                                </a>
                            </div>
                        </div>
                        <div class="ft-single">
                            <img src="assets/img/icon/feature/3.png" alt="icon">
                            <div class="meta-content">
                                <a target="_blank" href="https://mpuapp.wordpress.com/2018/05/18/provera-sigurnosnih-visina/">
                                    <h2>Provera sigurnosnih visina</h2>
                                    <p>Kreiranje poprečnog profila, proračun ugiba, naprezanja stubova, sigurnosnih razmaka,
                                        otklona izolatora i predmera/predračuna</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 hidden-sm col-xs-12">
                    <div class="ft-screen-img">
                        <img src="assets/img/mobile/ft-screen-img1.png" alt="image">
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="ft-content">
                        <div class="ft-single">
                            <img src="assets/img/icon/feature/4.png" alt="icon">
                            <div class="meta-content">
                                <a target="_blank" href="https://mpuapp.wordpress.com/2018/05/18/tabele-ugiba-2/">
                                    <h2>Tabele ugiba</h2>
                                    <p> Proračun ugiba u svakom pojedinačnom rasponu zateznog polјa, sa formiranjem tabele ugiba</p>
                                </a>
                            </div>
                        </div>
                        <div class="ft-single">
                            <img src="assets/img/icon/feature/5.png" alt="icon">
                            <div class="meta-content">
                                <a target="_blank" href="https://mpuapp.wordpress.com/2018/05/18/stvarno-naprezan%d1%98e-2/">
                                    <h2>Stvarno naprezanјe</h2>
                                    <p>Proračun naprezanja provodnika na osnovu merenja ugiba u rasponu</p>
                                </a>
                            </div>
                        </div>
                        <div class="ft-single">
                            <img src="assets/img/icon/feature/6.png" alt="icon">
                            <div class="meta-content">
                                <a target="_blank" href="https://mpuapp.wordpress.com/2018/05/18/tipovi-provodnika-2/">
                                    <h2>Tipovi užadi</h2>
                                    <p>Dodavanje/brisanje/izmena/detalji o tipovoma provodnika/užadi</p>
                                </a>
                            </div>
                        </div>
                        <div class="ft-single">
                            <img src="assets/img/icon/feature/7.png" alt="icon">
                            <div class="meta-content">
                                <a target="_blank" href="https://mpuapp.wordpress.com/2018/05/18/temperatura-provodnika-2/">
                                    <h2>Temperatura provodnika</h2>
                                    <p>Procena temperature provodnika u stacionarnom stanju</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ANDROID APP area end -->

    <!-- screen slider area start -->
    <section class="screen-area bg-gray ptb--100" id="screenshot">
        <div class="container">
            <div class="section-title">
                <h2>Screenshot</h2>
                <p>izgled ekrana pri unosu parametara i prikazu rezultata proračuna</p>
            </div>
            <img class="screen-img" src="assets/img/mobile/screen-slider.png" alt="mobile screen">
            <div class="screen-slider owl-carousel">
                <div class="single-screen">
                    <img src="assets/img/mobile/screen-slider/screen1.jpg" alt="mobile screen">
                </div>
                <div class="single-screen">
                    <img src="assets/img/mobile/screen-slider/screen2.jpg" alt="mobile screen">
                </div>
                <div class="single-screen">
                    <img src="assets/img/mobile/screen-slider/screen3.jpg" alt="mobile screen">
                </div>
                <div class="single-screen">
                    <img src="assets/img/mobile/screen-slider/screen4.jpg" alt="mobile screen">
                </div>
                <div class="single-screen">
                    <img src="assets/img/mobile/screen-slider/screen5.jpg" alt="mobile screen">
                </div>
                <div class="single-screen">
                    <img src="assets/img/mobile/screen-slider/screen6.jpg" alt="mobile screen">
                </div>
                <div class="single-screen">
                    <img src="assets/img/mobile/screen-slider/screen6-1.jpg" alt="mobile screen">
                </div>
                <div class="single-screen">
                    <img src="assets/img/mobile/screen-slider/screen6-2.jpg" alt="mobile screen">
                </div>
                <div class="single-screen">
                    <img src="assets/img/mobile/screen-slider/screen7.jpg" alt="mobile screen">
                </div>
                <div class="single-screen">
                    <img src="assets/img/mobile/screen-slider/screen8.jpg" alt="mobile screen">
                </div>
                <div class="single-screen">
                    <img src="assets/img/mobile/screen-slider/screen9.jpg" alt="mobile screen">
                </div>
            </div>
        </div>
    </section>
    <!-- screen slider area end -->

    <!-- APP LOAD area start -->
    <section class="call-to-action ptb--100" id="download">
        <div class="container">
            <div class="section-title text-white">
                <h2>Pokreni aplikaciju</h2>
                <p>web (desktop) &nbsp;&nbsp;
                    <i class="fas fa-exchange-alt"></i> &nbsp;&nbsp; android (mobile) </p>
            </div>
            <div class="download-btns btn-area text-center">
                <a href="webapp.php" target="_blank">
                    <i class="fab fa-chrome"></i>
                    <i class="fab fa-firefox"></i>
                    <i class="fab fa-opera"></i>
                    web
                </a>
                <a href="https://play.google.com/store/apps/details?id=nsp.mpu.demo" target="_blank">
                    <i class="fab fa-android"></i>
                    android playstore
                </a>
            </div>
        </div>
    </section>
    <!-- APP LOAD area end -->

    <!-- CONTACT area start -->
    <section class="contact-area ptb--100" id="contact">
        <div class="container">
            <div class="section-title">
                <h2>Kontakt</h2>
                <!-- <p>Nemo enim ipsam voluptatem quia voluptas sit</p> -->
            </div>
            <div class="row">
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <style>
                        .contact-area .contact_info span{display: inline-block;}
                    </style>
                    <div class="contact_info">
                        <div class="s-info">
                            <i class="fas fa-user-alt"></i>
                            <div class="meta-content">
                                <span data-toggle="tooltip" title="autor aplikacije">Nikola Pavlović</span>
                            </div>
                        </div>
                        <div class="s-info">
                            <i class="fa fa-map-marker"></i>
                            <div class="meta-content">
                                <span data-toggle="tooltip" title="trenutna lokacija">36000 Kraljevo, Srbija</span>
                            </div>
                        </div>
                        <div class="s-info">
                            <i class="fa fa-mobile"></i>
                            <div class="meta-content">
                                <span data-toggle="tooltip" title="kontakt preko tvitera">+381 64 64 83...</span>
                            </div>
                        </div>
                        <div class="s-info">
                            <i class="fa fa-paper-plane"></i>
                            <div class="meta-content">
                                <a href="https://twitter.com/MPU_app" target="_blank" data-toggle="tooltip" title="checkout mpu @twitter">
                                    <span>twitter</span>
                                </a>
                            </div>
                        </div>
                        <div class="c-social">
                            <ul>
                                <li>
                                    <a href="https://twitter.com/MPU_app" target="_blank" data-toggle="tooltip" title="checkout mpu @twitter">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://mpuapp.wordpress.com/" target="_blank" data-toggle="tooltip" title="more info about mpu">
                                    <i class="fab fa-wordpress"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UCktagbxi4weC7ThBtLDY6cA" target="_blank"  data-toggle="tooltip" title="mpu videos">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://play.google.com/store/apps/details?id=nsp.mpu.demo" target="_blank"  data-toggle="tooltip" title="mpu Andoid app">
                                        <i class="fab fa-google-play"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://rebrand.ly/mpu" target="_blank"  data-toggle="tooltip" title="redirect link">
                                    <i class="fas fa-link"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="twitterFeed" class="col-md-7 col-sm-6 col-xs-12">
                    <a href="https://twitter.com/mpu_app?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @mpu_app</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    <a href="https://twitter.com/intent/tweet?screen_name=mpu_app&ref_src=twsrc%5Etfw" class="twitter-mention-button" data-show-count="false">Tweet to @mpu_app</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    <a class="twitter-timeline" href="https://twitter.com/MPU_app?ref_src=twsrc%5Etfw">Tweets by MPU_app</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div>
        </div>
    </section>
    <!-- CONTACT area end -->
    <!-- footer area start -->
    <footer>
        <div class="footer-area">
            <div class="container">
                <p>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;
                    <script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with
                    <i class="fa fa-heart-o" aria-hidden="true"></i> by
                    <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
            </div>
        </div>
    </footer>
    <!-- footer area end -->

    

    <!-- Scripts -->
    <script src="assets/js/jquery-3.2.0.min.js"></script>
    <script src="js/home_loaderStop.js"></script>
    <script src="libraries/jquery-ui/jquery-ui.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.slicknav.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/magnific-popup.min.js"></script>
    <script src="assets/js/counterup.js"></script>
    <script src="assets/js/jquery.waypoints.min.js"></script>
    <script src="assets/js/jquery.mb.YTPlayer.min.js"></script>
    <script src="assets/js/theme.js"></script>

    <script>
        $(document).ready(function () {
            $('[data-toggle="popover"]').popover();
        });
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

</body>

</html>