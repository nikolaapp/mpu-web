/*
 * @author Nikola Pavlovic
 * sve funkcije potrebne za crtanje
 * njihov redosled je NEbitan
 */
////////////////////////////////////////////////////////////////////
/**
 * sirina div.a gde je smesten SVG  
 * @returns {Number} vraca vrednost u [pikselima]
 */
function getSirina() {
    return document.getElementById('drawing').clientWidth;
}

/**
 * visina div.a gde je smesten SVG  
 * @returns {Number} vraca vrednost u [pikselima]
 */
function getVisina() {
    return document.getElementById('drawing').clientHeight;
}
//////////////////////////////////////////////////////////////

/**
 * funkcija koja iz niza Objekata uzima samo zadati atribut
 * @param {String} atribut koji zelimo da izdvojimo npr. x
 * @param {Object[]} nizObj niz iz kojeg zelimo da izdvojimo odredjen attr 
 * @returns {Array} niz vrednosti apributa npr.x
 */
function izdvojNizIzObjekata(atribut, nizObj) {
    nizAtributa = [];
    for (let i = 0; i < nizObj.length; i++) {
        nizAtributa.push(nizObj[i][atribut]);
    }
    return nizAtributa;
}

/**
 * npr: niz=[1,1.5,2] => f(1.8) = index(1.5) = 1
 * @param {Number} tacka x koord. neke tacke 
 * @param {Number[]} niz x koordinata
 * @returns {Number} najveci index clana niza koji ima vrednost manju od tacke
 */
function indexSusednog_ManjegClanaNiza(tacka, niz) {
    /** vazi ako je niz rastuci*/
    if (tacka <= niz[0]) // van opsega (u minusu)
        return 0;
    else if (tacka >= niz[niz.length - 1])
        // van opsega (u plusu) 
        return niz.length - 2; // ako vraca -1, pravio bi gresku u  [indexStuba + 1]
    else { // u opsegu
        for (let i = 1; i < niz.length; i++) {
            if (niz[i] == tacka)
                return i; // u pitanju je tacka koja se poklapa sa tackom stuba
            else if (niz[i] > tacka)
                return i - 1;
        }
    }
    return g_greskaFlag; // ako je neka greska
}

/**
 * npr: niz=[1,1.5,2] => f(1.3) = index(1.5) = 1
 * @param {Number} tacka x koord. neke tacke 
 * @param {Number[]} niz x koordinata
 * @returns {Number} najmanji index clana niza koji ima vrednost vecu od tacke
 */
function indexSusednog_VecegClanaNiza(tacka, niz) {
    // vazi ako je niz rastuci
    if (tacka <= niz[0])
        return 0;
    else if (tacka >= niz[niz.length - 1])
        return niz.length - 1;
    else {
        for (i = 1; i < niz.length; i++) {
            if (niz[i] >= tacka)
                return i;
        }
    }
    return g_greskaFlag; // ako je neka greska
}

/**
 * Proracun y koord. neke tacke koja se nalazi izmedju dve tacke cije
 * x,y vrednosti znamo
 * @param {Number} tacka_X vrednost koord.X za koju trazimo koord.Y
 * @param {Number[]} niz_X niz x koordinata tacaka
 * @param {Number[]} niz_Y niz y koordinata tacaka
 * @returns {Number} vrednost koord.Y
 */
function linearnaInterpolacija(tacka_X, niz_X, niz_Y) {

    let index0 = indexSusednog_ManjegClanaNiza(tacka_X, niz_X);
    let index1 = indexSusednog_VecegClanaNiza(tacka_X, niz_X);

    if (index0 == g_greskaFlag || index1 == g_greskaFlag)
        return g_greskaFlag;
    else if (index0 == index1) // u pitanju je kota stuba koja se poklapa sa unesenom kotom tla
        return niz_Y[index0];

    let x0 = niz_X[index0];
    let x1 = niz_X[index1];

    let y0 = niz_Y[index0];
    let y1 = niz_Y[index1];

    let x = tacka_X;
    let y; // to se trazi

    y = y0 + (x - x0) * ((y1 - y0) / (x1 - x0));

    return y;
}
/////////////////////////////////////////////////////////////////////
/**
 * Zaokruzivanje float brojeva
 * @param {Number} value float vrednost
 * @param {Number} decimals broj decimala na koji zaoukruzujemo
 * @returns {Number} zaokruzen broj na zadati broj decimala
 */
function roundDef(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

/**
 * Zaokruzivanje float brojeva na 2 dicimale
 * @param {Number} value float vrednost
 * @returns {Number} zaokruzen broj na 2 decimale
 */
function round(value) {
    return roundDef(value, 2);
}
////////////////////////////////////////////////////////////////////
/**
 * Konverzija niza Pointa u niz brojeva potreban za crtanje SVG
 * polilinije
 * @param {Point[]} nizObj niz Point(x,y)
 * @returns {Number[]} niz brojeva [x1 y1 x2 y2]
 */
function konverzija_nizObj_nizSVG(nizObj) {
    nizTacaka_SVG = [];
    for (i = 0; i < nizObj.length; i++) {
        nizTacaka_SVG.push(nizObj[i].x);
        nizTacaka_SVG.push(nizObj[i].y);
    }
    return nizTacaka_SVG;
}
///////////////////////////////////////////////////////////////////

/**
 * proracun razmera i dodeljivanje vrednosti za g_skalarX i g_skalarY
 * @global g_greskaFlag
 * @global g_skalarX
 * @global g_skalarY
 * @returns {void} void
 */
function nadjiRazmere() {
    // provera da li su setovane bar dve tacke u g_nizTacakaTlo
    // a ne proverava za g_nizTacakaTlo_rel (to se kasnije racuna)
    if (!is_setArray(g_nizTacakaTlo, 2))
        return g_greskaFlag;

    let brojTacaka = g_nizTacakaTlo.length;

    let min_X = g_nizTacakaTlo[0].x;
    let max_X = g_nizTacakaTlo[brojTacaka - 1].x;

    //vred. da bi lepo radila petlja
    let min_Y = 9999999;
    let max_Y = -1;

    for (let i = 0; i < g_nizTacakaTlo.length; i++) {
        if (g_nizTacakaTlo[i].y > max_Y)
            max_Y = g_nizTacakaTlo[i].y;
        if (g_nizTacakaTlo[i].y < min_Y)
            min_Y = g_nizTacakaTlo[i].y;
    }
    // minimalna visina zavisi samo od tacaka tla i ona se racuna
    // samo u ovoj funkciji a koristi se pri skaliranju Y atributa
    // svih objekata
    g_minY = min_Y;

    
    // minimalna stacionaza zavisi samo od tacaka tla i ona se racuna
    // samo u ovoj funkciji a koristi se pri skaliranju X atributa
    // svih objekata
    g_minX = min_X;

    // uzimamo u obzir i visinu stuba pri skaliranju Yose
    // treba nam bar JEDAN stub	
    if (is_setArray(g_nizStubova, 1)) {
        let maxVisinaStuba = 0;
        for (let i = 0; i < g_nizStubova.length; i++)
            if (g_nizStubova[i].h > maxVisinaStuba)
                maxVisinaStuba = g_nizStubova[i].h;
        max_Y += maxVisinaStuba;
    }

    // uzimamo u obzir i visinu obj pri skaliranju Yose
    // treba nam bar JEDAN obj	
    if (is_setArray(g_objektiNaTrasi, 1)) {
        for (let i = 0; i < g_objektiNaTrasi.length; i++)
            for (let j = 0; j < g_objektiNaTrasi[i].length; j++)
                if (g_objektiNaTrasi[i][j].y > max_Y)
                    max_Y = g_objektiNaTrasi[i][j].y;
    }

    //debug("min_Y "+min_Y);
    //debug("max_Y "+max_Y);

    /* globalna var	*/
    g_skalarX = 0.95 * getSirina() / (max_X - min_X);

    //ako je tlo ravna linija onda moram da pazim na max_Y-min_Y
    let dY;
    if (max_Y == min_Y)
        dY = 10;
    else
        dY = max_Y - min_Y;

    /* globalna var	*/
    g_skalarY = 0.95 * (getVisina() - 50) / dY;

    //debug("g_skalarX "+g_skalarX);
    //debug("g_skalarY "+g_skalarY);

}

/**
 * Prilagodjavanje atributa niza objekata po razmeri za X i Y
 * @global g_minY
 * @param {Objeci[]} nizObj koji u sebi sadrze .x i .y atribute
 * @returns {Objeci[]} nizObj_rel sa skaliranim vrednostima .x .y .h atributa
 */
function skalirajNizObj(nizObj) {
    if (!nizObj)
        return;

    // kreiramo potpuno nov niz, bez reference na pocentni niz
    let nizObj_rel = JSON.parse(JSON.stringify(nizObj));

    // koristimo vec izracunatu g_minY i g_minX
    // ona je ista i za stubove, tlo, lancanicu...
    for (let i = 0; i < nizObj_rel.length; i++){
        nizObj_rel[i].x -= g_minX;
        nizObj_rel[i].y -= g_minY;
    }
    for (let i = 0; i < nizObj.length; i++) {
        nizObj_rel[i].x = kovnertAtribut_aps_to_rel("x", nizObj_rel[i].x);
        nizObj_rel[i].y = kovnertAtribut_aps_to_rel("y", nizObj_rel[i].y);
    }

    if (nizObj_rel[0]) {

        // proveravamo da li objekti imaju i h atribut
        let flag = (nizObj_rel[0]).hasOwnProperty("h");
        if (flag) {
            for (let i = 0; i < nizObj.length; i++)
                nizObj_rel[i].h = kovnertAtribut_aps_to_rel("h", nizObj[i].h);
            // za visinu stuba ne treba prilagodjavanje		
        }

    }

    return nizObj_rel;
}

/**
 * skalira zadatu vrednost po x koord
 * @global g_skalarX
 * @param {Number} varX u [m]
 * @returns {Number} varX_rel u [pix]
 */
function skaliraj_X(varX) {
    let varX_rel = g_skalarX * varX;
    return varX_rel;
}

/**
 * skalira zadatu vrednost po y koord
 * @global g_skalarY
 * @param {Number} varY u [m]
 * @returns {Number} varY_rel u [pix]
 */
function skaliraj_Y(varY) {
    let varY_rel = g_skalarY * varY;
    return prilagodiYosi(varY_rel);
}

/**
 * skalira zadatu Visini po y koord
 * @global g_skalarY
 * @param {Number} varH u [m]
 * @returns {Number} varH_rel u [pix]
 */
function skaliraj_H(varH) {
    let varH_rel = g_skalarY * varH;
    // za H nam ne traba da prilagodjavamo y osi jer vrednost od h dodajemo
    // na vec prilagodjenu vrednost y
    return varH_rel;
}

/**
 * prilagodjava y koordinatu prikazu u web browseru
 * jer browser racuna y koordinatu odozgo-na-dole
 * @param {Number} varY u [pix]
 * @global g_HEDER_HEIGHT
 * @returns {Number} varY u [pix] prilagodjeno prikazu na stranici
 */
function prilagodiYosi(varY) {
    // 50 je visina hedera
    return getVisina() - varY - g_HEDER_HEIGHT;
}

/**
 * skalira unaza zadatu vrednost po x koord
 * @global g_skalarX
 * @param {Number} varX_rel u [pix]
 * @returns {Number} varX u [m]
 */
function skaliraj_X_unazad(varX_rel) {
    let x = varX_rel / g_skalarX;
    return x;
}

/**
 * skalira unaza zadatu vrednost po y koord
 * @global g_skalarY
 * @param {Number} varY_rel u [pix]
 * @returns {Number} varY u [m]
 */
function skaliraj_Y_unazad(varY_rel) {
    // prvo vracamo unazad (ista f_ja!) prilogodjavanje ekranu
    let y = prilagodiYosi(varY_rel);
    y = y / g_skalarY;
    y += g_minY;
    return y;
}

/**
 * skalira unaza zadatu vrednost Visine po y koord
 * @global g_skalarY
 * @param {Number} varH_rel u [pix]
 * @returns {Number} h u [m]
 */
function skaliraj_H_unazad(varH_rel) {
    // za H nam ne treba prilagodiYosi	
    let h = varH_rel / g_skalarY;
    return h;
}
/////////////////////////////////////////////////////////////////

/**
 * crtanje SVG polyline - koristim za crtanje tacaka tla i lancanice
 * @param {Point[]} nizObj_rel niz Pointa sa relativnim vred. u [pix]
 * @param {String} stroke boja linije
 * @param {String} fill boja kojom se filuje linija (bitno za objekte)
 * @global g_svgObj
 * @returns {SVGPolylineElement} SVG polyline
 * @todo omoguci crtanje markera
 */
function crtaj_poliliniju(nizObj_rel, stroke, fill) {
    if (!nizObj_rel)
        return;
    //***8.8.2018. - problem da upisem markere nad polilinijom
    //	$("svg")[0].append('<defs><marker id="marker_dot" viewBox="0 0 10 10" refX="5" refY="5"  markerWidth="5" markerHeight="5"> <circle cx="5" cy="5" r="5" fill="red" /> </marker> </defs>');
    //	
    //	var mark = g_svgObj.marker(10, 10, function(add) {
    //	  add.rect(10, 10);
    //	});

    //	polyLinija.marker('mid', mark);
    let nizTacaka_SVG = konverzija_nizObj_nizSVG(nizObj_rel);
    let width = 1;
    if (nizObj_rel.length == 2) width = 3; // u pitanju je stari stub ispod DV
    let polyLinija = g_svgObj.polyline(nizTacaka_SVG).fill(fill).stroke({ color: stroke, width: width, linecap: 'round', linejoin: "round" });

    return polyLinija;
}

/**
 * crtanje pojedinacnih objekata
 * @param {Array} niz_rel niz sa tackama kojima se definise obj
 * @global g_nizTacakaTlo_rel
 */
function crtajObjekat(niz_rel) {
    if (!niz_rel)
        return;

    // pomocni niz u koji dodajemo dozemne tacke polilinije    
    let pomNiz = JSON.parse(JSON.stringify(niz_rel));

    // dodajemo  pocetnu i krajnju tacku kako bi spojili kraj objekta sa tlom
    let point_start = [];
    point_start.x = pomNiz[0].x;
    point_start.y = linearnaInterpolacija(point_start.x, izdvojNizIzObjekata("x", g_nizTacakaTlo_rel),
        izdvojNizIzObjekata("y", g_nizTacakaTlo_rel));

    pomNiz.unshift(point_start);  // upisujemo na pocetak niza

    if (niz_rel.length > 1) { // ako je 1 onda je u pitanju stub pa nam ovo ne treba
        let point_end = [];
        point_end.x = pomNiz[pomNiz.length - 1].x;
        point_end.y = linearnaInterpolacija(point_end.x, izdvojNizIzObjekata("x", g_nizTacakaTlo_rel),
            izdvojNizIzObjekata("y", g_nizTacakaTlo_rel));
        pomNiz.push(point_end); // upisujemo na kraj niza       
    }

    // crtamo polilijiju sa bojom za fill
    crtaj_poliliniju(pomNiz,"brown", "black");    
}

/**
 * crtanje SVG linije - stuba
 * @param {StubnoMesto} subnoMesto sa relativnim vred. u [pix]
 * @global g_svgObj, g_nizTacakaTlo_rel
 * @returns {SVGLineElement} SVG linija
 * @todo omoguci crtanje oznake iznad st.mesta
 */
function crtaj_stub(stub) {
    if (!stub)
        return;
    let x = stub.x;
    let y = stub.y;
    let h = stub.h;
    let idTag = "stub-" + stub.oznaka;
	/*let gradient = g_svgObj.gradient('linear', function(stop) {
		stop.at(0, '#333')
		stop.at(1, '#fff')
	});*/
    let linija = g_svgObj
        .line(x, y - h, x, y)
        .stroke({ color: 'black', width: 3, linecap: 'round', opacity: 0.7 })
        .id(idTag)
					/*.fill(gradient)*/;

    $("#" + idTag).html("<title>StubnoMesto:" + stub.oznaka + "<!title>");
    controlStubDrag(stub, linija);
    //$("#"+idTag).click(function(){alert("opaaaaaaaaaaaa");});
    return linija;
}

/**
 * crtanje ordinate - vertikale za prikaz koordinata
 * @global g_svgObj, g_ordinata
 */
function crtaj_Ordinatu() {
    // 2*getVisina da bi bio siguran da zauzima ceo ekran
    g_ordinata = g_svgObj.line(0, 0, 0, 2 * getVisina()).stroke({ color: "black", opacity: 0.6, width: 1 });
}

/**
 * kontrola x,y vrednosti StubnogMesta pri drag and drop - spoljasnja biblioteka
 * @param {StubnoMesto} stubRel-relativno cije koord. kontrolisemo
 * @param {SVGLineElement} linija stuba kojoj deljujemo listenere
 * @global g_nizTacakaTlo_rel
 * @returns {void}
 */
function controlStubDrag(stubRel, linija) {
    let xmax = g_nizTacakaTlo_rel[g_nizTacakaTlo_rel.length - 1].x;
    let xmin = g_nizTacakaTlo_rel[0].x;
    let idTag = "stub-" + stubRel.oznaka;

    // onemogucujemo da se stubRel pomeri van granica zat.polja
    linija.draggable().on('dragmove', function (e) {
        e.preventDefault();
        let x = e.detail.p.x;
        if (x < xmin) {
            x = xmin;
        }
        else if (x > xmax) {
            x = xmax;
        }
        this.move(x, e.detail.p.y);
    });

    // nakon pomeranja radimo update x,y vrednosti stuba
    linija.draggable().on('dragend', function (e) {
        e.preventDefault();
        //let index;
        let x = e.detail.p.x;
        if (x < xmin) {
            x = xmin;
        }
        else if (x > xmax) {
            x = xmax;
        }
        else if (g_snap) {
            //index = indexSusednog_ManjegClanaNiza(x,g_nizTacakaTlo_rel);
            // ne mora za prvu i poslednju tacku
            for (let i = 1; i < g_nizTacakaTlo_rel.length - 1; i++) {
                // da bi lepo pozicioniralo kada su susedne tacke tla dosta blizu 
                if (Math.abs(x - g_nizTacakaTlo_rel[i].x) < g_snap_osetljivost
                    && Math.abs(x - g_nizTacakaTlo_rel[i + 1].x) > g_snap_osetljivost) {
                    x = g_nizTacakaTlo_rel[i].x;
                    break;
                }
            }
        }
        let y = linearnaInterpolacija(x, izdvojNizIzObjekata("x", g_nizTacakaTlo_rel),
            izdvojNizIzObjekata("y", g_nizTacakaTlo_rel));

        // racuna se i ovaj deo - nesto ? zbog spoljasnje biblioteke za drag										
        let duzinaLinijeStuba = $("#" + idTag).attr("y2") - $("#" + idTag).attr("y1");
        this.move(x, y - duzinaLinijeStuba);

        // radiomo update relativnih vred stuba
        updateStubAtribut(stubRel, "x", x);
        updateStubAtribut(stubRel, "y", y);

        // radimo update apsolutnih vred istog stuba
        let x_aps = kovnertAtribut_rel_to_aps("x", x);
        let y_aps = kovnertAtribut_rel_to_aps("y", y);
        let stub_aps = g_nizStubova.find(function (obj) { return obj.oznaka === stubRel.oznaka; });
        updateStubAtribut(stub_aps, "x", x_aps);
        updateStubAtribut(stub_aps, "y", y_aps);

        crtajStuboveNaMapi();
    });

}

/**
 * brise sve elemente nacrtane u g_svgObj
 * @ global g_svgObj
 */
function obrisiCrtanje() {
    //$(g_svgObj).empty(); // ne moze ovako
    $("#drawing svg").empty();
}

/**
 * start/restart crtanja svih svg elemenata
 * @global g_nizTacakaTlo, g_nizStubova, g_nizTacakaLanc
 * @global g_nizTacakaTlo_rel, g_nizStubova_rel, g_nizTacakaLanc_rel
 * @returns {void}
 */
function pokreniCrtanje() {
    obrisiCrtanje();
    nadjiRazmere();

    //min dve tacke
    if (is_setArray(g_nizTacakaTlo, 2)) {
        g_nizTacakaTlo_rel = skalirajNizObj(g_nizTacakaTlo);
        crtaj_poliliniju(g_nizTacakaTlo_rel, 'brown', "none");
        crtaj_Ordinatu();
    }

    //min jedan objekat
    if (is_setArray(g_objektiNaTrasi, 1)) {
        // a unutar objekta moze da bude i samo jedan Point (stub)

        //resetujemo var jer joj dole ne dodeljujem vrednost preko funkcije
        // vec direktno radiomo push u nju
        g_objektiNaTrasi_rel = [];
        let pomNiz = [];
        for (let i = 0; i < g_objektiNaTrasi.length; i++) {
            pomNiz = JSON.parse(JSON.stringify(g_objektiNaTrasi[i]));
            g_objektiNaTrasi_rel.push(skalirajNizObj(pomNiz));
            crtajObjekat(g_objektiNaTrasi_rel[i]);
        }

        //ovo nam treba zbog spanON kada ocitavam x,y koordinate pri mouseover
        kreirajNizStacObj();

    }

    // min jedan stub
    if (is_setArray(g_nizStubova, 1)) {
        //prilagodiStuboveRazmeri();
        g_nizStubova_rel = skalirajNizObj(g_nizStubova);
        nacrtajStubove(g_nizStubova_rel);
    }

    //min dve tacke
    if (is_setArray(g_nizTacakaLanc, 2)) {
        g_nizTacakaLanc_rel = skalirajNizObj(g_nizTacakaLanc);
        crtaj_poliliniju(g_nizTacakaLanc_rel, "blue", "none");
    }
    
    if (is_setTlo()) {
        $("#tableNo1 .col2 input").attr("min",g_nizTacakaTlo[0].x);
        $("#tableNo1 .col2 input").attr("max",g_nizTacakaTlo[g_nizTacakaTlo.length-1].x);
    }
    
}
////////////////////////////////////////////////////////////////
/**
 * Konverzija vrednosti atributa iz rel u aps jedinice
 * @param {String} atribut "x" ili "y" ili "h"
 * @param {Number} varRel vrednost u [px]
 * @returns {Number} vrednost u [m]
 */
function kovnertAtribut_rel_to_aps(atribut, varRel) {
    let varAps;
    switch (atribut) {
        case "x":
            varAps = skaliraj_X_unazad(varRel);
            break;
        case "y":
            varAps = skaliraj_Y_unazad(varRel);
            break;
        case "h":
            varAps = skaliraj_H_unazad(varRel);
            break;
        default:
            varAps = g_greskaFlag;
    }
    // zaokruzivanje koristi samo za prikaz
    //varAps = round(parseFloat(varAps));
    return varAps;
}

/**
 * Konverzija vrednosti atributa iz aps u rel jedinice
 * @param {String} atribut "x" ili "y" ili "h"
 * @param {Number} varRel vrednost u [m]
 * @returns {Number} vrednost u [px]
 */
function kovnertAtribut_aps_to_rel(atribut, varAPS) {
    let varRel;
    switch (atribut) {
        case "x":
            varRel = skaliraj_X(varAPS);
            break;
        case "y":
            varRel = skaliraj_Y(varAPS);
            break;
        case "h":
            varRel = skaliraj_H(varAPS);
            break;
        default:
            varRel = g_greskaFlag;
    }
    //zaoukruzivanje koristi samo za prikaz
    //varRel = round(parseFloat(varRel));
    return varRel;
}

/**
 * Setuje atribut stuba na zadatu vrednost
 * @param {StubnoMesto} stub rel ili abs
 * @param {String} atribut "x","tip"...
 * @param {Number|String} vrednost 125 , "9/1000"
 */
function updateStubAtribut(stub, atribut, vrednost) {
    stub[atribut] = vrednost;
}

/**
 * validacija forme za proracun lancanice
 * zahteva se da bude unesene 
 * -bar dve tace za tlo
 * -bar dva stuba
 * @returns {Boolean} true ako je ovo gore ok, false+alert ako nije
 */
function validirajZahtev_lanc() {
    // zatvara dropdown meni ako je prikazan
    if ($("#lancDropdown").hasClass("show"))
        $("#lancDropdown button:first-of-type").click();

    if (is_setTlo() && is_setStubMesta()) {
        $("#formaLanc input[name='tackeTla']").val(JSON.stringify(g_nizTacakaTlo));
        $("#formaLanc input[name='stubovi']").val(JSON.stringify(g_nizStubova));
        return true;
    }
    else {
        let poruka = "Prethodno morate uneti:";
        if (!is_setTlo())
            poruka += "\n - tacke uzduznog profila trase";
        if (!is_setStubMesta())
            poruka += "\n - podatke o minimum dva stubna mesta";
        alert(poruka);
        return false;
    }
}

/**
 * validacija upita za kreiranje pdf fajla
 * zahteva 
 * -validirajZahtev_lanc
 * -is_setLancanica
 * @returns {Boolean} true ako je ovo gore ok, false+alert ako nije
 */
function validirajZahtev_pdf() {
    // zatvara dropdown meni ako je prikazan
    if (!validirajZahtev_lanc())
        return false;

    else if (!is_setLancanica()) {
        let poruka = "Lancanica mora biti nacrtana!";
        alert(poruka);
        return false;
    }
    else {
        $("#formaLanc input[name='tackeLanc']").val(JSON.stringify(g_nizTacakaLanc));
        return true;
    }
}


/**
 * validacija forme za sessija saveAS - nov proj - SQLinsert
 * @returns {Boolean} true ako je ucitan niz tlo|stub|lanc, false ako nije
 */
function validirajZahtev_sesijaSaveAS() {
    // zatvara dropdown meni ako je prikazan
    if ($("#lancDropdown").hasClass("show"))
        $("#lancDropdown button:first-of-type").click();

    let boolFlag = false;

    if (is_setTlo()) {
        $("#formaLanc input[name='tackeTla']").val(JSON.stringify(g_nizTacakaTlo));
        boolFlag = true;
    }
    if (is_setArray(g_objektiNaTrasi, 1)) {
        $("#formaLanc input[name='objektiTrase']").val(JSON.stringify(g_objektiNaTrasi));
        boolFlag = true;
    }
    if (is_setArray(g_nizStubova, 1)) {
        $("#formaLanc input[name='stubovi']").val(JSON.stringify(g_nizStubova));
        boolFlag = true;
    }
    if (is_setLancanica()) {
        $("#formaLanc input[name='tackeLanc']").val(JSON.stringify(g_nizTacakaLanc));
        boolFlag = true;
    }

    if (!boolFlag) {
        let poruka = "Nema sta da se sacuva";
        alert(poruka);
    }


    return boolFlag;
}


/**
 * validacija forme za sessija save - odnosno SQLupdate
 * @returns {Boolean} true ako je validirajZahtev_sesijaSaveAS() && definisan je projID
 */
function validirajZahtev_sesijaUpdate() {

    /// validira da li je unet niz tla ili niz stub ili niz lanc
    if (!validirajZahtev_sesijaSaveAS())
        return false;

    let projIdPom = $("#formaLanc input[name='projId']").val();
    if (typeof projIdPom == "number")
        return true;
    else if (typeof projIdPom == "string") {
        if (typeof parseInt(projIdPom) == "number")
            return true;
    }
    else
        return false;



    return true;
}

/**
 * Zadati string ("323.4 3423.34") konvertuje u brojeve i upisujemo u g_nizTacakaTlo(x,y)
 * ili u g_objektiNaTrasi
 * @global g_nizTacakaTlo ili g_objektiNaTrasi
 * @param {String} string iz kojeg izvlacimo brojeve
 * @param {Number} maxBrojeva int - broj brojeva koji izvlacimo iz stringa (-1 ako nema ogranicenja)
 */
function getBrojeveIzStringa(string, maxBrojeva) {
    string = string.trim();

    // prazan string
    if (string == "") { return g_greskaFlag; }
    // prvi karakter nije broj, verovatno je u pitanju komentar pa ne vraca gresku
    if (!Number.isInteger(parseInt(string.substring(0, 1)))) { return; }

    let nizStringova = string.split(" "); // delimo na pojedinacne stringove
    let rbStringa = 0;
    let parsiraneVrednosti = [];
    for (let n = 0; n < nizStringova.length; n++) {
        nizStringova[n].trim(); // prvo trim
        if (nizStringova[n] != "") {
            parsiraneVrednosti[rbStringa] = round(parseFloat(nizStringova[n]));
            rbStringa++;
        }
        // ako su nam potrebna samo 2 broja npr.(x,y), onda prekidamo petlju
        if (rbStringa == maxBrojeva)
            break;
    }

    if (maxBrojeva == -1) { // kada ucitavam objekte duz trase tla
        let nizTacaka = [];
        // ovde upisujemo niz tacaka koje se odnose na jedan isti objekat
        for (let i = 0; i < parsiraneVrednosti.length - 1; i = i + 2)
            nizTacaka.push(new Point(parsiraneVrednosti[i], parsiraneVrednosti[i + 1]));

        // taj objekat upisujemo u g_objektiNaTrasi    
        g_objektiNaTrasi.push(nizTacaka);
    }
    else {  // kada ucitavamo tacke tla      
        // provera, ako su ucitana tacno dva broja onda je ok da se napravi nova tacka
        if (parsiraneVrednosti.length == maxBrojeva) {
            g_nizTacakaTlo.push(new Point(parsiraneVrednosti[0], parsiraneVrednosti[1]));
        }
    }

}

/**
 * dodeljuje listener za brisanje reda u dialogu stubova
 * - kada se kreira novi red, kreira se i button, dodeljuje mu se ovaj listener
 */
function buttonEventZaBrisanje() {
    let nizButtona = $("#tableNo1 .obrisiRed");
    for (let i = 0; i < nizButtona.length; i++) {
        $(nizButtona[i]).click(function () {
            if ($("#tableNo1 .obrisiRed").length < 2)
                return;// onemoguceno brisanje ako imamo samo 1 red
            else
                $(nizButtona[i]).closest("tr").remove();
        });
    }

}

/**
 * funkcija za ucitavanje stubnih mesta iz dialoga
 * - na kraju pokrece pokreniCrtanje()
 * @global g_nizStubova
 */
function ucitajStuboveDialog() {
    g_nizStubova = [];
    let sviRedovi = $("#tableNo1 tbody tr");
    let sviInputiRed;
    let bool_flag=true;
    for (let i = 0; i < sviRedovi.length; i++) {
        sviInputiRed = $(sviRedovi[i]).find("input");
        let oznaka = $(sviInputiRed[0]).val();
        let x = parseFloat($(sviInputiRed[1]).val());
        if (x< g_nizTacakaTlo[0].x || x>g_nizTacakaTlo[g_nizTacakaTlo.length-1].x) {
            //stacionaza stuba nije ispravna
            alert("Za stubno mesto "+oznaka+" je uneta ne odgovaraujca stacionaza - stubno mesto je zanemareno! ");
            bool_flag=false;
            continue;
        }
        if (i!=0) {
            let inputX = $(sviRedovi[i-1]).find("input")[1];//x stacionaza
            if ( x < parseFloat(inputX.value) ){
                alert("Za stubno mesto "+oznaka+" je uneta manja stacionaza od stac. prethodnog stuba - stubno mesto je zanemareno! ");
                bool_flag=false;
                $(sviInputiRed[1]).focus();
                continue;
            }            
        }
        let h = parseFloat($(sviInputiRed[2]).val());
        if (h<1 || h>50) {
            //visina vesanja prov nije ispravna
            alert("Za stubno mesto "+oznaka+" je uneta ne odgovaraujca visina vesanja provodnika [min 1m - max50m] - stubno mesto je zanemareno!");
            bool_flag=false;
            continue;
        }
        let tip = $(sviInputiRed[3]).val();
        let konzola = $(sviInputiRed[4]).val();
        let y = linearnaInterpolacija(x, izdvojNizIzObjekata("x", g_nizTacakaTlo),
            izdvojNizIzObjekata("y", g_nizTacakaTlo));
        g_nizStubova.push(new StubnoMesto(oznaka, x, y, h, tip, konzola));
    }
    
    crtajStuboveNaMapi();

    pokreniCrtanje();

    return bool_flag;
}

/**
 * crta sve stubove iz zadatog niza
 * @param {StubnoMesto} nizStubovaRel 
 */
function nacrtajStubove(nizStubovaRel) {
    if (!nizStubovaRel)
        return;

    for (let i = 0; i < nizStubovaRel.length; i++) {
        crtaj_stub(nizStubovaRel[i]);
    }
}
//////////////////////////////////////////////////////////////
/**
 * niz animacija za dialog
 */
const nizAnimacijaDialoga = ["blind",/*"bounce",*/"clip", "drop", "blind", "explode", "fade", "fold", "highlight", "puff",/*"pulsate",*/"scale", "shake", "size", "slide"/*,"transfer"*/];

/**
 * Vraca random index iz niza
 * @param {Array} niz
 * @returns {Number} int index niza
 */
function getRandIndexFromNiz(niz) {
    randN = Math.floor(Math.random() * niz.length);
    return niz[randN];
}

////////////////////////////////////////////////////////////////
/**
 * from niz Object -> to niz Point  -- za lancanicu
 * @todo validairaj
 * @param {Object[]} nizObj sa atriburima x, y
 * @returns {Point[]} niz Pointa (x,y)
 */
function kreirajNizPointaLancanice(nizObj) {
    if (!nizObj || nizObj.length < 2) {
        return; /* todo dodaj kontrolni return */
    }

    let nizPointa = [];
    nizObj.forEach(function (element) {
        nizPointa.push(new Point(element.x, element.y));
    });
    return nizPointa;
}

////////////////////////////////////////////////////////////////
/**
 * translacija ordinate na novu x koordinatu
 * @param {Number} x_aps u [m]
 * @global g_ordinata
 */
function translirajOrdinatu(x_aps) {
    let x_rel = kovnertAtribut_aps_to_rel("x", x_aps);
    if ($("#" + g_ordinata.id()).css("display") !== "none") {
        g_ordinata.translate(x_rel, 0);
    }
}


/**
 * na osnovu x koord vrsi upis y,h u futer
 * @param {Number} xaps x koord u [m]
 * @global g_nizTacakaTlo
 */
function upisiYHfooter(xaps) {
    // upisivanje koord u futer
    let yaps;
    let index = tackaPripadaObjTrase(xaps);

    if (index !== false) // visina objekta na trasi
        yaps = linearnaInterpolacija(xaps, izdvojNizIzObjekata("x", g_objektiNaTrasi[index]),
            izdvojNizIzObjekata("y", g_objektiNaTrasi[index]));
    else // visina tla
        yaps = linearnaInterpolacija(xaps, izdvojNizIzObjekata("x", g_nizTacakaTlo),
            izdvojNizIzObjekata("y", g_nizTacakaTlo));

    $("#y_koor").html(round(yaps));

    if (is_setLancanica()) {
        if (xaps >= g_nizTacakaLanc[0].x && xaps <= g_nizTacakaLanc[g_nizTacakaLanc.length - 1].x) {
            haps = linearnaInterpolacija(xaps, izdvojNizIzObjekata("x", g_nizTacakaLanc),
                izdvojNizIzObjekata("y", g_nizTacakaLanc));
            if (haps - yaps < 0)
                $("#h_koor").html("-1"); //vise kao neki flag              
            else
                $("#h_koor").html(round(haps - yaps));
        }
        else {
            $("#h_koor").html("-"); //vise kao neki flag 
        }

    }
}
////////////////////////////////////////////////////////////////
/**
 * Provereva da li je definisan niz i da li je duzina >= brojElemenata
 * @param {Array} niz 
 * @param {Number} brojElemenata koji treba niz da ima kako bi vratili true
 * @returns {Boolean} 
 */
function is_setArray(niz, brojElemenata) {
    if (typeof niz !== 'undefined' && niz instanceof Array) {
        // minimum dva clana niza
        if (niz.length >= brojElemenata) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/**
 * Provereva da li su definisane bar 2 tacke tla
 * @global g_nizTacakaTlo,g_nizTacakaTlo_rel
 * @returns {Boolean} 
 */
function is_setTlo() {
    return is_setArray(g_nizTacakaTlo, 2) && is_setArray(g_nizTacakaTlo_rel, 2);
}

/**
 * Provereva da li su definisana bar 2 st.mesta
 * @global g_nizStubova,g_nizStubova_rel
 * @returns {Boolean} 
 */
function is_setStubMesta() {
    return is_setArray(g_nizStubova, 2) && is_setArray(g_nizStubova_rel, 2);
}

/**
 * Provereva da li su definisne bar 2 tacke lanc
 * @global g_nizTacakaLanc,g_nizTacakaLanc_rel
 * @returns {Boolean} 
 */
function is_setLancanica() {
    return is_setArray(g_nizTacakaLanc, 2) && is_setArray(g_nizTacakaLanc_rel, 2);
}
///////////////////////////////////////////////////////////////////////
/**
 * resetuje nizove i brise vrednosti u input[type='hidden']
 * @global g_nizTacakaTlo, g_nizStubova, g_nizTacakaLanc, g_nizTacakaTlo_rel, g_nizStubova_rel, g_nizTacakaLanc_rel = [];
 */
function resetGlobNizova() {
    g_nizTacakaTlo = [];
    g_nizStubova = [];
    g_nizTacakaLanc = [];
    g_nizTacakaTlo_rel = [];
    g_nizStubova_rel = [];
    g_nizTacakaLanc_rel = [];
    g_objektiNaTrasi = [];
    g_objektiNaTrasi_rel = [];
    g_nizStacObj = [];

    // resetujemo sve podatke u hidden inputima
    /* debug todo - imam problem posle sa save proj
    $("input[type='hidden']").each(function () {
        $(this).val("");
    }); */

    // resetujemo input za obj.ispod DV da bi mogli da ponovo
    // odaberemo isti fajl
    $("#file_obj").val("");
}


/**
 * Funkcija vrsi submit podataka ka serveru - u zavisnosti od selektora
 * @param {String} selector updateProject, saveAsProject, getLancanica
 */
function submitData(selector) {

    let url_controler; // url do kontrolera
    let action_controler; // ackija koja se od kontrolera trazi

    // save - odnosno update postojeceg projekta
    if (selector === 'updateProject') {
        if (!validirajZahtev_sesijaUpdate())
            return;
        else {
            url_controler = "../control/Controller_projekti.php";
            action_controler = "update";
        }
    }
    // save novog projekta   
    else if (selector === 'saveAsProject') {
        if (!validirajZahtev_sesijaSaveAS())
            return;
        else {
            url_controler = "../control/Controller_projekti.php";
            action_controler = "save";
        }
    }
    // crtanje lancanice 
    else if (selector === 'getLancanica') {
        if (!validirajZahtev_lanc())
            return;
        else {
            url_controler = "../control/Controller_js.php";
            action_controler = "getLancanica";
        }
    }
    // crtanje lancanice 
    else if (selector === 'getPdf') {
        if (!validirajZahtev_pdf()) // validacija za pdf
            return;
        else {
            url_controler = "../control/Controller_alati.php";
            action_controler = "getPdf";            
        }
    }
    else {
        return;
    }

    // mora da sadrzi ime kako bi bio kreiran pdf
    let kontrolaNazivaProj = $("#formaLanc input[name='projName']").val();
    if ( ! kontrolaNazivaProj) {
        $("#formaLanc input[name='projName']").val("uzduzni profil");
    }
    
    // podaci koje saljemo serveru
    let submitData = $("#formaLanc").serialize() + "&action=" + action_controler;

    
    if (selector === 'getPdf') { 
        // u pitanju je zahtev za download pdf
        downloadFile_byPost(url_controler,submitData,"uzduzni profil.pdf");
    }
    else{ 
        // svi ostali zaxtevi koriste jquey a j a x
        $.ajax({ /* save/updade proj i  getLancanica */
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: $("#formaLanc").serialize() + "&action=" + action_controler,
            success: function (data) {
                if (data.status != "success") {                    
                    alert(data.status + " - " + data.msg);
                    return;
                }

                if (selector === 'getLancanica') {
                    g_nizTacakaLanc = kreirajNizPointaLancanice(data.data);
                    $("#formaLanc input[name='jsID']").val(data.msg); 

                    obrisiSveLejere();
                    pokreniCrtanje();
                    alert("Uspesno kreirana lancanica");
                    
                }
                else if (selector === 'updateProject') {                    
                    alert("uspesno sacuvane izmene u projektu");

                }
                else if (selector === 'saveAsProject') {                    
                    alert("uspesno sacuvan nov projekat");

                }
            } // end  su cce s
        }); // end a j a x
    }

}

/**
 * validacija naziva projekta - text input u modalu
 */
function validirajZahtev_projName() {
    let text = $('#modalTextInput input[name="projName"]').val();
    if (typeof text !== "string")
        return false;
    else if (text.length < 1)
        return false;
    else {
        g_projName = text;
        // upisujemo u hidden input kako bi mogli da sacuvamo proj		
        $("#formaLanc input[name='projName']").val(g_projName);
        /* $("#formaLanc input[name='projId']").val(?????); */
        return true;
    }

}

/**
 * Brisanje podataka o projektu unutar dialoga 
 */
function deleteDialogInfo(){
    $(".dialogProjData").each(function() {
        $(this).html( "-" );
      });
}

/**
 * Ucitava projekat koji smo odabrali preko openProj dialoga
 * @param {Object} projData 
 */
function loadSelectedProj(projData) {
    try {
        $("#formaLanc select[name='provodnik']").val(projData.oznakaProv);
        $("#formaLanc input[name='temperatura']").val(projData.temperatura);
        $("#formaLanc select[name='odo']").val(projData.odo);
        $("#formaLanc input[name='naprezanje']").val(projData.naprezanje);

        g_nizTacakaTlo = JSON.parse(projData.tackeTlaJson);
        $("#formaLanc input[name='tackeTla']").val(JSON.stringify(g_nizTacakaTlo));
        g_objektiNaTrasi = JSON.parse(projData.objektiTraseJson);
        $("#formaLanc input[name='objektiTrase']").val(JSON.stringify(g_objektiNaTrasi));
        g_nizStubova = JSON.parse(projData.stubnaMestaJson);
        $("#formaLanc input[name='stubovi']").val(JSON.stringify(g_nizStubova));
        g_nizTacakaLanc = JSON.parse(projData.tackeLancJson);
        $("#formaLanc input[name='tackeLanc']").val(JSON.stringify(g_nizTacakaLanc));

        $("#formaLanc input[name='projName']").val(projData.naziv);
        g_projName = projData.naziv;

        $("#formaLanc input[name='projId']").val(projData.id);
        $("#formaLanc input[name='jsID']").val(projData.jsID);

        $("input[type='file']").val("");

        $("#tableNo1 .col2 input").each(function () {
            $(this).attr("min",g_nizTacakaTlo[0].x);
            $(this).attr("max",g_nizTacakaTlo[g_nizTacakaTlo.length-1].x);
        });
    } catch (error) {
        alert("Odabrani projekat ne moze da se ucita");
        return;
    }

    obrisiSveLejere();
    pokreniCrtanje();
}

/**
 * vraca false ili r.b objekta kojem pripada stacionaza x_aps
 * @param {Number} x_aps x koordinata u [m]
 * @returns {false|Number}
 */
function tackaPripadaObjTrase(x_aps) {
    if (!g_objektiNaTrasi) return false;

    let index = false;
    for (let i = 0; i < g_objektiNaTrasi.length; i++)
        if (x_aps >= g_objektiNaTrasi[i][0].x &&
            x_aps <= g_objektiNaTrasi[i][g_objektiNaTrasi[i].length - 1].x) {
            index = i;
            break;
        }

    return index;
}

/**
 * proracun i upis razdaljine dve latlong tacke u modalLatLong dialog
 */
function upisiRazdaljivuDialog() {
    let latA = $("input[name='latFirst']").val();
    let longA = $("input[name='longFirst']").val();  
    let latB = $("input[name='latLast']").val();
    let longB = $("input[name='longLast']").val(); 

    if (!latA || !longA || !latB  || !longB ) 
    return;

    let distance =  distanceLatLong([ latA,longA],[ latB,longB]);
    if (distance >= 3000) {
        distance = (distance/1000).toFixed(2) +' km';
    } else {
        distance = distance.toFixed(2) + ' m';
    }

    $('#modalLatLong strong').html(distance);
}

/**
 * vraca niz raspona
 * @global g_nizStubova
 * @returns float[]
 */
function getNizRaspona(){
    if (!is_setStubMesta()) {
        alert("moraju biti uneta bar dva stubna mesta");
        return false;
    }
    let nizRaspona = [];
    for (let i = 1; i < g_nizStubova.length; i++) {
        nizRaspona.push(round(g_nizStubova[i].x - g_nizStubova[i-1].x));
    }
    return nizRaspona;
}


/**
 * na osnovu karaktera u stringu radi toggle button-a u dialogu provodnika
 * @param {string} string c,e,d,s,o 
 */
function toggleProvButtons(string) {
    $("#copyProv").hide();
    $("#editProv").hide();
    $("#saveProv").hide();
    $("#deleteProv").hide();
    $("#cancelProv").hide();

    if (!string) return;

    let nizChar = string.split('');

    nizChar.forEach(char => {
        if (char.toLowerCase() == 's') $("#saveProv").show();
        else if (char.toLowerCase() == 'c') $("#copyProv").show();
        else if (char.toLowerCase() == 'e') $("#editProv").show();
        else if (char.toLowerCase() == 'd') $("#deleteProv").show();
        else if (char.toLowerCase() == 'o') $("#cancelProv").show();
    });
}

/**
 * funkcija koju koristimo pri a j a x error callback
 * @param {String} string poruka za consolu
 */
function ajax_error(string) {    
    alert("Neuspesna obrada zahteva - pokusajte opet kasnije!" );
    /*******todo izbrisi ovo ispod na serveru**********/
    //console.log(string);
    //console.error(string);
    console.warn(string);
    //console.debug(string);
    //console.trace(string);  
    /***************************************************/  
}

/**
 * funkcija koju koristimo pri a j a x beforeSend 
 * - pokretanje loadera
 */
function ajax_beforeSend() { 
    $('#loader-wrapper .loader-section').css("background", "transparent");
    $('body').removeClass('loaded');  
}

/**
 * funkcija koju koristimo pri a j a x beforeSend 
 * - stopiranje loadera
 */
function ajax_complete() { 
    $('body').addClass('loaded'); 
}

/**
* formiranje g_nizStacObj na osnnovu g_objektiNaTrasi
* @global g_objektiNaTrasi
* @global g_nizStacObj
* @returns {void} void
*/
function kreirajNizStacObj() { 
    if (! is_setArray(g_objektiNaTrasi, 1)) {
        g_nizStacObj = [];
    }
    for (let i = 0; i < g_objektiNaTrasi.length; i++) 
        for (let j = 0; j < g_objektiNaTrasi[i].length; j++) 
            g_nizStacObj.push(g_objektiNaTrasi[i][j].x);
}

/**
 * XMLHttpRequest POST zahtev za download fajla sa servera
 * @param {String} controler koji obradjuje zahter
 * @param {String} podaci podaci koje saljemo
 * @param {String} defaultFilename naziv sacuvanog fajla ukoliko server nije poslao naziv
 */
function downloadFile_byPost(controler, podaci, defaultFilename) {
    let req = new XMLHttpRequest();
        req.open("POST", controler, true);
        req.responseType = "blob";
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send(podaci);
        
        req.onload = function (event) {
            //var newBlob = new Blob([req.response], {type: "application/pdf"})
            let newBlob = req.response;

            let myReader = new FileReader();
            let data;
            myReader.onload = function(event){
                try {
                    data = JSON.parse(myReader.result);
                } catch (error) {
                    // ako ne moze da parsira onda je pdf kreiran i poslat
                    // ovde medjutim ne znamo da li je pdf ispravno kreiran ili
                    // je mozda samo download-ovan a ne moze da se otvori
                    data= [];
                } finally{
                    if (data.status) // ne postoji ako je u pitanju pdf
                        if (data.status != "success") // u pitanju je greska
                            alert(data.status);                    
                }

            };
            myReader.readAsText(newBlob);
            

            // Create a link pointing to the ObjectURL containing the blob.
            const dataUrl = window.URL.createObjectURL(newBlob);
            let link = document.createElement('a');
            link.style = "display: none";  
            link.href = dataUrl;

            // ime izlaznog fajla
            let filename = "";
            let disposition = req.getResponseHeader('Content-Disposition');
            if (disposition) { // trazimo ime definisano na serveru
                let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                let matches = filenameRegex.exec(disposition);
                if (matches !== null && matches[1])
                    filename = matches[1].replace(/['"]/g, '');                     
            }
            if (! filename) // ako ime nije definisano na serveru
                filename = defaultFilename;            

            link.download = filename;
            document.body.appendChild(link);
            link.click();
            setTimeout(function () {
                // For Firefox it is necessary to delay revoking the ObjectURL
                document.body.removeChild(link);
                window.URL.revokeObjectURL(dataUrl);
            }, 100);
        }
    
}