/*
 * @author Nikola Pavlovic
 * globalne promenljive
 * sve glob.promenljive imaju prefiks g_
 */

//koristim let da bi onemogucio ponovnu deklaraciju istih varijabli
//mada je to moguce unutar funkcije

/**
 * visina hedera u [pix]
 * @global
 * @constant
 * @type {Number}
 */
const g_HEDER_HEIGHT = 50;

/**
 * @global svg kontejener u kome se nalaze svi svg
 * elementi koje crtam (linije, tacke,...)
 * @type {SVG}
 */
let g_svgObj;

/**
 * @global niz (x,y) tacaka tla u [m]
 * @type {Array of Point}
 */
let g_nizTacakaTlo = [];

/**
 * @global niz (x,y) tacaka tla u [pikselima]
 * @type {Array of Point}
 */
let g_nizTacakaTlo_rel = [];
/**
 * @global niz stubova sa atributima u [m]
 * @type {Array of StubnoMesto}
 */
let g_nizStubova = [];

/**
 * @global niz stubova sa atributima u [pikselima]
 * @type {Array of StubnoMesto}
 */
let g_nizStubova_rel = [];

/**
 * @global niz (x,y) tacaka tla u [m]
 * @type {Array of Point}
 */
let g_nizTacakaLanc = [];

/**
 * @global niz (x,y) tacaka tla u [pikselima]
 * @type {Array of Point}
 */
let g_nizTacakaLanc_rel = [];

/**
 * @global vrednost za skaliranje (apsol->rel) po x osi
 * @type {Number}
 */
let g_skalarX;

/**
 * @global vrednost za skaliranje (apsol->rel) po y osi
 * @type {Number}
 */
let g_skalarY;

/**
 * const za vracanje greske
 * @constant 
 * @global 
 * @type {Number}
 */
const g_greskaFlag = -9999919;


/**
 * @global najniza kota tla
 * @type {Number}
 */
let g_minY;

/**
 * @global najniza stacionaza tla
 * @type {Number}
 */
let g_minX;

/**
 * @global kontrola pan i zoom - ekstena biblioteka
 * @type {SVGZoomAndPan}
 */
let g_svgPanZoom;

/**
 * @global mapa - ekstena biblioteka 
 * @type {ol.Map}
 */
let g_map;

/**
 * @classdesc Klasa Pointa - tacka(x,y)
 * @param {Number} x 
 * @param {Number} y 
 */
function Point(x, y) {
  this.x = x;
  this.y = y;
}

/**
 * @classdesc Klasa stubnih mesta
 * @param {String} oznaka - oznaka (r.b.) stubnog mesta
 * @param {Number} x - x koor
 * @param {Number} y - y koor 
 * @param {Number} h - visina vesanja provodnika 
 * @param {String} tip - tip stuba 
 * @param {String} konzola - tip konzole 
 */
function StubnoMesto(oznaka, x, y, h, tip, konzola) {
	this.oznaka = oznaka;
    this.x = x;
    this.y = y;
    this.h = h;
    this.tip = tip;
    this.konzola = konzola;
}

/**
 * prikaz ordinate - svg linija
 * @type {SVGLineElement}
 */
let g_ordinata;


/**
 * snap toggle za pomeranje stubova - kada se priblizi tacki tla 
 * stub dobije njenu x koord
 * @type {Boolean}
 */
let g_snap = false;
let g_snap_osetljivost = 3;

/**
 * sluzi nam kao selektor za formu - lancanica ili save sesion
 * @type {String}
 */
let g_submitButton;

/**
 * naziv projekta
 * @type {String}
 */
let g_projName;


/**
 * id projekta
 * @type {Number} int
 */
/* let g_projId; */

/**
 * podaci selektovanog projekta u openProj dialogu
 * @type {Object}
 */
let g_selectedProjData;


/**
 * podaci o objektima na trasi
 * @type {Object[]}
 */
let g_objektiNaTrasi;
let g_objektiNaTrasi_rel;


/**
 * flag za selectovanje tacke na mapi
 * @type {Boolean}
 */
let g_pickPoint = false;


/**
 * pocetna tacka trase sa koord lat, long
 * @type {Array [lat,long]}
 */
let g_latlongA=[];

/**
 * krajnja tacka trase sa koord lat, long
 * @type {Array [lat,long]}
 */
let g_latlongB=[];

/**
 * odabir elevacije ili ne prilikom submita latlong dialoga 
 * @type {boolean}
 */
let g_boolFlag_elevacija=false;

/**
 * Niz stacionaza svi objekata ispod trase dv
 * - kreira se na osnovu g_objektiNaTrasi
 * - koristi ga mousechange pri snapON 
 * @type {Array}
 */
let g_nizStacObj=[];