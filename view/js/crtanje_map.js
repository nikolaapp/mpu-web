
//openlayer mapa ima neki cudan koordinatni sistem - ovim ga prilagodjavam
/**
 * lat, long  sis -> opellayer sis
 * @param {Array} latLong[lat_long]
 * @returns webMercator_koor
 */
function prilagodiKoorSis_tacka(latLong) {
    let Long_Lat_koor = [];
    Long_Lat_koor.push(latLong[1]);
    Long_Lat_koor.push(latLong[0]);
    webMercator_koor = ol.proj.fromLonLat(Long_Lat_koor);
    return webMercator_koor;
}

/**
 * prolagodjava niz tacaka
 * @param {Point[]} latLongNiz 
 */
function prilagodiKoorSis_niz(latLongNiz) {
    let transformisanNiz = [];
    for (i = 0; i < latLongNiz.length; i++) {
        transformisanNiz.push(prilagodiKoorSis_tacka(
            [latLongNiz[i][0], latLongNiz[i][1]]
        ));
    }
    return transformisanNiz;
}

// kreiranje vektora tacaka na osnovu koga se crta lejer tacaka
function kreirajVektor_tacaka(nizTacaka) {
    let vektorTacaka = new ol.source.Vector({});
    for (i = 0; i < nizTacaka.length; i++) {
        let geometrijskaTacka = new ol.geom.Point(nizTacaka[i]);
        let oblikTacke = new ol.Feature({ geometry: geometrijskaTacka });
        vektorTacaka.addFeature(oblikTacke);
    }
    return vektorTacaka;
}
// kreiranje vektora linija na osnovu koga se crta lejer linija
function kreirajVektor_linije(nizTacaka) {
    let vectorLinije = new ol.source.Vector({});
    let geometrijskaLinija = new ol.geom.LineString(nizTacaka);
    let oblikLinije = new ol.Feature({
        name: "linija",
        geometry: geometrijskaLinija
    });
    vectorLinije.addFeature(oblikLinije);
    return vectorLinije;
}
/**
 * Rotira mapu za zadati ugao u rad
 * @param {Number} radiani 
 */
function rotirajMapu(radiani) {
    let mapView = g_map.getView();
    //get the current radians of the g_map's angle
    let currentRadians = g_map.getView().getRotation();
    //add 1.5 radians to the map's current getRotation() value
    mapView.setRotation(/*currentRadians + */radiani);
}
/**
 * Dodaje novi lejer na mapu
 * @param {ol.layer} lejer 
 */
function dodajLejerUMapu(lejer) {
    g_map.addLayer(lejer);
}

/**
 * Brise postojeci lejer sa mape na osnovu imena lejera
 * @param {String} name - naziv lejera koji brisemo 
 */
function obrisiLejerIzMape(name) {
    let layersToRemove = [];
    g_map.getLayers().forEach(function (lejer) {
        if (lejer.get('name') != undefined && lejer.get('name') === name) {
            layersToRemove.push(lejer);
        }
    });

    for (let i = 0; i < layersToRemove.length; i++) {
        g_map.removeLayer(layersToRemove[i]);
    }
}
/**
 * brise lejere iz mape:
 * -stacionaze
 * -trasa
 * -stub
 * -dv
 */
function obrisiSveLejere(){    
    obrisiLejerIzMape("stacionaze");
    obrisiLejerIzMape("trasa");
    obrisiLejerIzMape("stub");
    obrisiLejerIzMape("dv");
}
/**
 * kreiranje novog lejera za mapu
 * @param {ol.vector} vektor vektor tacaka lejera
 * @param {ol.style} stil stil linija
 * @param {string} name ime novog lejera
 * @returns {ol.layer} 
 */
function kreirajLejer(vektor, stil, name, zInd) {
    let lejer = new ol.layer.Vector({
        visible: true,
        source: vektor,
        style: stil
    });
    lejer.set('name', name);
    lejer.setZIndex(zInd)
    return lejer;
}

/**
 * Animacija zumiranja openlayer mape za zadatu lokaciju
 * - skinuto sa ol sajta
 */
function flyTo(location, done) {
    var duration = 2000;
    var zoom = g_map.getView().getZoom();
    var parts = 2;
    var called = false;
    function callback(complete) {
        --parts;
        if (called) {
            return;
        }
        if (parts === 0 || !complete) {
            called = true;
            done(complete);
        }
    }
    g_map.getView().animate({
        center: location,
        duration: duration
    }, callback);
    g_map.getView().animate({
        zoom: zoom - 2,
        duration: duration / 2
    }, {
            zoom: zoom,
            duration: duration / 2
        }, callback);
}

/**
 * racuna lat/long vrednost u radiajanima
 * @param {Number} degrees like 43.5458
 * @returns {Number} radians
 */
function deg_to_rad(degrees) {return degrees * (Math.PI / 180);}
/**
 * Racuna ugao izmedju dve latlong tacke za koji treba da rotiram mapu
 * da bi tacke prikazao u horizontali
 * @param {Array latlong} pointA 
 * @param {Array latlong} pointB 
 * @returns {Number} angle [rad]
 */
function angleFromCoordinate(pointA, pointB) {
    // projekcija tacke B na horizontalu tacke A
    let pointB0 = [pointB[0], pointA[1]];
    
    let c = distanceLatLong(pointA, pointB);//hipotenuza
    let a = distanceLatLong(pointA, pointB0);//krak od tacke a
    
    let radians = Math.acos(a/c); //ugao u radianima
    if ( isNaN(radians) ) return 0; // ako je c = 0, odnoso tacke A i B su iste

    let outVal = Math.PI/2 - radians; //fitovanje rezultata

    // u zavisnosti u kojem kavadrantu je krajnja tacka
    // XOR - tacno je ili samo I ili samo II
    if (pointA[0] > pointB[0] ? !(pointA[1] > pointB[1]) : (pointA[1] > pointB[1]) )
        outVal = - outVal; // alfa = - alfa
    

    return outVal;
}

// Earth radius at sea level
const ERe = 6378137.0; // [m] - equator
const ERp = 6356752.314; // [m] - pole
/**
 * get earth radius at given latitude
 * @param {Number} lat [deg]
 * @private dodatak 500m zbog visine
 * @see https://rechneronline.de/earth-radius/
 */
function get_radius_at_lat(lat) {
    lat = deg_to_rad(lat);
    let f1 = Math.pow((Math.pow(ERe, 2) * Math.cos(lat)), 2);
    let f2 = Math.pow((Math.pow(ERp, 2) * Math.sin(lat)), 2);
    let f3 = Math.pow((ERe * Math.cos(lat)), 2);
    let f4 = Math.pow((ERp * Math.sin(lat)), 2);

    //dodajem jos 500m nadmorske visine - neka srednja vrednost
    //??? ne utice mnogo na rezultate kasnije
    return Math.sqrt((f1 + f2) / (f3 + f4)) + 500;
}


/**
 * rastojanje izmedju dve latlong tacke u m
 * @param {Array latlong} pointA 
 * @param {Array latlong} pointB 
 * @returns {Number} distance [m]
 */
function distanceLatLong(pointA, pointB) {
    let lat1 = deg_to_rad(pointA[0]);
    let lat2 = deg_to_rad(pointB[0]);
    let dLat = (lat2 - lat1);

    let long1 = deg_to_rad(pointA[1]);
    let long2 = deg_to_rad(pointB[1]);    
    let dLong = (long2 - long1);

    
    //let R = 0.5 * (get_radius_at_lat(pointA[0]) + get_radius_at_lat(pointB[0]) ); // [m]
    let R = get_radius_at_lat(parseFloat(pointA[0]) + parseFloat(pointA[0] - pointB[0])); // [m]

    /**
     * Haversine Formula
     * @link https://andrew.hedges.name/experiments/haversine/
     */
    let coefA = Math.pow(Math.sin(dLat/2), 2)  +
            Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dLong / 2), 2);    
    let coefC = 2 * Math.atan2(Math.sqrt(coefA), Math.sqrt(1 - coefA));
    let distance = R * coefC;

  /*   
    console.log("lat1="+pointA[0]+" long1="+pointA[1]);
    console.log("lat2="+pointB[0]+" long2="+pointB[1]);
    console.log("distance = "+distance);
 */
    return distance;
}


/**
 * centriramo mapu na osnovu pocente i krajnje tacke
 * rotiramo u horizontalnu projekciju
 * crtamo lejera tacaka izmedju
 * @param {Array latlong} firstPoint 
 * @param {Array latlong} lastPoint 
 */
function centrirajMapu(firstPoint, lastPoint) {
    let lat = (firstPoint[0] + lastPoint[0]) / 2;
    let long = (firstPoint[1] + lastPoint[1]) / 2;

    if (lat === 0 && long ===0) {
        alert("Koordinate su indenticne!?");
        return;
    }

    let map_nizTacaka = [];
    map_nizTacaka.push(firstPoint, lastPoint);
    let map_nizTacaka_transformed = prilagodiKoorSis_niz(map_nizTacaka);

    let vektor_TacakaTrasa = kreirajVektor_tacaka(map_nizTacaka_transformed);
    let vektor_LinijeTrase = kreirajVektor_linije(map_nizTacaka_transformed);

    let lejer_TackeTrasa = kreirajLejer(vektor_TacakaTrasa, stilX, "stacionaze",25);
    let lejer_LinijaTrasa = kreirajLejer(vektor_LinijeTrase, stilTrase, "trasa",21);
    
    obrisiLejerIzMape("stacionaze");
    obrisiLejerIzMape("trasa");

    dodajLejerUMapu(lejer_TackeTrasa);
    dodajLejerUMapu(lejer_LinijaTrasa);

    crtajStuboveNaMapi();

    let ugao = angleFromCoordinate(firstPoint, lastPoint);

    flyTo(prilagodiKoorSis_tacka(
        [lat, long]),
        function () {
            let extent = lejer_TackeTrasa.getSource().getExtent();
            //g_map.getView().fit(extent, g_map.getSize());
            //let fitSize = g_map.getSize()[0];
            const elem = document.querySelector('svg:first-of-type polyline:first-of-type');
            let fitX = elem.getBoundingClientRect().width;
            //fitY = elem.getBoundingClientRect().height;
            g_map.getView().fit(extent, {size: [fitX,fitX], constrainResolution: false, padding: [0,25,0,-10]});
            rotirajMapu(ugao);
        }
    );
}

function crtajStuboveNaMapi(){    
    obrisiLejerIzMape("stub");
    obrisiLejerIzMape("dv");
    if (!g_nizStubova || g_nizStubova.length < 1) return;
    
    let stacMIN = g_nizTacakaTlo[0].x; //[m]
    let stacMAX = g_nizTacakaTlo[g_nizTacakaTlo.length-1].x; //[m]
    let stacDuzina = stacMAX-stacMIN;

    
    let nizStacionaza = izdvojNizIzObjekata("x",g_nizStubova);
    let latDiff = g_latlongB[0] - g_latlongA[0];
    let longDiff = g_latlongB[1] - g_latlongA[1]; 
    let nizLatLong=[], procenat;
    for (let i = 0; i < nizStacionaza.length; i++) {
        procenat = (nizStacionaza[i] - stacMIN)/stacDuzina;
        nizLatLong[i] = [g_latlongA[0] + procenat*latDiff, 
                         g_latlongA[1] + procenat*longDiff];
    }

    let map_nizTacaka_transformed = prilagodiKoorSis_niz(nizLatLong);

    let vektor_TacakaStub = kreirajVektor_tacaka(map_nizTacaka_transformed);
    let vektor_LinijeDV = kreirajVektor_linije(map_nizTacaka_transformed);

    let lejer_TackeStub = kreirajLejer(vektor_TacakaStub, stilStub, "stub",15);
    let lejer_LinijaDV = kreirajLejer(vektor_LinijeDV, stilDV, "dv",11);

    dodajLejerUMapu(lejer_TackeStub);
    dodajLejerUMapu(lejer_LinijaDV);


}


let stilStub = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 7,
        fill: new ol.style.Fill({ color: 'red' }),
        stroke: new ol.style.Stroke({ color: 'red', width: 0.5 })
    })
});
let stilTrase = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: "black",
        width: 2,
        lineDash: [4, 8]
    })
});
let stilDV = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: "red",
        width: 3
        /*,lineDash: [4, 8]*/
    })
});
let stilX = new ol.style.Style({
    image: new ol.style.RegularShape({
        stroke: new ol.style.Stroke({ color: 'black', width: 2 }),
        points: 4, //cetiri tacke
        radius: 10, // duzina linije
        radius2: 0, // formiranje x znaka umesto kvadrata
        angle: Math.PI / 4
    })
});

$(document).ready(function () {
    
    function kreirajMapu(lejeri, centar, zoomVar) {
/*
        var inchesPerMeter = 39.3700787;
        var dpi = 96;

        function getResolutionFromScale(scaleDenominator) {
        return scaleDenominator / inchesPerMeter / dpi;
        }
                var scales = [500, 1000, 2000, 4000, 10000, 25000, 50000];
        var resolutions = [];
        for(var i = 0; i < scales.length; i++) {
            resolutions.push(getResolutionFromScale(scales[i]));
        }
*/
        let newMap = new ol.Map({            
            layers: lejeri, // lejere tacak i linija dodajem preko funkcija
            // Improve user experience by loading tiles while dragging/zooming. Will make
            // zooming choppy on mobile or slow devices.
            loadTilesWhileInteracting: true,
            target: 'map', //id diva u koji se smesta mapa 
            view: new ol.View({
                center: prilagodiKoorSis_tacka(centar),
                zoom: zoomVar,
                maxZoom: 20
/*                
                minScale: scales[scales.length - 1],
                maxScale: scales[0],        
                resolution: resolutions,
                numZoomLevels: scales.length,
                units: 'm'
*/                
            })
        });
        
        newMap.addControl(new ol.control.ScaleLine());
        newMap.addControl(new ol.control.FullScreen());
        return newMap;
    }    

    function promeniBingMapu() {
        let selektovaniLejer = selektorBingMapa.value;
        for (let i = 0; i < bing_lejeri.length; i++)
            bing_lejeri[i].setVisible(oznakaLejera[i] === selektovaniLejer);
    }
    
    // moguci lejeri bing mape
    let oznakaLejera = ['Road', 'Aerial', 'AerialWithLabels'];
    // kreiranje niza lejera bingmape
    let bing_lejeri = [];
    for (let i = 0; i < oznakaLejera.length; i++) {
        bing_lejeri.push(new ol.layer.Tile({
            visible: false,
            preload: Infinity,
            source: new ol.source.BingMaps({
                key: 'ApuESI8iqT6sCpVk_CjJPlSVENnNbK19v_ubab_4XXI-zA_AvhVCYgl_TMvpqB4V',
                imagerySet: oznakaLejera[i]
                // use maxZoom 19 to see stretched tiles instead of the BingMaps
                // "no photos at this zoom level" tiles
                ,maxZoom: 19
            })
        }));
    }
    // na osnovu selektora zadajemo tip bing mape koja ce da se prikazuje
    let selektorBingMapa = document.getElementById('layer-select');

    selektorBingMapa.addEventListener('change', promeniBingMapu());
    promeniBingMapu();


    let centarMape = [43.72583, 20.68944]; // lat(y koordinata), long(x koordinata)
    let zoom_map = 8;
    // kreiranje mape
    g_map = kreirajMapu(bing_lejeri, centarMape, zoom_map);

    /**
     * klikom na mapu odabiramo tacku iz koje krece ili se zavrsava tarasa DV
     */
    g_map.on('click', function(e) {
        // ako g_pickPoint nije aktivno onda nam ne treba odabir tacke 
        if ( ! g_pickPoint) return; 

        let latlong = ol.proj.transform(e.coordinate, "EPSG:3857", "EPSG:4326");
        let lat = latlong[1]; // u njihovom koord sis prvo ide long pa lat
        let long = latlong[0];
        let inputs = $('#formLatLong .activePoint input');
        $(inputs[0]).val(lat);
        $(inputs[1]).val(long);

        // izlazak iz fullscreen
        $(".ol-full-screen-true").click();

        g_pickPoint=false;//iskljucujemo odabir tacke

        upisiRazdaljivuDialog(); 
    });

});   