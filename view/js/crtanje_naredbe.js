/*
 * @author Nikola Pavlovic
 * sve naredbe potrebne za crtanje
 * njihov redosled je BITAN
 */

$(document).ready(function () {
    $('.dropdown-submenu button').on("click", function (e) {
        $(this).next('div').toggle();
        e.stopPropagation();
        e.preventDefault();
    });

    // ukljucivanje bootstrap tooltipa  - spoljna biblioteka
    $('[data-toggle="tooltip"]').tooltip();
    // kada je vec ukljucen data-toggle=dropdown, onda moram preko drugog selektora
    $('[data-toggle1="tooltip1"]').tooltip();

    // kreiramo SVG u div sa id=drawing - spoljna biblioteka
    g_svgObj = SVG('drawing').size("100%", "100%");

    // resetovanje zuma - spoljna biblioteka
    $('#btn_ZoomReset').click(
        function () { g_svgPanZoom.reset(); }
    );

    //ponovno crtamo sve ispocetka prilikom window.resize
    $(window).resize(function () {
        g_svgPanZoom = $($("svg")[0]).svgPanZoom(
            {
                initialViewBox: {
                    x: 0, // the top-left corner X coordinate
                    y: 0, // the top-left corner Y coordinate
                    width: getSirina(), // the width of the viewBox
                    height: getSirina() // the height of the viewBox
                }
            }
        );
        pokreniCrtanje();
    });

    // eventListner za fileInput tacke tla
    // pri ucitavanju fajla: g_nizTacakaTlo = []
    document.getElementById("file_teren").addEventListener("change", function () {
        if (this.files && this.files[0]) { // ako je ispravno ucitan fajl i to samo jedan
            let myFile = this.files[0];
            let reader = new FileReader();

            reader.onload = function () {
                let fileContents = this.result;
                resetGlobNizova();

                // na osnovu '\n' izdvajanmo red po red iz stringa
                let novRed = '\n';
                let fromThisIndex = brojKaraktera = 0;
                let stringRed;
                while ((brojKaraktera = fileContents.indexOf(novRed, fromThisIndex)) !== -1) { // trazi novRed od fromThisIndex pa nadalje
                    stringRed = fileContents.substring(fromThisIndex, brojKaraktera);
                    getBrojeveIzStringa(stringRed, 2);
                    fromThisIndex = brojKaraktera + 1;
                }
                // ovo je za posledni red
                getBrojeveIzStringa(fileContents.substring(fromThisIndex), 2);

                pokreniCrtanje();
            };

            // prvo smo gore zadali lisener pa tek sada pozivam reader
            reader.readAsText(myFile);

        }// end if
    });

    // eventListner za fileInput tacke objekata
    // pri ucitavanju fajla: 
    document.getElementById("file_obj").addEventListener("change", function () {
        if (this.files && this.files[0]) { // ako je ispravno ucitan fajl i to samo jedan
            let myFile = this.files[0];
            let reader = new FileReader();

            reader.onload = function () {
                let fileContents = this.result;
                g_objektiNaTrasi = [];  // resetujemo samo objekte trase

                // na osnovu '\n' izdvajanmo red po red iz stringa
                let novRed = '\n';
                let fromThisIndex = brojKaraktera = 0;
                let stringRed;
                while ((brojKaraktera = fileContents.indexOf(novRed, fromThisIndex)) !== -1) { // trazi novRed od fromThisIndex pa nadalje
                    stringRed = fileContents.substring(fromThisIndex, brojKaraktera);
                    getBrojeveIzStringa(stringRed, -1);// - 1 kao selektor u funckiji
                    fromThisIndex = brojKaraktera + 1;
                }
                // ovo je za posledni red
                getBrojeveIzStringa(fileContents.substring(fromThisIndex), -1);// - 1 kao selektor u funckiji

                pokreniCrtanje();

            };

            // prvo smo gore zadali lisener pa tek sada pozivam reader
            reader.readAsText(myFile);

        }// end if
    });


    // definicija dialoga za stubove  
    $("#unesiStubove_dialog").dialog({
        autoOpen: false,
        modal: true, // onemogucen klik van dialoga
        width: 800,
        height: 400,
        closeText: "zatvori",
        classes: { // zadajemo klase za da bi prilagodili izgled dialoga
            "ui-dialog": "p-0 border-0 shadow-lg rounded", // ceo dialog
            "ui-dialog-titlebar": "border-0 bg-primary text-light", //heder
            "ui-dialog-content": "p-0", //body
            "ui-dialog-buttonpane": "p-0 bg-info" //footer
        },
        show: {
            // effect - uzimamo random animaciju, menja se tek pri ponovnom ucitavanjem stranice
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 1000, // 100ms
            easing: "swing"
        },
        hide: {
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 500
        },
        buttons: [
            {
                text: "Ok",
                /*icon: "ui-icon-heart",*/
                click: function () {
                    // na klik - pokrece submit forme
                    $(this).find("[type=submit]").click();
                }
            }
        ]
    });

    // pokretanje dialoga stubova i ucitavanje vrednosti u njegove inpute
    $("#unesiStubove_btn").click(function () {
        if (!is_setTlo()) {
            alert("Prethodno morate uneti tacke uzduznog profila trase");
            return;
        }

        $("#unesiStubove_dialog").dialog("open");

        // zadajemo timeout da bi bili sigurni da ce dialog da bude prikazan
        // pre nego sto mu pristupamo elementima preko jQ
        setTimeout(function () {
            if (is_setStubMesta()) {
                let singleRow = $("#tableNo1 tbody tr:first-of-type");
                let singleRowClon;
                let inputiNiz;
                $("#tableNo1 tbody tr").remove();
                for (let i = 0; i < g_nizStubova.length; i++) {
                    singleRowClon = $(singleRow).clone();
                    $(singleRowClon).attr("data-id", i);
                    $("#tableNo1 tbody").append(singleRowClon);
                    inputiNiz = $("#tableNo1 tbody tr:last-of-type").find("input");
                    $(inputiNiz[0]).val(g_nizStubova[i].oznaka);
                    $(inputiNiz[1]).val(g_nizStubova[i].x);
                    $(inputiNiz[2]).val(g_nizStubova[i].h);
                    $(inputiNiz[3]).val(g_nizStubova[i].tip);
                    $(inputiNiz[4]).val(g_nizStubova[i].konzola);
                }
            }
            else {  // brisemo sve redove koji su preostali od prosle raspodele stubnih mesta
                $("#tableNo1 tbody tr:not(:first-of-type)").each(function () {
                    $(this).remove();
                });

            }
            // dodeljujemo event za brisanje
            buttonEventZaBrisanje();
        }, 300);
    });

    // dialogStub - kopira poslednji red u tbody i appenduje ga na kraj tbody
    //nakon kopiranja inkrementira data-id i brise vrednosti input polja
    $("#dodajRed").click(function () {
        let singleRow = $("#tableNo1 tbody tr:last-of-type").clone();
        let lastDataID = parseInt($(singleRow).attr("data-id")) + 1;
        $(singleRow).attr("data-id", lastDataID);

        //brise vrednost inputa kreiranog reda
        nizInputa = $(singleRow).find("input");
        for (let i = 0; i < nizInputa.length; i++) {
            let valInput = $(nizInputa[i]).val();
            let nameInput = $(nizInputa[i]).attr("name");
            nameInput = nameInput.substr(0, nameInput.indexOf('-') + 1);
            $(nizInputa[i]).attr("name", nameInput + lastDataID);

            if (i === 0) {
                // incrementrira rb 
                let broj = parseInt(valInput);
                if (Number.isInteger(broj))
                    broj++;
                else
                    broj = lastDataID;

                $(nizInputa[i]).val(broj);
            }
            else if (i === 1) {
                // brise x
                $(nizInputa[i]).val("");
            }
            else {
                // sve ostalo ostaje isto
            }
        }

        $("#tableNo1 tbody").append(singleRow);

        //dodeljuje event-listener za button u novo kreiranom redu
        buttonEventZaBrisanje();

        //skroluje na-dole
        $("#unesiStubove_dialog_cointainer").animate({ scrollTop: $('#unesiStubove_dialog_cointainer').get(0).scrollHeight }, "fast");
        // fokus na input za X u novom redu
        $("#tableNo1 tbody tr:last-of-type").find("input")[1].focus();

    });

    // dialogStub forma - umesto submita, poziva se f-ja ucitajStuboveDialog() i zatvara se dialog
    $("#form_stubovi").on("submit", function (event) {
        event.preventDefault();
        let bool_flag = ucitajStuboveDialog();
        if (bool_flag) {
            $("#unesiStubove_dialog").dialog("close");
        }
    });

    //////////////////////////////////////////////////////////////////////
    // kliknuto na #saveProject - validiraj i pokreni submitData
    $('#saveProject').click(function () {
        if (typeof g_projName === "undefined") {
            event.preventDefault();
            $('#saveAsProject').click();
        }
        else {
            $("#modalTextInput").modal("show");
            $('#formModalInput input[name="projName"]').attr("disabled", "true");
            $('#formModalInput input[name="projName"]').val(g_projName);
            $('#formModalInput input[name="action"]').val("updateProject");
        }
    });

    // kliknuto na #saveAsProject - pokreni modal za unos imena novog proj
    $('#saveAsProject').click(function () {
        let textInput;
        if (typeof g_projName === "string")
            textInput = g_projName;
        else
            textInput = "";


        $("#modalTextInput").modal("show");
        $('#formModalInput input[name="projName"]').removeAttr("disabled");;
        $('#formModalInput input[name="projName"]').val(textInput);
        $('#formModalInput input[name="action"]').val("saveAsProject");
        /* nece da radi na 100ms, ali hoce na 1000ms */
        setTimeout(function () {
            $('#formModalInput input[name="projName"]').focus();
            $('#formModalInput input[name="projName"]').select();
        }, 1000);

    });

    /**
     * forma getLancanica
     */
    $("#formaLanc").submit(function (event) {
        event.preventDefault();
        submitData('getLancanica');
    });

    // kliknuto na #getPdf - validiraj i pokreni submitData
    $('#getPdf').click(function () {
        submitData('getPdf');
    });
    //////////////////////////////////////////////////////////////////
    // toggle ordinate 
    $("#ordinata").click(function () {
        if (g_ordinata) { // ako ordinata postoji
            if ($(this).html() === "ordinataOFF")
                $(this).html("ordinataON");
            else
                $(this).html("ordinataOFF");
            $("#" + g_ordinata.id()).toggle();
        }
    });

    // snap kontrola 
    $("#snap").click(function () {
        if (g_snap) {
            g_snap = false;
            $(this).html("snapOFF");
        } else {
            g_snap = true;
            $(this).html("snapON");
        }
    });

    // ukljucivanje pan/zoom - spoljna biblioteka
    // - ide preko timeout da bi bio siguran da je svg ucitan
    setTimeout(function () {
        let mainSVG = $("svg")[0];
        g_svgPanZoom = $(mainSVG).svgPanZoom(
            {
                events: {
                    mouseWheel: true, // enables mouse wheel zooming events
                    doubleClick: true, // enables double-click to zoom-in events
                    drag: true, // enables drag and drop to move the SVG events
                    dragCursor: "move" // cursor to use while dragging the SVG
                },
                animationTime: 300, // time in milliseconds to use as default for animations. Set 0 to remove the animation
                zoomFactor: 0.25, // how much to zoom-in or zoom-out
                maxZoom: 50, //maximum zoom in, must be a number bigger than 1
                panFactor: 100, // how much to move the viewBox when calling .panDirection() methods
                minZoom: -1,

                initialViewBox: { // the initial viewBox, if null or undefined will try to use the viewBox set in the svg tag. Also accepts string in the format "X Y Width Height"
                    x: 0, // the top-left corner X coordinate
                    y: 0, // the top-left corner Y coordinate
                    width: getSirina(), // the width of the viewBox
                    // AKO STAVIM DA SE WIDTH I HEIGHT RAZLIKUJU ONDA NECE LEPO DA RADI SCROLL ZUM
                    height: getSirina() // the height of the viewBox
                },
                limits: { // the limits in which the image can be moved. If null or undefined will use the initialViewBox plus 15% in each direction
                    x: -150,
                    y: -150,
                    x2: 1150,
                    y2: 1150
                }

            });

    }, 500);


    //$("#map_container").hide(); // ovo mi je pravilo bug jer krije i div i mapu
    $("#map_toggle").click(function () {
        $("#map_container").toggle("slide", { direction: "right" }, 1000);
        // slicno kao i gore
        $("#map_container canvas:first-of-type").toggle("slide", { direction: "right" }, 1000);
        $("#map_toggle i").toggleClass("fa-angle-double-left");
        $("#map_toggle i").toggleClass("fa-angle-double-right");
    });
    $("#map_toggle").click(); //sklanjamo mapu

    ////////////////////////////////////////////////////////////////
    // potrebno jedino za mousemove event - koji se cesto izvrsama
    // pa radimo predefiniciju    
    const event_svgVar = $("svg")[0];//nece da radi preko g_svgObj, zato kreiram svgVar  
    const event_ptCoordScreen = event_svgVar.createSVGPoint();//tacka kojoj se dodeljuju koord screen-a
    /**
     * funkcija uzima screen koord iz eventa -> tansf. u svg koord
     * @param {Event} event - mouseover
     * @returns {SVGPoint} - tacka sa koord. koje se odnose na SVG
     */
    function getCursorCoordInSVG(event) {
        event_ptCoordScreen.x = event.clientX;
        event_ptCoordScreen.y = event.clientY;
        // screen koord transformisemo u koord svg elementa
        return event_ptCoordScreen.matrixTransform(event_svgVar.getScreenCTM().inverse());
    }

    // promenljive koje su potrebne za mousemove listenet
    // njihova definicija unutar mousemove bi se nepotrebno izvrsavala pri svakom eventu
    let listener_xmax;
    let listener_xmin;
    let listener_x, xaps, yaps, haps;
    // 1 - uzimamo koordinate na mousemove i ispisujemo u footer
    // 2 - vrsimo translaciju g_ordinata
    event_svgVar.addEventListener('mousemove', function (event) {
        // ako trasa nije ucitana onda nema sta da upisujemo
        if (is_setTlo()) {
            listener_xmax = g_nizTacakaTlo_rel[g_nizTacakaTlo_rel.length - 1].x;
            listener_xmin = g_nizTacakaTlo_rel[0].x;

            // uzimamo samo x - drugu koord proracunavamo            
            listener_x = getCursorCoordInSVG(event).x;
            if (listener_x < listener_xmin)
                listener_x = listener_xmin;
            else if (listener_x > listener_xmax)
                listener_x = listener_xmax;

            x_aps = kovnertAtribut_rel_to_aps("x", listener_x);

            if (g_snap && is_setArray(g_nizStacObj,1)) {
                let osetljivost = skaliraj_X_unazad(g_snap_osetljivost);
                for (let i = 0; i < g_nizStacObj.length; i++) {
                    if (x_aps > g_nizStacObj[i]-osetljivost &&
                        x_aps < g_nizStacObj[i]+osetljivost) {
                            x_aps=g_nizStacObj[i];
                    }                    
                }
            }

            $("#x_koor").val(round(x_aps));
            upisiYHfooter(x_aps);
            translirajOrdinatu(x_aps);
        }
    });
    ////////////////////////////////////////////////////////////////
    // definicija dialoga za open sesiju  
    $("#openProject_dialog").dialog({
        autoOpen: false,
        modal: true, // onemogucen klik van dialoga
        width: 700,
        height: 450,
        closeText: "zatvori",
        classes: { // zadajemo klase za da bi prilagodili izgled dialoga
            "ui-dialog": "p-0 border-0 shadow-lg rounded", // ceo dialog
            "ui-dialog-titlebar": "border-0 bg-primary text-light", //heder
            "ui-dialog-content": "p-0", //body
            "ui-dialog-buttonpane": "p-0 m-0 bg-info" //footer
        },
        show: {
            // effect - uzimamo random animaciju, menja se tek pri ponovnom ucitavanjem stranice
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 1000, // 100ms
            easing: "swing"
        },
        hide: {
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 500
        },
        buttons: [
            {
                text: "Select",
                title: "ucitaj projekat",
                /*icon: "ui-icon-circle-check",*/
                class: "bg-primary text-light",
                click: function () {
                    if (typeof g_selectedProjData === "undefined") {
                        alert("prvo selektuj projekat");
                        return;
                    }
                    else {
                        $(this).dialog("close");
                        loadSelectedProj(g_selectedProjData);
                    }
                }
            }
        ]
    });
    // pokretanje dialoga openProj i ucitavanje vrednosti u njegove inpute
    $("#openProject").click(function () {
        if (false) {
            alert("Prethodno morate uneti tacke uzduznog profila trase");
            return;
        }

        $("#openProject_dialog").dialog("open");

        deleteDialogInfo();

        g_selectedProjData = undefined;

        let url_controler = "../control/Controller_projekti.php";
        let action_controler = "getUserProjectsList";
        $.ajax({ /*getUserProjectsList*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: "&action=" + action_controler,
            success: function (data) {
                if (data.status != "success") {
                    let tBody = $("#tableOpenProj tbody ");
                    $(tBody).find("tr:first-of-type td").html(data.status + " (" + data.msg + ")");
                    return;
                }

                let niz = data.data;

                let trDef = $("#tableOpenProj tbody tr:first-of-type").clone();
                $(trDef).css("display", "table-row");
                $(trDef).find("td").css("display", "table-cell");
                $("#tableOpenProj tbody").empty();
                for (let i = 0; i < niz.length; i++) {
                    newTr = $(trDef).clone();
                    $(newTr).find("td").html(niz[i].naziv);
                    $(newTr).find("td").attr("data-id", niz[i].id);
                    $("#tableOpenProj tbody").append(newTr);
                }

                redovi = $("#tableOpenProj tbody td");
                for (let i = 0; i < redovi.length; i++) {
                    $(redovi[i]).click(function () {

                        let url_controler = "../control/Controller_projekti.php";
                        let action_controler = "getSingleProj";
                        $.ajax({ /*getSingleProj*/
                            type: "POST",
                            url: url_controler,
                            timeout: 5000,
                            beforeSend: function(){ajax_beforeSend()},
                            complete: function(){ajax_complete()},
                            dataType: 'json',
                            error: function (dataErr) {
                                ajax_error(dataErr.responseText);
                            },
                            data: "&action=" + action_controler + "&projId=" + $(this).attr("data-id"),
                            success: function (data) {
                                if (data.status != "success") {
                                    let detailView = $("#openProject_dialog .detailView ");
                                    $(detailView).find("header em").html(data.status + " (" + data.msg + ")");
                                    g_selectedProjData = undefined;
                                    return;
                                }

                                g_selectedProjData = data.data;

                                let detailView = $("#openProject_dialog .detailView ");
                                $(detailView).find("header em").html(data.data.naziv);
                                $(detailView).find("#projVreme").html(data.data.vreme);
                                $(detailView).find("#projProv").html(data.data.oznakaProv);
                                $(detailView).find("#projNaprezanje").html(data.data.naprezanje);
                                $(detailView).find("#projOdo").html(data.data.odo);
                                $(detailView).find("#projTemperatura").html(data.data.temperatura);
                                let tackeTlo = JSON.parse(data.data.tackeTlaJson);
                                if (tackeTlo)
                                    $(detailView).find("#projTlo").html(tackeTlo.length);
                                else
                                    $(detailView).find("#projTlo").html("-");
                                let objektiTrase = JSON.parse(data.data.objektiTraseJson);
                                if (objektiTrase)
                                    $(detailView).find("#projObjektiTrase").html(objektiTrase.length);
                                else
                                    $(detailView).find("#projObjektiTrase").html("-");
                                let stubovi = JSON.parse(data.data.stubnaMestaJson);
                                if (stubovi)
                                    $(detailView).find("#projStub").html(stubovi.length);
                                else
                                    $(detailView).find("#projStub").html("-");
                                let tackeLanc = JSON.parse(data.data.tackeLancJson);
                                if (tackeLanc)
                                    $(detailView).find("#projLanc").html(tackeLanc.length);
                                else
                                    $(detailView).find("#projLanc").html("-");
                            }
                        });
                    });
                }//end for



                // nece da formatira tabelu
                //$("#tableOpenProj").removeClass();
                //$("#tableOpenProj").addClass("table table-striped table-hover ");
            }
        });// end  a j a x

    });

    // pokretanje saveAs iz modala
    $("#formModalInput").submit(function (event) {
        event.preventDefault();
        if (!validirajZahtev_projName())
            return;
        else {
            $("#modalTextInput").modal("hide");
            let action = $('#formModalInput input[name="action"]').val();
            submitData(action);
        }
    });

    // 
    $("#deleteProj").click(function (event) {
        event.preventDefault();
        if (typeof g_selectedProjData === "undefined") {
            alert("prvo selektuj projekat");
            return;
        }

        let url_controler = "../control/Controller_projekti.php";
        let action_controler = "delete";
        deleteDialogInfo();
        $.ajax({ /*delete proj*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: "action=" + action_controler + "&projId=" + g_selectedProjData.id + "&projName=" + g_selectedProjData.naziv,
            success: function (data) {
                if (data.status != "success") {
                    alert(data.status + " "+data.msg);
                    return;
                }

                g_selectedProjData = undefined;
                $("#openProject_dialog_cointainer .masterView td[data-id='" + data.data + "']").hide("slow");
                alert("izbrisan projekat");

            }
        });
    });


    // definicija dialoga za user info
    $("#userInfo_dialog").dialog({
        autoOpen: false,
        modal: true, // onemogucen klik van dialoga
        width: 600,
        height: 500,
        closeText: "zatvori",
        classes: { // zadajemo klase za da bi prilagodili izgled dialoga
            "ui-dialog": "p-0 border-0 shadow-lg rounded", // ceo dialog
            "ui-dialog-titlebar": "border-0 bg-primary text-light", //heder
            "ui-dialog-content": "p-0", //body
            "ui-dialog-buttonpane": "p-0 m-0 bg-info" //footer
        },
        show: {
            // effect - uzimamo random animaciju, menja se tek pri ponovnom ucitavanjem stranice
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 1000, // 100ms
            easing: "swing"
        },
        hide: {
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 500
        },
        buttons: [
            {
                text: "OK",
                title: "zatvori info dialog",
                /* icon: "ui-icon-circle-check", */
                class: "bg-primary text-light",
                click: function () { $(this).dialog("close"); }
            }
        ]
    });
    // pokretanje dialoga userInfo i ucitavanje vrednosti u njegova polja
    $("#userInfo_button").click(function () {
        $("#userInfo_dialog").dialog("open");

        let url_controler = "../control/Controller_user_history.php";
        let action_controler = "GetInfo";
        $.ajax({ /* get user info*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: "&action=" + action_controler,
            success: function (data) {
                if (data.status != "success") {
                    $("#userInfo_dialog_cointainer strong").html("-");
                    $("#userInfo_dialog_cointainer h5:first-of-type strong").html(data.status);
                    return;
                }

                let nizDay_1 = data.data.d01;
                let nizDay_30 = data.data.d30;
                let nizTotal = data.data.total;

                let nizQuota = data.data.quota;
                let nizDay_1_quota = [];
                nizDay_1_quota.push(nizQuota["noJS_day"], nizQuota["noProj_day"], nizQuota["noPDF_day"]);
                let nizDay_30_quota = [];
                nizDay_30_quota.push(nizQuota["noJS_month"], nizQuota["noProj_month"], nizQuota["noPDF_month"]);

                function upisiPodatke(niz, i) {
                    $(".brojJS:eq(" + i + ")").html(niz["brojJS"]);
                    $(".brojProj:eq(" + i + ")").html(niz["brojProj"]);
                    $(".brojPDF:eq(" + i + ")").html(niz["brojPDF"]);
                }
                function upisiPodatkeQuota(niz, i) {
                    $(".brojJS_quota:eq(" + i + ")").html(niz[0]);
                    $(".brojProj_quota:eq(" + i + ")").html(niz[1]);
                    $(".brojPDF_quota:eq(" + i + ")").html(niz[2]);
                }

                upisiPodatke(nizDay_1, 0);
                upisiPodatke(nizDay_30, 1);
                upisiPodatke(nizTotal, 2);
                upisiPodatkeQuota(nizDay_1_quota, 0);
                upisiPodatkeQuota(nizDay_30_quota, 1);


            }
        });// end a j a x

    });


    /**
     * validira unetu koordinaty X u futer
     * - pokrece trans. ordinate
     * - pokrece upis Y, H
     * @param {Event} event onsubmit
     * @global g_nizTacakaTlo
     */
    $("#form_x_koor").submit(function (event) {
        event.preventDefault(); // sprecavamo submit forme
        if (is_setTlo()) {
            let x_aps = parseFloat($("#x_koor").val());
            let xmax = g_nizTacakaTlo[g_nizTacakaTlo.length - 1].x;
            let xmin = g_nizTacakaTlo[0].x;

            if (x_aps >= xmin && x_aps <= xmax) {
                upisiYHfooter(x_aps);
                translirajOrdinatu(x_aps);
            }
            else
                alert("Unesite ispravnu stacionazu!");
        }
    });


    $('#btn_centerMap').click(function () {
        if (typeof g_nizTacakaTlo != "undefined" && g_nizTacakaTlo.length > 1) {
            //if(true)  { 
            $("#modalLatLong").modal("show");
            if ($("#map_container").css('display') == 'none') {
                $("#map_toggle").click();
            }
        } else {
            alert("Ucitaj prvo uzduzni profil terena!");
        }
        g_boolFlag_elevacija = false;
    });


    // centriranje mape
    $("#formLatLong").submit(function (event) {
        event.preventDefault();

        //returns key-value pairs
        let data = $(this).serializeArray().reduce(function (obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

        let lat1, lat2, long1, long2;
        try {
            lat1 = parseFloat(data.latFirst);
            long1 = parseFloat(data.longFirst);
            lat2 = parseFloat(data.latLast);
            long2 = parseFloat(data.longLast);
        } catch (error) {
            alert("nisu ispravno unete koordinate");
            return;
        }

        if (lat1 == lat2 && long1 == long2) {
            alert("Koordinate su indenticne!?");
            return;
        }

        let distance = distanceLatLong([lat1, long1], [lat2, long2]);
        if (distance > 3000) {
            alert("Duzina trase mora biti manja od 3000m");
            return;
        }

        g_latlongA[0] = lat1;
        g_latlongA[1] = long1;
        g_latlongB[0] = lat2;
        g_latlongB[1] = long2;

        if (g_boolFlag_elevacija) {
            g_boolFlag_elevacija = false;
            let url_controler = "../control/Controller_alati.php";
            let action_controler = "getElevation";
            $.ajax({ /* get elevation*/
                type: "POST",
                url: url_controler,
                timeout: 4*5000,
                beforeSend: function(){ajax_beforeSend()},
                complete: function(){ajax_complete()},
                dataType: 'json',
                error: function (dataErr) {
                    ajax_error(dataErr.responseText);
                },
                data: $("#formLatLong").serialize() + "&distance=" + distance + "&action=" + action_controler,
                success: function (data) {
                    if (data.status != "success") {
                        alert(data.status);
                        return;
                    }

                    resetGlobNizova();
                    g_nizTacakaTlo = data.data;
                    pokreniCrtanje();
                    centrirajMapu(g_latlongA, g_latlongB);
                    alert("uspesno kreiran profil");

                }
            });
        }

        else {
            // ne sme da se izrsava ako prethodno nije ucitana trasa
            // - odnosno nema svg polyline koja nam odredjuje sirinu vektora tacaka tla
            // koji se prikazuje na mapi
            if (is_setTlo()) {
                centrirajMapu(g_latlongA, g_latlongB);
            }
        }

        $("#modalLatLong").modal("hide");


    });

    /**
     * zamena vrednosti koordinata 
     */
    $("#swapLatLong").click(function () {
        let latA = $("input[name='latFirst']").val();
        let longA = $("input[name='longFirst']").val();
        let latB = $("input[name='latLast']").val();
        let longB = $("input[name='longLast']").val();
        $("input[name='latFirst']").val(latB);
        $("input[name='longFirst']").val(longB);
        $("input[name='latLast']").val(latA);
        $("input[name='longLast']").val(longA);
    });

    // odabir koordinate latlong klikom na mapu
    $('.pickCoord').click(function () {
        //$("#modalLatLong").modal("hide"); // ne mora da zatvaram sada
        $(".ol-full-screen-false").click(); // ulazimo u full screen
        g_pickPoint = true; // signaliziramo da pri kliku na mapu vrati koordinate

        // signal gde da se vracene koordinate uspisu
        $('.latlongPoint').removeClass('activePoint');
        $(this).parent().addClass('activePoint');
    });

    // racuna i upisuje razdaljinu pri promeni latlong vrednosti
    $("#formLatLong input").on("change paste keyup", function () {
        upisiRazdaljivuDialog();
    });


    $('#getElevation').click(function () {
        $("#modalLatLong").modal("show");
        if ($("#map_container").css('display') == 'none') {
            $("#map_toggle").click();
        }
        g_boolFlag_elevacija = true;

    });

    // definicija dialoga za user info
    $("#tabelaUgiba_dialog").dialog({
        autoOpen: false,
        modal: true, // onemogucen klik van dialoga
        width: 800,
        height: 500,
        closeText: "zatvori",
        classes: { // zadajemo klase za da bi prilagodili izgled dialoga
            "ui-dialog": "p-0 border-0 shadow-lg rounded", // ceo dialog
            "ui-dialog-titlebar": "border-0 bg-primary text-light", //heder
            "ui-dialog-content": "p-0", //body
            "ui-dialog-buttonpane": "p-0 m-0 bg-info" //footer
        },
        show: {
            // effect - uzimamo random animaciju, menja se tek pri ponovnom ucitavanjem stranice
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 1000, // 100ms
            easing: "swing"
        },
        hide: {
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 500
        },
        buttons: [
            {
                text: "OK",
                title: "zatvori dialog",
                /* icon: "ui-icon-circle-check", */
                class: "bg-primary text-light",
                click: function () { $(this).dialog("close"); }
            }
        ]
    });

    // pokretanje dialoga za prikaz tabele ugiba
    $("#getTabelaUgiba").click(function () {
        if (!is_setLancanica()) {
            alert("Prethodno morate da kreirate lancanicu");
            return;
        }

        $("#tabelaUgiba_dialog").dialog("open");

        let url_controler = "../control/Controller_js.php";
        let action_controler = "GetTabelaUgiba";
        $.ajax({ /*get tabela ugiba*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: $("#formaLanc").serialize() + "&action=" + action_controler,
            success: function (data) {
                if (data.status != "success") {
                    alert(data.status + " - " + data.msg);
                    return;
                }
                $("#tabelaUgiba_dialog_cointainer strong").html("-");
                $("#tabelaUgiba_dialog_cointainer td").html("-");

                let nizHedera = data.data.heder;
                let matricaUgiba = data.data.ugib;
                let nizRaspona = getNizRaspona();

                let nizHeaderStrong = $("#tabelaUgiba_dialog_cointainer header strong");
                for (let i = 0; i < nizHeaderStrong.length; i++) {
                    $(nizHeaderStrong[i]).html(nizHedera[i]);
                }


                let singleRow = $("#tabelaUgiba_dialog_cointainer tbody tr:first-of-type");
                let singleRowClon;
                let tdNiz;
                $("#tabelaUgiba_dialog_cointainer tbody tr").remove();
                for (let i = 0; i < nizRaspona.length; i++) {
                    singleRowClon = $(singleRow).clone();
                    $("#tabelaUgiba_dialog_cointainer tbody").append(singleRowClon);
                    tdNiz = $("#tabelaUgiba_dialog_cointainer tbody tr:last-of-type").find("td");
                    $(tdNiz[0]).html(nizRaspona[i]);

                    for (let j = 0; j < matricaUgiba[i].length; j++) {
                        $(tdNiz[j + 1]).html(matricaUgiba[i][j]);
                    }
                }
            } // end s u c c e s
        });// end a j a x

    });

    $("#viewProv_dialog").dialog({
        autoOpen: false,
        modal: true, // onemogucen klik van dialoga
        width: 700,
        height: 500,
        closeText: "zatvori",
        close: function( event, ui ) {/*alert('a5454'); primer kao da uzvatim dialog close*/},
        classes: { // zadajemo klase za da bi prilagodili izgled dialoga
            "ui-dialog": "p-0 border-0 shadow-lg rounded", // ceo dialog
            "ui-dialog-titlebar": "border-0 bg-primary text-light", //heder
            "ui-dialog-content": "p-0", //body
            "ui-dialog-buttonpane": "p-0 m-0 bg-info" //footer
        },
        show: {
            // effect - uzimamo random animaciju, menja se tek pri ponovnom ucitavanjem stranice
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 1000, // 100ms
            easing: "swing"
        },
        hide: {
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 500
        },
        buttons: [
            {
                text: "Ok",
                title: "zatvori pregled",
                /*icon: "ui-icon-circle-check",*/
                class: "bg-primary text-light",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });

    // pokretanje dialoga za prikaz podataka o provodnicima
    $("#provodnici_btn").click(function () {
        $('#elementiDV').click();
        $('#dropdownAlati').click();

        let url_controler = "../control/Controller_provodnik.php";
        let action_controler = "getUserProvList";
        $.ajax({ /* get listu prov*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: "&action=" + action_controler,            
            success: function (data) {
                /* ovo se automatski radi posto je dataType: 'json'
                data = JSON.parse(recivedData);
                if (!data) {
                    alert("nije moguce izvrsiti naredbu");
                    return;
                } */
                if (data.status != "success") {
                    alert(data.status + " - nije moguce izvrsiti naredbu");
                    return;
                }

                $("#viewProv_dialog").dialog("open");
                let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
                $(detailViewInputs).attr("disabled","disabled");
                toggleProvButtons(); // sakri sve buttone

                let nizOznakProv = data.data;

                let trDef = $("#viewProv_dialog_cointainer tbody tr:first-of-type").clone();
                $(trDef).css("display", "table-row");
                $(trDef).find("td").css("display", "table-cell");
                $("#tableViewProv tbody").empty();
                for (let i = 0; i < nizOznakProv.length; i++) {
                    newTr = $(trDef).clone();
                    $(newTr).find("td").html(nizOznakProv[i].oznaka);
                    $(newTr).find("td").attr("data-id", nizOznakProv[i].id);
                    $("#tableViewProv tbody").append(newTr);
                }

                redovi = $("#tableViewProv tbody td");
                for (let i = 0; i < redovi.length; i++) {
                    $(redovi[i]).click(function () {
                        let url_controler = "../control/Controller_provodnik.php";
                        let action_controler = "getProv";                        
                        let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
                        $(detailViewInputs).attr("disabled", "disabled");
                        $.ajax({ /* get single provodnik */
                            type: "POST",
                            url: url_controler,
                            timeout: 5000,
                            beforeSend: function(){ajax_beforeSend()},
                            complete: function(){ajax_complete()},
                            dataType: 'json',
                            error: function (dataErr) {
                                ajax_error(dataErr.responseText);
                            },
                            data: "&action=" + action_controler + "&provId=" + $(this).attr("data-id"),
                            success: function (data) {
                                let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
                                if (data.status != "success") {
                                    $(detailViewInputs).val("-");
                                    alert(data.status + " - nije moguce ucitavanje podataka");
                                    return;
                                }

                                if (data.data.userId == -1) toggleProvButtons("c");
                                else toggleProvButtons("cde");

                                $("#viewProv_dialog_cointainer input[name='provId']").val(data.data.id);
                                $("#viewProv_dialog_cointainer input[name='provName']").val(data.data.oznaka);
                                $("#viewProv_dialog_cointainer input[name='provPresek']").val(data.data.presek);
                                $("#viewProv_dialog_cointainer input[name='provPrecnik']").val(data.data.precnik);
                                $("#viewProv_dialog_cointainer input[name='provTezina']").val(data.data.tezina);
                                $("#viewProv_dialog_cointainer input[name='provE']").val(data.data.modul);
                                $("#viewProv_dialog_cointainer input[name='provAlfa']").val(data.data.alfa);
                                $("#viewProv_dialog_cointainer input[name='provI']").val(data.data.struja);

                            }
                        });
                    });
                }//end for
            } // end s u c c e s
        });// end a j a x

    });

    $('#copyProv').click(function () {
        let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
        $(detailViewInputs).removeAttr("disabled");

        toggleProvButtons("os");

        let inputOznaka = detailViewInputs[0];
        $(inputOznaka).val($(inputOznaka).val() + " -new");
        $(inputOznaka).focus();
    });

    $('#deleteProv').click(function () {
        let url_controler = "../control/Controller_provodnik.php";
        let action_controler = "deleteProv";
        let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
        $(detailViewInputs).removeAttr("disabled");
        let curentId = $("#form_provodnici input[name='provId']").val();
        $.ajax({ /*delete provodnik*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: $("#form_provodnici").serialize() + "&action=" + action_controler,
            success: function (data) {
                if (data.status != "success") {
                    alert(data.status +" --- "+ data.msg);
                    return;
                }
                
                let zaBrisanje = $('#formaLanc select[name="provodnik"] option[value='+"'"+data.msg+"'"+']');
                if (zaBrisanje) $(zaBrisanje).remove();

                alert(data.status +" --- "+ data.msg + "je obrisan");
                $(detailViewInputs).val("");        
                toggleProvButtons(); 
                
                $("#viewProv_dialog_cointainer .masterView td[data-id='"+curentId+"']").parent("tr").remove();
            }
        }); // end a j a x
        $(detailViewInputs).attr("disabled", "disabled");

    });

    $('#saveProv').click(function () {
        // html5 validacija preko checkValidity()
        if ($('#form_provodnici')[0].checkValidity() === false ){ 
            // ako val nije ok onda klik na submit samo 
            // da bi se prikazala poruka html5 validacije           
            $('#form_provodnici input[type="submit"]').click();
            return;
        }

        let url_controler = "../control/Controller_provodnik.php";
        let action_controler;

        let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
        let isDisabled = $(detailViewInputs).first().attr('disabled');
        
        // For some browsers, `attr` is undefined; for others, `attr` is false.  Check for both.
        if (typeof isDisabled !== typeof undefined && isDisabled !== false) {
            action_controler = "editProv";
        }
        else action_controler = "saveProv";
        
        $.ajax({ /*save provodnik*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: $("#form_provodnici").serialize() + "&action=" + action_controler,
            success: function (data) {
                if (data.status != "success") {
                    alert(data.status +" --- "+ data.msg);
                    return;
                }

                alert(data.status +" --- "+ data.msg + " ok");
                
                isDisabled = $(detailViewInputs).first().attr('disabled');
                if (typeof isDisabled == typeof undefined || isDisabled == false) {
                    // u pitanju je save
                    $('#formaLanc select[name="provodnik"]').append("<option value='"+data.msg+"'>"+data.msg+"</option>");
                }
                
                $("#viewProv_dialog").dialog("close");
                $("#provodnici_btn").click();
                $("#viewProv_dialog .master td").first().click();  
                $(detailViewInputs).attr("disabled", "disabled");
                setTimeout(function () {                  
                    toggleProvButtons("ced");
                }, 1000); 


            }
        });
    });

    $('#editProv').click(function () {        
        let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
        $(detailViewInputs).removeAttr("disabled");
        $(detailViewInputs).first().attr("disabled","disabled");

        toggleProvButtons("os");

        let inputOznaka1 = detailViewInputs[1];
        $(inputOznaka1).focus();
    });

    // koristi mi da obustavi proces save ili edit
    $('#cancelProv').click(function () {        
        let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
        $(detailViewInputs).attr("disabled","disabled");
        $(detailViewInputs).val("");

        toggleProvButtons();
    });
   

//////////////// modal dobrodoslica  
    $('#startServer').click(function(){
        $('#modalFirstAction').modal('hide');
        $('#openProject').click();
    });
    $('#startLocal').click(function(){
        $('#modalFirstAction').modal('hide');
        $('input[name="file_teren"]').click();
    });
    $('#startLocal_help').click(function(){
        let url_controler = "../control/Controller_user_history.php";
        let action_controler = "GetHelpFile_trasa";
        
        downloadFile_byPost(url_controler, 
            "action="+action_controler,
            "primer uzduznog profila.txt");        
    });
    $('#objekti_help').click(function(){
        let url_controler = "../control/Controller_user_history.php";
        let action_controler = "GetHelpFile_objekti";
        
        downloadFile_byPost(url_controler, 
            "action="+action_controler,
            "primer objekata na trasi DV.txt");        
    });
    $('#startMap').click(function(){
        $('#modalFirstAction').modal('hide');
        $('#getElevation').click();
    });

    // regEx kontrola dozvoljenih karaktera za upis u input polje
    $('#formModalInput input[name="projName"]').keypress(function (e) {
        let txt = String.fromCharCode(e.which);
        if (!txt.match(/[A-Za-z0-9. -]/))
            return false;
    });
}); // end (document).ready
