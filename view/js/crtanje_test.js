/*
 * @author Nikola Pavlovic
 * globalne promenljive
 * sve glob.promenljive imaju prefiks g_
 */

//koristim let da bi onemogucio ponovnu deklaraciju istih varijabli
//mada je to moguce unutar funkcije

/**
 * visina hedera u [pix]
 * @global
 * @constant
 * @type {Number}
 */
const g_HEDER_HEIGHT = 50;

/**
 * @global svg kontejener u kome se nalaze svi svg
 * elementi koje crtam (linije, tacke,...)
 * @type {SVG}
 */
let g_svgObj;

/**
 * @global niz (x,y) tacaka tla u [m]
 * @type {Array of Point}
 */
let g_nizTacakaTlo = [];

/**
 * @global niz (x,y) tacaka tla u [pikselima]
 * @type {Array of Point}
 */
let g_nizTacakaTlo_rel = [];
/**
 * @global niz stubova sa atributima u [m]
 * @type {Array of StubnoMesto}
 */
let g_nizStubova = [];

/**
 * @global niz stubova sa atributima u [pikselima]
 * @type {Array of StubnoMesto}
 */
let g_nizStubova_rel = [];

/**
 * @global niz (x,y) tacaka tla u [m]
 * @type {Array of Point}
 */
let g_nizTacakaLanc = [];

/**
 * @global niz (x,y) tacaka tla u [pikselima]
 * @type {Array of Point}
 */
let g_nizTacakaLanc_rel = [];

/**
 * @global vrednost za skaliranje (apsol->rel) po x osi
 * @type {Number}
 */
let g_skalarX;

/**
 * @global vrednost za skaliranje (apsol->rel) po y osi
 * @type {Number}
 */
let g_skalarY;

/**
 * const za vracanje greske
 * @constant 
 * @global 
 * @type {Number}
 */
const g_greskaFlag = -9999919;


/**
 * @global najniza kota tla
 * @type {Number}
 */
let g_minY;

/**
 * @global najniza stacionaza tla
 * @type {Number}
 */
let g_minX;

/**
 * @global kontrola pan i zoom - ekstena biblioteka
 * @type {SVGZoomAndPan}
 */
let g_svgPanZoom;

/**
 * @global mapa - ekstena biblioteka 
 * @type {ol.Map}
 */
let g_map;

/**
 * @classdesc Klasa Pointa - tacka(x,y)
 * @param {Number} x 
 * @param {Number} y 
 */
function Point(x, y) {
  this.x = x;
  this.y = y;
}

/**
 * @classdesc Klasa stubnih mesta
 * @param {String} oznaka - oznaka (r.b.) stubnog mesta
 * @param {Number} x - x koor
 * @param {Number} y - y koor 
 * @param {Number} h - visina vesanja provodnika 
 * @param {String} tip - tip stuba 
 * @param {String} konzola - tip konzole 
 */
function StubnoMesto(oznaka, x, y, h, tip, konzola) {
	this.oznaka = oznaka;
    this.x = x;
    this.y = y;
    this.h = h;
    this.tip = tip;
    this.konzola = konzola;
}

/**
 * prikaz ordinate - svg linija
 * @type {SVGLineElement}
 */
let g_ordinata;


/**
 * snap toggle za pomeranje stubova - kada se priblizi tacki tla 
 * stub dobije njenu x koord
 * @type {Boolean}
 */
let g_snap = false;
let g_snap_osetljivost = 3;

/**
 * sluzi nam kao selektor za formu - lancanica ili save sesion
 * @type {String}
 */
let g_submitButton;

/**
 * naziv projekta
 * @type {String}
 */
let g_projName;


/**
 * id projekta
 * @type {Number} int
 */
/* let g_projId; */

/**
 * podaci selektovanog projekta u openProj dialogu
 * @type {Object}
 */
let g_selectedProjData;


/**
 * podaci o objektima na trasi
 * @type {Object[]}
 */
let g_objektiNaTrasi;
let g_objektiNaTrasi_rel;


/**
 * flag za selectovanje tacke na mapi
 * @type {Boolean}
 */
let g_pickPoint = false;


/**
 * pocetna tacka trase sa koord lat, long
 * @type {Array [lat,long]}
 */
let g_latlongA=[];

/**
 * krajnja tacka trase sa koord lat, long
 * @type {Array [lat,long]}
 */
let g_latlongB=[];

/**
 * odabir elevacije ili ne prilikom submita latlong dialoga 
 * @type {boolean}
 */
let g_boolFlag_elevacija=false;

/**
 * Niz stacionaza svi objekata ispod trase dv
 * - kreira se na osnovu g_objektiNaTrasi
 * - koristi ga mousechange pri snapON 
 * @type {Array}
 */
let g_nizStacObj=[];

/*
 * @author Nikola Pavlovic
 * sve funkcije potrebne za crtanje
 * njihov redosled je NEbitan
 */
////////////////////////////////////////////////////////////////////
/**
 * sirina div.a gde je smesten SVG  
 * @returns {Number} vraca vrednost u [pikselima]
 */
function getSirina() {
    return document.getElementById('drawing').clientWidth;
}

/**
 * visina div.a gde je smesten SVG  
 * @returns {Number} vraca vrednost u [pikselima]
 */
function getVisina() {
    return document.getElementById('drawing').clientHeight;
}
//////////////////////////////////////////////////////////////

/**
 * funkcija koja iz niza Objekata uzima samo zadati atribut
 * @param {String} atribut koji zelimo da izdvojimo npr. x
 * @param {Object[]} nizObj niz iz kojeg zelimo da izdvojimo odredjen attr 
 * @returns {Array} niz vrednosti apributa npr.x
 */
function izdvojNizIzObjekata(atribut, nizObj) {
    nizAtributa = [];
    for (let i = 0; i < nizObj.length; i++) {
        nizAtributa.push(nizObj[i][atribut]);
    }
    return nizAtributa;
}

/**
 * npr: niz=[1,1.5,2] => f(1.8) = index(1.5) = 1
 * @param {Number} tacka x koord. neke tacke 
 * @param {Number[]} niz x koordinata
 * @returns {Number} najveci index clana niza koji ima vrednost manju od tacke
 */
function indexSusednog_ManjegClanaNiza(tacka, niz) {
    /** vazi ako je niz rastuci*/
    if (tacka <= niz[0]) // van opsega (u minusu)
        return 0;
    else if (tacka >= niz[niz.length - 1])
        // van opsega (u plusu) 
        return niz.length - 2; // ako vraca -1, pravio bi gresku u  [indexStuba + 1]
    else { // u opsegu
        for (let i = 1; i < niz.length; i++) {
            if (niz[i] == tacka)
                return i; // u pitanju je tacka koja se poklapa sa tackom stuba
            else if (niz[i] > tacka)
                return i - 1;
        }
    }
    return g_greskaFlag; // ako je neka greska
}

/**
 * npr: niz=[1,1.5,2] => f(1.3) = index(1.5) = 1
 * @param {Number} tacka x koord. neke tacke 
 * @param {Number[]} niz x koordinata
 * @returns {Number} najmanji index clana niza koji ima vrednost vecu od tacke
 */
function indexSusednog_VecegClanaNiza(tacka, niz) {
    // vazi ako je niz rastuci
    if (tacka <= niz[0])
        return 0;
    else if (tacka >= niz[niz.length - 1])
        return niz.length - 1;
    else {
        for (i = 1; i < niz.length; i++) {
            if (niz[i] >= tacka)
                return i;
        }
    }
    return g_greskaFlag; // ako je neka greska
}

/**
 * Proracun y koord. neke tacke koja se nalazi izmedju dve tacke cije
 * x,y vrednosti znamo
 * @param {Number} tacka_X vrednost koord.X za koju trazimo koord.Y
 * @param {Number[]} niz_X niz x koordinata tacaka
 * @param {Number[]} niz_Y niz y koordinata tacaka
 * @returns {Number} vrednost koord.Y
 */
function linearnaInterpolacija(tacka_X, niz_X, niz_Y) {

    let index0 = indexSusednog_ManjegClanaNiza(tacka_X, niz_X);
    let index1 = indexSusednog_VecegClanaNiza(tacka_X, niz_X);

    if (index0 == g_greskaFlag || index1 == g_greskaFlag)
        return g_greskaFlag;
    else if (index0 == index1) // u pitanju je kota stuba koja se poklapa sa unesenom kotom tla
        return niz_Y[index0];

    let x0 = niz_X[index0];
    let x1 = niz_X[index1];

    let y0 = niz_Y[index0];
    let y1 = niz_Y[index1];

    let x = tacka_X;
    let y; // to se trazi

    y = y0 + (x - x0) * ((y1 - y0) / (x1 - x0));

    return y;
}
/////////////////////////////////////////////////////////////////////
/**
 * Zaokruzivanje float brojeva
 * @param {Number} value float vrednost
 * @param {Number} decimals broj decimala na koji zaoukruzujemo
 * @returns {Number} zaokruzen broj na zadati broj decimala
 */
function roundDef(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

/**
 * Zaokruzivanje float brojeva na 2 dicimale
 * @param {Number} value float vrednost
 * @returns {Number} zaokruzen broj na 2 decimale
 */
function round(value) {
    return roundDef(value, 2);
}
////////////////////////////////////////////////////////////////////
/**
 * Konverzija niza Pointa u niz brojeva potreban za crtanje SVG
 * polilinije
 * @param {Point[]} nizObj niz Point(x,y)
 * @returns {Number[]} niz brojeva [x1 y1 x2 y2]
 */
function konverzija_nizObj_nizSVG(nizObj) {
    nizTacaka_SVG = [];
    for (i = 0; i < nizObj.length; i++) {
        nizTacaka_SVG.push(nizObj[i].x);
        nizTacaka_SVG.push(nizObj[i].y);
    }
    return nizTacaka_SVG;
}
///////////////////////////////////////////////////////////////////

/**
 * proracun razmera i dodeljivanje vrednosti za g_skalarX i g_skalarY
 * @global g_greskaFlag
 * @global g_skalarX
 * @global g_skalarY
 * @returns {void} void
 */
function nadjiRazmere() {
    // provera da li su setovane bar dve tacke u g_nizTacakaTlo
    // a ne proverava za g_nizTacakaTlo_rel (to se kasnije racuna)
    if (!is_setArray(g_nizTacakaTlo, 2))
        return g_greskaFlag;

    let brojTacaka = g_nizTacakaTlo.length;

    let min_X = g_nizTacakaTlo[0].x;
    let max_X = g_nizTacakaTlo[brojTacaka - 1].x;

    //vred. da bi lepo radila petlja
    let min_Y = 9999999;
    let max_Y = -1;

    for (let i = 0; i < g_nizTacakaTlo.length; i++) {
        if (g_nizTacakaTlo[i].y > max_Y)
            max_Y = g_nizTacakaTlo[i].y;
        if (g_nizTacakaTlo[i].y < min_Y)
            min_Y = g_nizTacakaTlo[i].y;
    }
    // minimalna visina zavisi samo od tacaka tla i ona se racuna
    // samo u ovoj funkciji a koristi se pri skaliranju Y atributa
    // svih objekata
    g_minY = min_Y;

    
    // minimalna stacionaza zavisi samo od tacaka tla i ona se racuna
    // samo u ovoj funkciji a koristi se pri skaliranju X atributa
    // svih objekata
    g_minX = min_X;

    // uzimamo u obzir i visinu stuba pri skaliranju Yose
    // treba nam bar JEDAN stub	
    if (is_setArray(g_nizStubova, 1)) {
        let maxVisinaStuba = 0;
        for (let i = 0; i < g_nizStubova.length; i++)
            if (g_nizStubova[i].h > maxVisinaStuba)
                maxVisinaStuba = g_nizStubova[i].h;
        max_Y += maxVisinaStuba;
    }

    // uzimamo u obzir i visinu obj pri skaliranju Yose
    // treba nam bar JEDAN obj	
    if (is_setArray(g_objektiNaTrasi, 1)) {
        for (let i = 0; i < g_objektiNaTrasi.length; i++)
            for (let j = 0; j < g_objektiNaTrasi[i].length; j++)
                if (g_objektiNaTrasi[i][j].y > max_Y)
                    max_Y = g_objektiNaTrasi[i][j].y;
    }

    //debug("min_Y "+min_Y);
    //debug("max_Y "+max_Y);

    /* globalna var	*/
    g_skalarX = 0.95 * getSirina() / (max_X - min_X);

    //ako je tlo ravna linija onda moram da pazim na max_Y-min_Y
    let dY;
    if (max_Y == min_Y)
        dY = 10;
    else
        dY = max_Y - min_Y;

    /* globalna var	*/
    g_skalarY = 0.95 * (getVisina() - 50) / dY;

    //debug("g_skalarX "+g_skalarX);
    //debug("g_skalarY "+g_skalarY);

}

/**
 * Prilagodjavanje atributa niza objekata po razmeri za X i Y
 * @global g_minY
 * @param {Objeci[]} nizObj koji u sebi sadrze .x i .y atribute
 * @returns {Objeci[]} nizObj_rel sa skaliranim vrednostima .x .y .h atributa
 */
function skalirajNizObj(nizObj) {
    if (!nizObj)
        return;

    // kreiramo potpuno nov niz, bez reference na pocentni niz
    let nizObj_rel = JSON.parse(JSON.stringify(nizObj));

    // koristimo vec izracunatu g_minY i g_minX
    // ona je ista i za stubove, tlo, lancanicu...
    for (let i = 0; i < nizObj_rel.length; i++){
        nizObj_rel[i].x -= g_minX;
        nizObj_rel[i].y -= g_minY;
    }
    for (let i = 0; i < nizObj.length; i++) {
        nizObj_rel[i].x = kovnertAtribut_aps_to_rel("x", nizObj_rel[i].x);
        nizObj_rel[i].y = kovnertAtribut_aps_to_rel("y", nizObj_rel[i].y);
    }

    if (nizObj_rel[0]) {

        // proveravamo da li objekti imaju i h atribut
        let flag = (nizObj_rel[0]).hasOwnProperty("h");
        if (flag) {
            for (let i = 0; i < nizObj.length; i++)
                nizObj_rel[i].h = kovnertAtribut_aps_to_rel("h", nizObj[i].h);
            // za visinu stuba ne treba prilagodjavanje		
        }

    }

    return nizObj_rel;
}

/**
 * skalira zadatu vrednost po x koord
 * @global g_skalarX
 * @param {Number} varX u [m]
 * @returns {Number} varX_rel u [pix]
 */
function skaliraj_X(varX) {
    let varX_rel = g_skalarX * varX;
    return varX_rel;
}

/**
 * skalira zadatu vrednost po y koord
 * @global g_skalarY
 * @param {Number} varY u [m]
 * @returns {Number} varY_rel u [pix]
 */
function skaliraj_Y(varY) {
    let varY_rel = g_skalarY * varY;
    return prilagodiYosi(varY_rel);
}

/**
 * skalira zadatu Visini po y koord
 * @global g_skalarY
 * @param {Number} varH u [m]
 * @returns {Number} varH_rel u [pix]
 */
function skaliraj_H(varH) {
    let varH_rel = g_skalarY * varH;
    // za H nam ne traba da prilagodjavamo y osi jer vrednost od h dodajemo
    // na vec prilagodjenu vrednost y
    return varH_rel;
}

/**
 * prilagodjava y koordinatu prikazu u web browseru
 * jer browser racuna y koordinatu odozgo-na-dole
 * @param {Number} varY u [pix]
 * @global g_HEDER_HEIGHT
 * @returns {Number} varY u [pix] prilagodjeno prikazu na stranici
 */
function prilagodiYosi(varY) {
    // 50 je visina hedera
    return getVisina() - varY - g_HEDER_HEIGHT;
}

/**
 * skalira unaza zadatu vrednost po x koord
 * @global g_skalarX
 * @param {Number} varX_rel u [pix]
 * @returns {Number} varX u [m]
 */
function skaliraj_X_unazad(varX_rel) {
    let x = varX_rel / g_skalarX;
    return x;
}

/**
 * skalira unaza zadatu vrednost po y koord
 * @global g_skalarY
 * @param {Number} varY_rel u [pix]
 * @returns {Number} varY u [m]
 */
function skaliraj_Y_unazad(varY_rel) {
    // prvo vracamo unazad (ista f_ja!) prilogodjavanje ekranu
    let y = prilagodiYosi(varY_rel);
    y = y / g_skalarY;
    y += g_minY;
    return y;
}

/**
 * skalira unaza zadatu vrednost Visine po y koord
 * @global g_skalarY
 * @param {Number} varH_rel u [pix]
 * @returns {Number} h u [m]
 */
function skaliraj_H_unazad(varH_rel) {
    // za H nam ne treba prilagodiYosi	
    let h = varH_rel / g_skalarY;
    return h;
}
/////////////////////////////////////////////////////////////////

/**
 * crtanje SVG polyline - koristim za crtanje tacaka tla i lancanice
 * @param {Point[]} nizObj_rel niz Pointa sa relativnim vred. u [pix]
 * @param {String} stroke boja linije
 * @param {String} fill boja kojom se filuje linija (bitno za objekte)
 * @global g_svgObj
 * @returns {SVGPolylineElement} SVG polyline
 * @todo omoguci crtanje markera
 */
function crtaj_poliliniju(nizObj_rel, stroke, fill) {
    if (!nizObj_rel)
        return;
    //***8.8.2018. - problem da upisem markere nad polilinijom
    //	$("svg")[0].append('<defs><marker id="marker_dot" viewBox="0 0 10 10" refX="5" refY="5"  markerWidth="5" markerHeight="5"> <circle cx="5" cy="5" r="5" fill="red" /> </marker> </defs>');
    //	
    //	var mark = g_svgObj.marker(10, 10, function(add) {
    //	  add.rect(10, 10);
    //	});

    //	polyLinija.marker('mid', mark);
    let nizTacaka_SVG = konverzija_nizObj_nizSVG(nizObj_rel);
    let width = 1;
    if (nizObj_rel.length == 2) width = 3; // u pitanju je stari stub ispod DV
    let polyLinija = g_svgObj.polyline(nizTacaka_SVG).fill(fill).stroke({ color: stroke, width: width, linecap: 'round', linejoin: "round" });

    return polyLinija;
}

/**
 * crtanje pojedinacnih objekata
 * @param {Array} niz_rel niz sa tackama kojima se definise obj
 * @global g_nizTacakaTlo_rel
 */
function crtajObjekat(niz_rel) {
    if (!niz_rel)
        return;

    // pomocni niz u koji dodajemo dozemne tacke polilinije    
    let pomNiz = JSON.parse(JSON.stringify(niz_rel));

    // dodajemo  pocetnu i krajnju tacku kako bi spojili kraj objekta sa tlom
    let point_start = [];
    point_start.x = pomNiz[0].x;
    point_start.y = linearnaInterpolacija(point_start.x, izdvojNizIzObjekata("x", g_nizTacakaTlo_rel),
        izdvojNizIzObjekata("y", g_nizTacakaTlo_rel));

    pomNiz.unshift(point_start);  // upisujemo na pocetak niza

    if (niz_rel.length > 1) { // ako je 1 onda je u pitanju stub pa nam ovo ne treba
        let point_end = [];
        point_end.x = pomNiz[pomNiz.length - 1].x;
        point_end.y = linearnaInterpolacija(point_end.x, izdvojNizIzObjekata("x", g_nizTacakaTlo_rel),
            izdvojNizIzObjekata("y", g_nizTacakaTlo_rel));
        pomNiz.push(point_end); // upisujemo na kraj niza       
    }

    // crtamo polilijiju sa bojom za fill
    crtaj_poliliniju(pomNiz,"brown", "black");    
}

/**
 * crtanje SVG linije - stuba
 * @param {StubnoMesto} subnoMesto sa relativnim vred. u [pix]
 * @global g_svgObj, g_nizTacakaTlo_rel
 * @returns {SVGLineElement} SVG linija
 * @todo omoguci crtanje oznake iznad st.mesta
 */
function crtaj_stub(stub) {
    if (!stub)
        return;
    let x = stub.x;
    let y = stub.y;
    let h = stub.h;
    let idTag = "stub-" + stub.oznaka;
	/*let gradient = g_svgObj.gradient('linear', function(stop) {
		stop.at(0, '#333')
		stop.at(1, '#fff')
	});*/
    let linija = g_svgObj
        .line(x, y - h, x, y)
        .stroke({ color: 'black', width: 3, linecap: 'round', opacity: 0.7 })
        .id(idTag)
					/*.fill(gradient)*/;

    $("#" + idTag).html("<title>StubnoMesto:" + stub.oznaka + "<!title>");
    controlStubDrag(stub, linija);
    //$("#"+idTag).click(function(){alert("opaaaaaaaaaaaa");});
    return linija;
}

/**
 * crtanje ordinate - vertikale za prikaz koordinata
 * @global g_svgObj, g_ordinata
 */
function crtaj_Ordinatu() {
    // 2*getVisina da bi bio siguran da zauzima ceo ekran
    g_ordinata = g_svgObj.line(0, 0, 0, 2 * getVisina()).stroke({ color: "black", opacity: 0.6, width: 1 });
}

/**
 * kontrola x,y vrednosti StubnogMesta pri drag and drop - spoljasnja biblioteka
 * @param {StubnoMesto} stubRel-relativno cije koord. kontrolisemo
 * @param {SVGLineElement} linija stuba kojoj deljujemo listenere
 * @global g_nizTacakaTlo_rel
 * @returns {void}
 */
function controlStubDrag(stubRel, linija) {
    let xmax = g_nizTacakaTlo_rel[g_nizTacakaTlo_rel.length - 1].x;
    let xmin = g_nizTacakaTlo_rel[0].x;
    let idTag = "stub-" + stubRel.oznaka;

    // onemogucujemo da se stubRel pomeri van granica zat.polja
    linija.draggable().on('dragmove', function (e) {
        e.preventDefault();
        let x = e.detail.p.x;
        if (x < xmin) {
            x = xmin;
        }
        else if (x > xmax) {
            x = xmax;
        }
        this.move(x, e.detail.p.y);
    });

    // nakon pomeranja radimo update x,y vrednosti stuba
    linija.draggable().on('dragend', function (e) {
        e.preventDefault();
        //let index;
        let x = e.detail.p.x;
        if (x < xmin) {
            x = xmin;
        }
        else if (x > xmax) {
            x = xmax;
        }
        else if (g_snap) {
            //index = indexSusednog_ManjegClanaNiza(x,g_nizTacakaTlo_rel);
            // ne mora za prvu i poslednju tacku
            for (let i = 1; i < g_nizTacakaTlo_rel.length - 1; i++) {
                // da bi lepo pozicioniralo kada su susedne tacke tla dosta blizu 
                if (Math.abs(x - g_nizTacakaTlo_rel[i].x) < g_snap_osetljivost
                    && Math.abs(x - g_nizTacakaTlo_rel[i + 1].x) > g_snap_osetljivost) {
                    x = g_nizTacakaTlo_rel[i].x;
                    break;
                }
            }
        }
        let y = linearnaInterpolacija(x, izdvojNizIzObjekata("x", g_nizTacakaTlo_rel),
            izdvojNizIzObjekata("y", g_nizTacakaTlo_rel));

        // racuna se i ovaj deo - nesto ? zbog spoljasnje biblioteke za drag										
        let duzinaLinijeStuba = $("#" + idTag).attr("y2") - $("#" + idTag).attr("y1");
        this.move(x, y - duzinaLinijeStuba);

        // radiomo update relativnih vred stuba
        updateStubAtribut(stubRel, "x", x);
        updateStubAtribut(stubRel, "y", y);

        // radimo update apsolutnih vred istog stuba
        let x_aps = kovnertAtribut_rel_to_aps("x", x);
        let y_aps = kovnertAtribut_rel_to_aps("y", y);
        let stub_aps = g_nizStubova.find(function (obj) { return obj.oznaka === stubRel.oznaka; });
        updateStubAtribut(stub_aps, "x", x_aps);
        updateStubAtribut(stub_aps, "y", y_aps);

        crtajStuboveNaMapi();
    });

}

/**
 * brise sve elemente nacrtane u g_svgObj
 * @ global g_svgObj
 */
function obrisiCrtanje() {
    //$(g_svgObj).empty(); // ne moze ovako
    $("#drawing svg").empty();
}

/**
 * start/restart crtanja svih svg elemenata
 * @global g_nizTacakaTlo, g_nizStubova, g_nizTacakaLanc
 * @global g_nizTacakaTlo_rel, g_nizStubova_rel, g_nizTacakaLanc_rel
 * @returns {void}
 */
function pokreniCrtanje() {
    obrisiCrtanje();
    nadjiRazmere();

    //min dve tacke
    if (is_setArray(g_nizTacakaTlo, 2)) {
        g_nizTacakaTlo_rel = skalirajNizObj(g_nizTacakaTlo);
        crtaj_poliliniju(g_nizTacakaTlo_rel, 'brown', "none");
        crtaj_Ordinatu();
    }

    //min jedan objekat
    if (is_setArray(g_objektiNaTrasi, 1)) {
        // a unutar objekta moze da bude i samo jedan Point (stub)

        //resetujemo var jer joj dole ne dodeljujem vrednost preko funkcije
        // vec direktno radiomo push u nju
        g_objektiNaTrasi_rel = [];
        let pomNiz = [];
        for (let i = 0; i < g_objektiNaTrasi.length; i++) {
            pomNiz = JSON.parse(JSON.stringify(g_objektiNaTrasi[i]));
            g_objektiNaTrasi_rel.push(skalirajNizObj(pomNiz));
            crtajObjekat(g_objektiNaTrasi_rel[i]);
        }

        //ovo nam treba zbog spanON kada ocitavam x,y koordinate pri mouseover
        kreirajNizStacObj();

    }

    // min jedan stub
    if (is_setArray(g_nizStubova, 1)) {
        //prilagodiStuboveRazmeri();
        g_nizStubova_rel = skalirajNizObj(g_nizStubova);
        nacrtajStubove(g_nizStubova_rel);
    }

    //min dve tacke
    if (is_setArray(g_nizTacakaLanc, 2)) {
        g_nizTacakaLanc_rel = skalirajNizObj(g_nizTacakaLanc);
        crtaj_poliliniju(g_nizTacakaLanc_rel, "blue", "none");
    }
    
    if (is_setTlo()) {
        $("#tableNo1 .col2 input").attr("min",g_nizTacakaTlo[0].x);
        $("#tableNo1 .col2 input").attr("max",g_nizTacakaTlo[g_nizTacakaTlo.length-1].x);
    }
    
}
////////////////////////////////////////////////////////////////
/**
 * Konverzija vrednosti atributa iz rel u aps jedinice
 * @param {String} atribut "x" ili "y" ili "h"
 * @param {Number} varRel vrednost u [px]
 * @returns {Number} vrednost u [m]
 */
function kovnertAtribut_rel_to_aps(atribut, varRel) {
    let varAps;
    switch (atribut) {
        case "x":
            varAps = skaliraj_X_unazad(varRel);
            break;
        case "y":
            varAps = skaliraj_Y_unazad(varRel);
            break;
        case "h":
            varAps = skaliraj_H_unazad(varRel);
            break;
        default:
            varAps = g_greskaFlag;
    }
    // zaokruzivanje koristi samo za prikaz
    //varAps = round(parseFloat(varAps));
    return varAps;
}

/**
 * Konverzija vrednosti atributa iz aps u rel jedinice
 * @param {String} atribut "x" ili "y" ili "h"
 * @param {Number} varRel vrednost u [m]
 * @returns {Number} vrednost u [px]
 */
function kovnertAtribut_aps_to_rel(atribut, varAPS) {
    let varRel;
    switch (atribut) {
        case "x":
            varRel = skaliraj_X(varAPS);
            break;
        case "y":
            varRel = skaliraj_Y(varAPS);
            break;
        case "h":
            varRel = skaliraj_H(varAPS);
            break;
        default:
            varRel = g_greskaFlag;
    }
    //zaoukruzivanje koristi samo za prikaz
    //varRel = round(parseFloat(varRel));
    return varRel;
}

/**
 * Setuje atribut stuba na zadatu vrednost
 * @param {StubnoMesto} stub rel ili abs
 * @param {String} atribut "x","tip"...
 * @param {Number|String} vrednost 125 , "9/1000"
 */
function updateStubAtribut(stub, atribut, vrednost) {
    stub[atribut] = vrednost;
}

/**
 * validacija forme za proracun lancanice
 * zahteva se da bude unesene 
 * -bar dve tace za tlo
 * -bar dva stuba
 * @returns {Boolean} true ako je ovo gore ok, false+alert ako nije
 */
function validirajZahtev_lanc() {
    // zatvara dropdown meni ako je prikazan
    if ($("#lancDropdown").hasClass("show"))
        $("#lancDropdown button:first-of-type").click();

    if (is_setTlo() && is_setStubMesta()) {
        $("#formaLanc input[name='tackeTla']").val(JSON.stringify(g_nizTacakaTlo));
        $("#formaLanc input[name='stubovi']").val(JSON.stringify(g_nizStubova));
        return true;
    }
    else {
        let poruka = "Prethodno morate uneti:";
        if (!is_setTlo())
            poruka += "\n - tacke uzduznog profila trase";
        if (!is_setStubMesta())
            poruka += "\n - podatke o minimum dva stubna mesta";
        alert(poruka);
        return false;
    }
}

/**
 * validacija upita za kreiranje pdf fajla
 * zahteva 
 * -validirajZahtev_lanc
 * -is_setLancanica
 * @returns {Boolean} true ako je ovo gore ok, false+alert ako nije
 */
function validirajZahtev_pdf() {
    // zatvara dropdown meni ako je prikazan
    if (!validirajZahtev_lanc())
        return false;

    else if (!is_setLancanica()) {
        let poruka = "Lancanica mora biti nacrtana!";
        alert(poruka);
        return false;
    }
    else {
        $("#formaLanc input[name='tackeLanc']").val(JSON.stringify(g_nizTacakaLanc));
        return true;
    }
}


/**
 * validacija forme za sessija saveAS - nov proj - SQLinsert
 * @returns {Boolean} true ako je ucitan niz tlo|stub|lanc, false ako nije
 */
function validirajZahtev_sesijaSaveAS() {
    // zatvara dropdown meni ako je prikazan
    if ($("#lancDropdown").hasClass("show"))
        $("#lancDropdown button:first-of-type").click();

    let boolFlag = false;

    if (is_setTlo()) {
        $("#formaLanc input[name='tackeTla']").val(JSON.stringify(g_nizTacakaTlo));
        boolFlag = true;
    }
    if (is_setArray(g_objektiNaTrasi, 1)) {
        $("#formaLanc input[name='objektiTrase']").val(JSON.stringify(g_objektiNaTrasi));
        boolFlag = true;
    }
    if (is_setArray(g_nizStubova, 1)) {
        $("#formaLanc input[name='stubovi']").val(JSON.stringify(g_nizStubova));
        boolFlag = true;
    }
    if (is_setLancanica()) {
        $("#formaLanc input[name='tackeLanc']").val(JSON.stringify(g_nizTacakaLanc));
        boolFlag = true;
    }

    if (!boolFlag) {
        let poruka = "Nema sta da se sacuva";
        alert(poruka);
    }


    return boolFlag;
}


/**
 * validacija forme za sessija save - odnosno SQLupdate
 * @returns {Boolean} true ako je validirajZahtev_sesijaSaveAS() && definisan je projID
 */
function validirajZahtev_sesijaUpdate() {

    /// validira da li je unet niz tla ili niz stub ili niz lanc
    if (!validirajZahtev_sesijaSaveAS())
        return false;

    let projIdPom = $("#formaLanc input[name='projId']").val();
    if (typeof projIdPom == "number")
        return true;
    else if (typeof projIdPom == "string") {
        if (typeof parseInt(projIdPom) == "number")
            return true;
    }
    else
        return false;



    return true;
}

/**
 * Zadati string ("323.4 3423.34") konvertuje u brojeve i upisujemo u g_nizTacakaTlo(x,y)
 * ili u g_objektiNaTrasi
 * @global g_nizTacakaTlo ili g_objektiNaTrasi
 * @param {String} string iz kojeg izvlacimo brojeve
 * @param {Number} maxBrojeva int - broj brojeva koji izvlacimo iz stringa (-1 ako nema ogranicenja)
 */
function getBrojeveIzStringa(string, maxBrojeva) {
    string = string.trim();

    // prazan string
    if (string == "") { return g_greskaFlag; }
    // prvi karakter nije broj, verovatno je u pitanju komentar pa ne vraca gresku
    if (!Number.isInteger(parseInt(string.substring(0, 1)))) { return; }

    let nizStringova = string.split(" "); // delimo na pojedinacne stringove
    let rbStringa = 0;
    let parsiraneVrednosti = [];
    for (let n = 0; n < nizStringova.length; n++) {
        nizStringova[n].trim(); // prvo trim
        if (nizStringova[n] != "") {
            parsiraneVrednosti[rbStringa] = round(parseFloat(nizStringova[n]));
            rbStringa++;
        }
        // ako su nam potrebna samo 2 broja npr.(x,y), onda prekidamo petlju
        if (rbStringa == maxBrojeva)
            break;
    }

    if (maxBrojeva == -1) { // kada ucitavam objekte duz trase tla
        let nizTacaka = [];
        // ovde upisujemo niz tacaka koje se odnose na jedan isti objekat
        for (let i = 0; i < parsiraneVrednosti.length - 1; i = i + 2)
            nizTacaka.push(new Point(parsiraneVrednosti[i], parsiraneVrednosti[i + 1]));

        // taj objekat upisujemo u g_objektiNaTrasi    
        g_objektiNaTrasi.push(nizTacaka);
    }
    else {  // kada ucitavamo tacke tla      
        // provera, ako su ucitana tacno dva broja onda je ok da se napravi nova tacka
        if (parsiraneVrednosti.length == maxBrojeva) {
            g_nizTacakaTlo.push(new Point(parsiraneVrednosti[0], parsiraneVrednosti[1]));
        }
    }

}

/**
 * dodeljuje listener za brisanje reda u dialogu stubova
 * - kada se kreira novi red, kreira se i button, dodeljuje mu se ovaj listener
 */
function buttonEventZaBrisanje() {
    let nizButtona = $("#tableNo1 .obrisiRed");
    for (let i = 0; i < nizButtona.length; i++) {
        $(nizButtona[i]).click(function () {
            if ($("#tableNo1 .obrisiRed").length < 2)
                return;// onemoguceno brisanje ako imamo samo 1 red
            else
                $(nizButtona[i]).closest("tr").remove();
        });
    }

}

/**
 * funkcija za ucitavanje stubnih mesta iz dialoga
 * - na kraju pokrece pokreniCrtanje()
 * @global g_nizStubova
 */
function ucitajStuboveDialog() {
    g_nizStubova = [];
    let sviRedovi = $("#tableNo1 tbody tr");
    let sviInputiRed;
    let bool_flag=true;
    for (let i = 0; i < sviRedovi.length; i++) {
        sviInputiRed = $(sviRedovi[i]).find("input");
        let oznaka = $(sviInputiRed[0]).val();
        let x = parseFloat($(sviInputiRed[1]).val());
        if (x< g_nizTacakaTlo[0].x || x>g_nizTacakaTlo[g_nizTacakaTlo.length-1].x) {
            //stacionaza stuba nije ispravna
            alert("Za stubno mesto "+oznaka+" je uneta ne odgovaraujca stacionaza - stubno mesto je zanemareno! ");
            bool_flag=false;
            continue;
        }
        if (i!=0) {
            let inputX = $(sviRedovi[i-1]).find("input")[1];//x stacionaza
            if ( x < parseFloat(inputX.value) ){
                alert("Za stubno mesto "+oznaka+" je uneta manja stacionaza od stac. prethodnog stuba - stubno mesto je zanemareno! ");
                bool_flag=false;
                $(sviInputiRed[1]).focus();
                continue;
            }            
        }
        let h = parseFloat($(sviInputiRed[2]).val());
        if (h<1 || h>50) {
            //visina vesanja prov nije ispravna
            alert("Za stubno mesto "+oznaka+" je uneta ne odgovaraujca visina vesanja provodnika [min 1m - max50m] - stubno mesto je zanemareno!");
            bool_flag=false;
            continue;
        }
        let tip = $(sviInputiRed[3]).val();
        let konzola = $(sviInputiRed[4]).val();
        let y = linearnaInterpolacija(x, izdvojNizIzObjekata("x", g_nizTacakaTlo),
            izdvojNizIzObjekata("y", g_nizTacakaTlo));
        g_nizStubova.push(new StubnoMesto(oznaka, x, y, h, tip, konzola));
    }
    
    crtajStuboveNaMapi();

    pokreniCrtanje();

    return bool_flag;
}

/**
 * crta sve stubove iz zadatog niza
 * @param {StubnoMesto} nizStubovaRel 
 */
function nacrtajStubove(nizStubovaRel) {
    if (!nizStubovaRel)
        return;

    for (let i = 0; i < nizStubovaRel.length; i++) {
        crtaj_stub(nizStubovaRel[i]);
    }
}
//////////////////////////////////////////////////////////////
/**
 * niz animacija za dialog
 */
const nizAnimacijaDialoga = ["blind",/*"bounce",*/"clip", "drop", "blind", "explode", "fade", "fold", "highlight", "puff",/*"pulsate",*/"scale", "shake", "size", "slide"/*,"transfer"*/];

/**
 * Vraca random index iz niza
 * @param {Array} niz
 * @returns {Number} int index niza
 */
function getRandIndexFromNiz(niz) {
    randN = Math.floor(Math.random() * niz.length);
    return niz[randN];
}

////////////////////////////////////////////////////////////////
/**
 * from niz Object -> to niz Point  -- za lancanicu
 * @todo validairaj
 * @param {Object[]} nizObj sa atriburima x, y
 * @returns {Point[]} niz Pointa (x,y)
 */
function kreirajNizPointaLancanice(nizObj) {
    if (!nizObj || nizObj.length < 2) {
        return; /* todo dodaj kontrolni return */
    }

    let nizPointa = [];
    nizObj.forEach(function (element) {
        nizPointa.push(new Point(element.x, element.y));
    });
    return nizPointa;
}

////////////////////////////////////////////////////////////////
/**
 * translacija ordinate na novu x koordinatu
 * @param {Number} x_aps u [m]
 * @global g_ordinata
 */
function translirajOrdinatu(x_aps) {
    let x_rel = kovnertAtribut_aps_to_rel("x", x_aps);
    if ($("#" + g_ordinata.id()).css("display") !== "none") {
        g_ordinata.translate(x_rel, 0);
    }
}


/**
 * na osnovu x koord vrsi upis y,h u futer
 * @param {Number} xaps x koord u [m]
 * @global g_nizTacakaTlo
 */
function upisiYHfooter(xaps) {
    // upisivanje koord u futer
    let yaps;
    let index = tackaPripadaObjTrase(xaps);

    if (index !== false) // visina objekta na trasi
        yaps = linearnaInterpolacija(xaps, izdvojNizIzObjekata("x", g_objektiNaTrasi[index]),
            izdvojNizIzObjekata("y", g_objektiNaTrasi[index]));
    else // visina tla
        yaps = linearnaInterpolacija(xaps, izdvojNizIzObjekata("x", g_nizTacakaTlo),
            izdvojNizIzObjekata("y", g_nizTacakaTlo));

    $("#y_koor").html(round(yaps));

    if (is_setLancanica()) {
        if (xaps >= g_nizTacakaLanc[0].x && xaps <= g_nizTacakaLanc[g_nizTacakaLanc.length - 1].x) {
            haps = linearnaInterpolacija(xaps, izdvojNizIzObjekata("x", g_nizTacakaLanc),
                izdvojNizIzObjekata("y", g_nizTacakaLanc));
            if (haps - yaps < 0)
                $("#h_koor").html("-1"); //vise kao neki flag              
            else
                $("#h_koor").html(round(haps - yaps));
        }
        else {
            $("#h_koor").html("-"); //vise kao neki flag 
        }

    }
}
////////////////////////////////////////////////////////////////
/**
 * Provereva da li je definisan niz i da li je duzina >= brojElemenata
 * @param {Array} niz 
 * @param {Number} brojElemenata koji treba niz da ima kako bi vratili true
 * @returns {Boolean} 
 */
function is_setArray(niz, brojElemenata) {
    if (typeof niz !== 'undefined' && niz instanceof Array) {
        // minimum dva clana niza
        if (niz.length >= brojElemenata) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/**
 * Provereva da li su definisane bar 2 tacke tla
 * @global g_nizTacakaTlo,g_nizTacakaTlo_rel
 * @returns {Boolean} 
 */
function is_setTlo() {
    return is_setArray(g_nizTacakaTlo, 2) && is_setArray(g_nizTacakaTlo_rel, 2);
}

/**
 * Provereva da li su definisana bar 2 st.mesta
 * @global g_nizStubova,g_nizStubova_rel
 * @returns {Boolean} 
 */
function is_setStubMesta() {
    return is_setArray(g_nizStubova, 2) && is_setArray(g_nizStubova_rel, 2);
}

/**
 * Provereva da li su definisne bar 2 tacke lanc
 * @global g_nizTacakaLanc,g_nizTacakaLanc_rel
 * @returns {Boolean} 
 */
function is_setLancanica() {
    return is_setArray(g_nizTacakaLanc, 2) && is_setArray(g_nizTacakaLanc_rel, 2);
}
///////////////////////////////////////////////////////////////////////
/**
 * resetuje nizove i brise vrednosti u input[type='hidden']
 * @global g_nizTacakaTlo, g_nizStubova, g_nizTacakaLanc, g_nizTacakaTlo_rel, g_nizStubova_rel, g_nizTacakaLanc_rel = [];
 */
function resetGlobNizova() {
    g_nizTacakaTlo = [];
    g_nizStubova = [];
    g_nizTacakaLanc = [];
    g_nizTacakaTlo_rel = [];
    g_nizStubova_rel = [];
    g_nizTacakaLanc_rel = [];
    g_objektiNaTrasi = [];
    g_objektiNaTrasi_rel = [];
    g_nizStacObj = [];

    // resetujemo sve podatke u hidden inputima
    /* debug todo - imam problem posle sa save proj
    $("input[type='hidden']").each(function () {
        $(this).val("");
    }); */

    // resetujemo input za obj.ispod DV da bi mogli da ponovo
    // odaberemo isti fajl
    $("#file_obj").val("");
}


/**
 * Funkcija vrsi submit podataka ka serveru - u zavisnosti od selektora
 * @param {String} selector updateProject, saveAsProject, getLancanica
 */
function submitData(selector) {

    let url_controler; // url do kontrolera
    let action_controler; // ackija koja se od kontrolera trazi

    // save - odnosno update postojeceg projekta
    if (selector === 'updateProject') {
        if (!validirajZahtev_sesijaUpdate())
            return;
        else {
            url_controler = "../control/Controller_projekti.php";
            action_controler = "update";
        }
    }
    // save novog projekta   
    else if (selector === 'saveAsProject') {
        if (!validirajZahtev_sesijaSaveAS())
            return;
        else {
            url_controler = "../control/Controller_projekti.php";
            action_controler = "save";
        }
    }
    // crtanje lancanice 
    else if (selector === 'getLancanica') {
        if (!validirajZahtev_lanc())
            return;
        else {
            url_controler = "../control/Controller_js.php";
            action_controler = "getLancanica";
        }
    }
    // crtanje lancanice 
    else if (selector === 'getPdf') {
        if (!validirajZahtev_pdf()) // validacija za pdf
            return;
        else {
            url_controler = "../control/Controller_alati.php";
            action_controler = "getPdf";            
        }
    }
    else {
        return;
    }

    // mora da sadrzi ime kako bi bio kreiran pdf
    let kontrolaNazivaProj = $("#formaLanc input[name='projName']").val();
    if ( ! kontrolaNazivaProj) {
        $("#formaLanc input[name='projName']").val("uzduzni profil");
    }
    
    // podaci koje saljemo serveru
    let submitData = $("#formaLanc").serialize() + "&action=" + action_controler;

    
    if (selector === 'getPdf') { 
        // u pitanju je zahtev za download pdf
        downloadFile_byPost(url_controler,submitData,"uzduzni profil.pdf");
    }
    else{ 
        // svi ostali zaxtevi koriste jquey a j a x
        $.ajax({ /* save/updade proj i  getLancanica */
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: $("#formaLanc").serialize() + "&action=" + action_controler,
            success: function (data) {
                if (data.status != "success") {                    
                    alert(data.status + " - " + data.msg);
                    return;
                }

                if (selector === 'getLancanica') {
                    g_nizTacakaLanc = kreirajNizPointaLancanice(data.data);
                    $("#formaLanc input[name='jsID']").val(data.msg); 

                    obrisiSveLejere();
                    pokreniCrtanje();
                    alert("Uspesno kreirana lancanica");
                    
                }
                else if (selector === 'updateProject') {                    
                    alert("uspesno sacuvane izmene u projektu");

                }
                else if (selector === 'saveAsProject') {                    
                    alert("uspesno sacuvan nov projekat");

                }
            } // end  su cce s
        }); // end a j a x
    }

}

/**
 * validacija naziva projekta - text input u modalu
 */
function validirajZahtev_projName() {
    let text = $('#modalTextInput input[name="projName"]').val();
    if (typeof text !== "string")
        return false;
    else if (text.length < 1)
        return false;
    else {
        g_projName = text;
        // upisujemo u hidden input kako bi mogli da sacuvamo proj		
        $("#formaLanc input[name='projName']").val(g_projName);
        /* $("#formaLanc input[name='projId']").val(?????); */
        return true;
    }

}

/**
 * Brisanje podataka o projektu unutar dialoga 
 */
function deleteDialogInfo(){
    $(".dialogProjData").each(function() {
        $(this).html( "-" );
      });
}

/**
 * Ucitava projekat koji smo odabrali preko openProj dialoga
 * @param {Object} projData 
 */
function loadSelectedProj(projData) {
    try {
        $("#formaLanc select[name='provodnik']").val(projData.oznakaProv);
        $("#formaLanc input[name='temperatura']").val(projData.temperatura);
        $("#formaLanc select[name='odo']").val(projData.odo);
        $("#formaLanc input[name='naprezanje']").val(projData.naprezanje);

        g_nizTacakaTlo = JSON.parse(projData.tackeTlaJson);
        $("#formaLanc input[name='tackeTla']").val(JSON.stringify(g_nizTacakaTlo));
        g_objektiNaTrasi = JSON.parse(projData.objektiTraseJson);
        $("#formaLanc input[name='objektiTrase']").val(JSON.stringify(g_objektiNaTrasi));
        g_nizStubova = JSON.parse(projData.stubnaMestaJson);
        $("#formaLanc input[name='stubovi']").val(JSON.stringify(g_nizStubova));
        g_nizTacakaLanc = JSON.parse(projData.tackeLancJson);
        $("#formaLanc input[name='tackeLanc']").val(JSON.stringify(g_nizTacakaLanc));

        $("#formaLanc input[name='projName']").val(projData.naziv);
        g_projName = projData.naziv;

        $("#formaLanc input[name='projId']").val(projData.id);
        $("#formaLanc input[name='jsID']").val(projData.jsID);

        $("input[type='file']").val("");

        $("#tableNo1 .col2 input").each(function () {
            $(this).attr("min",g_nizTacakaTlo[0].x);
            $(this).attr("max",g_nizTacakaTlo[g_nizTacakaTlo.length-1].x);
        });
    } catch (error) {
        alert("Odabrani projekat ne moze da se ucita");
        return;
    }

    obrisiSveLejere();
    pokreniCrtanje();
}

/**
 * vraca false ili r.b objekta kojem pripada stacionaza x_aps
 * @param {Number} x_aps x koordinata u [m]
 * @returns {false|Number}
 */
function tackaPripadaObjTrase(x_aps) {
    if (!g_objektiNaTrasi) return false;

    let index = false;
    for (let i = 0; i < g_objektiNaTrasi.length; i++)
        if (x_aps >= g_objektiNaTrasi[i][0].x &&
            x_aps <= g_objektiNaTrasi[i][g_objektiNaTrasi[i].length - 1].x) {
            index = i;
            break;
        }

    return index;
}

/**
 * proracun i upis razdaljine dve latlong tacke u modalLatLong dialog
 */
function upisiRazdaljivuDialog() {
    let latA = $("input[name='latFirst']").val();
    let longA = $("input[name='longFirst']").val();  
    let latB = $("input[name='latLast']").val();
    let longB = $("input[name='longLast']").val(); 

    if (!latA || !longA || !latB  || !longB ) 
    return;

    let distance =  distanceLatLong([ latA,longA],[ latB,longB]);
    if (distance >= 3000) {
        distance = (distance/1000).toFixed(2) +' km';
    } else {
        distance = distance.toFixed(2) + ' m';
    }

    $('#modalLatLong strong').html(distance);
}

/**
 * vraca niz raspona
 * @global g_nizStubova
 * @returns float[]
 */
function getNizRaspona(){
    if (!is_setStubMesta()) {
        alert("moraju biti uneta bar dva stubna mesta");
        return false;
    }
    let nizRaspona = [];
    for (let i = 1; i < g_nizStubova.length; i++) {
        nizRaspona.push(round(g_nizStubova[i].x - g_nizStubova[i-1].x));
    }
    return nizRaspona;
}


/**
 * na osnovu karaktera u stringu radi toggle button-a u dialogu provodnika
 * @param {string} string c,e,d,s,o 
 */
function toggleProvButtons(string) {
    $("#copyProv").hide();
    $("#editProv").hide();
    $("#saveProv").hide();
    $("#deleteProv").hide();
    $("#cancelProv").hide();

    if (!string) return;

    let nizChar = string.split('');

    nizChar.forEach(char => {
        if (char.toLowerCase() == 's') $("#saveProv").show();
        else if (char.toLowerCase() == 'c') $("#copyProv").show();
        else if (char.toLowerCase() == 'e') $("#editProv").show();
        else if (char.toLowerCase() == 'd') $("#deleteProv").show();
        else if (char.toLowerCase() == 'o') $("#cancelProv").show();
    });
}

/**
 * funkcija koju koristimo pri a j a x error callback
 * @param {String} string poruka za consolu
 */
function ajax_error(string) {    
    alert("Neuspesna obrada zahteva - pokusajte opet kasnije!" );
    /*******todo izbrisi ovo ispod na serveru**********/
    //console.log(string);
    //console.error(string);
    console.warn(string);
    //console.debug(string);
    //console.trace(string);  
    /***************************************************/  
}

/**
 * funkcija koju koristimo pri a j a x beforeSend 
 * - pokretanje loadera
 */
function ajax_beforeSend() { 
    $('#loader-wrapper .loader-section').css("background", "transparent");
    $('body').removeClass('loaded');  
}

/**
 * funkcija koju koristimo pri a j a x beforeSend 
 * - stopiranje loadera
 */
function ajax_complete() { 
    $('body').addClass('loaded'); 
}

/**
* formiranje g_nizStacObj na osnnovu g_objektiNaTrasi
* @global g_objektiNaTrasi
* @global g_nizStacObj
* @returns {void} void
*/
function kreirajNizStacObj() { 
    if (! is_setArray(g_objektiNaTrasi, 1)) {
        g_nizStacObj = [];
    }
    for (let i = 0; i < g_objektiNaTrasi.length; i++) 
        for (let j = 0; j < g_objektiNaTrasi[i].length; j++) 
            g_nizStacObj.push(g_objektiNaTrasi[i][j].x);
}

/**
 * XMLHttpRequest POST zahtev za download fajla sa servera
 * @param {String} controler koji obradjuje zahter
 * @param {String} podaci podaci koje saljemo
 * @param {String} defaultFilename naziv sacuvanog fajla ukoliko server nije poslao naziv
 */
function downloadFile_byPost(controler, podaci, defaultFilename) {
    let req = new XMLHttpRequest();
        req.open("POST", controler, true);
        req.responseType = "blob";
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send(podaci);
        
        req.onload = function (event) {
            //var newBlob = new Blob([req.response], {type: "application/pdf"})
            let newBlob = req.response;

            let myReader = new FileReader();
            let data;
            myReader.onload = function(event){
                try {
                    data = JSON.parse(myReader.result);
                } catch (error) {
                    // ako ne moze da parsira onda je pdf kreiran i poslat
                    // ovde medjutim ne znamo da li je pdf ispravno kreiran ili
                    // je mozda samo download-ovan a ne moze da se otvori
                    data= [];
                } finally{
                    if (data.status) // ne postoji ako je u pitanju pdf
                        if (data.status != "success") // u pitanju je greska
                            alert(data.status);                    
                }

            };
            myReader.readAsText(newBlob);
            

            // Create a link pointing to the ObjectURL containing the blob.
            const dataUrl = window.URL.createObjectURL(newBlob);
            let link = document.createElement('a');
            link.style = "display: none";  
            link.href = dataUrl;

            // ime izlaznog fajla
            let filename = "";
            let disposition = req.getResponseHeader('Content-Disposition');
            if (disposition) { // trazimo ime definisano na serveru
                let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                let matches = filenameRegex.exec(disposition);
                if (matches !== null && matches[1])
                    filename = matches[1].replace(/['"]/g, '');                     
            }
            if (! filename) // ako ime nije definisano na serveru
                filename = defaultFilename;            

            link.download = filename;
            document.body.appendChild(link);
            link.click();
            setTimeout(function () {
                // For Firefox it is necessary to delay revoking the ObjectURL
                document.body.removeChild(link);
                window.URL.revokeObjectURL(dataUrl);
            }, 100);
        }
    
}


//openlayer mapa ima neki cudan koordinatni sistem - ovim ga prilagodjavam
/**
 * lat, long  sis -> opellayer sis
 * @param {Array} latLong[lat_long]
 * @returns webMercator_koor
 */
function prilagodiKoorSis_tacka(latLong) {
    let Long_Lat_koor = [];
    Long_Lat_koor.push(latLong[1]);
    Long_Lat_koor.push(latLong[0]);
    webMercator_koor = ol.proj.fromLonLat(Long_Lat_koor);
    return webMercator_koor;
}

/**
 * prolagodjava niz tacaka
 * @param {Point[]} latLongNiz 
 */
function prilagodiKoorSis_niz(latLongNiz) {
    let transformisanNiz = [];
    for (i = 0; i < latLongNiz.length; i++) {
        transformisanNiz.push(prilagodiKoorSis_tacka(
            [latLongNiz[i][0], latLongNiz[i][1]]
        ));
    }
    return transformisanNiz;
}

// kreiranje vektora tacaka na osnovu koga se crta lejer tacaka
function kreirajVektor_tacaka(nizTacaka) {
    let vektorTacaka = new ol.source.Vector({});
    for (i = 0; i < nizTacaka.length; i++) {
        let geometrijskaTacka = new ol.geom.Point(nizTacaka[i]);
        let oblikTacke = new ol.Feature({ geometry: geometrijskaTacka });
        vektorTacaka.addFeature(oblikTacke);
    }
    return vektorTacaka;
}
// kreiranje vektora linija na osnovu koga se crta lejer linija
function kreirajVektor_linije(nizTacaka) {
    let vectorLinije = new ol.source.Vector({});
    let geometrijskaLinija = new ol.geom.LineString(nizTacaka);
    let oblikLinije = new ol.Feature({
        name: "linija",
        geometry: geometrijskaLinija
    });
    vectorLinije.addFeature(oblikLinije);
    return vectorLinije;
}
/**
 * Rotira mapu za zadati ugao u rad
 * @param {Number} radiani 
 */
function rotirajMapu(radiani) {
    let mapView = g_map.getView();
    //get the current radians of the g_map's angle
    let currentRadians = g_map.getView().getRotation();
    //add 1.5 radians to the map's current getRotation() value
    mapView.setRotation(/*currentRadians + */radiani);
}
/**
 * Dodaje novi lejer na mapu
 * @param {ol.layer} lejer 
 */
function dodajLejerUMapu(lejer) {
    g_map.addLayer(lejer);
}

/**
 * Brise postojeci lejer sa mape na osnovu imena lejera
 * @param {String} name - naziv lejera koji brisemo 
 */
function obrisiLejerIzMape(name) {
    let layersToRemove = [];
    g_map.getLayers().forEach(function (lejer) {
        if (lejer.get('name') != undefined && lejer.get('name') === name) {
            layersToRemove.push(lejer);
        }
    });

    for (let i = 0; i < layersToRemove.length; i++) {
        g_map.removeLayer(layersToRemove[i]);
    }
}
/**
 * brise lejere iz mape:
 * -stacionaze
 * -trasa
 * -stub
 * -dv
 */
function obrisiSveLejere(){    
    obrisiLejerIzMape("stacionaze");
    obrisiLejerIzMape("trasa");
    obrisiLejerIzMape("stub");
    obrisiLejerIzMape("dv");
}
/**
 * kreiranje novog lejera za mapu
 * @param {ol.vector} vektor vektor tacaka lejera
 * @param {ol.style} stil stil linija
 * @param {string} name ime novog lejera
 * @returns {ol.layer} 
 */
function kreirajLejer(vektor, stil, name, zInd) {
    let lejer = new ol.layer.Vector({
        visible: true,
        source: vektor,
        style: stil
    });
    lejer.set('name', name);
    lejer.setZIndex(zInd)
    return lejer;
}

/**
 * Animacija zumiranja openlayer mape za zadatu lokaciju
 * - skinuto sa ol sajta
 */
function flyTo(location, done) {
    var duration = 2000;
    var zoom = g_map.getView().getZoom();
    var parts = 2;
    var called = false;
    function callback(complete) {
        --parts;
        if (called) {
            return;
        }
        if (parts === 0 || !complete) {
            called = true;
            done(complete);
        }
    }
    g_map.getView().animate({
        center: location,
        duration: duration
    }, callback);
    g_map.getView().animate({
        zoom: zoom - 2,
        duration: duration / 2
    }, {
            zoom: zoom,
            duration: duration / 2
        }, callback);
}

/**
 * racuna lat/long vrednost u radiajanima
 * @param {Number} degrees like 43.5458
 * @returns {Number} radians
 */
function deg_to_rad(degrees) {return degrees * (Math.PI / 180);}
/**
 * Racuna ugao izmedju dve latlong tacke za koji treba da rotiram mapu
 * da bi tacke prikazao u horizontali
 * @param {Array latlong} pointA 
 * @param {Array latlong} pointB 
 * @returns {Number} angle [rad]
 */
function angleFromCoordinate(pointA, pointB) {
    // projekcija tacke B na horizontalu tacke A
    let pointB0 = [pointB[0], pointA[1]];
    
    let c = distanceLatLong(pointA, pointB);//hipotenuza
    let a = distanceLatLong(pointA, pointB0);//krak od tacke a
    
    let radians = Math.acos(a/c); //ugao u radianima
    if ( isNaN(radians) ) return 0; // ako je c = 0, odnoso tacke A i B su iste

    let outVal = Math.PI/2 - radians; //fitovanje rezultata

    // u zavisnosti u kojem kavadrantu je krajnja tacka
    // XOR - tacno je ili samo I ili samo II
    if (pointA[0] > pointB[0] ? !(pointA[1] > pointB[1]) : (pointA[1] > pointB[1]) )
        outVal = - outVal; // alfa = - alfa
    

    return outVal;
}

// Earth radius at sea level
const ERe = 6378137.0; // [m] - equator
const ERp = 6356752.314; // [m] - pole
/**
 * get earth radius at given latitude
 * @param {Number} lat [deg]
 * @private dodatak 500m zbog visine
 * @see https://rechneronline.de/earth-radius/
 */
function get_radius_at_lat(lat) {
    lat = deg_to_rad(lat);
    let f1 = Math.pow((Math.pow(ERe, 2) * Math.cos(lat)), 2);
    let f2 = Math.pow((Math.pow(ERp, 2) * Math.sin(lat)), 2);
    let f3 = Math.pow((ERe * Math.cos(lat)), 2);
    let f4 = Math.pow((ERp * Math.sin(lat)), 2);

    //dodajem jos 500m nadmorske visine - neka srednja vrednost
    //??? ne utice mnogo na rezultate kasnije
    return Math.sqrt((f1 + f2) / (f3 + f4)) + 500;
}


/**
 * rastojanje izmedju dve latlong tacke u m
 * @param {Array latlong} pointA 
 * @param {Array latlong} pointB 
 * @returns {Number} distance [m]
 */
function distanceLatLong(pointA, pointB) {
    let lat1 = deg_to_rad(pointA[0]);
    let lat2 = deg_to_rad(pointB[0]);
    let dLat = (lat2 - lat1);

    let long1 = deg_to_rad(pointA[1]);
    let long2 = deg_to_rad(pointB[1]);    
    let dLong = (long2 - long1);

    
    //let R = 0.5 * (get_radius_at_lat(pointA[0]) + get_radius_at_lat(pointB[0]) ); // [m]
    let R = get_radius_at_lat(parseFloat(pointA[0]) + parseFloat(pointA[0] - pointB[0])); // [m]

    /**
     * Haversine Formula
     * @link https://andrew.hedges.name/experiments/haversine/
     */
    let coefA = Math.pow(Math.sin(dLat/2), 2)  +
            Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dLong / 2), 2);    
    let coefC = 2 * Math.atan2(Math.sqrt(coefA), Math.sqrt(1 - coefA));
    let distance = R * coefC;

  /*   
    console.log("lat1="+pointA[0]+" long1="+pointA[1]);
    console.log("lat2="+pointB[0]+" long2="+pointB[1]);
    console.log("distance = "+distance);
 */
    return distance;
}


/**
 * centriramo mapu na osnovu pocente i krajnje tacke
 * rotiramo u horizontalnu projekciju
 * crtamo lejera tacaka izmedju
 * @param {Array latlong} firstPoint 
 * @param {Array latlong} lastPoint 
 */
function centrirajMapu(firstPoint, lastPoint) {
    let lat = (firstPoint[0] + lastPoint[0]) / 2;
    let long = (firstPoint[1] + lastPoint[1]) / 2;

    if (lat === 0 && long ===0) {
        alert("Koordinate su indenticne!?");
        return;
    }

    let map_nizTacaka = [];
    map_nizTacaka.push(firstPoint, lastPoint);
    let map_nizTacaka_transformed = prilagodiKoorSis_niz(map_nizTacaka);

    let vektor_TacakaTrasa = kreirajVektor_tacaka(map_nizTacaka_transformed);
    let vektor_LinijeTrase = kreirajVektor_linije(map_nizTacaka_transformed);

    let lejer_TackeTrasa = kreirajLejer(vektor_TacakaTrasa, stilX, "stacionaze",25);
    let lejer_LinijaTrasa = kreirajLejer(vektor_LinijeTrase, stilTrase, "trasa",21);
    
    obrisiLejerIzMape("stacionaze");
    obrisiLejerIzMape("trasa");

    dodajLejerUMapu(lejer_TackeTrasa);
    dodajLejerUMapu(lejer_LinijaTrasa);

    crtajStuboveNaMapi();

    let ugao = angleFromCoordinate(firstPoint, lastPoint);

    flyTo(prilagodiKoorSis_tacka(
        [lat, long]),
        function () {
            let extent = lejer_TackeTrasa.getSource().getExtent();
            //g_map.getView().fit(extent, g_map.getSize());
            //let fitSize = g_map.getSize()[0];
            const elem = document.querySelector('svg:first-of-type polyline:first-of-type');
            let fitX = elem.getBoundingClientRect().width;
            //fitY = elem.getBoundingClientRect().height;
            g_map.getView().fit(extent, {size: [fitX,fitX], constrainResolution: false, padding: [0,25,0,-10]});
            rotirajMapu(ugao);
        }
    );
}

function crtajStuboveNaMapi(){    
    obrisiLejerIzMape("stub");
    obrisiLejerIzMape("dv");
    if (!g_nizStubova || g_nizStubova.length < 1) return;
    
    let stacMIN = g_nizTacakaTlo[0].x; //[m]
    let stacMAX = g_nizTacakaTlo[g_nizTacakaTlo.length-1].x; //[m]
    let stacDuzina = stacMAX-stacMIN;

    
    let nizStacionaza = izdvojNizIzObjekata("x",g_nizStubova);
    let latDiff = g_latlongB[0] - g_latlongA[0];
    let longDiff = g_latlongB[1] - g_latlongA[1]; 
    let nizLatLong=[], procenat;
    for (let i = 0; i < nizStacionaza.length; i++) {
        procenat = (nizStacionaza[i] - stacMIN)/stacDuzina;
        nizLatLong[i] = [g_latlongA[0] + procenat*latDiff, 
                         g_latlongA[1] + procenat*longDiff];
    }

    let map_nizTacaka_transformed = prilagodiKoorSis_niz(nizLatLong);

    let vektor_TacakaStub = kreirajVektor_tacaka(map_nizTacaka_transformed);
    let vektor_LinijeDV = kreirajVektor_linije(map_nizTacaka_transformed);

    let lejer_TackeStub = kreirajLejer(vektor_TacakaStub, stilStub, "stub",15);
    let lejer_LinijaDV = kreirajLejer(vektor_LinijeDV, stilDV, "dv",11);

    dodajLejerUMapu(lejer_TackeStub);
    dodajLejerUMapu(lejer_LinijaDV);


}


let stilStub = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 7,
        fill: new ol.style.Fill({ color: 'red' }),
        stroke: new ol.style.Stroke({ color: 'red', width: 0.5 })
    })
});
let stilTrase = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: "black",
        width: 2,
        lineDash: [4, 8]
    })
});
let stilDV = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: "red",
        width: 3
        /*,lineDash: [4, 8]*/
    })
});
let stilX = new ol.style.Style({
    image: new ol.style.RegularShape({
        stroke: new ol.style.Stroke({ color: 'black', width: 2 }),
        points: 4, //cetiri tacke
        radius: 10, // duzina linije
        radius2: 0, // formiranje x znaka umesto kvadrata
        angle: Math.PI / 4
    })
});

$(document).ready(function () {
    
    function kreirajMapu(lejeri, centar, zoomVar) {
/*
        var inchesPerMeter = 39.3700787;
        var dpi = 96;

        function getResolutionFromScale(scaleDenominator) {
        return scaleDenominator / inchesPerMeter / dpi;
        }
                var scales = [500, 1000, 2000, 4000, 10000, 25000, 50000];
        var resolutions = [];
        for(var i = 0; i < scales.length; i++) {
            resolutions.push(getResolutionFromScale(scales[i]));
        }
*/
        let newMap = new ol.Map({            
            layers: lejeri, // lejere tacak i linija dodajem preko funkcija
            // Improve user experience by loading tiles while dragging/zooming. Will make
            // zooming choppy on mobile or slow devices.
            loadTilesWhileInteracting: true,
            target: 'map', //id diva u koji se smesta mapa 
            view: new ol.View({
                center: prilagodiKoorSis_tacka(centar),
                zoom: zoomVar,
                maxZoom: 20
/*                
                minScale: scales[scales.length - 1],
                maxScale: scales[0],        
                resolution: resolutions,
                numZoomLevels: scales.length,
                units: 'm'
*/                
            })
        });
        
        newMap.addControl(new ol.control.ScaleLine());
        newMap.addControl(new ol.control.FullScreen());
        return newMap;
    }    

    function promeniBingMapu() {
        let selektovaniLejer = selektorBingMapa.value;
        for (let i = 0; i < bing_lejeri.length; i++)
            bing_lejeri[i].setVisible(oznakaLejera[i] === selektovaniLejer);
    }
    
    // moguci lejeri bing mape
    let oznakaLejera = ['Road', 'Aerial', 'AerialWithLabels'];
    // kreiranje niza lejera bingmape
    let bing_lejeri = [];
    for (let i = 0; i < oznakaLejera.length; i++) {
        bing_lejeri.push(new ol.layer.Tile({
            visible: false,
            preload: Infinity,
            source: new ol.source.BingMaps({
                key: 'ApuESI8iqT6sCpVk_CjJPlSVENnNbK19v_ubab_4XXI-zA_AvhVCYgl_TMvpqB4V',
                imagerySet: oznakaLejera[i]
                // use maxZoom 19 to see stretched tiles instead of the BingMaps
                // "no photos at this zoom level" tiles
                ,maxZoom: 19
            })
        }));
    }
    // na osnovu selektora zadajemo tip bing mape koja ce da se prikazuje
    let selektorBingMapa = document.getElementById('layer-select');

    selektorBingMapa.addEventListener('change', promeniBingMapu());
    promeniBingMapu();


    let centarMape = [43.72583, 20.68944]; // lat(y koordinata), long(x koordinata)
    let zoom_map = 8;
    // kreiranje mape
    g_map = kreirajMapu(bing_lejeri, centarMape, zoom_map);

    /**
     * klikom na mapu odabiramo tacku iz koje krece ili se zavrsava tarasa DV
     */
    g_map.on('click', function(e) {
        // ako g_pickPoint nije aktivno onda nam ne treba odabir tacke 
        if ( ! g_pickPoint) return; 

        let latlong = ol.proj.transform(e.coordinate, "EPSG:3857", "EPSG:4326");
        let lat = latlong[1]; // u njihovom koord sis prvo ide long pa lat
        let long = latlong[0];
        let inputs = $('#formLatLong .activePoint input');
        $(inputs[0]).val(lat);
        $(inputs[1]).val(long);

        // izlazak iz fullscreen
        $(".ol-full-screen-true").click();

        g_pickPoint=false;//iskljucujemo odabir tacke

        upisiRazdaljivuDialog(); 
    });

});   

/*
 * @author Nikola Pavlovic
 * sve naredbe potrebne za crtanje
 * njihov redosled je BITAN
 */

$(document).ready(function () {
    $('.dropdown-submenu button').on("click", function (e) {
        $(this).next('div').toggle();
        e.stopPropagation();
        e.preventDefault();
    });

    // ukljucivanje bootstrap tooltipa  - spoljna biblioteka
    $('[data-toggle="tooltip"]').tooltip();
    // kada je vec ukljucen data-toggle=dropdown, onda moram preko drugog selektora
    $('[data-toggle1="tooltip1"]').tooltip();

    // kreiramo SVG u div sa id=drawing - spoljna biblioteka
    g_svgObj = SVG('drawing').size("100%", "100%");

    // resetovanje zuma - spoljna biblioteka
    $('#btn_ZoomReset').click(
        function () { g_svgPanZoom.reset(); }
    );

    //ponovno crtamo sve ispocetka prilikom window.resize
    $(window).resize(function () {
        g_svgPanZoom = $($("svg")[0]).svgPanZoom(
            {
                initialViewBox: {
                    x: 0, // the top-left corner X coordinate
                    y: 0, // the top-left corner Y coordinate
                    width: getSirina(), // the width of the viewBox
                    height: getSirina() // the height of the viewBox
                }
            }
        );
        pokreniCrtanje();
    });

    // eventListner za fileInput tacke tla
    // pri ucitavanju fajla: g_nizTacakaTlo = []
    document.getElementById("file_teren").addEventListener("change", function () {
        if (this.files && this.files[0]) { // ako je ispravno ucitan fajl i to samo jedan
            let myFile = this.files[0];
            let reader = new FileReader();

            reader.onload = function () {
                let fileContents = this.result;
                resetGlobNizova();

                // na osnovu '\n' izdvajanmo red po red iz stringa
                let novRed = '\n';
                let fromThisIndex = brojKaraktera = 0;
                let stringRed;
                while ((brojKaraktera = fileContents.indexOf(novRed, fromThisIndex)) !== -1) { // trazi novRed od fromThisIndex pa nadalje
                    stringRed = fileContents.substring(fromThisIndex, brojKaraktera);
                    getBrojeveIzStringa(stringRed, 2);
                    fromThisIndex = brojKaraktera + 1;
                }
                // ovo je za posledni red
                getBrojeveIzStringa(fileContents.substring(fromThisIndex), 2);

                pokreniCrtanje();
            };

            // prvo smo gore zadali lisener pa tek sada pozivam reader
            reader.readAsText(myFile);

        }// end if
    });

    // eventListner za fileInput tacke objekata
    // pri ucitavanju fajla: 
    document.getElementById("file_obj").addEventListener("change", function () {
        if (this.files && this.files[0]) { // ako je ispravno ucitan fajl i to samo jedan
            let myFile = this.files[0];
            let reader = new FileReader();

            reader.onload = function () {
                let fileContents = this.result;
                g_objektiNaTrasi = [];  // resetujemo samo objekte trase

                // na osnovu '\n' izdvajanmo red po red iz stringa
                let novRed = '\n';
                let fromThisIndex = brojKaraktera = 0;
                let stringRed;
                while ((brojKaraktera = fileContents.indexOf(novRed, fromThisIndex)) !== -1) { // trazi novRed od fromThisIndex pa nadalje
                    stringRed = fileContents.substring(fromThisIndex, brojKaraktera);
                    getBrojeveIzStringa(stringRed, -1);// - 1 kao selektor u funckiji
                    fromThisIndex = brojKaraktera + 1;
                }
                // ovo je za posledni red
                getBrojeveIzStringa(fileContents.substring(fromThisIndex), -1);// - 1 kao selektor u funckiji

                pokreniCrtanje();

            };

            // prvo smo gore zadali lisener pa tek sada pozivam reader
            reader.readAsText(myFile);

        }// end if
    });


    // definicija dialoga za stubove  
    $("#unesiStubove_dialog").dialog({
        autoOpen: false,
        modal: true, // onemogucen klik van dialoga
        width: 800,
        height: 400,
        closeText: "zatvori",
        classes: { // zadajemo klase za da bi prilagodili izgled dialoga
            "ui-dialog": "p-0 border-0 shadow-lg rounded", // ceo dialog
            "ui-dialog-titlebar": "border-0 bg-primary text-light", //heder
            "ui-dialog-content": "p-0", //body
            "ui-dialog-buttonpane": "p-0 bg-info" //footer
        },
        show: {
            // effect - uzimamo random animaciju, menja se tek pri ponovnom ucitavanjem stranice
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 1000, // 100ms
            easing: "swing"
        },
        hide: {
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 500
        },
        buttons: [
            {
                text: "Ok",
                /*icon: "ui-icon-heart",*/
                click: function () {
                    // na klik - pokrece submit forme
                    $(this).find("[type=submit]").click();
                }
            }
        ]
    });

    // pokretanje dialoga stubova i ucitavanje vrednosti u njegove inpute
    $("#unesiStubove_btn").click(function () {
        if (!is_setTlo()) {
            alert("Prethodno morate uneti tacke uzduznog profila trase");
            return;
        }

        $("#unesiStubove_dialog").dialog("open");

        // zadajemo timeout da bi bili sigurni da ce dialog da bude prikazan
        // pre nego sto mu pristupamo elementima preko jQ
        setTimeout(function () {
            if (is_setStubMesta()) {
                let singleRow = $("#tableNo1 tbody tr:first-of-type");
                let singleRowClon;
                let inputiNiz;
                $("#tableNo1 tbody tr").remove();
                for (let i = 0; i < g_nizStubova.length; i++) {
                    singleRowClon = $(singleRow).clone();
                    $(singleRowClon).attr("data-id", i);
                    $("#tableNo1 tbody").append(singleRowClon);
                    inputiNiz = $("#tableNo1 tbody tr:last-of-type").find("input");
                    $(inputiNiz[0]).val(g_nizStubova[i].oznaka);
                    $(inputiNiz[1]).val(g_nizStubova[i].x);
                    $(inputiNiz[2]).val(g_nizStubova[i].h);
                    $(inputiNiz[3]).val(g_nizStubova[i].tip);
                    $(inputiNiz[4]).val(g_nizStubova[i].konzola);
                }
            }
            else {  // brisemo sve redove koji su preostali od prosle raspodele stubnih mesta
                $("#tableNo1 tbody tr:not(:first-of-type)").each(function () {
                    $(this).remove();
                });

            }
            // dodeljujemo event za brisanje
            buttonEventZaBrisanje();
        }, 300);
    });

    // dialogStub - kopira poslednji red u tbody i appenduje ga na kraj tbody
    //nakon kopiranja inkrementira data-id i brise vrednosti input polja
    $("#dodajRed").click(function () {
        let singleRow = $("#tableNo1 tbody tr:last-of-type").clone();
        let lastDataID = parseInt($(singleRow).attr("data-id")) + 1;
        $(singleRow).attr("data-id", lastDataID);

        //brise vrednost inputa kreiranog reda
        nizInputa = $(singleRow).find("input");
        for (let i = 0; i < nizInputa.length; i++) {
            let valInput = $(nizInputa[i]).val();
            let nameInput = $(nizInputa[i]).attr("name");
            nameInput = nameInput.substr(0, nameInput.indexOf('-') + 1);
            $(nizInputa[i]).attr("name", nameInput + lastDataID);

            if (i === 0) {
                // incrementrira rb 
                let broj = parseInt(valInput);
                if (Number.isInteger(broj))
                    broj++;
                else
                    broj = lastDataID;

                $(nizInputa[i]).val(broj);
            }
            else if (i === 1) {
                // brise x
                $(nizInputa[i]).val("");
            }
            else {
                // sve ostalo ostaje isto
            }
        }

        $("#tableNo1 tbody").append(singleRow);

        //dodeljuje event-listener za button u novo kreiranom redu
        buttonEventZaBrisanje();

        //skroluje na-dole
        $("#unesiStubove_dialog_cointainer").animate({ scrollTop: $('#unesiStubove_dialog_cointainer').get(0).scrollHeight }, "fast");
        // fokus na input za X u novom redu
        $("#tableNo1 tbody tr:last-of-type").find("input")[1].focus();

    });

    // dialogStub forma - umesto submita, poziva se f-ja ucitajStuboveDialog() i zatvara se dialog
    $("#form_stubovi").on("submit", function (event) {
        event.preventDefault();
        let bool_flag = ucitajStuboveDialog();
        if (bool_flag) {
            $("#unesiStubove_dialog").dialog("close");
        }
    });

    //////////////////////////////////////////////////////////////////////
    // kliknuto na #saveProject - validiraj i pokreni submitData
    $('#saveProject').click(function () {
        if (typeof g_projName === "undefined") {
            event.preventDefault();
            $('#saveAsProject').click();
        }
        else {
            $("#modalTextInput").modal("show");
            $('#formModalInput input[name="projName"]').attr("disabled", "true");
            $('#formModalInput input[name="projName"]').val(g_projName);
            $('#formModalInput input[name="action"]').val("updateProject");
        }
    });

    // kliknuto na #saveAsProject - pokreni modal za unos imena novog proj
    $('#saveAsProject').click(function () {
        let textInput;
        if (typeof g_projName === "string")
            textInput = g_projName;
        else
            textInput = "";


        $("#modalTextInput").modal("show");
        $('#formModalInput input[name="projName"]').removeAttr("disabled");;
        $('#formModalInput input[name="projName"]').val(textInput);
        $('#formModalInput input[name="action"]').val("saveAsProject");
        /* nece da radi na 100ms, ali hoce na 1000ms */
        setTimeout(function () {
            $('#formModalInput input[name="projName"]').focus();
            $('#formModalInput input[name="projName"]').select();
        }, 1000);

    });

    /**
     * forma getLancanica
     */
    $("#formaLanc").submit(function (event) {
        event.preventDefault();
        submitData('getLancanica');
    });

    // kliknuto na #getPdf - validiraj i pokreni submitData
    $('#getPdf').click(function () {
        submitData('getPdf');
    });
    //////////////////////////////////////////////////////////////////
    // toggle ordinate 
    $("#ordinata").click(function () {
        if (g_ordinata) { // ako ordinata postoji
            if ($(this).html() === "ordinataOFF")
                $(this).html("ordinataON");
            else
                $(this).html("ordinataOFF");
            $("#" + g_ordinata.id()).toggle();
        }
    });

    // snap kontrola 
    $("#snap").click(function () {
        if (g_snap) {
            g_snap = false;
            $(this).html("snapOFF");
        } else {
            g_snap = true;
            $(this).html("snapON");
        }
    });

    // ukljucivanje pan/zoom - spoljna biblioteka
    // - ide preko timeout da bi bio siguran da je svg ucitan
    setTimeout(function () {
        let mainSVG = $("svg")[0];
        g_svgPanZoom = $(mainSVG).svgPanZoom(
            {
                events: {
                    mouseWheel: true, // enables mouse wheel zooming events
                    doubleClick: true, // enables double-click to zoom-in events
                    drag: true, // enables drag and drop to move the SVG events
                    dragCursor: "move" // cursor to use while dragging the SVG
                },
                animationTime: 300, // time in milliseconds to use as default for animations. Set 0 to remove the animation
                zoomFactor: 0.25, // how much to zoom-in or zoom-out
                maxZoom: 50, //maximum zoom in, must be a number bigger than 1
                panFactor: 100, // how much to move the viewBox when calling .panDirection() methods
                minZoom: -1,

                initialViewBox: { // the initial viewBox, if null or undefined will try to use the viewBox set in the svg tag. Also accepts string in the format "X Y Width Height"
                    x: 0, // the top-left corner X coordinate
                    y: 0, // the top-left corner Y coordinate
                    width: getSirina(), // the width of the viewBox
                    // AKO STAVIM DA SE WIDTH I HEIGHT RAZLIKUJU ONDA NECE LEPO DA RADI SCROLL ZUM
                    height: getSirina() // the height of the viewBox
                },
                limits: { // the limits in which the image can be moved. If null or undefined will use the initialViewBox plus 15% in each direction
                    x: -150,
                    y: -150,
                    x2: 1150,
                    y2: 1150
                }

            });

    }, 500);


    //$("#map_container").hide(); // ovo mi je pravilo bug jer krije i div i mapu
    $("#map_toggle").click(function () {
        $("#map_container").toggle("slide", { direction: "right" }, 1000);
        // slicno kao i gore
        $("#map_container canvas:first-of-type").toggle("slide", { direction: "right" }, 1000);
        $("#map_toggle i").toggleClass("fa-angle-double-left");
        $("#map_toggle i").toggleClass("fa-angle-double-right");
    });
    $("#map_toggle").click(); //sklanjamo mapu

    ////////////////////////////////////////////////////////////////
    // potrebno jedino za mousemove event - koji se cesto izvrsama
    // pa radimo predefiniciju    
    const event_svgVar = $("svg")[0];//nece da radi preko g_svgObj, zato kreiram svgVar  
    const event_ptCoordScreen = event_svgVar.createSVGPoint();//tacka kojoj se dodeljuju koord screen-a
    /**
     * funkcija uzima screen koord iz eventa -> tansf. u svg koord
     * @param {Event} event - mouseover
     * @returns {SVGPoint} - tacka sa koord. koje se odnose na SVG
     */
    function getCursorCoordInSVG(event) {
        event_ptCoordScreen.x = event.clientX;
        event_ptCoordScreen.y = event.clientY;
        // screen koord transformisemo u koord svg elementa
        return event_ptCoordScreen.matrixTransform(event_svgVar.getScreenCTM().inverse());
    }

    // promenljive koje su potrebne za mousemove listenet
    // njihova definicija unutar mousemove bi se nepotrebno izvrsavala pri svakom eventu
    let listener_xmax;
    let listener_xmin;
    let listener_x, xaps, yaps, haps;
    // 1 - uzimamo koordinate na mousemove i ispisujemo u footer
    // 2 - vrsimo translaciju g_ordinata
    event_svgVar.addEventListener('mousemove', function (event) {
        // ako trasa nije ucitana onda nema sta da upisujemo
        if (is_setTlo()) {
            listener_xmax = g_nizTacakaTlo_rel[g_nizTacakaTlo_rel.length - 1].x;
            listener_xmin = g_nizTacakaTlo_rel[0].x;

            // uzimamo samo x - drugu koord proracunavamo            
            listener_x = getCursorCoordInSVG(event).x;
            if (listener_x < listener_xmin)
                listener_x = listener_xmin;
            else if (listener_x > listener_xmax)
                listener_x = listener_xmax;

            x_aps = kovnertAtribut_rel_to_aps("x", listener_x);

            if (g_snap && is_setArray(g_nizStacObj,1)) {
                let osetljivost = skaliraj_X_unazad(g_snap_osetljivost);
                for (let i = 0; i < g_nizStacObj.length; i++) {
                    if (x_aps > g_nizStacObj[i]-osetljivost &&
                        x_aps < g_nizStacObj[i]+osetljivost) {
                            x_aps=g_nizStacObj[i];
                    }                    
                }
            }

            $("#x_koor").val(round(x_aps));
            upisiYHfooter(x_aps);
            translirajOrdinatu(x_aps);
        }
    });
    ////////////////////////////////////////////////////////////////
    // definicija dialoga za open sesiju  
    $("#openProject_dialog").dialog({
        autoOpen: false,
        modal: true, // onemogucen klik van dialoga
        width: 700,
        height: 450,
        closeText: "zatvori",
        classes: { // zadajemo klase za da bi prilagodili izgled dialoga
            "ui-dialog": "p-0 border-0 shadow-lg rounded", // ceo dialog
            "ui-dialog-titlebar": "border-0 bg-primary text-light", //heder
            "ui-dialog-content": "p-0", //body
            "ui-dialog-buttonpane": "p-0 m-0 bg-info" //footer
        },
        show: {
            // effect - uzimamo random animaciju, menja se tek pri ponovnom ucitavanjem stranice
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 1000, // 100ms
            easing: "swing"
        },
        hide: {
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 500
        },
        buttons: [
            {
                text: "Select",
                title: "ucitaj projekat",
                /*icon: "ui-icon-circle-check",*/
                class: "bg-primary text-light",
                click: function () {
                    if (typeof g_selectedProjData === "undefined") {
                        alert("prvo selektuj projekat");
                        return;
                    }
                    else {
                        $(this).dialog("close");
                        loadSelectedProj(g_selectedProjData);
                    }
                }
            }
        ]
    });
    // pokretanje dialoga openProj i ucitavanje vrednosti u njegove inpute
    $("#openProject").click(function () {
        if (false) {
            alert("Prethodno morate uneti tacke uzduznog profila trase");
            return;
        }

        $("#openProject_dialog").dialog("open");

        deleteDialogInfo();

        g_selectedProjData = undefined;

        let url_controler = "../control/Controller_projekti.php";
        let action_controler = "getUserProjectsList";
        $.ajax({ /*getUserProjectsList*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: "&action=" + action_controler,
            success: function (data) {
                if (data.status != "success") {
                    let tBody = $("#tableOpenProj tbody ");
                    $(tBody).find("tr:first-of-type td").html(data.status + " (" + data.msg + ")");
                    return;
                }

                let niz = data.data;

                let trDef = $("#tableOpenProj tbody tr:first-of-type").clone();
                $(trDef).css("display", "table-row");
                $(trDef).find("td").css("display", "table-cell");
                $("#tableOpenProj tbody").empty();
                for (let i = 0; i < niz.length; i++) {
                    newTr = $(trDef).clone();
                    $(newTr).find("td").html(niz[i].naziv);
                    $(newTr).find("td").attr("data-id", niz[i].id);
                    $("#tableOpenProj tbody").append(newTr);
                }

                redovi = $("#tableOpenProj tbody td");
                for (let i = 0; i < redovi.length; i++) {
                    $(redovi[i]).click(function () {

                        let url_controler = "../control/Controller_projekti.php";
                        let action_controler = "getSingleProj";
                        $.ajax({ /*getSingleProj*/
                            type: "POST",
                            url: url_controler,
                            timeout: 5000,
                            beforeSend: function(){ajax_beforeSend()},
                            complete: function(){ajax_complete()},
                            dataType: 'json',
                            error: function (dataErr) {
                                ajax_error(dataErr.responseText);
                            },
                            data: "&action=" + action_controler + "&projId=" + $(this).attr("data-id"),
                            success: function (data) {
                                if (data.status != "success") {
                                    let detailView = $("#openProject_dialog .detailView ");
                                    $(detailView).find("header em").html(data.status + " (" + data.msg + ")");
                                    g_selectedProjData = undefined;
                                    return;
                                }

                                g_selectedProjData = data.data;

                                let detailView = $("#openProject_dialog .detailView ");
                                $(detailView).find("header em").html(data.data.naziv);
                                $(detailView).find("#projVreme").html(data.data.vreme);
                                $(detailView).find("#projProv").html(data.data.oznakaProv);
                                $(detailView).find("#projNaprezanje").html(data.data.naprezanje);
                                $(detailView).find("#projOdo").html(data.data.odo);
                                $(detailView).find("#projTemperatura").html(data.data.temperatura);
                                let tackeTlo = JSON.parse(data.data.tackeTlaJson);
                                if (tackeTlo)
                                    $(detailView).find("#projTlo").html(tackeTlo.length);
                                else
                                    $(detailView).find("#projTlo").html("-");
                                let objektiTrase = JSON.parse(data.data.objektiTraseJson);
                                if (objektiTrase)
                                    $(detailView).find("#projObjektiTrase").html(objektiTrase.length);
                                else
                                    $(detailView).find("#projObjektiTrase").html("-");
                                let stubovi = JSON.parse(data.data.stubnaMestaJson);
                                if (stubovi)
                                    $(detailView).find("#projStub").html(stubovi.length);
                                else
                                    $(detailView).find("#projStub").html("-");
                                let tackeLanc = JSON.parse(data.data.tackeLancJson);
                                if (tackeLanc)
                                    $(detailView).find("#projLanc").html(tackeLanc.length);
                                else
                                    $(detailView).find("#projLanc").html("-");
                            }
                        });
                    });
                }//end for



                // nece da formatira tabelu
                //$("#tableOpenProj").removeClass();
                //$("#tableOpenProj").addClass("table table-striped table-hover ");
            }
        });// end  a j a x

    });

    // pokretanje saveAs iz modala
    $("#formModalInput").submit(function (event) {
        event.preventDefault();
        if (!validirajZahtev_projName())
            return;
        else {
            $("#modalTextInput").modal("hide");
            let action = $('#formModalInput input[name="action"]').val();
            submitData(action);
        }
    });

    // 
    $("#deleteProj").click(function (event) {
        event.preventDefault();
        if (typeof g_selectedProjData === "undefined") {
            alert("prvo selektuj projekat");
            return;
        }

        let url_controler = "../control/Controller_projekti.php";
        let action_controler = "delete";
        deleteDialogInfo();
        $.ajax({ /*delete proj*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: "action=" + action_controler + "&projId=" + g_selectedProjData.id + "&projName=" + g_selectedProjData.naziv,
            success: function (data) {
                if (data.status != "success") {
                    alert(data.status + " "+data.msg);
                    return;
                }

                g_selectedProjData = undefined;
                $("#openProject_dialog_cointainer .masterView td[data-id='" + data.data + "']").hide("slow");
                alert("izbrisan projekat");

            }
        });
    });


    // definicija dialoga za user info
    $("#userInfo_dialog").dialog({
        autoOpen: false,
        modal: true, // onemogucen klik van dialoga
        width: 600,
        height: 500,
        closeText: "zatvori",
        classes: { // zadajemo klase za da bi prilagodili izgled dialoga
            "ui-dialog": "p-0 border-0 shadow-lg rounded", // ceo dialog
            "ui-dialog-titlebar": "border-0 bg-primary text-light", //heder
            "ui-dialog-content": "p-0", //body
            "ui-dialog-buttonpane": "p-0 m-0 bg-info" //footer
        },
        show: {
            // effect - uzimamo random animaciju, menja se tek pri ponovnom ucitavanjem stranice
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 1000, // 100ms
            easing: "swing"
        },
        hide: {
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 500
        },
        buttons: [
            {
                text: "OK",
                title: "zatvori info dialog",
                /* icon: "ui-icon-circle-check", */
                class: "bg-primary text-light",
                click: function () { $(this).dialog("close"); }
            }
        ]
    });
    // pokretanje dialoga userInfo i ucitavanje vrednosti u njegova polja
    $("#userInfo_button").click(function () {
        $("#userInfo_dialog").dialog("open");

        let url_controler = "../control/Controller_user_history.php";
        let action_controler = "GetInfo";
        $.ajax({ /* get user info*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: "&action=" + action_controler,
            success: function (data) {
                if (data.status != "success") {
                    $("#userInfo_dialog_cointainer strong").html("-");
                    $("#userInfo_dialog_cointainer h5:first-of-type strong").html(data.status);
                    return;
                }

                let nizDay_1 = data.data.d01;
                let nizDay_30 = data.data.d30;
                let nizTotal = data.data.total;

                let nizQuota = data.data.quota;
                let nizDay_1_quota = [];
                nizDay_1_quota.push(nizQuota["noJS_day"], nizQuota["noProj_day"], nizQuota["noPDF_day"]);
                let nizDay_30_quota = [];
                nizDay_30_quota.push(nizQuota["noJS_month"], nizQuota["noProj_month"], nizQuota["noPDF_month"]);

                function upisiPodatke(niz, i) {
                    $(".brojJS:eq(" + i + ")").html(niz["brojJS"]);
                    $(".brojProj:eq(" + i + ")").html(niz["brojProj"]);
                    $(".brojPDF:eq(" + i + ")").html(niz["brojPDF"]);
                }
                function upisiPodatkeQuota(niz, i) {
                    $(".brojJS_quota:eq(" + i + ")").html(niz[0]);
                    $(".brojProj_quota:eq(" + i + ")").html(niz[1]);
                    $(".brojPDF_quota:eq(" + i + ")").html(niz[2]);
                }

                upisiPodatke(nizDay_1, 0);
                upisiPodatke(nizDay_30, 1);
                upisiPodatke(nizTotal, 2);
                upisiPodatkeQuota(nizDay_1_quota, 0);
                upisiPodatkeQuota(nizDay_30_quota, 1);


            }
        });// end a j a x

    });


    /**
     * validira unetu koordinaty X u futer
     * - pokrece trans. ordinate
     * - pokrece upis Y, H
     * @param {Event} event onsubmit
     * @global g_nizTacakaTlo
     */
    $("#form_x_koor").submit(function (event) {
        event.preventDefault(); // sprecavamo submit forme
        if (is_setTlo()) {
            let x_aps = parseFloat($("#x_koor").val());
            let xmax = g_nizTacakaTlo[g_nizTacakaTlo.length - 1].x;
            let xmin = g_nizTacakaTlo[0].x;

            if (x_aps >= xmin && x_aps <= xmax) {
                upisiYHfooter(x_aps);
                translirajOrdinatu(x_aps);
            }
            else
                alert("Unesite ispravnu stacionazu!");
        }
    });


    $('#btn_centerMap').click(function () {
        if (typeof g_nizTacakaTlo != "undefined" && g_nizTacakaTlo.length > 1) {
            //if(true)  { 
            $("#modalLatLong").modal("show");
            if ($("#map_container").css('display') == 'none') {
                $("#map_toggle").click();
            }
        } else {
            alert("Ucitaj prvo uzduzni profil terena!");
        }
        g_boolFlag_elevacija = false;
    });


    // centriranje mape
    $("#formLatLong").submit(function (event) {
        event.preventDefault();

        //returns key-value pairs
        let data = $(this).serializeArray().reduce(function (obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

        let lat1, lat2, long1, long2;
        try {
            lat1 = parseFloat(data.latFirst);
            long1 = parseFloat(data.longFirst);
            lat2 = parseFloat(data.latLast);
            long2 = parseFloat(data.longLast);
        } catch (error) {
            alert("nisu ispravno unete koordinate");
            return;
        }

        if (lat1 == lat2 && long1 == long2) {
            alert("Koordinate su indenticne!?");
            return;
        }

        let distance = distanceLatLong([lat1, long1], [lat2, long2]);
        if (distance > 3000) {
            alert("Duzina trase mora biti manja od 3000m");
            return;
        }

        g_latlongA[0] = lat1;
        g_latlongA[1] = long1;
        g_latlongB[0] = lat2;
        g_latlongB[1] = long2;

        if (g_boolFlag_elevacija) {
            g_boolFlag_elevacija = false;
            let url_controler = "../control/Controller_alati.php";
            let action_controler = "getElevation";
            $.ajax({ /* get elevation*/
                type: "POST",
                url: url_controler,
                timeout: 4*5000,
                beforeSend: function(){ajax_beforeSend()},
                complete: function(){ajax_complete()},
                dataType: 'json',
                error: function (dataErr) {
                    ajax_error(dataErr.responseText);
                },
                data: $("#formLatLong").serialize() + "&distance=" + distance + "&action=" + action_controler,
                success: function (data) {
                    if (data.status != "success") {
                        alert(data.status);
                        return;
                    }

                    resetGlobNizova();
                    g_nizTacakaTlo = data.data;
                    pokreniCrtanje();
                    centrirajMapu(g_latlongA, g_latlongB);
                    alert("uspesno kreiran profil");

                }
            });
        }

        else {
            // ne sme da se izrsava ako prethodno nije ucitana trasa
            // - odnosno nema svg polyline koja nam odredjuje sirinu vektora tacaka tla
            // koji se prikazuje na mapi
            if (is_setTlo()) {
                centrirajMapu(g_latlongA, g_latlongB);
            }
        }

        $("#modalLatLong").modal("hide");


    });

    /**
     * zamena vrednosti koordinata 
     */
    $("#swapLatLong").click(function () {
        let latA = $("input[name='latFirst']").val();
        let longA = $("input[name='longFirst']").val();
        let latB = $("input[name='latLast']").val();
        let longB = $("input[name='longLast']").val();
        $("input[name='latFirst']").val(latB);
        $("input[name='longFirst']").val(longB);
        $("input[name='latLast']").val(latA);
        $("input[name='longLast']").val(longA);
    });

    // odabir koordinate latlong klikom na mapu
    $('.pickCoord').click(function () {
        //$("#modalLatLong").modal("hide"); // ne mora da zatvaram sada
        $(".ol-full-screen-false").click(); // ulazimo u full screen
        g_pickPoint = true; // signaliziramo da pri kliku na mapu vrati koordinate

        // signal gde da se vracene koordinate uspisu
        $('.latlongPoint').removeClass('activePoint');
        $(this).parent().addClass('activePoint');
    });

    // racuna i upisuje razdaljinu pri promeni latlong vrednosti
    $("#formLatLong input").on("change paste keyup", function () {
        upisiRazdaljivuDialog();
    });


    $('#getElevation').click(function () {
        $("#modalLatLong").modal("show");
        if ($("#map_container").css('display') == 'none') {
            $("#map_toggle").click();
        }
        g_boolFlag_elevacija = true;

    });

    // definicija dialoga za user info
    $("#tabelaUgiba_dialog").dialog({
        autoOpen: false,
        modal: true, // onemogucen klik van dialoga
        width: 800,
        height: 500,
        closeText: "zatvori",
        classes: { // zadajemo klase za da bi prilagodili izgled dialoga
            "ui-dialog": "p-0 border-0 shadow-lg rounded", // ceo dialog
            "ui-dialog-titlebar": "border-0 bg-primary text-light", //heder
            "ui-dialog-content": "p-0", //body
            "ui-dialog-buttonpane": "p-0 m-0 bg-info" //footer
        },
        show: {
            // effect - uzimamo random animaciju, menja se tek pri ponovnom ucitavanjem stranice
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 1000, // 100ms
            easing: "swing"
        },
        hide: {
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 500
        },
        buttons: [
            {
                text: "OK",
                title: "zatvori dialog",
                /* icon: "ui-icon-circle-check", */
                class: "bg-primary text-light",
                click: function () { $(this).dialog("close"); }
            }
        ]
    });

    // pokretanje dialoga za prikaz tabele ugiba
    $("#getTabelaUgiba").click(function () {
        if (!is_setLancanica()) {
            alert("Prethodno morate da kreirate lancanicu");
            return;
        }

        $("#tabelaUgiba_dialog").dialog("open");

        let url_controler = "../control/Controller_js.php";
        let action_controler = "GetTabelaUgiba";
        $.ajax({ /*get tabela ugiba*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: $("#formaLanc").serialize() + "&action=" + action_controler,
            success: function (data) {
                if (data.status != "success") {
                    alert(data.status + " - " + data.msg);
                    return;
                }
                $("#tabelaUgiba_dialog_cointainer strong").html("-");
                $("#tabelaUgiba_dialog_cointainer td").html("-");

                let nizHedera = data.data.heder;
                let matricaUgiba = data.data.ugib;
                let nizRaspona = getNizRaspona();

                let nizHeaderStrong = $("#tabelaUgiba_dialog_cointainer header strong");
                for (let i = 0; i < nizHeaderStrong.length; i++) {
                    $(nizHeaderStrong[i]).html(nizHedera[i]);
                }


                let singleRow = $("#tabelaUgiba_dialog_cointainer tbody tr:first-of-type");
                let singleRowClon;
                let tdNiz;
                $("#tabelaUgiba_dialog_cointainer tbody tr").remove();
                for (let i = 0; i < nizRaspona.length; i++) {
                    singleRowClon = $(singleRow).clone();
                    $("#tabelaUgiba_dialog_cointainer tbody").append(singleRowClon);
                    tdNiz = $("#tabelaUgiba_dialog_cointainer tbody tr:last-of-type").find("td");
                    $(tdNiz[0]).html(nizRaspona[i]);

                    for (let j = 0; j < matricaUgiba[i].length; j++) {
                        $(tdNiz[j + 1]).html(matricaUgiba[i][j]);
                    }
                }
            } // end s u c c e s
        });// end a j a x

    });

    $("#viewProv_dialog").dialog({
        autoOpen: false,
        modal: true, // onemogucen klik van dialoga
        width: 700,
        height: 500,
        closeText: "zatvori",
        close: function( event, ui ) {/*alert('a5454'); primer kao da uzvatim dialog close*/},
        classes: { // zadajemo klase za da bi prilagodili izgled dialoga
            "ui-dialog": "p-0 border-0 shadow-lg rounded", // ceo dialog
            "ui-dialog-titlebar": "border-0 bg-primary text-light", //heder
            "ui-dialog-content": "p-0", //body
            "ui-dialog-buttonpane": "p-0 m-0 bg-info" //footer
        },
        show: {
            // effect - uzimamo random animaciju, menja se tek pri ponovnom ucitavanjem stranice
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 1000, // 100ms
            easing: "swing"
        },
        hide: {
            effect: nizAnimacijaDialoga[getRandIndexFromNiz(nizAnimacijaDialoga)],
            duration: 500
        },
        buttons: [
            {
                text: "Ok",
                title: "zatvori pregled",
                /*icon: "ui-icon-circle-check",*/
                class: "bg-primary text-light",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });

    // pokretanje dialoga za prikaz podataka o provodnicima
    $("#provodnici_btn").click(function () {
        $('#elementiDV').click();
        $('#dropdownAlati').click();

        let url_controler = "../control/Controller_provodnik.php";
        let action_controler = "getUserProvList";
        $.ajax({ /* get listu prov*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: "&action=" + action_controler,            
            success: function (data) {
                /* ovo se automatski radi posto je dataType: 'json'
                data = JSON.parse(recivedData);
                if (!data) {
                    alert("nije moguce izvrsiti naredbu");
                    return;
                } */
                if (data.status != "success") {
                    alert(data.status + " - nije moguce izvrsiti naredbu");
                    return;
                }

                $("#viewProv_dialog").dialog("open");
                let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
                $(detailViewInputs).attr("disabled","disabled");
                toggleProvButtons(); // sakri sve buttone

                let nizOznakProv = data.data;

                let trDef = $("#viewProv_dialog_cointainer tbody tr:first-of-type").clone();
                $(trDef).css("display", "table-row");
                $(trDef).find("td").css("display", "table-cell");
                $("#tableViewProv tbody").empty();
                for (let i = 0; i < nizOznakProv.length; i++) {
                    newTr = $(trDef).clone();
                    $(newTr).find("td").html(nizOznakProv[i].oznaka);
                    $(newTr).find("td").attr("data-id", nizOznakProv[i].id);
                    $("#tableViewProv tbody").append(newTr);
                }

                redovi = $("#tableViewProv tbody td");
                for (let i = 0; i < redovi.length; i++) {
                    $(redovi[i]).click(function () {
                        let url_controler = "../control/Controller_provodnik.php";
                        let action_controler = "getProv";                        
                        let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
                        $(detailViewInputs).attr("disabled", "disabled");
                        $.ajax({ /* get single provodnik */
                            type: "POST",
                            url: url_controler,
                            timeout: 5000,
                            beforeSend: function(){ajax_beforeSend()},
                            complete: function(){ajax_complete()},
                            dataType: 'json',
                            error: function (dataErr) {
                                ajax_error(dataErr.responseText);
                            },
                            data: "&action=" + action_controler + "&provId=" + $(this).attr("data-id"),
                            success: function (data) {
                                let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
                                if (data.status != "success") {
                                    $(detailViewInputs).val("-");
                                    alert(data.status + " - nije moguce ucitavanje podataka");
                                    return;
                                }

                                if (data.data.userId == -1) toggleProvButtons("c");
                                else toggleProvButtons("cde");

                                $("#viewProv_dialog_cointainer input[name='provId']").val(data.data.id);
                                $("#viewProv_dialog_cointainer input[name='provName']").val(data.data.oznaka);
                                $("#viewProv_dialog_cointainer input[name='provPresek']").val(data.data.presek);
                                $("#viewProv_dialog_cointainer input[name='provPrecnik']").val(data.data.precnik);
                                $("#viewProv_dialog_cointainer input[name='provTezina']").val(data.data.tezina);
                                $("#viewProv_dialog_cointainer input[name='provE']").val(data.data.modul);
                                $("#viewProv_dialog_cointainer input[name='provAlfa']").val(data.data.alfa);
                                $("#viewProv_dialog_cointainer input[name='provI']").val(data.data.struja);

                            }
                        });
                    });
                }//end for
            } // end s u c c e s
        });// end a j a x

    });

    $('#copyProv').click(function () {
        let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
        $(detailViewInputs).removeAttr("disabled");

        toggleProvButtons("os");

        let inputOznaka = detailViewInputs[0];
        $(inputOznaka).val($(inputOznaka).val() + " -new");
        $(inputOznaka).focus();
    });

    $('#deleteProv').click(function () {
        let url_controler = "../control/Controller_provodnik.php";
        let action_controler = "deleteProv";
        let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
        $(detailViewInputs).removeAttr("disabled");
        let curentId = $("#form_provodnici input[name='provId']").val();
        $.ajax({ /*delete provodnik*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: $("#form_provodnici").serialize() + "&action=" + action_controler,
            success: function (data) {
                if (data.status != "success") {
                    alert(data.status +" --- "+ data.msg);
                    return;
                }
                
                let zaBrisanje = $('#formaLanc select[name="provodnik"] option[value='+"'"+data.msg+"'"+']');
                if (zaBrisanje) $(zaBrisanje).remove();

                alert(data.status +" --- "+ data.msg + "je obrisan");
                $(detailViewInputs).val("");        
                toggleProvButtons(); 
                
                $("#viewProv_dialog_cointainer .masterView td[data-id='"+curentId+"']").parent("tr").remove();
            }
        }); // end a j a x
        $(detailViewInputs).attr("disabled", "disabled");

    });

    $('#saveProv').click(function () {
        // html5 validacija preko checkValidity()
        if ($('#form_provodnici')[0].checkValidity() === false ){ 
            // ako val nije ok onda klik na submit samo 
            // da bi se prikazala poruka html5 validacije           
            $('#form_provodnici input[type="submit"]').click();
            return;
        }

        let url_controler = "../control/Controller_provodnik.php";
        let action_controler;

        let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
        let isDisabled = $(detailViewInputs).first().attr('disabled');
        
        // For some browsers, `attr` is undefined; for others, `attr` is false.  Check for both.
        if (typeof isDisabled !== typeof undefined && isDisabled !== false) {
            action_controler = "editProv";
        }
        else action_controler = "saveProv";
        
        $.ajax({ /*save provodnik*/
            type: "POST",
            url: url_controler,
            timeout: 5000,
            beforeSend: function(){ajax_beforeSend()},
            complete: function(){ajax_complete()},
            dataType: 'json',
            error: function (dataErr) {
                ajax_error(dataErr.responseText);
            },
            data: $("#form_provodnici").serialize() + "&action=" + action_controler,
            success: function (data) {
                if (data.status != "success") {
                    alert(data.status +" --- "+ data.msg);
                    return;
                }

                alert(data.status +" --- "+ data.msg + " ok");
                
                isDisabled = $(detailViewInputs).first().attr('disabled');
                if (typeof isDisabled == typeof undefined || isDisabled == false) {
                    // u pitanju je save
                    $('#formaLanc select[name="provodnik"]').append("<option value='"+data.msg+"'>"+data.msg+"</option>");
                }
                
                $("#viewProv_dialog").dialog("close");
                $("#provodnici_btn").click();
                $("#viewProv_dialog .master td").first().click();  
                $(detailViewInputs).attr("disabled", "disabled");
                setTimeout(function () {                  
                    toggleProvButtons("ced");
                }, 1000); 


            }
        });
    });

    $('#editProv').click(function () {        
        let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
        $(detailViewInputs).removeAttr("disabled");
        $(detailViewInputs).first().attr("disabled","disabled");

        toggleProvButtons("os");

        let inputOznaka1 = detailViewInputs[1];
        $(inputOznaka1).focus();
    });

    // koristi mi da obustavi proces save ili edit
    $('#cancelProv').click(function () {        
        let detailViewInputs = $("#viewProv_dialog_cointainer .detailView input");
        $(detailViewInputs).attr("disabled","disabled");
        $(detailViewInputs).val("");

        toggleProvButtons();
    });
   

//////////////// modal dobrodoslica  
    $('#startServer').click(function(){
        $('#modalFirstAction').modal('hide');
        $('#openProject').click();
    });
    $('#startLocal').click(function(){
        $('#modalFirstAction').modal('hide');
        $('input[name="file_teren"]').click();
    });
    $('#startLocal_help').click(function(){
        let url_controler = "../control/Controller_user_history.php";
        let action_controler = "GetHelpFile_trasa";
        
        downloadFile_byPost(url_controler, 
            "action="+action_controler,
            "primer uzduznog profila.txt");        
    });
    $('#objekti_help').click(function(){
        let url_controler = "../control/Controller_user_history.php";
        let action_controler = "GetHelpFile_objekti";
        
        downloadFile_byPost(url_controler, 
            "action="+action_controler,
            "primer objekata na trasi DV.txt");        
    });
    $('#startMap').click(function(){
        $('#modalFirstAction').modal('hide');
        $('#getElevation').click();
    });
// regEx kontrola dozvoljenih karaktera za upis u input polje
$('#formModalInput input[name="projName"]').keypress(function (e) {
    let txt = String.fromCharCode(e.which);
    if (!txt.match(/[A-Za-z0-9. -]/))
        return false;
});

}); // end (document).ready
