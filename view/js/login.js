
//////validacija confirm password inputa
$('#newPassWordConf').on('keyup', function () {
    if ($('#newPassWord').val() == $('#newPassWordConf').val()){
        $('#message1').html('Matching').css('color', 'green');
    }
    else{
        $('#message1').html('Not Matching').css('color', 'red');
    }
});
$('#newPassWordConf').focus(function () {
    $('#message1').css('display', 'block');
});
$('#newPassWordConf').blur(function () {
    $('#message1').css('display', 'none');
});


// regEx kontrola dozvoljenih karaktera za upis u email input polje
$('#newEmail').keypress(function (e) {
    let txt = String.fromCharCode(e.which);
    if (!txt.match(/[A-Za-z0-9.@-]/))
        return false;
});
// regEx kontrola dozvoljenih karaktera za upis u ime username polje
$('#newUserName').keypress(function (e) {
    let txt = String.fromCharCode(e.which);
    if (!txt.match(/[A-Za-z0-9. -]/))
        return false;
});
// regEx kontrola dozvoljenih karaktera za upis u ime password polje
$('#newPassWord').keypress(function (e) {
    let txt = String.fromCharCode(e.which);
    if (!txt.match(/[A-Za-z0-9.-]/))
        return false;
});

//////validacija password inputa
var myInput = document.getElementById("newPassWord");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");

// When the user clicks on the password field, show the message box
myInput.onfocus = function () {
    document.getElementById("message").style.display = "block";
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function () {
    document.getElementById("message").style.display = "none";
}


var lowerCaseLetters = /[a-z]/g;
var upperCaseLetters = /[A-Z]/g;
var numbers = /[0-9]/g;

// When the user starts to type something inside the password field
myInput.onkeyup = function () {
    // Validate lowercase letters
    if (myInput.value.match(lowerCaseLetters)) {
        letter.classList.remove("invalid");
        letter.classList.add("valid");
    } else {
        letter.classList.remove("valid");
        letter.classList.add("invalid");
    }

    // Validate capital letters
    if (myInput.value.match(upperCaseLetters)) {
        capital.classList.remove("invalid");
        capital.classList.add("valid");
    } else {
        capital.classList.remove("valid");
        capital.classList.add("invalid");
    }

    // Validate numbers
    if (myInput.value.match(numbers)) {
        number.classList.remove("invalid");
        number.classList.add("valid");
    } else {
        number.classList.remove("valid");
        number.classList.add("invalid");
    }

    // Validate length
    if (myInput.value.length >= 8) {
        length.classList.remove("invalid");
        length.classList.add("valid");
    } else {
        length.classList.remove("valid");
        length.classList.add("invalid");
    }
}

/**
 * validira formu za registraciju novog usera
 */
function validirajReg(){

    let bool_flag = true;

    if ($('#newPassWord').val() !== $('#newPassWordConf').val()){
    bool_flag = false;
    alert("nije potvrdjen password!");
    }

    if ( ! myInput.value.match(upperCaseLetters) 
            || ! myInput.value.match(upperCaseLetters)
            || ! myInput.value.match(numbers)
            || ! myInput.value.length >= 8        ){

    bool_flag = false;
    alert("password nije odgovarajuci!");
    }

    return bool_flag;
  }


  /**
   * otvaranje odovarajuceg taba na osnovu url parametara
   */
  $(document).ready(function() {
    let tab = getURLParameter('tab');
    if (tab) $('a[href="#' +tab+ '"]').click();

    $('#a_TOS').click(function(e) {
        e.preventDefault();
        $('#modalTOS').modal("show");
    });
     
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });


  });
  function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
  }