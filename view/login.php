<?php 
session_start();
$msg = isset($_SESSION['msg']) ? $_SESSION['msg']: "";
$user = isset($_COOKIE['cookie_user'])?json_decode($_COOKIE['cookie_user']):NULL;
?>

<!DOCTYPE html>
<html lang="sr">

<head>
    <?php   include_once '../view/headData.html';   ?>

    <!-- ************ bootstrap 4 -->
    <link rel="stylesheet" href="libraries/css/bootstrap.min.css">
    <!-- ************ fontawesome 5-->
    <link rel="stylesheet" href="libraries/fonts/fontawesome/css/fontawesome-all.min.css">
    
    <!-- heder, meni i foter css -->
    <link rel="stylesheet" type="text/css" href="css/login.css">

</head>

<body>

    <header class="body_header">
        <div class="logo">
            <a href="../index.php">
                <img style="max-height: 70px;" src="media/logo1.png" alt="MPU mehanicki proracun uzadi logo">
            </a>

            <h1 class="logoText">
                <strong>webApp</strong>
            </h1>
        </div>
        <div class="logo_text">
        </div>
    </header>

    <div class="body_container">
        <div class="tabContainer">
            <!-- Nav pills -->
            <ul class="nav nav-pills" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="pill" href="#login">LOGIN</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#register">REGISTER</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div id="login" class="my-3 container tab-pane active">

                    <form id="login" action="../control/Controller_user.php" method="POST" class="m-0">
                        <div class="col-12 px-5 py-3">
                            <input type="text" name="username" class="mx-auto form-control myInput" id="userName" placeholder="username" value="<?php if ($user) echo $user->username?>"
                                required>
                        </div>

                        <div class="col-12 px-5  py-3">
                            <input type="password" name="password" class="mx-auto form-control myInput" id="passWord" placeholder="password" value="<?php if ($user) echo $user->password?>"
                                required>
                        </div>

                        <div class="col-12 px-5  py-3">
                            <label class="mx-auto form-check-label text-success font-weight-bold" for="id_prikaz" data-toggle="tooltip" data-placement="bottom"
                                title="auto upis pri sledecem logovanju">
                                <input type="checkbox" name="prikaz" id="id_prikaz" class="form-check-input" value="YES" checked> Zapamti podatke
                            </label>
                        </div>

                        <div class="col-12 px-5  py-3">
                            <input type="submit" class="btn btn-success mx-auto mt-4 myInput font-weight-bold" name="action" value="Login">
                        </div>

                        <div class="col-12 px-5  py-3">
                            <p class="mx-auto text-danger font-weight-bold">
                                <?=$msg;?>
                            </p>
                        </div>
                    </form>
                </div>

                <div id="register" class="my-3 container tab-pane fade">

                    <form id="register" action="../control/Controller_user.php" onsubmit="return validirajReg()" method="POST" class="m-0">
                        <div class="col-12 px-5 py-1">
                            <input type="text" name="ime" class="mx-auto form-control myInput" id="newIme" placeholder="ime i prezime" required>
                        </div>

                        <div class="col-12 px-5 py-1">
                            <input type="email" name="email" class="mx-auto form-control myInput" id="newEmail" placeholder="email" required>
                        </div>

                        <div class="col-12 px-5 py-1">
                            <input type="text" name="username" class="mx-auto form-control myInput" id="newUserName" placeholder="username" required>
                        </div>

                        <div class="col-12 px-5  py-1">
                            <input data-flag=0 type="password" name="password" class="mx-auto form-control myInput" id="newPassWord" placeholder="password"
                                required title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">
                        </div>

                        <div class="text-center p-0 m-0 " id="message">
                            <p id="letter" class="p-0 m-0 invalid">A
                                <b>lowercase</b> letter</p>
                            <p id="capital" class="p-0 m-0  invalid">A
                                <b>capital (uppercase)</b> letter</p>
                            <p id="number" class="p-0 m-0  invalid">A
                                <b>number</b>
                            </p>
                            <p id="length" class="p-0 m-0  invalid">Minimum
                                <b>8 characters</b>
                            </p>
                        </div>

                        <div data-flag=0 class="col-12 px-5  py-1">
                            <input type="password" name="passwordConf" class="mx-auto form-control myInput" id="newPassWordConf" placeholder="confirm password"
                                required>
                        </div>
                        <div class="text-center" id='message1'></div>

                        <div class="col-12 px-5 py-1 small" style="display: block">
                            Registrovanjem na ovaj sajt, potvrđujete da ste pročitali
                            <a id="a_TOS" href="#" data-toggle="tooltip" title="pročitaj">Pravila i uslove korišćenja sajta</a> i da ste sa njima u potpunosti saglasni.
                        </div>

                        <div class="col-12 px-5  py-1">
                            <input type="submit" class="btn btn-success mx-auto mt-4 myInput font-weight-bold" name="action" value="Register">
                        </div>

                        <div class="col-12 px-5  py-1">
                            <p class="mx-auto text-danger font-weight-bold">
                                <?=$msg;?>
                            </p>
                        </div>

                    </form>
                </div>
            </div>
        </div>


    </div>


    <div class="body_footer">
        <footer class="container_futer">
            <h2 class="text-center">
                Copyright &copy;
                <script>document.write(new Date().getFullYear());</script> by Nikola Pavlović
            </h2>
        </footer>
    </div>


    <div class="modal fade" id="modalTOS" data-backdrop="static">
        <!-- static - disable click outside modal -->
        <div class="modal-dialog modal-dialog-centered  modal-lg">
            <div class="modal-content ">
                <!-- Modal Header -->
                <div class="modal-header bg-primary text-white">
                    <h4 class="modal-title">Pravila i uslovi korišćenja sajta</h4>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="height: 60vh; overflow: auto;">
                    <p>Potrebno je da pažlјivo pročitate Pravila i uslove kori&scaron;ćenja ovog internet sajta (u daljem tekstu:
                        Sajt), &nbsp;pre nego &scaron;to počnete da ga koristitite. Kori&scaron;ćenjem ovog Sajta se dobrovolјno
                        i neopozivo sagla&scaron;avate s njegovim Pravilima i uslovima kori&scaron;ćenja. Ukoliko se ne slažete
                        &ndash; nemojte koristiti ovaj Sajt.</p>
                    <h3>Vlasni&scaron;tvo sadržaja</h3>
                    <p>Internet Sajt i svi tekstovi, logotipi, grafika, slike, video, softverski kod i ostali materijal na ovom
                        Sajtu (u daljem tekstu: Sadržaj), jesu autorsko pravo ili vlasni&scaron;tvo autora&nbsp;Nikole Pavlovića
                        iz Kraljeva, ili su na Sajt postavljeni uz dozvolu vlasnika ili ovla&scaron;ćenog nosioca prava.</p>
                    <p>Kori&scaron;ćenje, kopiranje, modifikovanje, publikacija (&scaron;tampana ili elektronska), prodaja i
                        transfer Sadržaja, osim na način opisan u ovim uslovima kori&scaron;ćenja, bez pisane dozvole vlasnika
                        Sadržaja je strogo zabranjeno.</p>
                    <p>Autor&nbsp;će za&scaron;titi svoja autorska prava, prava intelektualne svojine i ostala srodna prava,
                        u skladu sa zakonskom regulativom.</p>
                    <h3>Izuzeće od odgovornosti</h3>
                    <p>Internet Sajt koristite na sopstvenu odgovornost. Autor&nbsp;nije odgovoran za materijalnu ili nematerijalnu
                        &scaron;tetu, direktnu ili indirektnu, koja nastane pri kori&scaron;ćenju ili je u nekoj vezi sa
                        kori&scaron;ćenjem Sajta, njegovog Sadržaja ili rezultata proračuna softvera na Sajtu.</p>
                    <p>Autor ne obezbeđuje nikakvu garanciju korisniku ovog Sajta, niti omogućuje naknadu &scaron;tete korisniku
                        nastalu kori&scaron;ćenjem ovog Sajta, njegovog Sadržaja ili rezultata proračuna softvera na Sajtu.
                    </p>
                    <p> Autor ima pravo da postavi limite i restrikcije na ovom Sajtu, kao i da oduzme pravo kori&scaron;ćenja
                        Sajta bez obaveze da informi&scaron;e korisnika.</p>
                    <p>Sadržaj Sajta može sadržati netačne podatke ili &scaron;tamparske gre&scaron;ke. Promene na Sajtu se
                        mogu napraviti periodično, u bilo kom trenutku i bez obave&scaron;tenja. Međutim, Autor&nbsp;se ne
                        obavezuje da redovno ažurira informacije sadržane na Sajtu. Takođe, Autor&nbsp;ne garantuje da će
                        Sajt funkcionisati bez prekida ili gre&scaron;aka, da će nedostaci biti blagovremeno ispravljani
                        ili da je Sajt kompatibilan sa va&scaron;im hardverom ili softverom.</p>
                    <h3>Izmena uslova kori&scaron;ćenja</h3>
                    <p>Vlasnik Sajta zadržava pravo da dopuni ili izmeni sadržaj Sajta, ili bilo kojih softverskih alata na
                        Sajtu, bez obaveze najave i obave&scaron;tenja korisnika.&nbsp;Pravila i uslovi kori&scaron;ćenja
                        Sajta takođe mogu biti menjani bez prethodne najave i obave&scaron;tenja korisnika.</p>
                    <h3>Obaveze korisnika internet Sajta</h3>
                    <p> Korisnik Sajta je saglasan da ni u kom slučaju neće:</p>
                    <ul>
                        <li>koristiti Sajt na način koji će naru&scaron;avati vlasnička prava autora Sajta;</li>
                        <li>koristiti Sajt na način koji može o&scaron;tetiti neki od servera ili bilo koji deo hardverske/softverske
                            infrastrukture, ili preduzimati mere koje mogu preopteretiti Sajt i/ili serversku infrastrukturu;</li>
                        <li>kopirati, menjati, publikovati, distribuirati i prodavati bilo koji Sadržaj Sajta;</li>
                        <li>koristiti Sajt na način koji ima veze s piraterijom ili koji može za rezultat imati inficiranje Sajta
                            softverskim virusima, zlonamenim ili bilo kojim drugim softverom;</li>
                        <li>pristupati, ni poku&scaron;avati pristup delovima Sajta ili mreže za koje nije autorizovan.</li>
                    </ul>
                    <h3>Spoljni linkovi</h3>
                    <p>Sajt sadrži linkove ka drugim sajtovima koji nisu u vlasni&scaron;tvu Autora ovog Sajta, i on nije odgovoran
                        za rad spoljnih sajtova i za&scaron;titu privatnosti na tim sajtovima.</p>
                    <p>Osim toga, Sajt pratiti statističku posećenost isključivo radi pribavljanja nužnih informacija uz pomoć
                        zvaničnih servisa Google analitike (Google Analytics).</p>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer  bg-info">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">
                        Ok
                    </button>
                </div>
            </div>
        </div>
    </div>



</body>

<script src="libraries/js/jquery-3.3.1.min.js"></script>
<script src="libraries/js/popper.min.js"></script>
<script src="libraries/js/bootstrap.min.js"></script>

<!-- <script src="js/login.js"></script> -->
<script src="js/login_obf.js"></script>

<script>   

</script>

</html>

<?php 
//session_destroy();
?>