<?php
require_once '../control/root_config.php'; // root putanja
require_once ROOT.'model/access_file.php';
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    neovlascenPristup("__FILE__ == SCRIPT_FILENAME");
}

# ne treba jer je vec definisano u kontroleru
#require_once ROOT.'model/shared_func.php';
    
$prazanRed = "<br><br>";
$msg = $prazanRed."pristup je odobren".$prazanRed;    
$msg .= "realpath(__FILE__) --- ".realpath(__FILE__)."<br>";
$msg .= 'realpath($_SERVER["SCRIPT_NAME"]) --- '.realpath($_SERVER['SCRIPT_NAME'])."<br>";
$msg .= 'realpath($_SERVER["SCRIPT_FILENAME"]) --- '.realpath($_SERVER['SCRIPT_FILENAME'])."<br>";
$msg .= 'realpath($_SERVER["REQUEST_URI"]) --- '.realpath($_SERVER['REQUEST_URI'])."<br>";
$msg .= $prazanRed;
$msg .= 'TOP_FILE - je definisan -->'.TOP_FILE.$prazanRed;

posaljiRezAjax(STATUS_SUCCESS, "poruka", array(1,2,3));

?>