<?php 
//session_start(); 
require_once '../control/root_config.php'; // root putanja
require_once ROOT.'model/access_controler.php'; // dozvola pristupa ovom fajlu
require_once ROOT.'model/shared_func.php'; // uzimamo samo u kontroleru

$email = readGet('email');
$hash = readGet('hash');

$aTag = "go to HOME ";
$aTag_href = "../index.php";

if (! empty($email) && ! empty($hash)) {
    
    require_once ROOT.'model/DAOuser.php';
    $dao = new DAOuser("location: ../view/login.php"); 
    $user = $dao->selectUserFlagByMailAndHash($email, $hash);
    
    if ($user !== FALSE && is_array($user) ) {
        if ($user['isActive'] == 0) {
            $bool_flag = $dao->update_user_verify((int)$user['id'], 1);
            if ($bool_flag) {
                $msg = "Uspesna verifikacija - sada mozete da se logujete";
                $aTag = "continue to LOGIN page";
                $aTag_href = "../view/login.php";
            }else{ // ne moze u bazi da se updajtuje polje isactive
                $msg = "Verifikacija nije ispravno izvrsena - pokusajte kasnije";
            }
        }else{ // user je vec aktiviran
            $aTag = "continue to LOGIN page";
            $aTag_href = "../view/login.php";
            $msg = "Nalog je vec verifikovan";
        }
    }else{ // nije selktovan user u bazi
        $msg = "Neuspesna verifikacija - proverite podatke poslate mejlom";
    }
}else{ // ne dostaju podaci u url adresi
    $msg = "Kliknite na link poslat mejlom radi verifikacije naloga";
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  
  <?php   include_once '../view/headData.html';   ?>  
  
  <link rel="stylesheet" href="libraries/css/bootstrap.min.css">
  <!-- heder, meni i foter css -->
  <link rel="stylesheet" type="text/css" href="../view/css/login.css">


</head>

<body>

  <header class="body_header">
    <div class="logo">
      <a href="../index.php">
        <img style="max-height: 70px;" src="../view/media/logo1.png" alt="MPU mehanicki proracun uzadi logo">
      </a>

      <h1 class="logoText">
        <strong>webApp</strong>
      </h1>
    </div>
    <div class="logo_text">
    </div>
  </header>

  <div class="body_container">

	<div class="text-center">
		<h2><?=$msg?></h2>
		
		<h3><a href="<?=$aTag_href?>"><?=$aTag?></a></h3>
    </div>

  </div>
  

  <div class="body_footer">
    <footer class="container_futer">
      <h2 class="text-center">
        Copyright &copy;
        <script>document.write(new Date().getFullYear());</script> by Nikola Pavlović
      </h2>
    </footer>
  </div>


</body>



<script>
  
</script>

</html>

<?php 
//session_destroy();
?>