<?php
session_start();
if(! isset($_SESSION['user']))  {
    // ako nije user logovan - exit
    exit(header("location: ../view/login.php")); 
}

//ako neki od obaveznih fajlova nije dostupan - exit
if (is_readable("../control/root_config.php")){    
    // da bi ucitali ROOT var
    require_once "../control/root_config.php";
    
    if (is_readable(ROOT.'model/access_controler.php')
        && is_readable(ROOT.'model/shared_func.php') ) 
    {
        // dozvola pristupa fajlovima koji se includuju iz modela
        // simuliram da smo sada u controleru        
        require_once ROOT.'model/access_controler.php';
        // da bi koristio f_fileTestAndInclude($path)
        require_once ROOT.'model/shared_func.php';
    }
    else 
    {
        $_SESSION['msg'] = "MSG02 - servis trenutno nije dostupan";
        exit(header("location: ../view/login.php"));
    }
}
else {
    $_SESSION['msg'] = "MSG01 - servis trenutno nije dostupan";
    exit(header("location: ../view/login.php"));
}

$user = $_SESSION['user'];
f_fileTestAndRequire(ROOT.'model/DAOprovodnici.php');
$daoProv = new DAOprovodnici();

$userId = (int)$user['id'];
$provArray = $daoProv->get_provList_user($userId);
if (is_array($provArray) && sizeof($provArray)>0) {
    $nizOznakaProv = array_column($provArray,"oznaka");
}
else{
    //neki je problem u bazi - exit
    $_SESSION['msg'] = "MSG03 - servis trenutno nije dostupan";
    exit(header("location: ../view/login.php"));
}


?>
<!DOCTYPE html>
<html lang="sr">

<head>

    <?php include_once '../view/headData.html';  ?>

    <!-- ************ bootstrap 4 -->
    <link rel="stylesheet" href="libraries/css/bootstrap.min.css">
    <!-- ************ fontawesome 5-->
    <link rel="stylesheet" href="libraries/fonts/fontawesome/css/fontawesome-all.min.css">
      <!-- jquery UI -->
    <link rel="stylesheet" href="libraries/jquery-ui/jquery-ui.min.css">
     <!-- ************ input:file custom look-->
    <link rel="stylesheet" href="libraries/input_file.css">
    <!-- ************ openLayers map-->
    <link rel="stylesheet" href="https://openlayers.org/en/v5.1.3/css/ol.css" type="text/css">
    
    <link rel="stylesheet" href="css/loader.css">
    <link rel="stylesheet" href="css/crtanje_dialog.css">
    <link rel="stylesheet" href="css/crtanje.css">
    <link rel="stylesheet" href="css/crtanje_dialogOpenProj.css">

    <style>
    </style>

</head>

<body>
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div id="body_kontejner">
        <header class="navbar navbar-expand-md bg-info navbar-dark">
            <a class="navbar-brand" href="../index.php">
                <img style="max-height: 50px;" src="media/logo1.png" data-toggle="tooltip" title="go to HOME page" alt="MPU mehanicki proracun uzadi logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <div class="dropdown">
                            <button class=" hederButton btn btn-primary dropdown-toggle" type="button" id="dropdownProjekti" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="far fa-id-card"></i> projekti
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownProjekti">
                                <button class="dropdown-item" id="openProject" type="submit" name="action" value="openProject" data-toggle="tooltip" data-placement="bottom"
                                    title="Otvori projekat">
                                    <i class="fas fa-folder-open"></i> Open</button>
                                <div class="dropdown-divider"></div>
                                <button class="dropdown-item" id="saveProject" data-toggle="tooltip" data-placement="bottom" title="Sacuvaj projekat">
                                    <i class="fas fa-save"></i> Save</button>
                                <div class="dropdown-divider"></div>
                                <button class="dropdown-item" id="saveAsProject" data-toggle="tooltip" data-placement="bottom" title="Sacuvaj kao nov projekat">
                                    <i class="far fa-save"></i> Save as</button>
                            </div>
                        </div>
                    </li>
                    <!-- ******************************************* -->
                    <li class="nav-item">
                        <div class="dropdown">
                            <button class=" hederButton btn btn-primary dropdown-toggle" type="button" id="dropdownAlati" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="fas fa-wrench"></i> alati
                            </button>
                            <div class="dropdown-menu text-left" aria-labelledby="dropdownAlati">
                                <div class="dropdown-divider"></div>
                                <div class="dropdown-item p-0" style="display: flex;justify-content: space-between;">
                                    <input type="file" class="inputfile " name="file_obj" id="file_obj">
                                    <label class="" for="file_obj" data-toggle="tooltip" data-placement="right" title="Odaberi fajl sa podacima o objektima duz linije trase">
                                        <span>
                                            <i class="fas fa-folder-open"></i> ucitaj objekte</span>
                                    </label>
                                    <div id="objekti_help" style="display: inline-block; cursor:pointer;"  data-toggle="tooltip" title="preuzmi primer formata podatka u .txt fajlu - koristiti word za otvaranje fajla">
                                        <i style="padding: 10px;" class="far fa-question-circle"></i>
                                    </div>
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="dropdown-item p-0">
                                    <button class="btn bg-transparent text-left" id="getPdf" data-toggle="tooltip" data-placement="right" title="Preuzmi profil trase u pdf formatu ">
                                        <i class="fas fa-file-pdf"></i> print pdf</button>
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="dropdown-item p-0">
                                    <button class="btn bg-transparent text-left" id="getElevation" data-toggle="tooltip" data-placement="right" title="Ocitaj profil trase sa mape">
                                        <i class="fas fa-globe"></i> ucitaj trasu iz mape</button>
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="dropdown-item p-0">
                                    <button class="btn bg-transparent text-left" id="getTabelaUgiba" data-toggle="tooltip" data-placement="right" title="Kreiraj tabelu ugiba">
                                        <i class="fas fa-list"></i> tabela ugiba</button>
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="dropdown-item dropdown-submenu p-0">
                                    <div class="dropdown dropright">
                                        <button id="elementiDV" class="btn bg-transparent text-left dropdown-toggle" data-toggle="dropdown" title="pregled podataka o elementima DV"
                                            data-toggle1="tooltip1">
                                            <i class="fas fa-eye"></i> elementi DV
                                        </button>
                                        <div class="dropdown-menu" style="margin-left: -10px!important">
                                            <div class="dropdown-item text-left p-1">
                                                <button class="btn bg-transparent" id="provodnici_btn">provodnici</button>
                                            </div>
                                            <div class="dropdown-item text-left p-1">
                                                <button class="btn bg-transparent" title="nije dostupno" disabled>stubovi</button>
                                            </div>
                                            <div class="dropdown-item text-left p-1">
                                                <button class="btn bg-transparent" title="nije dostupno" disabled>konzole</button>
                                            </div>
                                            <div class="dropdown-item text-left p-1">
                                                <button class="btn bg-transparent" title="nije dostupno" disabled>izolatori</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>


                <button id="unesiStubove_btn" class="btn btn-primary hederButton" data-toggle="tooltip" data-placement="bottom" title="Rasporedi stubove">
                    <i class="far fa-chart-bar"></i> stubna mesta</button>

                <input type="file" class="inputfile" name="file_teren" id="file_teren">
                <label class="hederButton btn btn-primary" for="file_teren" data-toggle="tooltip" data-placement="bottom" title="Odaberi fajl sa podacima o uzduznom profilu trase">
                    <span>
                        <i class="fas fa-folder-open"></i> ucitaj trasu</span>
                </label>

                <div id="lancDropdown" class="dropdown">
                    <button class="btn btn-primary dropdown-toggle hederButton" id="dropDownLanc" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false" data-toggle1="tooltip1" data-placement="bottom" title="Nacrtaj lancanicu">
                        <i class="fas fa-pencil-alt"></i> lancanica
                    </button>
                    <div class="dropdown-menu dropdown-menu-right text-center">
                        <form id="formaLanc" class="p-1 text-left">
                            <div class="form-group m-0">
                                <label class="dropdown-header p-0 mb-1">Tip provodnika</label>
                                <select name="provodnik" class="form-control mr-1" style="width:150px" required>
                                    <?php foreach ($nizOznakaProv as $oznakaProv) {?>
                                    <option value='<?=$oznakaProv?>'>
                                        <?=$oznakaProv?>
                                    </option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="dropdown-divider"></div>
                            <div class="form-group m-0">
                                <label class="dropdown-header p-0 mb-1">Temperatura provodnika</label>
                                <input type="number" name="temperatura" placeholder="&deg;C" class="form-control mr-1" style="width:70px" required maxlength="3"
                                    max="120" min="-40" value="40">
                                <em>&deg;C</em>
                            </div>
                            <div class="dropdown-divider"></div>
                            <div class="form-group m-0">
                                <label class="dropdown-header p-0 mb-1">Dodatno opterecenje</label>
                                <select name="odo" class="form-control mr-1" style="width:70px" required>
                                    <option value=1 selected>1</option>
                                    <option value=1.6>1,6</option>
                                    <option value=2.5>2,5</option>
                                    <option value=4>4</option>
                                    <option value=8>8</option>
                                </select>
                                <em>x ODO</em>
                            </div>
                            <div class="dropdown-divider"></div>
                            <div class="form-group m-0">
                                <label class="dropdown-header p-0 mb-1">Max. naprezanje</label>
                                <input type="number" name="naprezanje" placeholder="sigma" class="form-control" style="width:85px" required maxlength="2"
                                    max="50" min="1" step="0.1" value="9">
                                <em>daN/mm&sup2;</em>
                            </div>
                            <input type="hidden" name="projName" />
                            <input type="hidden" name="projId" />
                            <input type="hidden" name="jsID" />
                            <input type="hidden" name="tackeTla" />
                            <input type="hidden" name="objektiTrase" />
                            <input type="hidden" name="stubovi" />
                            <input type="hidden" name="tackeLanc" />

                            <div class="dropdown-divider"></div>
                            <button id="getLancanica" type="submit" class="btn btn-danger" name="action" value="getLancanica">
                                <i class="fas fa-code"></i> Pokreni proracun</button>
                        </form>
                    </div>
                </div>

                <div class="dropdown ml-auto">
                    <button id="dropdownUser" class=" hederButton btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="fas fa-user"></i>
                        <span>
                            <?=$user["ime"]?>
                        </span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <button id="userInfo_button" class="dropdown-item" data-toggle="tooltip" data-placement="bottom" title="information about user activities">
                            <i class="fas fa-info"></i> user info</button>
                        <div class="dropdown-divider"></div>
                        <button class="dropdown-item text-danger" type="submit" form="logoutForm" name="action" value="Logout" data-toggle="tooltip"
                            data-placement="bottom" title="logout and exit app">
                            <i class="fas fa-sign-out-alt"></i> logout</button>
                        <div class="dropdown-divider"></div>
                    </div>
                </div>
            </div>

        </header>

        <div id="drawingKontejner">
            <div id="drawing"></div>
            <div id="map_container">
                <div id="map" class="map"></div>
                <select id="layer-select">
                    <option value="Aerial">Aerial</option>
                    <option value="AerialWithLabels" selected>Aerial with labels</option>
                    <option value="Road">Road</option>
                </select>
                <style>
                    .ol-full-screen {
                        right: 3em !important;
                    }

                    .ol-rotate {
                        right: 5em !important;
                    }
                </style>
            </div>
        </div>

        <button id="map_toggle" class="btn btn-primary p-1">map
            <br>
            <i class="fas fa-2x fa-angle-double-right"></i>
        </button>

        <footer class="bg-info">
            <!-- 
		<button id="plus" class="btn btn-primary">+</button>
  		<button id="minus" class="btn btn-primary">-</button>
  		<button id="btn_ZoomOut" class="btn btn-primary"><i class="fas fa-search-minus"></i></button>
  		<button id="btn_ZoomIn" class="btn btn-primary"><i class="fas fa-search-plus"></i></button>
         -->
            <div class="footer_left">
                <button id="snap" class="btn btn-primary futerButton">snapOFF</button>
                <button id="ordinata" class="btn btn-primary futerButton">ordinataON</button>
                <button id="btn_ZoomReset" class="btn btn-primary futerButton">reset zoom</button>
                <div>
                    <span data-toggle="tooltip" data-placement="top" title="stacionaza">
                        <em>x:&nbsp;</em>
                        <form id="form_x_koor">
                            <input id="x_koor" class="form-control p-1" type="number" maxLength="7" min="0" max="9999" step="0.01" oninput="this.value=this.value.slice(0,this.maxLength);"
                            />
                        </form>
                    </span>
                </div>
                <div>
                    <span data-toggle="tooltip" data-placement="top" title="kota terena">
                        <em>y:&nbsp;</em>
                        <strong id="y_koor">0.0</strong>
                    </span>
                </div>
                <div>
                    <span data-toggle="tooltip" data-placement="top" title="visina lancanice">
                        <em>h:&nbsp;</em>
                        <strong id="h_koor">0.0</strong>
                    </span>
                </div>

            </div>
            <div class="footer_right">
                <button style="display: none" id="btn_saveMap" class="btn btn-primary futerButton">
                    <i class="far fa-save"></i> sacuvaj mapu</button>
                <button id="btn_centerMap" class="btn btn-primary futerButton">
                    <i class="fas fa-link"></i> poveži mapu</button>
            </div>


        </footer>

    </div>

    <div id="openProject_dialog" title="Ucitaj projekat" style="z-index: 999">
        <div id="openProject_dialog_cointainer" class="h-100">
            <div class="row mx-0 h-100">
                <div class="masterView col-3 col-3-sm p-0">
                    <table class="table table-striped table-hover" id="tableOpenProj">
                        <thead>
                            <tr>
                                <th class="text-center" title="">odaberi:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="p-1"> primer </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="detailView col-9 col-9-sm">
                    <div>
                        <header class="my-2">
                            <h3>Detalji projekta:
                                <em class="dialogProjData">-</em>
                            </h3>
                        </header>
                        <main class="mx-2 my-3">
                            <div class="row p-0">
                                <div class="col-5 col-5-sm">vreme kreiranja</div>
                                <div class="col-7 col-7-sm dialogProjData" id="projVreme">-</div>
                            </div>
                            <div class="row p-0">
                                <div class="col-5 col-5-sm">provodnik</div>
                                <div class="col-7 col-7-sm dialogProjData" id="projProv">-</div>
                            </div>
                            <div class="row p-0">
                                <div class="col-5 col-5-sm">max naprezanje</div>
                                <div class="col-7 col-7-sm dialogProjData" id="projNaprezanje">-</div>
                            </div>
                            <div class="row p-0">
                                <div class="col-5 col-5-sm">odo</div>
                                <div class="col-7 col-7-sm dialogProjData" id="projOdo">-</div>
                            </div>
                            <div class="row p-0">
                                <div class="col-5 col-5-sm">temperatura</div>
                                <div class="col-7 col-7-sm dialogProjData" id="projTemperatura">-</div>
                            </div>
                            <div class="row p-0">
                                <div class="col-5 col-5-sm">br. tacaka tla</div>
                                <div class="col-7 col-7-sm dialogProjData" id="projTlo">-</div>
                            </div>
                            <div class="row p-0">
                                <div class="col-5 col-5-sm">br. objekata na trasi</div>
                                <div class="col-7 col-7-sm dialogProjData" id="projObjektiTrase">-</div>
                            </div>
                            <div class="row p-0">
                                <div class="col-5 col-5-sm">br. stubnih mesta</div>
                                <div class="col-7 col-7-sm dialogProjData" id="projStub">-</div>
                            </div>
                            <div class="row p-0">
                                <div class="col-5 col-5-sm">br. tacaka lancanice</div>
                                <div class="col-7 col-7-sm dialogProjData" id="projLanc">-</div>
                            </div>
                        </main>
                        <footer class="text-center">
                            <button class="btn btn-danger rounded" id="deleteProj" title="obrisi projekat">
                                <i class="fas fa-trash-alt"></i> obrisi projekat</button>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="unesiStubove_dialog" title="Rasporedi stubove" style="z-index: 999">
        <div class="" id="unesiStubove_dialog_cointainer" style="border: none;">

            <table class="table table-striped table-hover" id="tableNo1">
                <thead>
                    <tr>
                        <th class="col1" data-toggle="tooltip" title="redni broj stubnog mesta">rb</th>
                        <th class="col2" data-toggle="tooltip" title="stacionaza stuba">x</th>
                        <th class="col3" data-toggle="tooltip" title="visina vesanja najnizeg provodnika">h</th>
                        <th class="col4" data-toggle="tooltip" title="tip stuba">tip stuba</th>
                        <th class="col5" data-toggle="tooltip" title="tip konzole">konzola</th>
                        <th class="col6" data-toggle="tooltip" title="">akcija</th>
                    </tr>
                </thead>
                <tbody>
                    <form id="form_stubovi" action="#">
                        <tr data-id="0">
                            <!-- ne traba mi bootstra tooltip jer koristim ovaj obican(bs je prevelik) -->
                            <!--  form="form_stubovi" da bi forma uzela u obzir min max -->
                            <td class="col1">
                                <input form="form_stubovi" title="redni broj stubnog mesta" type="text" name="rb-0" class="form-control text-right" value=1
                                    required> </td>
                            <td class="col2">
                                <input form="form_stubovi" title="stacionaza stuba" type="number" name="x-0" class="form-control text-right" required min="0"
                                    step="0.01"> </td>
                            <td class="col3">
                                <input form="form_stubovi" title="visina vesanja najnizeg provodnika" type="number" name="h-0" class="form-control text-right"
                                    required min="1" step="0.1" max="50"> </td>
                            <td class="col4">
                                <input form="form_stubovi" title="tip stuba" type="text" name="tip-0" class="form-control text-right" required disabled value="-"> </td>
                            <td class="col5">
                                <input form="form_stubovi" title="tip konzole" type="text" name="konzola-0" class="form-control text-right" required disabled
                                    value="-"> </td>
                            <td class="col6 text-center p-0 m-0">
                                <button form="form_stubovi" title="obrisi" type="button" class="obrisiRed btn btn-danger rounded" tabindex="-1">obrisi</button>
                            </td>
                        </tr>
                    </form>
                    <input form="form_stubovi" type="submit" style="display: none">
                </tbody>
            </table>

            <div class="d-flex justify-content-center my-1">
                <button type="button" class="btn btn-primary rounded" id="dodajRed">dodaj stub</button>
            </div>

        </div>
    </div>

    <div class="modal fade" id="modalTextInput">
        <div class="modal-dialog modal-dialog-centered modal-sm">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Sacuvaj projekat</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form id="formModalInput">
                        <input type="hidden" name="action" required>
                        <input type="text" name="projName" maxLength="50" size="30" placeholder="unesi naziv novog projekta" required>
                        
                    </form>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        <i class="fas fa-ban"></i>
                    </button>
                    <button form="formModalInput" type="submit" class="btn btn-success">
                        <i class="fas fa-check-circle"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalLatLong">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Unesi koordinate uzduznog profila</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form id="formLatLong" class="text-center">
                        <div class="latlongPoint">
                            <label>pocetna tacka</label>
                            <input type="number" title="latitude" name="latFirst" placeholder="43.7077111" min="0" max="90" step="any" required value="43.753611215598255">
                            <input type="number" title="longitude" name="longFirst" placeholder="20.6895211" min="0" max="90" step="any" required value="20.5740835546875">
                            <button tabindex="-1" type="button" class="btn btn-primary btn-sm pickCoord" title="odabir tacke klikom na mapu" data-toggle="tooltip">
                                <i class="fas fa-crosshairs"></i>
                            </button>
                        </div>
                        <button tabindex="-1" type="button" class="btn btn-primary btn-sm m-1" title="zameni tacke" id="swapLatLong" data-toggle="tooltip">
                            <i class="fas fa-retweet"></i>
                        </button>
                        <div class="latlongPoint">
                            <label>krajnja tacka</label>
                            <input type="number" title="latitude" name="latLast" placeholder="43.707841299" min="0" max="90" step="any" required value="43.754">
                            <input type="number" title="longitude" name="longLast" placeholder="20.689899" min="0" max="90" step="any" required value="20.566">
                            <button tabindex="-1" type="button" class="btn btn-primary btn-sm pickCoord" data-id="b" title="odabir tacke klikom na mapu"
                                data-toggle="tooltip">
                                <i class="fas fa-crosshairs"></i>
                            </button>
                        </div>
                    </form>
                    <p class="text-center mt-2">rastojanje izmedju tacaka je:
                        <strong></strong>
                    </p>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        <i class="fas fa-ban"></i> cancel
                    </button>
                    <button form="formLatLong" type="submit" class="btn btn-success">
                        <i class="fas fa-check-circle"></i> ok
                    </button>
                </div>
            </div>
        </div>
    </div>

    <form style="display: none;" id="logoutForm" action="../control/Controller_user.php" method="POST"></form>


    <div id="userInfo_dialog" title="Informacije o korisniku" style="z-index: 999">
        <div id="userInfo_dialog_cointainer" class="h-100">
            <div class="row mx-0 h-100">
                <div class="detailView col-12 col-12-sm">
                    <div>
                        <header class="my-2">
                            <h2>
                                <i class="fas fa-id-card"></i>
                                <?=$user["ime"]?>
                            </h2>
                            <h5>
                                <span>account type: </span>
                                <strong class="badge badge-primary">
                                    <?=$user["level"]?>
                                </strong>
                            </h5>
                            <h5>
                                <span>username: </span>
                                <strong>
                                    <?=$user["username"]?>
                                </strong>
                            </h5>
                            <h5>
                                <span>email: </span>
                                <strong>
                                    <?=$user["email"]?>
                                </strong>
                            </h5>
                        </header>
                        <main class="my-3">
                            <table class="table table-striped table-hover">
                                <tr>
                                    <th>kreirano /
                                        <span class="badge badge-primary">dozvoljeno</span>
                                    </th>
                                    <th class="text-center">24h</th>
                                    <th class="text-center">30 dana</th>
                                    <th class="text-center">ukupno</th>
                                </tr>
                                <tr class="">
                                    <td class="">lančanica</td>
                                    <td class="text-center">
                                        <strong class="brojJS">0</strong> /
                                        <strong class="brojJS_quota badge badge-primary">0</strong>
                                    </td>
                                    <td class="text-center">
                                        <strong class="brojJS">0</strong> /
                                        <strong class="brojJS_quota badge badge-primary">0</strong>
                                    </td>
                                    <td class="text-center">
                                        <strong class="brojJS">0</strong>
                                    </td>
                                </tr>
                                <tr class="">
                                    <td> projekata</td>
                                    <td class="text-center">
                                        <strong class="brojProj">0</strong> /
                                        <strong class="brojProj_quota badge badge-primary">0</strong>
                                    </td>
                                    <td class="text-center">
                                        <strong class="brojProj">0</strong> /
                                        <strong class="brojProj_quota badge badge-primary">0</strong>
                                    </td>
                                    <td class="text-center">
                                        <strong class="brojProj">0</strong>
                                    </td>
                                </tr>
                                <tr class="">
                                    <td> pdf profila </td>
                                    <td class="text-center">
                                        <strong class="brojPDF">0</strong> /
                                        <strong class="brojPDF_quota badge badge-primary">0</strong>
                                    </td>
                                    <td class="text-center">
                                        <strong class="brojPDF">0</strong> /
                                        <strong class="brojPDF_quota badge badge-primary">0</strong>
                                    </td>
                                    <td class="text-center">
                                        <strong class="brojPDF">0</strong>
                                    </td>
                                </tr>
                            </table>

                        </main>
                        <footer class="text-center">
                            <!-- <button class="btn btn-danger rounded" id="deleteProj" title="obrisi projekat">
                                <i class="fas fa-trash-alt"></i> obrisi projekat</button> -->
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="tabelaUgiba_dialog" title="podaci" style="z-index: 999">
        <div id="tabelaUgiba_dialog_cointainer" class="h-100">
            <div class="row mx-0 h-100">
                <div class="detailView col-12 col-12-sm">
                    <div>
                        <header class="my-2">
                            <style>
                                #tabelaUgiba_dialog_cointainer header span {
                                    font-size: 0.85em;
                                }
                            </style>
                            <h2 class="text-center">
                                Tabela ugiba [cm]
                            </h2>
                            <h5>
                                <span>provodnik: </span>
                                <strong>-</strong>
                            </h5>
                            <h5>
                                <span class=""> spec / rez tezina
                                    <em>[daN/(m&CenterDot;mm&sup2;)]</em>: </span>
                                <strong>-</strong> /
                                <strong>-</strong>
                            </h5>
                            <h5>
                                <span>kritican raspon
                                    <em>[m]</em>: </span>
                                <strong>-</strong>
                                <span class="ml-4"> idealni raspon
                                    <em>[m]</em>: </span>
                                <strong>-</strong>
                                <span class="ml-4"> idealni cosFi: </span>
                                <strong>-</strong>
                            </h5>
                        </header>
                        <main class="my-4">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="p-0 m-0">
                                            <div class="text-right">temp
                                                <em>[&deg;C]</em>
                                            </div>
                                            <div class="text-left">raspon
                                                <em>[m]</em>
                                                </span>
                                            </div>
                                            <th class="text-center">-20</th>
                                            <th class="text-center">-10</th>
                                            <th class="text-center">0</th>
                                            <th class="text-center">10</th>
                                            <th class="text-center">20</th>
                                            <th class="text-center">40</th>
                                            <th class="text-center">60</th>
                                            <th class="text-center">80</th>
                                            <th class="text-center">-5led</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="">
                                        <td class="font-weight-bold text-center">/</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                    </tr>
                                </tbody>
                            </table>

                        </main>
                        <footer class="text-center">
                            <!-- <button class="btn btn-danger rounded" id="deleteProj" title="obrisi projekat">
                                <i class="fas fa-trash-alt"></i> obrisi projekat</button> -->
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- odredjivanje dpi ekrana koji koristi klijent -->
    <!-- <div id='testdiv' style='height: 1in; left: -100%; position: absolute; top: -100%; width: 1in;'></div>
    <script type='text/javascript'>
        dpi_x = document.getElementById('testdiv').offsetWidth;
        dpi_y = document.getElementById('testdiv').offsetHeight;
    </script> -->


    <div id="viewProv_dialog" title="Podaci o provodnicima" style="z-index: 999">
        <div id="viewProv_dialog_cointainer" class="h-100">
            <div class="row mx-0 h-100">
                <div class="masterView col-4 col-4-sm p-0">
                    <table class="table table-striped table-hover" id="tableViewProv">
                        <thead>
                            <tr>
                                <th class="text-center" title="">odaberi provodnik:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="p-1"> - </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="detailView col-8 col-8-sm">
                    <div>
                        <form id="form_provodnici" onsubmit="return false">
                            <header class="my-2">
                                <h3>
                                    <em class="p-0 dialogProvData">
                                        <input name="provName" type="text" maxlength="32" disabled class="form-control" required/>
                                    </em>
                                </h3>
                            </header>
                            <main class="mx-2 my-3">
                                <div class="row p-0">
                                    <div class="col-7  col-7-sm">presek
                                        <em>[mm&sup2;]</em>
                                    </div>
                                    <div class="p-0 mb-1 col-5 col-5-sm dialogProvData" id="">
                                        <input form="form_provodnici" name="provPresek" type="number" min="1" step="0.01" max="999" disabled class="form-control"
                                            required/>
                                    </div>
                                </div>
                                <div class="row p-0">
                                    <div class="col-7  col-7-sm">precnik
                                        <em>[mm]</em>
                                    </div>
                                    <div class="p-0 mb-1 col-5 col-5-sm dialogProvData" id="">
                                        <input name="provPrecnik" type="number" min="1" step="0.01" max="99" disabled class="form-control" required/>
                                    </div>
                                </div>
                                <div class="row p-0">
                                    <div class="col-7  col-7-sm">spec.tezina
                                        <em>[daN/(m&CenterDot;mm&sup2;)]</em>
                                    </div>
                                    <div class="p-0 mb-1 col-5 col-5-sm dialogProvData" id="">
                                        <input name="provTezina" type="number" min="0.00001" step="0.0000000001" max="0.1" disabled class="form-control" required/>
                                    </div>
                                </div>
                                <div class="row p-0">
                                    <div class="col-7  col-7-sm">modul elasticnosti
                                        <em>[daN/(mm&sup2;)]</em>
                                    </div>
                                    <div class="p-0 mb-1 col-5 col-5-sm dialogProvData" id="">
                                        <input name="provE" type="number" min="1000" step="10" max="100000" disabled class="form-control" required/>
                                    </div>
                                </div>
                                <div class="row p-0">
                                    <div class="col-7  col-7-sm">koef. &alpha;
                                        <em>[1/&deg;C]</em>
                                    </div>
                                    <div class="p-0 mb-1 col-5 col-5-sm dialogProvData" id="">
                                        <input name="provAlfa" type="number" min="0.0000001" step="0.0000000001" max="0.001" disabled class="form-control" required/>
                                    </div>
                                </div>
                                <div class="row p-0">
                                    <div class="col-7  col-7-sm">I
                                        <sub>max</sub>
                                        <em>[A]</em>
                                    </div>
                                    <div class="p-0 mb-1 col-5 col-5-sm dialogProvData" id="">
                                        <input name="provI" type="number" min="0" step="1" max="10000" disabled class="form-control" required/>
                                    </div>
                                    <input type="hidden" name="provId">
                                </div>
                            </main>
                            <footer class="text-center">
                                <button type="button" class="btn btn-primary rounded" id="copyProv" title="kopiraj u nov provodnik" data-toggle="tooltip">
                                    <i class="fas fa-copy"></i> copy</button>
                                <button type="button" class="btn btn-info rounded" id="editProv" title="izmeni provodnik" data-toggle="tooltip">
                                    <i class="fas fa-edit"></i> edit</button>
                                <button type="button" class="btn btn-success rounded" id="saveProv" title="sacuvaj provodnik" data-toggle="tooltip">
                                    <i class="fas fa-save"></i> save</button>
                                <button type="button" class="btn btn-danger rounded" id="deleteProv" title="obrisi provodnik" data-toggle="tooltip">
                                    <i class="fas fa-trash-alt"></i> delete</button>
                                <button type="button" class="btn btn-danger rounded" id="cancelProv" title="odustani" data-toggle="tooltip">
                                    <i class="fas fa-ban"></i> cancel</button>
                            </footer>
                            <!-- ne sme da bude na prvom mestu zbog disable toggle -->
                            <input type="submit" class="d-none">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalFirstAction" data-backdrop="static"> <!-- static - disable click outside modal -->
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content ">
                <!-- Modal Header -->
                <div class="modal-header bg-primary text-white">
                    <h4 class="modal-title">Dobrodošli</h4>
                    </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <h5 class="mb-4 text-center">Odaberite način unosa uzdužnog profila trase:</h5>
                    <div class="m-3 text-center">
                        <button id="startServer" class="btn btn-lg btn-primary" data-toggle="tooltip" title="učitaj projekat koji je prethodno sačuvan na serveru">
                            <i class="fas fa-cloud-download-alt"></i> projekat sa servera</button>
                    </div>
                    <div class="m-3 text-center">
                    <div class="btn-group" role="group" aria-label="First group">
                        <button id="startLocal" class="btn btn-lg btn-primary"  data-toggle="tooltip" title="učitaj uzdužni profil iz .txt fajla na kompjuteru">
                            <i class="fas fa-folder-open"></i> fajl sa lokalnog kompjutera</button>
                        <button id="startLocal_help" class="btn btn-lg btn-primary"  data-toggle="tooltip" title="preuzmi primer formata podatka u .txt fajlu - koristiti word za otvaranje fajla">
                            <i class="far fa-question-circle"></i> </button>
                        </div>        
                    </div>
                    <div class="m-3 text-center">
                        <button id="startMap" class="btn btn-lg btn-primary" data-toggle="tooltip" title="kreiraj profil izmedju dve tačke na mapi">
                            <i class="fas fa-globe"></i> korišćenje online mape</button>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer  bg-info">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        <i class="fas fa-ban"></i> cancel
                    </button>
                </div>
            </div>
        </div>
    </div>


</body>

<script src="libraries/js/jquery-3.3.1.min.js"></script>
<script src="libraries/js/popper.min.js"></script>
<script src="libraries/js/bootstrap.min.js"></script>

<script src="libraries/jquery-ui/jquery-ui.min.js"></script>
<script src="libraries/custom-file-input.js"></script>

<!-- ************ svg.js-->
<script src="libraries/svg.min.js" ></script>
<script src="libraries/svg.draggable.js" ></script>
<script src="libraries/jquery-svg-pan-zoom/compiled/jquery.svg.pan.zoom.js" ></script>

<script src="https://openlayers.org/en/v5.1.3/build/ol.js" ></script>


<!-- <script src="js/crtanje_globalVar.js"></script>
<script src="js/crtanje_funkcije.js"></script>
<script src="js/crtanje_map.js"></script>
<script src="js/crtanje_naredbe.js"></script> -->

<!-- <script src="js/crtanje_test.js"></script> -->
<script src="js/crtanje_test_obf.js"></script>

<script src="js/webapp_lastScript.js"></script>
</html>

<?php 


?>